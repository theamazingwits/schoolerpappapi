<?php $__env->startSection('header_scripts'); ?>
 <link href="<?php echo e(CSS); ?>ajax-datatables.css" rel="stylesheet">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>


<div id="page-wrapper">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
	    <div class="container-fluid content-wrap">
			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="<?php echo e(PREFIX); ?>"><i class="fa fa-home"></i></a> </li>
						<li><a  href="<?php echo e(URL_COURSES_DASHBOARD); ?>"><?php echo e(getPhrase('master_setup_dashboard')); ?></a></li>
						<li><?php echo e($title); ?></li>
					</ol>
				</div>
			</div>
							
			<!-- /.row -->
			<section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item">
            <!--Start Panel-->
	            <div class="panel bgc-white-dark">
	                <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
	                    <h2 class="pull-left"> <?php echo e($title); ?> </h2>
	                    <div class="pull-right messages-buttons">
						 
							<!-- <a href="<?php echo e(URL_TOPICS_IMPORT); ?>" class="btn  btn-primary button helper_step4 panel-header-button" ><?php echo e(getPhrase('import')); ?></a> -->
							<a href="<?php echo e(URL_SUBJECT_BOOKS_ADD); ?>" class="btn  btn-primary button helper_step1 panel-header-button" ><?php echo e(getPhrase('create')); ?></a>
							 
						</div>
	                    <!--Start panel icons-->
	                    <!--End panel icons-->
	                </div>
	                <div class="panel-body panel-body-p">
						<div > 
						<table class="table table-striped table-bordered datatable" cellspacing="0" width="100%">
							<thead>
								<tr>
									 
								 
									<th id="helper_step2"><?php echo e(getPhrase('subject')); ?></th>
									<th><?php echo e(getPhrase('book_name')); ?></th>
									<th><?php echo e(getPhrase('file_name')); ?></th>
									<th><?php echo e(getPhrase('description')); ?></th>
									 
									<th id="helper_step3"><?php echo e(getPhrase('action')); ?></th>
								  
								</tr>
							</thead>
							 
						</table>
						</div>

					</div>
				</div>
			</section>
		</div>
		<!-- /.container-fluid -->
	</section>
</div>
<?php $__env->stopSection(); ?>
 
<?php $url = URL_SUBJECT_BOOKS_GET_LIST;
 
 ?>
<?php $__env->startSection('footer_scripts'); ?>
  
 <?php echo $__env->make('common.datatables', array('route'=>$url, 'route_as_url' => TRUE), array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
 <?php echo $__env->make('common.deletescript', array('route'=>URL_SUBJECT_BOOKS_DELETE), array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin.adminlayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>