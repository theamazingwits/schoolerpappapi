

					     <!-- <div class="panel panel-primary">
					      <div class="panel-heading"><?php echo e($heading); ?></div> -->
					      <?php if(!count($records)): ?>
					      <br>
					 		 <p> &nbsp;&nbsp;&nbsp;<?php echo e(getPhrase('no_data_available')); ?></p>
					 		 
					 	 <?php else: ?>

					    	<table class="table">	
					    	<thead>
					    		<tr>
					    			<th><strong><?php echo e(getPhrase('name')); ?></strong></th>
					    			<th><strong><?php echo e(getPhrase('item')); ?></strong></th>
					    			<th><strong><?php echo e(getPhrase('gateway')); ?></strong></th>
					    			<th><strong><?php echo e(getPhrase('paid')); ?></strong></th>
					    			<th><strong><?php echo e(getPhrase('status')); ?></strong></th>
					    		</tr>
					    	</thead>
					    	<tbody>
					    	<?php $__currentLoopData = $records; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

					    	<?php 
					    	$class = '';

					    		if($record->payment_status == 'success')
					    			{
					    				$class='label label-success';
					    			}
					    		else if($record->payment_status == 'pending')
					    			{
					    				$class='label label-warning';
					    			}
					    		else if($record->payment_status == 'cancelled')
					    			{
					    				$class='label label-danger';
					    			}

					    		?>
					 			<tr>
					 				<td><?php echo e(ucfirst($record->name)); ?></td>
					 				<td><?php echo e(ucfirst($record->item_name)); ?></td>
					 				<td><?php echo e(ucfirst($record->payment_gateway)); ?></td>
					 				
					 				<td><?php echo e($record->paid_amount); ?></td>
					 				<td>
					 				<span class="<?php echo e($class); ?>">
					 				<?php echo e(ucfirst($record->payment_status)); ?>

					 				</span>
					 				</td>
					 				 
					 			</tr>
					 		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

					    	</tbody>
					    	</table>  
					    <?php endif; ?>
					     
					   <!--  </div> -->