	<script src="<?php echo e(JS); ?>bootstrap-toggle.min.js"></script>
 	<script src="<?php echo e(JS); ?>jquery.dataTables.min.js"></script>
	<script src="<?php echo e(JS); ?>dataTables.bootstrap.min.js"></script>
	
	<?php 	$routeValue= $route; ?> 

	<?php if(!isset($route_as_url)): ?>
	
		<?php $routeValue =  route($route); ?>
	
	<?php endif; ?>
	
	<?php  
	$setData = array();
		if(isset($table_columns))
		{
			foreach($table_columns as $col) {
				$temp['data'] = $col;
				$temp['name'] = $col;
				array_push($setData, $temp);
			}
			$setData = json_encode($setData);
		}
	?>
 
 <?php if(isset($extra_var)): ?>
   <?php if($extra_var==1): ?>
   <script>
   	
    $(function() {
    $('#users-table').DataTable({
        processing: true,
        serverSide: true,
         type: 'GET',

        ajax: '<?php echo e($routeValue); ?>',
        <?php if(isset($user_type)): ?>
          <?php if($user_type=='student'): ?>
        columns: [
            { data: 'roll_no', name: 'roll_no' },
            { data: 'image', name: 'users.image' },
            { data: 'first_name', name: 'first_name' },
            { data: 'last_name', name: 'last_name' },
            { data: 'email', name: 'users.email' }
        ]
        <?php else: ?>
        columns: [
            { data: 'staff_id', name: 'staff_id' },
            { data: 'image', name: 'users.image' },
            { data: 'first_name', name: 'first_name' },
            { data: 'last_name', name: 'last_name' },
            { data: 'email', name: 'users.email' }
        ]
        <?php endif; ?>
          <?php endif; ?>
    });
});

   </script>
   <?php endif; ?>
<?php else: ?>
  <script>

  var tableObj;
  var myarray = [];
   
  // console.log(<?php echo json_encode($setData); ?>);
    $(document).ready(function(){
    	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        	}
		});
       // tableObj = $('.datatable').DataTable();
    	 
        
         // console.log(tableObj);
   		 tableObj = $('.datatable').DataTable({
              processing: true,
	            serverSide: true,
	            cache: true,
	            type: 'GET',
	            ajax: '<?php echo e($routeValue); ?>',
	            <?php if(isset($table_columns)): ?>
	            columns: <?php echo $setData; ?>

	            <?php endif; ?>
	    });
    });
  </script>
  <?php endif; ?>
