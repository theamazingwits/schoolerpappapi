<?php $__env->startSection('content'); ?>

<!-- <div id="page-wrapper">
<div class="container-fluid">
<div class="row">
  <div class="col-lg-12">
  <ol class="breadcrumb">

  <li><?php echo e($title); ?></li>
  </ol>
  </div>
</div> -->

<?php
 $date    = date("Y-m-d");
 $user_id = $user->id;
 $student_data        = App\Student::where('user_id','=',$user_id)->first();
 $latest_fee_category = App\FeeCategory::getStudentLatestFeeCategory($student_data->id);
 
 if($latest_fee_category!=0){
 $feeschules          = App\FeeScheduleParticular::where('feecategory_id','=',$latest_fee_category)
                                                ->where('start_date','<=',$date)
                                                ->get();

   
   $data         = new App\FeeParticularPayment();                                            
   $amount       = 0;
   $paid_amount  = 0;
   $balance      = 0;                                             
   $other_amount = 0;
   $previous_amount = 0;
   $term_number = array();
   $other_amount +=  $data->getScheduleTotalOtherAmount($latest_fee_category,$student_data->id);
// dd($other_amount);
   $previous_amount +=  $data->getSchedulePreviousAmount($latest_fee_category,$student_data->id);


 foreach ($feeschules as $feeschedule)
  {  
    
      
      $amount      += $data->getScheduleTotalAmount($feeschedule->id,$student_data->id);
      $paid_amount += $data->getScheduleTotalPaidAmount($latest_fee_category,$feeschedule->id,$student_data->id);
      $term_number[]  = $data->getTerms($feeschedule->id,$student_data->id);

  }
  $other_paid_amount = $data->getNonTermsPaidAmount($latest_fee_category,$student_data->id);
  $final_balance     = round($amount + $other_amount ) - round($paid_amount+$other_paid_amount);
}

?>

<?php if($latest_fee_category!=0): ?>

<?php if($previous_amount>0): ?>
  
  <div class="alert alert-warning" style="margin-bottom:0px !important;">
     <strong><?php echo e(getPhrase('Note:')); ?></strong>You Have Previous Term Balance <strong><?php echo e(getCurrencyCode()); ?> <?php echo e($previous_amount); ?></strong>
   </div> 
  
<?php endif; ?>

<?php endif; ?>

<?php echo Form::open(array('url' => URL_FEE_PAY_ONLINE, 'method' => 'POST','name'=>'formFeePay ', 'novalidate'=>'')); ?>

<?php if($latest_fee_category!=0): ?>
  <?php $__currentLoopData = $term_number; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $term): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
     <?php if($term->end_date >= $date): ?>
  <div class="alert alert-warning">
     <strong><?php echo e(getPhrase('Note:')); ?></strong> Term - <?php echo e($term->term_number); ?> is Enabled  <strong>(<?php echo e($term->start_date); ?> - <?php echo e($term->end_date); ?>)</strong> You Have Balance To Pay&nbsp;&nbsp; <strong><?php echo e(getCurrencyCode()); ?> <?php echo e($final_balance); ?></strong>&nbsp;&nbsp;&nbsp;&nbsp;
     <?php if($final_balance > 0): ?>
     <span><button class="btn btn-sm btn-primary button"><?php echo e(getPhrase('paynow')); ?></button></span>
     <?php endif; ?>
     <input type="hidden" name="pay_amount" value="<?php echo e($final_balance); ?>">

   </div> 

    <?php endif; ?>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?>
 <input type="hidden" name="current_feecategory_id" value="<?php echo e($latest_fee_category); ?>">
<?php echo Form::close(); ?>




<?php
    
   $crecords        = App\UserCertificates::DocumetsSubmitted();
   $user_submites   = App\UserCertificates::where('user_id',$user_id)
                                           ->where('is_submitted',1)
                                           ->orWhere('is_approved',1)
                                           ->groupby('notification_id')
                                           ->pluck('notification_id')
                                           ->toArray();

?>  

<?php if($crecords): ?>                                         

   <?php if(count($user_submites) > 0 ): ?>

      <?php $__currentLoopData = $crecords; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
         
         <?php if(in_array($value['id'], $user_submites)): ?>

         <?php else: ?>

           <div class="alert alert-danger">
        <strong>Note:</strong> <?php echo e($value['description']); ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo e(URL_UPLOAD_USER_CERTIFICATES.$value['id']); ?>" class="btn btn-primary btn-sm">Click here to upload</a>
      </div>

         <?php endif; ?>

       
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

   <?php else: ?>
     
       <?php $__currentLoopData = $crecords; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $crecord): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          
          <div class="alert alert-danger">
        <strong>Note:</strong> <?php echo e($crecord['description']); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo e(URL_UPLOAD_USER_CERTIFICATES.$crecord['id']); ?>" class="btn btn-primary btn-sm">Click here to upload</a>
      </div>

      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

  <?php endif; ?> 

<?php endif; ?>  

    <section id="main" class="main-wrap bgc-white-darkest" role="main">
             <div class="container-fluid content-wrap">
                <div class="row panel-grid grid-stack" id="panel-grid">

                             <!--** EXAM CATEGORIES **-->
                  <section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                          <!--Start Panel-->
                          <div class="panel pb-0 bgc-white-dark">
                              <div class="panel-body pt-2 grid-stack-handle">
                                  <div class="h-lg pos-r panel-body-p py-0">
                                      <h3 class=""><span class="fs-2 fw-light"><?php echo e(getPhrase('exam_categories')); ?></span> <i class="pull-right fa fa-random fs-1 m-2"></i></h3>
                                      <h6 class="c-primary lh-5"></h6>
                                      <br><br>
                                      <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="<?php echo e(URL_STUDENT_EXAM_CATEGORIES); ?>">
                                          <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
                                              <i class="icon-directions fs-4"></i>
                                          </span>
                                          <span class="d-inline-block align-top lh-7">
                                              <span class="fs-6 d-block"><?php echo e(getPhrase('view_all')); ?></span>
                                              <span class="c-gray fs-7">Click to View more details</span>
                                          </span>
                                          <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
                                      </a>
                                  </div>
                              </div>
                          </div>
                          <!--End Panel-->
                    </section>
 
                        <!--** EXAMS **-->
                     <section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-body pt-2 grid-stack-handle">
                                <div class="h-lg pos-r panel-body-p py-0">
                                    <h3 class=""><span class="fs-2 fw-light"><?php echo e(getPhrase('exams')); ?></span> <i class="pull-right fa fa-clock-o fs-1 m-2"></i></h3>
                                    <h6 class="c-primary lh-5"></h6>
                                    <br><br>
                                    <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="<?php echo e(URL_STUDENT_QUIZ_DASHBOARD); ?>">
                                        <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
                                            <i class="icon-directions fs-4"></i>
                                        </span>
                                        <span class="d-inline-block align-top lh-7">
                                            <span class="fs-6 d-block"><?php echo e(getPhrase('view_all')); ?></span>
                                            <span class="c-gray fs-7">Click to View more details</span>
                                        </span>
                                        <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!--End Panel-->
                    </section>

                        <!--** Subjects Reports **-->
                   <section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                       <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-body pt-2 grid-stack-handle">
                                <div class="h-lg pos-r panel-body-p py-0">
                                    <h3 class=""><span class="fs-2 fw-light"><?php echo e(getPhrase('subjects_reports')); ?></span> <i class="pull-right fa fa-flag fs-1 m-2"></i></h3>
                                    <h6 class="c-primary lh-5"></h6>
                                    <br><br>
                                    <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="<?php echo e(URL_STUDENT_ANALYSIS_SUBJECT.Auth::user()->slug); ?>">
                                        <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
                                            <i class="icon-directions fs-4"></i>
                                        </span>
                                        <span class="d-inline-block align-top lh-7">
                                            <span class="fs-6 d-block"><?php echo e(getPhrase('view_analysis')); ?></span>
                                            <span class="c-gray fs-7">Click to View more details</span>
                                        </span>
                                        <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!--End Panel-->
                    </section>

                    <!--  ** Attendance Report **  -->
                    <section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-body pt-2 grid-stack-handle">
                                <div class="h-lg pos-r panel-body-p py-0">
                                    <h3 class=""><span class="fs-2 fw-light"><?php echo e(getPhrase('attendance_report')); ?></span> <i class="pull-right fa fa-calendar-check-o fs-1 m-2"></i></h3>
                                    <h6 class="c-primary lh-5"></h6>
                                    <br><br>
                                    <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="<?php echo e(URL_STUDENT_ATTENDENCE_REPORT.'/'.Auth::user()->slug); ?>">
                                        <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
                                            <i class="icon-directions fs-4"></i>
                                        </span>
                                        <span class="d-inline-block align-top lh-7">
                                            <span class="fs-6 d-block"><?php echo e(getPhrase('view_analysis')); ?></span>
                                            <span class="c-gray fs-7">Click to View more details</span>
                                        </span>
                                        <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!--End Panel-->
                    </section>

                    <!--  ** Timetable  **  -->
                    <section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                              <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-body pt-2 grid-stack-handle">
                                <div class="h-lg pos-r panel-body-p py-0">
                                    <h3 class=""><span class="fs-2 fw-light"><?php echo e(getPhrase('timetable')); ?></span> <i class="pull-right fa fa-calendar fs-1 m-2"></i></h3>
                                    <h6 class="c-primary lh-5"></h6>
                                    <br><br>
                                    <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="<?php echo e(URL_TIMETABLE_STAFF_STUDENT_PRINT.Auth::user()->slug); ?>">
                                        <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
                                            <i class="icon-directions fs-4"></i>
                                        </span>
                                        <span class="d-inline-block align-top lh-7">
                                            <span class="fs-6 d-block"><?php echo e(getPhrase('view_report')); ?></span>
                                            <span class="c-gray fs-7">Click to View more details</span>
                                        </span>
                                        <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!--End Panel-->
                    </section>

                    <!--  **  Marks **  -->
                    <section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                  <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-body pt-2 grid-stack-handle">
                                <div class="h-lg pos-r panel-body-p py-0">
                                    <h3 class=""><span class="fs-2 fw-light"><?php echo e(getPhrase('marks')); ?></span> <i class="pull-right fa fa-percent fs-1 m-2"></i></h3>
                                    <h6 class="c-primary lh-5"></h6>
                                    <br><br>
                                    <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="<?php echo e(URL_STUDENT_RESULTS.Auth::user()->slug); ?>">
                                        <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
                                            <i class="icon-directions fs-4"></i>
                                        </span>
                                        <span class="d-inline-block align-top lh-7">
                                            <span class="fs-6 d-block"><?php echo e(getPhrase('view_all')); ?></span>
                                            <span class="c-gray fs-7">Click to View more details</span>
                                        </span>
                                        <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!--End Panel-->
                    </section>

                    <!--  ** Library History  **   -->
                    <section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                     <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-body pt-2 grid-stack-handle">
                                <div class="h-lg pos-r panel-body-p py-0">
                                    <h3 class=""><span class="fs-2 fw-light"><?php echo e(getPhrase('library_history')); ?></span> <i class="pull-right fa fa-book fs-1 m-2"></i></h3>
                                    <h6 class="c-primary lh-5"></h6>
                                    <br><br>
                                    <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="<?php echo e(URL_USER_LIBRARY_DETAILS.Auth::user()->slug); ?>">
                                        <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
                                            <i class="icon-directions fs-4"></i>
                                        </span>
                                        <span class="d-inline-block align-top lh-7">
                                            <span class="fs-6 d-block"><?php echo e(getPhrase('view_all')); ?></span>
                                            <span class="c-gray fs-7">Click to View more details</span>
                                        </span>
                                        <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!--End Panel-->
                    </section>



                     <!--  **  LIBRARY HISTORY  **  -->
                    <section class="col-sm-12 col-md-12 col-xl-6 panel-wrap panel-grid-item grid-stack-item">
                        <!--Start Panel-->
                        <div class="panel bgc-white-dark">
                            <div class="panel-header clearfix grid-stack-handle panel-header-p bgc-white-dark panel-header-sm">
                                <h2 class="pull-left">
                                  <!-- <i class="menu-icon fa fa-book"></i> -->
                                <?php echo e(getPhrase('library_history')); ?> </h2>
                                 <div class="panel-input pull-right hidden-xs-down">
                                    <input class="form-control search-control" data-list=".referral-list" type="text" placeholder="Search">
                                    <a href="" class="clear-style"><i class="fa fa-search"></i></a>
                                </div>
                                <!--Start panel icons-->
                                <div class="panel-icons panel-icon-slide ">
                                    <ul>
                                        <li><a href=""><i class="fa fa-angle-left"></i></a>
                                            <ul>
                                                <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                                                <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                <!--End panel icons-->
                            </div>
                            <div class="panel-body panel-body-p">
                               <?php 
                                $records = App\LibraryIssue::issueHistory('',5);
                                ?>

                              <table class="table ">
                                <thead>
                                  <tr>
                                    <th>
                                    <strong><?php echo e(getPhrase('sno')); ?></strong></th>
                                    <th><strong><?php echo e(getPhrase('title')); ?>

                                    </strong></th>
                                    <th><strong><?php echo e(getPhrase('number')); ?></th>
                                    <th><strong><?php echo e(getPhrase('issued')); ?>

                                    </strong></th>
                                    <th><strong><?php echo e(getPhrase('status')); ?>

                                    </strong></th>
                                  </tr>

                                </thead>
                                <tbody class="referral-list">
                                  <?php $sno=1;?>
                                  <?php if(count($records)): ?>
                                  <?php $__currentLoopData = $records; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                  <?php $class = 'badge badge-pill badge-info';
                                  if($record->issue_type=='issue' || $record->issue_type=='renewal')
                                    $class = 'badge badge-pill badge-danger';
                                  if($record->issue_type=='lost')
                                    $class = 'badge badge-pill badge-warning';
                                  ?>
                                  <tr>
                                    <td><?php echo e($sno++); ?></td>
                                    <td><?php echo e($record->title); ?></td>
                                    <td><?php echo e($record->library_asset_no); ?></td>
                                    <td><?php echo e(date('Y-m-d',strtotime($record->issued_on))); ?></td>
                                    <td><span class='<?php echo e($class); ?>' ><?php echo e(ucfirst($record->issue_type)); ?></span></td>
                                  </tr>
                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php else: ?>
                                          <tr><td><?php echo e(getPhrase('no_data_available')); ?></td></tr>
                                  <?php endif; ?>
                              </tbody>
                              </table>
                        <!--End Panel-->
                    </section>


                     <!--  **  TODAY CLASSES  **  -->
                    <section class="col-sm-12 col-md-12 col-xl-6 panel-wrap panel-grid-item grid-stack-item">
                        <!--Start Panel-->
                        <div class="panel bgc-white-dark">
                            <div class="panel-header clearfix grid-stack-handle panel-header-p bgc-white-dark panel-header-sm">
                                <h2 class="pull-left">
                                  <!-- <i class="menu-icon fa fa-bell"></i> -->
                                   <?php echo e(getPhrase("today's_classes")); ?> </h2>
                                 <div class="panel-input pull-right hidden-xs-down">
                                    <input class="form-control search-control" data-list=".referral-list1" type="text" placeholder="Search">
                                    <a href="" class="clear-style"><i class="fa fa-search"></i></a>
                                </div>
                                <!--Start panel icons-->
                                <div class="panel-icons panel-icon-slide ">
                                    <ul>
                                        <li><a href=""><i class="fa fa-angle-left"></i></a>
                                            <ul>
                                                <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                                                <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                <!--End panel icons-->
                            </div>
                            <div class="panel-body panel-body-p table-responsive">
                             <?php 
                            $records = App\Timetable::getStudentDayClasses();
                             ?>

                               <table class="table">
                                <thead>
                                  <tr>
                                    <th>
                                    <strong><?php echo e(getPhrase('sno')); ?></strong></th>
                                    <th><strong><?php echo e(getPhrase('subject')); ?>

                                    </strong></th>
                                    <th><strong><?php echo e(getPhrase('class')); ?></th>
                                    <th><strong><?php echo e(getPhrase('from')); ?>

                                    </strong></th>
                                    <th><strong><?php echo e(getPhrase('to')); ?>

                                    </strong></th>
                                  </tr>
                                </thead>
                                <tbody class="referral-list1">
                                <?php $sno=1;?>
                                <?php if(count($records)): ?>
                                <?php $__currentLoopData = $records; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                     
                                  <tr>
                                    <td><?php echo e($sno++); ?></td>
                                    <td><?php echo e($record->subject_title); ?></td>
                                    <td><?php echo e($record->course_title); ?></td>
                                    <td><?php echo e($record->start_time); ?></td>
                                    <td><?php echo e($record->end_time); ?></td>
                                  </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php else: ?>
                                <tr>
                                    <td><?php echo e(getPhrase('no_data_available')); ?></td>
                                </tr>
                                <?php endif; ?>
                                </tbody>
                              </table>
                            <!--End Panel-->
                        </section>


                        <section class="col-sm-12 col-md-12 col-xl-12 panel-wrap panel-grid-item grid-stack-item">
                        <!--Start Panel-->
                        <div class="panel bgc-white-dark">
                            <div class="panel-header clearfix grid-stack-handle panel-header-p bgc-white-dark panel-header-sm">
                                <!-- <h2 class="pull-left">
                                   <?php echo e(getPhrase("overall performance")); ?> </h2> -->


                <div class="row">
                  <?php $ids=[];?>
                  <?php for($i=0; $i<count($chart_data); $i++): ?>
                  <?php 
                  $newid = 'myChart'.$i;
                  $ids[] = $newid; ?>
                  <div class="col-md-6">            
                    <div class="panel panel-primary dsPanel"> 
                      <div class="panel-body" style="padding:1.5em; background-color: #f4f9fc;" >
                        <canvas id="<?php echo e($newid); ?>" width="100" height="60">
                        </canvas>           
                      </div>        
                     </div>     
                </div>

                <?php endfor; ?> 

                               </div>
                        </div>
                    </section>





                </div>


          
      </div>
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer_scripts'); ?>
<?php echo $__env->make('common.chart', array($chart_data,'ids' =>$ids), array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>;
<?php $__env->stopSection(); ?>
<?php echo $__env->make($layout, array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>