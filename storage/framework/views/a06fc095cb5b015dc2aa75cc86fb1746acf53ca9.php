<?php $__env->startSection('content'); ?>


				<section id="main" class="main-wrap bgc-white-darkest" role="main">
			<div class="container-fluid content-wrap">
                <div class="row">
                <div class="col-lg-12">
                    <ol class="breadcrumb">
                         <li><a href="<?php echo e(PREFIX); ?>"><i class="fa fa-home"></i></a> </li>
                         <li>
                    <a href="<?php echo e(URL_ACADEMICOPERATIONS_DASHBOARD); ?>">
                        <?php echo e(getPhrase('academic_operations')); ?>

                    </a>
                </li>
                         <li><?php echo e($title); ?></li>
                    </ol>
                </div>
            </div>
            
				<div class="row panel-grid grid-stack">

					<section data-gs-min-width="3" data-gs-min-height="22" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-body pt-2 grid-stack-handle">
                                <div class="h-lg pos-r panel-body-p py-0">
									<h3 class=""><span class="fs-3 fw-light"><?php echo e(getPhrase('academic_years')); ?></span> <i class="pull-right fa fa-graduation-cap fs-1 m-2"></i></h3>
									<br><br><br><br>
                                    <a href="<?php echo e(URL_MASTERSETTINGS_ACADEMICS); ?>" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
									
                                </div>
                                <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80" sparkLineColor="#fafafa"  sparkFillColor="#97d881" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>
					


					<section data-gs-min-width="3" data-gs-min-height="22" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-body pt-2 grid-stack-handle">
                                <div class="h-lg pos-r panel-body-p py-0">
									<h3 class=""><span class="fs-3 fw-light"><?php echo e(getPhrase('course_list')); ?></span> <i class="pull-right fa fa-list-ul fs-1 m-2"></i></h3>
									<br><br><br><br>
                                    <a href="<?php echo e(URL_MASTERSETTINGS_COURSE); ?>" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
								
                                </div>
                                    <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80" sparkLineColor="#fafafa"  sparkFillColor="#f74948" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>
					


					<section data-gs-min-width="3" data-gs-min-height="22" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-body pt-2 grid-stack-handle">
                                <div class="h-lg pos-r panel-body-p py-0">
									<h3 class=""><span class="fs-3 fw-light"><?php echo e(getPhrase('add_course')); ?></span> <i class="pull-right fa fa-plus-square fs-1 m-2"></i></h3>
									<br><br><br><br>
                                    <a href="<?php echo e(URL_MASTERSETTINGS_COURSE_ADD); ?>" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
									
                                </div>
                                <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80" sparkLineColor="#fafafa"  sparkFillColor="#353f4d" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>
					


					<section data-gs-min-width="3" data-gs-min-height="22" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-body pt-2 grid-stack-handle">
                                <div class="h-lg pos-r panel-body-p py-0">
									<h3 class=""><span class="fs-3 fw-light"><?php echo e(getPhrase('subject_master')); ?></span> <i class="pull-right fa fa-suitcase fs-1 m-2"></i></h3>
									<br><br><br><br>
                                    <a href="<?php echo e(URL_MASTERSETTINGS_SUBJECTS); ?>" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
									
                                </div>
                                <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80" sparkLineColor="#fafafa"  sparkFillColor="#f3d74c" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>
					

					<section data-gs-min-width="3" data-gs-min-height="22" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-body pt-2 grid-stack-handle">
                                <div class="h-lg pos-r panel-body-p py-0">
									<h3 class=""><span class="fs-3 fw-light"><?php echo e(getPhrase('subject_topics')); ?></span> <i class="pull-right fa fa-tasks fs-1 m-2"></i></h3>
									<br><br><br><br>
                                    <a href="<?php echo e(URL_MASTERSETTINGS_TOPICS); ?>" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
									
                                </div>
                                <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80" sparkLineColor="#fafafa"  sparkFillColor="#aaaaaa" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>
					

					<section data-gs-min-width="3" data-gs-min-height="22" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-body pt-2 grid-stack-handle">
                                <div class="h-lg pos-r panel-body-p py-0">
									<h3 class=""><span class="fs-3 fw-light"><?php echo e(getPhrase('allocate_subject_to_course')); ?></span> <i class="pull-right fa fa-external-link-square fs-1 m-2"></i></h3>
									<br><br>
                                    <a href="<?php echo e(URL_MASTERSETTINGS_COURSE_SUBJECTS_ADD); ?>" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
								
                                </div>
                                    <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80" sparkLineColor="#fafafa"  sparkFillColor="#97d881" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>
					

					<section data-gs-min-width="3" data-gs-min-height="22" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-body pt-2 grid-stack-handle">
                                <div class="h-lg pos-r panel-body-p py-0">
									<h3 class=""><span class="fs-3 fw-light"><?php echo e(getPhrase('allocate_staff_to_subject')); ?></span> <i class="pull-right fa fa-street-view fs-1 m-2"></i></h3>
									<br><br><br><br>
                                    <a href="<?php echo e(URL_MASTERSETTINGS_COURSE_SUBJECTS); ?>" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
								
                                </div>
                                    <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80" sparkLineColor="#fafafa"  sparkFillColor="#46aaf9" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>
                    <section data-gs-min-width="3" data-gs-min-height="22" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-body pt-2 grid-stack-handle">
                                <div class="h-lg pos-r panel-body-p py-0">
									<h3 class=""><span class="fs-3 fw-light"><?php echo e(getPhrase('subject_books')); ?></span> <i class="pull-right fa fa-tasks fs-1 m-2"></i></h3>
									<br><br><br><br>
                                    <a href="<?php echo e(URL_MASTERSETTINGS_SUBJECTBOOKS); ?>" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
									
                                </div>
                                <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80" sparkLineColor="#fafafa"  sparkFillColor="#97d881" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>

  
                 

		</div>
		</div>
	</section>


		<!-- /#page-wrapper -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer_scripts'); ?>
 
 

<?php $__env->stopSection(); ?>

<?php echo $__env->make($layout, array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>