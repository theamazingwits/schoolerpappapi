<?php $__env->startSection('header_scripts'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo e(CSS); ?>select2.css">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div id="page-wrapper">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
	    <div class="container-fluid content-wrap">
			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="/"><i class="fa fa-home"></i></a> </li>
						<li><a  href="<?php echo e(URL_COURSES_DASHBOARD); ?>"><?php echo e(getPhrase('master_setup_dashboard')); ?></a></li>
						<li><a href="<?php echo e(URL_TOPICS); ?>"><?php echo e(getPhrase('subject_books')); ?></a> </li>
						<li class="active"><?php echo e(isset($title) ? $title : ''); ?></li>
					</ol>
				</div>
			</div>
				<?php echo $__env->make('errors.errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
			<!-- /.row -->
			
			<section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item">
            <!--Start Panel-->
	            <div class="panel bgc-white-dark">
	                <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
	                    <h2 class="pull-left"> <?php echo e($title); ?> </h2>
	                    <div class="pull-right messages-buttons">
							<a href="<?php echo e(URL_SUBJECT_BOOKS); ?>" class="btn  btn-primary button helper_step1 panel-header-button" ><?php echo e(getPhrase('list')); ?></a>
						</div>
	                    <!--Start panel icons-->
	                    <!--End panel icons-->
	                </div>
	                <div class="panel-body panel-body-p" ng-controller="angTopicsController">
					<?php $button_name = getPhrase('create'); ?>
					<?php if($record): ?>
					 <?php $button_name = getPhrase('update'); ?>
						<?php echo e(Form::model($record, 
						array('url' => URL_SUBJECT_BOOKS_EDIT.'/'.$record->slug, 
						'method'=>'patch' ,'novalidate'=>'','name'=>'formSubjectbooks','files'=>TRUE))); ?>

					<?php else: ?>
						<?php echo Form::open(array('url' => URL_SUBJECT_BOOKS_ADD, 'method' => 'POST', 
						'novalidate'=>'','name'=>'formSubjectbooks','files'=>TRUE)); ?>

					<?php endif; ?>

					 <?php echo $__env->make('mastersettings.subjectbooks.form_elements', 
					 array('button_name'=> $button_name,
					 'record'=> $record,),
					 array('subjects'=>$subjects), array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
					 
					<?php echo Form::close(); ?>

					 

					</div>
				</div>
			</section>
		</div>
		<!-- /.container-fluid -->
	</section>
</div>
<!-- /#page-wrapper -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer_scripts'); ?>
	<?php echo $__env->make('mastersettings.topics.scripts.js-scripts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>;
	<?php echo $__env->make('common.validations', array('isLoaded'=>TRUE), array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>;
<?php $__env->stopSection(); ?>
 
<?php echo $__env->make('layouts.admin.adminlayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>