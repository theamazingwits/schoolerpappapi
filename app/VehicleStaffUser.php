<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleStaffUser extends Model
{
   protected $table="vehicle_staff_user";

   protected $fillable = ['user_id','vehicle_id', 'route_id'];

    public function vehicle_name()
   {
   	  $vehicle   = Vechicle::find($this->vehicle_id);
   	  if($vehicle)
   	  	return ucwords($vehicle->number);

   	   return '-';
   }

    public function vroute_name()
   {
   	  $vroute   = Vroutes::find($this->route_id);
   	  if($vroute)
   	  	return ucwords($vroute->name);

   	   return '-';
   }

   public function seat_fee()
   {
   	  $vroute   = Vroutes::find($this->route_id);
   	  if($vroute)
   	  	return (int)$vroute->cost;

   	   return '-';
   }

   

 
}
