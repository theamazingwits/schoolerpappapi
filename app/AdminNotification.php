<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminNotification extends Model
{
    protected $table="admin_notifications";

    protected $fillable = ['title','role_id','description','academic_id','course_id','current_year','current_semister','added_by','course_parent_id','role_id'];

}
