<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleAssign extends Model
{
   protected $table    = "vechicle_assign";
   
   protected $fillable = ['vechicle_id','route_id'];
    
}
