<?php
namespace App\Http\Controllers;
use \App;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\RoomType;
use App\Hostel;
use Yajra\Datatables\Datatables;
use DB;
use Auth;
use Carbon\Carbon;


class RoomTypeController extends Controller
{
    
    public function __construct()
    {
    	$this->middleware('auth');
    }

    /**
     * Course listing method
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
      if(!checkRole(getUserGrade(21)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $data['active_class']       = 'hostel';
        $data['layout']             = getLayout();
        $data['title']              = getPhrase('room_types');
       
         return view('hostel.roomtype.list', $data);

         
    }

    /**
     * This method returns the datatables data to view
     * @return [type] [description]
     */
    public function getDatatable($slug = '')
    {

      if(!checkRole(getUserGrade(21)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $records = array();
 
        if(checkRole('owner') && Hostel::isMultiBranch() ) {
            $records = RoomType::select(['branch_id', 'id','room_type','description']);
        } else {
            // $records = RoomType::select(['id','room_type','description'])->where('branch_id', getUserRecord()->branch_id);
            $records = RoomType::select(['id','room_type','description']);
        }
            
        $records->orderBy('updated_at', 'desc');
             

        return Datatables::of($records)
        ->addColumn('action', function ($records) {
         
          $link_data = '<div class="dropdown more">
                        <a id="dLabel" type="button" class="more-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-ellipsis-v"></i>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <li><a href="'.URL_ROOM_TYPE_EDIT.$records->id.'"><i class="fa fa-pencil"></i>'.getPhrase("edit").'</a></li>';
                            
                           $temp = '';
                           if(checkRole(getUserGrade(2))) {
                    $temp .= ' <li><a href="javascript:void(0);" onclick="deleteRecord(\''.$records->id.'\');"><i class="fa fa-trash"></i>'. getPhrase("delete").'</a></li>';
                      }
                    
                    $temp .='</ul></div>';

                    $link_data .=$temp;
            return $link_data;
            })

        // ->editColumn('branch_id', function($records){
        
        //   $branch_rec = App\Branch::getBranchRecord($records->branch_id);

        //   return $branch_rec['branch_name'].' ('.$branch_rec['address'].')';
        // })
         
        ->removeColumn('id')
        ->make();
    }

    /**
     * This method loads the create view
     * @return void
     */
    public function create()
    {
      if(!checkRole(getUserGrade(21)))
      {
        prepareBlockUserMessage();
        return back();
      }
        $data['record']             = FALSE;
         if( Hostel::isMultiBranch() ){

        $data['branches']           = addSelectToList(getBranches());
        }else{

        $data['branches']           = null;
        }
        $data['active_class']       = 'hostel';
        $data['layout']             = getLayout();
        $data['title']              = getPhrase('create_room_type');

         return view('hostel.roomtype.add-edit', $data);

       
    }

    /**
     * This method loads the edit view based on unique slug provided by user
     * @param  [string] $slug [unique slug of the record]
     * @return [view with record]       
     */
    public function edit($id)
    {
      if(!checkRole(getUserGrade(21)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $record = RoomType::findOrFail($id);
        if($isValid = $this->isValidRecord($record))
            return redirect($isValid);

        $data['record']              = $record;
         if( Hostel::isMultiBranch() ){

        $data['branches']           = addSelectToList(getBranches());
        }else{

        $data['branches']           = null;
        }
        $data['active_class']        = 'hostel';
      
        $data['layout']             = getLayout();
        $data['title']              = getPhrase('edit_room_type');

           return view('hostel.roomtype.add-edit', $data);

        
    }

    /**
     * Update record based on slug and reuqest
     * @param  Request $request [Request Object]
     * @param  [type]  $slug    [Unique Slug]
     * @return void
     */
    public function update(Request $request, $id)
    {
      if(!checkRole(getUserGrade(21)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $record = RoomType::findOrFail($id);
        $record->update($request->all());
       
        flash('success','room_type_updated_successfully', 'success');
        return redirect(URL_ROOM_TYPE);
    }

    /**
     * This method adds record to DB
     * @param  Request $request [Request Object]
     * @return void
     */
    public function store(Request $request)
    {
      if(!checkRole(getUserGrade(21)))
      {
        prepareBlockUserMessage();
        return back();
      }

        RoomType::create($request->all());
        
        flash('success','room_type_added_successfully', 'success');
        return redirect(URL_ROOM_TYPE);
    }
 
    /**
     * Delete Record based on the provided slug
     * @param  [string] $slug [unique slug]
     * @return Boolean 
     */
    public function delete($id)
    {
      if(!checkRole(getUserGrade(21)))
      {
        prepareBlockUserMessage();
        return back();
      }
      /**
       * Delete the questions associated with this quiz first
       * Delete the quiz
       * @var [type]
       */
        $record = RoomType::where('id', $id)->first();
        try{
            if(!env('DEMO_MODE')) {
                $record->delete();
            }
            $response['status'] = 1;
            $response['message'] = getPhrase('record_deleted_successfully');
        }
         catch ( Exception $e) {
                 $response['status'] = 0;
           if(getSetting('show_foreign_key_constraint','module'))
            $response['message'] =  $e->getMessage();
           else
            $response['message'] =  getPhrase('this_record_is_in_use_in_other_modules');
       }
        return json_encode($response);
    }

    public function isValidRecord($record)
    {
        if ($record === null) {

            flash('Ooops...!', getPhrase("page_not_found"), 'error');
            return $this->getRedirectUrl();
        }

        return FALSE;
    }

    public function getReturnUrl()
    {
        return URL_ROOM_TYPE;
    }
}