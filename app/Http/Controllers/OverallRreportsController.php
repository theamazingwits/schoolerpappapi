<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Yajra\Datatables\Datatables;
use App;
use Auth;
use App\User;
use App\FeePayment;
use App\Expense;
use SMS;
use DB;
use Charts;
use Artisan;

class OverallRreportsController extends Controller
{
     public function __construct()
    {
      $this->middleware('auth');
    }

     public function index(Request $request)
    {
 
		
        $data['active_class']   = 'expenses';
        // $data['branches']       = addSelectToList(getBranches());
        $data['layout']         = getLayout();
        $data['title']          = getPhrase('overall_reports');
        $view_page = 'expenses.overallreports';

        $overall_payments = FeePayment::where('payment_status', 1);
        $overall_expenses = Expense::where('id', '>', 0);

        // $branch_id = (!empty($request->branch_id)) ? $request->branch_id : getUserRecord()->branch_id;

        // if($branch_id) {
        //     $overall_payments->where('branch_id', $branch_id);
        //     $overall_expenses->where('branch_id', $branch_id);
        //     $data['branch_id'] = $branch_id;
        // }

        if($request->date_from && $request->date_to) {
            $overall_payments->whereBetween('recevied_on', [$request->date_from, $request->date_to]);
            $overall_expenses->whereBetween('expense_date', [$request->date_from, $request->date_to]);
            $data['date_from']  = $request->date_from;
            $data['date_to']    = $request->date_to;
        }


       $data['overall_payments'] = $overall_payments->sum('paid_amount');
       $data['overall_expenses'] = $overall_expenses->sum('expense_amount');
       $data['profit']           = number_format(($data['overall_payments'] - $data['overall_expenses']),2);

        
         $view_name = $view_page;
        return view($view_name,$data);

        // return view($view_page, $data);
         
    }


    
}
