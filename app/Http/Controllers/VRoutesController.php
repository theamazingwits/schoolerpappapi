<?php
namespace App\Http\Controllers;
use \App;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Vroutes;
use Yajra\Datatables\Datatables;
use DB;
use Auth;
use App\LogHelper;
use Carbon\Carbon;


class VRoutesController extends Controller
{
    
    public function __construct()
    {
    	$this->middleware('auth');
    }

    /**
     * Course listing method
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
      if(!checkRole(getUserGrade(20)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $data['active_class']       = 'transport';
        $data['layout']             = getLayout();
        $data['title']              = getPhrase('vehicle_routes');
        
        return view('transport.route.list',$data);

        //    $view_name = getTheme().'::transport.route.list';
        // return view($view_name,$data);
    }

    /**
     * This method returns the datatables data to view
     * @return [type] [description]
     */
    public function getDatatable($slug = '')
    {

      if(!checkRole(getUserGrade(20)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $records = array();
 
        if(checkRole('owner') && Vroutes::isMultiBranch() ) {
            $records = Vroutes::select(['branch_id', 'id','name','cost','description']);
        } else {
            // $records = Vroutes::select(['id','name','cost','description'])
            //  ->where('branch_id', getUserRecord()->branch_id);

            $records = Vroutes::select(['id','name',])
                                ->where('parent_id',0);
        }
            
        $records->orderBy('updated_at', 'desc');
             

        return Datatables::of($records)

         ->addColumn('routes', function ($records) {
         
                   $vroutes = Vroutes::where('parent_id',$records->id)
                                     ->select(['name','cost'])
                                     ->orderBy('sort_order')
                                     ->get();
              
              $vdata = '';
              if($vroutes->count() > 0){
                 
                 foreach ($vroutes as $vroute) {
                
                  // $vdata .= '<p><b>'.ucwords($vroute->name).' ( '.getCurrencyCode().' '.$vroute->cost.' )'.'</b></p>';
                  $vdata .= '<span class="badge badge-default">'.ucwords($vroute->name).' ( '.getCurrencyCode().' '.$vroute->cost.' )'.'</span>&nbsp;';

                }
              }else{

                return '-';
              } 

              return $vdata;       
            })

        ->addColumn('action', function ($records) {
         
          $link_data = '<div class="dropdown more">
                        <a id="dLabel" type="button" class="more-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-ellipsis-v"></i>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <li><a href="'.URL_VECHICLE_ROUTE_EDIT.$records->id.'"><i class="fa fa-pencil"></i>'.getPhrase("edit").'</a></li>';
                            
                           $temp = '';
                           if(checkRole(getUserGrade(2))) {
                    $temp .= ' <li><a href="javascript:void(0);" onclick="deleteRecord(\''.$records->id.'\');"><i class="fa fa-trash"></i>'. getPhrase("delete").'</a></li>';
                      }
                    
                    $temp .='</ul></div>';

                    $link_data .=$temp;
            return $link_data;
            })

        // ->editColumn('branch_id', function($records){
        
        //   $branch_rec = App\Branch::getBranchRecord($records->branch_id);

        //   return $branch_rec['branch_name'].' ('.$branch_rec['address'].')';
        // })

        //  ->editColumn('cost', function($records){
        
        //    return getCurrencyCode().' '.$records->cost;
        // })

          ->editColumn('name', function($records){
        
           return ucwords($records->name);
        })
         
        ->removeColumn('id')
        ->make();
    }

    /**
     * This method loads the create view
     * @return void
     */
    public function create()
    {
      if(!checkRole(getUserGrade(20)))
      {
        prepareBlockUserMessage();
        return back();
      }
        $data['record']             = FALSE;
         if( Vroutes::isMultiBranch() ){

        $data['branches']           = addSelectToList(getBranches());
        }else{

        $data['branches']           = null;
        }
        $data['active_class']       = 'transport';
        $data['layout']             = getLayout();
        $data['title']              = getPhrase('create_vehicle_route');

        return view('transport.route.add-edit',$data);


        //  $view_name = getTheme().'::transport.route.add-edit';
        // return view($view_name,$data);
    }

    /**
     * This method loads the edit view based on unique slug provided by user
     * @param  [string] $slug [unique slug of the record]
     * @return [view with record]       
     */
    public function edit($id)
    {
      if(!checkRole(getUserGrade(20)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $record = Vroutes::findOrFail($id);
        if($isValid = $this->isValidRecord($record))
            return redirect($isValid);

        $data['record']              = $record;
         if( Vroutes::isMultiBranch() ){

        $data['branches']           = addSelectToList(getBranches());
        }else{

        $data['branches']           = null;
        }
        $data['active_class']        = 'transport';
      
        $data['layout']             = getLayout();
        $data['title']              = getPhrase('edit_vehicle_route');

        // dd($record);

          return view('transport.route.add-edit',$data);

        //  $view_name = getTheme().'::transport.route.add-edit';
        // return view($view_name,$data);
    }

    /**
     * Update record based on slug and reuqest
     * @param  Request $request [Request Object]
     * @param  [type]  $slug    [Unique Slug]
     * @return void
     */
    public function update(Request $request, $id)
    {
      if(!checkRole(getUserGrade(20)))
      {
        prepareBlockUserMessage();
        return back();
      }

        if(!$request->has('route_names')){

         flash('Oops..!','please_add_the_route_datails','error');
         return back();
      }
      
      // dd($request);
        $name         = $request->name;
        $description  = $request->description;
        $route_names  = $request->route_names;
        $route_costs  = $request->route_costs;
        $sort_order   = $request->sort_order;

        $all_ids  = array_keys($route_names);
        
        $record               = Vroutes::findOrFail($id);
        $record->name         = $name;
        $record->description  = $description;
        $record->save();

        $record->flag        = 'Update';
        $record->action      = 'Vroutes';
        $record->object_id   =  $id;
        $logs = new LogHelper();
        $logs->storeLogs($record);

        $exist_ids    = Vroutes::where('parent_id',$record->id)->pluck('id')->toArray();
        $removed_ids  = array_diff($exist_ids, $all_ids);
             
         Vroutes::whereIn('id',$removed_ids)
                      ->delete(); 
            

        foreach ($route_names as $key => $value) {
            
            $via_route                = Vroutes::where('id',$key)
                                                ->where('parent_id',$record->id)
                                                ->first();
            if($via_route){

              $via_route->name         = $value;
              $via_route->cost         = $route_costs[$key];
              $via_route->parent_id    = $record->id;
              $via_route->sort_order   = $sort_order[$key];
              $via_route->description  = $record->description;
              $via_route->save();  

              $via_route->flag        = 'Insert';
              $via_route->action      = 'Vroutes';
              $via_route->object_id   =  $key;
              $logs = new LogHelper();
              $logs->storeLogs($via_route);

            }else{//add new routes

                $via_route               = new Vroutes();
                $via_route->name         = $value;
                $via_route->cost         = $route_costs[$key];
                $via_route->parent_id    = $record->id;
                $via_route->sort_order   = $sort_order[$key];
                $via_route->description  = $record->description;
                $via_route->save();

                $via_route->flag        = 'Insert';
                $via_route->action      = 'Vroutes';
                $via_route->object_id   =  $key;
                $logs = new LogHelper();
                $logs->storeLogs($via_route);
            }


        }

          
       
        
        // $record->update($request->all());
       
        flash('success','route_updated_successfully', 'success');
        return redirect(URL_VECHICLE_ROUTE);
    }

    /**
     * This method adds record to DB
     * @param  Request $request [Request Object]
     * @return void
     */
    

    public function store(Request $request)
    {

      // dd($request);

      if(!checkRole(getUserGrade(20)))
      {
        prepareBlockUserMessage();
        return back();
      }

      if(!$request->has('route_names')){

         flash('Oops..!','please_add_the_route_datails','error');
         return back();
      }


        $name         = $request->name;
        $description  = $request->description;
        $route_names  = $request->route_names;
        $route_costs  = $request->route_costs;
        $sort_order   = $request->sort_order;
        $gps_lat = $request->gps_lat;
        $gps_lng = $request->gps_lng;

        $parent_route               = new Vroutes();
        $parent_route->name         = $name;
        $parent_route->description  = $description;
        $parent_route->save();

        $parent_route->flag        = 'Insert';
        $parent_route->action      = 'Vroutes';
        $parent_route->object_id   =  $parent_route->id;
        $logs = new LogHelper();
        $logs->storeLogs($parent_route);

        foreach ($route_names as $key => $value) {
            
            $via_route               = new Vroutes();
            $via_route->name         = $value;
            $via_route->cost         = $route_costs[$key];
            $via_route->parent_id    = $parent_route->id;
            $via_route->sort_order   = $sort_order[$key];
            $via_route->description  = $parent_route->description;
            $via_route->gps_lat   = $gps_lat[$key];
            $via_route->gps_lng   = $gps_lng[$key];
            $via_route->save();

            $via_route->flag        = 'Insert';
            $via_route->action      = 'Vroutes';
            $via_route->object_id   =  $parent_route->id;
            $logs = new LogHelper();
            $logs->storeLogs($via_route);
        }

        // Vroutes::create($request->all());
        
        flash('success','routes_added_successfully', 'success');
        return redirect(URL_VECHICLE_ROUTE);
    }
 
    /**
     * Delete Record based on the provided slug
     * @param  [string] $slug [unique slug]
     * @return Boolean 
     */
    public function delete($id)
    {
      if(!checkRole(getUserGrade(20)))
      {
        prepareBlockUserMessage();
        return back();
      }
      /**
       * Delete the questions associated with this quiz first
       * Delete the quiz
       * @var [type]
       */
        $record = Vroutes::where('id', $id)->first();

        try{
            if(!env('DEMO_MODE')) {
        Vroutes::where('parent_id', $record->id)->delete();
                $record->delete();
            }

            $via_route->flag        = 'Delete';
            $via_route->action      = 'Vroutes';
            $via_route->object_id   =  $id;
            $logs = new LogHelper();
            $logs->storeLogs($via_route);

            $response['status'] = 1;
            $response['message'] = getPhrase('record_deleted_successfully');
        }
         catch ( Exception $e) {
                 $response['status'] = 0;
           if(getSetting('show_foreign_key_constraint','module'))
            $response['message'] =  $e->getMessage();
           else
            $response['message'] =  getPhrase('this_record_is_in_use_in_other_modules');
       }
        return json_encode($response);
    }

    public function isValidRecord($record)
    {
        if ($record === null) {

            flash('Ooops...!', getPhrase("page_not_found"), 'error');
            return $this->getRedirectUrl();
        }

        return FALSE;
    }

    public function getReturnUrl()
    {
        return URL_VECHICLE_ROUTE;
    }


    public function viaRoutes(Request $request)
    {
       $parent_route_id  = $request->vroute_id;

         $vroutes = Vroutes::where('parent_id',$parent_route_id)
                                     // ->select(['name','cost'])
                                     ->orderBy('sort_order')
                                     ->get();

        return json_encode(array('all_routes'=>$vroutes));                              

    }
}