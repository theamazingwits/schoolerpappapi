<?php
namespace App\Http\Controllers;
use \App;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\AssetVendor;
use App\LogHelper;
use Yajra\Datatables\Datatables;
use DB;
use Auth;
use Carbon\Carbon;


class AssetVendorController extends Controller
{
    
    public function __construct()
    {
    	$this->middleware('auth');
    }

    /**
     * Course listing method
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
      if(!checkRole(getUserGrade(17)))
      {
        prepareBlockUserMessage();
        return back();
      }
        $data['active_class']       = 'assets';
        $data['layout']             = getLayout();
        $data['title']              = getPhrase('vendors');

        return view('assets.vendors.list', $data);

          
    }

    /**
     * This method returns the datatables data to view
     * @return [type] [description]
     */
    public function getDatatable($slug = '')
    {

      if(!checkRole(getUserGrade(17)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $records = array();
 
      
        $records = AssetVendor::select(['name','phone','address','supply','slug']);
    
            
        $records->orderBy('updated_at', 'desc');
             

        return Datatables::of($records)
        ->addColumn('action', function ($records) {
         
          $link_data = '<div class="dropdown more">
                        <a id="dLabel" type="button" class="more-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-ellipsis-v"></i>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <li><a href="'.URL_ASSET_VENDOR_EDIT.$records->slug.'"><i class="fa fa-pencil"></i>'.getPhrase("edit").'</a></li>';
                            
                           $temp = '';
                           if(checkRole(getUserGrade(2))) {
                    $temp .= ' <li><a href="javascript:void(0);" onclick="deleteRecord(\''.$records->slug.'\');"><i class="fa fa-trash"></i>'. getPhrase("delete").'</a></li>';
                      }
                    
                    $temp .='</ul></div>';

                    $link_data .=$temp;
            return $link_data;
            })

        ->removeColumn('slug')
        ->make();
    }

    /**
     * This method loads the create view
     * @return void
     */
    public function create()
    {
      if(!checkRole(getUserGrade(17)))
      {
        prepareBlockUserMessage();
        return back();
      }
        $data['record']             = FALSE;
       
        $data['active_class']       = 'assets';
        $data['layout']             = getLayout();
        $data['title']              = getPhrase('add_vendor');

         return view('assets.vendors.add-edit', $data);

       
    }

    /**
     * This method loads the edit view based on unique slug provided by user
     * @param  [string] $slug [unique slug of the record]
     * @return [view with record]       
     */
    public function edit($slug)
    {
      if(!checkRole(getUserGrade(17)))
      {
        prepareBlockUserMessage();
        return back();
      }
    
    // dd($slug);
        $record = AssetVendor::where('slug',$slug)->first();

        if($isValid = $this->isValidRecord($record))
            return redirect($isValid);

        $data['record']              = $record;
        $data['active_class']       = 'assets';
        $data['layout']             = getLayout();
        $data['title']              = getPhrase('edit_vendor');

         return view('assets.vendors.add-edit', $data);

        
    }

    /**
     * Update record based on slug and reuqest
     * @param  Request $request [Request Object]
     * @param  [type]  $slug    [Unique Slug]
     * @return void
     */
    public function update(Request $request, $slug)
    {
      if(!checkRole(getUserGrade(17)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $record = AssetVendor::where('slug',$slug)->first();

        if($isValid = $this->isValidRecord($record))
            return redirect($isValid);

            $name = $request->name;
        if($name != $record->name)
            $record->slug = $record->makeSlug($name);

        $record->update($request->all());


        $record->flag        = 'Update';
        $record->action      = 'Asset_vendors';
        $record->object_id   =  $record->slug;
        $logs = new LogHelper();
        $logs->storeLogs($record);

     
       
        flash('success','asset_vendor_updated_successfully', 'success');
        return redirect(URL_ASSET_VENDOR);
    }

    /**
     * This method adds record to DB
     * @param  Request $request [Request Object]
     * @return void
     */
    public function store(Request $request)
    {
      if(!checkRole(getUserGrade(17)))
      {
        prepareBlockUserMessage();
        return back();
      }
    
     // dd($request);
      $record        =   AssetVendor::create($request->all());
      $record->slug  = $record->makeSlug($request->name);
      $record->save();

      $record->flag        = 'Insert';
      $record->action      = 'Asset_vendors';
      $record->object_id   =  $record->slug;
      $logs = new LogHelper();
      $logs->storeLogs($record);
        
        flash('success','asset_vendor_added_successfully', 'success');
        return redirect(URL_ASSET_VENDOR);
    }
 
    /**
     * Delete Record based on the provided slug
     * @param  [string] $slug [unique slug]
     * @return Boolean 
     */
    public function delete($slug)
    {
      if(!checkRole(getUserGrade(17)))
      {
        prepareBlockUserMessage();
        return back();
      }
      /**
       * Delete the questions associated with this quiz first
       * Delete the quiz
       * @var [type]
       */
         $record = AssetVendor::where('slug',$slug)->first();
        try{
            if(!env('DEMO_MODE')) {
                $record->delete();
            }

            $record->flag        = 'Delete';
            $record->action      = 'Asset_vendors';
            $record->object_id   =  $slug;
            $logs = new LogHelper();
            $logs->storeLogs($record);

            $response['status'] = 1;
            $response['message'] = getPhrase('record_deleted_successfully');
        }
         catch ( Exception $e) {
                 $response['status'] = 0;
           if(getSetting('show_foreign_key_constraint','module'))
            $response['message'] =  $e->getMessage();
           else
            $response['message'] =  getPhrase('this_record_is_in_use_in_other_modules');
       }
        return json_encode($response);
    }

    public function isValidRecord($record)
    {
        if ($record === null) {

            flash('Ooops...!', getPhrase("page_not_found"), 'error');
            return $this->getRedirectUrl();
        }

        return FALSE;
    }

    public function getReturnUrl()
    {
        return URL_ASSET_VENDOR;
    }
}