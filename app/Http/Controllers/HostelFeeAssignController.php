<?php
namespace App\Http\Controllers;
use \App;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\RoomType;
use App\HostelRoom;
use App\Hostel;
use App\HostelFee;
use App\HostelFeeType;
use App\HostelUser;
use Yajra\Datatables\Datatables;
use DB;
use Auth;
use Exception;
use Carbon\Carbon;


class HostelFeeAssignController extends Controller
{
    
    public function __construct()
    {
      $this->middleware('auth');
    }

    /**
     * Course listing method
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
      
      if(!checkRole(getUserGrade(21)))
      {
        prepareBlockUserMessage();
        return back();
      }
     
     // dd('here');
        $data['active_class']  = 'hostel';
        if(Auth::user()->role_id == 13)
        $data['active_class']  = 'hostel_fee';
        $data['layout']        = getLayout();
        $data['title']         = getPhrase('assign_hostel_fee');
        $data['hostels_list']  = Hostel::pluck('name','id')->toArray();
        $data['years']         = array_combine(range(date("Y"), 2016), range(date("Y"), 2016));
        $data['months']        = HostelFeeType::getMonths();
       

        //    $view_name = getTheme().'::hostel.assignfee.list';
        // return view($view_name,$data);

        return view("hostel.assignfee.list",$data);
    }


    public function storeFee(Request $request)
    {

       $columns = array(

        'fee_type'   => 'bail|required',
        'start_date' => 'bail|required',
        'end_date'   => 'bail|required',
      );
      

        $this->validate($request,$columns);

        $hostel_id    = $request->hostel_id;
        $year         = $request->year;
        $month        = $request->month;

        $pre_data   = HostelFee::where('hostel_id', $hostel_id)
                                ->where('year', $year)
                                ->where('month', $month)
                                ->get()->count();

        if($pre_data > 0){

           flash('Oops..!','fee_is_already_assigned_for_selected_details','overlay');
           return back();
        }                         

        
        // dd($request);
        DB::beginTransaction();

        try{
              
              $fee_type             = new HostelFeeType();
              $fee_type->hostel_id  = $request->hostel_id;
              $fee_type->type       = $request->fee_type;
              $fee_type->save();

              $hostel_users   = HostelUser::where('hostel_id',$hostel_id)
                                            ->where('is_active',1)
                                            ->get();
              // dd($hostel_users);
              
              foreach ($hostel_users as $hst_user) {
                 
                 $fee_record                  = new HostelFee();
                 $fee_record->title           = $request->title;
                 $fee_record->user_id         = $hst_user->user_id;
                 $fee_record->hostel_id       = $hst_user->hostel_id;
                 $fee_record->room_id         = $hst_user->room_id;
                 $fee_record->hostel_user_id  = $hst_user->id;
                 $fee_record->year            = $year;
                 $fee_record->month           = $month;
                 $fee_record->start_date      = $request->start_date;
                 $fee_record->end_date        = $request->end_date;
                 $fee_record->added_by        = Auth::user()->id;
                 $details                     = $hst_user->roomname();
                 $fee_record->amount          = $details['cost'];
                 $fee_record->balance         = $details['cost'];
                 $fee_record->save();

              }



              DB::commit();
              flash('success','fee_assigned_successfully', 'overlay');     
        }
        catch( Exception $e ){

            DB::rollBack();
            flash('oops...!', $e->getMessage(), 'error');
      
        }

        return redirect( URL_ASSIGN_HOSTEL_FEE );
        
    }

    public function reports()
    {
       if(!checkRole(getUserGrade(21)))
      {
        prepareBlockUserMessage();
        return back();
      }
     
     // dd('here');
        $data['active_class']  = 'hostel';
         if(Auth::user()->role_id == 13)
        $data['active_class']  = 'hostel_fee';
        $data['layout']        = getLayout();
        $data['title']         = getPhrase('hostel_fee_reports');
        $data['hostels_list']  = Hostel::join('hostel_fee_type','hostel_fee_type.hostel_id','=','hostel.id')
                                       ->groupby('hostel.id')
                                       ->orderby('hostel_fee_type.created_at','desc')
                                       ->pluck('name','hostel.id')
                                       ->toArray();

        $data['years']         = array_combine(range(date("Y"), 2016), range(date("Y"), 2016));
        $data['months']        = HostelFeeType::getMonths();
       

        //    $view_name = getTheme().'::hostel.reports.list';
        // return view($view_name,$data);

         return view("hostel.reports.list",$data);


    }

    public function getmonths(Request $request)
    {
       
       $hostel_id  = $request->hostel_id;
       $record     = HostelFeeType::where('hostel_id', $hostel_id)->orderby('created_at','desc')->first();

       if(!$record || $record->type == 1)
         return $showmonth = 1;
       
       return $showmonth = 0;

    }

    public function getFeeReports(Request $request)
    {
        // return $request;
        $hostel_id  = $request->hostel_id;
        $year       = $request->year;
        
        if($request->has('year') && $request->has('month') && $request->has('hostel_id') ){
         
         $month       = $request->month;

          $reports   = HostelFee::join('users','users.id','=','hostel_fee.user_id')
                                ->join('students', 'users.id' ,'=', 'students.user_id')
                                ->join('courses','courses.id','=','students.course_id')
                                ->join('hostel_rooms','hostel_rooms.id','=','hostel_fee.room_id')
                                ->where('hostel_fee.hostel_id',$hostel_id)
                                ->where('hostel_fee.year',$year)
                                ->where('hostel_fee.month',$month)
                                ->select(['users.name','users.slug','roll_no','course_title','room_number','amount','paid_amount','balance','year','month','start_date','end_date'])
                                ->orderby('hostel_fee.created_at','desc')
                                ->get();

        }elseif ( $request->has('year') && $request->has('hostel_id') ) {
          
            $reports   = HostelFee::join('users','users.id','=','hostel_fee.user_id')
                                  ->join('students', 'users.id' ,'=', 'students.user_id')
                                  ->join('courses','courses.id','=','students.course_id')
                                  ->join('hostel_rooms','hostel_rooms.id','=','hostel_fee.room_id')
                                  ->where('hostel_fee.hostel_id',$hostel_id)
                                  ->where('hostel_fee.year',$year)
                                  ->select(['users.name','users.slug','roll_no','course_title','room_number','amount','paid_amount','balance','year','month','start_date','end_date'])
                                   ->orderby('hostel_fee.created_at','desc')
                                  ->get();
                // return $reports;                  
        }

        elseif ( $request->has('start_date') && $request->has('end_date') ) {

            $start_date  = $request->start_date;
            $end_date    = $request->end_date;

            if( $request->has('year') && $request->has('hostel_id') ){

                 $reports   = HostelFee::join('users','users.id','=','hostel_fee.user_id')
                                ->join('students', 'users.id' ,'=', 'students.user_id')
                                ->join('courses','courses.id','=','students.course_id')
                                ->join('hostel_rooms','hostel_rooms.id','=','hostel_fee.room_id')
                                ->where('hostel_fee.start_date','>=',$start_date)
                                ->where('hostel_fee.end_date','<=',$end_date)
                                ->where('hostel_fee.hostel_id',$hostel_id)
                                ->where('hostel_fee.year',$year)
                                ->select(['users.name','users.slug','roll_no','course_title','room_number','amount','paid_amount','balance','year','month','start_date','end_date'])
                                 ->orderby('hostel_fee.created_at','desc')
                                ->get();
            }
            else{

                   $reports   = HostelFee::join('users','users.id','=','hostel_fee.user_id')
                                ->join('students', 'users.id' ,'=', 'students.user_id')
                                ->join('courses','courses.id','=','students.course_id')
                                ->join('hostel_rooms','hostel_rooms.id','=','hostel_fee.room_id')
                                ->where('hostel_fee.start_date','>=',$start_date)
                                ->where('hostel_fee.end_date','<=',$end_date)
                                ->select(['users.name','users.slug','roll_no','course_title','room_number','amount','paid_amount','balance','year','month','start_date','end_date'])
                                 ->orderby('hostel_fee.created_at','desc')
                                ->get();
            }
          
           
        }
        

        return json_encode(array('users'=>$reports));                        

    }

    public function printFeeReports(Request $request)
    {
       

        $hostel_id  = $request->hostel_id;
        $year       = $request->year;
        
        if($request->year && $request->month && $request->hostel_id ){
         
         $month       = $request->month;

          $reports   = HostelFee::join('users','users.id','=','hostel_fee.user_id')
                                ->join('students', 'users.id' ,'=', 'students.user_id')
                                ->join('courses','courses.id','=','students.course_id')
                                ->join('hostel_rooms','hostel_rooms.id','=','hostel_fee.room_id')
                                ->where('hostel_fee.hostel_id',$hostel_id)
                                ->where('hostel_fee.year',$year)
                                ->where('hostel_fee.month',$month)
                                ->select(['users.name','users.slug','roll_no','course_title','room_number','amount','paid_amount','balance','year','month','start_date','end_date'])
                                 ->orderby('hostel_fee.created_at','desc')
                                ->get();

        }elseif ( $request->year && $request->hostel_id ) {
          
            $reports   = HostelFee::join('users','users.id','=','hostel_fee.user_id')
                                ->join('students', 'users.id' ,'=', 'students.user_id')
                                ->join('courses','courses.id','=','students.course_id')
                                ->join('hostel_rooms','hostel_rooms.id','=','hostel_fee.room_id')
                                ->where('hostel_fee.hostel_id',$hostel_id)
                                ->where('hostel_fee.year',$year)
                                ->select(['users.name','users.slug','roll_no','course_title','room_number','amount','paid_amount','balance','year','month','start_date','end_date'])
                                 ->orderby('hostel_fee.created_at','desc')
                                ->get();
        }

        elseif ( $request->start_date && $request->end_date ) {

            $start_date  = $request->start_date;
            $end_date    = $request->end_date;

            

                   $reports   = HostelFee::join('users','users.id','=','hostel_fee.user_id')
                                ->join('students', 'users.id' ,'=', 'students.user_id')
                                ->join('courses','courses.id','=','students.course_id')
                                ->join('hostel_rooms','hostel_rooms.id','=','hostel_fee.room_id')
                                ->where('hostel_fee.start_date','>=',$start_date)
                                ->where('hostel_fee.end_date','<=',$end_date)
                                ->select(['users.name','users.slug','roll_no','course_title','room_number','amount','paid_amount','balance','year','month','start_date','end_date'])
                                 ->orderby('hostel_fee.created_at','desc')
                                ->get();
          
          
           
        }

        // dd($reports);
          $hostel           = Hostel::find($hostel_id);
          $data['reports']  = $reports;
          $data['title']    = '';
          if($hostel){
            
          $data['title']    = ucwords($hostel->name);
          }
          
          // $view_name = getTheme().'::hostel.reports.print-students';

          $view = "hostel.reports.print-students";
          $view = \View::make($view, $data);

     
        $html_data = ($view);

        echo $html_data;

        die();

    }


    public function payHostelFee()
    {

        $data['active_class'] = 'hostel';
         if(Auth::user()->role_id == 13)
        $data['active_class']  = 'hostel_fee';
        $data['layout']       = getLayout();
        $data['title']        = getPhrase('pay_hostel_fee');
        $payment_ways         = array(  
                                        'cash'   =>getPhrase('cash'),
                                        'online' =>getPhrase('online'),
                                        'cheque' =>getPhrase('cheque'),
                                        'DD'     =>'DD',
                                        'other'  =>getPhrase('other_payment_way'));

        $data['payment_ways'] = $payment_ways;
        $user                 = Auth::user();
        $data['hostels_list']  = Hostel::join('hostel_fee_type','hostel_fee_type.hostel_id','=','hostel.id')
                                       ->groupby('hostel.id')
                                       ->orderby('hostel_fee_type.created_at','desc')
                                       ->pluck('name','hostel.id')
                                       ->toArray();

        $data['currency']   = getCurrencyCode();

        // $view_name = getTheme().'::hostel.payfee.select-particulars';
        // return view($view_name, $data);

         return view("hostel.payfee.select-particulars",$data);
       
    }

    public function getHostelRooms(Request $request)
    {
       // return $request;
       $hostel_id  = $request->hostel_id;
      
        $rooms      = HostelRoom::where('hostel_id',$hostel_id)
                                 ->get();
        $records    = array();
        foreach ($rooms as $rum) {
          
         $records[] = HostelRoom::join('room_types','room_types.id','=','hostel_rooms.room_type_id')
                                      ->select(DB::raw("CONCAT(room_number,' (',room_type,')') AS name"),'hostel_rooms.id')
                                      ->where('hostel_rooms.id',$rum->id)
                                      ->first(); 
           

        }
        
     return json_encode($records);
    }


    public function getRoomUsers(Request $request)
    {
       $hostel_id  = $request->hostel_id;
       $room_id    = $request->room_id;

       $users      = HostelUser::join('users','users.id','=','hostel_user_room.user_id')
                                  ->where('hostel_id',$hostel_id)
                                  ->where('room_id',$room_id)
                                  ->where('is_active',1)
                                  ->select(['users.id','users.name'])
                                  ->orderby('hostel_user_room.created_at','desc')
                                  ->get();
        
        return json_encode($users);                       

    }

    public function getStudentData(Request $request)
    {
        $hostel_id = $request->hostel_id;
        $room_id   = $request->room_id;
        $user_id   = $request->user_id;
        
        $user      = HostelUser::join('users','users.id','=','hostel_user_room.user_id')
                                  ->where('hostel_id',$hostel_id)
                                  ->where('room_id',$room_id)
                                  ->where('user_id',$user_id)
                                  ->where('is_active',1)
                                  ->first();

        $query          = HostelFee::where('user_id',$user_id);
        $total_pay      = $query->sum('amount');
        $total_discount = $query->sum('discount');
        $total_paid     = $query->sum('paid_amount');
        $total_balance  = $query->sum('balance');
        $records        = $query->get();
        $total_amount_pay =  $total_pay - ($total_discount + $total_paid);                         

        return json_encode( array( 

            'user'=>$user,
            'records'=>$records,
            'total_pay'=>$total_pay,
            'total_discount'=>$total_discount,
            'total_paid'=>$total_paid,
            'total_balance'=>$total_balance, 
            'total_amount_pay'=>$total_amount_pay 
        ));    

    }


    public function payFee(Request $request)
    {
        // dd($request);
        $user_hostel_id = $request->hostel_id;  
        $user_room_id   = $request->room_id;  
        $user_id        = $request->user_id;  
        $pay_amount     = $request->pay_amount;  
        $comments       = $request->notes; 

        if( $pay_amount <= 0 ){

          flash('Oopss..!','enter_the_amount','overlay');
          return back();
        }


          $query          = HostelFee::where('user_id',$user_id)
                                    ->where('hostel_id',$user_hostel_id)
                                    ->where('room_id',$user_room_id)
                                    ->where('is_paid',0)
                                    ->orWhere('is_paid',2);

        $total_amount      =  (int)$query->sum('amount');
        $total_paid_amount =  (int)$query->sum('paid_amount') + (int)$query->sum('balance');
        // dd($total_paid_amount);

        if( $pay_amount > $total_amount || $pay_amount > $total_paid_amount ){

          flash('Oopss..!','entered_amount_is_higher_than_total_amount_to_pay','overlay');
          return back();
        }

                DB::beginTransaction();

        try{                             

    
        $sec_records  =  HostelFee::where('user_id',$user_id)
                                    ->where('hostel_id',$user_hostel_id)
                                    ->where('room_id',$user_room_id)
                                    ->where('is_paid',0)
                                    ->orWhere('is_paid',2)
                                    ->get();
     // dd($sec_records);
            
            $bal_dection  = (int)$pay_amount;

            foreach ( $sec_records as $sec_record ) {

                if( $bal_dection > 0 ) {
                
                   $total_paid      = $sec_record->paid_amount + $sec_record->discount;
                   $balance         = (int)$sec_record->amount - (int)$total_paid;
                   // dd($bal_dection);
                   
                   if($bal_dection >= $balance){

                     $sec_record->paid_amount = $balance + (int)$sec_record->paid_amount;
                     $final_bal    = (int)$sec_record->amount -((int)$sec_record->paid_amount+(int)$sec_record->discount);
                     $sec_record->balance  = $final_bal;
                    
                   }elseif($bal_dection < $balance){
                     
                    $sec_record->paid_amount = (int)$bal_dection + (int)$sec_record->paid_amount;
                    $final_bal    = (int)$sec_record->amount - ((int)$sec_record->paid_amount+(int)$sec_record->discount);
                    $sec_record->balance  = $final_bal;

                   }


                    if($final_bal  == 0){

                     $sec_record->is_paid  = 1;
                    }else{

                     $sec_record->is_paid  = 2;
                    }

                  $sec_record->comments     = $comments;
                  $sec_record->payment_mode = $request->payment_mode;
                  $sec_record->paid_date    = date('Y-m-d');
                  $sec_record->save();
                }

// dd($sec_record->paid_amount); 
                  $bal_dection -=  (int)$sec_record->paid_amount;
                  // dd($bal_dection);

                
              }
         // }

         DB::commit();
         flash('success','fee_paid_successfully','success');

      }catch(Exception $e){

          // dd($e->getMenssage());
          DB::rollBack();
          flash('Oops..!','something_went_wrong_please_try_again','error');
      }                            
                                
        return back();


    }


     public function discount( Request $request )
     {


        // dd($request);
        $user_id        = $request->userid;  
        $user_hostel_id = $request->user_hostel_id;  
        $user_room_id   = $request->user_room_id;  
        $comments       = $request->comments; 
        $user_discount  = $request->user_discount; 

        if( $user_discount <= 0 ){

          flash('Oopss..!','enter_the_discount_amount','overlay');
          return back();
        }  

        $query          = HostelFee::where('user_id',$user_id)
                                    ->where('hostel_id',$user_hostel_id)
                                    ->where('room_id',$user_room_id)
                                    ->where('is_paid',0)
                                    ->orWhere('is_paid',2);

        $total_amount      =  (int)$query->sum('amount');
        $total_paid_amount =  (int)$query->sum('paid_amount') + (int)$query->sum('balance');
        // dd($total_paid_amount);

        if( $user_discount > $total_amount || $user_discount > $total_paid_amount ){

          flash('Oopss..!','entered_discount_amount_is_higher_than_total_amount_to_pay','overlay');
          return back();
        }

        DB::beginTransaction();

        try{                             

    
        $sec_records  =  HostelFee::where('user_id',$user_id)
                                    ->where('hostel_id',$user_hostel_id)
                                    ->where('room_id',$user_room_id)
                                    ->where('is_paid',0)
                                    ->orWhere('is_paid',2)
                                    ->get();
     // dd($sec_records);
            
            $bal_dection  = (int)$user_discount;

            foreach ( $sec_records as $sec_record ) {

                if( $bal_dection > 0 ) {
                
                   $total_paid      = $sec_record->paid_amount + $sec_record->discount;
                   $balance         = (int)$sec_record->amount - (int)$total_paid;
                   // dd($bal_dection);
                   
                   if($bal_dection >= $balance){

                     $sec_record->discount = $balance + (int)$sec_record->discount;
                     $final_bal            = (int)$sec_record->amount -((int)$sec_record->discount+(int)$sec_record->paid_amount);
                     $sec_record->balance  = $final_bal;
                    
                   }elseif($bal_dection < $balance){
                     
                    $sec_record->discount = (int)$bal_dection + (int)$sec_record->discount;
                    $final_bal            = (int)$sec_record->amount - ((int)$sec_record->discount+(int)$sec_record->paid_amount);
                    $sec_record->balance  = $final_bal;

                   }

                   $sec_record->comments = $comments;

                    if($final_bal  == 0){

                     $sec_record->is_paid  = 1;
                    }else{

                     $sec_record->is_paid  = 2;
                    }


                  $sec_record->save();
                }

// dd($sec_record->discount);
                  $bal_dection -=  (int)$sec_record->discount;
                  // dd($bal_dection);

                
              }
         // }

         DB::commit();
         flash('success','discount_added_successfully','success');

      }catch(Exception $e){

          // dd($e->getMenssage());
          DB::rollBack();
          flash('Oops..!','something_went_wrong_please_try_again','error');
      }                            
                                
        return back();
     } 


}