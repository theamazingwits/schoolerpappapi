<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use App\Http\Requests;
use App\User;
use App\AlumniGallery;
use Yajra\Datatables\Datatables;
use DB;
use Image;
use File;
use Auth;

class AlumniGalleryController extends Controller
{
    
    public function __construct()
    {
    	$this->middleware('auth');
    }

    /**
     * Course listing method
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        // dd('herere');
      if(!checkRole(getUserGrade(23)))
      {
        prepareBlockUserMessage();
        return back();
      }
        $data['active_class']       = 'alumni';
        $data['layout']             = getLayout();
        $data['title']              = getPhrase('gallery');

        return view('alumini.gallery.list', $data);

          
    }

    /**
     * This method returns the datatables data to view
     * @return [type] [description]
     */
    public function getDatatable($slug = '')
    {

      if(!checkRole(getUserGrade(23)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $records = array();
 
      
        $records = AlumniGallery::select(['title','gallery_id','id'])
                                ->where('gallery_id',0);
    
            
        $records->orderBy('updated_at', 'desc');
             

        return Datatables::of($records)
        ->addColumn('action', function ($records) {
         
          $link_data = '<div class="dropdown more">
                        <a id="dLabel" type="button" class="more-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-ellipsis-v"></i>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <li><a href="'.URL_ALUMNI_GALLERY_EDIT.$records->id.'"><i class="fa fa-pencil"></i>'.getPhrase("edit").'</a></li>
                            <li><a href="'.URL_ALUMNI_GALLERY_VIEW.$records->id.'"><i class="fa fa-eye"></i>'.getPhrase("view").'</a></li>';
                            
                           $temp = '';
                           if(checkRole(getUserGrade(23))) {
                    $temp .= ' <li><a href="javascript:void(0);" onclick="deleteRecord(\''.$records->id.'\');"><i class="fa fa-trash"></i>'. getPhrase("delete").'</a></li>';
                      }
                    
                    $temp .='</ul></div>';

                    $link_data .=$temp;
            return $link_data;
            })

        ->editColumn('gallery_id',function($records){

            $total  = AlumniGallery::where('gallery_id', $records->id)->get()->count();
            return $total;
        })

        ->removeColumn('id')
        ->make();
    }

    /**
     * This method loads the create view
     * @return void
     */
    public function create()
    {
      if(!checkRole(getUserGrade(23)))
      {
        prepareBlockUserMessage();
        return back();
      }
        $data['record']       = FALSE;
        $data['active_class'] = 'alumni';
        $data['layout']       = getLayout();
        $data['title']        = getPhrase('add_gallery');
       
        return view('alumini.gallery.add-edit', $data);

       
    }

    /**
     * This method loads the edit view based on unique slug provided by user
     * @param  [string] $slug [unique slug of the record]
     * @return [view with record]       
     */
    public function edit($id)
    {
      if(!checkRole(getUserGrade(23)))
      {
        prepareBlockUserMessage();
        return back();
      }
    
    // dd($id);
        $record = AlumniGallery::where('id',$id)->first();

        if($isValid = $this->isValidRecord($record))
            return redirect($isValid);

        $data['record']       = $record;
        $data['active_class'] = 'alumni';
        $data['layout']       = getLayout();
        $data['title']        = getPhrase('edit_gallery');

         return view('alumini.gallery.add-edit', $data);

        
    }

    
    /**
     * [update description]
     * @param  Request $request [description]
     * @param  [type]  $slug    [description]
     * @return [type]           [description]
     */
    public function update(Request $request, $id)
    {
        // dd($request);
      if(!checkRole(getUserGrade(23)))
      {
        prepareBlockUserMessage();
        return back();
      }

         $rules = [
              
              'title'      => 'bail|required',
              'date' => 'bail|required',
              
            ];

       
        $this->validate($request, $rules);

        $record = AlumniGallery::where('id',$id)->first();

        if($isValid = $this->isValidRecord($record))
            return redirect($isValid);


              $record->title      =  $request->title;
              $record->date       =  $request->date;
              $record->added_by   =  $request->added_by;
              $record->save();

           if($request->certificate_files){

              foreach ($request->certificate_files as $key => $user_document) 
              {

                   $columns = array(
                
                   'user_document'    => 'bail|mimes:png,jpg,jpeg,PNG,JPG,JPEG,pdf,PDF|max:2048',
                );
               
                $this->validate($request,$columns);

                  $sub_record             =  new AlumniGallery();
                  $sub_record->title      =  $request->title;
                  $sub_record->date       =  $request->date;
                  $sub_record->added_by   =  $request->added_by;
                  $sub_record->gallery_id =  $record->id;
                  $sub_record->save();

                   $destinationPath = "public/uploads/alumni/gallery/";

                  $fileName        = $sub_record->id.'gallery'.'._'.$key.$user_document->getClientOriginalName();

                   $user_document->move($destinationPath, $fileName);
                  $sub_record->image      =  $fileName;
                  $sub_record->save();

            }
            
          }

        flash('success','gallery_updated_successfully', 'success');

        return redirect(URL_ALUMNI_GALLERY);
    }

    /**
     * This method adds record to DB
     * @param  Request $request [Request Object]
     * @return void
     */
    public function store(Request $request)
    {
      if(!checkRole(getUserGrade(23)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $rules = [
              
              'title'      => 'bail|required',
              'date' => 'bail|required',
             
             
            ];

      
            $this->validate($request, $rules);

             // dd($request);

              $record             = new AlumniGallery();
              $record->title      =  $request->title;
              $record->date       =  $request->date;
              $record->added_by   =  $request->added_by;
              $record->gallery_id =  0;
              $record->save();
           
             if($request->certificate_files){

              foreach ($request->certificate_files as $key => $user_document) 
              {

                   $columns = array(
                
                   'user_document'    => 'bail|mimes:png,jpg,jpeg,PNG,JPG,JPEG,pdf,PDF|max:2048',
                );
               
                $this->validate($request,$columns);

                $destinationPath = "public/uploads/alumni/gallery/";

                $fileName        = $record->id.'gallery'.'._'.$key.$user_document->getClientOriginalName();

                $user_document->move($destinationPath, $fileName);


                  $sub_record             = new AlumniGallery();
                  $sub_record->title      =  $request->title;
                  $sub_record->date       =  $request->date;
                  $sub_record->added_by   =  $request->added_by;
                  $sub_record->gallery_id =  $record->id;
                  $sub_record->image      =  $fileName;
                  $sub_record->save();

            }

          }
              
         flash('success','gallery_added_successfully', 'success');

         return redirect(URL_ALUMNI_GALLERY);
    }

     
 
    /**
     * Delete Record based on the provided slug
     * @param  [string] $slug [unique slug]
     * @return Boolean 
     */
    public function delete($slug)
    {
      if(!checkRole(getUserGrade(23)))
      {
        prepareBlockUserMessage();
        return back();
      }
      /**
       * Delete the questions associated with this quiz first
       * Delete the quiz
       * @var [type]
       */
         $record = AlumniGallery::where('id',$slug)->first();
        try{
            if(!env('DEMO_MODE')) {

              AlumniGallery::where('gallery_id',$record->id)->delete();
                $record->delete();
            }
            $response['status'] = 1;
            $response['message'] = getPhrase('record_deleted_successfully');
        }
         catch ( Exception $e) {
                 $response['status'] = 0;
           if(getSetting('show_foreign_key_constraint','module'))
            $response['message'] =  $e->getMessage();
           else
            $response['message'] =  getPhrase('this_record_is_in_use_in_other_modules');
       }
        return json_encode($response);
    }

    public function isValidRecord($record)
    {
        if ($record === null) {

            flash('Ooops...!', getPhrase("page_not_found"), 'error');
            return $this->getRedirectUrl();
        }

        return FALSE;
    }

    public function getReturnUrl()
    {
        return URL_ALUMNI_GALLERY;
    }


    public function viewPhotos($id)
    {
        
         if(!checkRole(getUserGrade(23)))
      {
        prepareBlockUserMessage();
        return back();
      }
        $data['active_class'] = 'alumni';
        $data['layout']       = getLayout();
        $record               = AlumniGallery::find($id); 
        $data['sub_records']  = AlumniGallery::where('gallery_id',$id)->get(); 
        $data['title']        = ucwords($record->title).' - '.getPhrase('gallery_images');
        $data['record']       = $record;


        return view('alumini.gallery.view-photos', $data);

    }

    public function deleteGalImage($id)
    {   
      // dd($id);
        $record  = AlumniGallery::find($id);
        // dd($record);
        AlumniGallery::where('id',$id)->delete();

        flash('success','image_deleted_successfully','success');

        return redirect(URL_ALUMNI_GALLERY_VIEW.$record->gallery_id); 

    }

    


  

}