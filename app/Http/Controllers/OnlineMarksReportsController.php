<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App;
use App\Http\Requests;
use Yajra\Datatables\Datatables;
use DB;
use App\Quiz;
use App\QuizResult;
use App\Student;
use App\User;
use Auth;
use Exception;

class OnlineMarksReportsController extends Controller
{
   

    public function __construct()
    {
    	$this->middleware('auth');
    }

    public function dashboard()
    {
       if(!checkRole(getUserGrade(2)))
      {
        prepareBlockUserMessage();
        return back();
      }
        $data['active_class']       = 'online_marks_reports';
        $data['title']              = getPhrase('reports');
        $data['layout']             = getLayout();

      return view('onlinemarks.dashboard', $data);

       // $view_name = getTheme().'::onlinemarks.dashboard';
       //  return view($view_name,$data);


    }
     
     
    /**
     * Course listing method
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        if(!checkRole(getUserGrade(2)))
      {
        prepareBlockUserMessage();
        return back();
      }
        $data['active_class']       = 'online_marks_reports';
        $data['title']              = getPhrase('online_Marks_reports');
        // $data['branches']           = addSelectToList(getBranches());
        $data['academic_years']     = addSelectToList(getAcademicYears());
        $data['layout']             = getLayout();

    	return view('onlinemarks.selection-view', $data);

       // $view_name = getTheme().'::onlinemarks.selection-view';
       //  return view($view_name,$data);
        
    }



    /**
     * This method lists the list of online exams by the set of combination
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function onlineExamCategoryList(Request $request)
    {
        $academic_id  = $request->academic_id; 
        $course_id    = $request->course_id; 
        $course_id    = $request->course_id; 
        $year         = $request->year; 
        $semister     = $request->semister;

        $records = App\QuizCategory::join('quizzes', 'quizzes.category_id', '=', 'quizcategories.id')
                          ->join('quizapplicability', 'quizapplicability.quiz_id', '=', 'quizzes.id')
                          ->where('quizapplicability.academic_id', '=', $academic_id)
                          ->where('quizapplicability.course_id', '=', $course_id)
                          ->where('quizapplicability.year', '=', $year)
                          ->where('quizapplicability.semister', '=', $semister)
                          ->select(['quizcategories.id', 'quizcategories.category'])
                          ->groupBy('quizcategories.id')
                          ->get();
        return $records;
  }


  /**
     * Marks listing method
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function getClassMarks(Request $request)
    {

      // return $request;

    $academic_id = $request->academic_id; 
    $course_id   = $request->course_id; 
    $year        = $request->year; 
    $semister    = $request->semister;
    $online_exam_cateory_id = $request->online_exam_cateory_id;

      $course_record         = App\Course::where('id','=',$course_id)->first();
      $academic_record       = App\Academic::where('id','=',$academic_id)->first();
      $offline_quiz_category = App\QuizCategory::where('id', '=', $online_exam_cateory_id)->first();      
      $quiz_details          = App\Quiz::where('category_id','=',$online_exam_cateory_id)->get();
      $title                 = $academic_record->academic_year_title.' '.$course_record->course_title;
      if($course_record->course_dueration>1)
      {
        $title .= ' Year-'.$year;
        if($course_record->is_having_semister && $semister>0)
          $title .= ' Sem-'.$semister;
      }

      if($offline_quiz_category)
      $title .= ' '.$offline_quiz_category->category.' class marks';

       $subjects = App\QuizApplicability::join('quizzes', 'quizapplicability.quiz_id', '=', 'quizzes.id')
                          ->join('subjects', 'subjects.id', '=', 'quizzes.subject_id')
                          ->where('quizapplicability.academic_id', '=', $academic_id)
                          ->where('quizapplicability.course_id', '=', $course_id)
                          ->where('quizapplicability.year', '=', $year)
                          ->where('quizapplicability.semister', '=', $semister)
                          ->where('quizzes.category_id', '=', $online_exam_cateory_id)
                          ->select(['quizzes.id as quiz_id', 'quizzes.title','subject_id', 'subject_title','subject_code','quizzes.category_id','total_marks'])
                          ->get();
// return $quiz_details;
                          // $students  = array();
           foreach($quiz_details as $quiz_datail){
      $students = App\Student::join('users','users.id','=','students.user_id')
                              ->join('quizresults','quizresults.user_id','=', 'users.id')
                             ->where('quizresults.quiz_id', '=', $quiz_datail->id)
                             ->where('quizresults.academic_id', '=', $academic_id)
                            ->where('quizresults.course_id', '=', $course_id)
                            ->where('quizresults.year', '=', $year)
                            ->where('quizresults.semister', '=', $semister)
                            ->select(['users.id as user_id','roll_no','name','image','students.id as student_id','users.slug as user_slug'])
                            ->groupBy('quizresults.user_id')
                            ->get();
                        }
                        // return $students;
       $students_list = [];
       $final_list = [];

       foreach($students as $student)
       {

        $temp = [];
        $temp['user_id'] = $student->user_id;
        $temp['name']    = $student->name;
        $temp['roll_no'] = $student->roll_no;
        $temp['image']   = $student->image;
        $temp['slug']    = $student->user_slug;
        $subject_marks   = [];
        $average = 0;
          foreach($subjects as $subject)
          {
            $marks_records = $this->getSubjectMarks($student->user_id,$online_exam_cateory_id, $subject->subject_id);
            $subject_marks['subject_id']    = $subject->subject_id;
            $subject_marks['subject_title'] = $subject->subject_title;
            $subject_marks['subject_code']  = $subject->subject_code;
            $subject_marks ['score']        = isset($marks_records) ? $marks_records : null;
            $subject_marks ['percentage']   = isset($marks_records->percentage) ? $marks_records->percentage: 0;
            $average                        = $average+$subject_marks ['percentage'];
            $temp['marks'][]                = $subject_marks;
          }
          $temp['average']   = 0;
          $total_subjects = count($subjects);

          if($total_subjects){
           $temp['average']        = round($average/count($subjects));
           // $GPA_data               = resultGrade($temp['average']);
           // $temp['grade_points']   = $GPA_data['grade_points'];
           // $temp['grade']          = $GPA_data['grade'];
           // $temp['course_type']    = getStudentCourseType($temp['user_id']);
       }
          
          $students_list[] = $temp;

       }
      $final_list['students']     = $students_list;
      $final_list['subjects']     = $subjects;
      $final_list['course_title'] = $title;
      return $final_list;
       
    }
    /**
     * Returns the list of marks with the sent combination
     * @param  [type] $user_id                  [description]
     * @param  [type] $quiz_offline_category_id [description]
     * @param  [type] $subject_id               [description]
     * @return [type]                           [description]
     */
    public function getSubjectMarks($user_id, $quiz_offline_category_id, $subject_id)
    { 

       $result_type  =  getSetting('online_exam_results', 'exam_settings');

       if($result_type == 3){
           
             $records = App\Quiz::join('quizresults','quizzes.id','=','quizresults.quiz_id')
                          ->where('quizzes.category_id','=',$quiz_offline_category_id)
                          ->where('quizzes.subject_id','=',$subject_id)
                          ->where('quizresults.user_id','=',$user_id)
                          ->select(['user_id','quizresults.marks_obtained','subject_id', 'quizresults.total_marks','quizresults.percentage','quizresults.exam_status'])
                          ->orderby('quizresults.updated_at','desc')
                          ->first();
           

       }
       elseif ($result_type == 2) {
         
          $records = App\Quiz::join('quizresults','quizzes.id','=','quizresults.quiz_id')
                          ->where('quizzes.category_id','=',$quiz_offline_category_id)
                          ->where('quizzes.subject_id','=',$subject_id)
                          ->where('quizresults.user_id','=',$user_id)
                          ->select(['user_id','quizresults.marks_obtained','subject_id', 'quizresults.total_marks','quizresults.percentage','quizresults.exam_status'])
                          ->orderby('quizresults.updated_at','desc')
                          ->first();
       }

       elseif ($result_type == 1) {
         
          $records = App\Quiz::join('quizresults','quizzes.id','=','quizresults.quiz_id')
                          ->where('quizzes.category_id','=',$quiz_offline_category_id)
                          ->where('quizzes.subject_id','=',$subject_id)
                          ->where('quizresults.user_id','=',$user_id)
                          ->select(['user_id','quizresults.marks_obtained','subject_id', 'quizresults.total_marks','quizresults.percentage','quizresults.exam_status'])
                          ->orderby('quizresults.updated_at','asc')
                          ->first();
       }
     
      return $records;
    }


        public function printClassMarks(Request $request)
    { 

      // dd($request);
    $academic_id = $request->academic_id; 
    $course_id   = $request->course_id; 
    $year        = $request->current_year; 
    $semister    =$request->current_semister;
    $online_exam_cateory_id = $request->online_exam_cateory_id;

      $course_record         = App\Course::where('id','=',$course_id)->first();
      $academic_record       = App\Academic::where('id','=',$academic_id)->first();
      $offline_quiz_category = App\QuizCategory::where('id', '=', $online_exam_cateory_id)->first();      
      $quiz_details          = App\Quiz::where('category_id','=',$online_exam_cateory_id)->get();
      $title                 = $academic_record->academic_year_title.' '.$course_record->course_title;
      if($course_record->course_dueration>1)
      {
        $title .= ' Year-'.$year;
        if($course_record->is_having_semister && $semister>0)
          $title .= ' Sem-'.$semister;
      }
      else{
        $year  = 1;
        $semister  = 0;
      }

      if($offline_quiz_category)
      $title .= ' '.$offline_quiz_category->category.' class marks';


       $subjects = App\QuizApplicability::join('quizzes', 'quizapplicability.quiz_id', '=', 'quizzes.id')
                          ->join('subjects','subjects.id', '=', 'quizzes.subject_id')
                          ->where('quizapplicability.academic_id', '=', $academic_id)
                          ->where('quizapplicability.course_id', '=', $course_id)
                          ->where('quizapplicability.year', '=', $year)
                          ->where('quizapplicability.semister', '=', $semister)
                          ->where('quizzes.category_id', '=', $online_exam_cateory_id)
                          ->select(['quizzes.id as quiz_id', 'quizzes.title','subject_id', 'subject_title','subject_code','quizzes.category_id','total_marks'])
                          ->get();
// dd($subjects);
           foreach($quiz_details as $quiz_datail){

      $students = App\Student::join('users','users.id','=','students.user_id')
                              ->join('quizresults','quizresults.user_id','=', 'users.id')
                              ->where('quizresults.quiz_id', '=', $quiz_datail->id)
                              ->where('quizresults.academic_id', '=', $academic_id)
                              ->where('quizresults.course_id', '=', $course_id)
                              ->where('quizresults.year', '=', $year)
                              ->where('quizresults.semister', '=', $semister)
                              ->select(['users.id as user_id','roll_no','name','image','students.id as student_id','users.slug as user_slug'])
                                ->groupBy('quizresults.user_id')
                              ->get();
                        }

       $students_list = [];
       $final_list = [];

       foreach($students as $student)
       {

        $temp = [];
        $temp['user_id'] = $student->user_id;
        $temp['name']    = $student->name;
        $temp['roll_no'] = $student->roll_no;
        $temp['image']   = $student->image;
        $temp['slug']    = $student->user_slug;
        $subject_marks   = [];
        $average = 0;
          foreach($subjects as $subject)
          {
        $marks_records = $this->getSubjectMarks($student->user_id,$online_exam_cateory_id, $subject->subject_id);
            
            $subject_marks['subject_id']    = $subject->subject_id;
            $subject_marks['subject_title'] = $subject->subject_title;
            $subject_marks['subject_code']  = $subject->subject_code;
            $subject_marks ['score']        = isset($marks_records) ? $marks_records : null;
            $subject_marks ['percentage']   = isset($marks_records->percentage) ? $marks_records->percentage: 0;
            $average                        = $average+$subject_marks ['percentage'];
            $temp['marks'][]                = $subject_marks;
          }
          $temp['average']   = 0;
          $total_subjects = count($subjects);

          if($total_subjects){
           $temp['average']        = round($average/count($subjects));
           // $GPA_data               = resultGrade($temp['average']);
           // $temp['grade_points']   = $GPA_data['grade_points'];
           // $temp['grade']          = $GPA_data['grade'];
           // $temp['course_type']    = getStudentCourseType($temp['user_id']);
       }
          
          $students_list[] = $temp;

       }
      $final_list['students']     = $students_list;
      $final_list['subjects']     = $subjects;
      $final_list['course_title'] = $title;
      $data ['final_list']        = $final_list;

      if($course_record->course_dueration>1 && $course_record->is_having_semister==1){

         $data['title']     = $academic_record->academic_year_title.' '.$course_record->course_title.' '.$year.' '.'year'.' '.$semister.' '.'semester '.' '.$offline_quiz_category->title.' '.getPhrase('marks');
         }

        elseif ($course_record->course_dueration>1 && $course_record->is_having_semister==0) {

         $data['title']     = $academic_record->academic_year_title.' '.$course_record->course_title.' '.$year.' '.'year'.' '.$offline_quiz_category->title.' '.getPhrase('marks');
        }

        else{

          $data['title']     = $academic_record->academic_year_title.' '.$course_record->course_title.' '.$offline_quiz_category->title.' '.getPhrase('marks');
        }
     // dd($final_list);
        if($request->studentid==""){
          $view_name = 'onlinemarks.print-file';
        $view     = \View::make($view_name, $data);
       }

       else{
          $data['user_id']   = $request->studentid;
          $view_name = 'onlinemarks.print-student-file';
         $view     = \View::make($view_name, $data);
       }
        $contents = $view->render();

        return $contents;
      
  }


  // This method view student list as per selection wise

  public function studentList()
  {
     
      if(!checkRole(getUserGrade(2)))
      {
        prepareBlockUserMessage();
        return back();
      }
        $data['active_class']       = 'online_marks_reports';
        $data['title']              = getPhrase('student_reports');
        // $data['branches']           = addSelectToList(getBranches());
        $data['academic_years']     = addSelectToList(getAcademicYears());
        $list                       = App\Course::getCourses(0);

        
      return view('onlinemarks.consolidate_reports.student-list', $data);

      

  }

  //This method view the consolidate reports of the student 

  public function viewStudentConsolidateReport(Request $request)
  {
    // dd($request);

     if(!checkRole(getUserGrade(2)))
      {
        prepareBlockUserMessage();
        return back();
      }
    // dd($request);

    $academic_id      = $request->academic_id;
    $course_parent_id = $request->course_parent_id;
    $course_id        = $request->course_id;
    $user_id          = $request->user_id;

    $year  = 1;
    if($request->has('current_year')){
      $year   = $request->current_year;
    }

    $semister  = 0;
    if($request->has('current_semister')){
      $semister   = $request->current_semister;
    }

   

    $is_records   = QuizResult::where('user_id','=',$user_id)->get();
    if(!count($is_records)){
      
      flash('Ooops....','no_records_available','overlay');
      return back();
    }

    $academic_data = QuizResult::getStudentAcademicYears($request->user_id);
    // dd($academic_data);
    
    $course_titles = QuizResult::getCourseTitle($academic_data);
    $marks_data    = QuizResult::getConsolidateReport($academic_data, $user_id);
    // dd($marks_data);
    
    $present_academic_data = QuizResult::getPresentAcademicYear($request->user_id);
    // dd($present_course_titles);
    $present_course_title = QuizResult::getCourseTitle($present_academic_data);
    // dd($present_academic_data);
    $present_marks_data    = QuizResult::getConsolidateReport($present_academic_data, $user_id);
    // dd($present_marks_data);
    

    $student_data   = Student::where('user_id','=',$user_id)->first();
    $user_data      = User::where('id','=',$user_id)->first();
    
    $parent_course_title   = App\Course::where('id','=',$student_data->course_parent_id)->first()->course_title;
    $course_title   = App\Course::where('id','=',$student_data->course_id)->first()->course_title;
   
    $data['active_class']       = 'online_marks_reports';
    $data['student_data']       = $student_data;
    $data['user_data']          = $user_data;
    $data['title']              = getPhrase('view_reports');
    $data['marks_data']         = $marks_data;
    
    $promotions   = App\StudentPromotion::where('user_id','=',$user_id)->get();
    // dd($promotions);

   
    if(count($promotions) >= 2){
      
      $data['current_data']          = $present_marks_data;
      $data['current_course_title']  = $present_course_title;
    
     }else{

      $data['current_data']          = null;
      $data['current_course_title']  = $present_course_title;
     }

    $data['course_titles']      = $course_titles;
    $data['parent_course_title']= $parent_course_title;
    $data['course_title']       = $course_title;

    $data['academic_id']       = $academic_id;
    $data['course_parent_id']  = $course_parent_id;
    $data['course_id']         = $course_id;
    $data['current_year']      = $year;
    $data['current_semister']  = $semister;
    
    // dd($data);  

    return view('onlinemarks.consolidate_reports.marks-view', $data);

       // $view_name = getTheme().'::onlinemarks.consolidate_reports.marks-view';
       //  return view($view_name,$data);


}


public function printConsolidate(Request $request)
{
   // dd($request);


        if(!checkRole(getUserGrade(2)))
      {
        prepareBlockUserMessage();
        return back();
      }
    // dd($request);

    $academic_id      = $request->academic_id;
    $course_parent_id = $request->course_parent_id;
    $course_id        = $request->course_id;
    $user_id          = $request->user_id;

    $year  = 1;
    if($request->has('current_year')){
      $year   = $request->current_year;
    }

    $semister  = 0;
    if($request->has('current_semister')){
      $semister   = $request->current_semister;
    }

   

    $is_records   = QuizResult::where('user_id','=',$user_id)->get();
    if(!count($is_records)){
      
      flash('Ooops....','no_records_available','overlay');
      return back();
    }

    $academic_data = QuizResult::getStudentAcademicYears($request->user_id);
    // dd($academic_data);
    
    $course_titles = QuizResult::getCourseTitle($academic_data);
    $marks_data    = QuizResult::getConsolidateReport($academic_data, $user_id);
    // dd($marks_data);
    
    $present_academic_data = QuizResult::getPresentAcademicYear($request->user_id);
    // dd($present_course_titles);
    $present_course_title = QuizResult::getCourseTitle($present_academic_data);
    // dd($present_academic_data);
    $present_marks_data    = QuizResult::getConsolidateReport($present_academic_data, $user_id);
    // dd($present_marks_data);
    

    $student_data   = Student::where('user_id','=',$user_id)->first();
    $user_data      = User::where('id','=',$user_id)->first();
    
    $parent_course_title   = App\Course::where('id','=',$student_data->course_parent_id)->first()->course_title;
    $course_title   = App\Course::where('id','=',$student_data->course_id)->first()->course_title;
   
    $data['active_class']       = 'online_marks_reports';
    $data['student_data']       = $student_data;
    $data['user_data']          = $user_data;
    $data['title']              = getPhrase('view_reports');
    $data['marks_data']         = $marks_data;
    
    $promotions   = App\StudentPromotion::where('user_id','=',$user_id)->get();
    // dd($promotions);

   
    if(count($promotions) >= 2){
      
      $data['current_data']          = $present_marks_data;
      $data['current_course_title']  = $present_course_title;
    
     }else{

      $data['current_data']          = null;
      $data['current_course_title']  = $present_course_title;
     }

    $data['course_titles']      = $course_titles;
    $data['parent_course_title']= $parent_course_title;
    $data['course_title']       = $course_title;

    $data['academic_id']       = $academic_id;
    $data['course_parent_id']  = $course_parent_id;
    $data['course_id']         = $course_id;
    $data['current_year']      = $year;
    $data['current_semister']  = $semister;
    
    // dd($data);  

    return view('onlinemarks.consolidate_reports.print-view', $data);

}


}
  