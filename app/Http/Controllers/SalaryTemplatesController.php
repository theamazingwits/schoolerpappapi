<?php
namespace App\Http\Controllers;
use \App;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\SalaryTemplate;
use App\LogHelper;
use Yajra\Datatables\Datatables;
use DB;
use Exception; 

class SalaryTemplatesController extends Controller
{
        
    public function __construct()
    {
    	$this->middleware('auth');

    }


    public function index()
    {
        if(!checkRole(getUserGrade(18)))
        {
          prepareBlockUserMessage();
          return back();
        }

        $data['active_class']        = 'payroll';
        $data['title']               = getPhrase('salary_templates');
        $data['layout']              = getLayout();

        return view('payroll.salary_templates.list', $data);

        //  $view_name = getTheme().'::payroll.salary_templates.list';
        // return view($view_name,$data);
    }

    /**
     * This method returns the datatables data to view
     * @return [type] [description]
     */
    public function getDatatable()
    {

         $records = SalaryTemplate::select([ 'salary_grades','basic_salary','overtime_rate','slug']);
        
        return Datatables::of($records)
        ->addColumn('action', function ($records) {
           
            return '<div class="dropdown more">
                        <a id="dLabel" type="button" class="more-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-ellipsis-v"></i>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">

                            <li><a href="'.URL_PAYROLL_SALARY_TEMPLATES_EDIT.$records->slug.'"><i class="fa fa-pencil"></i>'.getPhrase("edit").'</a></li>

                            <li><a href="javascript:void(0);" onclick="deleteRecord(\''.$records->slug.'\');"><i class="fa fa-trash"></i>'. getPhrase("delete").'</a></li>
                        </ul>
                    </div>';
            })
        ->removeColumn('slug')
        ->make();
    }


    /**
     * This method loads the create view
     * @return void
     */
    public function create()
    {
    	if(!checkRole(getUserGrade(18)))
        {
          prepareBlockUserMessage();
          return back();
        }

        $data['record']         	= FALSE;
        $data['allowances']         = array();
        $data['deductions']         = array();
    	$data['active_class']       = 'payroll';
    	$data['title']              = getPhrase('add_salary_template');
        $data['layout']             = getLayout();
        return view('payroll.salary_templates.add-edit', $data);

        //    $view_name = getTheme().'::payroll.salary_templates.add-edit';
        // return view($view_name,$data);


    }


    /**
     * This method loads the edit view based on unique slug provided by user
     * @param  [string] $slug [unique slug of the record]
     * @return [view with record]       
     */
    public function edit($slug)
    {
    	if(!checkRole(getUserGrade(18)))
        {
          prepareBlockUserMessage();
          return back();
        }

        $record = SalaryTemplate::where('slug', $slug)->get()->first();
        $data['record']             = $record;
    	$data['allowances']        = DB::table('salarytemplate_allowances')->where('salarytemplate_id', $record->id)->get();
        $data['deductions']       	= DB::table('salarytemplate_deductions')->where('salarytemplate_id', $record->id)->get();
    	$data['active_class']       = 'payroll';
        $data['title']              = getPhrase('edit_salary_template');
        $data['layout']              = getLayout();
        
    	return view('payroll.salary_templates.add-edit', $data);

        //   $view_name = getTheme().'::payroll.salary_templates.add-edit';
        // return view($view_name,$data);
    }


    /**
     * Update record based on slug and reuqest
     * @param  Request $request [Request Object]
     * @param  [type]  $slug    [Unique Slug]
     * @return void
     */
    public function update(Request $request, $slug)
    {

        if(!checkRole(getUserGrade(18)))
        {
          prepareBlockUserMessage();
          return back();
        }

        $st_record  = SalaryTemplate::where('slug', $slug)->first();
        
          $this->validate($request, [
                    'salary_grades'     => 'bail|required|max:30|unique:salary_templates,salary_grades,'. $st_record->id,
                    'basic_salary'      => 'required|numeric',
                    'overtime_rate'     => 'required|numeric',
                    'gross_salary'      => 'required|numeric',
                    'total_deduction'   => 'required|numeric',
                    'net_salary'        => 'required|numeric',
                    ]);

        $allowance_names    = $request->allowance_name;
        $allowance_values   = $request->allowance_value;

        $deduction_names    = $request->deduction_name;
        $deduction_values   = $request->deduction_value;


        $name                       = $request->salary_grades;
       
       /**
        * Check if the title of the record is changed, 
        * if changed update the slug value based on the new title
        */
        if($name != $st_record->salary_grades)
            $st_record->slug = $st_record->makeSlug($name);
    	
        $st_record->salary_grades 	       = $name;
        $st_record->basic_salary       = $request->basic_salary;
        $st_record->overtime_rate      = $request->overtime_rate;
        $st_record->gross_salary       = $request->gross_salary;
        $st_record->total_deduction    = $request->total_deduction;
        $st_record->net_salary         = $request->net_salary;
        
        $st_record->save();

        $st_record->flag        = 'Update';
        $st_record->action      = 'Salary_templates';
        $st_record->object_id   =  $st_record->slug;
        $logs = new LogHelper();
        $logs->storeLogs($st_record);

        //Allowances Records
        if(count($allowance_names) > 0 && count($allowance_values) > 0) {

            //Delete old records
            DB::table('salarytemplate_allowances')->where('salarytemplate_id', $st_record->id)->delete();
            DB::table('salarytemplate_deductions')->where('salarytemplate_id', $st_record->id)->delete();

            $alw_record_final = array();

            for($i=0;$i<count($allowance_names);$i++) {

                if($allowance_names[$i] != "" && $allowance_values[$i] > 0) {

                    $alw_record['salarytemplate_id']    = $st_record->id;
                    $alw_record['allowance_name']       = $allowance_names[$i];
                    $alw_record['allowance_value']      = $allowance_values[$i];
                    array_push($alw_record_final, $alw_record);
                }
            }

            if(count($alw_record_final))
                DB::table('salarytemplate_allowances')->insert($alw_record_final);
        }


        //Deduction Records
        if(count($deduction_names) > 0 && count($deduction_values) > 0) {

            $ded_record_final = array();

            for($i=0;$i<count($deduction_names);$i++) {

                if($deduction_names[$i] != "" && $deduction_values[$i] > 0) {

                    $ded_record['salarytemplate_id']    = $st_record->id;
                    $ded_record['deduction_name']       = $deduction_names[$i];
                    $ded_record['deduction_value']      = $deduction_values[$i];
                    array_push($ded_record_final, $ded_record);
                }
            }

            if(count($ded_record_final))
                DB::table('salarytemplate_deductions')->insert($ded_record_final);
        }



    	flash('success','record_updated_successfully', 'success');
    	return redirect(URL_PAYROLL_SALARY_TEMPLATES);
    }

    /**
     * This method adds record to DB
     * @param  Request $request [Request Object]
     * @return void
     */
    public function store(Request $request)

    {

        if(!checkRole(getUserGrade(18)))
        {
          prepareBlockUserMessage();
          return back();
        }

        $this->validate($request, [
                    'salary_grades'     => 'bail|required|max:30|unique:salary_templates',
                    'basic_salary'      => 'required|numeric',
                    'overtime_rate'     => 'required|numeric',
                    'gross_salary'      => 'required|numeric',
                    'total_deduction'   => 'required|numeric',
                    'net_salary'        => 'required|numeric',
                    ]);


        $allowance_names    = $request->allowance_name;
        $allowance_values   = $request->allowance_value;

        $deduction_names    = $request->deduction_name;
        $deduction_values   = $request->deduction_value;

    	$st_record = new SalaryTemplate();
        $salary_grades			       = $request->salary_grades;
        $st_record->salary_grades 	   = $salary_grades;
        $st_record->basic_salary       = $request->basic_salary;
        $st_record->overtime_rate      = $request->overtime_rate;
        $st_record->gross_salary       = $request->gross_salary;
        $st_record->total_deduction    = $request->total_deduction;
        $st_record->net_salary         = $request->net_salary;
        $st_record->slug 			   = $st_record->makeSlug($salary_grades);
        $st_record->save();

        $st_record->flag        = 'Insert';
        $st_record->action      = 'Salary_templates';
        $st_record->object_id   =  $st_record->slug;
        $logs = new LogHelper();
        $logs->storeLogs($st_record);


        //Allowances Records
        if(count($allowance_names) > 0 && count($allowance_values) > 0) {

            $alw_record_final = array();

            for($i=0;$i<count($allowance_names);$i++) {

                if($allowance_names[$i] != "" && $allowance_values[$i] > 0) {

                    $alw_record['salarytemplate_id']    = $st_record->id;
                    $alw_record['allowance_name']       = $allowance_names[$i];
                    $alw_record['allowance_value']      = $allowance_values[$i];
                    array_push($alw_record_final, $alw_record);
                }
            }

            if(count($alw_record_final))
                DB::table('salarytemplate_allowances')->insert($alw_record_final);
        }


        //Deduction Records
        if(count($deduction_names) > 0 && count($deduction_values) > 0) {

            $ded_record_final = array();

            for($i=0;$i<count($deduction_names);$i++) {

                if($deduction_names[$i] != "" && $deduction_values[$i] > 0) {

                    $ded_record['salarytemplate_id']    = $st_record->id;
                    $ded_record['deduction_name']       = $deduction_names[$i];
                    $ded_record['deduction_value']      = $deduction_values[$i];
                    array_push($ded_record_final, $ded_record);
                }
            }

            if(count($ded_record_final))
                DB::table('salarytemplate_deductions')->insert($ded_record_final);
        }


        flash('success','record_added_successfully', 'success');
    	return redirect(URL_PAYROLL_SALARY_TEMPLATES);
    }


    /**
     * Delete Record based on the provided slug
     * @param  [string] $slug [unique slug]
     * @return Boolean 
     */
    public function delete($slug)
    {
        if(!checkRole(getUserGrade(18)))
        {
          prepareBlockUserMessage();
          return back();
        }

        try{
             if(!env('DEMO_MODE')) {
                $record = SalaryTemplate::where('slug', $slug)->first();
                $record = SalaryTemplate::where('slug', $slug)->delete();

                $record->flag        = 'Delete';
                $record->action      = 'Salary_templates';
                $record->object_id   =  $slug;
                $logs = new LogHelper();
                $logs->storeLogs($record);

                DB::table('salarytemplate_allowances')->where('salarytemplate_id', $record->id)->delete();
                DB::table('salarytemplate_deductions')->where('salarytemplate_id', $record->id)->delete();
            }
            $response['status'] = 1;
            $response['message'] = getPhrase('record_deleted_successfully');
        }
        catch(Exception $e)
        {
           $response['status'] = 0;
           if(getSetting('show_foreign_key_constraint','module'))
            $response['message'] =  $e->getMessage();
           else
            $response['message'] =  getPhrase('this_record_is_in_use_in_other_modules');
        }
        return json_encode($response);
    }

}
