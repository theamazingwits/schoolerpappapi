<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App;
use App\Http\Requests;
use App\Expense;
use App\LogHelper;
use Yajra\Datatables\Datatables;
use DB;
use Auth;
use Exception;

class ExpensesController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth');
    }

    /**
     * Expenses listing method
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
      if(!checkRole(getUserGrade(18)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $data['active_class']       = 'expenses';
        $data['layout']             = getLayout();
        $data['title']              = getPhrase('expenses');
        $data['module_helper']      = getModuleHelper('expenses-list');
    	   
         return view('expenses.list', $data);

          //  $view_name = getTheme().'::expenses.list';
          // return view($view_name,$data);
    }

    /**
     * This method returns the datatables data to view
     * @return [type] [description]
     */
    public function getDatatable($slug = '')
    {

      if(!checkRole(getUserGrade(18)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $records = array();
 

     
         $records = Expense::select(['title', 'expense_category_id', 'expense_amount', 'expense_date', 'id','slug' ]);
        
        $records->orderBy('updated_at', 'desc');


        return Datatables::of($records)
        ->addColumn('action', function ($records) {
         
          $link_data = '<div class="dropdown more">
                        <a id="dLabel" type="button" class="more-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-ellipsis-v"></i>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <li><a href="'.URL_EXPENSES_EDIT.$records->slug.'"><i class="fa fa-pencil"></i>'.getPhrase("edit").'</a></li>';
                            
                           $temp = '';
                           if(checkRole(getUserGrade(1))) {
                    $temp .= ' <li><a href="javascript:void(0);" onclick="deleteRecord(\''.$records->slug.'\');"><i class="fa fa-trash"></i>'. getPhrase("delete").'</a></li>';
                      }
                    
                    $temp .='</ul></div>';

                    $link_data .=$temp;
            return $link_data;
            })
        // ->editColumn('branch_id', function($records){
        //     $branch_rec = App\Branch::getBranchRecord($records->branch_id);
        //     return $branch_rec['branch_name'].' ('.$branch_rec['address'].')';
        // })
        ->editColumn('expense_category_id', function($records){
            
            $ec_rec = App\ExpenseCategory::where('id', $records->expense_category_id)->first();
            return $ec_rec['category_name'];
        })
        
        ->removeColumn('id')
        ->removeColumn('slug')
         
        ->make();
    }

    /**
     * This method loads the create view
     * @return void
     */
    public function create()
    {
      if(!checkRole(getUserGrade(18)))
      {
        prepareBlockUserMessage();
        return back();
      }
    	$data['record']         	 = FALSE;
    	$data['active_class']        = 'expenses';

        // $data['branches']            = addSelectToList(getBranches());
        $data['expense_categories']  = App\ExpenseCategory::orderBy('category_name', 'asc')->pluck('category_name', 'id');

        $data['title']               = getPhrase('create_expense');
        $data['module_helper']       = getModuleHelper('create-expense');
         $data['layout']              = getLayout();
    	// 
         return view('expenses.add-edit', $data);

          // $view_name = getTheme().'::expenses.add-edit';
          // return view($view_name,$data);
    }

    /**
     * This method loads the edit view based on unique slug provided by user
     * @param  [string] $slug [unique slug of the record]
     * @return [view with record]       
     */
    public function edit($slug)
    {
      if(!checkRole(getUserGrade(18)))
      {
        prepareBlockUserMessage();
        return back();
      }

    	$record = Expense::getRecordWithSlug($slug);
    	if($isValid = $this->isValidRecord($record))
    		return redirect($isValid);

    	  $data['record']       		= $record;
        // $data['branches']           = addSelectToList(getBranches());
        $data['expense_categories'] = App\ExpenseCategory::orderBy('category_name', 'asc')->pluck('category_name', 'id');
    	  $data['active_class']       = 'expenses';
      	$data['title']            = getPhrase('edit_expense');
         $data['layout']              = getLayout();
    	
        return view('expenses.add-edit', $data);

          // $view_name = getTheme().'::expenses.add-edit';
          // return view($view_name,$data);
    }

    /**
     * Update record based on slug and reuqest
     * @param  Request $request [Request Object]
     * @param  [type]  $slug    [Unique Slug]
     * @return void
     */
    public function update(Request $request, $slug)
    {
      if(!checkRole(getUserGrade(18)))
      {
        prepareBlockUserMessage();
        return back();
      }

    	$record = Expense::getRecordWithSlug($slug);
		 $rules = [
         // 'branch_id'        => 'bail|required' ,
         'title'            => 'bail|required|max:512' ,
         'expense_category_id' => 'bail|required' ,
         'expense_amount'   => 'bail|required|numeric' ,
         'expense_date' 	=> 'bail|required|date' ,
            ];
         /**
        * Check if the title of the record is changed, 
        * if changed update the slug value based on the new title
        */
       $name = $request->title;
        if($name != $record->title)
            $record->slug = $record->makeSlug($name);
      
       //Validate the overall request
       $this->validate($request, $rules);

        $record->title   = $name;
        // $record->branch_id          = $request->branch_id;
        $record->expense_category_id= $request->expense_category_id;
        $record->expense_amount     = $request->expense_amount;
        $record->expense_date       = $request->expense_date;

        $record->save();

        $record->flag        = 'Update';
        $record->action      = 'Expenses';
        $record->object_id   =  $record->slug;
        $logs = new LogHelper();
        $logs->storeLogs($record);


        flash('success','record_updated_successfully', 'success');
    	return redirect(URL_EXPENSES);
    }

    /**
     * This method adds record to DB
     * @param  Request $request [Request Object]
     * @return void
     */
    public function store(Request $request)
    {
      if(!checkRole(getUserGrade(18)))
      {
        prepareBlockUserMessage();
        return back();
      }

	    $rules = [
         // 'branch_id'        => 'bail|required' ,
         'title' => 'bail|required|max:512' ,
         'expense_category_id'      => 'bail|required' ,
         'expense_amount'   => 'bail|required|numeric' ,
         'expense_date'     => 'bail|required|date' ,
            ];
        $this->validate($request, $rules);

        $record                     = new Expense();
      	$name  						          = $request->title;
		    $record->title 	            = $name;
       	$record->slug 				      = $record->makeSlug($name);
        // $record->branch_id          = $request->branch_id;
        $record->expense_category_id= $request->expense_category_id;
        $record->expense_amount     = $request->expense_amount;
        $record->expense_date       = $request->expense_date;
        $record->save();

        $record->flag        = 'Insert';
        $record->action      = 'Expenses';
        $record->object_id   =  $record->slug;
        $logs = new LogHelper();
        $logs->storeLogs($record);


        flash('success','record_added_successfully', 'success');
    	return redirect(URL_EXPENSES);
    }
 
    /**
     * Delete Record based on the provided slug
     * @param  [string] $slug [unique slug]
     * @return Boolean 
     */
    public function delete($slug)
    {
      if(!checkRole(getUserGrade(18)))
      {
        prepareBlockUserMessage();
        return back();
      }
      /**
       * Delete the questions associated with this quiz first
       * Delete the quiz
       * @var [type]
       */
        $record = Expense::where('slug', $slug)->first();
        try{
            if(!env('DEMO_MODE')) {
                $record->delete();
            }

            $record->flag        = 'Delete';
            $record->action      = 'Expenses';
            $record->object_id   =  $slug;
            $logs = new LogHelper();
            $logs->storeLogs($record);

            $response['status'] = 1;
            $response['message'] = getPhrase('record_deleted_successfully');
        }
         catch ( Exception $e) {
                 $response['status'] = 0;
           if(getSetting('show_foreign_key_constraint','module'))
            $response['message'] =  $e->getMessage();
           else
            $response['message'] =  getPhrase('this_record_is_in_use_in_other_modules');
       }
        return json_encode($response);
    }

    public function isValidRecord($record)
    {
    	if ($record === null) {

    		flash('Ooops...!', getPhrase("page_not_found"), 'error');
   			return $this->getRedirectUrl();
		}

		return FALSE;
    }

    public function getReturnUrl()
    {
    	return URL_EXPENSES;
    }
   
  


}
