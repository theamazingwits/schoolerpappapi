<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App;
use \Auth;
class UserNotificationsController extends Controller
{
         public function __construct()
    {
    	$this->middleware('auth');
    }

    /**
     * Course listing method
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
      

        $data['active_class']       = 'notifications';
        $data['title']              = getPhrase('notifications');
        $data['layout']             = getLayout();
        $data['notifications']      = $notifications  = Auth::user()->notifications()->paginate(getRecordsPerPage());
        
        
    	return view('user-notifications.list', $data);
    }

    /**
     * This method returns the datatables data to view
     * @return [type] [description]
     */
    public function getDatatable($slug = '')
    {

      if(!checkRole(getUserGrade(2)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $records = array();
 
             
        $user = Auth::user();
        $records = $user->notifications;
            // $records = Notification::select(['title', 'valid_from', 'valid_to', 'url', 'id','slug' ])
            // ->orderBy('updated_at', 'desc');
             

        return Datatables::of($records)
        ->addColumn('action', function ($records) {
         
          $link_data = '<div class="dropdown more">
                        <a id="dLabel" type="button" class="more-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-ellipsis-v"></i>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <li><a href="'.URL_ADMIN_NOTIFICATIONS_EDIT.$records->id.'"><i class="fa fa-pencil"></i>'.getPhrase("edit").'</a></li>';
                            
                           $temp = '';
                           if(checkRole(getUserGrade(1))) {
                    $temp .= ' <li><a href="javascript:void(0);" onclick="deleteRecord(\''.$records->id.'\');"><i class="fa fa-trash"></i>'. getPhrase("delete").'</a></li>';
                      }
                    
                    $temp .='</ul></div>';

                    $link_data .=$temp;
            return $link_data;
            })
        ->editColumn('status', function($records)
        {
            return ($records->status == 'Active') ? '<i class="fa fa-check text-success"></i>' : '<i class="fa fa-times text-danger"></i>';
        })
        
        ->removeColumn('id')
        
        ->make();
    }
 

 
 
 
    /**
     * Delete Record based on the provided slug
     * @param  [string] $slug [unique slug]
     * @return Boolean 
     */
    public function delete($slug)
    {
      if(!checkRole(getUserGrade(2)))
      {
        prepareBlockUserMessage();
        return back();
      }
      /**
       * Delete the questions associated with this quiz first
       * Delete the quiz
       * @var [type]
       */
        $record = Notification::where('slug', $slug)->first();
        if(!env('DEMO_MODE')) {
            $record->delete();
        }

        $response['status'] = 1;
        $response['message'] = getPhrase('record_deleted_successfully');
        return json_encode($response);
    }

    public function isValidRecord($record)
    {
    	if ($record === null) {

    		flash('Ooops...!', getPhrase("page_not_found"), 'error');
   			return $this->getRedirectUrl();
		}

		return FALSE;
    }

    public function getReturnUrl()
    {
    	return URL_ADMIN_NOTIFICATIONS;
    }

 

    public function display($slug)
    {
       $user = Auth::user();
        $record = $user->notifications()->where('id','=',$slug)->first();
        if($record)
          $record->markAsRead();
        $data['active_class']       = 'notifications';
        $data['title']              = ($record)?$record->title:$record;
        $data['layout']             = getLayout();
        $data['notification']       = $record;
        
        return view('user-notifications.details', $data);  
    }
}
