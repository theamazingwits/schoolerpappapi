<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use App\Http\Requests;
use App\InventoryItemStock;
use App\InventoryItem;
use App\InventoryCategory;
use App\InventoryItemSupplier;
use App\InventoryStore;
use App\LogHelper;
use Yajra\Datatables\Datatables;
use DB;
use Auth;
use Exception;

class InventoryItemStockController extends Controller
{
        
  public function __construct()
    { 
      $this->middleware('auth');
    }
    /**
     * Course listing method
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {

      if(!checkRole(getUserGrade(17)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $data['active_class']       = 'inventory';
        $data['layout']             = getLayout();
        $data['title']              = getPhrase('inventory_item_stock');
         return view('inventory.itemstock.list', $data);
    }

    /**
     * This method returns the datatables data to view
     * @return [type] [description]
     */
    public function getDatatable($id ='')
    {

      if(!checkRole(getUserGrade(17)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $records = array();

        
            $records = InventoryItemStock::select(['category_id','item_id','supplier_id','store_id','id','updated_at'])
            ->orderBy('updated_at', 'desc');
             

        return Datatables::of($records)
        ->addColumn('action', function ($records) {
         
          $link_data = '<div class="dropdown more">
                        <a id="dLabel" type="button" class="more-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-ellipsis-v"></i>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                           <li><a href="'.URL_INVENTORY_ITEM_STOCK_EDIT.$records->id.'"><i class="fa fa-pencil"></i>'.getPhrase("edit").'</a></li>';
                            
                           $temp = '';
                           if(checkRole(getUserGrade(2))) {
                    $temp .= ' <li><a href="javascript:void(0);" onclick="deleteRecord(\''.$records->id.'\');"><i class="fa fa-trash"></i>'. getPhrase("delete").'</a></li>';
                      }
                    
                    $temp .='</ul></div>';


                    $link_data .=$temp;
                    return $link_data;
            })

        ->editColumn('category_id', function($records){
            
            $category  = InventoryCategory::find($records->category_id);
            if($category)
            return ucwords($category->name);
            return '-';
        })

        ->editColumn('item_id',function($records){

          $item = InventoryItem::find($records->item_id);
          if($item)
          return ucwords($item->name);
          return '-';

        })

         ->editColumn('supplier_id',function($records){

          $supplier = InventoryItemSupplier::find($records->supplier_id);
          if($supplier)
          return ucwords($supplier->name);
          return '-';

        })


         ->editColumn('store_id',function($records){

          $store = InventoryStore::find($records->store_id);
          if($store)
          return ucwords($store->store_name);
          return '-';

        })
        
        ->removeColumn('id')
        ->removeColumn('updated_at')
        ->make();
    }

     /**
     * This method loads the create view
     * @return void
     */

     public function create()
    {

     
      if(!checkRole(getUserGrade(17)))
      {
        prepareBlockUserMessage();
        return back();
      }
      $data['record']             = FALSE;
      $data['layout']             = getLayout();
      $data['active_class']       = 'inventory';
      $data['title']               = getPhrase('add-item');

      $data['categories']  = InventoryCategory::pluck('name','id')->toArray();
      $data['items']       = InventoryItem::pluck('name','id')->toArray();
      $data['suppliers']   = InventoryItemSupplier::pluck('name','id')->toArray();
      $data['stores']      = InventoryStore::pluck('store_name','id')->toArray();

      return view('inventory.itemstock.add-edit', $data);
    }


     /**
     * This method adds record to DB
     * @param  Request $request [Request Object]
     * @return void
     */


    public function store(Request $request)
    {

      if(!checkRole(getUserGrade(17)))
      {
        prepareBlockUserMessage();
        return back();
      }

       $rules = [

         //'item_file'              => 'bail|required|mimes:doc,docx,pdf,jpg,png' ,      
         'description'            => 'bail|required' 
       ];

        $this->validate($request, $rules);

        $record               =  new InventoryItemStock();
        $record->category_id  =  $request->category_id;
        $record->item_id      =  $request->item_id;
        $record->supplier_id  =  $request->supplier_id;
        $record->store_id     =  $request->store_id;
        $record->quantity     =  $request->quantity;
        $record->date         =  $request->date;
        $record->description  =  $request->description;
          
        $record->save();


        $record->flag      = 'Insert';
        $record->action      = 'Inventory_item_stocks';
        $record->object_id   =  $record->id;
        $logs = new LogHelper();
        $logs->storeLogs($record);

        $this->processUpload($request, $record, "item_file");

        flash('success','record_added_successfully', 'success');
        return redirect(URL_INVENTORY_ITEM_STOCK);
    }




    public function processUpload(Request $request, $record, $file_name)
    {
        
        if($request->hasFile($file_name)) 
        {
           
           $destinationPath  =  "public/uploads/inventory/";

           $fileName         =  $record->id.'-'.$request->$file_name->getClientOriginalName();

           $request->file($file_name)->move($destinationPath, $fileName);

           $record->upload_file  =  $fileName;

           $record->save();
       }

    }

    public function itemFile(Request $request)
    {
      $itemFile_type = $request->itemFile_type;
      $records   = InventoryItemStock::where('slug','=',$itemFile_type)->first();
      return $records;
    }
     /**
     * This method loads the edit view based on unique id provided by user
     * @param  [string] $id [unique id of the record]
     * @return [view with record]       
     */
    public function edit($id)
    {

      if(!checkRole(getUserGrade(17)))
      {
        prepareBlockUserMessage();
        return back();
      }

      
      $record = InventoryItemStock::getRecordWithSlug($id);

      if($isValid = $this->isValidRecord($record))
      return redirect($isValid);

      $data['record']             = $record;
      $data['active_class']       = 'inventory';
      $data['layout']             = getLayout();
      $data['title']              = getPhrase('edit_item');
     
      $data['categories']  = InventoryCategory::pluck('name','id')->toArray();
      $data['items']       = InventoryItem::pluck('name','id')->toArray();
      $data['suppliers']   = InventoryItemSupplier::pluck('name','id')->toArray();
      $data['stores']      = InventoryStore::pluck('store_name','id')->toArray();

      return view('inventory.itemstock.add-edit', $data);
    }

  



    /**
     * Update record based on id and reuqest
     * @param  Request $request [Request Object]
     * @param  [type]  $id    [Unique id]
     * @return void
     */
    public function update(Request $request, $id)
    {
      if(!checkRole(getUserGrade(17)))
      {
        prepareBlockUserMessage();
        return back();
      }

      $record = InventoryItemStock::getRecordWithSlug($id);
   

      $record->category_id  = $request->category_id;
      $record->item_id      = $request->item_id;
      $record->supplier_id  = $request->supplier_id;
      $record->store_id     = $request->store_id;
      $record->quantity     =  $request->quantity;
      $record->date         =  $request->date;
      $record->description  = $request->description;
      $record->save();


      $record->flag      = 'Update';
      $record->action      = 'Inventory_item_stocks';
      $record->object_id   =  $id;
      $logs = new LogHelper();
      $logs->storeLogs($record);

      $this->processUpload($request, $record, "item_file");

      
      
      flash('success','record_updated_successfully', 'success');
      return redirect(URL_INVENTORY_ITEM_STOCK);
    }


     /**
     * Delete Record based on the provided id
     * @param  [string] $id [unique id]
     * @return Boolean 
     */
    public function delete($id)
    {
      if(!checkRole(getUserGrade(17)))
      {
        prepareBlockUserMessage();
        return back();
      }
      /**
       * Check if any quizzes are associated with this instructions page, 
       * if not delete
       * @var [type]
       */
        $record   = InventoryItemStock::where('id', $id)->first();
        $response = [];
       try {
         if(!env('DEMO_MODE')) {
          $record->delete();
          }

          $record->flag      = 'Delete';
          $record->action      = 'Inventory_item_stocks';
          $record->object_id   =  $id;
          $logs = new LogHelper();
          $logs->storeLogs($record);

          $response['status']  = 1;
          $response['message'] = getPhrase('record_deleted_successfully');
      }
       catch ( \Illuminate\Database\QueryException $e) {
                 $response['status'] = 0;
           if(getSetting('show_foreign_key_constraint','module'))
            $response['message'] =  $e->errorInfo;
           else
            $response['message'] =  getPhrase('this_record_is_in_use_in_other_modules');
       }
        return json_encode($response);
    }

     public function isValidRecord($record)
    {
      if ($record === null) {

        flash('Ooops...!', getPhrase("page_not_found"), 'error');
        return $this->getRedirectUrl();
    }

    return FALSE;
    }

    public function getReturnUrl()
    {
      return URL_INVENTORY_ITEM_STOCK;
    }




}
