<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App;
use App\Http\Requests;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\User;
use Exception;
use Schema;
use DB;


class HostelPluginController extends Controller
{
    
  public function addHostel()
  {
  	  

  	  DB::beginTransaction();

      try { 
            
            // Table1
      	    $query =   "CREATE TABLE `hostel` (
						  `id` bigint(20) UNSIGNED NOT NULL,
						  `branch_id` bigint(20) DEFAULT NULL,
						  `name` varchar(100) DEFAULT NULL,
						  `type` tinyint(2) DEFAULT NULL,
						  `intake` int(10) DEFAULT NULL,
						  `description` text,
						  `address` text,
						  `created_at` timestamp NULL DEFAULT NULL,
						  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
						)"; 
						DB::statement($query);

		      $query  = "ALTER TABLE `hostel` ADD PRIMARY KEY (`id`)";  
		                 DB::statement($query);

		      $query  = "ALTER TABLE `hostel` MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5";  
		                 DB::statement($query);
              
               //Table2
             
		       $query  = "CREATE TABLE `hostel_fee` (
						  `id` bigint(20) UNSIGNED NOT NULL,
						  `title` varchar(100) DEFAULT NULL,
						  `user_id` int(10) DEFAULT NULL,
						  `branch_id` int(10) DEFAULT NULL,
						  `hostel_id` bigint(20) UNSIGNED NOT NULL,
						  `room_id` bigint(20) UNSIGNED NOT NULL,
						  `hostel_user_id` bigint(20) UNSIGNED NOT NULL,
						  `is_paid` tinyint(2) NOT NULL DEFAULT '0',
						  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
						  `paid_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
						  `paid_date` date DEFAULT NULL,
						  `discount` decimal(10,2) NOT NULL DEFAULT '0.00',
						  `balance` decimal(10,2) NOT NULL DEFAULT '0.00',
						  `year` varchar(20) DEFAULT NULL,
						  `month` varchar(20) DEFAULT NULL,
						  `start_date` date DEFAULT NULL,
						  `end_date` date DEFAULT NULL,
						  `created_at` timestamp NULL DEFAULT NULL,
						  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
						  `added_by` int(10) DEFAULT NULL,
						  `comments` text,
						  `payment_mode` varchar(20) DEFAULT NULL
						)";  
		                 DB::statement($query);

           
		    $query =   "ALTER TABLE `hostel_fee`
						  ADD PRIMARY KEY (`id`),
						  ADD KEY `hostel_id` (`hostel_id`),
						  ADD KEY `room_id` (`room_id`),
						  ADD KEY `hostel_user_id` (`hostel_user_id`)"; 
                       DB::statement($query);

		      $query  = "ALTER TABLE `hostel_fee` MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68";  
		                 DB::statement($query);
		     
		     //Table3

		      $query  = "CREATE TABLE `hostel_fee_type` (
						  `id` bigint(20) UNSIGNED NOT NULL,
						  `hostel_id` bigint(20) UNSIGNED NOT NULL,
						  `type` tinyint(2) NOT NULL DEFAULT '0',
						  `created_at` timestamp NULL DEFAULT NULL,
						  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
						)";  
		                 DB::statement($query);
 
		     $query  = "ALTER TABLE `hostel_fee_type` ADD PRIMARY KEY (`id`), ADD KEY `hostel_id` (`hostel_id`)";  
		                 DB::statement($query);

		    $query =   "ALTER TABLE `hostel_fee_type` MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23";       DB::statement($query);

		    
		    

		    //Table5
		      $query  = "CREATE TABLE `hostel_user_room` (
						  `id` bigint(20) UNSIGNED NOT NULL,
						  `user_id` bigint(20) UNSIGNED NOT NULL,
						  `hostel_id` bigint(20) UNSIGNED NOT NULL,
						  `room_id` bigint(20) UNSIGNED NOT NULL,
						  `is_active` tinyint(2) NOT NULL DEFAULT '1',
						  `is_vacate` tinyint(2) NOT NULL DEFAULT '0',
						  `created_at` timestamp NULL DEFAULT NULL,
						  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
						)";  
		                 DB::statement($query);

		      $query  = "ALTER TABLE `hostel_user_room`
						  ADD PRIMARY KEY (`id`),
						  ADD KEY `user_id` (`user_id`),
						  ADD KEY `hostel_id` (`hostel_id`),
						  ADD KEY `room_id` (`room_id`)";  
		                 DB::statement($query);


		    $query =  "ALTER TABLE `hostel_user_room` MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20";       DB::statement($query);
						
              
              //Table 6
		      $query  = "CREATE TABLE `room_types` (
						  `id` bigint(20) UNSIGNED NOT NULL,
						  `branch_id` int(10) DEFAULT NULL,
						  `room_type` varchar(100) DEFAULT NULL,
						  `description` text,
						  `created_at` timestamp NULL DEFAULT NULL,
						  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
						)";  
		                 DB::statement($query);

		      $query  = "ALTER TABLE `room_types` ADD PRIMARY KEY (`id`)";  
		                 DB::statement($query);

		    $query  = "ALTER TABLE `room_types` MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5";  
		                 DB::statement($query); 


		                  //Table4
		      $query  = "CREATE TABLE `hostel_rooms` (
						  `id` bigint(20) UNSIGNED NOT NULL,
						  `branch_id` int(10) DEFAULT NULL,
						  `room_number` varchar(50) DEFAULT NULL,
						  `hostel_id` bigint(20) UNSIGNED NOT NULL,
						  `room_type_id` bigint(20) UNSIGNED NOT NULL,
						  `beds` int(10) DEFAULT '0',
						  `cost` decimal(10,2) DEFAULT '0.00',
						  `description` text,
						  `created_at` timestamp NULL DEFAULT NULL,
						  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
						)";  
		                 DB::statement($query);

		      $query  = "ALTER TABLE `hostel_rooms` ADD PRIMARY KEY (`id`), ADD KEY `hostel_id` (`hostel_id`), ADD KEY `room_type_id` (`room_type_id`)";  
		                 DB::statement($query);

		    $query  = "ALTER TABLE `hostel_rooms`  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6";  
		                 DB::statement($query); 


		    $query =   "ALTER TABLE `hostel_rooms`
						  ADD CONSTRAINT `hostel_rooms_ibfk_1` FOREIGN KEY (`hostel_id`) REFERENCES `hostel` (`id`),
						  ADD CONSTRAINT `hostel_rooms_ibfk_2` FOREIGN KEY (`room_type_id`) REFERENCES `room_types` (`id`)"; 
						  DB::statement($query);



		   
		                                          
           flash('success','hostel_plugin_added_successfully', 'overlay');

           DB::commit();
      }

       catch ( Exception $e ) {

            DB::rollBack();
              // dd($e->getMessage());
            flash('success','hostel_plugin_added_successfully', 'overlay');
             
        }

        return redirect( URL_USERS_LOGIN );
  }

}