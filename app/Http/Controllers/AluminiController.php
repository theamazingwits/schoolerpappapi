<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use App\Http\Requests;
use App\User;
use App\AlumniDetails;
use App\AlumniEvent;
use App\AlumniStory;
use App\StoryComments;
use App\AlumniGallery;
use App\AlumniNotice;
use App\AlumniDonation;
use App\AlumniUserDonation;
use App\ImageSettings;
use Yajra\Datatables\Datatables;
use DB;
use Image;
use File;
use Auth;
use Exception;
use App\Paypal;

class AluminiController extends Controller
{
    //  public function __construct()
    // {
    // 	$this->middleware('auth');
     
    // }


    public function profile($user_id)
    {

        $data['active_class'] = '';
        $data['title']        = getPhrase('edit_profile');
        $data['user']         = User::join('alumni_details','alumni_details.user_id','=','users.id')
	                                  ->where('users.id',$user_id)
	                                  ->first();
        $data['mystories']  = TRUE;
        $data['countries']  =  DB::table('countries')->pluck('country_name','country_name')->toArray();
        $data['years']      = getAlumniYears();
        $data['profession'] = getAlumniProfession();
        $data['groups']     = bloodGroups();

        return view('alumini.edit-profile', $data);
    }


    public function updateProfile(Request $request)
    {   

    	$record  = User::find($request->user_id);
    	$user_data  = AlumniDetails::where('user_id',$request->user_id)->first();
    	// dd($user_data);
    		 $rules = [

		         'name'             => 'bail|required|max:60' ,
		         'class'            => 'bail|required',
		         'email'            => 'bail|required|unique:users,email,'.$record->id,
		         'username'         => 'bail|required|unique:users,username,'.$record->id,
		         'mobile_number'    => 'bail|required',
		         'location'         => 'bail|required',
		         'website'          => 'bail|required',
		         'biography'        => 'bail|required',
		         'alumni_profession' => 'bail|required',
		         'date_of_birth'    => 'bail|required',
		         'alumni_social'    => 'bail|required',
		         'blood_group'      => 'bail|required',

            ];
		        $custMsg  = ['class.required' => 'Passed out year required'];

            $this->validate($request, $rules, $custMsg);
    	// dd($request);
		   
		    $name = $request->name;
         
          if($name != $record->name)
            $record->slug              = $record->makeSlug($name);

        $record->alumni_class      = $request->class;
		    $record->alumni_profession = $request->alumni_profession;
		    $record->alumni_social     = $request->website;
		    $record->email             = $request->email;
		    $record->phone             = $request->mobile_number;
		    $record->username          = $request->username;
		    $record->name              = $request->name;

		    if($request->password){

		    	$password       = $request->password;
                $record->password = bcrypt($password);
		    }

		    $record->save();

		    $this->processUpload($request, $record);

		    $user_data->blood_group    = $request->blood_group;
		    $user_data->quote          = $request->biography;
		    $user_data->date_of_birth  = $request->date_of_birth;
		    $user_data->location       = $request->location;
		    $user_data->save();

           flash('success','your_profile_is_updated','success');
    	   return redirect(PREFIX);
    }

      protected function processUpload(Request $request, User $user)
     {
         if ($request->hasFile('image')) {
          
          $imageObject = new ImageSettings();
          
          $destinationPath      = $imageObject->getProfilePicsPath();
          $destinationPathThumb = $imageObject->getProfilePicsThumbnailpath();
          
          $fileName = $user->id.'.'.$request->image->guessClientExtension();
          
          $request->file('image')->move($destinationPath, $fileName);
          $user->image = $fileName;
         
          Image::make($destinationPath.$fileName)->fit($imageObject->getProfilePicSize())->save($destinationPath.$fileName);
         
          Image::make($destinationPath.$fileName)->fit($imageObject->getThumbnailSize())->save($destinationPathThumb.$fileName);
          $user->save();
        }
     }

     

     public function alumniEvents()
     {
        $data['active_class'] = 'events';
        $data['title']        = getPhrase('events');
        $events               = AlumniEvent::paginate(4);
        $data['events']       = $events;
       
        return view('alumini.events', $data);
     }

     public function eventInfo($id)
     {
        $data['active_class'] = 'events';
        $data['title']        = getPhrase('events');
        $event_data           = AlumniEvent::find($id);
        $data['event_data']   = $event_data;
        $data['mystories']    = TRUE;
        $data['year']         = getAlumniDate($event_data->date,'year');
        $data['month']        = getAlumniDate($event_data->date,'month');
        $data['day']          = getAlumniDate($event_data->date,'day');
        $data['dayname']      = getAlumniDate($event_data->date,'dayname');
        $data['user']         = User::find($event_data->volunteer_id);
        
        
        return view('alumini.event-info', $data);
     }


     public function stories()
     {
         
        $data['active_class'] = 'stories';
        $data['title']        = getPhrase('stories');
        $stories              = AlumniStory::paginate(4);
        $data['stories']      = $stories;
       
        return view('alumini.stories', $data);
     }

     public function storyInfo($id)
     {
        $data['active_class'] = 'stories';
        $data['title']        = getPhrase('stories');
        $story                = AlumniStory::find($id);
        $data['story']        = $story;
        $data['user']         = User::find($story->added_by);
        // $data['mystories']    = TRUE;
        $data['comments']     = StoryComments::where('story_id',$id)->get();
        // dd($comments);
        
        
        return view('alumini.story-info', $data);
     }

     public function userComment(Request $request)
     {
         $rules = [
              
              'comment'      => 'bail|required',
             
            ];

       
          $this->validate($request, $rules);

          $record  = new StoryComments();
          $record->user_id   = $request->user_id;
          $record->story_id  = $request->story_id;
          $record->comment   = $request->comment;
          $record->save();

          flash('success','your_comment_submitted_successfully','success');
          return redirect(URL_STORY_INFO.$request->story_id);

     }


     public function gallery()
     {
        
        $data['active_class'] = 'gallery';
        $data['title']        = getPhrase('gallery');
        $gallery              = AlumniGallery::where('gallery_id',0)->paginate(4);
        // dd($gallery);
        $data['gallery']      = $gallery;
        $data['mystories']    = TRUE;
       
        return view('alumini.gallery', $data);
     }


     public function notices()
     {
        $data['active_class'] = 'notices';
        $data['title']        = getPhrase('notices');
        $notices              = AlumniNotice::paginate(4);
        $data['notices']      = $notices;
        $data['mystories']    = TRUE;
       
        return view('alumini.notice', $data);
     }


     public function volunteers()
     {
        $data['active_class'] = 'volunteers';
        $data['title']        = getPhrase('volunteers');
        $volunteers           = AlumniEvent::join('users','users.id','=','almni_events.volunteer_id')
                                             ->groupby('users.id')
                                             ->select(['almni_events.id as event_id','name','title','email','alumni_class','alumni_profession','date','eaddress'])
                                             ->paginate(12);
        $data['volunteers']   = $volunteers;
        $data['mystories']    = TRUE;
       
        return view('alumini.volunteer', $data);
     }

     public function contact()
     {
        $data['active_class'] = 'contact_us';
        $data['title']        = getPhrase('contact_us');
        // $data['mystories']    = TRUE;
        $data['user']         = User::where('role_id',1)->first();
       
        return view('alumini.contact', $data);
     }


      /**
     * Send a email to super admin with user contact us details
     * @param Request $request [description]
     */
    public function ContactUs(Request $request)
    {
     

       $rules = [
              
              'name'              => 'bail|required',
              'email'             => 'bail|required',
              'message'           => 'bail|required',
            

            ];
        
        $this->validate($request, $rules);

       $data  = array(); 
       $data['name']     = $request->name;
       $data['email']    = $request->email; 
       $data['message']  = $request->message; 
        
        try {
           
            $super_admin  = User::where('role_id',1)->first();

            $super_admin->notify(new \App\Notifications\UserContactUs($super_admin, $data));

            
         } catch (Exception $e) {
           // dd($e->getMessage());
         } 
        
        flash('congratulations','our_team_will_contact_you_soon','overlay');
        return redirect(URL_ALUMNI_USER_CONTACT);

    }


    public function search()
    {
      
        $data['active_class'] = 'alumini-users';
        $data['title']        = getPhrase('alumni_users');
        $data['users']        = User::join('alumni_details','alumni_details.user_id','=','users.id')
                                      ->where('users.id','!=',Auth::user()->id )
                                      ->where('is_alumni',1)
                                      ->where('login_enabled',1)
                                      ->paginate(4);

        $data['years']       = getAlumniYears();                              
       
        return view('alumini.search-users', $data);
    }


    public function getAlumni(Request $request)
    {
       $batch  = $request->batch;
       $name   = $request->name;

        $data['active_class'] = 'alumini-users';
        $data['title']        = getPhrase('alumni_users');
         
        if( $request->has('name') && $request->has('batch') )
        {
           
            $data['users']        = User::join('alumni_details','alumni_details.user_id','=','users.id')
                                      ->where('users.name','=',$name )
                                      ->where('users.alumni_class','=',$batch )
                                      ->where('users.id','!=',Auth::user()->id )
                                      ->where('is_alumni',1)
                                      ->where('login_enabled',1)
                                      ->paginate(4);

        } 
        elseif($request->has('name') )
        {
            
              $data['users']        = User::join('alumni_details','alumni_details.user_id','=','users.id')
                                      ->where('users.name','=',$name )
                                      ->where('is_alumni',1)
                                      ->where('users.id','!=',Auth::user()->id )
                                      ->where('login_enabled',1)
                                      ->paginate(4);

        }elseif ($request->has('batch') ) 
        {
              $data['users']        = User::join('alumni_details','alumni_details.user_id','=','users.id')
                                      ->where('users.alumni_class','=',$batch )
                                      ->where('is_alumni',1)
                                      ->where('users.id','!=',Auth::user()->id )
                                      ->where('login_enabled',1)
                                      ->paginate(4);
        }else{

               $data['users']        = User::join('alumni_details','alumni_details.user_id','=','users.id')
                                      ->where('users.id','!=',Auth::user()->id )
                                      ->where('is_alumni',1)
                                      ->where('login_enabled',1)
                                      ->paginate(4);

        }
        

        $data['years']       = getAlumniYears();                              
       
        return view('alumini.search-users', $data);

    }

    public function viewAlumniDetails($user_id)
    {
       
        $data['active_class'] = 'alumini-users';
        $data['title']        = getPhrase('alumni_users');  
        $data['user']         = User::join('alumni_details','alumni_details.user_id','=','users.id')
                                      ->where('users.id',$user_id)
                                      ->first();
      
       return view('alumini.profile', $data);

    }

    public function donations()
     {
        
        $data['active_class'] = 'donations';
        $data['title']        = getPhrase('donations');
        $donations            = AlumniDonation::where('status',1)->paginate(4);
        $data['donations']    = $donations;
       
        return view('alumini.donations', $data);
     }

     public function paynow($id)
     {
        
            $donation   = AlumniDonation::find($id);
             // dd($donation);
            
            if(!getSetting('paypal', 'module'))
            {
                flash('Ooops...!', 'this_payment_gateway_is_not_available', 'error');          
                return back();
            }

            $record              = new AlumniUserDonation();
            $record->slug        = $record->makeSlug(getHashCode());;
            $record->donation_id = $donation->id;
            $record->user_id     = Auth::user()->id;
            $record->amount      = $donation->amount;
            $record->save();


            $paypal                          = new Paypal();
            $paypal->config['return']        = URL_DONATION_PAYPAL_PAYMENT_SUCCESS.'?token='.$record->slug;
            $paypal->config['cancel_return'] = URL_DONATION_PAYPAL_PAYMENT_CANCEL.'?token='.$record->slug;
            $paypal->invoice                 = $record->slug;
            $paypal->add($donation->title, $donation->amount); //ADD  item
            $paypal->pay(); //Proccess the payment
    
    }


    public function donationSuccess(Request $request)
    {
         
         $params = explode('?token=',$_SERVER['REQUEST_URI']) ;
    
         if(!count($params))
          return FALSE;
        
          $slug = $params[1];

          $payment_record                   = AlumniUserDonation::where('slug', '=', $slug)->first();
          $payment_record->status           = 1;
          $payment_record->amount           = $request->mc_gross;
          $payment_record->transaction_id   = $request->txn_id;
          $payment_record->paid_by          = $request->payer_email;
          $payment_record->transaction_data = json_encode($request->request->all());
          $payment_record->save();

          flash('success','your_payment_done_successfully','success');
          return redirect(URL_USER_DONATIONS.Auth::user()->id);
    }

    
    public function donationFail(Request $request)
    {
          $params = explode('?token=',$_SERVER['REQUEST_URI']) ;
    
         if(!count($params))
          return FALSE;
        
            $slug                    = $params[1];
            $payment_record          = AlumniUserDonation::where('slug', '=', $slug)->first();
            $payment_record->status  = 2;
            $payment_record->save();

          flash('Oops..!','your_payment_was_cancelled','overlay');
          return redirect(URL_ALUMNI_USER_DONATIONS);
    }

    public function subscriptions($id)
    {
        if(Auth::user()->id != $id){

            prepareBlockUserMessage();
            return back();
        }

        $data['active_class'] = 'user_donations';
        $data['title']        = getPhrase('user_donations');
        $user_donations       = AlumniUserDonation::join('users','users.id','=','alumni_user_donations.user_id')
                                                   ->join('alumni_donations','alumni_donations.id','=','alumni_user_donations.donation_id')
                                                   ->where('alumni_user_donations.status','=',1)
                                                   ->where('alumni_user_donations.user_id','=',Auth::user()->id)
                                                   ->select(['title','alumni_user_donations.created_at','alumni_user_donations.amount'])
                                                   ->orderby('alumni_user_donations.updated_at','desc')
                                                   ->paginate(12);

        $data['user_donations']   = $user_donations;
       
        return view('alumini.user-donations', $data);


    }


    /**
     * Course listing method
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function userDonations()
    {

      if(!checkRole(getUserGrade(23)))
      {
        prepareBlockUserMessage();
        return back();
      }
        $data['active_class']       = 'alumni';
        $data['layout']             = getLayout();
        $data['title']              = getPhrase('user_donations');

        return view('alumini.donations-list', $data);

          
    }

    /**
     * This method returns the datatables data to view
     * @return [type] [description]
     */
    public function getUserDoantions()
    {

      if(!checkRole(getUserGrade(23)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $records = array();
 
      
        $records = AlumniUserDonation::join('users','users.id','=','alumni_user_donations.user_id')
                                       ->join('alumni_donations','alumni_donations.id','=','alumni_user_donations.donation_id')
                                       ->select(['alumni_donations.title','users.name','alumni_user_donations.transaction_id','alumni_user_donations.paid_by','alumni_user_donations.amount','alumni_user_donations.status','users.alumni_class']);
    
            
        $records->orderBy('alumni_user_donations.updated_at', 'desc');
             

        return Datatables::of($records)
      

        ->editColumn('status',function($records){
          
             if($records->status == 1)
              return '<span class="label label-success">Success</span>';
             elseif($records->status == 2)
              return '<span class="label label-danger">Cancelled</span>';
            return '<span class="label label-info">Pending</span>';
        })


        ->editColumn('name',function($records){
          
             return ucwords($records->name).' - '.$records->alumni_class;
        })

         ->editColumn('transaction_id',function($records){
          
             if($records->transaction_id)
              return $records->transaction_id;
            return '-';
        })

           ->editColumn('paid_by',function($records){
          
             if($records->transaction_id)
              return $records->transaction_id;
            return '-';
        })

        

        ->removeColumn('alumni_class')
        ->make();
    }
     



   
}