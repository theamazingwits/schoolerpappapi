<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use \App;
use Yajra\Datatables\Datatables;
use DB;
use Auth;
use Input;
use App\Vechicle;
use App\Vroutes;
use App\VehicleAssign;
use App\LogHelper;

use Exception;

class RouteVechicleController extends Controller
{
     public function __construct()
    {
      $this->middleware('auth');
    }


    public function index($vechicle_id)
    {

    	if(!checkRole(getUserGrade(20)))
      {
        prepareBlockUserMessage();
        return back();
      }
    	
    	$vechicle  = Vechicle::findOrFail($vechicle_id);

         if( Vroutes::isMultiBranch() ){


         	 $main_routes = App\Vroutes::where('branch_id', getUserRecord()->branch_id)
	                             ->select(['id','name'])
                               ->where('parent_id',0)
	                             ->get();
         }else{

         	 $main_routes = App\Vroutes::select(['id','name'])
                                   ->where('parent_id',0) 
	                                 ->get();
         }

         foreach ($main_routes as $vroute) {
           
            $child_routes[]  = Vroutes::where('parent_id',$vroute->id)->get();
         }
            // dd($child_routes);
     

       $list = App\VehicleAssign::join('vroutes','vechicle_assign.parent_route_id','=','vroutes.id')
        					              ->where('vechicle_id','=',$vechicle->id)
        					              ->select(['vroutes.id','vroutes.name'])
                                ->groupby('vechicle_assign.parent_route_id')
        					              ->get();

       
        $data['active_class']       = 'transport';
        $data['record']             = $vechicle;
        $data['title']              = getPhrase('vehicle_number').' '.$vechicle->number.' '.getPhrase('assign_routes');
        $data['items']              = json_encode(array('source_items'=>$main_routes,'target_items'=>$list ,'show_items'=>$child_routes));
        $data['right_bar']          = TRUE;
        $data['right_bar_path']     = 'transport.assignroute.item-view-right-bar';
        $data['right_bar_data']     = array(
                                            'item' => $main_routes,
                                            'child_routes' => $child_routes,
                                            );
        $data['layout']              = getLayout();
        // dd($data);
      
      return view('transport.assignroute.add-edit',$data);

      // $view_name = getTheme().'::transport.assignroute.add-edit';
      //   return view($view_name,$data);

    }


    public function update(Request $request)
    {
    // dd($request);
      
      $vechicle  = Vechicle::findOrFail($request->vehicle_id);

      DB::beginTransaction();

      try{
           $model = VehicleAssign::where('vechicle_id','=',$vechicle->id);
           $count = $model->count();

          if($count)
          {
            //Previous records exists  
            $model->delete();
          }

          if($request->has('selected_list')){

              foreach($request->selected_list as $key=>$value)
              {


                $child_routes  =  Vroutes::where('parent_id',$value)->get();

                foreach ($child_routes as $sub_route) {

                  $newRecord                  = new VehicleAssign();
                  $newRecord->vechicle_id     = $vechicle->id;
                  $newRecord->route_id        = $sub_route->id;
                  $newRecord->parent_route_id = $sub_route->parent_id;
                  $newRecord->save();

                  $newRecord->flag        = 'Update';
                  $newRecord->action      = 'Vechicle_assigns';
                  $newRecord->object_id   =  $newRecord->id;
                  $logs = new LogHelper();
                  $logs->storeLogs($newRecord);          

                }

                
              }

            DB::commit();  
            flash('success...!','records_updated_successfully', 'success');
          }
          else{
             
             flash('Oops','please_select_routes', 'overlay');
          	
          }  
            
            DB::commit();
        }
      catch(Exception $ex)
      {
       
        DB::rollBack();

        if(getSetting('show_foreign_key_constraint','module'))
       {

          flash('oops...!',$e->errorInfo, 'error');
       }

       else {
          flash('oops...!','improper_data', 'error');
        }

      }

      return redirect( URL_VIEW_VEHICLES );
    }


    /**
     * Course listing method
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function vehicleList()
    {
      
      if(!checkRole(getUserGrade(20)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $data['active_class']       = 'transport';
        $data['layout']             = getLayout();
        $data['title']              = getPhrase('vehicle_routes');

         return view('transport.assignroute.vehicle-list',$data);
         
        //    $view_name = getTheme().'::transport.assignroute.vehicle-list';
        // return view($view_name,$data);
    }

    /**
     * This method returns the datatables data to view
     * @return [type] [description]
     */
    public function getDatatable($slug = '')
    {

      if(!checkRole(getUserGrade(20)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $records = array();
 
       
        $records = Vechicle::select(['id','number'])
                            ->orderBy('updated_at', 'desc');

        return Datatables::of($records)

        ->addColumn('vehicle_routes', function ($records) {
              
             $vroutes = Vechicle::join('vechicle_assign','vechicles.id','=','vechicle_assign.vechicle_id')
                                     ->join('vroutes','vroutes.id','=','vechicle_assign.route_id')
                                     ->where('vechicle_assign.vechicle_id','=',$records->id)
                                     ->select(['name','cost'])
                                     ->get();
              
              $vdata = '';
              if($vroutes->count() > 0){
                 
                 foreach ($vroutes as $vroute) {
                

                    $vdata .= '<span class="badge badge-default">'.ucwords($vroute->name).' ( '.getCurrencyCode().$vroute->cost.' )'.'</span>&nbsp;';

                }
              }else{

              	return '-';
              } 

              return $vdata;                      
             
         }) 

        ->addColumn('action', function ($records) {
         
          $link_data = '<div class="dropdown more">
                        <a id="dLabel" type="button" class="more-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-ellipsis-v"></i>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <li><a href="'.URL_ROUTE_VECHICLE_ASSIGN.$records->id.'"><i class="fa fa-exchange"></i>'.getPhrase("assign_routes").'</a></li></ul></div>';

                   
            return $link_data;
            })

        ->editColumn('number', function($records){

        	return '<a href="'. URL_ROUTE_VECHICLE_ASSIGN.$records->id .'">'.ucwords($records->number).'</a>';
        })
        
        ->removeColumn('id')
        ->make();
    }


}    