<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App;
use App\Http\Requests;
use App\SystemCertificates;
use App\LmsSettings;
use Yajra\Datatables\Datatables;
use DB;
use Auth;
use Image;
use ImageSettings;
use File;
use Exception;

class GenrateCertificateController extends Controller
{
   

    public function __construct()
    {
    	$this->middleware('auth');
    }

    protected  $examSettings;

    public function setSettings()
    {
        $this->examSettings = getSettings('lms');
    }

    public function getSettings()
    {
        return $this->examSettings;
    }
     
     
    /**
     * Course listing method
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        if(!checkRole(getUserGrade(18)))
      {
        prepareBlockUserMessage();
        return back();
      }
        $data['active_class']       = 'certificates';
        $data['title']              = getPhrase('certificates');
         $data['layout']             = getLayout();
    	
        return view('certificates.genrate.list', $data);

        //   $view_name = getTheme().'::certificates.genrate.list';
        // return view($view_name,$data);
        
    }

    /**
     * This method returns the datatables data to view
     * @return [type] [description]
     */
    public function getDatatable()
    {
        if(!checkRole(getUserGrade(18)))
      {
        prepareBlockUserMessage();
        return back();
      }

         $records = SystemCertificates::select([   
         	'title', 'slug','left_sign','left_sign_designation','left_sign_name','right_sign','right_sign_designation','right_sign_name','status','id'])
         ->where('slug','!=','tc')
         ->where('slug','!=','bonafide')
         ->orderby('updated_at','desc');
        $this->setSettings();
        return Datatables::of($records)
        ->addColumn('action', function ($records) {
         

            return '<div class="dropdown more">
                        <a id="dLabel" type="button" class="more-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-ellipsis-v"></i>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <li><a href="'.URL_CERTIFICATES_EDIT.$records->slug.'"><i class="fa fa-pencil"></i>'.getPhrase("edit").'</a></li>
                            
                            <li><a href="javascript:void(0);" onclick="deleteRecord(\''.$records->slug.'\');"><i class="fa fa-trash"></i>'. getPhrase("delete").'</a></li>
                        </ul>
                    </div>';
            })
       
        ->editColumn('left_sign',function($records){

            $data  = '<p>'.getPhrase('left_sign_name :').'<strong>'.$records->left_sign_name.'</strong></p>
                      <p>'.getPhrase('left_sign_designation :').'<strong>'.$records->left_sign_designation.'</strong></p>';
            $image = '<img src="'.IMAGE_PATH_UPLOAD_LMS_DEFAULT.'" height="100" width="100" />';
            if($records->left_sign)
            $image = '<img src="'.IMAGE_PATH_UPLOAD_CERTIFICATES.$records->left_sign.'" height="100" width="100" />';

             $data  .=$image;
               return $data;       

        })

        ->editColumn('right_sign',function($records){

            $data  = '<p>'.getPhrase('right_sign_name :').'<strong>'.$records->right_sign_name.'</strong></p>
                      <p>'.getPhrase('right_sign_designation :').'<strong>'.$records->right_sign_designation.'</strong></p>';
            $image = '<img src="'.IMAGE_PATH_UPLOAD_LMS_DEFAULT.'" height="100" width="100" />';
            if($records->right_sign)
            $image = '<img src="'.IMAGE_PATH_UPLOAD_CERTIFICATES.$records->right_sign.'" height="100" width="100" />';

             $data  .=$image;
               return $data;  
        })
        ->editColumn('status', function($records)
        {
            if($records->status==1){
                return $rec = '<span class="label label-success">'.getPhrase('active').'</span>';
            }
            else{

                return $rec = '<span class="label label-danger">'.getPhrase('inactive').'</span>';   
            }
        })
        ->removeColumn('slug')
        ->removeColumn('left_sign_designation')
        ->removeColumn('left_sign_name')
        ->removeColumn('right_sign_designation')
        ->removeColumn('right_sign_name')
        ->removeColumn('id')
        ->make();
    }

    /**
     * This method loads the create view
     * @return void
     */
    public function create()
    {
        if(!checkRole(getUserGrade(18)))
      {
        prepareBlockUserMessage();
        return back();
      }
    	$data['record']         	= FALSE;
    	$data['active_class']       = 'certificates';
    	$data['title']              = getPhrase('add_certificate');
        $data['layout']             = getLayout();

    	 return view('certificates.genrate.add-edit', $data);

        //  $view_name = getTheme().'::certificates.genrate.add-edit';
        // return view($view_name,$data);
    }

    /**
     * This method loads the edit view based on unique slug provided by user
     * @param  [string] $slug [unique slug of the record]
     * @return [view with record]       
     */
    public function edit($slug)
    {
        if(!checkRole(getUserGrade(18)))
      {
        prepareBlockUserMessage();
        return back();
      }
    	$record = SystemCertificates::getRecordWithSlug($slug);
    	if($isValid = $this->isValidRecord($record))
    		return redirect($isValid);

    	$data['record']       		= $record;
    	$data['active_class']       = 'certificates';
    	$data['title']              = getPhrase('edit_certificate');
        $data['layout']             = getLayout();
    	
        return view('certificates.genrate.add-edit', $data);

        //  $view_name = getTheme().'::certificates.genrate.add-edit';
        // return view($view_name,$data);
    }

    /**
     * Update record based on slug and reuqest
     * @param  Request $request [Request Object]
     * @param  [type]  $slug    [Unique Slug]
     * @return void
     */
    public function update(Request $request, $slug)
    {

        // dd($request);
        if(!checkRole(getUserGrade(18)))
      {
        prepareBlockUserMessage();
        return back();
      }
    	$record = SystemCertificates::getRecordWithSlug($slug);


		$rules = [
         'title'               => 'bail|required|max:60',
         
            ];
         /**
        * Check if the title of the record is changed, 
        * if changed update the slug value based on the new title
        */
       $name = $request->title;
        if($name != $record->title)
            $record->slug = $record->makeSlug($name,TRUE);
      
       //Validate the overall request
       $this->validate($request, $rules);
    	$record->title 			    = $name;
        $record->content                = $request->content;
        $record->status                 = $request->status;
        $record->left_sign_name         = $request->left_sign_name;
        $record->left_sign_designation  = $request->left_sign_designation;
        $record->right_sign_name        = $request->right_sign_name;
        $record->right_sign_designation = $request->right_sign_designation;
        $record->save();
 		 $file_name = 'left_sign';
 		if ($request->hasFile($file_name))
        {

             $rules = array( $file_name => 'mimes:jpeg,jpg,png,gif|max:10000' );
              $this->validate($request, $rules);
              $this->setSettings();
              $examSettings = $this->getSettings();
              $path = "public/uploads/systemcertificates/";
              $this->deleteFile($record->left_sign, $path);

              $record->left_sign  = $this->processUpload($request, $record,$file_name);
              $record->save();
        }

         $file_name = 'right_sign';
        if ($request->hasFile($file_name))
        {

             $rules = array( $file_name => 'mimes:jpeg,jpg,png,gif|max:10000' );
              $this->validate($request, $rules);
              $this->setSettings();
              $examSettings = $this->getSettings();
              $path = "public/uploads/systemcertificates/";
              $this->deleteFile($record->right_sign, $path);

              $record->right_sign  = $this->processUpload($request, $record,$file_name);
              $record->save();
        }

        flash('success','record_updated_successfully', 'success');
    	return redirect(URL_CERTIFICATES);
    }

    /**
     * This method adds record to DB
     * @param  Request $request [Request Object]
     * @return void
     */
    public function store(Request $request)
    { 
        // dd($request);

        if(!checkRole(getUserGrade(18)))
      {
        prepareBlockUserMessage();
        return back();
      }
       
	    $rules = [
         'title'               => 'bail|required|max:60',
       
            ];
        $this->validate($request, $rules);
        $record                         = new SystemCertificates();
      	$title  				        = $request->title;
		$record->title 			        = $title;
       	$record->slug 				    = $record->makeSlug($title,TRUE);
        $record->content                = $request->content;
        $record->status                 = $request->status;
        $record->left_sign_name         = $request->left_sign_name;
        $record->left_sign_designation  = $request->left_sign_designation;
        $record->right_sign_name        = $request->right_sign_name;
        $record->right_sign_designation = $request->right_sign_designation;
        $record->save();
 		$file_name = 'left_sign';
        if ($request->hasFile($file_name))
        {

            $rules = array( $file_name => 'mimes:jpeg,jpg,png,gif|max:10000' );
            $this->validate($request, $rules);
		    $this->setSettings();
            $examSettings = $this->getSettings();
	        $path = "public/uploads/systemcertificates/";
	        $this->deleteFile($record->left_sign, $path);

              $record->left_sign   = $this->processUpload($request, $record,$file_name);
              $record->save();
        }


        $file_name = 'right_sign';
        if ($request->hasFile($file_name))
        {

            $rules = array( $file_name => 'mimes:jpeg,jpg,png,gif|max:10000' );
            $this->validate($request, $rules);
            $this->setSettings();
            $examSettings = $this->getSettings();
            $path = "public/uploads/systemcertificates/";
            $this->deleteFile($record->right_sign, $path);

              $record->right_sign   = $this->processUpload($request, $record,$file_name);
              $record->save();
        }

        flash('success','record_added_successfully', 'success');
    	return redirect(URL_CERTIFICATES);
    }
 
    /**
     * Delete Record based on the provided slug
     * @param  [string] $slug [unique slug]
     * @return Boolean 
     */
    public function delete($slug)
    {
        if(!checkRole(getUserGrade(18)))
      {
        prepareBlockUserMessage();
        return back();
      }
        $record = SystemCertificates::where('slug', $slug)->first();
 
        try{
        $this->setSettings();
        
        $examSettings = $this->getSettings();
        $path = "public/uploads/systemcertificates/";
        $r =  $record;
         if(!env('DEMO_MODE')) {
            $record->delete();
            $this->deleteFile($r->left_sign, $path);
            $this->deleteFile($r->right_sign, $path);
        }
        
            $response['status'] = 1;
            $response['message'] = getPhrase('record_deleted_successfully');
        }
        catch (Exception $e) {
           $response['status'] = 0;
           if(getSetting('show_foreign_key_constraint','module'))
            $response['message'] =  $e->getMessage();
           else
            $response['message'] =  getPhrase('this_record_is_in_use_in_other_modules');
       }

         return json_encode($response);
   
    }

    public function isValidRecord($record)
    {
    	if ($record === null) {

    		flash('Ooops...!', getPhrase("page_not_found"), 'error');
   			return $this->getRedirectUrl();
		}

		return FALSE;
    }

    public function getReturnUrl()
    {
    	 return URL_CERTIFICATES;
    }

     public function deleteFile($record, $path, $is_array = FALSE)
    {
         if(!env('DEMO_MODE')) {
        $files = array();
        $files[] = $path.$record;
        File::delete($files);
        }
    }

     /**
     * This method process the image is being refferred
     * by getting the settings from ImageSettings Class
     * @param  Request $request   [Request object from user]
     * @param  [type]  $record    [The saved record which contains the ID]
     * @param  [type]  $file_name [The Name of the file which need to upload]
     * @return [type]             [description]
     */
     public function processUpload(Request $request, $record, $file_name)
     {
         if(env('DEMO_MODE')) {
            return;
         }
         if ($request->hasFile($file_name)) {
          $settings = json_decode((new LmsSettings())->getSettings());
          
          
          $destinationPath      = "public/uploads/systemcertificates/";
          
          $fileName = $record->id.'-'.$file_name.'.'.$request->$file_name->guessClientExtension();
          
          $request->file($file_name)->move($destinationPath, $fileName);
         
         //Save Normal Image with 300x300
          Image::make($destinationPath.$fileName)->fit($settings->imageSize)->save($destinationPath.$fileName);
         return $fileName;
        }
     }


     public function certificateData(Request $request)
     {
         $certificate_type  = $request->certificate_type;

         $records   = SystemCertificates::where('slug','=',$certificate_type)->first();
         return $records;
     }
}
