<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use App\Http\Requests;
use App\User;
use App\AlumniDetails;
use App\ImageSettings;
use Yajra\Datatables\Datatables;
use DB;
use Image;
use File;
use Auth;
use Exception;

class AlumniUsersController extends Controller
{
    
    public function __construct()
    {
    	$this->middleware('auth');
    }

    /**
     * Course listing method
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        // dd('herere');
      if(!checkRole(getUserGrade(23)))
      {
        prepareBlockUserMessage();
        return back();
      }
        $data['active_class']       = 'alumni';
        $data['layout']             = getLayout();
        $data['title']              = getPhrase('users');

        return view('alumini.users.list', $data);

          
    }

    /**
     * This method returns the datatables data to view
     * @return [type] [description]
     */
    public function getDatatable($slug = '')
    {

      if(!checkRole(getUserGrade(23)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $records = array();
 
      
        $records = User::select(['image','name','alumni_class','phone','email','id','login_enabled'])
                       ->where('is_alumni',1);
    
            
        $records->orderBy('updated_at', 'desc');
             

        return Datatables::of($records)
        ->addColumn('action', function ($records) {
         
          $link_data = '<div class="dropdown more">
                        <a id="dLabel" type="button" class="more-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-ellipsis-v"></i>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <li><a href="'.URL_ALUMNI_USERS_EDIT.$records->id.'"><i class="fa fa-pencil"></i>'.getPhrase("edit").'</a></li>';
                            
                           $temp = '';

                            $status_name = getPhrase('make_inactive');
                      if(!$records->login_enabled)
                        $status_name = getPhrase('make_active');

                         $temp .=   '<li><a href="javascript:void(0);" onclick="changeStatus(\''.$records->id.'\',\''.$records->login_enabled.'\');" ><i class="fa fa-star" aria-hidden="true"></i>'.$status_name.'</a></li>';

                           if(checkRole(getUserGrade(23))) {
                    $temp .= ' <li><a href="javascript:void(0);" onclick="deleteRecord(\''.$records->id.'\');"><i class="fa fa-trash"></i>'. getPhrase("delete").'</a></li>';
                      }
                    
                    $temp .='</ul></div>';

                    $link_data .=$temp;
            return $link_data;
            })

          ->editColumn('image', function($records){
            return '<img src="'.getProfilePath($records->image).'"  />';
        })
         ->editColumn('login_enabled',function($records){

              if($records->login_enabled == 1)
                return '<span class="label label-success label-sm">Active</span>';
              return '<span class="label label-warning label-sm">Inactive</span>';
         }) 

        ->removeColumn('id')
        ->make();
    }

    /**
     * This method loads the create view
     * @return void
     */
    public function create()
    {
      if(!checkRole(getUserGrade(23)))
      {
        prepareBlockUserMessage();
        return back();
      }
        $data['record']       = FALSE;
        $data['active_class'] = 'alumni';
        $data['layout']       = getLayout();
        $data['title']        = getPhrase('add_user');
        $data['years']        = getAlumniYears();
        $data['profession']   = getAlumniProfession();
       
        return view('alumini.users.add-edit', $data);

       
    }

    /**
     * This method loads the edit view based on unique slug provided by user
     * @param  [string] $slug [unique slug of the record]
     * @return [view with record]       
     */
    public function edit($id)
    {
      if(!checkRole(getUserGrade(23)))
      {
        prepareBlockUserMessage();
        return back();
      }
    
    // dd($id);
        $record = User::where('id',$id)->first();

        if($isValid = $this->isValidRecord($record))
            return redirect($isValid);

        $data['record']       = $record;
        $data['active_class'] = 'alumni';
        $data['layout']       = getLayout();
        $data['title']        = getPhrase('edit_user');
        $data['years']        = getAlumniYears();
        $data['profession']   = getAlumniProfession();

         return view('alumini.users.add-edit', $data);

        
    }

    
    /**
     * [update description]
     * @param  Request $request [description]
     * @param  [type]  $slug    [description]
     * @return [type]           [description]
     */
    public function update(Request $request, $id)
    {
        // dd($request);
      if(!checkRole(getUserGrade(23)))
      {
        prepareBlockUserMessage();
        return back();
      }


        $record = User::where('id',$id)->first();

        if($isValid = $this->isValidRecord($record))
            return redirect($isValid);


        $rules = [
              
              'name'              => 'bail|required',
              'alumni_class'      => 'bail|required',
              'email'             => 'bail|required|unique:users,email,'.$record->id,
              'phone'             => 'bail|required',
              'alumni_profession' => 'bail|required',
              'alumni_social'     => 'bail|required',
            ];

      
     

         $customMsg = ['alumni_class.required' => 'Passed out year required'];   

      
        $this->validate($request, $rules,$customMsg);

           
         
            if($request->password){
                
                $password         = $request->password;
                $record->password = bcrypt($password);
            }

            $name = $request->name;
     
         
          if($name != $record->name)
            $record->slug = $record->makeSlug($name);
            
            // $record->username       = $request->name;
            $record->phone          = $request->phone;
            $record->alumni_class   = $request->alumni_class;
            $record->alumni_profession = $request->alumni_profession;
            $record->alumni_social   = $request->alumni_social;
            $record->save();

         $this->processUpload($request, $record);

        flash('success','user_updated_successfully', 'success');

        return redirect(URL_ALUMNI_USERS);
    }

    /**
     * This method adds record to DB
     * @param  Request $request [Request Object]
     * @return void
     */
    public function store(Request $request)
    {
      if(!checkRole(getUserGrade(23)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $rules = [
              
              'name'              => 'bail|required',
              'alumni_class'      => 'bail|required',
              'email'             => 'bail|required|unique:users,email',
              'password'          => 'bail|required',
              'phone'             => 'bail|required',
              'alumni_profession' => 'bail|required',
              'alumni_social'     => 'bail|required',
            ];

         $customMsg = ['alumni_class.required' => 'Passed out year required'];   

      
        $this->validate($request, $rules,$customMsg);

            $user           = new User();
            $name           = $request->name;
            $user->name     = $name;
            $user->email    = $request->email;
            $password       = $request->password;
            $user->password = bcrypt($password);
         
            $user->role_id        = 14;
            $user->login_enabled  = 1;
            $slug                 = $user->makeSlug($name);
            // $user->username       = $request->name;
            $user->slug           = $slug;
            $user->phone          = $request->phone;
            $user->alumni_class   = $request->alumni_class;
            $user->alumni_profession = $request->alumni_profession;
            $user->alumni_social   = $request->alumni_social;
            $user->is_alumni      = 1;
            $user->save();

            $user->roles()->attach($user->role_id);

            $alumni           = new AlumniDetails();
            $alumni->user_id  = $user->id;
            $alumni->save();

            $link  = URL_ALUMINI_LOGIN;

        $this->processUpload($request, $user);

          try 
        {
            if (!env('DEMO_MODE')) {

             $user->notify(new \App\Notifications\NewUserRegistration($user,$user->email,$password, $link ));
            }

        }
        catch(Exception $ex)
        {
         
        }

        flash('success','user_added_successfully', 'success');

        return redirect(URL_ALUMNI_USERS);
    }


      protected function processUpload(Request $request, User $user)
     {
         if ($request->hasFile('image')) {
          
          $imageObject = new ImageSettings();
          
          $destinationPath      = $imageObject->getProfilePicsPath();
          $destinationPathThumb = $imageObject->getProfilePicsThumbnailpath();
          
          $fileName = $user->id.'.'.$request->image->guessClientExtension();
          ;
          $request->file('image')->move($destinationPath, $fileName);
          $user->image = $fileName;
         
          Image::make($destinationPath.$fileName)->fit($imageObject->getProfilePicSize())->save($destinationPath.$fileName);
         
          Image::make($destinationPath.$fileName)->fit($imageObject->getThumbnailSize())->save($destinationPathThumb.$fileName);
          $user->save();
        }
     }

     
 
    /**
     * Delete Record based on the provided slug
     * @param  [string] $slug [unique slug]
     * @return Boolean 
     */
    public function delete($slug)
    {
      if(!checkRole(getUserGrade(23)))
      {
        prepareBlockUserMessage();
        return back();
      }
      /**
       * Delete the questions associated with this quiz first
       * Delete the quiz
       * @var [type]
       */
         $record = User::where('id',$slug)->first();
        try{
            if(!env('DEMO_MODE')) {

              AlumniDetails::where('user_id',$record->id)->delete();
                $record->delete();
            }
            $response['status'] = 1;
            $response['message'] = getPhrase('record_deleted_successfully');
        }
         catch ( Exception $e) {
                 $response['status'] = 0;
           if(getSetting('show_foreign_key_constraint','module'))
            $response['message'] =  $e->getMessage();
           else
            $response['message'] =  getPhrase('this_record_is_in_use_in_other_modules');
       }
        return json_encode($response);
    }

    public function isValidRecord($record)
    {
        if ($record === null) {

            flash('Ooops...!', getPhrase("page_not_found"), 'error');
            return $this->getRedirectUrl();
        }

        return FALSE;
    }

    public function getReturnUrl()
    {
        return URL_ALUMNI_USERS;
    }

    public function changeStatus(Request $request)
    {
        
        $user = User::find($request->user_id);
        
        if($user)
        {  
           if($request->current_status == 0){

             $user->login_enabled  = 1;
           }else{

             $user->login_enabled  = 0;
           }
           
           $user->save();

           flash('success','alumni_status_updated_successfully','success');
           return redirect(URL_ALUMNI_USERS);
        }
        else{

           flash('error','user_is_not_existed','error');
           return back();
        }
    }

    


  

}