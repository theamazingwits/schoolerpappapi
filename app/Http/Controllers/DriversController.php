<?php
namespace App\Http\Controllers;
use \App;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Vroutes;
use App\Vdrivers;
use App\VdriverDocuments;
use App\LogHelper;
use Yajra\Datatables\Datatables;
use DB;
use Auth;
use Carbon\Carbon;
use Exception;
use Response;


class DriversController extends Controller
{
    
    public function __construct()
    {
    	$this->middleware('auth');
    }

    /**
     * Course listing method
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
      if(!checkRole(getUserGrade(20)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $data['active_class']       = 'transport';
        $data['layout']             = getLayout();
        $data['title']              = getPhrase('drivers');
        
        return view('transport.drivers.list',$data);

       
    }

    /**
     * This method returns the datatables data to view
     * @return [type] [description]
     */
    public function getDatatable($slug = '')
    {

      if(!checkRole(getUserGrade(20)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $records = array();
 
        if(checkRole('owner') && Vroutes::isMultiBranch() ) {
            $records = Vdrivers::select(['name','licence_number','experience','phone_number','slug']);
        } else {
          
            $records = Vdrivers::select(['name','licence_number','experience','phone_number','slug']);                              
        }
            
        $records->orderBy('updated_at', 'desc');
             

        return Datatables::of($records)

        ->addColumn('action', function ($records) {
         
          $link_data = '<div class="dropdown more">
                        <a id="dLabel" type="button" class="more-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-ellipsis-v"></i>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <li><a href="'.URL_VECHICLE_DRIVER_EDIT.$records->slug.'"><i class="fa fa-pencil"></i>'.getPhrase("edit").'</a></li>';
                            
                           $temp = '';
                           if(checkRole(getUserGrade(2))) {
                    $temp .= ' <li><a href="javascript:void(0);" onclick="deleteRecord(\''.$records->slug.'\');"><i class="fa fa-trash"></i>'. getPhrase("delete").'</a></li>';
                      }
                    
                    $temp .='</ul></div>';

                    $link_data .=$temp;
            return $link_data;
            })

     
          ->editColumn('name', function($records){
        
           return ucwords($records->name);
        })
         
        ->removeColumn('slug')
        ->make();
    }

    /**
     * This method loads the create view
     * @return void
     */
    public function create()
    {
      if(!checkRole(getUserGrade(20)))
      {
        prepareBlockUserMessage();
        return back();
      }
        $data['record']             = FALSE;
         if( Vroutes::isMultiBranch() ){

        $data['branches']           = addSelectToList(getBranches());
        }else{

        $data['branches']           = null;
        }
        $data['active_class']       = 'transport';
        $data['layout']             = getLayout();
        $data['title']              = getPhrase('add_driver');
        
        $experience  = [];

        for ($i=1; $i<=30 ; $i++) { 
          
          $experience[$i]  = $i;
        }
        // dd($experience);
        $data['experience']  = $experience;

        return view('transport.drivers.add-edit',$data);


    }

    /**
     * This method loads the edit view based on unique slug provided by user
     * @param  [string] $slug [unique slug of the record]
     * @return [view with record]       
     */
    public function edit($slug)
    {
      if(!checkRole(getUserGrade(20)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $record = Vdrivers::where('slug',$slug)->first();

      
        $data['record']              = $record;
         if( Vroutes::isMultiBranch() ){

        $data['branches']           = addSelectToList(getBranches());
        }else{

        $data['branches']           = null;
        }
        $data['active_class']        = 'transport';
      
        $data['layout']             = getLayout();
        $data['title']              = getPhrase('edit_driver');

         for ($i=1; $i<=30 ; $i++) { 
          
          $experience[$i]  = $i;
        }
        $data['experience']  = $experience;


          return view('transport.drivers.add-edit',$data);

    
    }

    /**
     * Update record based on slug and reuqest
     * @param  Request $request [Request Object]
     * @param  [type]  $slug    [Unique Slug]
     * @return void
     */
    public function update(Request $request, $id)
    {
        $rules = [
         'name'              => 'bail|required|max:100' ,
         'experience'                => 'bail|required',
         'phone_number'                => 'bail|required',
         'licence_number'                => 'bail|required',
         'spouse_name'                    => 'bail|required|max:100',
         'number_of_children'            => 'bail|required',
         'alternate_mobile_number'      => 'bail|required',
            ];
        $this->validate($request, $rules);

        if(!checkRole(getUserGrade(20)))
        {
          prepareBlockUserMessage();
          return back();
        }

         $record   = Vdrivers::find($id);
         $record->update($request->all());

         $name = $request->name;
         
         if($name != $record->name)
         $record->slug = $record->makeSlug($name);
         $record->save(); 
         
         $record->flag        = 'Update';
         $record->action      = 'Vdrivers';
         $record->object_id   =  $record->slug;
         $logs = new LogHelper();
         $logs->storeLogs($record);

          $this->processUpload($request, $record,'licence', 'edit');
          $this->processUpload($request, $record,'experience', 'edit');
          $this->processUpload($request, $record,'id_proof', 'edit');
          $this->processUpload($request, $record,'other', 'edit');
        

         flash('success','driver_data_updated_successfully', 'success');

         return redirect(URL_VECHICLE_DRIVER);
    }

    /**
     * This method adds record to DB
     * @param  Request $request [Request Object]
     * @return void
     */
    public function store(Request $request)
    {

      // dd($request);
// print_r($request->alternate_mobile_number);
// exit;
      if(!checkRole(getUserGrade(20)))
      {
        prepareBlockUserMessage();
        return back();
      }

       $rules = [
         'name'              => 'bail|required|max:100' ,
         'experience'                => 'bail|required',
         'phone_number'                => 'bail|required',
         'licence_number'                => 'bail|required',
         'spouse_name'                    => 'bail|required|max:100',
         'number_of_children'            => 'bail|required',
         'alternate_mobile_number'      => 'bail|required',
          ];
        $this->validate($request, $rules);


       DB::beginTransaction();
      //  print_r('hi');
       try {
        // print_r('welcome');
          
          $record         = Vdrivers::create($request->all());
          // print_r($record);
          // exit;
          $record->slug   = $record->makeSlug($record->name);
     
          $record->save();

          $record->flag        = 'Insert';
          $record->action      = 'Vdrivers';
          $record->object_id   =  $record->slug;
          $logs = new LogHelper();
          $logs->storeLogs($record);

          $this->processUpload($request, $record,'licence');
          $this->processUpload($request, $record,'experience');
          $this->processUpload($request, $record,'id_proof');
          $this->processUpload($request, $record,'other');
          
          // dd($record);
          
          DB::commit();

          flash('success','driver_added_successfully', 'success');
       } 
       catch (Exception $e) {
        // print_r('end');
            
          DB::rollBack();
          // dd($e->getMessage());
          flash('Oops...!','Error! Please Try again', 'error');
      }

       return redirect(URL_VECHICLE_DRIVER);
    }


     public function processUpload(Request $request, $record, $file_name,$type="edit")
     {
          if(env('DEMO_MODE')) {
              return;
          }
         

         if ($request->hasFile($file_name)) {
           
           if($type == "edit"){

             VdriverDocuments::where('driver_id',$record->id)
                               ->where('document_type', $file_name)
                               ->delete();
           }
        
          $destinationPath  = "public/uploads/transport";
          
          $fileName = ucwords($record->slug).'-'.$request->$file_name->getClientOriginalName();
          
          $request->file($file_name)->move($destinationPath, $fileName);

           $driver_file                = new VdriverDocuments();
           $driver_file->driver_id     = $record->id;
           $driver_file->document_type = ucfirst($file_name);
           $driver_file->document_name = $fileName;
           $driver_file->save();

           $driver_file->flag        = 'Insert';
           $driver_file->action      = 'Vdriver_documents';
           $driver_file->object_id   =  $record->slug;
           $logs = new LogHelper();
           $logs->storeLogs($driver_file);

         
      
        }

     }


     // public function getDocuments(Request $request)
     // {
         
     //     $documents   = VdriverDocuments::where('driver_id',$request->driver_id)
     //                                    ->get();

     //     return json_encode(array('all_documents'=>$documents));                               
     // }

      public function downloadFile($id)
     {
        
        try {
          
           $record  = VdriverDocuments::where('id',$id)->first();

         $path   = public_path()."/uploads/transport/".$record->document_name;
       
         return Response::download($path);

        } catch (Exception $e) {
           
           flash('Ooops..!','file_is_not_found_please_upload_new_one','overlay');
           return back();
        } 
        
     }
 
    /**
     * Delete Record based on the provided slug
     * @param  [string] $slug [unique slug]
     * @return Boolean 
     */
    public function delete($id)
    {
      if(!checkRole(getUserGrade(20)))
      {
        prepareBlockUserMessage();
        return back();
      }
      /**
       * Delete the questions associated with this quiz first
       * Delete the quiz
       * @var [type]
       */
        $record = Vroutes::where('id', $id)->first();

        try{
            if(!env('DEMO_MODE')) {
        Vroutes::where('parent_id', $record->id)->delete();
                $record->delete();
            }

            $record->flag        = 'Insert';
            $record->action      = 'Vroutes';
            $record->object_id   =  $record->id;
            $logs = new LogHelper();
            $logs->storeLogs($record);
 
            $response['status'] = 1;
            $response['message'] = getPhrase('record_deleted_successfully');
        }
         catch ( Exception $e) {
                 $response['status'] = 0;
           if(getSetting('show_foreign_key_constraint','module'))
            $response['message'] =  $e->getMessage();
           else
            $response['message'] =  getPhrase('this_record_is_in_use_in_other_modules');
       }
        return json_encode($response);
    }

    public function isValidRecord($record)
    {
        if ($record === null) {

            flash('Ooops...!', getPhrase("page_not_found"), 'error');
            return $this->getRedirectUrl();
        }

        return FALSE;
    }

    public function getReturnUrl()
    {
        return URL_VECHICLE_DRIVER;
    }


    
}