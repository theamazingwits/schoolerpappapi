<?php
namespace App\Http\Controllers;
use \App;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Vechicle;
use App\Vroutes;
use App\VehicleFuelDetails;
use App\LogHelper;
use Yajra\Datatables\Datatables;
use DB;
use Auth;
use Carbon\Carbon;


class VechiclesController extends Controller
{
    
    public function __construct()
    {
    	$this->middleware('auth');
    }

    /**
     * Course listing method
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
      if(!checkRole(getUserGrade(20)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $data['active_class']       = 'transport';
        $data['layout']             = getLayout();
        $data['title']              = getPhrase('vehicles');
        
          return view('transport.vechicles.list',$data);

        //    $view_name = getTheme().'::transport.vechicles.list';
        // return view($view_name,$data);
    }

    /**
     * This method returns the datatables data to view
     * @return [type] [description]
     */
    public function getDatatable($slug = '')
    {

      if(!checkRole(getUserGrade(20)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $records = array();
 
        if(checkRole('owner') && Vroutes::isMultiBranch() ) {
            $records = Vechicle::select(['branch_id','id','number', 'model','seats','driver_id']);
        } else {
            // $records = Vechicle::select(['id','number', 'model','driver_id','driver_license','driver_contact','seats'])->where('branch_id', getUserRecord()->branch_id);
            
            $records = Vechicle::select(['id','number', 'model','seats','driver_id']);
        }
            
        $records->orderBy('updated_at', 'desc');
             

        return Datatables::of($records)
        ->addColumn('driver_licence', function($records){

              $driver  = App\Vdrivers::where('id',$records->driver_id)->first();
            return $driver->licence_number;
        })
         ->addColumn('driver_number', function($records){

              $driver  = App\Vdrivers::where('id',$records->driver_id)->first();
            return $driver->phone_number;
        })
        ->addColumn('action', function ($records) {
         
          $link_data = '<div class="dropdown more">
                        <a id="dLabel" type="button" class="more-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-ellipsis-v"></i>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <li><a href="'.URL_VECHICLES_EDIT.$records->id.'"><i class="fa fa-pencil"></i>'.getPhrase("edit").'</a></li>
                            <li><a href="'.URL_ROUTE_VECHICLE_ASSIGN.$records->id.'"><i class="fa fa-exchange"></i>'.getPhrase("assign_routes").'</a></li>';


                            
                           $temp = '';
                           if(checkRole(getUserGrade(2))) {
                    $temp .= ' <li><a href="javascript:void(0);" onclick="deleteRecord(\''.$records->id.'\');"><i class="fa fa-trash"></i>'. getPhrase("delete").'</a></li>';
                      }
                    
                    $temp .='</ul></div>';

                    $link_data .=$temp;
            return $link_data;
            })

        ->editColumn('driver_id', function($records){
        
            $driver  = App\Vdrivers::where('id',$records->driver_id)->first();
            return ucwords($driver->name);
        })

        
        ->removeColumn('id')
        ->make();
    }

    /**
     * This method loads the create view
     * @return void
     */
    public function create()
    {
     if(!checkRole(getUserGrade(20)))
      {
        prepareBlockUserMessage();
        return back();
      }
        $data['record']             = FALSE;
         if( Vroutes::isMultiBranch() ){

        $data['branches']     = addSelectToList(getBranches());
        }else{

        $data['branches']     = null;
        }
        $data['active_class'] = 'transport';
        $data['layout']       = getLayout();
        $data['title']        = getPhrase('create_vehicle');
        $data['drivers']      = App\Vdrivers::where('vehicle_id',0)->pluck('name','id')->toArray();
        $data['trackdevice'] = array('1'=>'Driver Mobile','2'=>'Tracking Device');
        $data['component'] = array('1'=>'QR Scanner','2'=>'RF ID','3'=>'Blutooth Device');

            return view('transport.vechicles.add-edit',$data);

        //  $view_name = getTheme().'::transport.vechicles.add-edit';
        // return view($view_name,$data);
    }

    /**
     * This method loads the edit view based on unique slug provided by user
     * @param  [string] $slug [unique slug of the record]
     * @return [view with record]       
     */
    public function edit($id)
    {
      if(!checkRole(getUserGrade(20)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $record = Vechicle::findOrFail($id);
        if($isValid = $this->isValidRecord($record))
            return redirect($isValid);

        $data['record']              = $record;
         if( Vroutes::isMultiBranch() ){

        $data['branches']      = addSelectToList(getBranches());
        }else{

        $data['branches']      = null;
        }
        $data['active_class']  = 'transport';
      
        $data['layout']        = getLayout();
        $data['title']         = getPhrase('edit_vehicle');
        $data['drivers']       = App\Vdrivers::pluck('name','id')->toArray();

        return view('transport.vechicles.add-edit',$data);


        //  $view_name = getTheme().'::transport.vechicles.add-edit';
        // return view($view_name,$data);
    }

    /**
     * Update record based on slug and reuqest
     * @param  Request $request [Request Object]
     * @param  [type]  $slug    [Unique Slug]
     * @return void
     */
    public function update(Request $request, $id)
    {
      if(!checkRole(getUserGrade(20)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $record              = Vechicle::findOrFail($id);
        $driver              = App\Vdrivers::where('id',$record->driver_id)->first();
        $driver->vehicle_id  = $record->id;
        $driver->save();

        $record->flag        = 'Update';
        $record->action      = 'Vechicles';
        $record->object_id   =  $record->id;
        $logs = new LogHelper();
        $logs->storeLogs($record);

        $record->update($request->all());
       
        flash('success','vehicle_updated_successfully', 'success');
        return redirect(URL_VECHICLES);
    }

    /**
     * This method adds record to DB
     * @param  Request $request [Request Object]
     * @return void
     */
    public function store(Request $request)
    {
      if(!checkRole(getUserGrade(20)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $record              = Vechicle::create($request->all());
        $driver              = App\Vdrivers::where('id',$request->driver_id)->first();
        $driver->vehicle_id  = $record->id;
        $driver->save();

        $record->flag        = 'Insert';
        $record->action      = 'Vechicles';
        $record->object_id   =  $record->id;
        $logs = new LogHelper();
        $logs->storeLogs($record);
        

        flash('success','vehicle_added_successfully', 'success');
        return redirect(URL_VECHICLES);
    }
 
    /**
     * Delete Record based on the provided slug
     * @param  [string] $slug [unique slug]
     * @return Boolean 
     */
    public function delete($id)
    {
      if(!checkRole(getUserGrade(20)))
      {
        prepareBlockUserMessage();
        return back();
      }
      /**
       * Delete the questions associated with this quiz first
       * Delete the quiz
       * @var [type]
       */
        $record = Vechicle::where('id', $id)->first();
        try{
            if(!env('DEMO_MODE')) {
                $record->delete();
            }

            $record->flag        = 'Delete';
            $record->action      = 'Vechicles';
            $record->object_id   =  $id;
            $logs = new LogHelper();
            $logs->storeLogs($record);

            $response['status'] = 1;
            $response['message'] = getPhrase('record_deleted_successfully');
        }
         catch ( Exception $e) {
                 $response['status'] = 0;
           if(getSetting('show_foreign_key_constraint','module'))
            $response['message'] =  $e->getMessage();
           else
            $response['message'] =  getPhrase('this_record_is_in_use_in_other_modules');
       }
        return json_encode($response);
    }

    public function isValidRecord($record)
    {
        if ($record === null) {

            flash('Ooops...!', getPhrase("page_not_found"), 'error');
            return $this->getRedirectUrl();
        }

        return FALSE;
    }

    public function getReturnUrl()
    {
        return URL_VECHICLES;
    }



        /**
     * Course listing method
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function vehiclefuelindex()
    {
      if(!checkRole(getUserGrade(20)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $data['active_class']       = 'transport';
        $data['layout']             = getLayout();
        $data['title']              = getPhrase('vehicles_fuel_details');
        
          return view('transport.vechicles.vehicle_fuel_list',$data);

        //    $view_name = getTheme().'::transport.vechicles.list';
        // return view($view_name,$data);
    }
        /**
     * This method returns the datatables data to view
     * @return [type] [description]
     */
    public function getfuelDatatable($slug = '')
    {

      if(!checkRole(getUserGrade(20)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $records = array();
 
        $records  =   DB::table('vehicle_fuel_details as a')
                         ->select('a.fuel_date','a.driver_name','a.vehicle_name','a.fuel_quantity',
                                  'a.fuel_cost','a.receipt','a.receipt_name','a.driver_id','a.vehicle_id')
                         ->orderBy('updated_at','DESC')
                         ->get();
                 //  print_r($records);
                //  exit;
  
          return Datatables::of($records)

       ->removeColumn('driver_id')
        ->removeColumn('vehicle_id')
        ->make();
    }
}