<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App;
use App\Http\Requests;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\User;
use Exception;
use Schema;
use DB;


class PayrollPluginController extends Controller
{
    
  public function addHostel()
  {
  	  

  	  DB::beginTransaction();

      try { 
            
            // Table1
      	    $query =   "CREATE TABLE `expenses` (
						  `id` bigint(20) UNSIGNED NOT NULL,
						  `title` varchar(512) NOT NULL,
						  `expense_category_id` int(11) NOT NULL,
						  `slug` varchar(550) NOT NULL,
						  `expense_amount` decimal(10,2) NOT NULL,
						  `expense_date` date NOT NULL,
						  `branch_id` int(11) NOT NULL,
						  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
						  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
						)"; 
						DB::statement($query);

		      $query  = "ALTER TABLE `expenses` ADD PRIMARY KEY (`id`)";  
		                 DB::statement($query);

		      $query  = "ALTER TABLE `expenses` MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11";       DB::statement($query);
              
               //Table2
             
		       $query  = "CREATE TABLE `expense_categories` (
						  `id` int(11) NOT NULL,
						  `category_name` varchar(512) CHARACTER SET utf8 NOT NULL,
						  `slug` varchar(550) CHARACTER SET utf8 NOT NULL,
						  `code` varchar(20) DEFAULT NULL,
						  `description` varchar(1000) CHARACTER SET utf8 NOT NULL,
						  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
						  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
						)";  
		                 DB::statement($query);

           
		    $query =   "ALTER TABLE `expense_categories` ADD PRIMARY KEY (`id`)"; 
                       DB::statement($query);

		      $query  = "ALTER TABLE `expense_categories` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5";  
		                 DB::statement($query);
		     
		     //Table3

		      $query  = "CREATE TABLE `hourly_templates` (
						  `id` int(11) NOT NULL,
						  `hourly_grades` varchar(25) NOT NULL,
						  `slug` varchar(55) NOT NULL,
						  `hourly_rate` decimal(10,2) NOT NULL,
						  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
						  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
						)";  
		                 DB::statement($query);
 
		     $query  = "ALTER TABLE `hourly_templates` ADD PRIMARY KEY (`id`)";  
		                 DB::statement($query);

		    $query =   "ALTER TABLE `hourly_templates` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT";       
		               DB::statement($query);

		    
		    

		    //Table4
		      $query  = "CREATE TABLE `salarytemplate_allowances` (
						  `id` bigint(20) NOT NULL,
						  `salarytemplate_id` bigint(20) NOT NULL,
						  `allowance_name` varchar(55) NOT NULL,
						  `allowance_value` decimal(10,2) NOT NULL
						)";  
		                 DB::statement($query);

		      $query  = "ALTER TABLE `salarytemplate_allowances` ADD PRIMARY KEY (`id`)";  
		                 DB::statement($query);


		    $query =  "ALTER TABLE `salarytemplate_allowances` MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16";       DB::statement($query);
						
              
              //Table 5
		      $query  = "CREATE TABLE `salarytemplate_deductions` (
						  `id` bigint(20) NOT NULL,
						  `salarytemplate_id` bigint(20) NOT NULL,
						  `deduction_name` varchar(55) NOT NULL,
						  `deduction_value` decimal(10,2) NOT NULL
						)";  
		                 DB::statement($query);

		      $query  = "ALTER TABLE `salarytemplate_deductions` ADD PRIMARY KEY (`id`)";  
		                 DB::statement($query);

		    $query  = "ALTER TABLE `salarytemplate_deductions` MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12";  
		                 DB::statement($query); 


		       //Table6
		       
		      $query  = "CREATE TABLE `salary_payment_history` (
						  `id` bigint(20) NOT NULL,
						  `user_id` bigint(20) NOT NULL,
						  `salary_month` varchar(15) NOT NULL,
						  `payment_date` date NOT NULL,
						  `user_salary_template_id` bigint(20) NOT NULL,
						  `total_hours` tinyint(5) NOT NULL DEFAULT '0',
						  `payment_amount` decimal(10,2) NOT NULL,
						  `payment_type` enum('Cash','Cheque') NOT NULL,
						  `comments` varchar(512) DEFAULT NULL,
						  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
						  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
						)";  
		                 DB::statement($query);

		       $query  = "ALTER TABLE `salary_payment_history` ADD PRIMARY KEY (`id`)";  
		                 DB::statement($query);

		    $query  = "ALTER TABLE `salary_payment_history` MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT ";  
		                 DB::statement($query); 


		        //Table7
		       
		      $query  = "CREATE TABLE `salary_templates` (
						  `id` bigint(20) NOT NULL,
						  `salary_grades` varchar(25) NOT NULL,
						  `slug` varchar(50) NOT NULL,
						  `basic_salary` decimal(10,2) NOT NULL,
						  `overtime_rate` decimal(10,2) NOT NULL,
						  `gross_salary` decimal(10,2) NOT NULL,
						  `total_deduction` decimal(10,2) NOT NULL,
						  `net_salary` decimal(10,2) NOT NULL,
						  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
						  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
						)";  
		                 DB::statement($query);

		       $query  = "ALTER TABLE `salary_templates` ADD PRIMARY KEY (`id`)";  
		                 DB::statement($query);

		    $query  = "ALTER TABLE `salary_templates` MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6 ";  
		                 DB::statement($query);  
		                 

		       //Table8
		       
		      $query  = "CREATE TABLE `salay_payments` (
						  `id` bigint(20) UNSIGNED NOT NULL,
						  `user_id` bigint(20) NOT NULL,
						  `role_id` int(10) DEFAULT NULL,
						  `employee_id` varchar(20) DEFAULT NULL,
						  `template_id` int(10) DEFAULT NULL,
						  `net_salary` decimal(10,0) NOT NULL DEFAULT '0',
						  `paid_amount` decimal(10,0) NOT NULL DEFAULT '0',
						  `payment_method` varchar(20) DEFAULT NULL,
						  `month` date DEFAULT NULL,
						  `comments` text,
						  `updated_by` int(10) DEFAULT NULL,
						  `expense_category_id` int(10) DEFAULT NULL,
						  `expense_id` bigint(20) UNSIGNED DEFAULT NULL,
						  `created_at` timestamp NULL DEFAULT NULL,
						  `updated_at` timestamp NULL DEFAULT NULL
						)";  
		                 DB::statement($query);

		       $query  = "ALTER TABLE `salay_payments`
						  ADD PRIMARY KEY (`id`),
						  ADD KEY `user_id` (`user_id`),
						  ADD KEY `expense_id` (`expense_id`)";  
		                 DB::statement($query);

		         $query  = "ALTER TABLE `salay_payments` MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12 ";  
		                 DB::statement($query);

		          //Table9
		       
		      $query  = "CREATE TABLE `user_salary_templates` (
						  `id` bigint(20) NOT NULL,
						  `role_id` tinyint(5) NOT NULL,
						  `user_id` bigint(20) NOT NULL,
						  `salary_type` varchar(25) NOT NULL,
						  `template_id` bigint(20) NOT NULL,
						  `template_name` varchar(25) NOT NULL,
						  `created_at` int(11) NOT NULL,
						  `updated_at` int(11) NOT NULL
						)";  
		                 DB::statement($query);

		      $query  = "ALTER TABLE `user_salary_templates` ADD PRIMARY KEY (`id`)";  
		                 DB::statement($query);

		    $query  = "ALTER TABLE `user_salary_templates` MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12";  
		                 DB::statement($query);                                  

           
           DB::commit();
           flash('success','payroll_plugin_added_successfully', 'overlay');
      }

       catch ( Exception $e ) {

            DB::rollBack();
              // dd($e->getMessage());
            flash('success','payroll_plugin_added_successfully', 'overlay');
             
        }

        return redirect( URL_USERS_LOGIN );
  }

}