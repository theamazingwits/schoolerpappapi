<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use App\Http\Requests;
use App\User;
use App\AlumniDonation;
use Yajra\Datatables\Datatables;
use DB;
use Image;
use File;
use Auth;

class AlumniDonationController extends Controller
{
    
    public function __construct()
    {
    	$this->middleware('auth');
    }

    /**
     * Course listing method
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        // dd('herere');
      if(!checkRole(getUserGrade(23)))
      {
        prepareBlockUserMessage();
        return back();
      }
        $data['active_class']       = 'alumni';
        if(Auth::user()->role_id == 15)
        $data['active_class']       = 'donations';
        $data['layout']             = getLayout();
        $data['title']              = getPhrase('donations');

        return view('alumini.donations.list', $data);

          
    }

    /**
     * This method returns the datatables data to view
     * @return [type] [description]
     */
    public function getDatatable($slug = '')
    {

      if(!checkRole(getUserGrade(23)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $records = array();
 
      
        $records = AlumniDonation::select(['title','amount','date','status','id']);
    
            
        $records->orderBy('updated_at', 'desc');
             

        return Datatables::of($records)
        ->addColumn('action', function ($records) {
         
          $link_data = '<div class="dropdown more">
                        <a id="dLabel" type="button" class="more-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-ellipsis-v"></i>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <li><a href="'.URL_ALUMNI_DONATIONS_EDIT.$records->id.'"><i class="fa fa-pencil"></i>'.getPhrase("edit").'</a></li>';
                            
                           $temp = '';
                           if(checkRole(getUserGrade(23))) {
                    $temp .= ' <li><a href="javascript:void(0);" onclick="deleteRecord(\''.$records->id.'\');"><i class="fa fa-trash"></i>'. getPhrase("delete").'</a></li>';
                      }
                    
                    $temp .='</ul></div>';

                    $link_data .=$temp;
            return $link_data;
            })

        ->editColumn('status',function($records){
          
             if($records->status == 1)
              return '<span class="label label-success">Active</span>';
            return '<span class="label label-info">In Active</span>';
        })

        ->removeColumn('id')
        ->make();
    }

    /**
     * This method loads the create view
     * @return void
     */
    public function create()
    {
      if(!checkRole(getUserGrade(23)))
      {
        prepareBlockUserMessage();
        return back();
      }
        $data['record']       = FALSE;
        $data['active_class'] = 'alumni';
        if(Auth::user()->role_id == 15)
        $data['active_class']       = 'donations';
        $data['layout']       = getLayout();
        $data['title']        = getPhrase('add_donation');
       
        return view('alumini.donations.add-edit', $data);

       
    }

    /**
     * This method loads the edit view based on unique slug provided by user
     * @param  [string] $slug [unique slug of the record]
     * @return [view with record]       
     */
    public function edit($id)
    {
      if(!checkRole(getUserGrade(23)))
      {
        prepareBlockUserMessage();
        return back();
      }
    
    // dd($id);
        $record = AlumniDonation::where('id',$id)->first();

        if($isValid = $this->isValidRecord($record))
            return redirect($isValid);

        $data['record']       = $record;
        $data['active_class'] = 'alumni';
        if(Auth::user()->role_id == 15)
        $data['active_class']       = 'donations';
        $data['layout']       = getLayout();
        $data['title']        = getPhrase('edit_donation');

         return view('alumini.donations.add-edit', $data);

        
    }

    
    /**
     * [update description]
     * @param  Request $request [description]
     * @param  [type]  $slug    [description]
     * @return [type]           [description]
     */
    public function update(Request $request, $id)
    {
        // dd($request);
      if(!checkRole(getUserGrade(23)))
      {
        prepareBlockUserMessage();
        return back();
      }

         $rules = [
              
              'title'      => 'bail|required',
              'date' => 'bail|required',
                'amount' => 'bail|required',
            ];

       
        $this->validate($request, $rules);

        $record = AlumniDonation::where('id',$id)->first();

        if($isValid = $this->isValidRecord($record))
            return redirect($isValid);


        $record->update($request->all());

         $this->processUpload($request, $record,'catimage');

        flash('success','notice_updated_successfully', 'success');

        return redirect(URL_ALUMNI_DONATIONS);
    }

    /**
     * This method adds record to DB
     * @param  Request $request [Request Object]
     * @return void
     */
    public function store(Request $request)
    {
      if(!checkRole(getUserGrade(23)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $rules = [
              
              'title'      => 'bail|required',
              'date' => 'bail|required',
              'amount' => 'bail|required',
             
            ];

      
        $this->validate($request, $rules);

        $record  =  AlumniDonation::create($request->all());

        $this->processUpload($request, $record,'catimage');

        flash('success','notice_added_successfully', 'success');

        return redirect(URL_ALUMNI_DONATIONS);
    }

     public function processUpload(Request $request, $record, $file_name)
     {
        if(env('DEMO_MODE')) {
            return;
        }

        
         if ($request->hasFile($file_name)) {

        
          $destinationPath  = "public/uploads/alumni/donations";
          
          $fileName = ucwords($record->slug).'-'.$request->$file_name->getClientOriginalName();
          
          $request->file($file_name)->move($destinationPath, $fileName);

          $record->image  =  $fileName;

          $record->save();
         
      
        }
     }
 
    /**
     * Delete Record based on the provided slug
     * @param  [string] $slug [unique slug]
     * @return Boolean 
     */
    public function delete($slug)
    {
      if(!checkRole(getUserGrade(23)))
      {
        prepareBlockUserMessage();
        return back();
      }
      /**
       * Delete the questions associated with this quiz first
       * Delete the quiz
       * @var [type]
       */
         $record = AlumniDonation::where('id',$slug)->first();
        try{
            if(!env('DEMO_MODE')) {
                $record->delete();
            }
            $response['status'] = 1;
            $response['message'] = getPhrase('record_deleted_successfully');
        }
         catch ( Exception $e) {
                 $response['status'] = 0;
           if(getSetting('show_foreign_key_constraint','module'))
            $response['message'] =  $e->getMessage();
           else
            $response['message'] =  getPhrase('this_record_is_in_use_in_other_modules');
       }
        return json_encode($response);
    }

    public function isValidRecord($record)
    {
        if ($record === null) {

            flash('Ooops...!', getPhrase("page_not_found"), 'error');
            return $this->getRedirectUrl();
        }

        return FALSE;
    }

    public function getReturnUrl()
    {
        return URL_ALUMNI_DONATIONS;
    }

    


  

}