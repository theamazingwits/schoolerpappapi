<?php

namespace App\Http\Controllers;
use \App;
use Illuminate\Http\Request;
use App\Http\Requests;
use Yajra\Datatables\Datatables;
use DB;
use App\User;
use App\Staff;
use App\UserSalaryGrade;
use App\SalaryPayments;
use App\SalaryTemplate;
use App\Expense;
use Auth;
use Exception;

class PaySalaryController extends Controller{
   
   public function __construct()
   {
   	   $this->middleware('auth');
   }


   /**
    * Select the user type to pay the salary
    * @return [type] [description]
    */
   public function index()
   {
   	  
   	   if(!checkRole(getUserGrade(18)))
        {
          prepareBlockUserMessage();
          return back();
        }

       
        
        // $data['branches']            = addSelectToList(getBranches());
        $others                      = array('2','3','7','8','9','10');
        $data['roles']               = App\Role::whereIn('id', $others)->pluck('display_name','id');
        $data['current_academic_id'] = getDefaultAcademicId();
        $data['layout']              = getLayout();
        $data['active_class']        = 'payroll';
        $data['title']               = getPhrase('make_payment');
        $data['module_helper']       = getModuleHelper('payroll');
        // dd($data);

        // $view_name = getTheme().'::salary.select-particulars';
        // return view($view_name,$data);

          return view('salary.select-particulars', $data);
   }

   /**
    * Get Staff as per section details
    * @param  Request $request [description]
    * @return [type]           [description]
    */
   public function viewStaff(Request $request)
   {
   	   

   	   if(!checkRole(getUserGrade(18)))
        {
          prepareBlockUserMessage();
          return back();
        }
 
   	   // $branch_id          = $request->branch_id;
       $course_parent_id   = 0;
       if($request->has('course_parent_id'))
       $course_parent_id   = $request->course_parent_id;

       $role_id            = $request->role_id;

        if( $request->role_id=='')
        {

            flash('Oops...!','Please Select The Details', 'overlay');
            return redirect()->back()->withInput($request->except('_token'));

        }

        // $data['branch_id']        = $branch_id;
        $data['role_id']          = $role_id;
        $data['course_parent_id'] = $course_parent_id;
        $data['title']            = 'Pay Salary';
        $data['role_name']        = ucwords(getRoleData($role_id));
        $data['active_class']     = 'payroll';
        $data['layout']           = getLayout();

        //    $view_name = getTheme().'::salary.list';
        // return view($view_name,$data);

         return view('salary.list', $data);
  

   }



    /**
     * This method returns the datatables data to view
     * @return [type] [description]
     */
    public function getDatatable($role_id, $course_parent_id)
    {

         if( $course_parent_id > 0 ){
               
               $records = User::join('staff','staff.user_id','=','users.id')
                              // ->where('staff.branch_id',$branch_id)
                              ->where('course_parent_id',$course_parent_id)
                              ->select(['users.id','image','name','employee_id','email','slug'])
                              ->get();
               
             
         }
         else{

             $records   = User::where('role_id', $role_id)
                              ->select(['users.id','image','name','employee_id','email','slug'])
                              ->get();
         }
        
        return Datatables::of($records)
        ->addColumn('action', function ($records) {
           
           $grade_assigned  = UserSalaryGrade::getUserTemplate($records->id);
           $montly_paid     = SalaryPayments::MonthPaid($records->id);

           if($grade_assigned){
                 
             if(count($montly_paid) <= 0){

                     return '<a class="btn btn-primary btn-sm" href="'.URL_VIEW_USER_PAYS.$records->slug.'" target ="_blank">'.getPhrase('pay_salary').'</a>';

                 }else{

                      return '<h5 class="label label-info">'.getPhrase('payment_done').'</h5>&nbsp;&nbsp;&nbsp;<a class="btn btn-primary btn-sm" href="'.URL_VIEW_USER_PAYS.$records->slug.'" target ="_blank">'.getPhrase('details').'</a>';
                 }


              }
              else{

              	return '<a href="'.URL_PAYROLL_MANAGE_SALARY_ADD.'"><h5 class="label label-warning">'.getPhrase('please_assign_grade').'</h5></a>';
              }

            })
         ->editColumn('image', function($records){
            return '<img src="'.getProfilePath($records->image).'"  height="50px" width="50px"/>';
        })


        ->removeColumn('slug')
        ->removeColumn('id')
        ->make();
    }

     /**
      * View users pay history and add new pay
      * @param  [type] $slug [description]
      * @return [type]       [description]
      */
    public function payToStaff($slug)
    {


    	 if(!checkRole(getUserGrade(18)))
        {
          prepareBlockUserMessage();
          return back();
        }
        
        $user   = getUserWithSlug($slug);
        
        $data['record']           = $user;
        $data['payment_methods']  = array('cash'=>'Cash', 'online'=>'Online', 'other' =>'Other');
        $data['expense_categories'] = App\ExpenseCategory::orderBy('category_name', 'asc')
                                                          ->pluck('category_name', 'id');
        $data['template']         = UserSalaryGrade::getUserTemplate($user->id);
        $data['title']            = "Pay salary to ". ucwords($user->name);
        $data['role_name']        = ucwords(getRoleData($user->role_id));
        $data['active_class']     = 'payroll';
        $data['layout']           = getLayout();

        //    $view_name = getTheme().'::salary.user-pay';
        // return view($view_name,$data);

         return view('salary.user-pay', $data);


    }


    /**
     * Add payment to satff and expense
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function paySalary(Request $request)
    {
    	
    	SalaryPayments::addSalaryRecord($request);

    	flash('success','payment_added_successfully','success');
    	return redirect(URL_PAY_SALARY);

    }

    public function staffSalaryHistory($user_id)
    {
    	

    	  $records   = SalaryPayments::where('user_id', $user_id)
                              ->select(['month','net_salary','paid_amount','id'])
                              ->get();
         
        
        return Datatables::of($records)

        ->addColumn('action', function ($records) {
           
             return '<a href="'.URL_VIEW_SALARY_SLIP.$records->id.'" data-toggle="tooltip" data-placement="auto" title="'.getPhrase("view").'" class="btn btn-sm btn-icon btn-info"><i class="fa fa-eye" aria-hidden="true"></i></a>
                  
                  <a href="javascript:void(0);" onclick="deleteRecord(\''.$records->id.'\');" data-toggle="tooltip" data-placement="auto" title="'.getPhrase("delete").'" class="btn btn-sm btn-icon btn-warning"><i class="fa fa-trash" aria-hidden="true"></i></a>
                 ';
         })

          

        ->removeColumn('id')
        ->make();
    }

    /**
     * View paid salary details
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function details($id)
    {
      

       if(!checkRole(getUserGrade(18)))
        {
          prepareBlockUserMessage();
          return back();
        }
        
        $record   = SalaryPayments::find($id);
        $template = App\SalaryTemplate::find($record->template_id);
        $user     = getUserRecord($record->user_id);

        $total_allowance  = $template->totalAllownses();
        $total_deductions  = $template->totalDeductions();
        
        $data['record']           = $record;
        $data['user']             = $user;
        $data['template']         = $template;
        $data['total_allowance']  = $total_allowance;
        $data['total_deductions'] = $total_deductions;
        $data['role_name']        = ucwords(getRoleData($user->role_id));
        $name                     = ucwords($user->name).' - '.getRole($user->id).' - '.$record->month.' Payment Details';
        $data['title']            = $name;
        $data['active_class']     = 'payroll';
        $data['layout']           = getLayout();

        //    $view_name = getTheme().'::salary.user-pay-detials';
        // return view($view_name,$data);

         return view('salary.user-pay-detials', $data);
    }


    /**
     * Delete the enetred salary record
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function delete($id)
    { 

    	try{

            $record         = SalaryPayments::find($id);
            $expense_record = Expense::find($record->expense_id);

            $record->delete();
            $expense_record->delete();

            $record->flag        = 'Delete';
            $record->action      = 'Salay_payments';
            $record->object_id   =  $id;
            $logs = new LogHelper();
            $logs->storeLogs($record);

            $expense_record->flag        = 'Delete';
            $expense_record->action      = 'Expenses';
            $expense_record->object_id   =  $record->expense_id;
            $logs = new LogHelper();
            $logs->storeLogs($expense_record);
          
            $response['status'] = 1;
            $response['message'] = getPhrase('record_deleted_successfully');
        }
        catch (Exception $e) {

            $response['status'] = 0;
           if(getSetting('show_foreign_key_constraint','module'))
            $response['message'] =  $e->getMessage();
           else
            $response['message'] =  getPhrase('this_record_is_in_use_in_other_modules');
       }
       return json_encode($response);
    	
    }

    /**
     * View the salary reports month and date wise
     * [reports description]
     * @return [type] [description]
     */
    public function reports()
    {
         
        if(!checkRole(getUserGrade(18)))
        {
          prepareBlockUserMessage();
          return back();
        }

       
        
        // $data['branches']            = addSelectToList(getBranches());
        $others                      = array('2','3','7','8','9','10');
        $data['roles']               = App\Role::whereIn('id', $others)->pluck('display_name','id');
        $data['current_academic_id'] = getDefaultAcademicId();
        $data['layout']              = getLayout();
        $data['active_class']        = 'payroll';
        $data['title']               = getPhrase('reports');
        $data['module_helper']       = getModuleHelper('payroll');
        // dd($data);

        // $view_name = getTheme().'::salary.reports.select-particulars';
        // return view($view_name,$data);

         return view('salary.reports.select-particulars', $data);
    }

    
    public function getMonthlyReports(Request $request)
    {
       
       // $branch_id   = $request->branch_id;
       $role_id     = $request->role_id;
       $mydate      = $request->mydate;

       $first_day   = date("Y-m-01", strtotime($mydate));

       $records   = SalaryPayments::join('users','salay_payments.user_id','=','users.id')
                                   // ->where('salay_payments.branch_id',$branch_id)
                                   ->where('salay_payments.role_id',$role_id)
                                   ->where('month','>=',$first_day)
                                   ->where('month','<=',$mydate)
                                   ->select(['name','users.employee_id','net_salary','paid_amount','month','payment_method','user_id','salay_payments.id as pay_id'])
                                   ->get();

          return $records;                         
    }

    
    /**
     * Print the salary reports
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function printReports(Request $request)
    {   

        if(!checkRole(getUserGrade(18)))
        {
          prepareBlockUserMessage();
          return back();
        }

        if($request->pay_slip == 'no'){
        
        // $branch_id          = $request->branch_id;
        $role_id            = $request->role_id;
        $pay_month          = $request->pay_month;

        // $branch_name        = getbranchname($branch_id);
        $role_name          = getRoleData($role_id);

        $first_day   = date("Y-m-01", strtotime($pay_month));

        $data['title']      = ucfirst($role_name).' Payments From '.$first_day.' Upto '.$pay_month;

        $records   = SalaryPayments::join('users','salay_payments.user_id','=','users.id')
                                   // ->where('salay_payments.branch_id',$branch_id)
                                   ->where('salay_payments.role_id',$role_id)
                                   ->where('month','>=',$first_day)
                                   ->where('month','<=',$pay_month)
                                   ->select(['name','users.employee_id','net_salary','paid_amount','month','payment_method'])
                                   ->get();
      
        
        $data['records']   = $records;
        
        // $view_name = getTheme().'::salary.reports.print-reports';
        $view_name = "salary.reports.print-reports";

        $view     = \View::make($view_name,$data);
        
        $contents = $view->render();

        return $contents;

        // $html_data = ($contents);

        // echo $html_data;

        // die();
        
        }
        elseif ($request->pay_slip == 'yes') {
           
           $record  = SalaryPayments::find($request->salary_payid);
           $user    = getUserRecord($record->user_id);

           $template        = SalaryTemplate::where('id',$record->template_id)->first();
           // dd($template);
           $data['allowances']      = $template->Particulars('allowances');
           $data['deductions']      = $template->Particulars('deductions');
           // dd($data['deductions']);

           $month = date("m", strtotime($record->month));
           $year  = date("Y", strtotime($record->month));

           $data['record']     = $record;
           $data['template']   = $template;
           $data['user']       = $user;
           $data['role_name']  = ucfirst(getRoleData($user->role_id));
           $data['total_days'] = cal_days_in_month(CAL_GREGORIAN, $month, $year);
           $data['title']      = date("F Y", strtotime($record->month));
           $data['total_allowance']  = $template->totalAllownses();
           $data['total_deductions'] = $template->totalDeductions();

           if($record->net_salary != $record->paid_amount){

            $data['other']  = $record->net_salary - $record->paid_amount;
           }

           // $view_name = getTheme().'::salary.reports.pay-slip';
           // return view($view_name,$data);

           return view('salary.reports.pay-slip', $data);



        }


    }


    public function staffDetails($slug)
    {  


        $user   = getUserWithSlug($slug);
        
        $data['record']           = $user;
       
        $data['template']         = UserSalaryGrade::getUserTemplate($user->id);
        $data['title']            = ucwords($user->name).' '.getPhrase('salary_detials');
        $data['role_name']        = ucwords(getRoleData($user->role_id));
        $data['active_class']     = 'users';
        $data['layout']           = getLayout();

   

         return view('salary.staff-details', $data);
       
    }


     public function SalaryHistory($user_id)
    {
      

        $records   = SalaryPayments::where('user_id', $user_id)
                              ->select(['month','net_salary','paid_amount','id'])
                              ->get();
         
        
        return Datatables::of($records)

        ->addColumn('action', function ($records) {
           
             return '<a href="'.URL_VIEW_SALARY_SLIP.$records->id.'" data-toggle="tooltip" data-placement="auto" title="'.getPhrase("view").'" class="btn btn-sm btn-icon btn-info"><i class="fa fa-eye" aria-hidden="true"></i></a>&nbsp;&nbsp;&nbsp;
             <a href="'.URL_STAFF_PAY_SLIP.$records->id.'" class="btn btn-primary btn-sm" target="_blank">Pay Slip</a>';
         })

          

        ->removeColumn('id')
        ->make();
    }


    public function staffPaySlip($pay_id)
    {
      
           $record  = SalaryPayments::find($pay_id);
           $user    = getUserRecord($record->user_id);

           $template        = SalaryTemplate::where('id',$record->template_id)->first();
           // dd($template);
           $data['allowances']      = $template->Particulars('allowances');
           $data['deductions']      = $template->Particulars('deductions');
           // dd($data['deductions']);

           $month = date("m", strtotime($record->month));
           $year  = date("Y", strtotime($record->month));

           $data['record']     = $record;
           $data['template']   = $template;
           $data['user']       = $user;
           $data['role_name']  = ucfirst(getRoleData($user->role_id));
           $data['total_days'] = cal_days_in_month(CAL_GREGORIAN, $month, $year);
           $data['title']      = date("F Y", strtotime($record->month));
           $data['total_allowance']  = $template->totalAllownses();
           $data['total_deductions'] = $template->totalDeductions();

           if($record->net_salary != $record->paid_amount){

            $data['other']  = $record->net_salary - $record->paid_amount;
           }

           // $view_name = getTheme().'::salary.reports.pay-slip';
           // return view($view_name,$data);

           return view('salary.reports.pay-slip', $data);
    }

}
