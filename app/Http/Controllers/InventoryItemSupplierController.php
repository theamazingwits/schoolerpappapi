<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use App\Http\Requests;
use App\InventoryItemSupplier;
use App\LogHelper;
use Yajra\Datatables\Datatables;
use DB;
use Auth;
use Exception;

class InventoryItemSupplierController extends Controller
{
        
  public function __construct()
    {
      $this->middleware('auth');
    }
    /**
     * Course listing method
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {

      if(!checkRole(getUserGrade(17)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $data['active_class']       = 'inventory';
        $data['layout']             = getLayout();
        $data['title']         = getPhrase('inventory_item_supplier');
        return view('inventory.itemssupplier.list', $data);
    }

    /**
     * This method returns the datatables data to view
     * @return [type] [description]
     */
    public function getDatatable($slug = '')
    {

      if(!checkRole(getUserGrade(17)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $records = array();

        
         $records = InventoryItemSupplier::select(['name','phone', 'email', 'address' ,'slug','updated_at'])
            ->orderBy('updated_at', 'desc');
             

        return Datatables::of($records)
        ->addColumn('action', function ($records) {
         
          $link_data = '<div class="dropdown more">
                        <a id="dLabel" type="button" class="more-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-ellipsis-v"></i>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                           <li><a href="'.URL_INVENTORY_ITEMS_SUPPLIER_EDIT.$records->slug.'"><i class="fa fa-pencil"></i>'.getPhrase("edit").'</a></li>';
                            
                           $temp = '';
                           if(checkRole(getUserGrade(2))) {
                    $temp .= ' <li><a href="javascript:void(0);" onclick="deleteRecord(\''.$records->slug.'\');"><i class="fa fa-trash"></i>'. getPhrase("delete").'</a></li>';
                      }
                    
                    $temp .='</ul></div>';


                    $link_data .=$temp;
            return $link_data;
            })
        
        ->removeColumn('id')
        ->removeColumn('slug')
        ->removeColumn('updated_at')
         
        ->make();
    }

   /**
     * This method loads the create view
     * @return void
     */

     public function create()
    {
    
     
      if(!checkRole(getUserGrade(17)))
      {
        prepareBlockUserMessage();
        return back();
      }
      $data['record']             = FALSE;
      $data['layout']             = getLayout();
      $data['active_class']       = 'inventory';
      $data['title']              = getPhrase('items_supplier');
      return view('inventory.itemssupplier.add-edit', $data);
    }

    
     /**
     * This method adds record to DB
     * @param  Request $request [Request Object]
     * @return void
     */


    public function store(Request $request)
    {
      if(!checkRole(getUserGrade(17)))
      {
        prepareBlockUserMessage();
        return back();
      }

      $rules = [
         'name'               => 'bail|required|max:40' ,
         'phone'               => 'bail|required|min:10|max:10',
         'email'               => 'bail|required|min:4|max:40',
         'description'         => 'bail|required' ];
        $this->validate($request, $rules);
        $record = new InventoryItemSupplier();
        $name                 =  $request->name;
        $record->name         = $name;

        $phone                 =  $request->phone;
        $record->phone         = $phone;

        $email                 =  $request->email;
        $record->email         = $email;

        $record->slug         = $record->makeSlug($name);
        
        $record->address  = $request->address;
        $record->contact_person_number  = $request->contact_person_number;
        $record->contact_person_email  = $request->contact_person_email;
        $record->contact_person_name  = $request->contact_person_name;
        $record->description  = $request->description;
        $record->save();


        $record->flag       = 'Insert';
        $record->action      = 'Inventory_items_suppliers';
        $record->object_id   =  $record->slug;
        $logs = new LogHelper();
        $logs->storeLogs($record);
       
        flash('success','record_added_successfully', 'success');
      
      return redirect(URL_INVENTORY_ITEMS_SUPPLIER);
    }

    /**
     * This method loads the edit view based on unique slug provided by user
     * @param  [string] $slug [unique slug of the record]
     * @return [view with record]       
     */
    public function edit($slug)
    {
      if(!checkRole(getUserGrade(17)))
      {
        prepareBlockUserMessage();
        return back();
      }

      $record = InventoryItemSupplier::getRecordWithSlug($slug);
      if($isValid = $this->isValidRecord($record))
        return redirect($isValid);

      $data['record']             = $record;
      $data['active_class']       = 'inventory';
      $data['layout']             = getLayout();
      $data['title']         	  = getPhrase('edit_itemssupplier');
      return view('inventory.itemssupplier.add-edit', $data);
    }

    /**
     * Update record based on slug and reuqest
     * @param  Request $request [Request Object]
     * @param  [type]  $slug    [Unique Slug]
     * @return void
     */
    public function update(Request $request, $slug)
    {
      if(!checkRole(getUserGrade(17)))
      {
        prepareBlockUserMessage();
        return back();
      }

      $record = InventoryItemSupplier::getRecordWithSlug($slug);
      	 $rules = [
         'name'               => 'bail|required|max:40' ,
         'phone'               => 'bail|required|min:10|max:10',
         'email'               => 'bail|required|min:4|max:40',
         'description'         => 'bail|required' ];
 		 $this->validate($request, $rules);
         /**
        * Check if the title of the record is changed, 
        * if changed update the slug value based on the new title
        */
       $name 				= $request->name;
       $phone 				= $request->phone;
       $email 				= $request->email;
       $address 			= $request->address;
       $contact_person_number 		= $request->contact_person_number;
       $contact_person_email 	= $request->contact_person_email;
       $contact_person_name 	= $request->contact_person_name;
        
        if($name != $record->name)
            $record->slug = $record->makeSlug($name);
      
       //Validate the overall request
       $this->validate($request, $rules);
    
        $record->name            	= $name;
        $record->phone   			= $phone;
        $record->email   			= $email;
   	    $record->address   			= $address;
        $record->contact_person_number  	= $contact_person_number;
        $record->contact_person_email = $contact_person_email;
        $record->contact_person_name  = $contact_person_name;

        $record->description  = $request->description;
     
        $record->save();

        $record->flag       = 'Update';
        $record->action      = 'Inventory_items_suppliers';
        $record->object_id   =  $record->slug;
        $logs = new LogHelper();
        $logs->storeLogs($record);

        flash('success','record_updated_successfully', 'success');
      return redirect(URL_INVENTORY_ITEMS_SUPPLIER);
    }

     /**
     * Delete Record based on the provided slug
     * @param  [string] $slug [unique slug]
     * @return Boolean 
     */
    public function delete($slug)

    {
      if(!checkRole(getUserGrade(17)))
      {
        prepareBlockUserMessage();
        return back();
      }
      /**
       * Check if any quizzes are associated with this instructions page, 
       * if not delete
       * @var [type]
       */
        $record = InventoryItemSupplier::where('slug', $slug)->first();
        $response = [];
       try {
         if(!env('DEMO_MODE')) {
          $record->delete();
          }

          $record->flag        = 'Delete';
          $record->action      = 'Inventory_items_suppliers';
          $record->object_id   =  $slug;
          $logs = new LogHelper();
          $logs->storeLogs($record);

          $response['status'] = 1;
          $response['message'] = getPhrase('record_deleted_successfully');
      }
       catch ( \Illuminate\Database\QueryException $e) {
                 $response['status'] = 0;
           if(getSetting('show_foreign_key_constraint','module'))
            $response['message'] =  $e->errorInfo;
           else
            $response['message'] =  getPhrase('this_record_is_in_use_in_other_modules');
       }
        return json_encode($response);
    }


    public function isValidRecord($record)
    {
      if ($record === null) {

        flash('Ooops...!', getPhrase("page_not_found"), 'error');
        return $this->getRedirectUrl();
    }

    return FALSE;
    }

    public function getReturnUrl()
    {
      return URL_INVENTORY_ITEMS_SUPPLIER;
    }


    
}