<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use \App;
use App\Http\Requests;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Schema;
use DB;
use Exception;
class UpdatesController extends Controller
{
     public function __construct()
    {
     
         $this->middleware('auth');
    
    }

    /**
     * This is the first patch which updates the currency code to
     * Site Settings module
     * This can be used by the existing users
     * To access this method, user need to type the following url
     * http://sitename/updates/patch1
     * @return [type] [description]
     */
    public function patch1()
    {

      if(!checkRole(getUserGrade(1)))
      {
        prepareBlockUserMessage();
        return back();
      }
     // For Bonafide Certificate
     
      $bonafide        = App\Settings::where('slug', 'bonafide-settings')->first();
       
      $uptained_data = (array) json_decode($bonafide->settings_data);
        
       unset($uptained_data['orientation'],$uptained_data['margin'],$uptained_data['format'],$uptained_data['printable_file']);

       $changed_data =  $uptained_data;

       $bonafide->settings_data = json_encode($changed_data);
     
       $bonafide->save();
       
       //For Id Card
      
       $idcard        = App\Settings::where('slug', 'id-card-settings')->first();
       
       $idcard_setting_data = (array) json_decode($idcard->settings_data);
        
       unset($idcard_setting_data['orientation'],$idcard_setting_data['margin'],$idcard_setting_data['format'],$idcard_setting_data['printable_file']);

       $final_id_data =  $idcard_setting_data;

       $idcard->settings_data = json_encode($final_id_data);
     
       $idcard->save();

       //For Transfer Certificate

        $tc_data        = App\Settings::where('slug', 'transfer-certificate-settings')->first();

       
       $tc_setting_data = (array) json_decode($tc_data->settings_data);
        
       unset($tc_setting_data['orientation'],$tc_setting_data['margin'],$tc_setting_data['format'],$tc_setting_data['printable_file']);

       $final_tc_data =  $tc_setting_data;

       $tc_data->settings_data = json_encode($tc_setting_data);
     
       $tc_data->save();

       //For Timetable
       
       $timetable_data  = App\Settings::where('slug', 'timetable-settings')->first();
       if($timetable_data!=''){
       $timetable_data->delete();
       }


       flash('success','system_upgraded_successfully', 'success');
       return redirect(URL_MASTERSETTINGS_SETTINGS);
    }


    public function patch2()
    {

       if(!checkRole(getUserGrade(1)))
      {
        prepareBlockUserMessage();
        return back();
      }  

       DB::beginTransaction();
      try{
         
        $query =   "CREATE TABLE `feecategories` (
                    `id` bigint(20) UNSIGNED NOT NULL,
                    `title` varchar(255) NOT NULL,
                    `slug` varchar(255) NOT NULL,
                    `description` text NOT NULL,
                    `status` tinyint(2) NOT NULL DEFAULT '1',
                    `academic_id` bigint(20) UNSIGNED NOT NULL,
                    `course_parent_id` bigint(20) UNSIGNED NOT NULL,
                    `course_id` bigint(20) UNSIGNED NOT NULL,
                    `year` int(20) NOT NULL,
                    `semister` int(20) NOT NULL,
                    `total_fee` decimal(10,2) NOT NULL DEFAULT '0.00',
                    `total_installments` int(10) NOT NULL DEFAULT '1',
                    `installment_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
                    `other_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
                    `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
                  )";

        DB::statement($query);
        
        $query1  = "CREATE TABLE `feecategory_particulars` (
                      `id` bigint(20) UNSIGNED NOT NULL,
                      `feecategory_id` bigint(20) UNSIGNED NOT NULL,
                      `particular_id` bigint(20) UNSIGNED NOT NULL,
                      `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
                      `is_refundable` tinyint(2) NOT NULL DEFAULT '0',
                      `is_term_applicable` tinyint(2) NOT NULL DEFAULT '0',
                      `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                      `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
                    )"; 

        DB::statement($query1);
        
        $query2    = "CREATE TABLE `feeparticular_paymets` (
                        `id` bigint(20) UNSIGNED NOT NULL,
                        `feepayment_id` bigint(20) UNSIGNED NOT NULL,
                        `feecategory_id` bigint(20) UNSIGNED NOT NULL,
                        `feeparticular_id` bigint(20) UNSIGNED NOT NULL,
                        `feecategory_particular_id` bigint(20) UNSIGNED NOT NULL,
                        `is_schedule` tinyint(2) NOT NULL DEFAULT '0',
                        `feeschedule_particular_id` bigint(20) UNSIGNED DEFAULT NULL,
                        `feeschedule_id` bigint(20) UNSIGNED DEFAULT NULL,
                        `paid_amount` decimal(10,2) NOT NULL,
                        `amount` decimal(10,2) NOT NULL,
                        `balance` decimal(10,2) NOT NULL DEFAULT '0.00',
                        `discount` decimal(10,2) NOT NULL DEFAULT '0.00',
                        `net_balance` decimal(10,2) NOT NULL DEFAULT '0.00',
                        `paid_percentage` decimal(10,2) NOT NULL DEFAULT '0.00',
                        `term_number` int(10) DEFAULT NULL,
                        `carry_forward` tinyint(2) NOT NULL DEFAULT '1',
                        `previous_feecategory_id` bigint(20) UNSIGNED NOT NULL,
                        `notes` text NOT NULL,
                        `comments` text NOT NULL,
                        `user_id` bigint(20) UNSIGNED NOT NULL,
                        `student_id` bigint(20) UNSIGNED NOT NULL,
                        `payment_received_by` bigint(20) UNSIGNED NOT NULL,
                        `academic_id` bigint(20) UNSIGNED NOT NULL,
                        `course_parent_id` bigint(20) UNSIGNED NOT NULL,
                        `course_id` bigint(20) UNSIGNED NOT NULL,
                        `year` int(10) NOT NULL DEFAULT '1',
                        `semister` int(10) NOT NULL DEFAULT '0',
                        `received_on` timestamp NULL DEFAULT NULL,
                        `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                        `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
                      )"; 

        DB::statement($query2);
      
         $query3  = "CREATE TABLE `feepayments` (
                        `id` bigint(20) UNSIGNED NOT NULL,
                        `transaction_id` varchar(255) NOT NULL,
                        `academic_id` bigint(20) UNSIGNED NOT NULL,
                        `course_parent_id` bigint(20) UNSIGNED NOT NULL,
                        `course_id` bigint(20) UNSIGNED NOT NULL,
                        `year` int(10) NOT NULL DEFAULT '1',
                        `semister` int(10) NOT NULL DEFAULT '0',
                        `feecategory_id` bigint(20) UNSIGNED NOT NULL,
                        `feecategory_title` varchar(255) NOT NULL,
                        `student_id` bigint(20) UNSIGNED NOT NULL,
                        `user_id` bigint(20) UNSIGNED NOT NULL,
                        `iseligible_for_discount` tinyint(2) NOT NULL DEFAULT '1',
                        `discount_id` bigint(20) UNSIGNED DEFAULT NULL,
                        `discount_title` varchar(255) NOT NULL,
                        `discount_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
                        `ishaving_fine` tinyint(2) NOT NULL DEFAULT '1',
                        `fine_id` bigint(20) UNSIGNED DEFAULT NULL,
                        `fine_name` varchar(255) NOT NULL,
                        `fine_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
                        `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
                        `balance` decimal(10,2) NOT NULL DEFAULT '0.00',
                        `previous_balance` decimal(10,2) NOT NULL DEFAULT '0.00',
                        `total_amount` decimal(10,2) NOT NULL,
                        `paid_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
                        `is_refundable` tinyint(2) NOT NULL DEFAULT '0',
                        `refunded_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
                        `refund_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                        `refund_description` text NOT NULL,
                        `payment_mode` enum('cash','online','cheque','DD','other','offline') NOT NULL DEFAULT 'cash',
                        `payment_mode_name` varchar(255) NOT NULL,
                        `payment_refrenceno` int(50) NOT NULL,
                        `payment_notes` text,
                        `payment_status` tinyint(2) NOT NULL DEFAULT '1',
                        `recevied_on` date NOT NULL,
                        `payment_recevied_by` bigint(20) UNSIGNED NOT NULL,
                        `any_extra_particular_added` tinyint(2) NOT NULL DEFAULT '0',
                        `any_extra_discount_added` tinyint(2) NOT NULL DEFAULT '0',
                        `will_referto_other` tinyint(2) NOT NULL DEFAULT '0',
                        `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                        `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
                      )";

         DB::statement($query3);
          
         $query30  = "CREATE TABLE `feepayments_transactions` (
                      `id` bigint(20) UNSIGNED NOT NULL,
                      `feepayment_id` bigint(20) UNSIGNED NOT NULL,
                      `receipt_no` varchar(255) NOT NULL,
                      `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
                      `paid_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
                      `transaction_no` int(50) NOT NULL,
                      `payment_mode` enum('cash','online','cheque','DD','other') NOT NULL DEFAULT 'cash',
                      `payment_mode_name` varchar(50) NOT NULL,
                      `payment_refrenceno` int(50) NOT NULL,
                      `payment_recevied_by` bigint(20) UNSIGNED NOT NULL,
                      `payment_notes` text NOT NULL,
                      `recevied_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                      `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                      `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
                    )"; 

         DB::statement($query30);
         
         $query4  = "CREATE TABLE `feepaymets_online` (
                      `id` bigint(20) UNSIGNED NOT NULL,
                      `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
                      `user_id` bigint(20) UNSIGNED NOT NULL,
                      `feecategory_id` bigint(20) UNSIGNED NOT NULL,
                      `plan_type` enum('fee') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'fee',
                      `payment_gateway` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                      `transaction_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                      `paid_by_parent` tinyint(1) NOT NULL DEFAULT '0',
                      `paid_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                      `paid_amount` decimal(10,2) NOT NULL,
                      `payment_status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                      `other_details` text COLLATE utf8_unicode_ci NOT NULL,
                      `transaction_record` text COLLATE utf8_unicode_ci,
                      `notes` text COLLATE utf8_unicode_ci,
                      `admin_comments` text COLLATE utf8_unicode_ci,
                      `created_at` timestamp NULL DEFAULT NULL,
                      `updated_at` timestamp NULL DEFAULT NULL
                    )";

         DB::statement($query4);

         
         $query5  = "CREATE TABLE `feeschedules` (
                      `id` bigint(20) UNSIGNED NOT NULL,
                      `feecategory_id` bigint(20) UNSIGNED NOT NULL,
                      `total_installments` int(10) NOT NULL DEFAULT '1',
                      `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                      `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
                    )";
 
         DB::statement($query5);
         
         $query6   = "CREATE TABLE `feeschedule_particulars` (
                      `id` bigint(20) UNSIGNED NOT NULL,
                      `feecategory_id` bigint(20) UNSIGNED NOT NULL,
                      `feeschedule_id` bigint(20) UNSIGNED NOT NULL,
                      `installment` int(10) NOT NULL DEFAULT '1',
                      `total_installments` int(10) NOT NULL DEFAULT '1',
                      `start_date` text NOT NULL,
                      `end_date` text NOT NULL,
                      `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                      `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
                    )";

         DB::statement($query6);

          $query7  = "CREATE TABLE `feeschedule_payments` (
                      `id` bigint(20) UNSIGNED NOT NULL,
                      `feepayment_id` bigint(20) UNSIGNED NOT NULL,
                      `feecategory_id` bigint(20) UNSIGNED NOT NULL,
                      `feeschedule_id` bigint(20) UNSIGNED NOT NULL,
                      `feeschedule_particular_id` bigint(20) UNSIGNED NOT NULL,
                      `student_id` bigint(20) UNSIGNED NOT NULL,
                      `user_id` bigint(20) UNSIGNED NOT NULL,
                      `payment_recevied_by` bigint(20) UNSIGNED NOT NULL,
                      `academic_id` bigint(20) UNSIGNED NOT NULL,
                      `course_parent_id` bigint(20) UNSIGNED NOT NULL,
                      `course_id` bigint(20) UNSIGNED NOT NULL,
                      `amount` decimal(10,2) NOT NULL,
                      `paid_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
                      `balance` decimal(10,2) NOT NULL DEFAULT '0.00',
                      `net_balance` decimal(10,2) NOT NULL DEFAULT '0.00',
                      `paid_percentage` decimal(10,2) NOT NULL DEFAULT '0.00',
                      `term_number` int(10) NOT NULL,
                      `year` int(10) NOT NULL,
                      `semister` int(10) NOT NULL,
                      `received_on` timestamp NULL DEFAULT NULL,
                      `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                      `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
                    )";

         DB::statement($query7);

          $query8 = "CREATE TABLE `particulars` (
                      `id` bigint(20) UNSIGNED NOT NULL,
                      `title` varchar(255) NOT NULL,
                      `slug` varchar(255) NOT NULL,
                      `description` text NOT NULL,
                      `status` tinyint(2) NOT NULL DEFAULT '1',
                      `is_income` tinyint(2) NOT NULL DEFAULT '1',
                      `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                      `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
                    )";

         DB::statement($query8);
         
         $query9  = "ALTER TABLE `feecategories`
                      ADD PRIMARY KEY (`id`),
                      ADD KEY `academic_id` (`academic_id`,`course_parent_id`,`course_id`)";

         DB::statement($query9);
          
         $query10  = "ALTER TABLE `feecategory_particulars`
                      ADD PRIMARY KEY (`id`),
                      ADD KEY `feecategory_id` (`feecategory_id`,`particular_id`),
                      ADD KEY `particular_id` (`particular_id`)"; 

         DB::statement($query10);
          
         $query11 = "ALTER TABLE `feeparticular_paymets`
                      ADD PRIMARY KEY (`id`),
                      ADD KEY `feecategory_id` (`feecategory_id`,`feeparticular_id`,`feecategory_particular_id`,`feeschedule_particular_id`,`feeschedule_id`,`academic_id`,`course_parent_id`,`course_id`),
                      ADD KEY `user_id` (`user_id`,`student_id`),
                      ADD KEY `feeparticular_id` (`feeparticular_id`),
                      ADD KEY `feecategory_particular_id` (`feecategory_particular_id`),
                      ADD KEY `feeschedule_id` (`feeschedule_id`),
                      ADD KEY `feeschedule_particular_id` (`feeschedule_particular_id`),
                      ADD KEY `student_id` (`student_id`),
                      ADD KEY `academic_id` (`academic_id`),
                      ADD KEY `course_id` (`course_id`),
                      ADD KEY `payement_recevied_by` (`payment_received_by`),
                      ADD KEY `previous_feecategory_id` (`previous_feecategory_id`)"; 

         DB::statement($query11);
          
         $query12  = "ALTER TABLE `feepayments`
                      ADD PRIMARY KEY (`id`),
                      ADD KEY `academic_id` (`academic_id`,`course_parent_id`,`course_id`,`feecategory_id`,`discount_id`,`fine_id`,`payment_recevied_by`),
                      ADD KEY `student_id` (`student_id`),
                      ADD KEY `user_id` (`user_id`)"; 

         DB::statement($query12);

         $query13  = "ALTER TABLE `feepayments_transactions`
                      ADD PRIMARY KEY (`id`),
                      ADD KEY `feepayment_id` (`feepayment_id`,`payment_recevied_by`)";

         DB::statement($query13);
          
          $query14  = "ALTER TABLE `feepaymets_online`
                      ADD PRIMARY KEY (`id`),
                      ADD KEY `user_id` (`user_id`),
                      ADD KEY `feecategory_id` (`feecategory_id`)";

         DB::statement($query14);

          $query15  = "ALTER TABLE `feeschedules`
                      ADD PRIMARY KEY (`id`),
                      ADD KEY `fee_categoryid` (`feecategory_id`)";

         DB::statement($query15);
         
          $query16  = "ALTER TABLE `feeschedule_particulars`
                        ADD PRIMARY KEY (`id`),
                        ADD KEY `feecategory_id` (`feecategory_id`,`feeschedule_id`),
                        ADD KEY `feeschedule_id` (`feeschedule_id`)";

         DB::statement($query16);
          
          $query17   = "ALTER TABLE `feeschedule_payments`
                        ADD PRIMARY KEY (`id`),
                        ADD KEY `feecategory_id` (`feecategory_id`,`feeschedule_id`,`feeschedule_particular_id`,`student_id`,`user_id`,`academic_id`,`course_parent_id`,`course_id`),
                        ADD KEY `academic_id` (`academic_id`),
                        ADD KEY `course_id` (`course_id`),
                        ADD KEY `feeschedule_id` (`feeschedule_id`),
                        ADD KEY `feeschedule_particular_id` (`feeschedule_particular_id`),
                        ADD KEY `student_id` (`student_id`),
                        ADD KEY `user_id` (`user_id`),
                        ADD KEY `payement_recevied_by` (`payment_recevied_by`),
                        ADD KEY `feepayment_id` (`feepayment_id`)";

         DB::statement($query17);
          
         $query18 = "ALTER TABLE `particulars`
  ADD PRIMARY KEY (`id`)"; 

         DB::statement($query18);
          
         $query19  = "ALTER TABLE `feecategories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1"; 

         DB::statement($query19);
          
         $query20  = "ALTER TABLE `feecategory_particulars`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1"; 

         DB::statement($query20);

          $query21  = "ALTER TABLE `feeparticular_paymets`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1";

         DB::statement($query21);

         $query22  = "ALTER TABLE `feepayments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1";
          
         DB::statement($query22);
            
         $query23 = "ALTER TABLE `feepayments_transactions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1";   

         DB::statement($query23);
         
         $query24 = "ALTER TABLE `feepaymets_online`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1";

         DB::statement($query24);

         $query25  = "ALTER TABLE `feeschedules`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1";
          
         DB::statement($query25);
         
         $query26 = "ALTER TABLE `feeschedule_particulars`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1";  

         DB::statement($query26);
         
         $query27  = "ALTER TABLE `feeschedule_payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1";

         DB::statement($query27);
         
         $query28 = "ALTER TABLE `particulars`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1";

         DB::statement($query28);
           
         $query29 = "ALTER TABLE `feecategory_particulars`
  ADD CONSTRAINT `feecategory_particulars_ibfk_1` FOREIGN KEY (`feecategory_id`) REFERENCES `feecategories` (`id`),
  ADD CONSTRAINT `feecategory_particulars_ibfk_2` FOREIGN KEY (`particular_id`) REFERENCES `particulars` (`id`)";  

         DB::statement($query29);

          
         $query31  = "INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(9, 'clerk', 'Clerk', 'Clerk', NULL, NULL)"; 
         
         DB::insert($query31);
         
         $query33   = "ALTER TABLE `students` ADD `student_belongsto` ENUM('S','F') NULL DEFAULT NULL AFTER `updated_at`";
         
         DB::statement($query33);


      }
      catch(Exception $ex)
      {
           DB::rollBack();
           flash('oops...!',$ex->getMessage(), 'error');
          
      }   
      flash('success','all_fee_tables_created_successfully','success');
      return redirect(URL_MASTERSETTINGS_SETTINGS); 
     

    }

    
}
