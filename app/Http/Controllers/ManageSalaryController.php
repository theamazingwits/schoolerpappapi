<?php
namespace App\Http\Controllers;
use \App;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\ManageSalary;
use Yajra\Datatables\Datatables;
use App\LogHelper;
use DB;
use Exception; 

class ManageSalaryController extends Controller
{
        
    public function __construct()
    {
    	$this->middleware('auth');

    }


    public function index()
    {
        if(!checkRole(getUserGrade(18)))
        {
          prepareBlockUserMessage();
          return back();
        }

        $data['active_class']        = 'payroll';
        $data['title']               = getPhrase('manage_salary');
        $data['layout']              = getLayout();

         return view('payroll.manage_salary.list', $data);

        //  $view_name = getTheme().'::payroll.manage_salary.list';
        // return view($view_name,$data);
    }

    /**
     * This method returns the datatables data to view
     * @return [type] [description]
     */
    public function getDatatable()
    {

         $records = DB::table('user_salary_templates')
                        // ->join('branches', 'branches.id', '=', 'user_salary_templates.branch_id')
                        ->join('roles', 'roles.id', '=', 'user_salary_templates.role_id')
                        ->join('users', 'users.id', '=', 'user_salary_templates.user_id')
                        ->select('roles.display_name', 'users.name', 'users.email', 'users.created_at', 'user_salary_templates.salary_type', 'user_salary_templates.template_name', 'user_salary_templates.id')
                        ->get();

        return Datatables::of($records)
        ->addColumn('action', function ($records) {
           
            return '<div class="dropdown more">
                        <a id="dLabel" type="button" class="more-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-ellipsis-v"></i>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">

                            <li><a href="'.URL_PAYROLL_MANAGE_SALARY_EDIT.$records->id.'"><i class="fa fa-pencil"></i>'.getPhrase("edit").'</a></li>

                            <li><a href="javascript:void(0);" onclick="deleteRecord(\''.$records->id.'\');"><i class="fa fa-trash"></i>'. getPhrase("delete").'</a></li>
                        </ul>
                    </div>';
            })
        ->removeColumn('id')
        ->make();
    }


    /**
     * This method loads the create view
     * @return void
     */
    public function create()
    {
    	if(!checkRole(getUserGrade(18)))
        {
          prepareBlockUserMessage();
          return back();
        }

        $data['record']             = FALSE;
        // $data['branches']           = addSelectToList(getBranches());
        $data['roles']              = addSelectToList(getRoles());
        $data['salary_types']  	    = array('' => getPhrase('select'),'Monthly' => getPhrase('monthly'), 'Hourly' => getPhrase('hourly'));
    	$data['active_class']       = 'payroll';
    	$data['title']              = getPhrase('add');
        $data['layout']             = getLayout();

        return view('payroll.manage_salary.add-edit', $data);

        //  $view_name = getTheme().'::payroll.manage_salary.add-edit';
        // return view($view_name,$data);
    }


    /**
     * This method loads the edit view based on unique slug provided by user
     * @param  [string] $slug [unique slug of the record]
     * @return [view with record]       
     */
    public function edit($id)
    {
    	if(!checkRole(getUserGrade(18)))
        {
          prepareBlockUserMessage();
          return back();
        }

        $record = ManageSalary::where('id', $id)->get()->first();
        $data['record']             = $record;
        // $data['branches']           = addSelectToList(getBranches());
        $data['roles']              = addSelectToList(getRoles());
        $data['salary_types']       = array('' => getPhrase('select'),'Monthly' => getPhrase('monthly'), 'Hourly' => getPhrase('hourly'));
    	$data['active_class']       = 'payroll';
        $data['title']              = getPhrase('edit');
        $data['layout']              = getLayout();
        
    	return view('payroll.manage_salary.add-edit', $data);

        //   $view_name = getTheme().'::payroll.manage_salary.add-edit';
        // return view($view_name,$data);
    }


    /**
     * Update record based on slug and reuqest
     * @param  Request $request [Request Object]
     * @param  [type]  $slug    [Unique Slug]
     * @return void
     */
    public function update(Request $request, $id)
    {

        if(!checkRole(getUserGrade(18)))
        {
          prepareBlockUserMessage();
          return back();
        }

        $record  = ManageSalary::where('id', $id)->first();
        
        $this->validate($request, [
                    // 'branch_id'     => 'bail|required',
                    'role_id'       => 'required',
                    'user_id'       => 'required',
                    'salary_type'   => 'required',
                    'template_id'   => 'required',
                    'template_name' => 'required',
                    ]);

    	
        // $record->branch_id      = $request->branch_id;
        $record->role_id        = $request->role_id;
        $record->user_id        = $request->user_id;
        $record->salary_type    = $request->salary_type;
        $record->template_id    = $request->template_id;
        $record->template_name  = $request->template_name;
        $record->save();

        $record->flag        = 'Update';
        $record->action      = 'User_salary_templates';
        $record->object_id   =  $id;
        $logs = new LogHelper();
        $logs->storeLogs($record);


    	flash('success','record_updated_successfully', 'success');
    	return redirect(URL_PAYROLL_MANAGE_SALARY);
    }

    /**
     * This method adds record to DB
     * @param  Request $request [Request Object]
     * @return void
     */
    public function store(Request $request)

    {

        if(!checkRole(getUserGrade(18)))
        {
          prepareBlockUserMessage();
          return back();
        }

        $rules  = [

                    'role_id'       => 'required',
                    'user_id'       => 'required',
                    'salary_type'   => 'required',
                    'template_id'   => 'required',
                    ];

        $customMsg =  [
        'template_id.required' => 'Template is required',
    ];

        $this->validate($request,$rules, $customMsg );

    	$record = new ManageSalary();
        // $record->branch_id      = $request->branch_id;
        $record->role_id        = $request->role_id;
        $record->user_id        = $request->user_id;
        $record->salary_type    = $request->salary_type;
        $record->template_id    = $request->template_id;
        $record->template_name 	= $request->template_name;
        $record->save();

        $record->flag        = 'Insert';
        $record->action      = 'User_salary_templates';
        $record->object_id   =  $record->id;
        $logs = new LogHelper();
        $logs->storeLogs($record);


        flash('success','record_added_successfully', 'success');
    	return redirect(URL_PAYROLL_MANAGE_SALARY);
    }


    /**
     * Delete Record based on the provided id
     * @param  [string] $id [unique id]
     * @return Boolean 
     */
    public function delete($id)
    {
        if(!checkRole(getUserGrade(18)))
        {
          prepareBlockUserMessage();
          return back();
        }

        try{
             if(!env('DEMO_MODE')) {

                $record = ManageSalary::where('id', $id)->delete();

                $record->flag        = 'Delete';
                $record->action      = 'User_salary_templates';
                $record->object_id   =  $id;
                $logs = new LogHelper();
                $logs->storeLogs($record);
            }
            $response['status'] = 1;
            $response['message'] = getPhrase('record_deleted_successfully');
        }
        catch(Exception $e)
        {
           $response['status'] = 0;
           if(getSetting('show_foreign_key_constraint','module'))
            $response['message'] =  $e->getMessage();
           else
            $response['message'] =  getPhrase('this_record_is_in_use_in_other_modules');
        }
        return json_encode($response);
    }


    public function ajaxGetUsers(Request $request)
    {

        // $branch_id  = $request->branch_id;
        $role_id    = $request->role_id;

        // $users = DB::table('users')->where('branch_id', $branch_id)->where('role_id', $role_id)->select('id','name')->get();
        $users = DB::table('users')->where('role_id', $role_id)->select('id','name')->get();

        $options = '<option value="">'.getPhrase('no_users_available').'</option>';

        if(count($users)) {

            $options = '<option value="">'.getPhrase('select').'</option>';

            foreach ($users as $key => $value) {
                $options .= '<option value="'.$value->id.'">'.$value->name.'</option>';
            }
        }

        echo $options;
    }

    public function ajaxGetTemplates(Request $request)
    {

        $salary_type  = $request->salary_type;

        if($salary_type == "Monthly") {
            $table = "salary_templates";
            $column = 'salary_grades';
        }
        else {
            $table = "hourly_templates";
            $column = 'hourly_grades';
        }

        $templates = DB::table($table)->select('id',$column)->get();

        $options = '<option value="">'.getPhrase('no_templates_available').'</option>';

        if(count($templates)) {

            $options = '<option value="">'.getPhrase('select').'</option>';

            foreach ($templates as $key => $value) {
                $options .= '<option value="'.$value->id.'">'.$value->$column.'</option>';
            }
        }

        echo $options;
    }


    public function ajaxGetUserDetails(Request $request)
    {
        $user_id = $request->user_id;

        $user_det = DB::table('users')->where('id', $user_id)->first();

        $det = '';

        if(count($user_det)) {
            $det .= '<div class="div-usr-dtls"><img src="'.getProfilePath($user_det->image).'" />';
            $det .= '<div class="user-dtls"><p></p><p><strong>'.getPhrase('email').':</strong>'.$user_det->email.'</p><p><strong>'.getPhrase('phone').':</strong>'.$user_det->phone.'</p><p><strong>'.getPhrase('joining_date').':</strong>'.$user_det->created_at.'</p></div></div>';
        }

        echo $det;
    }





}
