<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use App\Http\Requests;
use App\User;
use App\Role;
use App\InventoryItem;
use App\LogHelper;
use App\InventoryCategory;
use App\InventoryIssueItem;
use Yajra\Datatables\Datatables;
use DB;
use Auth;
use Exception;

class InventoryIssueItemController extends Controller
{

	public function __construct()
    { 
      $this->middleware('auth');
    }

    public function index()
    {

      if(!checkRole(getUserGrade(17)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $data['active_class']       = 'inventory';
        $data['layout']             = getLayout();
        $data['title']              = getPhrase('inventory_issue_item');

        return view('inventory.issueitem.list', $data);
    }

      /**
     * This method returns the datatables data to view
     * @return [type] [description]
     */
    public function getDatatable($id ='')
    {

      if(!checkRole(getUserGrade(17)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $records = array();

        
$records = InventoryIssueItem::select(['item_id','category_id','issue_date','return_date','role_id','user_id','added_by','quantity','status','id'])

            ->orderBy('updated_at', 'desc');
             
        return Datatables::of($records)
        ->addColumn('action', function ($records) {
         
          $link_data = '<div class="dropdown more">
                        <a id="dLabel" type="button" class="more-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-ellipsis-v"></i>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">';
                           $temp = '';
                           

                           if($records->status == 1)
                           
                           $temp .= '<li><a href="'.URL_INVENTORY_ISSUE_ITEM_EDIT.$records->id.'"><i class="fa fa-pencil"></i>'.getPhrase("edit").'</a></li>
                                   <li><a href="javascript:void(0);" onclick="returnRecord(\''.$records->id.'\');"><i class="fa fa-exchange"></i>'.getPhrase("return").'</a></li>';
                                        
                           if(checkRole(getUserGrade(2)) && $records->status == 2) {

                           $temp .= '<li><a href="javascript:void(0);" onclick="deleteRecord(\''.$records->id.'\');"><i class="fa fa-trash"></i>'. getPhrase("delete").'</a></li>';
                      }
                    
                    $temp .='</ul></div>';


                    $link_data .=$temp;
                    return $link_data;
            })

        ->editColumn('category_id', function($records){
            
            $category  = InventoryCategory::find($records->category_id);
            if($category)
            return ucwords($category->name);
            return '-';
        })

        ->editColumn('item_id',function($records){

          $item = InventoryItem::find($records->item_id);
          if($item)
          return ucwords($item->name);
          return '-';

        })

          ->editColumn('issue_date', function($records){
            
 
             return $records->issue_date .' - '. $records->return_date;
         })

         ->editColumn('user_id', function($records){
            
             $user  =  User::find($records->user_id);
             if($user){
               
                $role  = Role::find($user->role_id);
                return ucwords($user->name) .' ( '.$role->display_name.' ) '; 
             }
             else{
             	return '-';
             }
         })


           ->editColumn('added_by', function($records){
            
             $user  =  User::find($records->added_by);
             if($user){
               
                $role  = Role::find($user->role_id);
                return ucwords($user->name) .' ( '.$role->display_name.' ) '; 
             }
             else{
             	return '-';
             }
         })

           ->editColumn('status', function($records){

                if($records->status == 1)
                  return '<span class="label label-primary">Issued</span>';

                  return '<span class="label label-success">Returned</span>';
           })

        ->removeColumn('id')
        ->removeColumn('return_date')
        ->removeColumn('role_id')
         
        ->make();
    }

     /**
     * This method loads the create view
     * @return void
     */

     public function create()
    {

     
      if(!checkRole(getUserGrade(17)))
      {
        prepareBlockUserMessage();
        return back();
      }
      $data['record']             = FALSE;
      $data['layout']             = getLayout();
      $data['active_class']       = 'inventory';
      $data['title']              = getPhrase('add-item');

      $data['users']       = User::pluck('name','id')->toArray();
      $data['categories']  = InventoryCategory::pluck('name','id')->toArray();
      $data['roles']       = Role::pluck('display_name','id')->toArray();

      return view('inventory.issueitem.add-edit', $data);
    
    }


     /**
     * This method adds record to DB
     * @param  Request $request [Request Object]
     * @return void
     */
 
         

   public function store(Request $request)
    {

      // dd($request);

      if(!checkRole(getUserGrade(17)))
      {
        prepareBlockUserMessage();
        return back();
      }

      

        $record  =  new InventoryIssueItem();
        
        $record->role_id      =  $request->role_id;
        $record->user_id      =  $request->user_id;
        $record->added_by     =  Auth::user()->id;
        $record->issue_date   =  date('Y-m-d');
        $record->category_id  =  $request->category_id;
        $record->item_id      =  $request->item_id;
        $record->quantity     =  $request->quantity;
        $record->description  =  $request->description;
        $record->save();


        $record->flag      = 'Insert';
        $record->action      = 'Inventory_issue_items';
        $record->object_id   =  $record->id;
        $logs = new LogHelper();
        $logs->storeLogs($record);

        flash('success','record_added_successfully', 'success');
        return redirect(URL_INVENTORY_ISSUE_ITEM);
}
  

	/**
     * This method loads the edit view based on unique id provided by user
     * @param  [string] $id [unique id of the record]
     * @return [view with record]       
     */
    public function edit($id)
    {

      if(!checkRole(getUserGrade(17)))
      {
        prepareBlockUserMessage();
        return back();
      }

      
      $record = InventoryIssueItem::getRecordWithSlug($id);

      if($isValid = $this->isValidRecord($record))
      return redirect($isValid);

      $data['record']             = $record;
      $data['active_class']       = 'inventory';
      $data['layout']             = getLayout();
      $data['title']              = getPhrase('edit_issue_item');
     
      $data['categories']  = InventoryCategory::pluck('name','id')->toArray();
      $data['items']       = InventoryItem::pluck('name','id')->toArray();
      $data['users']       = User::pluck('name','id')->toArray();
      $data['roles']       = Role::pluck('name','id')->toArray();

      return view('inventory.issueitem.add-edit', $data);
    }


    /**
     * Update record based on id and reuqest
     * @param  Request $request [Request Object]
     * @param  [type]  $id    [Unique id]
     * @return void
     */
    public function update(Request $request, $id)
    {
      if(!checkRole(getUserGrade(17)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $record = InventoryIssueItem::getRecordWithSlug($id);
   
        $record->role_id      =  $request->role_id;
        $record->user_id      =  $request->user_id;
        $record->added_by     =  Auth::user()->id;
        $record->issue_date   =  date('Y-m-d');
        $record->category_id  =  $request->category_id;
        $record->item_id      =  $request->item_id;
        $record->quantity     =  $request->quantity;
        $record->description  =  $request->description;
        $record->save();

        $record->flag      = 'Update';
        $record->action      = 'Inventory_issue_items';
        $record->object_id   =  $id;
        $logs = new LogHelper();
        $logs->storeLogs($record);
    
      flash('success','record_updated_successfully', 'success');
      return redirect(URL_INVENTORY_ISSUE_ITEM);
    }

     /**
     * Delete Record based on the provided id
     * @param  [string] $id [unique id]
     * @return Boolean 
     */
    public function delete($id)
    {
      if(!checkRole(getUserGrade(17)))
      {
        prepareBlockUserMessage();
        return back();
      }
      /**
       * Check if any quizzes are associated with this instructions page, 
       * if not delete
       * @var [type]
       */
        $record   = InventoryIssueItem::where('id', $id)->first();
        $response = [];
       try {
         if(!env('DEMO_MODE')) {
          $record->delete();
          }

          $record->flag      = 'Delete';
          $record->action      = 'Inventory_issue_items';
          $record->object_id   =  $id;
          $logs = new LogHelper();
          $logs->storeLogs($record);

          $response['status']  = 1;
          $response['message'] = getPhrase('record_deleted_successfully');
      }
       catch ( \Illuminate\Database\QueryException $e) {
                 $response['status'] = 0;
           if(getSetting('show_foreign_key_constraint','module'))
            $response['message'] =  $e->errorInfo;
           else
            $response['message'] =  getPhrase('this_record_is_in_use_in_other_modules');
       }
        return json_encode($response);
         
  }

     public function isValidRecord($record)
    {
      if ($record === null) {

        flash('Ooops...!', getPhrase("page_not_found"), 'error');
        return $this->getRedirectUrl();
    }

    return FALSE;
    }

    public function getReturnUrl()
    {
      return URL_INVENTORY_ISSUE_ITEM;
    }


    public function getItems(Request $request)
    {
    	 $records  = InventoryItem::where('category_id',$request->category_id)
    	                            ->get();


    	 return $records;                             

    }

     public function getUsers(Request $request)
    {
    	
      $records  = User::where('role_id',$request->role_id)
    	                  ->get();

       return $records;                             

    }


    public function returnItem(Request $request)
    {
        $record = InventoryIssueItem::find($request->return_id);
        $record->return_date = date('Y-m-d');
        $record->notes       = $request->notes;
        $record->updated_by  = Auth::user()->id;
        $record->status      = 2;
        $record->save();


        $record->flag        = 'Update';
        $record->action      = 'Inventory_issue_items';
        $record->object_id   =  $request->return_id;
        $logs = new LogHelper();
        $logs->storeLogs($record);

       
        flash('success','item_returned_successfully', 'success');
        return redirect(URL_INVENTORY_ISSUE_ITEM);
    }

}
  
