<?php
namespace App\Http\Controllers;
use \App;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\TransportFee;
use App\TransportFeeType;
use App\Vroutes;
use App\Vechicle;
use App\VehicleUser;
use App\VehicleAssign;
use Yajra\Datatables\Datatables;
use DB;
use Auth;
use Exception;
use Carbon\Carbon;


class TransportFeeAssignController extends Controller
{
    
    public function __construct()
    {
      $this->middleware('auth');
    }

    /**
     * Course listing method
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
      
      if(!checkRole(getUserGrade(20)))
      {
        prepareBlockUserMessage();
        return back();
      }
     
     // dd('here');
        $data['active_class']  = 'transport';
        if(Auth::user()->role_id == 12)
        $data['active_class']  = 'transport_fee';
        $data['layout']        = getLayout();
        $data['title']         = getPhrase('assign_transport_fee');
        $data['routes_list']   = Vroutes::where('parent_id',0)->pluck('name','id')->toArray();
        $data['years']         = array_combine(range(date("Y"), 2016), range(date("Y"), 2016));
        $data['months']        = TransportFeeType::getMonths();
       

        return view("transport.assignfee.list",$data);
    }


    public function storeFee(Request $request)
    {

       $columns = array(

        'fee_type'   => 'bail|required',
        'start_date' => 'bail|required',
        'end_date'   => 'bail|required',
      );
      

        $this->validate($request,$columns);

        $route_id     = $request->route_id;
        $year         = $request->year;
        $month        = $request->month;

        $pre_data   = TransportFee::where('route_id', $route_id)
                                ->where('year', $year)
                                ->where('month', $month)
                                ->get()->count();

        if($pre_data > 0){

           flash('Oops..!','fee_is_already_assigned_for_selected_details','overlay');
           return back();
        }                         

        
        // dd($request);
        DB::beginTransaction();

        try{
              
              $fee_type             = new TransportFeeType();
              $fee_type->route_id   = $request->route_id;
              $fee_type->type       = $request->fee_type;
              $fee_type->save();

              $vehicle_users   = VehicleUser::where('parent_route_id',$route_id)
                                            ->where('is_active',1)
                                            ->get();
              // dd($vehicle_users);
              
              foreach ($vehicle_users as $vehi_user) {
                 
                 $fee_record                  = new TransportFee();
                 $fee_record->title           = $request->title;
                 $fee_record->user_id         = $vehi_user->user_id;
                 $fee_record->vehicle_id      = $vehi_user->vehicle_id;
                 $fee_record->route_id        = $vehi_user->route_id;
                 $fee_record->parent_route_id = $vehi_user->parent_route_id;
                 $fee_record->vehicle_user_id = $vehi_user->id;
                 $fee_record->year            = $year;
                 $fee_record->month           = $month;
                 $fee_record->start_date      = $request->start_date;
                 $fee_record->end_date        = $request->end_date;
                 $fee_record->added_by        = Auth::user()->id;
                 $fee_record->amount          = $vehi_user->seat_fee();
                 $fee_record->balance         = $vehi_user->seat_fee();
                 $fee_record->save();

              }



              DB::commit();
              flash('success','fee_assigned_successfully', 'overlay');     
        }
        catch( Exception $e ){

            DB::rollBack();
            flash('oops...!', $e->getMessage(), 'error');
      
        }

        return redirect( URL_ASSIGN_TRANSPORT_FEE );
        
    }


     public function payHostelFee()
    {

        $data['active_class'] = 'transport';
         if(Auth::user()->role_id == 12)
        $data['active_class']  = 'transport_fee';
        $data['layout']       = getLayout();
        $data['title']        = getPhrase('pay_transport_fee');
        $payment_ways         = array(  
                                        'cash'   =>getPhrase('cash'),
                                        'online' =>getPhrase('online'),
                                        'cheque' =>getPhrase('cheque'),
                                        'DD'     =>'DD',
                                        'other'  =>getPhrase('other_payment_way'));

        $data['payment_ways'] = $payment_ways;
        $user                 = Auth::user();
        $data['routes_list'] = Vroutes::join('transport_fee_type','transport_fee_type.route_id','=','vroutes.id')
                                       ->groupby('vroutes.id')
                                       ->orderby('transport_fee_type.created_at','desc')
                                       ->pluck('name','vroutes.id')
                                       ->toArray();

        $data['currency']   = getCurrencyCode();

     

         return view("transport.payfee.select-particulars",$data);
       
    }

    public function getRouteVehicles(Request $request)
    {
       // return $request;
       $route_id  = $request->route_id;
      
        $routes      = VehicleAssign::where('parent_route_id',$route_id)
                                      ->get();
        // $records    = array();
        foreach ($routes as $route) {
          
         $records = VehicleAssign::join('vechicles','vechicle_assign.vechicle_id','=','vechicles.id')
                                      ->select(DB::raw("CONCAT(number,' (',seats,')') AS name"),'vechicles.id')
                                      ->where('vechicle_assign.route_id',$route->route_id)
                                      ->get(); 
           

        }
        
     return json_encode($records);
    }


    public function getVechicleUsers(Request $request)
    {
       $route_id   = $request->route_id;
       $vehicle_id = $request->vehicle_id;

       $users      = VehicleUser::join('users','users.id','=','vehicle_user.user_id')
                                  ->where('parent_route_id',$route_id)
                                  ->where('vehicle_id',$vehicle_id)
                                  ->where('is_active',1)
                                  ->select(['users.id','users.name'])
                                  ->orderby('vehicle_user.created_at','desc')
                                  ->get();
        
        return json_encode($users);                       

    }

    public function getStudentData(Request $request)
    { 
      // return $request;

        $route_id = $request->route_id;
        $vechicle_id   = $request->vechicle_id;
        $user_id   = $request->user_id;
        
        $user      = VehicleUser::join('users','users.id','=','vehicle_user.user_id')
                                  ->where('parent_route_id',$route_id)
                                  ->where('vehicle_id',$vechicle_id)
                                  ->where('user_id',$user_id)
                                  ->where('is_active',1)
                                  ->first();

        $query          = TransportFee::where('user_id',$user_id);
        $total_pay      = $query->sum('amount');
        $total_discount = $query->sum('discount');
        $total_paid     = $query->sum('paid_amount');
        $total_balance  = $query->sum('balance');
        $records        = $query->get();
        $total_amount_pay =  $total_pay - ($total_discount + $total_paid);                         

        return json_encode( array( 

            'user'=>$user,
            'records'=>$records,
            'total_pay'=>$total_pay,
            'total_discount'=>$total_discount,
            'total_paid'=>$total_paid,
            'total_balance'=>$total_balance, 
            'total_amount_pay'=>$total_amount_pay 
        ));    

    }


    public function payFee(Request $request)
    {
        // dd($request);
        $user_route_id   = $request->route_id;  
        $user_vehicle_id = $request->vehicle_id;  
        $user_id         = $request->user_id;  
        $pay_amount      = $request->pay_amount;  
        $comments        = $request->notes; 

        if( $pay_amount <= 0 ){

          flash('Oopss..!','enter_the_amount','overlay');
          return back();
        }


          $query          = TransportFee::where('user_id',$user_id)
                                    ->where('parent_route_id',$user_route_id)
                                    ->where('vehicle_id',$user_vehicle_id)
                                    ->where('is_paid',0)
                                    ->orWhere('is_paid',2);

        $total_amount      =  (int)$query->sum('amount');
        $total_paid_amount =  (int)$query->sum('paid_amount') + (int)$query->sum('discount');
        // dd($total_amount);

        if( $pay_amount > $total_amount  ){

          flash('Oopss..!','entered_amount_is_higher_than_total_amount_to_pay','overlay');
          return back();
        }
        elseif ($pay_amount > $total_paid_amount && $total_paid_amount != 0) {
          
            flash('Oopss..!','entered_amount_is_higher_than_total_amount_to_pay','overlay');
            return back();
        }

                DB::beginTransaction();

        try{                             

    
        $sec_records  =  TransportFee::where('user_id',$user_id)
                                    ->where('parent_route_id',$user_route_id)
                                    ->where('vehicle_id',$user_vehicle_id)
                                    ->where('is_paid',0)
                                    ->orWhere('is_paid',2)
                                    ->get();
     // dd($sec_records);
            
            $bal_dection  = (int)$pay_amount;

            foreach ( $sec_records as $sec_record ) {

                if( $bal_dection > 0 ) {
                
                   $total_paid      = $sec_record->paid_amount + $sec_record->discount;
                   $balance         = (int)$sec_record->amount - (int)$total_paid;
                   // dd($bal_dection);
                   
                   if($bal_dection >= $balance){

                     $sec_record->paid_amount = $balance + (int)$sec_record->paid_amount;
                     $final_bal    = (int)$sec_record->amount -((int)$sec_record->paid_amount+(int)$sec_record->discount);
                     $sec_record->balance  = $final_bal;
                    
                   }elseif($bal_dection < $balance){
                     
                    $sec_record->paid_amount = (int)$bal_dection + (int)$sec_record->paid_amount;
                    $final_bal    = (int)$sec_record->amount - ((int)$sec_record->paid_amount+(int)$sec_record->discount);
                    $sec_record->balance  = $final_bal;

                   }


                    if($final_bal  == 0){

                     $sec_record->is_paid  = 1;
                    }else{

                     $sec_record->is_paid  = 2;
                    }

                  $sec_record->comments     = $comments;
                  $sec_record->payment_mode = $request->payment_mode;
                  $sec_record->paid_date    = date('Y-m-d');
                  $sec_record->save();
                }

// dd($sec_record->paid_amount); 
                  $bal_dection -=  (int)$sec_record->paid_amount;
                  // dd($bal_dection);

                
              }
         // }

         DB::commit();
         flash('success','fee_paid_successfully','success');

      }catch(Exception $e){

          // dd($e->getMenssage());
          DB::rollBack();
          flash('Oops..!','something_went_wrong_please_try_again','error');
      }                            
                                
        return back();


    }


     public function discount( Request $request )
     {


        // dd($request);
        $user_id          = $request->userid;  
        $user_vechicle_id = $request->user_vechicle_id;  
        $user_route_id    = $request->user_route_id;  
        $comments         = $request->comments; 
        $user_discount    = $request->user_discount; 

        if( $user_discount <= 0 ){

          flash('Oopss..!','enter_the_discount_amount','overlay');
          return back();
        }  

        $query          = TransportFee::where('user_id',$user_id)
                                    ->where('vehicle_id',$user_vechicle_id)
                                    ->where('parent_route_id',$user_route_id)
                                    ->where('is_paid',0)
                                    ->orWhere('is_paid',2);

        $total_amount      =  (int)$query->sum('amount');
        $total_paid_amount =  (int)$query->sum('paid_amount') + (int)$query->sum('balance');
        // dd($total_paid_amount);

        if( $user_discount > $total_amount || $user_discount > $total_paid_amount ){

          flash('Oopss..!','entered_discount_amount_is_higher_than_total_amount_to_pay','overlay');
          return back();
        }

        DB::beginTransaction();

        try{                             

    
        $sec_records  =  TransportFee::where('user_id',$user_id)
                                    ->where('vehicle_id',$user_vechicle_id)
                                    ->where('parent_route_id',$user_route_id)
                                    ->where('is_paid',0)
                                    ->orWhere('is_paid',2)
                                    ->get();
     // dd($sec_records);
            
            $bal_dection  = (int)$user_discount;

            foreach ( $sec_records as $sec_record ) {

                if( $bal_dection > 0 ) {
                
                   $total_paid      = $sec_record->paid_amount + $sec_record->discount;
                   $balance         = (int)$sec_record->amount - (int)$total_paid;
                   // dd($bal_dection);
                   
                   if($bal_dection >= $balance){

                     $sec_record->discount = $balance + (int)$sec_record->discount;
                     $final_bal            = (int)$sec_record->amount -((int)$sec_record->discount+(int)$sec_record->paid_amount);
                     $sec_record->balance  = $final_bal;
                    
                   }elseif($bal_dection < $balance){
                     
                    $sec_record->discount = (int)$bal_dection + (int)$sec_record->discount;
                    $final_bal            = (int)$sec_record->amount - ((int)$sec_record->discount+(int)$sec_record->paid_amount);
                    $sec_record->balance  = $final_bal;

                   }

                   $sec_record->comments = $comments;

                    if($final_bal  == 0){

                     $sec_record->is_paid  = 1;
                    }else{

                     $sec_record->is_paid  = 2;
                    }


                  $sec_record->save();
                }

// dd($sec_record->discount);
                  $bal_dection -=  (int)$sec_record->discount;
                  // dd($bal_dection);

                
              }
         // }

         DB::commit();
         flash('success','discount_added_successfully','success');

      }catch(Exception $e){

          // dd($e->getMenssage());
          DB::rollBack();
          flash('Oops..!','something_went_wrong_please_try_again','error');
      }                            
                                
        return back();
     } 




    public function reports()
    {
       if(!checkRole(getUserGrade(20)))
      {
        prepareBlockUserMessage();
        return back();
      }
     
     // dd('here');
        $data['active_class']  = 'transport';
         if(Auth::user()->role_id == 12)
        $data['active_class']  = 'transport_fee';
        $data['layout']        = getLayout();
        $data['title']         = getPhrase('transportation_fee_reports');
        $data['routes_list']   = Vroutes::join('transport_fee_type','transport_fee_type.route_id','=','vroutes.id')
                                       ->groupby('vroutes.id')
                                       ->orderby('transport_fee_type.created_at','desc')
                                       ->pluck('name','vroutes.id')
                                       ->toArray();

        $data['years']         = array_combine(range(date("Y"), 2016), range(date("Y"), 2016));
        $data['months']        = TransportFeeType::getMonths();
       

        //    $view_name = getTheme().'::hostel.reports.list';
        // return view($view_name,$data);

         return view("transport.reports.list",$data);


    }

    public function getmonths(Request $request)
    {
       // return $request;
       $route_id  = $request->route_id;
       $record     = TransportFeeType::where('route_id', $route_id)->orderby('created_at','desc')->first();

       if(!$record || $record->type == 1)
         return $showmonth = 1;
       
       return $showmonth = 0;

    }

    public function getFeeReports(Request $request)
    {
        // return $request;
        $route_id    = $request->route_id;
        $vehicle_id  = $request->vehicle_id;
        $year        = $request->year;
        
        if( ( $request->has('year') && $request->has('month') ) && ( $request->has('route_id') && $request->has('vehicle_id') ) ){
         
         $month       = $request->month;

          $reports   = TransportFee::join('users','users.id','=','transport_fee.user_id')
                                ->join('students', 'users.id' ,'=', 'students.user_id')
                                ->join('courses','courses.id','=','students.course_id')
                                ->join('vroutes','vroutes.id','=','transport_fee.route_id')
                                ->join('vechicles','vechicles.id','=','transport_fee.vehicle_id')
                                ->where('transport_fee.parent_route_id',$route_id)
                                ->where('transport_fee.vehicle_id',$vehicle_id)
                                ->where('transport_fee.year',$year)
                                ->where('transport_fee.month',$month)
                                ->select(['users.name','users.slug','roll_no','course_title','vechicles.number','vroutes.name as route_name','amount','paid_amount','balance','year','month','start_date','end_date','discount'])
                                ->orderby('transport_fee.created_at','desc')
                                ->get();

        }elseif ( $request->has('year') && $request->has('route_id') && $request->has('vehicle_id') ) {
          
            $reports   = TransportFee::join('users','users.id','=','transport_fee.user_id')
                                  ->join('students', 'users.id' ,'=', 'students.user_id')
                                  ->join('courses','courses.id','=','students.course_id')
                                  ->join('vroutes','vroutes.id','=','transport_fee.route_id')
                                  ->join('vechicles','vechicles.id','=','transport_fee.vehicle_id')
                                  ->where('transport_fee.parent_route_id',$route_id)
                                  ->where('transport_fee.vehicle_id',$vehicle_id)
                                  ->where('transport_fee.year',$year)
                                  ->select(['users.name','users.slug','roll_no','course_title','vechicles.number','vroutes.name as route_name','amount','paid_amount','balance','year','month','start_date','end_date','discount'])
                                   ->orderby('transport_fee.created_at','desc')
                                  ->get();
                // return $reports;                  
        }

        elseif ( $request->has('start_date') && $request->has('end_date') ) {

            $start_date  = $request->start_date;
            $end_date    = $request->end_date;

            if( $request->has('year') && ( $request->has('route_id') && $request->has('vehicle_id') ) ){

                 $reports   = HostelFee::join('users','users.id','=','transport_fee.user_id')
                                ->join('students', 'users.id' ,'=', 'students.user_id')
                                ->join('courses','courses.id','=','students.course_id')
                                ->join('vroutes','vroutes.id','=','transport_fee.route_id')
                                ->join('vechicles','vechicles.id','=','transport_fee.vehicle_id')
                                ->where('transport_fee.start_date','>=',$start_date)
                                ->where('transport_fee.end_date','<=',$end_date)
                                ->where('transport_fee.parent_route_id',$route_id)
                                 ->where('transport_fee.vehicle_id',$vehicle_id)
                                ->where('transport_fee.year',$year)
                                ->select(['users.name','users.slug','roll_no','course_title','vechicles.number','vroutes.name as route_name','amount','paid_amount','balance','year','month','start_date','end_date','discount'])
                                 ->orderby('transport_fee.created_at','desc')
                                ->get();
            }
            else{

                   $reports   = HostelFee::join('users','users.id','=','transport_fee.user_id')
                                ->join('students', 'users.id' ,'=', 'students.user_id')
                                ->join('courses','courses.id','=','students.course_id')
                                ->join('vroutes','vroutes.id','=','transport_fee.route_id')
                                ->join('vechicles','vechicles.id','=','transport_fee.vehicle_id')
                                ->where('transport_fee.start_date','>=',$start_date)
                                ->where('transport_fee.end_date','<=',$end_date)
                                ->select(['users.name','users.slug','roll_no','course_title','vechicles.number','vroutes.name as route_name','amount','paid_amount','balance','year','month','start_date','end_date','discount'])
                                 ->orderby('transport_fee.created_at','desc')
                                ->get();
            }
          
           
        }
        

        return json_encode(array('users'=>$reports));                        

    }

    public function printFeeReports(Request $request)
    {
       // dd($request);

        $route_id    = $request->route_id;
        $vehicle_id  = $request->vehicle_id;
        $year        = $request->year;
        
        if( ($request->year && $request->month) && ( $request->route_id && $request->vehicle_id ) ){
         
         $month       = $request->month;

          $reports   = TransportFee::join('users','users.id','=','transport_fee.user_id')
                                ->join('students', 'users.id' ,'=', 'students.user_id')
                                ->join('courses','courses.id','=','students.course_id')
                                ->join('vroutes','vroutes.id','=','transport_fee.route_id')
                                ->join('vechicles','vechicles.id','=','transport_fee.vehicle_id')
                                ->where('transport_fee.parent_route_id',$route_id)
                                ->where('transport_fee.vehicle_id',$vehicle_id)
                                ->where('transport_fee.year',$year)
                                ->where('transport_fee.month',$month)
                                ->select(['users.name','users.slug','roll_no','course_title','vechicles.number','vroutes.name as route_name','amount','paid_amount','balance','year','month','start_date','end_date','discount'])
                                ->orderby('transport_fee.created_at','desc')
                                ->get();

        }elseif ( $request->year && ( $request->route_id && $request->vehicle_id ) ) {
          
           $reports   = TransportFee::join('users','users.id','=','transport_fee.user_id')
                                  ->join('students', 'users.id' ,'=', 'students.user_id')
                                  ->join('courses','courses.id','=','students.course_id')
                                  ->join('vroutes','vroutes.id','=','transport_fee.route_id')
                                  ->join('vechicles','vechicles.id','=','transport_fee.vehicle_id')
                                  ->where('transport_fee.parent_route_id',$route_id)
                                  ->where('transport_fee.vehicle_id',$vehicle_id)
                                  ->where('transport_fee.year',$year)
                                  ->select(['users.name','users.slug','roll_no','course_title','vechicles.number','vroutes.name as route_name','amount','paid_amount','balance','year','month','start_date','end_date','discount'])
                                   ->orderby('transport_fee.created_at','desc')
                                  ->get();
        }

        elseif ( $request->start_date && $request->end_date ) {

            $start_date  = $request->start_date;
            $end_date    = $request->end_date;

            

                   $reports   = HostelFee::join('users','users.id','=','transport_fee.user_id')
                                ->join('students', 'users.id' ,'=', 'students.user_id')
                                ->join('courses','courses.id','=','students.course_id')
                                ->join('hostel_rooms','hostel_rooms.id','=','transport_fee.room_id')
                                ->where('transport_fee.start_date','>=',$start_date)
                                ->where('transport_fee.end_date','<=',$end_date)
                                ->select(['users.name','users.slug','roll_no','course_title','room_number','amount','paid_amount','balance','year','month','start_date','end_date'])
                                 ->orderby('transport_fee.created_at','desc')
                                ->get();
          
          
           
        }

        // dd($reports);
          $vehicle          = Vechicle::find($vehicle_id);
          $vroute           = Vroutes::find($route_id);
          $data['reports']  = $reports;
          $data['title']    = '';
          if($vehicle && $vroute){
            
          $data['title']    = $vehicle->number.' - '. $vroute->name;
          }
          
          // $view_name = getTheme().'::hostel.reports.print-students';

          $view = "transport.reports.print-students";
          $view = \View::make($view, $data);

     
        $html_data = ($view);

        echo $html_data;

        die();

    }


    public function transportDetails($user_id)
    {
       $user                       = getUserRecord($user_id);

        $data['transport_details']  = App\VehicleUser::where('user_id',$user->id)->get();
        $data['user']               = $user;
        $data['record']             = $user;
        $data['active_class']       = 'users';
        $data['title']              = ucwords($user->name).' '.getPhrase('transport_details');
        $data['layout']             = getLayout();

        $query                  = App\TransportFee::where('user_id',$user_id);
        $data['total_pay']      = $query->sum('amount');
        $data['total_discount'] = $query->sum('discount');
        $data['total_paid']     = $query->sum('paid_amount');
        $data['total_balance']  = $query->sum('balance');
        $data['records']        = $query->get();
        $data['currency']       = getCurrencyCode();
       

        return view('transport.user-details', $data);


    }


    

}