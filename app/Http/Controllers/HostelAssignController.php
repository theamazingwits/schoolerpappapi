<?php
namespace App\Http\Controllers;
use \App;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\RoomType;
use App\HostelRoom;
use App\Hostel;
use App\HostelUser;
use App\User;
use Yajra\Datatables\Datatables;
use DB;
use Auth;
use Carbon\Carbon;


class HostelAssignController extends Controller
{
    
    public function __construct()
    {
    	$this->middleware('auth');
    }

    /**
     * Course listing method
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
      if(!checkRole(getUserGrade(21)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $data['active_class']       = 'hostel';
        $data['title']              = getPhrase('assign_hostel_to_student');
        
       if( Hostel::isMultiBranch() ){

        $data['branches']           = addSelectToList(getBranches());
        }else{

        $data['branches']           = null;
        }

        $data['is_multibranch']     = Hostel::isMultiBranch();
        $data['academic_years']     = addSelectToList(getAcademicYears());
        $list                       = App\Course::getCourses(0);
        
        $data['layout']             = getLayout();

        return view('hostel.hostelassign.list' ,$data);

         
    }


    public function assignHostel($slug)
    { 
        $user                       = User::getRecordWithSlug($slug);

        $data['hoste_details']      = HostelUser::where('user_id',$user->id)->get();
        $data['details']            = HostelUser::where('user_id',$user->id)->get()->count();
        $data['user']               = $user;
        $data['record']             = $user;
        $data['active_class']       = 'hostel';
        $data['title']              = ucwords($user->name).' '.getPhrase('hostel_details');
        $data['layout']             = getLayout();
        $data['hostels_list']       = Hostel::pluck('name','id')->toArray();
        $data['years']              = array_combine(range(date("Y"), 2016), range(date("Y"), 2016));
        $data['months']             = App\HostelFeeType::getMonths();

        return view('hostel.hostelassign.student-hostel', $data);

          
        
    }

    public function getRooms(Request $request)
    {
       
       $hostel_id  = $request->hostel_id;
      
        $rooms      = HostelRoom::where('hostel_id',$hostel_id)
                                 ->get();
        $records    = array();
        foreach ($rooms as $rum) {
          
          $avaialbe_rums  = HostelUser::where('room_id',$rum->id)->where('is_vacate',0)->get()->count();

          if( $rum->beds > $avaialbe_rums ){

             $records[] = HostelRoom::join('room_types','room_types.id','=','hostel_rooms.room_type_id')
                                      ->select(DB::raw("CONCAT(room_number,' (',room_type,')') AS name"),'hostel_rooms.id')
                                      ->where('hostel_rooms.id',$rum->id)
                                      ->first(); 
           }

        }
        
     return json_encode($records);
    }

    public function store(Request $request)
    {    

      // dd($request);  
       $columns = array(

        'hostel_id'     => 'bail|required',
        'room_id'       => 'bail|required',
       
        );

        $this->validate($request,$columns);

         //Any Active Records
         $user_id  = $request->user_id;

         $balance_amount   = App\HostelFee::userBalance($user_id);

         if($balance_amount > 0){

             flash('Oops...!','user_need_to_pay '.getCurrencyCode(). $balance_amount.' hostel_fee_so_unable_change_user_room','overlay');

             return back();
          }

         $pre_records  = HostelUser::where('user_id',$user_id)->get();

          foreach ($pre_records as $record) {
            
            $record->is_vacate = 1;
            $record->is_active = 0;
            $record->save();
          }

        if( $request->user_join_type == 1 ){

             HostelUser::create($request->all());
             flash('success','user_added_successfully', 'success');  
        }
        elseif( $request->user_join_type == 0 ){
            
            $hostel_id   = $request->hostel_id;
            $fee_types   = App\HostelFeeType::where('hostel_id', $hostel_id)->get()->count();

            if($fee_types > 0){

                 $hst_user    =  HostelUser::create( $request->all() );

                 $pre_record  =  App\HostelFee::where('hostel_id',$hostel_id)
                                                ->where('year', $request->year)
                                                ->orderby('created_at','desc')
                                                ->first();
                 // dd($pre_record); 
                 $fee_record                  = new App\HostelFee();
                 $fee_record->title           = $request->title;
                 $fee_record->user_id         = $hst_user->user_id;
                 $fee_record->hostel_id       = $hst_user->hostel_id;
                 $fee_record->room_id         = $hst_user->room_id;
                 $fee_record->hostel_user_id  = $hst_user->id;
                 $fee_record->year            = $request->year;
                 $fee_record->month           = $request->month;
                 if($pre_record){

                     $fee_record->start_date      = $pre_record->start_date;
                 $fee_record->end_date        = $pre_record->end_date;
                 }
                
                 $fee_record->added_by        = Auth::user()->id;
                 $details                     = $hst_user->roomname();
                 $fee_record->amount          = $details['cost'];
                 $fee_record->balance         = $details['cost'];
                 $fee_record->save();
                  flash('success','user_added_successfully', 'success');
            }else{

                flash('Oops..!','no_fee_type_is_created_for_selected_hostel', 'overlay'); 
            }
        }

        
        
        
        return redirect(URL_ASSIGN_HOSTEL);
    }


    public function vacateHostel(Request $request)
    {
      $user_id  = $request->vacate_user_id;

      $balance_amount   = App\HostelFee::userBalance($user_id);
      
      if($balance_amount > 0){

         flash('Oops...!','user_need_to_pay '.getCurrencyCode(). $balance_amount.' hostel_fee_so_unable_vacate_user','overlay');
         return back();
      }

      $record  = HostelUser::where('user_id',$user_id)
                             ->where('is_active',1)
                             ->first();
      $record->is_vacate = 1;  
      $record->is_active = 0;  
      $record->save();

      flash('success','user_vacated_from_hostel_successfully', 'success');
      return redirect(URL_ASSIGN_HOSTEL);                       
    }
     



     /**
     * This method returns the list of students avaialbe with the requested 
     * combination
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getStudents(Request $request)
    {
        $branch_id          = $request->branch_id;
        $academic_id        = $request->academic_id;
        $course_parent_id   = $request->parent_course_id;
        $course_id          = $request->course_id;
        $year               = $request->year;
        $semister           = $request->semister;

        // return $request;

        $records = User::join('students', 'users.id' ,'=', 'students.user_id')  
        ->join('branches','branches.id','=','students.branch_id') 
        ->join('academics','academics.id','=','students.academic_id') 
        ->join('courses','courses.id','=','students.course_id')
        ->where('students.branch_id','=',$branch_id)
        ->where('academic_id','=',$academic_id)
        ->where('course_parent_id','=',$course_parent_id)
        ->where('course_id','=',$course_id)
        ->where('current_year','=',$year)
        ->where('current_semister','=',$semister)
        ->select(['students.user_id as id','users.name', 'roll_no','admission_no', 'course_title','blood_group','mobile','home_phone','image','branch_name','branches.address','academic_year_title', 'email', 'current_year', 'current_semister','course_dueration','students.branch_id as branch_id','students.academic_id as academic_id', 'students.course_id as course_id', 'students.user_id as user_id','users.slug'])
        ->get();
        return $records;
    }


      /**
     * This method returns the list of students avaialbe with the requested 
     * combination
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getSingleBranchStudents(Request $request)
    {
        $academic_id        = $request->academic_id;
        $course_parent_id   = $request->course_parent_id;
        $course_id          = $request->course_id;
        $year               = $request->year;
        $semister           = $request->semister;

        $records = User::join('students', 'users.id' ,'=', 'students.user_id')  
        ->join('academics','academics.id','=','students.academic_id') 
        ->join('courses','courses.id','=','students.course_id')
        ->where('academic_id','=',$academic_id)
        // ->where('course_parent_id','=',$course_parent_id)
        ->where('course_id','=',$course_id)
        ->where('current_year','=',$year)
        ->where('current_semister','=',$semister)
        ->select(['students.user_id as id','users.name', 'roll_no','admission_no', 'course_title','blood_group','mobile','home_phone','image','academic_year_title', 'email', 'current_year', 'current_semister','course_dueration','students.academic_id as academic_id', 'students.course_id as course_id', 'students.user_id as user_id','users.slug'])
        ->get();
        return $records;
    } 


      /**
     * This method returns the list of parent courses available with the specific 
     * academic year
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getParentCourses(Request $request)
    {
        $records = App\AcademicCourse::join('courses', 'academic_course.course_parent_id', '=' ,'courses.id')
         ->select([  
            'course_title','courses.id'])
         ->where('academic_id', '=', $request->academic_id)
         ->groupBy('course_parent_id')->get();
         return $records;
    }

   
    /**
     * This method will return the child courses based on the request
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getChildCourses(Request $request)
    {
     
        $current_user = Auth::user();
        $user_role = getRoleData($current_user->role_id);
        $student_record = '';
        if($user_role=='student')
        {
          $student_record = App\Student::where('user_id','=',$current_user->id)->first();
        }


        $course_id =$request->course_id;
        $academic_id = $request->academic_id;
        $parent_course_id = $request->parent_course_id;
        $records = App\AcademicCourse::join('courses', 'academic_course.course_id', '=' ,'courses.id')
         ->select([  
            'course_title','courses.id', 'course_dueration','is_having_semister','academic_id', 'course_parent_id'])
         ->where('academic_course.academic_id', '=', $academic_id)
         ->where('academic_course.course_parent_id', '=', $parent_course_id);

         if($user_role=='student')
         {
            $records = $records->where('academic_course.course_id','=',$student_record->course_id);
         }
         else {
           $records = $records->groupBy('course_id');
        }
        $records = $records->get();
         $final_records = [];
         foreach($records as $key => $value)
         {
          $temp['course'] = $value;
          $temp['semister'] = $value->semisters();

          $final_records[] = $temp;
         }
         return $final_records;
    }


    public function hostelDetails($user_id)
    {
         

        $user                       = getUserRecord($user_id);

        $data['hoste_details']      = HostelUser::where('user_id',$user->id)->get();
        $data['details']            = HostelUser::where('user_id',$user->id)->get()->count();
        $data['user']               = $user;
        $data['record']             = $user;
        $data['active_class']       = 'users';
        $data['title']              = ucwords($user->name).' '.getPhrase('hostel_details');
        $data['layout']             = getLayout();

        $query                  = App\HostelFee::where('user_id',$user_id);
        $data['total_pay']      = $query->sum('amount');
        $data['total_discount'] = $query->sum('discount');
        $data['total_paid']     = $query->sum('paid_amount');
        $data['total_balance']  = $query->sum('balance');
        $data['records']        = $query->get();
        $data['currency']       = getCurrencyCode();
       

        return view('hostel.user-details', $data);
    }


}