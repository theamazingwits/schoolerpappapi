<?php
namespace App\Http\Controllers;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Illuminate\Http\Request;

use App\Http\Requests;
use App;
use Auth;
use App\User;

use SMS;
use Yajra\Datatables\Datatables;
use DB;
use Charts;
use Artisan;
use Input;
use Excel;
 
// use Codedge\Fpdf\Facades\Fpdf;

// use Illuminate\Support\Facades\App;

class EmailNotificationsController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth');
    }

     public function index()
    { 
      $data['active_class']       = 'emailnotifications';
      $data['layout']             = getLayout();
      $data['title']              = getPhrase('email_notifications');
      $slug = Auth::user()->slug;
      $user = App\User::where('slug','=',$slug)->first();
      $role = getRoleData($user->role_id);
      return view('emailnotifications.dashboard',$data);
    }

    public function getPromotions()
    { 
      $data['active_class']       = 'promotions';
      $data['layout']             = getLayout();
      $data['title']              = getPhrase('promotions');
      $slug = Auth::user()->slug;
      $user = App\User::where('slug','=',$slug)->first();
      $role = getRoleData($user->role_id);
      $data['record'] = DB::table('email_list')
                ->get();
      return view('emailnotifications.promotions.promotions',$data);
    }


    public function getDueRemainder()
    { 
      $data['active_class']       = 'dueremainder';
      $data['layout']             = getLayout();
      $data['title']              = getPhrase('due_remainder');
      $slug = Auth::user()->slug;
      $user = App\User::where('slug','=',$slug)->first();
      $role = getRoleData($user->role_id);
      $data['record'] = DB::table('email_list')
                ->get();
      return view('emailnotifications.promotions.dueremainder',$data);
    }


    public function getDueRemainderList()
    { 
      $data['active_class']       = 'dueremainder';
      $data['layout']             = getLayout();
      $data['title']              = getPhrase('due_remainder');
      $slug = Auth::user()->slug;
      $user = App\User::where('slug','=',$slug)->first();
      $role = getRoleData($user->role_id);
      $records = array();
      $records = DB::table('feeparticular_paymets')
                ->join('feecategories','feecategories.id','=','feeparticular_paymets.feecategory_id')
                ->join('feeschedule_particulars','feeschedule_particulars.id','=','feeparticular_paymets.feeschedule_particular_id')
                ->join('students','students.user_id','=','feeparticular_paymets.user_id')
                ->join('users','users.id','=','feeparticular_paymets.user_id')
                ->join('particulars','particulars.id','=','feeparticular_paymets.feeparticular_id')
                ->whereIn('feeparticular_paymets.feepayment_id',['0'])
                ->where('feeschedule_particulars.start_date','<=',date('Y-m-d'))
                ->select(['users.name','students.roll_no','feecategories.title','feeschedule_particulars.start_date','feeschedule_particulars.end_date',DB::raw('sum(feeparticular_paymets.amount) as sum'),'feeparticular_paymets.user_id','users.parent_id'])
                ->groupBy('feeparticular_paymets.user_id')
                ->get();
      return Datatables::of($records)
        ->addColumn('action', function ($records) {
         
          $link_data = '<a href="'.URL_SEND_DUEREMAINDER_MAIL.'/'.$records->name.'/'.$records->roll_no.'/'.$records->title.'/'.$records->start_date.'/'.$records->end_date.'/'.$records->sum.'/'.$records->user_id.'/'.$records->parent_id.'" class="btn btn-primary">Send Notifications</a>';
            return $link_data;
            })
        ->editColumn('end_date', function($records)
        {
            if($records->end_date <= date('Y-m-d')){
                return $rec = '<span class="label label-danger">'.$records->end_date.'</span>';
            }
            else{

                return $rec = '<span class="label label-warning">'.$records->end_date.'</span>';   
            }
        })

        // ->editColumn('is_income', function($records)
        // {
        //     if($records->is_income==1){
        //         return $rec = '<span class="label label-info">'.getPhrase('Yes').'</span>';
        //     }
        //     else{

        //         return $rec = '<span class="label label-warning">'.getPhrase('no').'</span>';   
        //     }
        // })
        
        
        
        ->removeColumn('user_id')
        ->removeColumn('parent_id')
        ->make();
      echo "JSON RECORD".json_encode($record);
      exit;
      // $data['record'] = DB::table('email_list')
      //           ->get();
      // return view('emailnotifications.promotions.dueremainder',$data);
    }

    public function getEventNotifications()
    { 
      $data['active_class']       = 'eventnotifications';
      $data['layout']             = getLayout();
      $data['title']              = getPhrase('event_notifications');
      $roles                = \App\Role::select('display_name', 'id','name')->get();
      $data['roles']        = $roles;
      $slug = Auth::user()->slug;
      $user = App\User::where('slug','=',$slug)->first();
      $role = getRoleData($user->role_id);
      $data['record'] = DB::table('email_list')
                ->get();
      return view('emailnotifications.promotions.eventnotification',$data);
    }

    public function getCommunicationMail()
    { 
      $data['active_class']       = 'communicationmail';
      $data['layout']             = getLayout();
      $data['title']              = getPhrase('communication_mail');
      $slug = Auth::user()->slug;
      $user = App\User::where('slug','=',$slug)->first();
      $role = getRoleData($user->role_id);
      return view('emailnotifications.promotions.communicationmail',$data);
    }

     public function getPromotionsemails(Request $request)
    { 
      
      $data['emaillist'] = DB::table('promotion_email_list')
                ->where('email_listid',$request->id)
                ->get();
     return $data;
    }

     public function addemaillist(Request $request)
    { 

      $rules =[
          'list_title'          => 'bail|required',
          'list_des'           => 'bail|required',
          ];
       $this->validate($request, $rules);
      $values = array(
        'title' => $request->list_title,
        'description'=>$request->list_des,
        'created_at' => date('Y-m-d H:i:s') , 
        'updated_at' => date('Y-m-d H:i:s')
      );
      $record = DB::table('email_list')
      ->insert($values);
      if($record > 0){
         flash('success','record_added_successfully', 'success');
        //$response = response()->json(['status' => 'success', 'message' => 'List created']);
      }else{
          flash('Failed','Reords already Exixts', 'error');
        //$response = response()->json(['status' => 'failure', 'message' => 'Something went wrong!']);
      }
     return redirect(URL_GET_EMAILPROMOTIONS);
    }

    public function importList(Request $request){

      $rules = [
         'excel'               => 'bail|required' ,
            ];

        $this->validate($request, $rules);
        $values = [];
          if(Input::hasFile('excel')){
            $path = Input::file('excel')->getRealPath();
            $data = Excel::load($path, function($reader) {
            })->get();
            if(!empty($data) && $data->count()){
              foreach ($data as $key => $value) {
                foreach($value as $record)
                {
                  echo $record->email_listid;
                  echo $record->email_id;
                  $record1 = DB::table('promotion_email_list')
                            ->where('email_listid',$record->email_listid)
                            ->where('email_id',$record->email_id)
                            ->get();
                  echo count($record1);
                  if(count($record1) > 0){
                    echo "Welcome";
                  }
                  else{
                      echo " exit";
                      $values[] = ['email_listid' => $record->email_listid,
                                  'name' => $record->name,
                                  'email_id' => $record->email_id,
                                   'created_at' => date('Y-m-d H:i:s') , 
                                    'updated_at' => date('Y-m-d H:i:s')];
                      
                      

                  }
                 
                 
                }
              }
              if(count($values) > 0){

            $insert =   DB::table('promotion_email_list')
                                  ->insert($values);
                                  echo "uu".$insert;
                                  echo json_encode($values);
                      flash('success','record_added_successfully', 'success');
                    }else{
                       flash('Failed','Reords already Exixts', 'error');
                     
                    }
              
                       
            }
          }
    
     return redirect(URL_GET_EMAILPROMOTIONS);
 
    }

    public function sendPromotionmail(Request $request){

      $emails = $request->emails;
      // echo count($emails);
      // echo json_encode($emails);
      $content = json_encode([
                  'content' => $request->content]);
      $values = [];
      // foreach ($variable as $key => $value) {
      //   # code...
      // }
      foreach ($emails as $key => $value) {
          // echo json_encode($value['emailid']);
          if($value['emailid'] != "1"){
          $values[] = [
                  'email_to' => $value['emailid'],
                  'email_cc' => '',
                  'email_bcc'=>'',
                  'email_subject'=>'Promotions',
                  'email_content'=>$content,
                  'email_template'=>'emailtemplates.promotionemails',
                  'email_template_data'=>$content,
                  'is_sent'=>0,
                  'created_at' => date('Y-m-d H:i:s') , 
                  'updated_at' => date('Y-m-d H:i:s')
                ];
          }
      }
      $insert =   DB::table('email_queue')
                  ->insert($values);

      return response()->json(['status' => 'success', 'message' => 'Mail send Successfully']);

    }

    public function sendEventmail(Request $request){

      $type = $request->type;
      $eventtitle = $request->title;
      $eventstartdate = $request->startdate;
      $eventenddate = $request->enddate;
      $eventstarttime = $request->starttime;
      $eventendtime = $request->endtime;
      $eventdescription = $request->description;
      $eventvenue = $request->venue;
      $content = json_encode([
                  'title' =>$eventtitle,
                  'startdate'=>$eventstartdate,
                  'enddate'=>$eventenddate,
                  'starttime'=>$eventstarttime,
                  'endtime'=>$eventendtime,
                  'description'=>$eventdescription,
                  'venue'=>$eventvenue ]);

      $emails =  DB::table('users')
                ->where('role_id',$type)
                ->get();
      $values = [];
      // foreach ($variable as $key => $value) {
      //   # code...
      // }
      foreach ($emails as $value) {
          // echo json_encode($value['emailid']);

          $values[] = [
                  'email_to' => $value->email,
                  'email_cc' => '',
                  'email_bcc'=>'',
                  'email_subject'=>'Event Notification',
                  'email_content'=>$content,
                  'email_template'=>'emailtemplates.eventnotification',
                  'email_template_data'=>$content,
                  'is_sent'=>0,
                  'created_at' => date('Y-m-d H:i:s') , 
                  'updated_at' => date('Y-m-d H:i:s')
                ];
      }
      $insert =   DB::table('email_queue')
                  ->insert($values);

      return response()->json(['status' => 'success', 'message' => 'Notification Send Successfully']);

    }

    public function sendCommunicationmail(Request $request){

      $emails = $request->toemail;
      $content = json_encode([
                  'content' => $request->message]);
      $values = [];
      foreach ($emails as $key => $value) {
          // echo json_encode($value['emailid']);
          $values[] = [
                  'email_to' => $value['email'],
                  'email_cc' => $request->ccemail,
                  'email_bcc'=> $request->bccemail,
                  'email_subject'=>$request->subject,
                  'email_content'=>$content,
                  'email_template'=>'emailtemplates.promotionemails',
                  'email_template_data'=>$content,
                  'is_sent'=>0,
                  'created_at' => date('Y-m-d H:i:s') , 
                  'updated_at' => date('Y-m-d H:i:s')
                ];
      }
      $insert =   DB::table('email_queue')
                  ->insert($values);

      return response()->json(['status' => 'success', 'message' => 'Mail send Successfully']);

    }

    public function sendDueremaindermail($name,$roll_no,$title,$startdate,$end_date,$sum,$user_id,$parent_id){
      $emails = DB::table('users')
                ->where('id',$parent_id)
                ->first();
      $content = json_encode([
                  'studentname' => $name,
                  'rollno' => $roll_no,
                  'title' => $title,
                  'startdate' => $startdate,
                  'end_date' => $end_date,
                  'sum' => $sum,
                  'parentname'=>$emails->name]);
      $values = [];
      $values[] = [
              'email_to' => $emails->email,
              'email_cc' => '',
              'email_bcc'=> '',
              'email_subject'=>'Due Remainder Notifications',
              'email_content'=>$content,
              'email_template'=>'emailtemplates.promotionemails',
              'email_template_data'=>$content,
              'is_sent'=>0,
              'created_at' => date('Y-m-d H:i:s') , 
              'updated_at' => date('Y-m-d H:i:s')
            ];
      $insert =   DB::table('email_queue')
                  ->insert($values);
      flash(getPhrase('notification_send_successfully'));
      return redirect(URL_GET_EMAIL_DUEREMAINDER);
      
    }

    
}
