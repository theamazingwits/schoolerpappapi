<?php
namespace App\Http\Controllers;
use \App;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\HourlyTemplate;
use Yajra\Datatables\Datatables;
use DB;
use Exception; 

class HourlyTemplatesController extends Controller
{
        
    public function __construct()
    {
    	$this->middleware('auth');

    }


    public function index()
    {
        if(!checkRole(getUserGrade(18)))
        {
          prepareBlockUserMessage();
          return back();
        }

        $data['active_class']        = 'payroll';
        $data['title']               = getPhrase('hourly_templates');
        $data['layout']              = getLayout();

        return view('payroll.hourly_templates.list', $data);

        //    $view_name = getTheme().'::payroll.hourly_templates.list';
        // return view($view_name,$data);
    }

    /**
     * This method returns the datatables data to view
     * @return [type] [description]
     */
    public function getDatatable()
    {

         $records = HourlyTemplate::select([ 'hourly_grades','hourly_rate','slug']);
        
        return Datatables::of($records)
        ->addColumn('action', function ($records) {
           
            return '<div class="dropdown more">
                        <a id="dLabel" type="button" class="more-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-ellipsis-v"></i>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">

                            <li><a href="'.URL_PAYROLL_HOURLY_TEMPLATES_EDIT.$records->slug.'"><i class="fa fa-pencil"></i>'.getPhrase("edit").'</a></li>

                            <li><a href="javascript:void(0);" onclick="deleteRecord(\''.$records->slug.'\');"><i class="fa fa-trash"></i>'. getPhrase("delete").'</a></li>
                        </ul>
                    </div>';
            })
        ->removeColumn('slug')
        ->make();
    }


    /**
     * This method loads the create view
     * @return void
     */
    public function create()
    {
    	if(!checkRole(getUserGrade(18)))
        {
          prepareBlockUserMessage();
          return back();
        }

        $data['record']         	= FALSE;
    	$data['active_class']       = 'payroll';
    	$data['title']              = getPhrase('add_hourly_template');
        $data['layout']             = getLayout();

         return view('payroll.hourly_templates.add-edit', $data);

        //  $view_name = getTheme().'::payroll.hourly_templates.add-edit';
        // return view($view_name,$data);
    }


    /**
     * This method loads the edit view based on unique slug provided by user
     * @param  [string] $slug [unique slug of the record]
     * @return [view with record]       
     */
    public function edit($slug)
    {
    	if(!checkRole(getUserGrade(18)))
        {
          prepareBlockUserMessage();
          return back();
        }

        $record = HourlyTemplate::where('slug', $slug)->get()->first();
        $data['record']             = $record;
    	$data['active_class']       = 'payroll';
        $data['title']              = getPhrase('edit_hourly_template');
        $data['layout']              = getLayout();

    	 return view('payroll.hourly_templates.add-edit', $data);

        //   $view_name = getTheme().'::payroll.hourly_templates.add-edit';
        // return view($view_name,$data);
    }


    /**
     * Update record based on slug and reuqest
     * @param  Request $request [Request Object]
     * @param  [type]  $slug    [Unique Slug]
     * @return void
     */
    public function update(Request $request, $slug)
    {

        if(!checkRole(getUserGrade(18)))
        {
          prepareBlockUserMessage();
          return back();
        }

        $record  = HourlyTemplate::where('slug', $slug)->first();
        
          $this->validate($request, [
                    'hourly_grades'     => 'bail|required|max:30|unique:hourly_templates,hourly_grades,'. $record->id ,
                    'hourly_rate'       => 'required|numeric',
                    ]);


        $name = $request->hourly_grades;
       
       /**
        * Check if the title of the record is changed, 
        * if changed update the slug value based on the new title
        */
        if($name != $record->hourly_grades)
            $record->slug = $record->makeSlug($name);
    	
        $record->hourly_grades 	    = $name;
        $record->hourly_rate        = $request->hourly_rate;
        
        $record->save();


    	flash('success','record_updated_successfully', 'success');
    	return redirect(URL_PAYROLL_HOURLY_TEMPLATES);
    }

    /**
     * This method adds record to DB
     * @param  Request $request [Request Object]
     * @return void
     */
    public function store(Request $request)

    {

        if(!checkRole(getUserGrade(18)))
        {
          prepareBlockUserMessage();
          return back();
        }

        $this->validate($request, [
                    'hourly_grades'     => 'bail|required|max:30|unique:hourly_templates',
                    'hourly_rate'       => 'required|numeric',
                    ]);

    	$record = new HourlyTemplate();
        $hourly_grades			    = $request->hourly_grades;
        $record->hourly_grades 	    = $hourly_grades;
        $record->hourly_rate        = $request->hourly_rate;
        $record->slug 			    = $record->makeSlug($hourly_grades);
        $record->save();


        flash('success','record_added_successfully', 'success');
    	return redirect(URL_PAYROLL_HOURLY_TEMPLATES);
    }


    /**
     * Delete Record based on the provided slug
     * @param  [string] $slug [unique slug]
     * @return Boolean 
     */
    public function delete($slug)
    {
        if(!checkRole(getUserGrade(18)))
        {
          prepareBlockUserMessage();
          return back();
        }

        try{
             if(!env('DEMO_MODE')) {

                HourlyTemplate::where('slug', $slug)->delete();
            }
            $response['status'] = 1;
            $response['message'] = getPhrase('record_deleted_successfully');
        }
        catch(Exception $e)
        {
           $response['status'] = 0;
           if(getSetting('show_foreign_key_constraint','module'))
            $response['message'] =  $e->getMessage();
           else
            $response['message'] =  getPhrase('this_record_is_in_use_in_other_modules');
        }
        return json_encode($response);
    }

}
