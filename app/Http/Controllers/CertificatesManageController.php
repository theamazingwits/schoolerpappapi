<?php

namespace App\Http\Controllers;

use \App;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\AdminNotification;
use App\UserCertificates;
use App\User;
use Yajra\Datatables\Datatables;
use DB;
use Auth;
use Carbon\Carbon;
use Exception;
use Response;



class CertificatesManageController extends Controller
{
    
    public function __construct()
    {
    	$this->middleware('auth');
    }


    protected $final_user_id = 0;

    public function setUserId($user_id)
    {
    	$this->final_user_id  = $user_id;
    }

    public function getUserId()
    {
    	return $this->final_user_id;
    }

    
    public function index($notification_id, $user_id = "")
    {
    	 
        $record               = AdminNotification::find($notification_id); 
        $data['record']       = $record;
        $data['active_class'] = 'dashboard';
        $data['layout']       = getLayout();
        $data['title']        = getPhrase('upload_certificates');

        $myuser_id  = Auth::user()->id;
        if($user_id)
        $myuser_id   = $user_id;

        $data['user_id']  = $myuser_id;

        return view('user-certificates.add-edit',$data);
    }


    public function store(Request $request)
    {
         
          $columns = array(
           
            'certificate_names'     => 'bail|required',
          );
    
          $this->validate($request,$columns);


	      try { 

			      $names   = $request->certificate_names;
		          $user_id = $request->user_id;
		          $user    = User::find($user_id);


		         if( $request->hasFile('certificate_files' ) )  
		         {

		         	  //Previos Records
		               UserCertificates::where('notification_id',$request->notification_id)
				    	                ->where('user_id',$user_id)
				    	                ->delete();

		          $i=1;

		          foreach ($request->certificate_files as $key => $user_document) 
		          {

		          	   $columns = array(
				        
				           'user_document'    => 'bail|mimes:png,jpg,jpeg,PNG,JPG,JPEG,pdf,PDF|max:2048',
				        );
				       
				        $this->validate($request,$columns);

			          $destinationPath = "public/uploads/user-certificates";

			          $fileName        = $user_id.'_user_document'.'._'.$key.$user_document->getClientOriginalName();

		              $user_document->move($destinationPath, $fileName);

		            


		              $record                   = new UserCertificates();
		              $record->notification_id  = $request->notification_id;

		              if(array_key_exists( $key, $names) ){
		               
		                $record->name  = $names[$key];
		              }

		              $record->is_submitted = 1;
		              $record->image        = $fileName;
		              $record->user_id      = $user_id;
		              $record->role_id      = getRoleId($user_id);
		              $record->save();

		           }
		          
		        }
		        else{

		        	flash('Oops..!','please_select_files','overlay');
		        	return back();
		        }
                

		         flash('success','certificates_submitted_successfully','success');
                 return redirect( URL_USER_UPLODS.$user->role_id.'/'.$user->id );
				
         } 
 		 catch (Exception $e) {
            

		    flash('Oops..!','something_went_wrong_try_again_once','overlay');
		    return back();		
	     }

    }


      /**
     * Course listing method
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function userUploads($role_id, $user_id)
    {
     
         $data['active_class'] = 'users';
        if(checkRole(getUserGrade(5)))
        $data['active_class'] = 'user-uploads';
        $data['layout']       = getLayout();
        $data['role_id']      = $role_id;    
        $data['user_id']      = $user_id;
        $user  = getUserRecord($user_id);
        $data['title']        = ucwords($user->name).' - '.getPhrase('uploads');

        return view('user-certificates.list', $data);

          
    }

    /**
     * This method returns the datatables data to view
     * @return [type] [description]
     */
    public function getDatatable($role_id, $user_id)
    {

        $this->setUserId($user_id);

        $records = array();

        if($role_id == 5){


            $student  = App\Student::where('user_id','=',$user_id)->first();

            $academic_id        = $student->academic_id;       
            $course_parent_id   = $student->course_parent_id;       
            $course_id          = $student->course_id;       
            $year               = $student->current_year;       
            $semister           = $student->current_semister;  
             
         
            $records = AdminNotification::select(['title','description','slug','id'])
                                        ->where('role_id',$role_id)
                                        ->where('academic_id',$academic_id)
                                        ->where('course_id',$course_id)
                                        ->where('current_year',$year)
                                        ->where('current_semister',$semister);
        }else{

               $records = AdminNotification::select(['title','description','slug','id'])
                                             ->where('role_id',$role_id);
        }

      
    
            
        $records->orderBy('updated_at', 'desc');
             

        return Datatables::of($records)

        ->addColumn('status', function ($records) {

             $user_id  = $this->getUserId();
             $record   = UserCertificates::where('notification_id',$records->id)
                                          ->where('user_id',$user_id)
                                          ->orderby('updated_at','desc')
                                          ->first();
            if($record){

             if($record->is_approved == 1){

             	 return '<span class="label label-success">Approved</span>';
             }
             elseif ($record->is_resubmitted == 1) {

                return '<span class="label label-warning">Rejected</span>';
             }else{
             	
                return '<span class="label label-info">Submitted</span>';
             }

           }
           else{

           	    return '<span class="label label-primary">Not Submitted</span>';
           }                              
      
                                       
        }) 

        ->addColumn('action', function ($records) {
          
          $user_id  = $this->getUserId();

              return '<a href="'.URL_VIEW_UPLOADED_CERTIFICATES.$records->slug.'/'.$user_id.'" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i> View</a>

                   <a href="'.URL_UPLOAD_USER_CERTIFICATES.$records->id.'/'.$user_id.'" class="btn btn-info btn-sm"><i class="fa fa-cloud-upload"></i> Upload </a>';

        
         })

        ->editColumn('title', function($records){
          
           $user_id  = $this->getUserId();

           return '<a href="'.URL_VIEW_UPLOADED_CERTIFICATES.$records->slug.'/'.$user_id.'" >'.ucwords($records->title).'</a>';
        }) 

        ->removeColumn('slug')
        ->removeColumn('id')
        ->make();
    }



    public function viewCertificates($slug, $user_id)
    {
       
         $loggedin_user_id  = Auth::user()->id;
        

        if($user_id != $loggedin_user_id){
           
           if(checkRole(getUserGrade(5))){

           	   prepareBlockUserMessage();
               return back();

           }
        	  
        }


    	$record               = AdminNotification::where('slug',$slug)->first(); 
    	$uploads              = UserCertificates::where('notification_id',$record->id)
    	                                         ->where('user_id',$user_id)
    	                                         ->get();
    	 $any_approved   = UserCertificates::where('notification_id',$record->id)
    	                                         ->where('user_id',$user_id)
    	                                         ->where('is_approved',1)
    	                                         ->get()->count();

    	$any_rejected   = UserCertificates::where('notification_id',$record->id)
    	                                         ->where('user_id',$user_id)
    	                                         ->where('is_resubmitted',1)
    	                                         ->get()->count();
    	// dd($uploads);                                        
        $data['any_approved'] = $any_approved;
        $data['any_rejected'] = $any_rejected;
        $data['record']       = $record;
        $data['uploads']      = $uploads;
        $data['active_class'] = 'users';
        if(checkRole(getUserGrade(5)))
        $data['active_class'] = 'user-uploads';
        $data['layout']       = getLayout();
        $data['title']        = getPhrase('uploaded_certificates');

        $data['user']      = User::find($user_id);

        $status_record   = UserCertificates::where('notification_id',$record->id)
                                          ->where('user_id',$user_id)
                                          ->orderby('updated_at','desc')
                                          ->first();

        $data['status_record'] = $status_record;
        return view('user-certificates.view-files',$data);
    }


    public function downloadCertificate($image)
    {
    	
    	try {
          

          $path   = public_path()."/uploads/user-certificates/".$image;
       
          return Response::download($path);

        } catch (Exception $e) {
           
           flash('Ooops..!','file_is_not_found_please_upload_new_one','overlay');
           return back();
        } 
    }


    public function approveCertificates(Request $request)
    {  

    	  if(!checkRole(getUserGrade(1))){

           	   prepareBlockUserMessage();
               return back();

           }
    	
    	$user_id         = $request->user_id;
    	$notification_id = $request->notification_id;
    	$admin_status    = $request->admin_status;

    	$records   = UserCertificates::where('notification_id',$notification_id)
    	                               ->where('user_id',$user_id)
    	                               ->get();
        
        if($admin_status == 'approve'){

	         foreach ($records as $record) 
	    	 {
	    	     
	    	     $record->is_approved    = 1;
	    	     $record->is_submitted   = 1;
	    	     $record->is_resubmitted = 0;
	    	     $record->approved_by    = Auth::user()->id;
	    	     $record->save();

	    	 }  

	    	 flash('success','certificates_are_approved','success');
       }
       elseif($admin_status == 'reject'){


	         foreach ($records as $record) 
	    	 {
	    	     
	    	     $record->is_resubmitted   = 1;
	    	     $record->is_submitted     = 0;
	    	     $record->is_approved      = 0;
	    	     $record->approved_by      = Auth::user()->id;
	    	     $record->save();

	    	 }  

	    	 flash('success','certificates_are_rejected','overlay');
       }
	    	 return redirect(URL_USERS."student");
    	                             
    }


}    