<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use App\Http\Requests;
use App\User;
use App\AlumniDetails;
use Carbon\Carbon;
use DB;
use Exception;

class SiteController extends Controller
{
     
     public function login()
     {
     	$data['record']       = FALSE;
        $data['active_class'] = 'login';
        $data['mystories']    = TRUE;
        $data['title']        = getPhrase('login');
        $data['years']        = getAlumniYears();
        $data['profession']   = getAlumniProfession();
        
        return view('alumini.login', $data);
     }


     public function register(Request $request)
     {

     	 $rules = [

		         'name'   => 'bail|required|max:60' ,
		         'class'  => 'bail|required',
		         'email'    => 'bail|required|unique:users,email',
		         'password'  => 'bail|required',
		         'mobile_number'  => 'bail|required',
		         'profession'  => 'bail|required',
		         'social_link'  => 'bail|required',

            ];

            $custMsg  = ['class.required' => 'Passed out year required'];

		        $this->validate($request, $rules, $custMsg);
		     	// dd($request);

		     	$user           = new User();
		        $name           = $request->name;
		        $user->name     = $name;
		        $user->email    = $request->email;
		        $password       = $request->password;
		        $user->password = bcrypt($password);
		     
		        $user->role_id        = 14;
		        $user->login_enabled  = 0;
		        $slug                 = $user->makeSlug($name);
		        $user->username       = $request->name;
		        $user->slug           = $slug;
		        $user->phone          = $request->mobile_number;
		        $user->alumni_class   = $request->class;
		        $user->alumni_profession = $request->profession;
		        $user->alumni_social   = $request->social_link;
		        $user->is_alumni      = 1;
		        $user->save();

		        $user->roles()->attach($user->role_id);

		        $alumni           = new AlumniDetails();
		        $alumni->user_id  = $user->id;
		        $alumni->save();
		         $link  = URL_USERS_LOGIN;
		        
		        $super_admin   = User::where('role_id',1)->first();


		           try 
			        {
			            if (!env('DEMO_MODE')) {

			             $user->notify(new \App\Notifications\NewUserRegistration($user,$user->email,$password ));
			             $super_admin->notify(new \App\Notifications\OwnerNewUserRegistration($super_admin,$user->email,$password,$link ));
			            }

			        }
			        catch(Exception $ex)
			        {
			         // dd($ex->getMessage());
			        }

		        flash('success','account_added_successfully_you_need_admin_approval_to_access_your_account','overlay');
		        return redirect(URL_ALUMINI_LOGIN);
     }
}