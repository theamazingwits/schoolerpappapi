<?php

namespace App\Http\Controllers\API;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Auth;

class GeneralController extends Controller
{
     /**
       * This method identifies the user type logged in and prints the appropriate timetable
       * for logged in user
       * @param  [type] $id [description]
       * @return [type]       [description]
       */
      public function StaffStudentTimetable($id)
      { 

        $user_record = \App\User::where('id', '=', $id)->first();
    

        $academic_id    = getDefaultAcademicId();
        $course_id      = 0;
        $year           = 0;
        $semister       = 0;
        $notes          = '';
        $timetable_title = '';

        $role = getRoleData($user_record->role_id);
        if($role=='staff')
        {
            $user_id        = $user_record->id;
            $timetable_title = getPhrase('timetable_for'). ' '.ucfirst($user_record->name);
          
        }

        if($role=='student')
        {

            $user_id        = 0;
            $record         = $this->StudentSessionRecord($id);

            if(!$record->student->roll_no)
            {   

            	 $response['status']  = 0;
                 $response['message'] = 'Student Roll Number Is Not Generated';
                 return $response;

            }

            $academic_id    = $record->student->academic_id;
            $course_id      = $record->student->course_id;
            $year           = $record->student->current_year;
            $semister       = $record->student->current_semister;


           $academic_record = \App\Academic::where('id', '=', $academic_id)
                              ->select('academic_year_title')->first();
           $course_record = \App\Course::where('id', '=', $course_id)
                            ->select(['id','parent_id','course_title','course_dueration', 'is_having_semister'])->first();
            
           $course_parent_record = \App\Course::where('id', '=', $course_record->parent_id)
                            ->select('course_title')->first();


             $timetable_title         = getPhrase('timetable_for')
                                .' '.$academic_record->academic_year_title
                                .' '.$course_parent_record->course_title
                                .' '.$course_record->course_title;
        if($course_record->course_dueration>1) {                                
            $timetable_title .= ' '.$year.' '.getPhrase('year');
        }

        if($course_record->is_having_semister && $semister) {                                
            $timetable_title .= ' '. $semister.' '.getPhrase('semister');
        }

        }
      
       $data['timetable_title'] = $timetable_title;
        $days = getDay();
       
        if($user_id)
        {
            //Print Staff Timetable
            $allocated_periods = (object)$this->getSchedules($academic_id,0,0,0,$user_id);
          
           if(!count($allocated_periods->timemaps[1]['timeset'])) {

           	   $response['status']  = 0;
               $response['message'] = 'Time table is not allocated for you';
                 return $response;

          
          }
        
        }
        else{

            //Print Class timetable
         $allocated_periods = (object)$this->getSchedules(
                                $academic_id,
                                $course_id,
                                $year,
                                $semister,
                                $user_id);
        }

        if(!count($allocated_periods->timemaps[1]['timeset'])){
           
           $response['status']  = 0;
           $response['message'] = 'Time table is not created for your class';
                 return $response;

       
        }
           
  
           $response['status']             = 1;
           $response['message']            = '';
           // $response['allocated_periods']  = $allocated_periods;
           $response['notes']              = $notes;
           $response['timetable_title']    = $timetable_title;

           $periods =[];
           $day=0;
           $allperiods = $allocated_periods->maximum_periods_set;
           $days = $allocated_periods->days;


           $timemaps = $allocated_periods->timemaps;


           $temp = [];
           $days_list = [];
           foreach( $timemaps as $record)
           {

            $record = (object)$record;
            $key = $days[$day++]['day'];
            $days_list[] = $key;
            $temp = [];
            $timeset = $record->timeset;
            $plist = $record->periods;
              // dd($plist);
            $temp = [];
            $mylist = [];
            for($i=0; $i<count($timeset); $i++)
            {   
                $temp['period_name'] = $timeset[$i]->period_name;
                $temp['start_time'] = $timeset[$i]->start_time;
                $temp['end_time'] = $timeset[$i]->end_time;
                $temp['is_break'] = $timeset[$i]->is_break; 

                $temp['course_title'] = $plist[$i]['course_title']; 
                $temp['subject_title'] = $plist[$i]['subject_title'];    
                $temp['subject_code'] = $plist[$i]['subject_code'];    
                $temp['is_lab'] = $plist[$i]['is_lab'];    
                $temp['is_elective'] = $plist[$i]['is_elective'];    
                $mylist[] = $temp;
            
            }
            
            // $plist = [];
            // foreach($record->periods as $p)
            // {
            //     $pdata = [];
            //     $pdata['subject_title'] = $p->subject_title;
            //     $pdata['subject_code'] = $p->subject_code;
            //     $pdata['is_lab'] = $p->is_lab;
            //     $pdata['is_elective'] = $p->is_elective;
            //     $plist[] = $pdata;
            // }
            
            // $temp[$key]['periods'] = $plist;

            $periods[$key] = $mylist;
           }

            
           $response['days_list'] = $days_list;
           $response['periods'] = $periods;

        return $response;
      

     }



           /**
     * This method will return the detailed scheduled report for the week
     * with all users or a specific user
     * @param  [type]  $academic_id [description]
     * @param  [type]  $course_id   [description]
     * @param  integer $year        [description]
     * @param  integer $user_id     [description]
     * @return [type]               [description]
     */
    public function getSchedules($academic_id =1, $course_id=1, $year=1,$semister=0, $user_id = 0)
    {
       
        //Collect the set of mapping records for particular
        //academic year, course and year
        $map_records = \App\TimingsetMap::
                        join('timingset', 'timingset.id', '=', 'timingset_id');
      
        $map_records = $map_records->select(['timingsetmap.id as map_id', 'timingset.id as timingset_id', 'timingsetmap.day','name as timeset_name'])
                        ->get();

        $timemaps = [];
        $days = [];

        //Find the maximum periods per day
        $maximum_periods_in_day = [];
        foreach($map_records as $record)
        {
            $days[] = array('day_id' => $record->day, 'day'=>getDay($record->day));
            $timemaps[$record->day]['record'] = $record;
            $timeset_details = \App\TimingsetDetails::
                                where('timingset_id', '=', $record->timingset_id)
                                ->select(['id', 'period_name', 'start_time', 'end_time', 'is_break','timingset_id'])
                                ->get();
            if(count($timeset_details)>count($maximum_periods_in_day))
                $maximum_periods_in_day = $timeset_details;

            $timemaps[$record->day]['timeset'] = $timeset_details;
            $period_details = [];
           
           //Prepare the list of classes/periods available with detailed view
            foreach($timeset_details as $details) {
           
            $timetable_record = \App\Timetable::
                      join('users', 'users.id', '=', 'timetable.user_id')
                      ->join('staff', 'users.id', '=', 'staff.user_id')
                      ->join('subjects', 'subjects.id', '=', 'timetable.subject_id');
            if($user_id == 0) {
             $timetable_record = $timetable_record
                      ->where('timetable.academic_id', '=', $academic_id)
                      ->where('timetable.course_id', '=', $course_id)
                      ->where('timetable.year', '=', $year)
                      ->where('timetable.semister', '=', $semister);
 
            }
            $timetable_record = $timetable_record->where('day', '=', $record->day)
                      ->where('timetable.academic_id', '=', $academic_id)
                      ->where('timingset_id', '=', $record->timingset_id)
                      ->where('timingset_map_id', '=', $record->map_id)
                      ->where('timingset_details_id', '=', $details->id);
            
            if($user_id!=0)
            $timetable_record = $timetable_record->where('timetable.user_id','=', $user_id);
            $timetable_record = $timetable_record->select([
                'timetable.academic_id as academic_id', 
                'timetable.course_id as course_id', 
                'timetable.year as year',
                'timetable.day as day',
                'timetable.timingset_id as timingset_id',
                'timetable.timingset_map_id as timingset_map_id',
                'timetable.timingset_map_id as map_id',
                'timetable.user_id as staff_id',
                'timetable.subject_id as subject_id',
                'users.id as user_id',
                'users.name',
                'users.image',
                'subjects.subject_title',
                'subjects.subject_code',
                'subjects.is_lab',
                'subjects.is_elective_type as is_elective',
                ]);
            $timetable_record = $timetable_record->first();
            $period_details['is_assigned'] = ($timetable_record) ? 1 : 0;
            $period_details['day_number'] = $record->day;
            $period_details['user_id'] = 
                            ($timetable_record) ? $timetable_record->user_id :null;
            $period_details['academic_id'] = 
                            ($timetable_record) ? $timetable_record->academic_id :null;
            $period_details['course_id'] = 
                            ($timetable_record) ? $timetable_record->course_id :null;
            $period_details['year'] = 
                            ($timetable_record) ? $timetable_record->year :null;
            $period_details['semister'] = 
                            ($timetable_record) ? $timetable_record->semister :null;
            
            $period_details['staff_id'] = 
                            ($timetable_record) ? $timetable_record->staff_id :null;

            $period_details['name'] = ($timetable_record) ? $timetable_record->name :null;
            $period_details['image'] = ($timetable_record) ? $timetable_record->image :null;
            $period_details['subject_id'] = ($timetable_record) ? $timetable_record->subject_id :null;
            $period_details['subject_title'] = ($timetable_record) ? $timetable_record->subject_title :null;
            $period_details['subject_code'] = ($timetable_record) ? $timetable_record->subject_code :null;
            $period_details['is_lab'] = ($timetable_record) ? $timetable_record->is_lab :null;;
            $period_details['is_elective'] = ($timetable_record) ? $timetable_record->is_elective :null;
            $period_details['is_break'] = $details->is_break;
            $period_details['period_name'] = $details->period_name;

            $period_details['id'] = 
                                    $academic_id
                                    .'_'.$course_id
                                    .'_'.$year
                                    .'_'.$semister
                                    .'_'.$record->day
                                    .'_'.$record->timingset_id
                                    .'_'.$record->map_id
                                    .'_'.$details->id;


            if($user_id==0) {
                $period_details['course_title'] = $this->prepareCourseTitle($academic_id,$course_id, $year, $semister);
               

            }
            else {

            $period_details['course_title'] = $this->prepareCourseTitle($period_details['academic_id'], $period_details['course_id'], $period_details['year'], $period_details['semister']);

                 $period_details['id'] = 
                                    $period_details['academic_id']
                                    .'_'.$period_details['course_id']
                                    .'_'.$period_details['year']
                                    .'_'.$period_details['semister']
                                    .'_'.$record->day
                                    .'_'.$record->timingset_id
                                    .'_'.$record->map_id
                                    .'_'.$details->id;


            }
            $timemaps[$record->day]['periods'][] = $period_details;
            }
        }

         $data['days'] = $days;
       
        $data['timemaps'] = $timemaps;

        $data['maximum_periods_set'] = $maximum_periods_in_day;
        return $data;
    }


   public function StudentSessionRecord($id='')
  {
    if($id=='')
    {
        $id = Auth::user()->id;
    }
    
    $user = \App\User::where('id','=',$id)->first();
    $user_record = [];
    $user_record['user'] = $user;
    $user_record['student'] = \App\Student::where('user_id','=', $user->id)->first();
    
    return (object) $user_record;
  }


   public function prepareCourseTitle($academic_id, $course_id, $year=1, $semister=0)
    {
        $course_title = '';
        if(!$course_id)
            return $course_title;
       $course_title = '';
        if(!$academic_id)
            return $course_title;
        $academic_record = \App\Academic::where('id','=',$academic_id)->first();
        
        if(!$academic_record)
            return $course_title;

        $course_record = \App\Course::where('id','=',$course_id)->first();
        if(!$course_record)
            return $course_title;

        $course_title = '('.$academic_record->academic_year_title.')'.$course_record->course_title;
        if($course_record->course_dueration>1)
            $course_title .=  ' '.$year;
        if($course_record->is_having_semister && $semister !=0)
            $course_title .= '-'.$semister;
        return $course_title;
    }



    // public function getStudentDayClasses($user_id=0, $date='', $day_no = -1, $limit=10)
    

    public function getStudentDayClasses(Request $request)
    {
        $limit=10;
        $user_id = $request->user_id;
        $date = $request->date;

        $academic_id = getDefaultAcademicId();
         
        //date 
        $selected_date = strtotime($date);
        $day_no  = date('w', $selected_date);

        if($day_no==-1)
            $day_no = date('N');
        if(!$user_id)
            $user_id = Auth::user()->id;

         $student_record = \App\Student::where('user_id', '=', $user_id)->first();

         $class_name  = studentClassName($student_record);

        $records  =  \App\Timetable::join('subjects', 'subjects.id','=','timetable.subject_id')
                        ->join('courses','courses.id','=', 'timetable.course_id')
                        ->join('timingsetdetails','timingsetdetails.id', 
                            '=', 'timetable.timingset_details_id')
                        ->join('users','users.id','=','timetable.user_id')
                        ->join('staff','staff.user_id','=','users.id')
                        ->join('course_subject', 'course_subject.subject_id','=','subjects.id')
                        ->where('timetable.academic_id','=',$academic_id)
                        ->where('timetable.course_id', '=', $student_record->course_id)
                        ->where('timetable.year', '=', $student_record->current_year)
                        ->where('timetable.semister', '=', $student_record->current_semister)
                        ->where('day', '=', $day_no)
                        ->where('course_subject.academic_id','=',$academic_id)
                        ->where('course_subject.course_id','=', $student_record->course_id)
                        ->where('course_subject.year','=', $student_record->current_year)
                        ->where('course_subject.semister','=',$student_record->current_semister)
                        ->select(['subjects.id as subject_id', 'subjects.subject_title','course_subject.slug as subject_slug','course_title','start_time','end_time','users.name as staff_name','users.image as staff_image','staff.staff_id','qualification','is_lab','is_elective_type','timetable.academic_id','timetable.course_id','timetable.year','timetable.semister','users.id as user_id'])
                        ->limit($limit)
                        ->orderBy('start_time')
                        ->get();

          foreach ($records as $record) {
              
              $course_subject  = \App\CourseSubject::where('academic_id',$record->academic_id)
                                             ->where('course_id',$record->course_id)
                                             ->where('year',$record->year)
                                             ->where('semister',$record->semister)
                                             ->where('subject_id',$record->subject_id)
                                             ->where('staff_id',$record->user_id)
                                             ->first();
          if($course_subject){
             $percentage [] =  $this->getSubjectCompletedStatus($record->subject_id,$record->user_id, $course_subject->id);
          }else{
             $percentage [] = 0;
          }
            
          }              

              // return $data['data']  = $percentage;

          if( count($records) > 0 ){
          
           $response['status']     = 1;
           $response['message']    = '';
           $response['data']       = $records;
           $response['percentage'] = $percentage;
           $response['class_name'] = $class_name;

        }else{
             
           $response['status']  = 0;
           $response['message'] = 'No records available';
        }

        return $response;


     }


 /**
     * This method will return the detailed report for topics
     * which are completed from total available topics
     * It also returns the status in %
     * @param  [type] $subject_id [description]
     * @return [type]             [description]
     */
    public function getSubjectCompletedStatus($subject_id, $user_id, $course_subject_id)
    {
      //Get the Topics count with the given subject id
      // return $data['test']  = getDefaultAcademicId();
      $total_topics = \App\Topic::where('subject_id', '=', $subject_id)
                                 ->where('parent_id','!=',0)
                                 ->count();
      $completed_topics = \App\LessionPlan::join('course_subject',
                          'course_subject.id', '=', 'course_subject_id')
                        ->where('academic_id','=',getDefaultAcademicId()) 
                        ->where('subject_id','=',$subject_id) 
                        ->where('staff_id', '=', $user_id)
                        ->where('lessionplans.is_completed', '=', 1)
                        ->where('course_subject.id', '=', $course_subject_id)
                        ->count();

        $percent_completed = 0;
        if($total_topics)
          $percent_completed = ($completed_topics*100)/$total_topics;

        return round($percent_completed);


    }


    public function getStaffDetails($user_id)
    { 
      
       $record  = User::join('staff','staff.user_id','=','users.id')
                        ->where('users.id',$user_id)
                        ->first();
       
        if( $record ){
          
           $response['status']     = 1;
           $response['message']    = '';
           $response['data']       = $record;

        }else{
             
           $response['status']  = 0;
           $response['message'] = 'No record available';
        }

        return $response;
    }



    public function SubjectLessonPlans(Request $request)
    {  

       
       $course_subject  = \App\CourseSubject::where('academic_id',$request->academic_id)
                                             ->where('course_id',$request->course_id)
                                             ->where('year',$request->year)
                                             ->where('semister',$request->semister)
                                             ->where('subject_id',$request->subject_id)
                                             ->where('staff_id',$request->user_id)
                                             ->first();
         
      

       
       if( $course_subject ){

         $lessionplans  = \App\LessionPlan::join('topics','topics.id','=','lessionplans.topic_id')
                                        ->where('course_subject_id',$course_subject->id)
                                        ->get();  
          
           $response['status']       = 1;
           $response['message']      = '';
           $response['lessionplans'] = $lessionplans;

        }else{
             
           $response['status']  = 0;
           $response['message'] = 'No record available';
        }

        return $response;                                 
    }


    public function testStudentTimetable($id)
    { 

        $user_record = \App\User::where('id', '=', $id)->first();
    

       /* $response['status']             = 1;
        $response['message']            = 'USER RECORD';
        $response['user_record']        = $user_record;
        return $response;*/


        $academic_id    = getDefaultAcademicId();
        $course_id      = 0;
        $year           = 0;
        $semister       = 0;
        $notes          = '';
        $timetable_title = '';

        $role = getRoleData($user_record->role_id);


       /* $response['status']             = 1;
        $response['message']            = 'USER ROLE';
        $response['user_record']        = $role;
        return $response;*/


        if($role=='staff')
        {
            $user_id        = $user_record->id;
            $timetable_title = getPhrase('timetable_for'). ' '.ucfirst($user_record->name);              
        }



        if($role=='student')
        {

            $user_id        = 0;
            $record         = $this->StudentSessionRecord($id);

            if(!$record->student->roll_no)
            {   

                 $response['status']  = 0;
                 $response['message'] = 'Student Roll Number Is Not Generated';
                 return $response;

            }

            /*$response['status']             = 1;
            $response['message']            = 'STUDENT SESSION RECORD';
            $response['user_record']        = $record;
            return $response;*/


            $academic_id    = $record->student->academic_id;
            $course_id      = $record->student->course_id;
            $year           = $record->student->current_year;
            $semister       = $record->student->current_semister;


            $academic_record = \App\Academic::where('id', '=', $academic_id)
                              ->select('academic_year_title')->first();


            /*$response['status']             = 1;
            $response['message']            = 'ACADEMIC RECORD';
            $response['user_record']        = $academic_record;
            return $response;*/


            $course_record = \App\Course::where('id', '=', $course_id)
                            ->select(['id','parent_id','course_title','course_dueration', 'is_having_semister'])->first();


            /*$response['status']             = 1;
            $response['message']            = 'COURSE RECORD';
            $response['user_record']        = $course_record;
            return $response;*/


            
            $course_parent_record = \App\Course::where('id', '=', $course_record->parent_id)
                            ->select('course_title')->first();



           /* $response['status']             = 1;
            $response['message']            = 'COURSE PARENT RECORD';
            $response['user_record']        = $course_parent_record;
            return $response;*/


            $timetable_title         = getPhrase('timetable_for')
                                .' '.$academic_record->academic_year_title
                                .' '.$course_parent_record->course_title
                                .' '.$course_record->course_title;


            if ($course_record->course_dueration>1) {                                
                $timetable_title .= ' '.$year.' '.getPhrase('year');
            }

            if ($course_record->is_having_semister && $semister) {                                
                $timetable_title .= ' '. $semister.' '.getPhrase('semister');
            }


           /* $response['status']             = 1;
            $response['message']            = 'COURSE TIMETABLE TITLE';
            $response['user_record']        = $timetable_title;
            return $response;*/

        }

      
        $data['timetable_title'] = $timetable_title;
        $days = getDay();


        /*$response['status']             = 1;
        $response['message']            = 'DAYS';
        $response['days']               = $days;
        return $response;*/
       
        if($user_id)
        {
            //Print Staff Timetable
            $allocated_periods = (object)$this->getSchedules($academic_id,0,0,0,$user_id);

           if(!count($allocated_periods->timemaps[1]['timeset'])) {

               $response['status']  = 0;
               $response['message'] = 'Time table is not allocated for you';
                 return $response;
          }
        
        }
        else{


        //Print Class timetable
         $allocated_periods = (object)$this->getSchedules(
                                $academic_id,
                                $course_id,
                                $year,
                                $semister,
                                $user_id);


       /* $response['status']             = 1;
        $response['message']            = 'ALLOCATED PERIODS';
        $response['data']               = $allocated_periods;
        return $response;*/


        }

        if(!count($allocated_periods->timemaps[1]['timeset'])){
           
           $response['status']  = 0;
           $response['message'] = 'Time table is not created for your class';
                 return $response;

       
        }
           
  
           $response['status']             = 1;
           $response['message']            = '';
           // $response['allocated_periods']  = $allocated_periods;
           $response['notes']              = $notes;
           $response['timetable_title']    = $timetable_title;

           $periods =[];
           $day=0;
           $allperiods = $allocated_periods->maximum_periods_set;
           $days = $allocated_periods->days;


           /* $response['status']             = 1;
            $response['message']            = 'DAYS';
            $response['data']               = $days;
            return $response;*/


            $timemaps = $allocated_periods->timemaps;


            //get p1 class each day
            /*$all_rows=[];
            foreach($days as $day_key)
            {
                $period1=[];

                foreach($i=0;$i<count($timemaps);$i++) {

                   //get record
                   $record = $timemaps[$i];

                   $response['status']             = 1;
                   $response['message']            = 'DAYS';
                   $response['data']               = $record;
                   return $response;

                }

            }*/



            $temp = [];
            $days_list = [];


            /*$response['status']             = 1;
            $response['message']            = 'DAYS LIST';
            $response['data']               = $days_list;
            return $response;
            */

           foreach( $timemaps as $record)
           {
                $record = (object)$record;
                $key = $days[$day++]['day'];
                $days_list[] = $key;
                $temp = [];
                $timeset = $record->timeset;
                $plist = $record->periods;
                  // dd($plist);
                $temp = [];
                $mylist = [];
                for($i=0; $i<count($timeset); $i++)
                {
                    $temp['period_name'] = $timeset[$i]->period_name;
                    $temp['start_time'] = $timeset[$i]->start_time;
                    $temp['end_time'] = $timeset[$i]->end_time;
                    $temp['is_break'] = $timeset[$i]->is_break; 

                    $temp['subject_title'] = $plist[$i]['subject_title'];    
                    $temp['subject_code'] = $plist[$i]['subject_code'];    
                    $temp['is_lab'] = $plist[$i]['is_lab'];    
                    $temp['is_elective'] = $plist[$i]['is_elective'];  

                    $temp['staff_name'] = $plist[$i]['name'];  
                    $mylist[] = $temp;
                
                }
                
                // $plist = [];
                // foreach($record->periods as $p)
                // {
                //     $pdata = [];
                //     $pdata['subject_title'] = $p->subject_title;
                //     $pdata['subject_code'] = $p->subject_code;
                //     $pdata['is_lab'] = $p->is_lab;
                //     $pdata['is_elective'] = $p->is_elective;
                //     $plist[] = $pdata;
                // }
                
                // $temp[$key]['periods'] = $plist;

                $periods[$key] = $mylist;
           }


            
           

           //no of periods each day(all days same no.of periods)
            $no_of_periods = count($periods[$days_list[0]]);


            
            $dataa=[];
            for ($p=0;$p<$no_of_periods;$p++) {
                //0
                
                $period_classes = [];
                for ($d=0;$d<count($days_list);$d++) {
                    $period_classes[] = $periods[$days_list[$d]][$p];
                }
            
                $dataa[] = $period_classes;
                
                /*foreach($days_list as $day) {
                    //mon
                    $period_classes[]  = $periods[$day][$p];

                    //$periods["Mon"][0]
                    //$periods["Tue"][0]
                    //---
                }
                */
                
            }

            $response['status']            = 1;
            $response['message']           = 'NO.OF PERIODS';
            $response['result']         = $dataa;

            $response['days_list']         = $days_list;
            $response['no_of_periods']     = $no_of_periods;
            return $response;


            

           



            


           
           $response['days_list'] = $days_list;
           $response['periods'] = $periods;

        return $response;
      

     }



   /**
     * Delete Record based on the provided slug
     * @param  [string] $slug [unique slug]
     * @return Boolean 
     */
    public function deleteBookMark(Request $request)
    {
        // dd($request->bookmark_id);
       $record = \App\Bookmark::where('id', '=',$request->bookmark_id )
                  ->where('user_id', '=', $request->user_id)
                  ->where('item_type', '=', 'questions')
                  ->first();

        // $record = \App\Bookmark::all();
        // dd($record);
        if($record){


             $record->delete();

        $response['status'] = 1;
        $response['message'] = 'Bookmark Removed';
        }else{


              $response['status'] = 0;
                $response['message'] = 'Bookmark not available';
        }          
                return $response;
       
    }


    public function getCurrency()
    {

        $response['status']     = 1;
        $response['message']    = 'Site currency';
        $response['currency']   = getSetting('currency_code','site_settings');

        return $response;
    }


    public function getPaymentGateways()
    {

        $response['status']     = 1;
        $response['message']    = 'Site currency';

        $data = [];
        $data['payu']           = getSetting('payu', 'mobile_gateways');
        $data['paypal']         = getSetting('paypal', 'mobile_gateways');
        $data['paytm']          = getSetting('paytm', 'mobile_gateways');
        $data['stripe']         = getSetting('stripe', 'mobile_gateways');
        $data['offline_payment'] = getSetting('offline_payment', 'mobile_gateways');

        $response['data']  = $data;
        
        return $response;
    }

}