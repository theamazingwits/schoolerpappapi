<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Subject;
use App\Student;
use App\Course;
use App\User;
use App\SubjectPreference;
use App\CourseSubject;
use App\Assignments;
use App\Academic;
use App\AssignmentAllocate;
use App\UserCertificates;
use App\ServiceProvider;
use DB;
use Auth;
use Exception;
use Response;


class StaffController extends Controller
{
  private $ServiceProvider;

  private $json_val;

  public function __construct()
  {
        if(!isset(self::$this->ServiceProvider)){
          $this->ServiceProvider = new ServiceProvider();
      }

      $path = 'public/NotifyMessage.json';
      $this->json_val = json_decode(file_get_contents($path), true);
  }

     /**
     * This method adds Class Notes to DB
     * @param  Request $request [Request Object]
     * @return void
     */
     public function staffClasses($user_id)
     {
     	
     	$user = \App\User::where('id','=',$user_id)->first();

    
        $current_academic_id = getDefaultAcademicId();

        /**
         * 1) Get the list of records from course_subjects table with 
         *     combination of current academic ID and current logged in staf/user id
         * 2) 
         */

        $records = \App\CourseSubject::join('courses','courses.id', 
                                            '=', 
                                            'course_subject.course_id')
                                      ->join('subjects','subjects.id', '=', 
                                        'course_subject.subject_id')
                                      ->where('academic_id', '=', $current_academic_id)
                                      ->where('staff_id','=', $user->id)
                                      ->select(['course_subject.id as id','course_subject.slug', 'course_subject.year',
                                        'course_subject.semister', 'course_subject.subject_id',
                                        'courses.course_code','courses.course_title','subjects.subject_title',
                                        'courses.course_dueration','course_subject.course_parent_id','course_subject.academic_id','course_id'])  
                                      // ->get();
                                      ->paginate(getRecordsPerPage());
      
        $select_options = [];
        
        // $select_options[''] = getPhrase('Select');

        //Prepare the select list in the below format
        // Parent Course Code - Year-Sem - Subject-title  
        foreach($records as $record) {


            $year_semister = 0;
            if($record->course_dueration>1)
            {
              $year_semister = 'Year '.$record->year;
              if($record->semister)
                $year_semister .= ' Sem '.$record->semister;
            }
            $final_title = $record->course_code;
            
            if($year_semister)
              $final_title .= ' - '.$year_semister;
            
            $final_title .= ' - '.$record->subject_title;


            // $select_options[$record->id] = $final_title;
            
            $options = (object)[];
            $options->id = $record->id;
            $options->lessonplan = $final_title;
            $options->year = $record->year;
            $options->semister = $record->semister;
            $options->subject_id = $record->subject_id;
            $options->course_parent_id = $record->course_parent_id;
            $options->academic_id = $record->academic_id;
            $options->course_id = $record->course_id;

            $boys_count = DB::table('students as stud')
                      ->join('users as usr', 'usr.id', 'stud.user_id')
                      ->where('stud.academic_id',$options->academic_id)
                      ->where('stud.course_parent_id',$options->course_parent_id)
                      ->where('stud.course_id',  $options->course_id)
                      ->where('stud.current_year', $options->year)
                      ->where('stud.current_semister',$options->semister)
                      ->where('stud.gender', 'male')
                      ->get();
                      // ->paginate(getRecordsPerPage());

            $girls_count = DB::table('students as stud')
                      ->join('users as usr', 'usr.id', 'stud.user_id')
                      ->where('stud.academic_id',$options->academic_id)
                      ->where('stud.course_parent_id',$options->course_parent_id)
                      ->where('stud.course_id',  $options->course_id)
                      ->where('stud.current_year', $options->year)
                      ->where('stud.current_semister',$options->semister)
                      ->where('stud.gender', 'female')
                      ->get();
                      // ->paginate(getRecordsPerPage());
           
            $options->total_count = count($boys_count) + count($girls_count);
            $options->boys_count = count($boys_count);
            $options->girls_count = count($girls_count);

            $select_options[] = $options;
            unset($options);

        }
 
        if( count( $select_options ) > 0 ){

            $response['data']    = $records;
            $response['status']  = 1;
        }else{

            $response['message']  = 'No assignments available';
            $response['status']   = 0;
        }                                
        

        return $response;
     }


     public function getDatatable($academic_id, $course_parent_id, $course_id, $year, $semister)

    {    
        $records = array();

        $records = \App\Student::join('users','users.id','=','students.user_id')
        ->join('courses','courses.id','=','students.course_id')

        ->select(['students.academic_id','students.course_parent_id','students.course_id','students.first_name','students.last_name','users.image','students.roll_no','courses.course_title','users.email'])

        ->where('students.academic_id',      '=',$academic_id)
        ->where('students.course_parent_id', '=',$course_parent_id)
        ->where('students.course_id',        '=',$course_id)
        ->where('students.current_year',     '=',$year)
        ->where('students.current_semister', '=',$semister)

        ->orderBy('students.updated_at','desc')->paginate(getRecordsPerPage());

        return $records;

    }


     public function getClassStudents(Request $request)
     {

        $attendance_date = date('Y-m-d', strtotime($request->attendance_date));
        
        /*$response['message']  = 'test';
        $response['status']   = 0;
        $response['data']  = $request;
        return $response;*/
     	  
     	           // dd($request);
        $user = \App\User::where('id','=',$request->user_id)->first();
       
        if($request->course_subject_id==''||$request->total_class=='' || $attendance_date=='')
        {

            $response['message']  = 'Please select the details';
            $response['status']   = 0;
            return $response;

        }

        $course_subject_record = \App\CourseSubject::where('id', '=', $request->course_subject_id)->first();
        $academic_title = \App\Academic::where('id','=',$course_subject_record->academic_id)->get()->first();
        if(!$course_subject_record)
        {
            $response['message']  = 'Invalid_details_supplied';
            $response['status']   = 0;
            return $response;
        }


      
        /**
         * Find wether the attendance is already added for the day or not
         * @var [type]
         */
         $att_records = $this->isAttendanceAlreadyTaken($course_subject_record,$request->user_id, $request);
        
        $data['attendance_taken'] = FALSE;
        if(count($att_records))
            $data['attendance_taken'] = TRUE;
        
        $data['attendance_records'] = $att_records;

        $attendance_date1 = date('Y-m-d', strtotime($request->attendance_date));

        $data['attendance_present_count'] = \App\StudentAttendance::
              where('academic_id',        '=', $course_subject_record->academic_id)
             ->where('course_id',         '=', $course_subject_record->course_id)
             ->where('year',              '=', $course_subject_record->year)
             ->where('semester',          '=', $course_subject_record->semister)
             ->where('subject_id',        '=', $course_subject_record->subject_id)
             ->where('total_class',       '=', $request->total_class)
             ->where('record_updated_by', '=', $user->id)
             ->where('attendance_date',   '=', $attendance_date1)
             ->where('attendance_code',   '=', "P")
             ->count();

        $data['attendance_absent_count'] = \App\StudentAttendance::
              where('academic_id',        '=', $course_subject_record->academic_id)
             ->where('course_id',         '=', $course_subject_record->course_id)
             ->where('year',              '=', $course_subject_record->year)
             ->where('semester',          '=', $course_subject_record->semister)
             ->where('subject_id',        '=', $course_subject_record->subject_id)
             ->where('total_class',       '=', $request->total_class)
             ->where('record_updated_by', '=', $user->id)
             ->where('attendance_date',   '=', $attendance_date1)
             ->where('attendance_code',   '=', "A")
             ->count();

        $data['attendance_leave_count'] = \App\StudentAttendance::
              where('academic_id',        '=', $course_subject_record->academic_id)
             ->where('course_id',         '=', $course_subject_record->course_id)
             ->where('year',              '=', $course_subject_record->year)
             ->where('semester',          '=', $course_subject_record->semister)
             ->where('subject_id',        '=', $course_subject_record->subject_id)
             ->where('total_class',       '=', $request->total_class)
             ->where('record_updated_by', '=', $user->id)
             ->where('attendance_date',   '=', $attendance_date1)
             ->where('attendance_code',   '=', "L")
             ->count();

        $current_year       = $course_subject_record->year;
        $current_semister   = $course_subject_record->semister;


       
        $course_record = \App\Course::where('id', '=', $course_subject_record->course_id)->first();
        
        $submitted_data = array(
                                'attendance_date'   => $attendance_date,
                                'current_year'      => $current_year,
                                'current_semister'  => $current_semister,
                                'course_record'     => $course_record,
                                'subject_id'        => $course_subject_record->subject_id,
                                'total_class'       => $request->total_class,
                                'updated_by'        => $user->id,
                                'academic_id'       => $course_subject_record->academic_id,
                                'academic_title'    => $academic_title    
                                );

        $studentObject              = new \App\Student();
        /*$students_records           = $studentObject->getStudentsone(
                                                                $course_subject_record->academic_id,
                                                                $course_subject_record->course_id,
                                                                $current_year,
                                                                $current_semister
                                                                 );
*/
        $students_records    = Student::where('academic_id', '=', $course_subject_record->academic_id)
                        ->where('course_id', '=', $course_subject_record->course_id)
                        ->where('current_year', '=', $current_year)
                        ->where('current_semister', '=', $current_semister)
                        ->get();
                        //->paginate(getRecordsPerPage());

        //return $students_records;

        $students=[];
        if (!empty($students_records)) {

          foreach ($students_records as $stu) {
            
              $temp = [];

              $stu->image =  $stu->user()->first()->image; 
              $present = true;
              $absent = false;
              $leave = false;
              $remarks = '';
              $notes = '';

              if ($data['attendance_taken']) { 
                  foreach ($att_records as $atr)
                  { 
                    if ($stu->id == $atr->student_id)
                    {
                      $stu->notes = $atr->notes;
                      $stu->remarks = $atr->remarks;
                      $stu->attendance_code = $atr->attendance_code;
                    }
                  }
              }

              $temp = $stu;

              $students[] = $temp;
          }

        }

        //$dat=$students->paginate(getRecordsPerPage());
        
        //return $students;

        $data['submitted_data']     = (object)$submitted_data;
        $data['students']           = $students;
        $data['userdata']           = $user;
         
        if(count($students)){
             
              $response['class_data']       = $data;
              $response['status']           = 1;
              $response['academic_id']      = $course_subject_record->academic_id;
              $response['course_id']        = $course_subject_record->course_id;
              $response['subject_id']       = $course_subject_record->subject_id;
              $response['current_year']     = $current_year;
              $response['current_semister'] = $current_semister;
              return $response;
        }
        else{
             
              $response['message']  = 'Student are not available in this class';
              $response['status']   = 0;
              return $response;
        }
        
     }


     public function isAttendanceAlreadyTaken($course_subject_record, $user_id, $request,$delete = FALSE)
    {
        
        $attendance_date = date('Y-m-d', strtotime($request->attendance_date));


        $user = \App\User::where('id','=',$user_id)->first();
       
        $year       = $course_subject_record->year;
        $semister   = $course_subject_record->semister;
              $data                        = \App\StudentAttendance::
              where('academic_id',        '=', $course_subject_record->academic_id)
             ->where('course_id',         '=', $course_subject_record->course_id)
             ->where('year',              '=', $year)
             ->where('semester',          '=', $semister)
             ->where('subject_id',        '=', $course_subject_record->subject_id)
             ->where('total_class',       '=', $request->total_class)
             ->where('record_updated_by', '=', $user->id)
             ->where('attendance_date',   '=', $attendance_date);
        if(!$delete) {
            return $data->get();
        }

        return $data->delete();

    }

    public function updateAttendance(Request $request)
    {

      /*print_r("all".$request);
      exit;*/

    	  $user = \App\User::where('id','=',$request->user_id)->first();
       
        /**
         * 1) Check if any record exists with the combination
         * 2) If exists Delete the records with same combination
         * 3) If not exists, continue the flow as it is
         */

        $course_subject_record = \App\CourseSubject::where('academic_id','=',$request->academic_id)
                                                    ->where('course_id', '=', $request->course_id)
                                                    ->where('subject_id', '=', $request->subject_id)
                                                    ->where('staff_id', '=', $user->id)
                                                    ->first();


        if(count($this->isAttendanceAlreadyTaken($course_subject_record,$request->user_id, $request)))
        {
            //Data exists, remove all data
            $this->isAttendanceAlreadyTaken($course_subject_record,$request->user_id, $request, TRUE);
        }

        $attendance_date='';
        if ($request->attendance_date)
          $attendance_date = date('Y-m-d', strtotime($request->attendance_date));


        $attendance             = new \App\StudentAttendance();  
        $academic_id            = $request->academic_id;
        $course_id              = $request->course_id;
        $current_year           = $request->current_year;
        $current_semister       = $request->current_semister;
        $subject_id             = $request->subject_id;
        $total_class            = $request->total_class;
        $updated_by             = $request->record_updated_by;
        // $user_id                = Auth::User()->id;
        $user_id                = $request->user_id;

        if (!empty($request->attendance_records)) {

          foreach($request->attendance_records as $value)
          {   
           
              $attendance  = new \App\StudentAttendance();  
            
              $attendance->academic_id        = $academic_id;
              $attendance->course_id          = $course_id;
              $attendance->year               = $current_year;
              $attendance->semester           = $current_semister;
              $attendance->attendance_date    = $attendance_date;
              $attendance->subject_id         = $subject_id;
              $attendance->total_class        = $total_class;
              $attendance->record_updated_by  = $updated_by;
              $attendance->student_id         = $value['student_id'];
              $attendance->attendance_code    = $value['attendance_code'];
              $attendance->remarks            = $value['remarks'];
              $attendance->notes              = $value['notes'];
              $attendance->record_updated_by  = $user_id;
              $attendance->save();
          }

        }

      $notificationusers = DB::table('users as usr')
              ->where('usr.id', $value['student_id'])
              ->select(['usr.push_web_id','usr.push_app_id','usr.os_type','usr.name','usr.id','usr.app_type'])
              ->get();

      $aptyp = DB::table('users')
              ->where('id',$value['student_id'])
              ->first(['app_type']);

      $this->ServiceProvider->data = $notificationusers;
      $this->ServiceProvider->aptyp = $aptyp->app_type;
      $this->ServiceProvider->message = $this->json_val['NOWmsg0013A'].$value['student_id'].$this->json_val['NOWmsg0013B'];
      $this->ServiceProvider->pageId = "UNKNOWN";
      $this->ServiceProvider->insertNotification($this->ServiceProvider);

        $response['message']  = 'Attendance is added successfully';
        $response['status']   = 1;
        return $response;
    }


    public function lessonPlans($user_id)
    {
    	  
    	  $user     = \App\User::where('id','=',$user_id)->first();
          $subjects = \App\LessionPlan::getSubjects($user->id);
    
          if(count($subjects)){
          	
          	
	        $subjects_data  = []; 
	        $lessionPlanObject = new \App\LessionPlan();
	        
            foreach($subjects as $subject) {

                 $summary = $lessionPlanObject->getSubjectCompletedStatus($subject->subject_id, $subject->staff_id, $subject->id);
				 $percent_completed = round($summary->percent_completed);

                $temp['percent_completed'] = $percent_completed;
                $temp['subject_title'] = $subject->subject_title;
                $temp['course_title'] = $subject->course_title;
                $temp['subject_slug'] = $subject->slug;
                $subjects_data[]  = $temp;

            }

            $data['user']               = $user;
	        $data['subjects']           = $subjects_data;
	        
	        $response['data']     = $data;
	        $response['status']   = 1;
	             

	        } else {
	          
	              $response['message']  = 'No data available';
	              $response['status']   = 0;
	        }

	              return $response;
    }

 public function getStaffClassSubject($user_id)
 {
        $academic_id  = getDefaultAcademicId();

        $classes  = CourseSubject::join('academics', 'academics.id','=','course_subject.academic_id')
                                  ->join('courses', 'courses.id','=','course_subject.course_id')
                                  ->where('course_subject.staff_id',$user_id)
                                  ->where('course_subject.academic_id',$academic_id)
                                  ->select(DB::raw("CONCAT(academic_year_title,'-',course_title) AS class_name"),'course_subject.id')
                                  ->get();

        $subjects  = CourseSubject::join('subjects', 'subjects.id','=','course_subject.subject_id')
                                  ->where('staff_id',$user_id)
                                  ->where('academic_id',$academic_id)
                                  ->groupby('course_subject.subject_id')
                                  ->select('subjects.subject_title','subjects.id')
                                  ->get();

        $data['subjects']   = $subjects;
        $data['classes']    = $classes;
        $response['status']   = 1;

        return $data;
 }

 public function staffAssignments($user_id)
 {

    $records = \App\Assignments::leftJoin('subjects', 'assignments.subject_id', 'subjects.id')
                                ->select(['assignments.title','assignments.subject_id','assignments.course_subject_id','assignments.deadline','assignments.file_name','assignments.slug','assignments.id','subjects.subject_title'])
                               ->where('assignments.user_id',$user_id)
                               ->paginate(getRecordsPerPage());
                               // ->get();                       

    $new = [];

     foreach ($records as $key => $value) {
          
       $class  = CourseSubject::join('academics', 'academics.id','=','course_subject.academic_id')
                                  ->join('courses', 'courses.id','=','course_subject.course_id')
                                  ->where('course_subject.id',$value->course_subject_id)
                                  ->select(DB::raw("CONCAT(academic_year_title,'-',course_title) AS class_name"),'course_subject.id')
                                  ->get();

        array_push($new, $class);
     }

     $result = array('data' => $records,'class' => $new );

        if(count($records) > 0 ){
             
              $response['assignemtns']  = $records;
              $response['status']   = 1;
              return $result;
        }
        else{
             
              $response['message']  = 'No data available';
              $response['status']   = 0;
              return $response;
        }                         
 }


      public function viewAssignment($id)
     {
       
        
        $record    = \App\Assignments::where('id',$id)->first();


        $students  = \App\Assignments::join('assignments_allocate','assignments_allocate.assignment_id','=','assignments.id')
                                  ->join('users','users.id','=','assignments_allocate.student_id')
                                  ->join('students','students.user_id','=','users.id')
                                  ->join('subjects','subjects.id','=','assignments.subject_id')
                                  ->where('assignments_allocate.assignment_id',$record->id)
                                  ->select(['assignments.title','subject_title','users.name','deadline','assignments_allocate.id as allocate_id','assignments.id as assignment_id','assignments_allocate.user_file','assignments_allocate.updated_at as submitted_on','students.roll_no','credits','assignments_allocate.student_id','is_submitted','is_approved'])
                                  ->get();
      
        if(count($students) <= 0){

              $response['message']  = 'No data available';
              $response['status']   = 0;
              return $response;
        }                       
        
      
        $data['students']     = $students;
        $data['record']       = $record;

        $class  = \App\CourseSubject::join('academics', 'academics.id','=','course_subject.academic_id')
                                  ->join('courses', 'courses.id','=','course_subject.course_id')
                                  ->where('course_subject.id',$record->course_subject_id)
                                  ->select(DB::raw("CONCAT(academic_year_title,'-',course_title) AS class_name"),'course_subject.id')
                                  ->first();

        $data['class_name']  =   ucwords($class->class_name);
        $data['user']        =   \App\User::find($record->user_id);

        $response['data']     = $data;
        $response['status']   = 1;

        return $response;
     
     }



    public function appoveSingleAssignment(Request $request)
     {
         print_r($request);
         $assignment               = \App\AssignmentAllocate::find($request->allocation_id);
         print_r($assignment);

         $assignment->is_approved  = 1;
         $assignment->credits      = $request->credits;
         $assignment->save();

         $user        = \App\User::find($assignment->student_id);
         print_r($user);
         $assignment  = \App\Assignments::find($assignment->assignment_id);
          
          try {
             
               $user->notify(new \App\Notifications\StaffApproveAssignment($user,$assignment));
             
           } catch (Exception $e) {
              
              // dd($e->getMessage());
           }
        $notificationusers = DB::table('users')
                           ->where('id',$user->id)
                           ->select(['push_web_id','push_app_id','os_type','name','id','app_type'])
                           ->get();

        $aptyp = DB::table('users')
                ->where('id',$user->id)
                ->first(['app_type']);

        $this->ServiceProvider->data = $notificationusers;
        $this->ServiceProvider->aptyp = $aptyp;
        $this->ServiceProvider->message = $this->json_val['NOWmsg0014A'].$user->id.$this->json_val['NOWmsg0014B'];
        $this->ServiceProvider->pageId = "UNKNOWN";
        $this->ServiceProvider->insertNotification($this->ServiceProvider);


              $response['message']  = 'Assignment approved successfully';
              $response['status']   = 1;
              return $response;
    }


    public function appoveAssignment(Request $request)
     {  
        // dd($request);
        //$credits    = json_decode($request->credits); 

        $record     = \App\Assignments::where('id',$request->assignment_id)->first();

        $allocates = \App\AssignmentAllocate::where('assignment_id',$record->id)->get();
         

        if ($record) {
            if (!empty($request->credits) && $allocates) {

                foreach($request->credits as $value)
                {  
                  
                    $asign_allocte = \App\AssignmentAllocate::where('assignment_id', $record->id)
                                                          ->where('student_id', $value['id'])
                                                          ->first();

                    if (!empty($asign_allocte)) {
                      $asign_allocte->is_approved = 1;
                      $asign_allocte->credits     = $value['credits'];
                      $asign_allocte->save();
                    }
                }
              $notificationusers = DB::table('users')
                                 ->where('id',$record->user_id)
                                 ->select(['push_web_id','push_app_id','os_type','name','id','app_type'])
                                 ->get();

              $aptyp = DB::table('users')
                      ->where('id',$record->user_id)
                      ->first(['app_type']);

              $this->ServiceProvider->data = $notificationusers;
              $this->ServiceProvider->aptyp = $aptyp;
              $this->ServiceProvider->message = $this->json_val['NOWmsg0010A'].$record->user_id.$this->json_val['NOWmsg0010B'];
              $this->ServiceProvider->pageId = "UNKNOWN";
              $this->ServiceProvider->insertNotification($this->ServiceProvider);

                $response['message']  = 'Assignment approved successfully';
                $response['status']   = 1;
                return $response;

            } else {
              $response['message']  = 'No data to approve';
              $response['status']   = 1;
              return $response;
            }
        } else {
            $response['message']  = 'Assignment record not found';
            $response['status']   = 0;
            return $response;
        }
          
     }

     public function viewTopics($user_id, $subject_slug)
    {

      $user = \App\User::where('id','=',$user_id)->first();
     
      $courseSubjectRecord = \App\CourseSubject::where('slug','=',$subject_slug)->first();

      if($courseSubjectRecord){
        $courseRecord = \App\Course::where('id','=',$courseSubjectRecord->course_id)->first();
        $subjectRecord = \App\Subject::where('id','=', $courseSubjectRecord->subject_id)->first();

        $available_records = \App\LessionPlan::where('course_subject_id', '=', $courseSubjectRecord->id)->get();
        
        $topics = $this->prepareTopicsList($courseSubjectRecord->subject_id, $courseSubjectRecord->id);


      return array('status'=>'sucess', 'topics'=>$topics);
    }else{
      return array('status'=>'Failure', 'topics'=>'No Records found...');
    }
       
   }


     public function updateLessionTopic(Request $request)
   {
    $topic_id           = $request->topic_id;
    $course_subject_id  = $request->course_subject_id;
    $is_completed       = $request->status;

    $record = \App\LessionPlan::where('course_subject_id', '=', $course_subject_id)
    ->where('topic_id', '=', $topic_id)->first();
    $status = 0;
    if($record)
    {
      //Record already exists, just update that record
      $record->course_subject_id  = $course_subject_id;
      $record->topic_id       = $topic_id;
      $record->is_completed     = $is_completed;
      $record->completed_on     = date('Y-m-d');

      $record->save();
      $status = 1;
    }
    else {
      //Record not available, create new record with the data
      $record = new \App\LessionPlan();
      $record->course_subject_id  = $course_subject_id;
      $record->topic_id       = $topic_id;
      $record->is_completed     = $is_completed;
      $record->completed_on     = date('Y-m-d');
      $record->save();
      $status = 1;
    }
    $topics = [];
    $course_subject_record = \App\CourseSubject::where('id','=',$record->course_subject_id)->first();
    $topics = $this->prepareTopicsList($course_subject_record->subject_id, $course_subject_record->id);

    return array('status'=>$status, 'topics'=>$topics);
   }

      /**
    * This method prepares  the list of topics and child topics and returns an array
    * @param  [type] $subject_id      [description]
    * @param  [type] $courseSubjectId [description]
    * @return [type]                  [description]
    */
   public function prepareTopicsList($subject_id, $courseSubjectId)
   {
     $parent_topics = $this->getTopicRecord($subject_id, 0,0);

      $topics = [];
      foreach($parent_topics as $topic)
      {

        //$topics[$topic->id] = $topic;
        $topics['course_subject_id'] = $courseSubjectId;

        $subject_topics_list = \App\Topic::where('parent_id','=',$topic->id)->get()->toArray();
        $lession_plan_topics = \App\LessionPlan::where('course_subject_id','=',$courseSubjectId)
                                                ->get()->toArray();
        $topics['childs'] = $this->prepareChildRecords($subject_topics_list, $lession_plan_topics);
      }
     
      return $topics;
   }

    /**
    * This method returns the specific topics based on the condition
    *
    * @param      <type>   $subject_id  The subject identifier
    * @param      integer  $parent_id   The parent identifier
    *
    * @return     <type>   The topic record.
    */
   public function getTopicRecord($subject_id, $parent_id = 0, $courseSubjectId = 0 )
   {
 
    $result = \App\Topic::join('subjects', 'subjects.id','=','topics.subject_id')
      ->leftJoin('lessionplans','topic_id','=','topics.id');
   

      $result = $result->where('subjects.id', '=',$subject_id)
      ->where('topics.parent_id', '=', $parent_id)
      ->select(['topics.id as id', 'topic_name','lessionplans.is_completed','completed_on'])
      ->groupBy(['topics.id'])->get();

      return $result;
   }


   public function prepareChildRecords($topics_list, $lession_plan_list)
   {

    $final_records = [];
     $child_topics = [];
     foreach($topics_list as $topic)
     {
      $topic = (object)$topic;
      $child_topics['id'] = $topic->id;
      $child_topics['topic_name'] = $topic->topic_name;
      $completed_status = 0;
      $completed_date = '';

      foreach($lession_plan_list as $plan_record)
      {
        $plan_record = (object)$plan_record;
        if($topic->id == $plan_record->topic_id)
        {
          $completed_status = ($plan_record->is_completed) ? 1 : 0;
          $completed_date = $plan_record->completed_on;
        }
      }
      $child_topics['is_completed'] = $completed_status;
      $child_topics['completed_on'] = $completed_date;
      $final_records[] = $child_topics;
     }
     return $final_records;
   }



   public function updateTopic(Request $request)
   {
    $topic_id           = $request->topic_id;
    $course_subject_id  = $request->course_subject_id;
    $is_completed       = $request->status;

    $record = \App\LessionPlan::where('course_subject_id', '=', $course_subject_id)
                                  ->where('topic_id', '=', $topic_id)
                                  ->first();
    $status = 0;
    if($record)
    {
      //Record already exists, just update that record
      $record->course_subject_id  = $course_subject_id;
      $record->topic_id           = $topic_id;
      $record->is_completed       = $is_completed;
      $record->completed_on       = date('Y-m-d');

      $record->save();
      $status = 1;
    }
    else {
      //Record not available, create new record with the data
      $record                     = new \App\LessionPlan();
      $record->course_subject_id  = $course_subject_id;
      $record->topic_id           = $topic_id;
      $record->is_completed       = $is_completed;
      $record->completed_on       = date('Y-m-d');
      $record->save();
      $status = 1;
    }
   
     $response['message']  = 'Topic updated successfully';
     $response['status']   = 1;
     return $response;
   }


   public function addAssignment($user_id)
   {
       
        $staff        = \App\User::find($user_id);
        $academic_id  = getDefaultAcademicId();

        // $subjects     = \App\CourseSubject::getStaffSubjetcs($staff->id,$academic_id);

       
        $records=[];
        if (is_numeric($academic_id)) {

        $records  = \App\CourseSubject::join('subjects', 'subjects.id','=','course_subject.subject_id')
                                  ->select(['subjects.subject_title','subjects.id'])
                                  ->where('staff_id',$staff->id)
                                  ->where('academic_id',$academic_id)
                                  ->groupby('course_subject.subject_id')
                                  ->get();
                                  // ->pluck('subject_title','subjects.id')
                                  // ->toArray();
                                  
        } else{

        $records  = \App\CourseSubject::join('subjects', 'subjects.id','=','course_subject.subject_id')
                                  ->select(['subjects.subject_title','subjects.id'])
                                  ->where('staff_id',$staff->id)
                                  ->groupby('course_subject.subject_id')
                                  ->get();
                                  // ->pluck('subject_title','subjects.id')
                                  // ->toArray();
        }

        $subjects=[];
        if (!empty($records)) {
          foreach ($records as $record) {
            $temp = (object)[];

            $temp->subject_id     = $record->id;
            $temp->subject_title  = $record->subject_title;

            $subjects[] = $temp;
            
          } 
        }





        // $classes      = \App\CourseSubject::getStaffClasses($staff->id,$academic_id);
        $class_records=[];
        if(is_numeric($academic_id)){

        $class_records  = \App\CourseSubject::join('academics', 'academics.id','=','course_subject.academic_id')
                                  ->join('courses', 'courses.id','=','course_subject.course_id')
                                  ->where('course_subject.staff_id',$staff->id)
                                  ->where('course_subject.academic_id',$academic_id)
                                  ->groupby('course_subject.course_id')
                                  ->select(DB::raw("CONCAT(academic_year_title,'-',course_title) AS class"),'course_subject.id')
                                  ->get();
                                  // ->pluck('class', 'id')
                                  // ->toArray();
                                  
                                  
        }
        else{

        $class_records  = \App\CourseSubject::join('academics', 'academics.id','=','course_subject.academic_id')
                                  ->join('courses', 'courses.id','=','course_subject.course_id')
                                  ->where('course_subject.staff_id',$staff->id)
                                  ->groupby('course_subject.course_id')
                                  ->select(DB::raw("CONCAT(academic_year_title,'-',course_title) AS class"),'course_subject.id')
                                  ->get();
                                  // ->pluck('class', 'id')
                                  // ->toArray();
        }


        $classes=[];
        if (!empty($class_records)) {
          foreach ($class_records as $clasrcord) {

              $temp = (object)[];

              $temp->class_id     = $clasrcord->id;
              $temp->class_title  = $clasrcord->class;

              $classes[] = $temp;
            
          }
        }



        if( count($subjects) == 0 ){
            
              $response['message']  = 'Subjects are not available';
              $response['status']   = 1;
              return $response;

        }elseif (count($classes) == 0) {

              $response['message']  = 'Classes are not available';
              $response['status']   = 1;
              return $response;
        }


        $data['subjects']   = $subjects;
        $data['classes']    = $classes;

         $response['data']     = $data;
         $response['status']   = 1;
         return $response;
   }

    public function getAssignmentStudents(Request $request)
    {  

      $course_subject_id = $request->course_subject_id;
       $record_type       = $request->record_type;

       $record  = CourseSubject::find($course_subject_id);

       $all_students  = \App\Student::join('users','users.id','=','students.user_id')
                                ->where('academic_id',$record->academic_id)
                                ->where('course_parent_id',$record->course_parent_id) 
                                ->where('course_id',$record->course_id) 
                                ->where('current_year',$record->year) 
                                ->where('current_semister',$record->semister)
                                ->select(['users.name','roll_no','users.id'])
                                ->get();
         
        $temp     = [];
        $students = [];

        foreach ($all_students as $student) 
        {
           
           $assigned           = AssignmentAllocate::where('student_id',$student->id)->first();
           $temp['user']       = $student; 
           $temp['is_assined'] = 0; 

           if( $assigned && $record_type == 2 )
           {

              $temp['is_assined'] = 1; 
            
           }

           $students[] = $temp;

        }                          

       return $students;      

       /*$course_subject_id = $request->course_subject_id;
       $record_type       = $request->record_type;

       $record  = \App\CourseSubject::find($course_subject_id);

       $all_students  = \App\Student::join('users','users.id','=','students.user_id')
                                ->where('academic_id',$record->academic_id)
                                ->where('course_parent_id',$record->course_parent_id) 
                                ->where('course_id',$record->course_id) 
                                ->where('current_year',$record->year) 
                                ->where('current_semister',$record->semister)
                                ->select(['users.name','roll_no','users.id'])
                                ->get();
         
        $temp     = [];
        $students = [];

        foreach ($all_students as $student) 
        {
           
          $assigned = \App\AssignmentAllocate::where('assignment_id', $request->assignment_id)
                                    ->where('student_id',$student->id)->first();

           $temp['is_assined'] = 0; 
           $temp['name']       = $student->name; 
           $temp['roll_no']    = $student->roll_no; 
           $temp['id']         = $student->id; 

           if (count($assigned)>0 && $record_type==2)
           {
              $temp['is_assined'] = 1; 
           }

           $students[] = $temp;

        }                          
         
        if(count($students) <= 0 ){

              $response['message']  = 'No students are available';
              $response['status']   = 0;
              
         }else{

             $response['students']     = $students;
             $response['status']   = 1;
         }
         
         return $response;            */         
  }



   /**
     * This method adds record to DB
     * @param  Request $request [Request Object]
     * @return void
     */
    public function storeAssignment(Request $request)
    { 
      
      if( $request->type == 1){

         if (!$request->input('users_id')) {

              $response['message']  = 'please select students';
              $response['status']   = 0;
               return $response;
         }

      }

      DB::beginTransaction();

       try {

           $record                = new \App\Assignments();
           $record->user_id       = $request->staff_id;//Auth::user()->id;
           $record->title         = $request->title;
           $record->slug          = $record->makeSlug($request->title);
           $record->subject_id    = $request->subject_id;
           $record->course_subject_id   = $request->course_subject_id;
           $record->type          = $request->type;
           $record->deadline      = $request->deadline;
           $record->description   = $request->description;
           $record->save();

            //$this->processUpload($request, $record, 'catimage');
            if ($request->file('filename')) {

              $this->processUpload($request, $record,'filename','assignments','id','file_name','ASSIGN/IMAGE');

              /*$filename = $request->file('filename');
              $filename->move('public/uploads/assignments/',$request->file_name); 

              $record->file_name = $request->file_name;
              $record->save();*/

          /* 
              $base64_string = base64_decode($request->input('filename'));
              $file_name = $request->file_name;
              //file_put_contents('public/uploads/assignments/'.$request->file_name, $filename);
          */
            }

           //if type==0 assignment allocate to all students
           if($request->type == 0){

              $record->addAllStudents($request);
           }else{
              //if type==1 for selected students
              $record->addSelectedStudentsService($request);
           }

           DB::commit();

            $response['message']  = 'Assignment added successfully';
            $response['status']   = 1;
            return $response;
         
       } 
       catch (Exception $e) {
          
          DB::rollBack();
            $response['message']  = 'please try again once';
            $response['status']   = 0;
            return $response;
       }

   }


    /*public function processUpload(Request $request, $record, $file_name)
     {
       
         if ($request->hasFile($file_name)) {

          $examSettings     = new \App\GeneralSettings();
          // dd($examSettings);
          
          $destinationPath  = $examSettings->assignmentPath();
          // dd($destinationPath);
          
          $fileName = ucwords($record->slug).'-'.$request->$file_name->getClientOriginalName();
          // dd($fileName);
          
          $request->file($file_name)->move($destinationPath, $fileName);

          $record->file_name  =  $fileName;
          $record->save();
         
      
        }
     }*/
     public function processUpload(Request $request, $record, $file_name,$table_name,$column_name,$update_column_name,$tag)
     {  
         if ($request->hasFile($file_name)) {
            $path = $_FILES[$file_name]['name'];
            $ext = pathinfo($path, PATHINFO_EXTENSION);
            $fileName = $record->id.'-'.$file_name.'.'.$ext;
            $this->ServiceProvider->original_filename = $file_name;
            $this->ServiceProvider->file_name = $fileName;
            $this->ServiceProvider->record = $record;
            $this->ServiceProvider->userId = $request->staff_id;
            $this->ServiceProvider->table = $table_name; 
            $this->ServiceProvider->update_column_name = $update_column_name;
            $this->ServiceProvider->column_name = $column_name;
            $this->ServiceProvider->tag = $tag; 
            $this->ServiceProvider->uploadFiles($this->ServiceProvider);
        }
     }

      /**
     * Delete Record based on the provided slug
     * @param  [string] $slug [unique slug]
     * @return Boolean 
     */
    public function deleteAssignment($id)
    {
      
        $record      = \App\Assignments::where('id', $id)->first();

        $any_upload  = \App\AssignmentAllocate::where('assignment_id','=',$record->id)
                                            ->where('is_submitted',1)
                                            ->get()
                                            ->count();

        if($any_upload > 0){
           
                  $response['status']  = 0;
                  $response['message'] = getPhrase('student_is_already_uploaded_assignment_submission_file_so_you_are_unable_to_delete');
         }
         else{
            
            \App\AssignmentAllocate::where('assignment_id','=',$record->id)->delete();    
            $record->delete();
            
            $response['status'] = 1;
            $response['message'] = getPhrase('record_deleted_successfully'); 

         }

        return $response;
    }


    /**
     * This method loads the edit view based on unique slug provided by user
     * @param  [string] $slug [unique slug of the record]
     * @return [view with record]       
     */
    public function editAssignment($id)
    {
     
        $assignment = \App\Assignments::where('id',$id)->first();

        $academic_id  = getDefaultAcademicId();

        $record = \App\Assignments::join('subjects', 'assignments.subject_id', 'subjects.id')
                                    ->join('course_subject', 'assignments.course_subject_id', 'course_subject.id')
                                    ->join('academics', 'academics.id', 'course_subject.academic_id')
                                    ->join('courses', 'courses.id', 'course_subject.course_id')
                                    ->where('assignments.id', '=', $id)
                                    ->where('course_subject.staff_id', '=', $assignment->user_id)
                                    ->where('course_subject.academic_id', '=', $academic_id)
                                    ->select(['assignments.*','subjects.subject_title', DB::raw('CONCAT(academics.academic_year_title,"-",courses.course_title) AS course_subject_title')])
                                    ->first();

        if (!$record) {

          $response['data']     = [];
          $response['message']  = "Assignment record not found";
          $response['status']   = 0;
          return $response;
        }

        $assignment_upload   = \App\AssignmentAllocate::where('assignment_id',$record->id)
                                                  ->where('is_submitted',1)
                                                  ->get()->count();
        $data['uploaded']   = FALSE;
        if($assignment_upload > 0){

            $data['uploaded']   = TRUE;
        }                                          

        // $staff        = Auth::user();
        $staff = getUserRecord($record->user_id);

        

        // $subjects     = \App\CourseSubject::getStaffSubjetcs($staff->id,$academic_id);

        $records=[];
        if (is_numeric($academic_id)) {

        $records  = \App\CourseSubject::join('subjects', 'subjects.id','=','course_subject.subject_id')
                                  ->select(['subjects.subject_title','subjects.id'])
                                  ->where('staff_id',$staff->id)
                                  ->where('academic_id',$academic_id)
                                  ->groupby('course_subject.subject_id')
                                  ->get();
                                  // ->pluck('subject_title','subjects.id')
                                  // ->toArray();
                                  
        } else{

        $records  = \App\CourseSubject::join('subjects', 'subjects.id','=','course_subject.subject_id')
                                  ->select(['subjects.subject_title','subjects.id'])
                                  ->where('staff_id',$staff->id)
                                  ->groupby('course_subject.subject_id')
                                  ->get();
                                  // ->pluck('subject_title','subjects.id')
                                  // ->toArray();
        }

        $subjects=[];
        if (!empty($records)) {
          foreach ($records as $rcrd) {
            $temp = (object)[];

            $temp->subject_id     = $rcrd->id;
            $temp->subject_title  = $rcrd->subject_title;

            $subjects[] = $temp;
            
          } 
        }


        // $classes      = \App\CourseSubject::getStaffClasses($staff->id,$academic_id);

        $class_records=[];
        if(is_numeric($academic_id)){

        $class_records  = \App\CourseSubject::join('academics', 'academics.id','=','course_subject.academic_id')
                                  ->join('courses', 'courses.id','=','course_subject.course_id')
                                  ->where('course_subject.staff_id',$staff->id)
                                  ->where('course_subject.academic_id',$academic_id)
                                  ->groupby('course_subject.course_id')
                                  ->select(DB::raw("CONCAT(academic_year_title,'-',course_title) AS class"),'course_subject.id')
                                  ->get();
                                  // ->pluck('class', 'id')
                                  // ->toArray();
                                  
                                  
        }
        else{

        $class_records  = \App\CourseSubject::join('academics', 'academics.id','=','course_subject.academic_id')
                                  ->join('courses', 'courses.id','=','course_subject.course_id')
                                  ->where('course_subject.staff_id',$staff->id)
                                  ->groupby('course_subject.course_id')
                                  ->select(DB::raw("CONCAT(academic_year_title,'-',course_title) AS class"),'course_subject.id')
                                  ->get();
                                  // ->pluck('class', 'id')
                                  // ->toArray();
        }


        $classes=[];
        if (!empty($class_records)) {
          foreach ($class_records as $clasrcord) {

              $temp = (object)[];

              $temp->class_id     = $clasrcord->id;
              $temp->class_title  = $clasrcord->class;

              $classes[] = $temp;
            
          }
        }


        
        $data['subjects'] = $subjects;
        $data['classes']  = $classes;
        $data['record']   = $record;
        $data['assignment_id']  = $record->id; 
        $data['add_record'] = 2;

        $response['data']    = $data;
        $response['status']  = 1;
        return $response;

    }


        /**
     * Update record based on slug and reuqest
     * @param  Request $request [Request Object]
     * @param  [type]  $slug    [Unique Slug]
     * @return void
     */
    public function updateAssignment(Request $request, $id)
    {
     
         $record      = \App\Assignments::where('id',$id)->first();
         $any_upload  = \App\AssignmentAllocate::where('assignment_id','=',$record->id)
                                            ->where('is_submitted',1)
                                            ->get()
                                            ->count();


        if($any_upload > 0){
            
            $response['message'] = getPhrase('student_is_already_uploaded_assignment_submission_file_so_you_are_unable_to_modify'); 
            $response['status']  = 0;
            return $response;

         }

       if( $request->type == 1){

         if (!$request->input('users_id')) {

              $response['message']  = 'please select students';
              $response['status']   = 0;
               return $response;
         }


      }
        //Delete Previous Records
         
         DB::beginTransaction();

         try {
           
           $alocated_record = \App\AssignmentAllocate::where('assignment_id','=',$record->id)->delete();   
    
            $name = $request->title;
           if($name != $record->title)
            $record->slug = $record->makeSlug($name);

           $record->user_id       = $request->staff_id;
           $record->title         = $request->title;
           $record->subject_id    = $request->subject_id;
           $record->course_subject_id   = $request->course_subject_id;
           $record->type          = $request->type;
           $record->deadline      = $request->deadline;
           $record->description   = $request->description;
           $record->save();
            // dd('herere');
           // $this->processUpload($request, $record, 'catimage');
            // dd($record);
            if ($request->file('filename')) {

              $this->processUpload($request, $record,'filename','assignments','id','file_name','ASSIGN/IMAGE');

              /*$filename = $request->file('filename');
              $filename->move('public/uploads/assignments/',$request->file_name); 

              $record->file_name      = $request->file_name;
              $record->save();*/

            }

            if ($request->type == 0){
              $record->addAllStudents($request);
            } else {
              $record->addSelectedStudentsService($request);
            }

          
           DB::commit();

           $response['message'] = 'Record is updated successfully';
           $response['status']  = 1;
           return $response;
         
       } 
       catch (Exception $e) {
          
           DB::rollBack();
           $response['message'] = 'Error please try again once';
           $response['status']  = 0;
           return $response;
       }

    }


    public function updateAssignment1(Request $request, $id)
    {
      // dd($request);
         $record      = Assignments::where('id',$id)->first();
         $any_upload  = AssignmentAllocate::where('assignment_id','=',$record->id)
                                            ->where('is_submitted',1)
                                            ->get()->count();
        if($any_upload > 0){

            $response['message'] = getPhrase('student_is_already_uploaded_assignment_submission_file_so_you_are_unable_to_modify'); 
            $response['status']  = 0;
            return $response;
         }

       if( $request->type == 1){
        
         if(!$request->has('users_id')){

            $response['message']  = 'please select students';
              $response['status']   = 0;
               return $response;
         }

      }


         //Delete Previous Records
         
         DB::beginTransaction();

         try {
            
           AssignmentAllocate::where('assignment_id','=',$record->id)->delete();                          

            $name = $request->title;
           if($name != $record->title)
            $record->slug = $record->makeSlug($name);

           $record->user_id       = $request->staff_id;
           $record->title         = $request->title;
           $record->subject_id    = $request->subject_id;
           $record->course_subject_id   = $request->course_subject_id;
           $record->type          = $request->type;
           $record->deadline      = $request->deadline;
           $record->description   = $request->description;
           $record->save();
// dd('herere');
           if ($request->file('filename')) {

              $filename = $request->file('filename');
              $filename->move('public/uploads/assignments/',$request->file_name); 

              $record->file_name      = $request->file_name;
              $record->save();

            }
// dd($record);
           
           if($request->type == 0){

              $record->addAllStudents($request);
           }else{
              

              $record->addSelectedStudents($request);
           }
           DB::commit();

           $response['message'] = 'Record is updated successfully';
           $response['status']  = 1;
           return $response;
           
         
       } 
       catch (Exception $e) {
          
          DB::rollBack();
          // dd($e->getMessage());

         $response['message'] = 'Error please try again once';
           $response['status']  = 0;
           return $response;
       }
    }

    //preferences
    public function subjectPreferences($user_id)
    {
   
     $data['subjects_list'] = \App\Subject::where('status', '=', 'Active')->select(['id','subject_title','subject_code','maximum_marks','is_lab','is_elective_type'])->paginate(getRecordsPerPage());

     $data['Preference_Subject'] = \App\SubjectPreference::join('subjects', 'subjectpreferences.subject_id','=','subjects.id')
              ->where('user_id','=',$user_id)
              ->select(['subjects.id','subject_title','subject_code','maximum_marks','is_lab','is_elective_type'])
              ->paginate(getRecordsPerPage());
              // ->get();

    $data['Subject_count'] = \App\SubjectPreference::join('subjects', 'subjectpreferences.subject_id','=','subjects.id')
              ->where('user_id','=',$user_id)
              ->count('subjects.id');
    
    $data['Lab_count'] = \App\SubjectPreference::join('subjects', 'subjectpreferences.subject_id','=','subjects.id')
              ->where('user_id','=',$user_id)
              ->where('is_lab','=',1)
              ->count('subjects.id');

    $data['Elective_count'] = \App\SubjectPreference::join('subjects', 'subjectpreferences.subject_id','=','subjects.id')
              ->where('user_id','=',$user_id)
              ->where('is_elective_type','=',1)
              ->count('subjects.id');

      return  $data;   
    }
    
    public function update(Request $request, $user_id)
    {
      // dd($request);

       $record = \App\User::where('id', '=', $user_id)->first();
       $staff_record = $record->staff()->first();

      DB::beginTransaction();
      try{
           $model = SubjectPreference::where('user_id','=',$user_id);
           $count = $model->count();
          if($count)
          {
            //Previous records exists  
            $model->delete();
          }
          if($request->selected_list){   
              foreach($request->selected_list as $key=>$value)
              {
                $newRecord             = new SubjectPreference();
                $newRecord->user_id    = $record->id;
                $newRecord->staff_id   = $staff_record->id;
                $newRecord->subject_id = $value;
                $newRecord->save();
              }
          }  
           DB::commit();
           $response['message'] = 'Record is updated successfully';
           $response['status']  = 1;
        }
      catch(Exception $e)
      {
       DB::rollBack();
       $response['message'] = 'Error please try again once';
       $response['status']  = 0;
      }
      return $response;

    }

    //Certificate Store
    public function store(Request $request)
    {
          $columns = array(      
            'certificate_names'     => 'bail|required',
          );
    
          $this->validate($request,$columns);

        try { 
            $names   = $request->certificate_names;
            $user_id = $request->user_id;
            $user    = User::find($user_id);

            if( $request->hasFile('certificate_files' ) )  
            {
              //Previos Records
              UserCertificates::where('notification_id',$request->notification_id)
                              ->where('user_id',$user_id)
                              ->delete();
              $i=1;
              foreach ($request->certificate_files as $key => $user_document) 
              {
                  $columns = array(
                
                  'user_document'    => 'bail|mimes:png,jpg,jpeg,PNG,JPG,JPEG,pdf,PDF|max:2048',
                );
               
                $this->validate($request,$columns);

                $destinationPath = "public/uploads/user-certificates";
                $fileName        = $user_id.'_user_document'.'._'.$key.$user_document->getClientOriginalName();
                  $user_document->move($destinationPath, $fileName);

                  $record                   = new UserCertificates();
                  $record->notification_id  = $request->notification_id;

                  if(array_key_exists( $key, $names) ){
                   
                    $record->name  = $names[$key];
                  }
                  $record->is_submitted = 1;
                  $record->image        = $fileName;
                  $record->user_id      = $user_id;
                  $record->role_id      = getRoleId($user_id);
                  $record->save();
               }              
            }
            else{

              $data['message'] = 'please_select_files';
              $data['status'] = 0;

              return $data;
            }
                
              $data['message'] = 'certificates_submitted_successfully';
              $data['status'] = 1;

              return $data;

         } 
     catch (Exception $e) {
            
        return $e;    
       }

    }


    //Chatting
    public function getStudentsandParentsId(Request $request){
    
      $course_subject_id = $request->course_subject_id;
       $record  = \App\CourseSubject::find($course_subject_id);
       $data = [];
       $all_students  = \App\Student::join('users','users.id','=','students.user_id')
                                ->where('academic_id',$record->academic_id)
                                ->where('course_parent_id',$record->course_parent_id) 
                                ->where('course_id',$record->course_id) 
                                ->where('current_year',$record->year) 
                                ->where('current_semister',$record->semister)
                                ->select(['users.name','roll_no','users.id','users.parent_id'])
                                ->get();
         $studentId = [];
        foreach ($all_students as $students) {
          $studentId[] = $students->parent_id;
        } 
        $all_parents = \App\User::whereIn('id',$studentId)
                                ->select(['name','id'])
                                ->get();
        $data['students'] = $all_students;
        $data['parents'] = $all_parents;
       return $data;                         
  }

    /**
     * Course listing method
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function userUploadsdoc($role_id, $user_id)
    {

        $records = array();
        $records = UserCertificates::where('role_id',$role_id)
                              ->where('user_id',$user_id)
                              ->orderBy('updated_at','desc')
                              ->get();

        return $records;

    }


    public function getStudentsCount(Request $request) {    

       $records = array();

        $boys_count = DB::table('students as stud')
                        ->join('users as usr', 'usr.id', 'stud.user_id')
                        ->where('stud.academic_id',$request->academic_id)
                        ->where('stud.course_parent_id',$request->course_parent_id)
                        ->where('stud.course_id',  $request->course_id)
                        ->where('stud.current_year', $request->year)
                        ->where('stud.current_semister',$request->semister)
                        ->where('stud.gender', 'male')
                        ->get();

        $girls_count = DB::table('students as stud')
                        ->join('users as usr', 'usr.id', 'stud.user_id')
                        ->where('stud.academic_id',$request->academic_id)
                        ->where('stud.course_parent_id',$request->course_parent_id)
                        ->where('stud.course_id',  $request->course_id)
                        ->where('stud.current_year', $request->year)
                        ->where('stud.current_semister',$request->semister)
                        ->where('stud.gender', 'female')
                        ->get();

      $present_count = DB::table('students as stud')
                        ->join('studentattendance as stud_att', 'stud_att.student_id', 'stud.id')
                        ->where('stud_att.academic_id',$request->academic_id)
                        ->where('stud_att.course_id',  $request->course_id)
                        ->where('stud_att.year', $request->year)
                        ->where('stud_att.semester',$request->semister)
                        ->where('stud_att.attendance_date', date('Y-m-d'))
                        ->where('stud_att.attendance_code', 'P')
                        ->get();

      $absent_count = DB::table('students as stud')
                        ->join('studentattendance as stud_att', 'stud_att.student_id', 'stud.id')
                        ->where('stud.academic_id',$request->academic_id)
                        ->where('stud.course_id',  $request->course_id)
                        ->where('stud.current_year', $request->year)
                        ->where('stud.current_semister',$request->semister)
                        ->where('stud_att.attendance_date', date('Y-m-d'))
                        ->where('stud_att.attendance_code', 'A')
                        ->get();

        $count = array(
                 "Total_Boys" => count($boys_count),
                 "Total_Girls" => count($girls_count),
                 "Today_Present" => count($present_count),
                 "Today_Absent" => count($absent_count)
        );
        
        $records['status'] = '1';
        $records['data'] = $count;
        return $records;
    }

}	