<?php

namespace App\Http\Controllers\API;

use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Exception;
use DB;

class ParentsController extends Controller
{
   
   public function getChildren($user_id)
   {
   	   
     
       $records = \App\User::join('students','students.user_id','=','users.id')
                           ->join('courses','courses.id','=','students.course_id')
                            ->select(['users.name', 'image','users.email',  'users.id','roll_no','course_title as class'])
                            ->where('users.parent_id', '=', $user_id)
                            ->get();

      if(count($records) > 0){

         $response['status'] = 1;
         $response['data']   = $records;
      }else{

        $response['status'] = 0;
       $response['message'] = 'Children data not updated';

      } 

      return $response;
   }

  
       public function subscriptions($user_id)
       {
           
           $records = \App\Payment::join('users', 'users.id','=','payments.user_id')
                                   ->where('users.parent_id','=',$user_id)
                                   ->select(['users.image', 'users.name',  'item_name', 'plan_type', 'start_date', 'end_date', 'payment_gateway','payments.updated_at','payment_status','payments.cost', 'payments.after_discount', 'payments.paid_amount','payments.id' ])
                                   ->orderby('payments.updated_at','desc')
                                   ->get();

             if(count($records) > 0){

                 $response['status'] = 1;
                 $response['data']   = $records;

              }else{

                $response['status'] = 0;
                $response['message'] = 'No subscriptions are available';

              } 
             return $response;
       }



    public function getPayChildren(Request $request)
    {
        $parent_id  = $request->parent_id;
        $item_id    = $request->item_id;
        $item_type  = $request->item_type;

        //parent user 
        $parent = \App\User::where('id',$parent_id)->get();

        $childs=[];

        if (!empty($parent)) {

            $parent = $parent[0];

            if ($item_id!="" && $item_type!="") {

              $record = $this->checkItem($item_type,$item_id);

              if (!empty($record)) {

                  //get children
                  $children = \App\User::join('students','students.user_id','=','users.id')
                             ->join('courses','courses.id','=','students.course_id')
                            ->select(['users.name','users.id','roll_no'])
                            ->where('users.parent_id', '=', $parent->id)
                            ->get();

                  if (!empty($children)) {

                      foreach($children as $key=>$child):
                        if(isItemPurchased($item_id, $item_type, $child->id))
                            unset($children[$key]);
                        else
                          $childs[] = $child;
                      endforeach;

                      $response['status']  = 1;
                      $response['data']    = $childs;
                      $response['message'] = 'Children';

                  } else {

                    $response['status']  = 0;
                    $response['message'] = 'No children found';

                  }
              } else {

                  $response['status']  = 0;
                  $response['message'] = $item_type. ' Record not found';
              }
            } else {

              $response['status']  = 0;
              $response['message'] = 'Invalid input';
            } 

        } else {

            $response['status']  = 0;
            $response['message'] = 'User not authenticated';
        }

        return $response;
    }


    public function checkItem($type,$item_id)
    {
        switch($type)
        {
          case 'combo':
            return \App\ExamSeries::where('id','=',$item_id)->first();
            break;
          case 'lms':
            return \App\LmsSeries::where('id','=',$item_id)->first();
            break;
          case 'exam':
            return \App\Quiz::where('id','=',$item_id)->first();
            break;
        }
        return null;
    }

    public function getStaffs($user_id){
       $data = [];
       $academicid = \App\Student::where('user_id',$user_id)
                                ->get(['academic_id']);
       $course_details  = \App\Student::join('courses','courses.id','=','students.course_id')
                                ->where('academic_id',$academicid[0]->academic_id)
                                ->where('user_id',$user_id) 
                                ->select(['courses.course_title'])
                                ->get();
        $course_details1  = \App\Student::join('courses','courses.id','=','students.course_parent_id')
                                ->where('academic_id',$academicid[0]->academic_id)
                                ->where('user_id',$user_id) 
                                ->select(['courses.course_title'])
                                ->get();

        $staffids = \App\Student::join('course_subject','course_subject.course_id','=','students.course_id')
                                ->where('course_subject.academic_id',$academicid[0]->academic_id)
                                ->where('user_id',$user_id) 
                                ->select(['course_subject.staff_id'])
                                ->get();
         $staffId = [];
        foreach ($staffids as $staff) {
          $staffId[] = $staff->staff_id;
        } 
        $all_staffs = \App\User::whereIn('id',$staffId)
                                ->select(['name','id'])
                                ->get();

        // $all_staffs = \App\Staff::whereIn('user_id',$staffId)
        //                         ->first();
        $data['staff'] = $all_staffs;

        $data['coursedetails'] = $course_details;
        $data['parentcourse'] = $course_details1;

        // print_r($all_staffs);
        // exit;
       return $data;                         
  }

  public function getchildrenId($user_id)
   {
     
     $records = \App\User::whereIn('parent_id',[$user_id])
                                ->select(['name','id'])
                                ->get();
      /* $records = \App\User::join('students','students.user_id','=','users.id')
                           ->join('courses','courses.id','=','students.course_id')
                            ->select(['users.name', 'image','users.email',  'users.id','roll_no','course_title as class'])
                            ->where('users.parent_id', '=', $user_id)
                            ->get();*/

      if(count($records) > 0){

         $response['status'] = 1;
         $response['data']   = $records;
      }else{

        $response['status'] = 0;
       $response['message'] = 'Children data not updated';

      } 

      return $response;
   }

   public function getTrack($user_id){

      $data['active_class']       = 'tracking';

      $user = \App\User::where('id','=',$user_id)->first();
      $role = getRoleData($user->role_id);
        if($role == 'parent'){
            $students = \App\User::whereIn('parent_id',[$user->id])
                                ->select(['id'])
                                ->get();
             $vehiclelist = \App\VehicleUser::join('users', 'users.id','=','vehicle_user.user_id')
                ->join('vdrivers','vdrivers.vehicle_id','=','vehicle_user.vehicle_id')
                ->join('vechicles','vechicles.id','=','vehicle_user.vehicle_id')
                ->whereIn('users.id',$students)
                ->where('vehicle_user.is_active','1')
                 ->select(['vehicle_user.vehicle_id','vdrivers.name as drivername','users.id','vdrivers.id','users.name as studentname','vechicles.number','vechicles.trip_status'])
                ->get();
        $data['vehiclelist'] = $vehiclelist;

        }else{
            $vehiclelist = \App\Vechicle::join('vdrivers', 'vdrivers.vehicle_id','=','vechicles.id')
                // ->join('vdrivers','vdrivers.vehicle_id','=','vehicle_user.vehicle_id')
                // ->join('vechicles','vechicles.id','=','vehicle_user.vehicle_id')
                // ->whereIn('users.id',$students)
                // ->where('vehicle_user.is_active','1')
                 ->select(['vdrivers.vehicle_id','vdrivers.name as drivername','vdrivers.id','vechicles.number','vechicles.trip_status'])
                ->get();
            $data['vehiclelist'] = $vehiclelist;
        }
     
      // DB::table('users as u')
      //                ->join('vehicle_user as vu', 'vu.user_id','=','u.id') 
      //                 ->join('vehicle_user as vu','vu.vehicle_id','=','vdrivers','vdrivers.vehicle_id')     

      
      // exit;

        /*$response['status'] = 1;
        $response['data']   = $data;*/
        
        return $data;
    }


    //Fetch Student Hostel Attendance Details
  public function getchildrenHosattendance($user_id)
 {
        $result = DB::table('users')
                  ->where('parent_id',$user_id)
                  ->get();

        $studentsid = [];
        foreach ($result as $value) {
          $studentsid[] = $value->id;
        }

        /*$result = DB::table('hostel_attendance')
                  ->where('parent_id',$user_id)
                  ->get();*/

        $result2 = DB::table('hostel_attendance_list as hosatnlst')
              ->join('users as use', 'hosatnlst.user_id', 'use.id')
              ->join('hostel_attendance as hosatn', 'hosatnlst.attendance_id', 'hosatn.id')
              ->select('use.name','use.image','hosatn.date','hosatnlst.status')
              ->whereIn('hosatnlst.user_id',$studentsid)
              ->get();

        print_r($result2);
        exit;


        $response['status']   = 1;
        $response['data']    = $result;
        return $response;
 }
//PARENTS SIDE CRUD OPERATION FOR STUDENT MEDICAL TREATMENT 
  public function add_std_medical_treat(Request $request)
  {
    $values = array(
                'treatment_name' => $request->treatment_name,
                'medicine_name' => $request->medicine_name,
                'student_id' => $request->student_id,
                'parent_id' => $request->parent_id,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
                );

        DB::table('medical_treatment')
        ->insert($values);

    $response['status']  = 1;
    $response['message'] = 'Medical Treatment Details Store successfully';
    return $response;
  }
  public function update_std_medical_treat(Request $request)
  {
    $values = array(
                'treatment_name' => $request->treatment_name,
                'medicine_name' => $request->medicine_name,
                'student_id' => $request->student_id,
                'parent_id' => $request->parent_id,
                'updated_at' => date('Y-m-d H:i:s')
                );

      DB::table('medical_treatment')
      ->where('id',$request->treatment_id)
      ->update($values);
    //$data =
    $response['status']  = 1;
    $response['message'] = 'Medical Treatment Details updated successfully';
    //$response['data'] = $data;
    return $response;
  }
  public function remove_std_medical_treat($treatment_id)
  {
      DB::table('medical_treatment')
      ->where('id',$treatment_id)
      ->delete();

    $response['status']  = 1;
    $response['message'] = 'Medical Treatment Details delete successfully';
    return $response;
  }
  public function get_std_medical_treat($parent_id)
  {
      $records = DB::table('medical_treatment as medtr')
      ->join('users as use','medtr.student_id','use.id')
      ->where('medtr.parent_id',$parent_id)
      ->select('medtr.id as treatment_id','medtr.student_id','medtr.parent_id','medtr.treatment_name','medtr.medicine_name','medtr.created_at','medtr.updated_at','use.name')
      ->paginate(getRecordsPerPage());

    if(count($records) > 0){

         $response['status'] = 1;
         $response['data']   = $records;
      }else{

        $response['status'] = 0;
       $response['message'] = 'Medical Treatment data not found';

      } 
      return $response;
  }
  public function get_std_all_medical_treat($student_id)
  {
      $records = DB::table('medical_treatment as medtr')
      ->join('users as use','medtr.student_id','use.id')
      ->where('medtr.student_id',$student_id)
      ->select('medtr.id as treatment_id','medtr.student_id','medtr.parent_id','medtr.treatment_name','medtr.medicine_name','medtr.created_at','medtr.updated_at','use.name')
      ->paginate(getRecordsPerPage());

    if(count($records) > 0){

         $response['status'] = 1;
         $response['data']   = $records;
      }else{

        $response['status'] = 0;
       $response['message'] = 'Medical Treatment data not found';

      } 
      return $response;
  }
  public function get_ind_std_medical_treat($treatment_id)
  {
      $records = $records = DB::table('medical_treatment as medtr')
      ->join('users as use','medtr.student_id','use.id')
      ->where('medtr.id',$treatment_id)
      ->select('medtr.id as treatment_id','medtr.student_id','medtr.parent_id','medtr.treatment_name','medtr.medicine_name','medtr.created_at','medtr.updated_at','use.name')
      ->get();

    if(count($records) > 0){

         $response['status'] = 1;
         $response['data']   = $records;
      }else{

        $response['status'] = 0;
       $response['message'] = 'Medical Treatment data not found';

      } 
      return $response;
  }

//PARENTS SIDE CRUD OPERATION FOR STUDENT ALLERGY FOOD
  public function add_std_allergy_food(Request $request)
  {
    $values = array(
                'allergy_food' => $request->allergy_food,
                'student_id' => $request->student_id,
                'parent_id' => $request->parent_id,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
                );

        DB::table('allergy_food')
        ->insert($values);

    $response['status']  = 1;
    $response['message'] = 'Allergy Food Details Store successfully';
    return $response;
  }
  public function update_std_allergy_food(Request $request)
  {
    $values = array(
                'allergy_food' => $request->allergy_food,
                'student_id' => $request->student_id,
                'parent_id' => $request->parent_id,
                'updated_at' => date('Y-m-d H:i:s')
                );

      DB::table('allergy_food')
      ->where('id',$request->allergy_food_id)
      ->update($values);
    //$data =
    $response['status']  = 1;
    $response['message'] = 'Allergy Food Details updated successfully';
    //$response['data'] = $data;
    return $response;
  }
  public function remove_std_allergy_food($allergy_food_id)
  {
      DB::table('allergy_food')
      ->where('id',$allergy_food_id)
      ->delete();

    $response['status']  = 1;
    $response['message'] = 'Allergy Food Details delete successfully';
    return $response;
  }
  public function get_std_allergy_food($parent_id)
  {
      $records = DB::table('allergy_food as allfo')
      ->join('users as use','allfo.student_id','use.id')
      ->where('allfo.parent_id',$parent_id)
      ->select('allfo.id as allergy_food_id','allfo.student_id','allfo.parent_id','allfo.allergy_food','allfo.created_at','allfo.updated_at','use.name', 'use.image')
      ->paginate(getRecordsPerPage());

    if(count($records) > 0){

         $response['status'] = 1;
         $response['data']   = $records;
      }else{

        $response['status'] = 0;
       $response['message'] = 'Allergy Food data not found';

      } 
      return $response;
  }
  public function get_std_all_allergy_food($student_id)
  {
      $records = DB::table('allergy_food as allfo')
      ->join('users as use','allfo.student_id','use.id')
      ->where('allfo.student_id',$student_id)
      ->select('allfo.id as allergy_food_id','allfo.student_id','allfo.parent_id','allfo.allergy_food','allfo.created_at','allfo.updated_at','use.name')
      ->paginate(getRecordsPerPage());

    if(count($records) > 0){

         $response['status'] = 1;
         $response['data']   = $records;
      }else{

        $response['status'] = 0;
       $response['message'] = 'Allergy Food data not found';

      } 
      return $response;
  }
  public function get_ind_std_allergy_food($allergy_food_id)
  {
      $records = $records = DB::table('allergy_food as allfo')
      ->join('users as use','allfo.student_id','use.id')
      ->where('allfo.id',$allergy_food_id)
      ->select('allfo.id as allergy_food_id','allfo.student_id','allfo.parent_id','allfo.allergy_food','allfo.created_at','allfo.updated_at','use.name')
      ->get();

    if(count($records) > 0){

         $response['status'] = 1;
         $response['data']   = $records;
      }else{

        $response['status'] = 0;
        $response['message'] = 'Allergy Food data not found';
      } 
      return $response;
  }

  //PARENTS SIDE CRUD OPERATION FOR STUDENT HEALTH INFO
  public function add_std_health(Request $request)
  {
    $values = array(
                'height' => $request->height,
                'weight' => $request->weight,
                'blood_group' => $request->blood_group,
                'student_id' => $request->student_id,
                'parent_id' => $request->parent_id,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
                );

        DB::table('basic_studhealth')
        ->insert($values);

    $response['status']  = 1;
    $response['message'] = 'Health Details Store successfully';
    return $response;
  }
  public function update_std_health(Request $request)
  {
    $values = array(
                'height' => $request->height,
                'weight' => $request->weight,
                'blood_group' => $request->blood_group,
                'student_id' => $request->student_id,
                'parent_id' => $request->parent_id,
                'updated_at' => date('Y-m-d H:i:s')
                );

      DB::table('basic_studhealth')
      ->where('studhealth_id',$request->studhealth_id)
      ->update($values);
    //$data =
    $response['status']  = 1;
    $response['message'] = 'Health Details updated successfully';
    //$response['data'] = $data;
    return $response;
  }
  public function remove_std_health($studhealth_id)
  {
      DB::table('basic_studhealth')
      ->where('studhealth_id',$request->studhealth_id)
      ->delete();

    $response['status']  = 1;
    $response['message'] = 'Health Details delete successfully';
    return $response;
  }
  public function get_std_health($parent_id)
  {
      $records = DB::table('basic_studhealth as basst')
      ->join('users as use','basst.student_id','use.id')
      ->where('basst.parent_id',$parent_id)
      ->select('basst.height','basst.weight','basst.blood_group','basst.student_id','basst.parent_id','basst.created_at','basst.updated_at','use.name',  'use.image')
      ->paginate(getRecordsPerPage());

    if(count($records) > 0){

         $response['status'] = 1;
         $response['data']   = $records;
      }else{

        $response['status'] = 0;
       $response['message'] = 'Health data not found';

      } 
      return $response;
  }
  public function get_std_all_health($student_id)
  {
      $records = DB::table('basic_studhealth as basst')
      ->join('users as use','basst.student_id','use.id')
      ->where('basst.student_id',$student_id)
      ->select('basst.height','basst.weight','basst.blood_group','basst.student_id','basst.parent_id','basst.created_at','basst.updated_at','use.name', 'use.image')
      ->paginate(getRecordsPerPage());

    if(count($records) > 0){

         $response['status'] = 1;
         $response['data']   = $records;
      }else{

        $response['status'] = 0;
       $response['message'] = 'Health data not found';

      } 
      return $response;
  }
  public function get_ind_std_health($basic_studhealth)
  { 

    $students = DB::table('users')
                ->where('parent_id',$basic_studhealth)
                ->select('name','id')
                ->get();
         
        $studentid = [];
          foreach($students as $value){
            $studentid[] = $value->id;
        }   

        $records =  DB::table('hostel_attendance_list as hosatnlst')
              ->join('users as use', 'hosatnlst.user_id', 'use.id')
              ->join('hostel_attendance as hosatn', 'hosatnlst.attendance_id', 'hosatn.id')
              ->select('hosatn.date','use.name','use.image','hosatnlst.status','hosatn.session')
              ->whereIn('hosatnlst.user_id',$studentid)
              ->get();

               print_r($records);
               exit;

     /* $records = DB::table('basic_studhealth as basst')
      ->join('users as use','basst.student_id','use.id')
      ->where('basst.studhealth_id',$basic_studhealth)
      ->select('basst.height','basst.weight','basst.blood_group','basst.student_id','basst.parent_id','basst.created_at','basst.updated_at','use.name')
      ->get();

    if(count($records) > 0){

         $response['status'] = 1;
         $response['data']   = $records;
      }else{

        $response['status'] = 0;
        $response['message'] = 'Health data not found';
      } 
      return $response;*/
  }

  public function getParentChildren($user_id) {
     $records = \App\User::join('students','students.user_id','=','users.id')
                         ->join('courses','courses.id','=','students.course_id')
                          ->select(['users.name', 'image','users.email',  'users.id','roll_no','course_title as class'])
                          ->where('users.parent_id', '=', $user_id)
                          ->paginate(getRecordsPerPage());

    if(count($records) > 0){
       $response['status'] = 1;
       $response['data']   = $records;
    }else{
      $response['status'] = 0;
      $response['message'] = 'Children data not updated';
    } 
    return $response;
  }



 public function get_std_health_record($student_id)
  {
      $records = DB::table('basic_studhealth as basst')
      ->join('allergy_food as allfo','basst.student_id','allfo.student_id')
      ->join('medical_treatment as medtr','basst.student_id','medtr.student_id')
      ->join('users as use','basst.student_id','use.id')
      ->where('basst.student_id',$student_id)
      ->select('basst.student_id','basst.parent_id','use.name as student_name','use.image', 
        'basst.height','basst.weight','basst.blood_group',
        'medtr.id as treatment_id','medtr.treatment_name','medtr.medicine_name',
        'allfo.id as allergy_food_id','allfo.get_std_all_allergy_food')
      ->orderby('basst.updated_at','desc')
      ->paginate(getRecordsPerPage());

    if(count($records) > 0){

         $response['status'] = 1;
         $response['data']   = $records;
      }else{

        $response['status'] = 0;
       $response['message'] = 'Health data not found';

      } 
      return $response;
  }
  

}