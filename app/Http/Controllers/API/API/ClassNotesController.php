<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ServiceProvider;

use DB;

class ClassNotesController extends Controller
{

  private $ServiceProvider;

  private $json_val;

  public function __construct()
  {
        if(!isset(self::$this->ServiceProvider)){
          $this->ServiceProvider = new ServiceProvider();
      }

      $path = 'public/NotifyMessage.json';
      $this->json_val = json_decode(file_get_contents($path), true);
  }

     /**
     * This method adds Class Notes to DB
     * @param  Request $request [Request Object]
     * @return void
     */

    public function addStaffClassNotes(Request $request) { 
        
        if(!$request->file_name) {
            $response['status']   = 0;
            $response['message']  = 'File is required';
            return $response;
        }

        /* Set Array For Class Notes Fields */
         $class_notes_slug = new \App\Assignments();
         $record = array(
            'staff_user_id' => $request->staff_id,
            'title' => $request->title,
            'slug' => $class_notes_slug->makeSlug($request->title),
            'subject_id' => $request->subject_id,
            'course_subject_id' => $request->course_subject_id,
            'type' => $request->type,
            'file_name' => $request->file_name,
            'description' => $request->description,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        );

        /* If check File Exists */
        if ($request->file('filename')) {

            $this->processUpload($request, $record,'filename','class_notes','id','file_name','NOTES/IMAGE');

            /*$filename = $request->file('filename');
            $filename->move('public/uploads/classnotes/',$request->file_name);*/ 
        }

        /* Insert Data & Get Last Record ID */

        $class_notes_id = DB::table('class_notes')
        ->insertGetId($record);

        if($request->type == 0){
            //if type == 0 for All Students
            
            /* Fetch Couse Data based on Cousres_Subject_ID */
            $course_data = DB::table('course_subject')
                                ->where('id', $request->course_subject_id)
                                ->first();

            /* Fetch Students Data based on course_data values */  
            $students  = DB::table('users as usr')
                            ->join('students as stud','stud.user_id','usr.id')
                            ->where('stud.academic_id',$course_data->academic_id)
                            ->where('stud.course_parent_id',$course_data->course_parent_id) 
                            ->where('stud.course_id',$course_data->course_id) 
                            ->where('stud.current_year',$course_data->year) 
                            ->where('stud.current_semister',$course_data->semister)
                            ->select('usr.id')
                            ->get();
                            

            /* For Each For Mulitple Students */     

           foreach ($students as $key=>$value) {
                
                /* Set Array For Class Notes Allocate Fields */
                $allocate_record = array(
                    'class_notes_id' => $class_notes_id,
                    'student_id' => $value->id,
                    'academic_id' => $course_data->academic_id,
                    'course_parent_id' => $course_data->course_parent_id,
                    'course_id' => $course_data->course_id,
                    'year' => $course_data->year,
                    'semister' => $course_data->semister,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                );
            
                /* Insert Data */
                DB::table('class_notes_allocate')
                    ->insert($allocate_record);

             $notificationusers  = DB::table('users as usr')
                            ->join('students as stud','stud.user_id','usr.id')
                            ->where('stud.academic_id',$course_data->academic_id)
                            ->where('stud.course_parent_id',$course_data->course_parent_id) 
                            ->where('stud.course_id',$course_data->course_id) 
                            ->where('stud.current_year',$course_data->year) 
                            ->where('stud.current_semister',$course_data->semister)
                            ->select(['usr.push_web_id','usr.push_app_id','usr.os_type','usr.name','usr.id','usr.app_type'])
                            ->get();


             $aptyp = DB::table('users')
                        ->where('id',$value->id)
                        ->first(['app_type']);

            $this->ServiceProvider->data = $notificationusers;
            $this->ServiceProvider->message = $this->json_val['NOWmsg0001A'].$this->json_val['NOWmsg0001B'];
            // hai . class notes added
            $this->ServiceProvider->pageId = "UNKNOWN";
            $this->ServiceProvider->aptyp = $aptyp->app_type;
            $this->ServiceProvider->insertNotification($this->ServiceProvider);
            }
            
         } else {
            //if type == 1 for selected students
            if (!$request->users_id) {
              
                $response['status']   = 0;
                $response['message']  = 'Please Select Students';
                return $response;

            } else {
                
                /* Fetch Couse Data based on Cousres_Subject_ID */
                $course_data = DB::table('course_subject')
                                ->where('id', $request->course_subject_id)
                                ->first();

                $students = $request->users_id;
                //$students = json_decode($students, true);

                /*print_r($students);
                exit;*/

                /* For Each For Selected Students */    
                foreach ($students as $key=>$value) {

                    /* Set Array For Class Notes Allocate Fields */
                    $allocate_record = array(
                        'class_notes_id' => $class_notes_id,
                        'student_id' => $value['user_id'],
                        'academic_id' => $course_data->academic_id,
                        'course_parent_id' => $course_data->course_parent_id,
                        'course_id' => $course_data->course_id,
                        'year' => $course_data->year,
                        'semister' => $course_data->semister,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    );

                    /* Insert Data */
                    DB::table('class_notes_allocate')
                        ->insert($allocate_record);


                 $notificationusers  = DB::table('users as usr')
                                    ->join('students as stud','stud.user_id','usr.id')
                                    ->where('stud.academic_id',$course_data->academic_id)
                                    ->where('stud.course_parent_id',$course_data->course_parent_id) 
                                    ->where('stud.course_id',$course_data->course_id) 
                                    ->where('stud.current_year',$course_data->year) 
                                    ->where('stud.current_semister',$course_data->semister)
                                    ->select(['usr.push_web_id','usr.push_app_id','usr.os_type','usr.name','usr.id','usr.app_type'])
                                    ->get();


                 $aptyp = DB::table('users')
                            ->where('id',$value['user_id'])
                            ->first(['app_type']);

            $this->ServiceProvider->data = $notificationusers;
            $this->ServiceProvider->message = $this->json_val['NOWmsg0001A'].$value['user_id'].$this->json_val['NOWmsg0001B'];
            // hai {{student_name}} class notes added
            $this->ServiceProvider->pageId = "UNKNOWN";
            $this->ServiceProvider->aptyp = $aptyp->app_type;
            $this->ServiceProvider->insertNotification($this->ServiceProvider);
                }
            }
         }

        /* Return Response */
        $response['status']  = 1;
        $response['message'] = 'Class Notes Added Successfully';
        return $response;
    }


    /**
     * This method returns the class_notes for staff
     * @return [type] [description]
     */
    public function getStaffClassNotes($staff_id) {

        $records = DB::table('class_notes as clsnotes')
                        ->leftJoin('subjects as subj', 'clsnotes.subject_id', 'subj.id')
                        ->select('clsnotes.id','clsnotes.type','clsnotes.title', 'clsnotes.description','clsnotes.subject_id','clsnotes.course_subject_id','clsnotes.file_name','clsnotes.slug','subj.subject_title')
                        ->where('clsnotes.staff_user_id',$staff_id)
                        ->orderBy('clsnotes.updated_at', 'desc')
                        ->paginate(getRecordsPerPage());
                        // ->get();

        $record_data = [];
        foreach ($records as $key => $value) {

            $class_details = [];
                
            $class_details = DB::table('course_subject as cs')
                                ->join('academics as acad', 'acad.id','cs.academic_id')
                                ->join('courses as cour', 'cour.id','cs.course_id')
                                ->where('cs.id',$value->course_subject_id)
                                ->select(DB::raw("CONCAT(academic_year_title,'-',course_title) AS class_name"), 'cour.course_title','cour.course_code','cs.id')
                                ->distinct('cs.id')
                                ->paginate(getRecordsPerPage());
                                // ->get();

            array_push($record_data, $class_details);
            
        }   
       
        $result = array('class_notes' => $records,'class_list' => $record_data );

        if(count($records) > 0 ){
             
              $response['status']   = 1;
              $response['data']  = $result;
              return $response;
        }
        else{
                
              $response['status']   = 0;
              $response['message']  = 'No data available';
              return $response;
        }   

    }

     /** 
     * This method returns the class_notes for staff
     * @return [type] [description]
     */

    public function deleteStaffClasNotes($class_notes_id) {

        DB::table('class_notes_allocate')
            ->where('class_notes_id',$class_notes_id)
            ->delete();

        DB::table('class_notes')
            ->where('id',$class_notes_id)
            ->delete();
  
        $response['status']  = 1;
        $response['message'] = 'Class Notes delete successfully';
        return $response;
    }

     /** 
     * This method returns the class_notes for student
     * @return [type] [description]
     */

public function getStudentClassNotes($student_id) {

        $user = DB::table('users')
                    ->where('id', $student_id)
                    ->first();

        $student = DB::table('students')
                    ->where('user_id', $user->id)
                    ->first();

        $records = DB::table('class_notes as cls_notes')
                        ->join('class_notes_allocate as cls_note_all','cls_note_all.class_notes_id','cls_notes.id')
                        ->join('users as usr','usr.id','cls_note_all.student_id')
                        ->join('users as usr_staff','usr_staff.id','cls_notes.staff_user_id')
                        ->join('subjects as subj','subj.id','cls_notes.subject_id')
                        ->join('courses as cour','cour.id','cls_note_all.course_id')
                        ->where('academic_id',$student->academic_id)
                        ->where('course_parent_id',$student->course_parent_id)
                        ->where('course_id',$student->course_id)
                        ->where('year',$student->current_year)
                        ->where('semister',$student->current_semister)
                        ->select('cls_notes.title','cls_notes.file_name', 'cls_notes.type as notes_type', 'cls_notes.description', 'subj.subject_title', 'subj.subject_code',
                        'usr_staff.name as staff_name', 'cour.course_title', 'cour.course_code', 'cls_note_all.created_at')
                        ->where('cls_note_all.student_id',$user->id)
                        ->orderby('cls_note_all.updated_at','desc')
                        ->paginate(getRecordsPerPage());
        
        if( count( $records ) > 0 ){

            $response['data']    = $records;
            $response['status']  = 1;
        }else{

            $response['message']  = 'No Class Notes available';
            $response['status']   = 0;
        }                                
        return $response;
    }


    public function getAllocatedStudentClassNotes($class_notes_id) {

        $records = DB::table('class_notes_allocate')
                        ->select('student_id')
                        ->where('class_notes_id',$class_notes_id)                        
                        ->get();

                        // print_r($records);
                        // exit;
        
        if(count( $records ) > 0 ){

            $response['data']    = $records;
            $response['status']  = 1;
        }else{

            $response['message']  = 'No Class Notes available';
            $response['status']   = 0;
        }                                
        

        return $response;
    }


     /**
     * This method update Class Notes to DB
     * @param  Request $request [Request Object]
     * @return void
     */

    public function updateStaffClassNotes(Request $request, $class_notes_id) { 


        $data = DB::table('class_notes')
                     ->where('id', $class_notes_id)
                     ->first();
      
        DB::table('class_notes_allocate')
            ->where('class_notes_id',$data->id)
            ->delete();
            
        if(!$request->file_name) {
            $response['status']   = 0;
            $response['message']  = 'File is required';
            return $response;
        }
        
        /* Set Array For Class Notes Fields */
         $class_notes_slug = new \App\Assignments();
         $record = array(
            'staff_user_id' => $request->staff_id,
            'title' => $request->title,
            'slug' => $class_notes_slug->makeSlug($request->title),
            'subject_id' => $request->subject_id,
            'course_subject_id' => $request->course_subject_id,
            'type' => $request->type,
            'file_name' => $request->file_name,
            'description' => $request->description,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        );

        /* If check File Exists */
        if ($request->file('filename')) {
            
            $this->processUpload($request, $record,'filename','class_notes','id','file_name','NOTES/IMAGE');

/*            $filename = $request->file('filename');
            $filename->move('public/uploads/classnotes/',$request->file_name); 
*/        }

        /* Update Data & Get Last Record ID */

        DB::table('class_notes')
                ->where('id',$data->id)
                ->update($record);

        if($request->type == 0){
            //if type == 0 for All Students
            
            /* Fetch Couse Data based on Cousres_Subject_ID */
            $course_data = DB::table('course_subject')
                                ->where('id', $request->course_subject_id)
                                ->first();

            /* Fetch Students Data based on course_data values */  
            $students  = DB::table('users as usr')
                            ->join('students as stud','stud.user_id','usr.id')
                            ->where('stud.academic_id',$course_data->academic_id)
                            ->where('stud.course_parent_id',$course_data->course_parent_id) 
                            ->where('stud.course_id',$course_data->course_id) 
                            ->where('stud.current_year',$course_data->year) 
                            ->where('stud.current_semister',$course_data->semister)
                            ->select('usr.id')
                            ->get();

            /* For Each For Mulitple Students */     
                               
           foreach ($students as $key=>$value) {
                
                /* Set Array For Class Notes Allocate Fields */
                $allocate_record = array(
                    'class_notes_id' => $data->id,
                    'student_id' => $value->id,
                    'academic_id' => $course_data->academic_id,
                    'course_parent_id' => $course_data->course_parent_id,
                    'course_id' => $course_data->course_id,
                    'year' => $course_data->year,
                    'semister' => $course_data->semister,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                );
            
                /* Insert Data */
                DB::table('class_notes_allocate')
                    ->insert($allocate_record);

             $notificationusers  = DB::table('users as usr')
                                ->join('students as stud','stud.user_id','usr.id')
                                ->where('stud.academic_id',$course_data->academic_id)
                                ->where('stud.course_parent_id',$course_data->course_parent_id) 
                                ->where('stud.course_id',$course_data->course_id) 
                                ->where('stud.current_year',$course_data->year) 
                                ->where('stud.current_semister',$course_data->semister)
                                ->select(['usr.push_web_id','usr.push_app_id','usr.os_type','usr.name','usr.id','usr.app_type'])
                                ->get();


                 $aptyp = DB::table('users')
                            ->where('id',$value->id)
                            ->first(['app_type']);

            $this->ServiceProvider->data = $notificationusers;
            $this->ServiceProvider->message = $this->json_val['NOWmsg0002A'].$value->id.$this->json_val['NOWmsg0002B'];
            // hai.{{student_name}}.class notes updated
            $this->ServiceProvider->pageId = "UNKNOWN";
            $this->ServiceProvider->aptyp = $aptyp->app_type;
            $this->ServiceProvider->insertNotification($this->ServiceProvider);
            }
            
         } else {
            //if type == 1 for selected students
            if (!$request->users_id) {
              
                $response['status']   = 0;
                $response['message']  = 'Please Select Students';
                return $response;

            } else {
                
                /* Fetch Couse Data based on Cousres_Subject_ID */
                $course_data = DB::table('course_subject')
                                ->where('id', $request->course_subject_id)
                                ->first();

                $students = $request->users_id;

                $students = json_decode($students, true);

                /* For Each For Selected Students */    
                foreach ($students as $key=>$value) {

                    /* Set Array For Class Notes Allocate Fields */
                    $allocate_record = array(
                        'class_notes_id' => $data->id,
                        'student_id' => $value['user_id'],
                        'academic_id' => $course_data->academic_id,
                        'course_parent_id' => $course_data->course_parent_id,
                        'course_id' => $course_data->course_id,
                        'year' => $course_data->year,
                        'semister' => $course_data->semister,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    );

                    /* Insert Data */
                    DB::table('class_notes_allocate')
                        ->insert($allocate_record);

                 $notificationusers  = DB::table('users as usr')
                                    ->join('students as stud','stud.user_id','usr.id')
                                    ->where('stud.academic_id',$course_data->academic_id)
                                    ->where('stud.course_parent_id',$course_data->course_parent_id) 
                                    ->where('stud.course_id',$course_data->course_id) 
                                    ->where('stud.current_year',$course_data->year) 
                                    ->where('stud.current_semister',$course_data->semister)
                                    ->select(['usr.push_web_id','usr.push_app_id','usr.os_type','usr.name','usr.id','usr.app_type'])
                                    ->get();


                 $aptyp = DB::table('users')
                            ->where('id',$value['user_id'])
                            ->first(['app_type']);

            $this->ServiceProvider->data = $notificationusers;
            $this->ServiceProvider->message =$this->json_val['NOWmsg0001']. $value['user_id'].$this->json_val['NOWmsg0001C'];
            // hai{{student_name}} class notes updated
            $this->ServiceProvider->pageId = "UNKNOWN";
            $this->ServiceProvider->aptyp = $aptyp->app_type;
            $this->ServiceProvider->insertNotification($this->ServiceProvider);
                }
            }
         }

        /* Return Response */
        $response['status']  = 1;
        $response['message'] = 'Class Notes Updated Successfully';
        return $response;
    }


    
public function getHostalAttendance($parent_id) {
   
    $students = DB::table('users')
            ->select('name','id')
            ->whereIn('parent_id',[$parent_id])
            ->get();
     
    $studentid = [];
    foreach($students as $value){
      $studentid[] = $value->id;            
    }   


    $attendance_list = DB::table('hostel_attendance_report')
                            ->whereIn('user_id',$studentid)
                            ->get();  

    $response = array();                          
      
    $array = [];
    
    $i = 0;
    foreach($attendance_list as $new_record){
            
            $timestamp = (strtotime($new_record->created_at));
            $date = date('Y-m-d', $timestamp);
            
            $new_record->attendance_data = DB::table('users as usr')
                                ->leftjoin('hostel_attendance_report as hsr', 'usr.id', 'hsr.user_id')
                                ->leftjoin('hostel_attendance as ha', 'ha.id','hsr.attendance_id')
                                ->leftjoin('hostel as hos', 'hos.id','ha.hostel_id')
                                ->leftjoin('hostel_rooms as hroom', 'hroom.id','ha.room_id')
                                // ->whereIn('hsr.attendance_id',[$new_record->attendance_id])
                                ->whereIn('hsr.user_id',[$new_record->user_id])
                                ->whereIn('ha.date',[$date])
                                ->groupBy('hsr.user_id','ha.date')                                
                                ->select('hsr.user_id', 'usr.name as student_name', 'ha.id', 
                                'hsr.attendance_id', 'ha.date', 
                                'hos.name as hostal_name',
                                'hroom.room_number',
                                    DB::raw("(GROUP_CONCAT(hsr.status SEPARATOR ',')) as `Attendace_Status`"),
                                    DB::raw("(GROUP_CONCAT(ha.session SEPARATOR ',')) as `Attendance_Session`"))
                                    // ->distinct(['hsr.attendance_id'])
                                    // ->paginate(getRecordsPerPage());
                                    // ->union('hsr.attendance_id' -> $new_record->attendance_id)
                                    ->first();

                                    

                            //    $record_data = array(
                            //         'user_id' => $attendance_data[$i]->user_id,
                            //         'student_name' => $attendance_data[$i]->student_name,
                            //         'id' => $attendance_data[$i]->id,
                            //         'attendance_id' => $attendance_data[$i]->attendance_id,
                            //         'date' => $attendance_data[$i]->date,
                            //         'hostal_name' => $attendance_data[$i]->hostal_name,
                            //         'room_number' => $attendance_data[$i]->room_number,
                            //         'status' => $attendance_data[$i]->Attendace_Status,
                            //         'session' => $attendance_data[$i]->Attendance_Session
                            //    );
                               
                            $array  =  array_push($response, $new_record);


    } 
    $i++;

    $data['response'] = $array;
    $data['data'] = $response;
    return $data;
  }

  public function processUpload(Request $request, $record, $file_name, $table_name,$column_name,$update_column_name,$tag)
     {  
         if ($request->hasFile($file_name)) {
            $path = $_FILES[$file_name]['name'];
            $ext = pathinfo($path, PATHINFO_EXTENSION);
            $fileName = $column_name.'-'.$file_name.'.'.$ext;
            // print_r($record);
            // exit();
            $this->ServiceProvider->original_filename = $file_name;
            $this->ServiceProvider->file_name = $fileName;
            $this->ServiceProvider->record = $record;
            $this->ServiceProvider->userId = $request->staff_id;
            $this->ServiceProvider->table = $table_name; 
            $this->ServiceProvider->update_column_name = $update_column_name;
            $this->ServiceProvider->column_name = $column_name;
            $this->ServiceProvider->tag = $tag; 
            $this->ServiceProvider->uploadFiles($this->ServiceProvider);
        }
     }

}
