<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App;
use App\Http\Requests;
use App\User;
use App\Student;
use App\Course;
use App\Academic;
use App\StudentPromotion;
use App\LibraryIssue;
use App\LibraryMaster;
use App\FeeParticularPayment;
use App\GeneralSettings as Settings;
use Image;
use ImageSettings;
use Yajra\Datatables\Datatables;
use DB;
use Illuminate\Support\Facades\Hash;
use Excel;
use Input;
use File;
use Exception;
use Auth;
 

class FeeController extends Controller
{
	 /**
    This Method Display The Fee Shules for the student 
    **/
    public function feeShedulesView($id, $feecat_id)
    {

      $response['status'] = 0;
      $response['message'] = '';



      if($id==''){
       
       $response['message'] = 'Student ID is missing';
       return $response;
      
      }

      $user  = \App\User::where('id',$id)->first(); 
      $student_details = \App\Student::where('user_id','=',$user->id)->first();
      // $response['student_details'] = $student_details;
      // return $response;

       // $response['feecat_id'] = $feecat_id;
       // return $response;

      $allFeeCategories    = \App\FeeScheduleParticular::getAllCategories($student_details->id);
         // dd($allFeeCategories);
         
         if(count($allFeeCategories)==0){
           $response['message'] = 'No schedules available';
           return $response;
         }

      

       if(!$student_details)
       {
         $response['message'] = 'Student is not avilable';
         return $response;
       }
      
      // $feecategory_details =  \App\FeeCategory::getCategory($student_details);
      $feecategory_details =  \App\FeeCategory::where('id','=',$feecat_id)->first();
        // $response['feecategory_details'] = $feecategory_details;
       // return $response;

        if(!$feecategory_details)
       {
         $response['message'] = 'FeeCategory is not avilable';
         return $response;
       }

      // return $response['mydata']  = $feecategory_details;

         if(!$feecategory_details)
         {
            $response['message'] = 'No Fee details avaialble';
            return $response;
         }

      $student_id = $student_details->id;
      $feecategory_id = $feecategory_details->id;

      $academic_id        = $feecategory_details->academic_id;
      $course_parent_id   = $feecategory_details->course_parent_id;
      $course_id          = $feecategory_details->course_id;
      $year               = $feecategory_details->year;
      $semister           = $feecategory_details->semister;


      $data['student_id']     = $student_details->id;
      $data['fee_categories'] = $allFeeCategories;

      $feecategory_details->final_fee =  $feecategory_details->total_fee + $feecategory_details->other_amount;

      $data['fee_details']    =  $feecategory_details;
      $data['feecategory_id'] = $feecategory_details->id;
      $data['student_details'] = $student_details;


      $data['particulars'] = \App\FeeParticularPayment::getStudentSchedules($student_id,$feecategory_details->id);
      $data['user_details'] = \App\User::where('id','=',$student_details->user_id)->first();
      $data['currency']   = getSetting('currency_code','site_settings');
      $data['feeshedules']  = \App\FeeScheduleParticular::getStudentSchedules($feecategory_id,$student_id);

      // dd($data['fee_categories']);
      // $data['particulars']  = App\FeeParticularPayment::getStudentSchedules($student_id,$feecategory_id);

         $student_data = App\Student::join('feeparticular_paymets','feeparticular_paymets.student_id','=','students.id')
        ->join('users', 'users.id' ,'=', 'students.user_id')  
        ->join('academics','academics.id','=','students.academic_id') 
        ->join('courses','courses.id','=','students.course_id')
        ->where('feeparticular_paymets.academic_id','=',$academic_id)
        ->where('feeparticular_paymets.course_parent_id','=',$course_parent_id)
        ->where('feeparticular_paymets.course_id','=',$course_id)
        ->where('feeparticular_paymets.year','=',$year)
        ->where('feeparticular_paymets.semister','=',$semister)
        ->where('feeparticular_paymets.student_id','=',$student_id)
        ->select(['students.id as id','users.name', 'students.roll_no','students.admission_no', 'course_title','academic_year_title', 'email', 'current_year', 'current_semister','course_dueration','students.academic_id as academic_id', 'students.course_id as course_id', 'students.user_id as user_id','users.slug'])
        ->groupBy('students.id')
        ->get();

        $data['student_data']  = $student_data;



      /*$total_fee_details = [];
      $total_fee_details['total_installments']=$feecategory_details->total_installments;
      $total_fee_details['total_fee'] = $feecategory_details->total_fee;
      $total_fee_details['installment_amount']=$feecategory_details->installment_amount;
      $total_fee_details['other_amount'] = $feecategory_details->other_amount;
      $total_fee_details['final_fee'] = $feecategory_details->total_fee + $feecategory_details->other_amount;

      $data['total_fee_details'] = $total_fee_details;*/


      $response['status'] = 1;
      $response['data'] = $data;
      return $response;
         
      // $target_items     = array();
      //    $target_items[]   = $id;
      //    $target_items[]   = $feecategory_details->id;
      //    $data['right_bar']          = TRUE;
      //   $data['right_bar_path']     = 'common.student-total-feecategories';
      //   $data['right_bar_data']     = array(
      //                                       'item' => $allFeeCategories,
      //                                       );
       
       // $data['items']              = json_encode(array('source_items'=>$allFeeCategories,'target_items'=>$target_items));

      //  if(checkRole(getUserGrade(5))){ 
      // $data['active_class']  = 'fee';
      // }
      // else{
      //    $data['active_class']  = 'users';
      // }
       
       // return view('common.student-feeschedules',$data);
        
    }

     /**
    This Method Display The Fee Paid History of selected student 
    **/
    public function feePaidHistoryView($id)
    {
      
     $response['status'] = 0;
      $response['message'] = '';

      if($id==''){
       
       $response['message'] = 'Student ID is missing';
       return $response;
      
      }

      $allFeeCategories    = \App\FeeScheduleParticular::getAllCategories($id);
        if(count($allFeeCategories)==0){
           $response['message'] = 'No schedules available';
           return $response;
         }

     
      $student_details = \App\Student::where('id','=',$id)->first();

       if(!$student_details)
       {
         $response['message'] = 'Student is not avilable';
         return $response;
       }
      
      $feecategory_details =  \App\FeeCategory::getCategory($student_details);

         if(!$feecategory_details)
         {
            $response['message'] = 'No Fee details avaialble';
            return $response;
         }


    $student_id = $id;
    $feecategory_id = $feecategory_details->id;
      
      // $data['student_id']    = $id;
      // $data['feecategory_id'] = $feecategory_details->id;
      
        
       //  $target_items     = array();
       //  $target_items[]   = $id;
       //  $target_items[]   = $feecategory_details->id;
       //  $data['right_bar']          = TRUE;
       //  $data['right_bar_path']     = 'common.student-total-feecategories';
       //  $data['right_bar_data']     = array(
       //                                      'item' => $allFeeCategories,
       //                                      );
       
       // $data['items']              = json_encode(array('source_items'=>$allFeeCategories,'target_items'=>$target_items));

        
      //   if(checkRole(getUserGrade(5))){ 
      // $data['active_class']  = 'fee';
      // }
      // else{
      //    $data['active_class']  = 'users';
      // }
      // $particulars = App\FeeParticularPayment::getStudentSchedules($student_id,$feecategory_id);
      $student_details = \App\Student::where('id','=',$student_id)->first();
      // $feecategory_details =  App\FeeCategory::getCategory($student_details);
      $currency  = getSetting('currency_symbol','site_settings');
      // $feeshedules  = App\FeeScheduleParticular::getStudentSchedules($feecategory_id,$student_id);
       
       $payments = $this->getStudentFeePaidDetails($feecategory_id, $student_id);


       $data['student'] = $student_details;
       $data['currency'] = $currency;
       $data['payments'] = $payments;
       // return view('common.student-feehistory',$data);
        $response['data'] = $data;
        return $response;
    }


      /**
    This Method return the student fee paid history details
    based on studentid and feecategory_id
    **/
    public function getStudentFeePaidDetails($feecategory_id, $student_id)
    {

      // return $request;
       $finaldata = array();
       // $feecategory_id = $request->feecategory_id;
       // $student_id     = $request->student_id;
       $previous_details = [];

      $paid_data   = \App\FeePayment::where('feecategory_id','=',$feecategory_id)
                                 ->where('student_id','=',$student_id)
                                 ->get();

      $feecategory_details =  \App\FeeCategory::where('id','=',$feecategory_id)->first();
      
       $academic_id        = $feecategory_details->academic_id;
      $course_parent_id   = $feecategory_details->course_parent_id;
      $course_id          = $feecategory_details->course_id;
      $year               = $feecategory_details->year;
      $semister           = $feecategory_details->semister;
   
     $student_data = \App\Student::join('feeparticular_paymets','feeparticular_paymets.student_id','=','students.id')
        ->join('users', 'users.id' ,'=', 'students.user_id')  
        ->join('academics','academics.id','=','students.academic_id') 
        ->join('courses','courses.id','=','students.course_id')
        ->where('feeparticular_paymets.academic_id','=',$academic_id)
        ->where('feeparticular_paymets.course_parent_id','=',$course_parent_id)
        ->where('feeparticular_paymets.course_id','=',$course_id)
        ->where('feeparticular_paymets.year','=',$year)
        ->where('feeparticular_paymets.semister','=',$semister)
        ->where('feeparticular_paymets.student_id','=',$student_id)
        ->select(['students.id as id','users.name', 'students.roll_no','students.admission_no', 'course_title','academic_year_title', 'email', 'current_year', 'current_semister','course_dueration','students.academic_id as academic_id', 'students.course_id as course_id', 'students.user_id as user_id','users.slug'])
        ->groupBy('students.id')
        ->get();
      
      $finaldata['feecategory_details'] = $feecategory_details;
      $finaldata['paid_data']           = $paid_data;
      $finaldata['student']             = $student_data;

      $previous_particulars = \App\FeeParticularPayment::where('student_id','=',$student_id)
                                                ->where('feecategory_id','=',$feecategory_id) 
                                                ->where('carry_forward','=',0);

      $previous_fee_particulars  =  $previous_particulars->get();

        if(count($previous_fee_particulars)){

          foreach ($previous_fee_particulars as $previous_particular) {

        $category_title  =  App\FeeCategory::where('id','=',$previous_particular->previous_feecategory_id)->first()->title;
        $particular_name =  App\Feeparticulars::where('id','=',$previous_particular->feeparticular_id)->first()->title;
            $temp1['title']              = $category_title; 
            $temp1['particular_title']   = $particular_name; 
            $temp1['term_number']        = $previous_particular->term_number; 
            $temp1['is_schedule']        = $previous_particular->is_schedule; 
            $temp1['amonut']             = $previous_particular->amount; 
            $previous_details[]          = $temp1;

          }

        $previous_amount = $previous_particulars->sum('amount');
        $finaldata['previous_details']     = $previous_details;
        $finaldata['previous_amount']      = round($previous_amount);
        }
        else{
          $finaldata['previous_details'] = null;
          $finaldata['previous_amount']      = 0;
        }                                           

      return $finaldata;
      


    }



//     public function feeUserData($user_id, $type)
//     {
      
//        $pending_data   = App\FeeParticularPayment::getStudentFeeData($user_id,'pending');
//        $paid_data      = App\FeeParticularPayment::getStudentFeeData($user_id,'paid');

//        $final_data   = [];
//        $temp         = [];
//        $final_data1  = [];
//        $temp1        = [];

//       if( count($pending_data) > 0 ){

//           foreach ($pending_data as $record) {

//              $temp['title']        = $record->title;
//              $temp['term_number']  = $record->term_number;
//              $temp['start_date']   = $record->start_date;
//              $temp['end_date']     = $record->end_date;
//              $temp['feecategory_id']     = $record->feecategory_id;

//              $amount   = App\FeeParticularPayment::where('feeschedule_particular_id',$record->feeschedule_particular_id)
//                                                     ->where('user_id',$user_id)
//                                                     ->sum('amount');
//              $temp['amount']       = (int)$amount;
//              $temp['status']       = "pending";
//              $final_data[]         = $temp;

//          }

//      }
   
//       if( count($paid_data) > 0 ){
       
//          foreach ($paid_data as $record) {
           
//              $temp1['title']        = $record->title;
//              $temp1['term_number']  = $record->term_number;
//              $temp1['start_date']   = $record->start_date;
//              $temp1['end_date']     = $record->end_date;
//              $temp1['feecategory_id'] = $record->feecategory_id;

//              $amount   = App\FeeParticularPayment::where('feeschedule_particular_id',$record->feeschedule_particular_id)
//                                                     ->where('user_id',$user_id)
//                                                     ->sum('paid_amount');
//              $temp1['amount']        = (int)$amount;
//              $temp1['status']        = "paid";
//              $final_data1[]          = $temp1;

//          }

        
//      }
       
//        if( $type == "all"  )
//        {

//           $all_data   = array_merge($final_data1,$final_data);

//           $response['status']  = 1;
//           $response['data']    = $all_data;

//        }

//        elseif ($type == "pending") {

//           $response['status']  = 1;
//           $response['data']    = $final_data;
//        }
//        elseif ($type == "paid") {

//           $response['status']  = 1;
//           $response['data']    = $final_data1;
//        }
//        else{

//               $response['status']  = 0;
//               $response['data']    = 'No data available';

//        }  

//        $response['currency']   = getSetting('currency_code','site_settings');


//               return $response;
//     }

    public function feeUserData($user_id, $type)
    {

      // Pending
      if($type == 'pending') {
        
        $pending_data = FeeParticularPayment::join('feecategories','feecategories.id','=','feeparticular_paymets.feecategory_id')
                          ->join('feeschedule_particulars','feeschedule_particulars.id','=','feeparticular_paymets.feeschedule_particular_id')
                          ->select(['feeschedule_particular_id','title','term_number','start_date','end_date','feeparticular_paymets.feecategory_id'])
                          ->where('feeparticular_paymets.feepayment_id','=',0)
                          ->where('feeparticular_paymets.user_id',$user_id)
                          ->groupby('feeschedule_particular_id')
                          ->paginate(getRecordsPerPage());

        if( count($pending_data) > 0 ){

          foreach ($pending_data as $i => $record) {
              $amount   = App\FeeParticularPayment::where('feeschedule_particular_id',$record->feeschedule_particular_id)
                                                    ->where('user_id',$user_id)
                                                    ->sum('amount');
              $pending_data[$i]->amount       = (int)$amount;
              $pending_data[$i]->status     = "pending";
          }

        }
      
      // Paid
      } else if($type == 'paid') {
        $paid_data   = FeeParticularPayment::join('feecategories','feecategories.id','=','feeparticular_paymets.feecategory_id')
                        ->join('feeschedule_particulars','feeschedule_particulars.id','=','feeparticular_paymets.feeschedule_particular_id')
                        ->select(['feeschedule_particular_id','title','term_number','start_date','end_date','feeparticular_paymets.feecategory_id'])
                        ->where('feeparticular_paymets.feepayment_id','!=',0)
                        ->where('feeparticular_paymets.user_id',$user_id)
                        ->groupby('feeschedule_particular_id')
                        ->paginate(getRecordsPerPage());

        if( count($paid_data) > 0 ){

          foreach ($paid_data as $i => $record) {
              $amount   = App\FeeParticularPayment::where('feeschedule_particular_id',$record->feeschedule_particular_id)
                                                      ->where('user_id',$user_id)
                                                      ->sum('paid_amount');
              $paid_data[$i]->amount = (int)$amount;
              $paid_data[$i]->status = "paid";
          }

        }

      }

      if( $type == "all") {

        $all_data   = array_merge($final_data1,$final_data);

        $response['status']  = 1;
        $response['data']    = $all_data;

      } else if ($type == "pending") {

        $response['status']  = 1;
        $response['data']    = $pending_data;

      } else if ($type == "paid") {

        $response['status']  = 1;
        $response['data']    = $paid_data;

      } else {

        $response['status']  = 0;
        $response['data']    = 'No data available';

      }  

      $response['currency']   = getSetting('currency_code','site_settings');
      return $response;

    }
}

