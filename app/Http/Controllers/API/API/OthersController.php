<?php

namespace App\Http\Controllers\API;

use App\User;
use Validator;
use \App;
use \Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Image;
use ImageSettings;
use App\Subject;
use Exception;
use DB;

class OthersController extends Controller
{  

       /**
     * This method will reset 
     * @param  Request $request [description]
     * @return [type]           [description]
     */
      public function resetUsersPassword(Request $request)
     {

        $user_type = $request->user_type;

         if (!$user_type) {
          $response['status'] = 0;
          $response['message'] = 'Specify user type';
         }


          $role_id=5;
          if ($user_type=="staff")
            $role_id=3;
          elseif ($user_type=="parent")
            $role_id=6;
          elseif ($user_type=="student")
            $role_id=5;

        // dd($request);
         $user  = \App\User::where('email','=',$request->email)
                            ->where('role_id','=',$role_id)
                            ->first();

         $response['status'] = 0;
         $response['message'] = '';
         
         if(!$user)
         {
            $response['message'] = 'Invalid User';
            return $response;
         }

          \DB::beginTransaction();

         try{

         if($user!=null){

           $password       = str_random(8);
           $user->password = bcrypt($password);

           $user->save();
           
           \DB::commit();
           
           try {
             
             $user->notify(new \App\Notifications\UserForgotPassword($user,$password));

           } catch (Exception $e) {
             
           }

           $response['status'] = 1;
           $response['message'] = 'New password is sent to your email account';

         }

         else{

            $response['status'] = 0;
           $response['message'] = 'No Email exists';
            
         }
      }

      catch(\Exception $ex){
          \DB::rollBack();
          $response['message'] = $ex->getMessage();
      }
         
         
        return $response;
         
     }


    public function resetUsersPasswordAPI(Request $request)
     {

        // dd($request);
         $user  = \App\User::where('email','=',$request->email)
                            ->first();

         $response['status'] = 0;
         $response['message'] = '';
         
         if(!$user)
         {
            $response['message'] = 'Invalid User';
            return $response;
         }

          \DB::beginTransaction();

         try{

         if($user!=null){

           $password       = str_random(8);
           $user->password = bcrypt($password);

           $user->save();
           
           \DB::commit();
           
           try {
             
             $user->notify(new \App\Notifications\UserForgotPassword($user,$password));

           } catch (Exception $e) {
             
           }

           $response['status'] = 1;
           $response['message'] = 'New password is sent to your email account';

         }

         else{

            $response['status'] = 0;
           $response['message'] = 'No Email exists';
            
         }
      }

      catch(\Exception $ex){
          \DB::rollBack();
          $response['message'] = $ex->getMessage();
      }
         
         
        return $response;
         
     }

       /**
     * This method updates the password submitted by the user
     * @param  Request $request [description]
     * @return [type]           [description]
     */
     public function updatePassword(Request $request)
    {

      $response['status'] = 0;
      $response['message'] = '';

        $columns = [
            'old_password' => 'required',
            'password'     => 'required|confirmed',
        ];

         $validated =  \Validator::make($request->all(),$columns);
            if(count($validated->errors()))
            {
                $response['status'] = 0;        
                $response['message'] = 'Validation Errors';
                $response['errors'] = $validated->errors()->messages();
                return $response;
            }    

             $credentials = $request->only('old_password', 'password', 'password_confirmation');


        $id = $request->user_id;
      
        $user = \App\User::where('id', '=', $id)->first();

      if(!$user)
      {
        $response['message'] = 'Invalid Userid';
        return $response;
      }

        
        if (\Hash::check($credentials['old_password'], $user->password)){
            $password = $credentials['password'];
            $user->password = bcrypt($password);
            $user->save();
            $response['status'] = 1;
            $response['message'] = 'Password updated successfully';

        }else {
            $response['status'] = 0;
            $response['message'] = 'Old and new passwords are not same';
       }

       return $response;
  }


     public function saveFeedback(Request $request)
   {
      $user_id = $request->user_id;


         $columns = array(
                            'title'                   => 'bail|required|max:40' ,
                            'subject'                 => 'bail|required|max:40' ,
                            'description'             => 'bail|required' ,
                            );
           
            $validated =  \Validator::make($request->all(),$columns);
            if(count($validated->errors()))
            {
                $response['status'] = 0;        
                $response['message'] = 'Validation Errors';
                $response['errors'] = $validated->errors()->messages();
                return $response;
            }    

        $record = new \App\Feedback();
        $name             =  $request->title;
        $record->title        = $name;
        $record->slug         = $record->makeSlug($name);
        $record->subject      = $request->subject;
        $record->description    = $request->description;
        $record->user_id      = $user_id;
        $record->save();

      $response['status'] = 1;
      $response['message'] = 'Feedback updated successfully';
      return $response;
   }


   public function notifications()
  {
      $response['status'] = 1;
          $response['message'] = '';
           $date = date('Y-m-d');
     $records = \App\Notification::where('valid_from', '<=', $date)
                              ->where('valid_to', '>=', $date)
                              //->where('notifiable_id', $id)
                              ->select(['title', 'valid_from', 'valid_to', 'url', 'id','slug' ])
            ->orderBy('updated_at', 'desc')->get();
            $response['records'] = $records;
            return $response;
  }

  public function getAllNotifications($notifi_id)
  {
      $response['status'] = 1;
      $response['message'] = '';
           $date = date('Y-m-d');
     $records = DB::table('notifications')
                ->where('notifiable_id', $notifi_id)
                ->where('read_at',NULL)
                ->orderBy('updated_at', 'desc')
                ->get();
            $response['records'] = $records;
            return $response;
  }
  public function getIndNotifications($id)
  {
      $response['status'] = 1;
      $response['message'] = '';
           $date = date('Y-m-d');
     $records = DB::table('notifications')
                ->where('id', $id)
                ->first();
            $response['records'] = $records;
            return $response;
  }

  public function deleteNotifications($id){
    /**
       * Delete the questions associated with this quiz first
       * Delete the quiz
       * @var [type]
       */
        $record = DB::table('notifications')
                  ->where('id', $id)
                  ->delete();
        /*if(!env('DEMO_MODE')) {
            $record->delete();
        }*/

        $response['status'] = 1;
        $response['message'] = 'Notifications_deleted_successfully';
        return $response;
  }

  public function updateReadNotification($id)
  {
      $response['status'] = 1;
      $response['message'] = '';

     $records = DB::table('notifications')
                ->where('id', $id)
                ->update(['read_at'=>date('Y-m-d')]);

            $response['records'] = $records;
            return $response;
  }

  public function deleteAllNotifications($notifiable_id){
    /**
       * Delete the questions associated with this quiz first
       * Delete the quiz
       * @var [type]
       */
        $record = DB::table('notifications')
                  ->where('notifiable_id', $notifiable_id)
                  ->delete();
    /*    if(!env('DEMO_MODE')) {
            $record->delete();
        }*/

        $response['status'] = 1;
        $response['message'] = 'Notifications_deleted_successfully';
        return $response;
  }


  public function validateCoupon(Request $request)
    {     
      $coupon_code        = $request->coupon_code;
        $user             = getUserRecord($request->student_id);
      $item_type          = $request->item_type;
      $purchased_amount   = $request->cost;

      $item         = null;
      if($item_type=='combo')
      {
        $item = \App\ExamSeries::where('slug', '=', $request->item_name)->first();
      }
        else if($item_type=='exam'){
            $item = \App\Quiz::where('slug', '=', $request->item_name)->first();
        }
        else if($item_type=='lms'){
            $item = \App\LmsSeries::where('slug', '=', $request->item_name)->first();
        }

      if(!$item){

         $response['status']  = 0;
             $response['message'] = 'invalid item';
             return $response; 
        // return json_encode(array('status'=>'0', 'message'=>'invalid item'));
      }
      

        $coupon_data = \App\Couponcode::where('coupon_code', '=', $coupon_code)->first();
        if($coupon_data){

            if($coupon_data->institute_id != $user->institute_id){
                 
                 $response['status']  = 0;
                 $response['message'] = 'You are not eligible to use this coupon code';
                 return $response; 
              
                // return json_encode(array('status'=>'0', 'message'=>'You are not eligible to use this coupon code'));
            }
        }

      $couponObject     = new \App\Couponcode();
      $couponRecord     = $couponObject->checkValidity($coupon_code, $item_type);
      $status       = 0;
      $discount_availed   = 0;
      $message      = 'invalid';
      $amount_to_pay    = $purchased_amount;
      $coupon_id      = 0;

      if($couponRecord)
      {
            if($this->checkCouponUsage($couponRecord, $item, $item_type, $user))
        {
          //Coupon is valid
          //Limit is not reached so do the follwoing operations
          // 1) Check the minimum amount critria
          // 2) Calculate discount eligiblity
          // 3) Return the status message to user
          if($purchased_amount>=$couponRecord->minimum_bill){
            $discount_amount = $this->calculateDiscount($couponRecord, $purchased_amount);
            $amount_to_pay = $purchased_amount - $discount_amount;
            $discount_availed = $discount_amount;
            if($amount_to_pay<0)
              $amount_to_pay = 0;
            $message  = 'hey_you_are_eligible_for_discount';
            $status   = 1;
            $coupon_id  = $couponRecord->id;
          }
          else {
            $message = 'minimum_bill_not_reached. this_is_valid_for_minimum_purchase_of'.' '.getCurrencyCode().$couponRecord->minimum_bill;
          }

        }
        else {
          $message = 'limit_reached';
        }

      }
      else
      {
        $message = 'invalid_coupon';
      }

               

               $response['status']        = $status;
                 $response['message']       = $message;
                 $response['amount_to_pay'] = $amount_to_pay;
                 $response['discount']      = $discount_availed;
                 $response['coupon_id']     = $coupon_id;
                 return $response; 

      

       
    }



      public function checkCouponUsage($couponRecord, $item, $item_type, $user)
    {

        $recs = DB::table('couponcodes_usage')
                        ->where('user_id','=',$user->id)
                        ->where('coupon_id', '=', $couponRecord->id)
                        ->get();
      
          $count = count($recs);
      if($count >= $couponRecord->usage_limit)
        return FALSE;
      return TRUE;
    }


     /**
     * This method calculates the eligible discount for using for this coupon
     * @param  [type] $couponRecord     [description]
     * @param  [type] $purchased_amount [description]
     * @return [type]                   [description]
     */
    public function calculateDiscount($couponRecord, $purchased_amount)
    {
      $discount_amount = 0;
      if($couponRecord->discount_type == 'percent')
      {
        $actual_discount = ($purchased_amount * ($couponRecord->discount_value / 100));
        if($actual_discount > $couponRecord->discount_maximum_amount)
        {
          $actual_discount = $couponRecord->discount_maximum_amount;
        }
        $discount_amount = $actual_discount;
        if($discount_amount<0)
          $discount_amount = 0;
      }
      else {
        $discount_amount = $couponRecord->discount_value;
      }

      return $discount_amount;
    }




     /**
     * This method will return the list of available categories
     *
     * @param      \Illuminate\Http\Request  $request  The request
     *
     * @return     <type>                    The exam categories.
     */
    public function getExamCategories(Request $request)
    { 
        $student  = getStudentRecord($request->user_id);

        $academic_id        = $student->academic_id;
        $course_parent_id   = $student->course_parent_id;
        $course_id          = $student->course_id;
        $year               = $student->current_year;
        $semister           = $student->current_semister;
        $user_id            = $request->user_id;
       
        $categories = \App\QuizResult::join('quizzes', 'quizzes.id', '=', 'quizresults.quiz_id')
        ->join('quizcategories', 'quizzes.category_id','=','quizcategories.id')
        ->where('quizresults.year', '=',$year)
        ->where('quizresults.semister', '=',$semister)
        ->where('quizresults.user_id', '=',$user_id)
        ->where('quizresults.academic_id', '=',$academic_id)
        ->where('quizresults.course_id', '=',$course_id)
        ->where('quizzes.applicable_to_specific', '=',1)

        ->select(['category', 'quizcategories.id as id', 'quizcategories.description as description'])
        ->groupBy('quizcategories.id')
        ->get();
       
        if(count($categories) > 0){
            
           //$student_cats = array();
            
           foreach ($categories as $category) {
              

              /*$temp  = [];
              $exams = [];
              $temp['categories']    = $category->category;
              $temp['id']            = $category->id;
              $temp['description']   = $category->description;

              $student_cats[]        = $temp; */

              /*$quizzes = \App\QuizResult::join('quizzes', 'quizzes.id', '=', 'quizresults.quiz_id')
                                        ->join('quizcategories', 'quizzes.category_id','=','quizcategories.id')
                                        ->where('quizresults.year', '=',$year)
                                        ->where('quizresults.semister', '=',$semister)
                                        ->where('quizresults.academic_id', '=',$academic_id)
                                        ->where('quizresults.course_id', '=',$course_id)
                                        ->where('quizzes.category_id', '=',$category->id)
                                        ->where('quizresults.user_id', '=', $user_id)
                                        ->select(['category', 'quizcategories.id as id', 
                                                    'quizcategories.description as description', 
                                                    'quizzes.id as quiz_id', 'quizzes.title',
                                                    'quizresults.marks_obtained','quizresults.total_marks',
                                                    'quizresults.percentage','quizresults.exam_status',
                                                    'quizresults.slug as result_slug','quizresults.updated_at',
                                                    'quizzes.slug as quiz_slug'
                                                    ])
                                        ->get();*/

                $quizzes = \App\QuizResult::join('quizzes', 'quizzes.id', '=', 'quizresults.quiz_id')
                                        ->join('quizcategories', 'quizzes.category_id','=','quizcategories.id')
                                        ->where('quizresults.year', '=',$year)
                                        ->where('quizresults.semister', '=',$semister)
                                        ->where('quizresults.academic_id', '=',$academic_id)
                                        ->where('quizresults.course_id', '=',$course_id)
                                        ->where('quizzes.category_id', '=',$category->id)
                                        ->where('quizresults.user_id', '=', $user_id)
                                        ->select([ 'quizzes.id as quiz_id', 'quizzes.title',
                                                    'quizresults.marks_obtained','quizresults.total_marks',
                                                    'quizresults.percentage','quizresults.exam_status',
                                                    'quizresults.slug as result_slug','quizresults.updated_at',
                                                    'quizzes.slug as quiz_slug'
                                                    ])
                                        ->get();                        

              $category->quizzes =  $quizzes;                       

             /* foreach ($quizzes as $quiz) {
                  $exams[] = $quiz;
              }   
              $student_cats[]       = $quizzes;*/ 


           }

            $response['status']  = 1;
            $response['categories']  = $categories;//$student_cats;

        } else {
              
              $response['status']  = 0;
              $response['messages']  = 'No Categories Avaialble';
        } 

        return $response;
    }


   public function getExamsByCategory(Request $request)
    {
        $student  = getStudentRecord($request->user_id);
        
        $academic_id        = $student->academic_id;
        $course_parent_id   = $student->course_parent_id;
        $course_id          = $student->course_id;
        $year               = $student->current_year;
        $semister           = $student->current_semister;
        $category_id        = $request->category_id;
        $user_id            = $request->user_id;

        $quizzes = \App\QuizResult::join('quizzes', 'quizzes.id', '=', 'quizresults.quiz_id')
        ->join('quizcategories', 'quizzes.category_id','=','quizcategories.id')
        ->where('quizresults.year', '=',$year)
        ->where('quizresults.semister', '=',$semister)
        ->where('quizresults.academic_id', '=',$academic_id)
        ->where('quizresults.course_id', '=',$course_id)
        ->where('quizzes.category_id', '=',$category_id)
        ->where('quizresults.user_id', '=', $user_id)
        ->select(['category', 'quizcategories.id as id', 
                    'quizcategories.description as description', 
                    'quizzes.id as quiz_id', 'quizzes.title',
                    'quizresults.marks_obtained','quizresults.total_marks',
                    'quizresults.percentage','quizresults.exam_status',
                    'quizresults.slug as result_slug','quizresults.updated_at',
                    'quizzes.slug as quiz_slug'
                    ])
        ->get();
        
         if(count($quizzes) > 0){

            $response['status']  = 1;
            $response['quizzes']  = $quizzes;
        }else{
              
               $response['status']  = 0;
               $response['messages']  = 'No exams Avaialble';
        } 

        return $response;
    }


    public function getStudentSubjects(Request $request)
    {
        $student  = getStudentRecord($request->user_id);
        
        $course_parent_id   = $student->course_parent_id;
        $course_id          = $student->course_id;
        $year               = $student->current_year;
        $semister           = $student->current_semister;

        $current_academic_id = getDefaultAcademicId();
        $subjects            = \App\CourseSubject::join('subjects', 'subjects.id','=','course_subject.subject_id')
                                ->join('courses','courses.id','=','course_subject.course_id')
                                ->where('academic_id','=',$current_academic_id)
                                ->where('course_parent_id','=',$course_parent_id)
                                ->where('course_id','=',$course_id)
                                ->where('year','=',$year)
                                ->where('semister','=',$semister)
                                ->select(['course_subject.id as id','course_subject.slug as slug', 'subject_title', 'course_title', 'year',
                                 'semister', 'subject_id','staff_id','course_dueration',
                                 'subjects.is_lab', 'subjects.is_elective_type'	, 'subjects.subject_code',
                                 'subjects.maximum_marks', 'subjects.pass_marks', 'subjects.internal_marks','subjects.external_marks',

                                 ])
                                // ->get();
                                ->paginate(getRecordsPerPage());
          $all_subjects = []; 
          $lessionPlanObject = new \App\LessionPlan();
          // foreach ($subjects as $subject) 
          // {
          //   $temp =[];
          //   $temp['id']               = $subject->id;    
          //   $temp['slug']             = $subject->slug;    
          //   $temp['subject_title']    = $subject->subject_title;    
          //   $temp['course_title']     = $subject->course_title;    
          //   $temp['year']             = $subject->year;    
          //   $temp['semister']         = $subject->semister;    
          //   $temp['subject_id']       = $subject->subject_id;    
          //   $temp['staff_id']         = $subject->staff_id;    
          //   $temp['course_dueration'] = $subject->course_dueration; 
          //   $temp['subject_code'] = $subject->subject_code; 
          //   $temp['is_lab'] = $subject->is_lab; 
          //   $temp['is_elective_type'] = $subject->is_elective_type; 
          //   $temp['internal_marks'] = $subject->internal_marks; 
          //   $temp['external_marks'] = $subject->external_marks; 
          //   $temp['pass_marks'] = $subject->pass_marks; 
          //   $temp['maximum_marks'] = $subject->maximum_marks; 
          //   $summary                  = $lessionPlanObject->getSubjectCompletedStatus($subject->subject_id, $subject->staff_id, $subject->id);
          //   $temp['completed_percentage']  = round($summary->percent_completed);
          //   $all_subjects[] = $temp;


          // }   
          foreach ($subjects as $i => $subject) 
          { 
            $summary = $lessionPlanObject->getSubjectCompletedStatus($subject->subject_id, $subject->staff_id, $subject->id);
            $subjects[$i]->completed_percentage  = round($summary->percent_completed);
          }                    
        
         if(count($subjects) > 0){

            $response['status']  = 1;
            $response['subjects']  = $subjects;
        }else{
              
               $response['status']  = 0;
               $response['messages']  = 'No subjects Available';
        } 

        return $response;
    }


    public function getSubjectTopics($courseSubjectSlug)
    {
       

      $courseSubjectRecord = \App\CourseSubject::where('slug','=',$courseSubjectSlug)->first();


      $courseRecord        = \App\Course::where('id','=',$courseSubjectRecord->course_id)->first();
      $subjectRecord       = \App\Subject::where('id','=', $courseSubjectRecord->subject_id)->first();
      $available_records   = \App\LessionPlan::where('course_subject_id', '=', $courseSubjectRecord->id)->get();
      $topics              = $this->prepareTopicsList($courseSubjectRecord->subject_id, $courseSubjectRecord->id);
      

      if(count($topics) == 0 ){

          $response['status']  = 0;
          $response['message'] = 'No topics available';
          return $response;
      }  

      if($subjectRecord){

         
         //get staff details who currently teaching this subject
         if ($courseSubjectRecord->staff_id) {
            $staff = getUserRecord($courseSubjectRecord->staff_id);

            if ($staff) {
              $subjectRecord->staff_id  = $courseSubjectRecord->staff_id;
              $subjectRecord->name      = $staff->name;
              $subjectRecord->image     = $staff->image;
            }

            //get subject completed percentage
            $lessionPlanObject = new \App\LessionPlan();
            $summary = $lessionPlanObject->getSubjectCompletedStatus($courseSubjectRecord->subject_id, $courseSubjectRecord->staff_id, $courseSubjectRecord->id);

            $subjectRecord->completed_percentage  = round($summary->percent_completed);

         } else {

            $subjectRecord->staff_id  =0;
            $subjectRecord->name      ="";
            $subjectRecord->image     ="";
            $subjectRecord->completed_percentage=0;
            
         }

         $response['subjectRecord']  = $subjectRecord;
      }
      if(count($available_records)){
         
          $response['available_records']  = $available_records;
      }

      if(count($topics)){

          $response['topics']  = $topics;
      }

      return $response;     
    }


     /**
    * This method prepares  the list of topics and child topics and returns an array
    * @param  [type] $subject_id      [description]
    * @param  [type] $courseSubjectId [description]
    * @return [type]                  [description]
    */
   public function prepareTopicsList($subject_id, $courseSubjectId)
   {
      $parent_topics = $this->getTopicRecord($subject_id, 0,0);


     /* $response['status']  = 0;
      $response['message'] = 'TESTING';
      $response['parent_topics'] = $parent_topics;

      return $response;*/



      /*$topics = [];
      foreach($parent_topics as $topic)
      {

        $topics[$topic->id] = $topic;
        $topics[$topic->id]['course_subject_id'] = $courseSubjectId;

        $subject_topics_list = \App\Topic::where('parent_id','=',$topic->id)->get()->toArray();
        $lession_plan_topics = \App\LessionPlan::where('course_subject_id','=',$courseSubjectId)
                                                ->get()->toArray();
        $topics[$topic->id]['childs'] = $this->prepareChildRecords($subject_topics_list, $lession_plan_topics);
      }*/
     
      // return $topics;


      $total_topics=[];
      foreach($parent_topics as $key=>$topic)
      { 
          $topics = [];
          $topics = $topic;
          $topics['course_subject_id'] = $courseSubjectId;

          $subject_topics_list = \App\Topic::where('parent_id','=',$topic->id)->get()->toArray();
          $lession_plan_topics = \App\LessionPlan::where('course_subject_id','=',$courseSubjectId)
                                                  ->get()->toArray();
          $topics['childs'] = $this->prepareChildRecords($subject_topics_list, $lession_plan_topics);

          array_push($total_topics,$topics);
      }

      return $total_topics;

   }


   public function prepareChildRecords($topics_list, $lession_plan_list)
   {

       //parent topic is completed or not - calculate based on child topics complted
       
       

       $final_records = [];
       $child_topics  = [];

       foreach($topics_list as $topic)
       {
          
          $topic = (object)$topic;
          $child_topics['id']         = $topic->id;
          $child_topics['topic_name'] = $topic->topic_name;
          $completed_status           = 0;
          $completed_date             = '';


          /*$parnt_tpc_cmpltd_status = 0;
          $parnt_tpc_cmplted = array();*/

          foreach($lession_plan_list as $plan_record)
          {

              $plan_record = (object)$plan_record;
              if($topic->id == $plan_record->topic_id)
              {
                $completed_status = ($plan_record->is_completed) ? 1 : 0;
                $completed_date = $plan_record->completed_on;
              }

              // array_push($parnt_tpc_cmplted, $completed_status);
          }

          $child_topics['is_completed'] = $completed_status;
          $child_topics['completed_on'] = $completed_date;

         /* if (in_array(0, $parnt_tpc_cmplted)) 
            $parnt_tpc_cmpltd_status = 0;
          else
            $parnt_tpc_cmpltd_status = 1;*/


          $final_records[] = $child_topics;

         
       }

       return $final_records;
   }


    /**
    * This method returns the specific topics based on the condition
    *
    * @param      <type>   $subject_id  The subject identifier
    * @param      integer  $parent_id   The parent identifier
    *
    * @return     <type>   The topic record.
    */
   public function getTopicRecord($subject_id, $parent_id = 0, $courseSubjectId = 0 )
   {
 
      $result = \App\Topic::join('subjects', 'subjects.id','=','topics.subject_id')
                           ->leftJoin('lessionplans','topic_id','=','topics.id');
   

      $result = $result->where('subjects.id', '=',$subject_id)
                        ->where('topics.parent_id', '=', $parent_id)
                        ->select(['topics.id as id', 'topic_name','lessionplans.is_completed','completed_on'])
                        ->groupBy(['topics.id'])->get();
                             
      return $result;
  }




    public function getAttendance(Request $request)
    {  
        $student  = getStudentRecord($request->user_id);
        
        $academic_id        = $student->academic_id;
        $course_parent_id   = $student->course_parent_id;
        $course_id          = $student->course_id;
        $year               = $student->current_year;
        $semister           = $student->current_semister;
        $student_id         = $student->id;

        $attendance         = \App\StudentAttendance::join('subjects', 'subjects.id', '=', 'studentattendance.subject_id')
                                                   ->select(['studentattendance.id','student_id','attendance_date','attendance_code','academic_id','course_parent_id','course_id','year','subject_title', 'total_class','studentattendance.record_updated_by as staff_id','subjects.id as subject_id'])

                                                    ->where('student_id',   '=', $student_id)
                                                    ->where('academic_id',  '=', $academic_id)
                                                    ->where('course_id',    '=', $course_id)
                                                    ->where('year',         '=', $year)
                                                    ->where('semester',     '=', $semister)
                                                    ->get();

        $sum = \App\StudentAttendance::where('student_id',   '=', $student_id)
                                    ->where('academic_id',  '=', $academic_id)
                                    ->where('course_id',    '=', $course_id)
                                    ->where('year',         '=', $year)
                                    ->where('semester',     '=', $semister)
                                    ->sum('total_class');


        if (count($attendance) > 0 ) {

            $response['attendance']  = $attendance;
            $response['sum']  = $sum;

        } else {

            $response['status']   = 0;
            $response['message']   = " No data available ";
        }

        return $response;
    }



    public function getStudentAttendance(Request $request)
    {  

        $student = getStudentRecord($request->user_id);

        $attnd_date = $request->date;
        //get year and month from date
        $attnd_year = date('Y', strtotime($attnd_date));
        $attnd_month = date('m', strtotime($attnd_date));


        $academic_id        = $student->academic_id;
        $course_parent_id   = $student->course_parent_id;
        $course_id          = $student->course_id;
        $year               = $student->current_year;
        $semister           = $student->current_semister;
        $student_id         = $student->id;

        $attendance  = \App\StudentAttendance::join('subjects', 'subjects.id', '=', 'studentattendance.subject_id')
                        ->select(['studentattendance.id','student_id','attendance_date','attendance_code','academic_id','course_parent_id','course_id','year','subject_title', 'total_class','studentattendance.record_updated_by as staff_id','subjects.id as subject_id'])
                        ->where('student_id',   '=', $student_id)
                        // ->where('academic_id',  '=', $academic_id)
                        // ->where('course_id',    '=', $course_id)
                        // ->where('year',         '=', $year)
                        // ->where('semester',     '=', $semister)
                        ->whereYear('attendance_date', '=', $attnd_year)
                        ->whereMonth('attendance_date', '=', $attnd_month)
                        ->get();


         /* $response['status']       = 1;
          $response['attendance']   = $attendance;
          return $response;*/

       /*$sum = \App\StudentAttendance::where('student_id',   '=', $student_id)
                                    ->where('academic_id',  '=', $academic_id)
                                    ->where('course_id',    '=', $course_id)
                                    ->where('year',         '=', $year)
                                    ->where('semester',     '=', $semister)
                                    ->sum('total_class');*/


        if (count($attendance) > 0 ) {

           /* $response['status']       = 1;
            $response['attendance']   = $attendance;
            $response['sum']          = $sum;
        
            return $response;*/

            $total_classes  = 0;
            $present        = 0;
            $absent         = 0;
            $leave          = 0;



            foreach ($attendance as $att) {

              $total_classes += $att->total_class;

                if ($att->attendance_code=='P')
                {
                    $present += $att->total_class;
                } 
                elseif ($att->attendance_code=='A')
                {
                    $absent += $att->total_class;
                }
                else if($att->attendance_code=='L')
                {
                    $leave += $att->total_class;
                }
            }
            
            $data = array('total_classes' => $total_classes,
                          'present' => $present,
                          'absent'  => $absent,
                          'leave'   => $leave
                          );


            $response['status']       = 1;
            $response['data']         = $data;
            $response['message']      = "Attendance summary";
            
            return $response;

        } else {

            $response['status']   = 0;
            $response['message']   = " No data available ";
        }

        return $response;
    }



    public function getOfflineExamsCategories($user_id='')
    {
        
      if (!$user_id) {
          $response['status']  = 0;
          $response['message'] = 'Invalid request';
          return $response;
      }

      $user     = getUserRecord($user_id);
      $student  = getStudentRecord($user->id);

      $academic_id        = $student->academic_id;
      $course_parent_id   = $student->course_parent_id;
      $course_id          = $student->course_id;
      $year               = $student->current_year;
      $semister           = $student->current_semister;
      $student_id         = $student->id;


      $records = \App\OfflineQuizCategories::
                          join('quizzes', 'quizzes.offline_quiz_category_id', '=', 'quizofflinecategories.id')
                          ->join('quizapplicability', 'quizapplicability.quiz_id', '=', 'quizzes.id')
                          ->where('quizapplicability.academic_id', '=', $academic_id)
                          ->where('quizapplicability.course_id', '=', $course_id)
                          ->where('quizapplicability.year', '=', $year)
                          ->where('quizapplicability.semister', '=', $semister)
                          ->select(['quizofflinecategories.id', 'quizofflinecategories.title'])
                          ->groupBy('quizofflinecategories.id')
                          ->get();

      if (count($records)) {

         $categories = [];

         foreach ($records as $record) {
            $temp = (object)[];

            $temp->id     = $record->id;
            $temp->title  = $record->title;

            $categories[] = $temp;
         }


        $response['status']   = 1;
        $response['data']     = $categories;
        $response['message']  = 'categories list';

      }  else {

          $response['status']  = 0;
          $response['message'] = 'No categories available';
      }

      return $response;
    }


    public function getClassMarks(Request $request)
    {

       $user_id = $request->user_id;
       $quizofflinecategory_id = $request->category_id;

       if (!$user_id || !$quizofflinecategory_id) {

          $response['status']  = 0;
          $response['message'] = 'Invalid request';
          return $response;
       }



      $user     = getUserRecord($user_id);
      $student  = getStudentRecord($user->id);


      $academic_id        = $student->academic_id;
      $course_parent_id   = $student->course_parent_id;
      $course_id          = $student->course_id;
      $year               = $student->current_year;
      $semister           = $student->current_semister;
      $student_id         = $student->id;





       $offline_quiz_category_id = $quizofflinecategory_id;

       $course_record         = \App\Course::where('id','=',$course_id)->first();

       $academic_record       = \App\Academic::where('id','=',$academic_id)->first();

       $offline_quiz_category = \App\OfflineQuizCategories::where('id', '=', $offline_quiz_category_id)->first();    


     /* $response['status']  = 1;
      $response['data']    = $offline_quiz_category;
      $response['message'] = 'TEST OFFLINE QUIZ CATEGORY';*/
      // return $response;  


       /*$quiz_details          = \App\Quiz::where('offline_quiz_category_id','=',$offline_quiz_category_id)->get();*/


       $quiz_details = \App\Quiz::join('quizapplicability','quizapplicability.quiz_id','=','quizzes.id')
        ->join('subjects','subjects.id','=','quizzes.subject_id')
        ->join('academics','academics.id','=','quizapplicability.academic_id')
        ->join('courses','courses.id','=','quizapplicability.course_id')
        ->join('quizofflinecategories','quizzes.offline_quiz_category_id','=','quizofflinecategories.id')
        ->select(['quizzes.title','quizofflinecategories.title as quiz_offline_category','subjects.subject_title','quizzes.total_marks','quizzes.pass_percentage', 'quizzes.type',  'quizzes.id','quizzes.slug','start_date','end_date' ])

        ->where('quizzes.type','!=','online')
        ->where('quizapplicability.academic_id','=',$academic_id)
        ->where('quizzes.offline_quiz_category_id','=',$offline_quiz_category_id)
        ->get();

      
     /* $response['quiz_details']    = $quiz_details;
      $response['message'] = 'TEST QUIZ DEAILS';
      return $response;*/



       $title                 = $academic_record->academic_year_title.' '.$course_record->course_title;

       if ($course_record->course_dueration>1)
       {
          $title .= ' Year-'.$year;
          if ($course_record->is_having_semister && $semister>0)
            $title .= ' Sem-'.$semister;
       }


       if ($offline_quiz_category)
       $title .= ' '.$offline_quiz_category->title.' class marks';
      

      $subjects = \App\QuizApplicability::join('quizzes', 'quizapplicability.quiz_id', '=', 'quizzes.id')
                          ->join('subjects', 'subjects.id', '=', 'quizzes.subject_id')
                          ->where('quizapplicability.academic_id', '=', $academic_id)
                          ->where('quizapplicability.course_id', '=', $course_id)
                          ->where('quizapplicability.year', '=', $year)
                          ->where('quizapplicability.semister', '=', $semister)
                          ->where('quizzes.offline_quiz_category_id', '=', $offline_quiz_category_id)
                          ->select(['quizzes.id as quiz_id', 'quizzes.title','subject_id', 'subject_title','subject_code','quizzes.offline_quiz_category_id','total_marks'])
                          ->get();

    /*  $response['status']  = 1;
      $response['data']    = $subjects;
      $response['message'] = 'TEST';
      return $response;*/

      $students_list = [];
      $final_list = [];

      foreach ($quiz_details as $quiz_datail) {

        $students = \App\Student::join('users','users.id','=','students.user_id')
                                ->join('quizresults','quizresults.user_id','=', 'users.id')

                            ->where('quizresults.quiz_id', '=', $quiz_datail->id)
                            ->where('quizresults.academic_id', '=', $academic_id)
                            ->where('quizresults.course_id', '=', $course_id)
                            ->where('quizresults.year', '=', $year)
                            ->where('quizresults.semister', '=', $semister)
                            ->select(['users.id as user_id','roll_no','name','image','students.id as student_id','users.slug as user_slug'])
                            ->get();
          //}
         

          foreach ($students as $student)
          {
              $temp = [];
              $temp['user_id'] = $student->user_id;
              $temp['name']    = $student->name;
              $temp['roll_no'] = $student->roll_no;
              $temp['image']   = $student->image;
              $temp['slug']    = $student->user_slug;
              $subject_marks   = [];
              $average = 0;

              foreach ($subjects as $subject)
              {
                  $marks_records = $this->getSubjectMarks($student->user_id,$offline_quiz_category_id, $subject->subject_id);
                  
                  $subject_marks['subject_id']    = $subject->subject_id;
                  $subject_marks['subject_title'] = $subject->subject_title;
                  $subject_marks['subject_code']  = $subject->subject_code;

                  if ($marks_records) {
                    $subject_marks['marks_obtained'] = $marks_records->marks_obtained;
                    $subject_marks['total_marks']   = $marks_records->total_marks;
                    $subject_marks['exam_status']   = $marks_records->exam_status;
                  }

                  // $subject_marks ['score']        = isset($marks_records) ? $marks_records : null;


                  $subject_marks ['percentage']   = isset($marks_records->percentage) ? $marks_records->percentage: 0;
                  $average                        = $average+$subject_marks ['percentage'];
                  $temp['marks'][]                = $subject_marks;
              }

              $temp['average']    = 0;
              $total_subjects     = count($subjects);

              if($total_subjects)
              $temp['average']   = round($average/count($subjects));
              
              $students_list[] = $temp;
          }

        }

          $final_list['students']     = $students_list;
          $final_list['subjects']     = $subjects;
          $final_list['course_title'] = $title;


          $response['status']  = 1;
          $response['data']    = $final_list;
          $response['message'] = 'Class Marks';

          return $response;
    }


    public function getSubjectMarks($user_id, $quiz_offline_category_id, $subject_id)
    {
        $records = \App\Quiz::join('quizresults','quizzes.id','=','quizresults.quiz_id')
                            ->where('quizzes.offline_quiz_category_id','=',$quiz_offline_category_id)
                            ->where('quizzes.subject_id','=',$subject_id)
                            ->where('quizresults.user_id','=',$user_id)
                            ->select(['user_id','quizresults.marks_obtained','subject_id', 'quizresults.total_marks','quizresults.percentage', 'quizresults.exam_status'])->first();
        return $records;
    }

    public function getAllSubjects()
    {
        return  Subject::where('status','=','Active')->select(['id','subject_title','subject_code','slug'])->get();
         
    }



  public function getNotificationCount($notifi_id)
  {
     $records = DB::table('notifications')
                ->where('notifiable_id', $notifi_id)
                ->where('read_at',NULL)
                ->orderBy('updated_at', 'desc')
                ->get();

      $data['notication_count'] = count($records);

      $response['records'] = $data;
      return $response;
  }
  
  public function getTodayHostalMeals() {

      $date = date('Y-m-d');
      $records = array();
      $records =  DB::table('mess_meals')
              ->leftJoin('mess', 'mess.id', 'mess_meals.mess_id')
              ->leftJoin('mess_meals_time', 'mess_meals_time.id', 'mess_meals.mess_meals_time_id')
              ->select(['mess_meals.mess_id', 'mess.name as mess_name',  'mess_meals.mess_meals_time_id', 
              'mess_meals_time.time as mess_time', 'mess_meals.meals_name', 'mess_meals.meals_price', 'mess_meals.date'])
              ->where('mess_meals.date', '=', $date)
              ->orderBy('mess_meals.updated_at', 'desc')
              ->paginate(getRecordsPerPage());

      if(!$records) {
        $response = array(
          'status' => 0,
          'message' => 'No Records available'
        );
      } else {
        $response = array(
          'status' => 1,
          'data' => $records
        );
      }
      return $response;
  }

}