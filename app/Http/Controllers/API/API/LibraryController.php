<?php

namespace App\Http\Controllers\API;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use DB;



class LibraryController extends Controller
{
    /*
     * records as library history
     * @param  integer $user_id [description]
     * @param  integer $limit   [description]
     * @return [type]           [description]
     */
    public static function issueHistory($user_id)
    {
      
      
      $records =  \App\LibraryIssue::join('librarymasters', 'librarymasters.id', '=', 'libraryissues.master_asset_id')
                          ->where('user_id','=',$user_id)
                          ->select(['title','library_asset_no','issued_on','issue_type','libraryissues.updated_at','libraryissues.id','due_date','return_on'])
                          ->orderBy('updated_at','desc')
                          // ->limit($limit)
                          // ->get();
                          ->paginate(getRecordsPerPage());

        if( count($records) > 0 ){
          
           $response['status']  = 1;
           $response['message'] = '';
           $response['data']    = $records;

        }else{
             
           $response['status']  = 0;
           $response['message'] = 'No records available';
        }

        return $response;
    }	 


    /*
     * records as library only returns history
     * @param  integer $user_id [description]
     * @param  integer $limit   [description]
     * @return [type]           [description]
     */
    public static function libraryReturns($user_id)
    {
      
      
      $records =  \App\LibraryIssue::join('librarymasters', 'librarymasters.id', '=', 'libraryissues.master_asset_id')
                          ->where('user_id','=',$user_id)
                          ->select(['title','library_asset_no','issued_on','issue_type','libraryissues.updated_at','libraryissues.id','due_date','return_on'])
                          ->where('issue_type','=','return')
                          ->orderBy('updated_at','desc')
                          // ->limit($limit)
                          // ->get();
                          ->paginate(getRecordsPerPage());

        if( count($records) > 0 ){
          
           $response['status']  = 1;
           $response['message'] = '';
           $response['data']    = $records;

        }else{
             
           $response['status']  = 0;
           $response['message'] = 'No records available';
        }

        return $response;
    }


    public function getBookDetails($issue_id)
    {
       
       $record =  \App\LibraryIssue::join('librarymasters', 'librarymasters.id', '=', 'libraryissues.master_asset_id')
                         ->join('authors','authors.id','=','librarymasters.author_id')
                         ->join('publishers','publishers.id','=','librarymasters.publisher_id')
                         ->join('libraryassettypes','libraryassettypes.id','=','librarymasters.asset_type_id')
                          ->where('libraryissues.id','=',$issue_id)
                          ->select(['title','library_asset_no','isbn','edition','issued_on','author as author_name','publisher as publisher_name','issue_type','due_date','libraryissues.updated_at','libraryissues.id','asset_type','image','libraryissues.return_on'])
                          ->first();

        if( $record){
          
           $response['status']  = 1;
           $response['message'] = '';
           $response['data']    = $record;

        }else{
             
           $response['status']  = 0;
           $response['message'] = 'No records available';
        }

        return $response;
    }

    public function getEbookLibDetails() {
      
      $record = DB::table('library_ebooks as leb')
                ->leftjoin('authors as au', 'leb.author_id', 'au.id')
                ->leftjoin('publishers as pu', 'leb.publisher_id', 'pu.id')
                ->select(['leb.id as ebook_id', 'leb.title', 'leb.author_id','au.author as author_name', 
                'leb.publisher_id','pu.publisher as publisher_name', 'leb.isbn_number', 'leb.edition', 'leb.description', 'leb.status',
                'leb.edition', 'leb.slug', 'leb.ebook', 'leb.created_at as create_date'])	
                ->orderby('leb.updated_at','desc')
                ->paginate(getRecordsPerPage());

        if($record) {
            $response = array(
               "status" => '1',
               "data" => $record
            );        
        } else {
            $response = array(
               "status" => '0',
               "message" => 'No records available',
               "data" => $record
            );
        }
        return $response;
    }


}


