<?php

namespace App\Http\Controllers\API;

use App\User;
use DB;
use Validator;
use App\LmsCategory;
use App\LmsContent;
use App\LmsSeries;
use App\LmsQuestionBank;
use App\LmsQuiz;
use App\LmsQuestionCategory;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

class LMSController extends Controller
{

    /**
     * List the categories available
     * @param  [type] $slug [description]
     * @return [type]       [description]
     */
    public function lmsIndCategories(Request $request)
    {

      $user_id = $request->user_id;
      $user =   $user = \App\User::where('id','=', $user_id)->first();

      $response['status'] = 0;
      $response['message'] = null;
      $response['records'] = null;
     $records = null;
    
          $category = \App\LmsCategory::getRecordWithId($request->id); 
          
          if($category){
             $records             = \App\LmsSeries::where('lms_category_id','=',$category->id)
                                        ->where('start_date','<=',date('Y-m-d'))
                                        ->where('end_date','>=',date('Y-m-d'))        
                                        ->get();

              $recs = [];
              
              foreach ($records  as $item) {
                $temp=[];
                
                $temp['id']          = $item->id;
                
                $temp['title']        = $item->title;
                $temp['slug']        = $item->slug;
                $temp['image']       = $item->image;
                $temp['description'] = $item->description;
                $temp['total_items'] = $item->total_items;

                $temp['is_paid']     = $item->is_paid;
                $temp['cost']     = $item->cost;
                if($item->is_paid == 1){
                   $temp['validity']     = $item->validity;
                }elseif($item->is_paid == 0){
                   $temp['validity']     = -1;

                }
                $temp['is_purchased'] = (int)\App\Payment::isItemPurchased($item->id, 'lms', $user->id);

                $temp['created_at']  = getViewFormat($item->created_at);
                $temp['updated_at']  = getViewFormat($item->updated_at);
                // $temp['items'] = $item->contents()->count();
                $recs[] = $temp;

              }

              $records = $recs;
                 $response['status'] = 1;
            }
            else{
              $response['message'] = 'Invalid category';
            }
        

     
      $response['records'] = $records;

      return $response;
    } 



       /**
     * This method displays all the details of selected exam series
     * @param  [type] $slug [description]
     * @return [type]       [description]
     */
    public function viewItem(Request $request, $slug, $content_slug='')
    { 
        
        $record = \App\LmsSeries::getRecordWithId($request->id); 
          $user_id = $request->user_id;
          $user = \App\User::where('id','=', $user_id)->first();
        $response['status'] = 0;
        $response['message'] = '';
        $response['contents'] = null;
        if(!$record)
        {
          $response['message'] = 'Invalid category';
          return $response;
        }

        $content_record = FALSE;
        if($content_slug) {
          $content_record = \App\LmsContent::getRecordWithSlug($content_slug);
          if(!$content_record)
          {
            $response['message'] = 'Invalid Content Record';
            return $response;
          }
        }
        

        if($content_record){
            if($record->is_paid) {
            if(!isItemPurchased( $record->id, 'lms'))
            {
                $response['message'] = 'This user has no permission to access';
                return $response;
            }
          }
        }

        

        $response['item']           = $record;
        $response['is_purchased']   = (int)\App\Payment::isItemPurchased($record->id, 'lms', $user->id);

        $response['list']           = $record->getContents();
        $response['content_record']     = $content_record;
        $response['status']=1;
        return $response;
    
    }

    public function lmsSeries(Request $request)
    {
      
      $user_id = $request->user_id;

      $user    = \App\User::where('id','=', $user_id)->first();


      $interested_categories  = null;
      $categories             = null;

      $response['status']  = 0;
      $response['message'] = null;
      $response['records'] = null;

       if($user->settings)
       {
         $interested_categories =  json_decode($user->settings)->user_preferences;
       }

        if($interested_categories){

         if( count( $interested_categories->lms_categories ) ){
           
                  $categories  = \App\LmsCategory::whereIn('id',(array) $interested_categories->lms_categories)
                                           ->pluck('id')
                                           ->toArray();

             }                                      
        }

     /*   print_r($interested_categories);
        exit;*/

        if ( count( $categories ) > 0 ) {
              
            $recs = \App\LmsSeries::where('start_date','<=',date('Y-m-d'))
                                ->where('end_date','>=',date('Y-m-d'))
                                ->whereIn('lms_category_id',$categories)->get();
            
            $records = [];

            foreach($recs as $r)
            {
              
              $total_items  = DB::table('lmsseries_data')
                              ->where('lmsseries_id',$r->id)
                              ->get()->count();

              $temp['title']              = $r->title;
              $temp['id']                 = $r->id;
              $temp['slug']               = $r->slug;
              $temp['category_id']        = $r->category_id;
              $temp['is_paid']            = $r->is_paid;
              $temp['cost']               = $r->cost;
              $temp['validity']           = $r->validity;
              $temp['total_exams']        = $r->total_exams;
              $temp['total_questions']    = $r->total_questions;
              $temp['image']              = $r->image;
              $temp['short_description']  = $r->short_description;
              $temp['start_date']         = $r->start_date;
              $temp['end_date']           = $r->end_date;
              $temp['total_items']        = $total_items;
              $temp['is_purchased']       = (int)\App\Payment::isItemPurchased($r->id, 'lms', $user->id);
              
              $records[] = $temp;
            }

            $response['status']  = 1;
            $response['records'] = $records;
            return $response;

         }
         else{

            $response['status']  = 0;
            $response['message'] = 'Please Update Your Settings';
            return $response;
         }
        
       

       
    }


    /**
   * This method will return the list of lms categories for this student
   * @param  Request $request [description]
   * @return [type]           [description]
   */
  public function lmsCategories(Request $request)
  {

    $user_id = $request->user_id;
    $user = \App\User::where('id','=', $user_id)->first();
    
    $interested_categories      = null;
    
    $response['status'] = 1;
    $response['categories'] = null;

         $interested_categories      = null;
        if($user->settings)
        {
          $interested_categories =  json_decode($user->settings)->user_preferences;
        }
        
        if($interested_categories)    {
         if(count($interested_categories->lms_categories))
        $response['categories']       = \App\LmsCategory::
                                      whereIn('id',(array) $interested_categories->lms_categories)
                                      ->get();
        // $response['categories']       = \App\LmsCategory::all();
        }

        
        if(!$response['categories'])
        {
          $response['status'] = 0;
          $response['message'] = 'No preferences selected';
        }
        return $response;
  }


  public function SeriesItems($slug)
  {
      $series = \App\LmsSeries::where('slug',$slug)->first();
      
      $items  = DB::table('lmsseries_data')
                ->where('lmsseries_id',$series->id)
                ->pluck('lmscontent_id')
                ->toArray();

      if(count($items) == 0 ){


           $response['status']  = 0;
          $response['message']  = 'No Data Available';

        return $response;
      }

      $contents   = \App\LmsContent::whereIn('id',$items)->get();

      $response['status']  = 1;
      $response['items']  = $contents;

      return $response;


  }

  public function viewCategoryItems($slug)
  {
     $response = array();
      $record = DB::table('lmscategories')
                    ->where('slug', $slug)
                    ->first(); 

      
      if(!$record) {
        $response = array(
          'status' => '0',
          'message' => 'No Data Available'
        );
        return $response; 
      }
        

      $records = DB::table('lmsseries')
                    ->where('lms_category_id','=',$record->id)
                    ->where('start_date','<=',date('Y-m-d'))
                    ->where('end_date','>=',date('Y-m-d'))        
                    ->paginate(getRecordsPerPage());

    
      $response = array(
        'status' => '1',
        'data' => $records
      );
      return $response; 
  }

  public function lmscategoriesadd(Request $request)
  {
    $record = new LmsCategory();
    $name   = $request->category;
      $value = array(
          'category'          =>$request->category,
          'slug'              =>$record->makeSlug($name,TRUE),
          'image'             =>$request->image,
          'description'       =>$request->description,
          'record_updated_by' =>$request->record_updated_by,
          'created_at'        =>date('Y-m-d H:i:s'),
          'updated_at'        =>date('Y-m-d H:i:s')
      );

      DB::table('lmscategories')
      ->insert($value);

      $response['status'] = 1;
      $response['message'] = "lmscategories added successfully";

      return $response;
  }
  public function getlmscategories(){

    // $category_id = $request->id;
    // print_r($category_id);

    $lmscategories = DB::table('lmscategories')
                      // ->where('id', $id)
                      ->get();
    
    $response['status'] = 1;                
    $response['data'] = $lmscategories;
    return $response;

  }

  public function getindilmscategory($id){

    $lmscategories = DB::table('lmscategories')
                      ->where('id', $id)
                      ->get();
    
    $response['status'] = 1;                
    $response['data'] = $lmscategories;
    return $response;

  }
  public function dellmscategory($id){
    $lmscategories = DB::table('lmscategories')
                    ->where('id', $id)
                    ->delete();

    $response['status'] = 1;
    $response['message'] = "category successfully deleted";
    return $response;
  }
  public function updatelmscategory(Request $request, $id){

      $record = new LmsCategory();
      $name   = $request->category;
      $value = array(
          'category'          =>$request->category,
          'slug'              =>$record->makeSlug($name,TRUE),
          'image'             =>$request->image,
          'description'       =>$request->description,
          'record_updated_by' =>$request->record_updated_by,
          'created_at'        =>date('Y-m-d H:i:s'),
          'updated_at'        =>date('Y-m-d H:i:s')
      );

      $lmscategories =  DB::table('lmscategories')
                          ->where('id', $id )
                          ->update($value);

      $response['status'] = 1;
      $response['message'] = "Lms category successfully updated";
      $response['data'] = $value;
      return $response;
  }

  public function addlmscontent(Request $request){
    $record = new LmsContent();
    $name = $request->title;
    $value = array(
        'title'             => $request->title,
        'slug'              => $record->makeSlug($name,TRUE),
        'code'              => $request->code,
        'image'             => $request->image,
        'subject_id'        => $request->subject_id,
        'content_type'      => $request->content_type,
        'is_url'            => $request->is_url,
        'file_path'         => $request->file_path,
        'description'       => $request->description,
        'record_updated_by' => $request->record_updated_by,
        'created_at'        => date('Y-m-d H:i:s'),
        'updated_at'        => date('Y-m-d H:i:s') 

    );
    $lmscontent = DB::table('lmscontents')
                  ->insert($value);

    $response['status'] = 1;
    $response['message'] = "Lms content successfully added";
    return $response;

  }

  public function getindilmscontent($id){
      $lmscontent = DB::table('lmscontents')
                  ->where('id', $id)
                  ->get();
      $response['status'] = 1;
      $response['data'] = $lmscontent;
      return $response;
  }

  public function getlmscontent(){
    $lmscontent = DB::table('lmscontents')
                ->get();

    $response['status'] = 1;
    $response['data'] = $lmscontent;
    return $response;
  }
  public function updatelmscontent(Request $request, $id){
    $record = new LmsContent();
    $name = $request->title;
    $value = array(
        'title'             => $request->title,
        'slug'              => $record->makeSlug($name,TRUE),
        'code'              => $request->code,
        'image'             => $request->image,
        'subject_id'        => $request->subject_id,
        'content_type'      => $request->content_type,
        'is_url'            => $request->is_url,
        'file_path'         => $request->file_path,
        'description'       => $request->description,
        'record_updated_by' => $request->record_updated_by,
        'created_at'        => date('Y-m-d H:i:s'),
        'updated_at'        => date('Y-m-d H:i:s') 

    );
    $lmscontent = DB::table('lmscontents')
                  ->where('id', $id)
                  ->update($value);

    $response['status'] = 1;
    $response['message'] = "LmsContent successfully updated";

    return $response;
  }
  public function delelmscontent($id){
    $lmscontents  = DB::table('lmscontents')
                  ->where('id', $id)
                  ->delete();

    $response['status'] = 1;
    $response['message'] = "lmscontent successfully deleted";
    return $response;
  }
  public function addlmsSeries(Request $request){
    $record = new LmsSeries();
    $name = $request->title;
    $value = array(
      "title"               => $request->title,
      "slug"                => $record->makeSlug($name,TRUE),
      "is_paid"             => $request->is_paid,
      "cost"                => $request->cost,
      "validity"            => $request->validity,
      "total_items"         => $request->total_items,
      "lms_category_id"     => $request->lms_category_id,
      "image"               => $request->image,
      "short_description"   => $request->short_description,
      "start_date"          => $request->start_date,
      "end_date"            => $request->end_date,
      "record_updated_by"   => $request->record_updated_by,
      "created_at"          => date('Y-m-d H:i:s'),
      "updated_at"          => date('Y-m-d H:i:s')
    );

    $addlmsseries = DB::table('lmsseries')
                    ->insert($value);

    $response['status'] = 1;
    $response['message'] = "Lms series added successfully";

    return $response;
  }
  public function getlmsseries(){
    $lmsSeries = DB::table('lmsseries')
                ->get();

    $response['data'] = $lmsSeries;
    return $response;
  }
  public function updatelmsSeries(Request $response, $id){
    $record = new LmsSeries();
    $name = $request->title;
    $value = array(
      "title"               => $request->title,
      "slug"                => $record->makeSlug($name,TRUE),
      "is_paid"             => $request->is_paid,
      "cost"                => $request->cost,
      "validity"            => $request->validity,
      "total_items"         => $request->total_items,
      "lms_category_id"     => $request->lms_category_id,
      "image"               => $request->image,
      "short_description"   => $request->short_description,
      "start_date"          => $request->start_date,
      "end_date"            => $request->end_date,
      "record_updated_by"   => $request->record_updated_by,
      "created_at"          => date('Y-m-d H:i:s'),
      "updated_at"          => date('Y-m-d H:i:s')
    );

    $lmsseries = DB::table('lmsseries')
              ->where('id', $id)
              ->update($value);

    $response['status'] = 1;
    $response['message']= "lmsseries updated successfully";
    $response['data']   = $lmsseries;

    return $response;
  }

  public function deletelmsseries($id){
    DB::table('lmsseries')
    ->where('id', $id)
    ->delete();

  $response['status']   = 1;
  $response['message']  = "LMS series deleted successfully";

  return $response;
  }

   // //// Questions\\\\ \\

  public function addQuestionsbank(Request $request){
    $record = new LmsQuestionBank();
    $name = $request->question_tags;
    $value = array(
      // 'subject_id'           => $request->subject_id,
      // 'topic_id'             => $request->topic_id,
      'category_id'          => $request->category_id,
      'question_tags'         => $request->question_tags,
      'slug'                 => $record->makeSlug($name,TRUE),
      'question_type'        => $request->question_type,
      'question'             => $request->question,
      'question_file'        => $request->question_file,
      'question_file_is_url' => $request->question_file_is_url,
      'answers'              => $request->answers,
      'total_correct_answers'=> $request->total_correct_answers,
      'correct_answers'      => $request->correct_answers,
      'marks'                => $request->marks,
      'time_to_spend'        => $request->time_to_spend,
      'difficulty_level'     => $request->difficulty_level,
      'hint'                 => $request->hint,
      'explanation'          => $request->explanation,
      'explanation_file'     => $request->explanation_file,
      'status'               => $request->status,
      'created_at'          => date('Y-m-d H:i:s'),
      'updated_at'           => date('Y-m-d H:i:s')
    );

    $questions = DB::table('lmsquestionbank')
                ->insert($value);

    $response['status'] = 1;
    $response['message'] = "QuestionBank added successfully";
    return $response;
  }

  public function getQuestionbank(){
    $questionbank = DB::table('lmsquestionbank')
                  ->get();
    $response['data'] = $questionbank;
    return $response;
  }

  public function deleteQuestionBank($id){
    $questionbank = DB::table('lmsquestionbank')
                  ->where('id', $id)
                  ->delete();

    $response['status'] = 1;
    $response['message'] = "QuestionBank deleted successfully";

    return $response;
  }

    public function updateQuestionbank(Request $request, $id){
    $record = new LmsQuestionBank();
    $name = $request->question_tags;
    $value = array(
      // 'subject_id'           => $request->subject_id,
      // 'topic_id'             => $request->topic_id,
      'category_id'          => $request->category_id,
      'question_tags'         => $request->question_tags,
      'slug'                 => $record->makeSlug($name,TRUE),
      'question_type'        => $request->question_type,
      'question'             => $request->question,
      'question_file'        => $request->question_file,
      'question_file_is_url' => $request->question_file_is_url,
      'answers'              => $request->answers,
      'total_correct_answers'=> $request->total_correct_answers,
      'correct_answers'      => $request->correct_answers,
      'marks'                => $request->marks,
      'time_to_spend'        => $request->time_to_spend,
      'difficulty_level'     => $request->difficulty_level,
      'hint'                 => $request->hint,
      'explanation'          => $request->explanation,
      'explanation_file'     => $request->explanation_file,
      'status'               => $request->status,
      'created_at'          => date('Y-m-d H:i:s'),
      'updated_at'           => date('Y-m-d H:i:s')
    );

    $questions = DB::table('lmsquestionbank')
                ->where('id', $id)
                ->update($value);

    $response['status'] = 1;
    $response['message'] = "QuestionBank updated successfully";
    return $response;
  }

  public function addLmsQuesCategory(Request $request){
    $record = new LmsQuestionCategory();
    $name = $request->category;
    $value = array(
      'category'            => $request->category,
      'slug'                => $record->makeSlug($name, TRUE),
      'description'        => $request->description,
      'record_updated_by'   => $request->record_updated_by,
      'created_at'          => date('Y-m-d H:i:s'),
      'updated_at'          => date('Y-m-d H:i:s')
    );

    $lmsquescategory = DB::table('lmsquestioncategories')
                      ->insert($value);

    $response['status'] = 1;
    $response['message'] = 'LmsQuestionCategory successfully added';
    return $response;
  }

  public function getLmsQuesCategory(){
    $lmsquescategory = DB::table('lmsquestioncategories')
                      ->get();

    $response['status'] = 1;
    $response['data']   = $lmsquescategory;
    return $response;
  }
  public function updateLmsQuesCategory(Request $request, $id){
    $record = new LmsQuestionCategory();
    $name = $request->category;
    $value = array(
      'category'            => $request->category,
      'slug'                => $record->makeSlug($name, TRUE),
      'description'        => $request->description,
      'record_updated_by'   => $request->record_updated_by,
      'created_at'          => date('Y-m-d H:i:s'),
      'updated_at'          => date('Y-m-d H:i:s')
    );
    $lmsquescategory = DB::table('lmsquestioncategories')
                      ->where('id', $id)
                      ->update($value);
    $response['status'] = 1;
    $response['message'] = "LmsQuestionCategory successfully updated";
    return $response;
  }
  public function delLmsQuesCategory($id){
    $lmsquescategory = DB::table('lmsquestioncategories')
                      -> where('id', $id)
                      ->delete();
    $response['status']=1;
    $response['message'] = 'LmsQuestionCategory successfully deleted';
    return $response;
  }

  ////// Quiz list\\\\\\

  public function addQuizlist(Request $request){

    $record = new LmsQuiz();
    $name = $request->title;
    $value = array(
      'title'                              => $request->title,
      'slug'                               => $record->makeSlug($name,TRUE),
      'dueration'                          => $request->dueration,
      'category_id'                        => $request->category_id,
      'series_id'                          => $request->series_id,
      'attempts'                           => $request->attempts,
      'total_marks'                        => $request->total_marks,
      'having_negative_mark'              => $request->having_negative_mark,
      'negative_mark'                     => $request->negative_mark,
      'pass_percentage'                    => $request->pass_percentage,
      'tags'                               => $request->tags,
      'publish_results_immediately'         => $request->publish_results_immediately,
      'description'                        => $request->description,
      'total_questions'                    => $request->total_questions,
      'instructions_page_id'                => $request->instructions_page_id,
      'record_updated_by'                  => $request->record_updated_by,
      'created_at'                         => date('Y-m-d H:i:s'),
      'updated_at'                         => date('Y-m-d H:i:s')
    );

    $quizlist = DB::table('lmsquizzes')
              ->insert($value);

    $response['status'] = 1;
    $response['message'] = "Quiz list added successfully";
    return $response;
  }

  public function getQuizlist(){

  $quizlist = DB::table('lmsquizzes')
            ->get();

  $response['data'] = $quizlist;
  return $response;
}

  public function updateQuizlist(Request $request , $id){
    $record = new LmsQuiz();
    $name = $request->title;
    $value = array(
      'title'                              => $request->title,
      'slug'                               => $record->makeSlug($name,TRUE),
      'dueration'                          => $request->dueration,
      'category_id'                        => $request->category_id,
      'series_id'                          => $request->series_id,
      'attempts'                           => $request->attempts,
      'total_marks'                        => $request->total_marks,
      'having_negative_mark'              => $request->having_negative_mark,
      'negative_mark'                     => $request->negative_mark,
      'pass_percentage'                    => $request->pass_percentage,
      'tags'                               => $request->tags,
      'publish_results_immediately'         => $request->publish_results_immediately,
      'description'                        => $request->description,
      'total_questions'                    => $request->total_questions,
      'instructions_page_id'                => $request->instructions_page_id,
      'record_updated_by'                  => $request->record_updated_by,
      'created_at'                         => date('Y-m-d H:i:s'),
      'updated_at'                         => date('Y-m-d H:i:s')
    );

    $quizlist = DB::table('lmsquizzes')
              ->where('id', $id)
              ->update($value);
    $response['status'] = 1;
    $response['message'] = 'quizzes updated successfully';
    return $response;

  }

  public function deleteQuizlist($id){
    $quizlist = DB::table('lmsquizzes')
              ->where('id', $id)
              ->delete();

  $response['status'] = 1;
  $response['message'] = "quizlist deleted successfully";
  return $response;
  }


}