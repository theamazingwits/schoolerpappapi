<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ServiceProvider;

use DB;

class EventsController extends Controller {


  private $ServiceProvider;

  private $json_val;

  public function __construct()
  {
        if(!isset(self::$this->ServiceProvider)){
          $this->ServiceProvider = new ServiceProvider();
      }

      $path = 'public/NotifyMessage.json';
      $this->json_val = json_decode(file_get_contents($path), true);
  }

     /**
     * This method adds Class Notes to DB
     * @param  Request $request [Request Object]
     * @return void
     */

    public function getStaffEventsCategory($staff_id) { 

        $staff_info = DB::table('event_incharges')
                        ->whereIn('staff_user_id', [$staff_id])
                        ->get();


        if(!count($staff_info)) {
            $response = array(
                "status" => '0',
                "message" => 'No Assign Events...'
            );
    
            return $response;
        } 

        $eventid = [];
        foreach($staff_info as $value){
            $eventid[] = $value->event_id;            
        }   

        // print_r($eventid);exit;

        $record = DB::table('event as eve')
                    ->join('event_category as eve_cat', 'eve.event_category_id', 'eve_cat.id')
                    ->whereIn('eve.id', $eventid)
                    ->select('eve.event_category_id', 'eve_cat.name as category_name')
                    // ->orderby('eve_cat.updated_at','desc')
                    ->paginate(getRecordsPerPage());
                    // ->get();

        $response = array(
            "status" => '1',
            "data" => $record
        );

        return $response;

       
    }

    public function getStaffEvents($category_id) { 

        $response = [];

        $eventsInfo = DB::table('event')
                    ->where('event_category_id', $category_id)
                    ->select('id', 'event_category_id', 'name as category_name')
                    ->orderby('updated_at','desc')
                    ->get();
      
        $response = array(
            "status" => '1',
            "data" => $eventsInfo
        );

        return $response;
       
    }

    public function getAllAcademicYear() { 

        $response = [];

        $acadmic_info = DB::table('academics')
                        ->orderby('updated_at','desc')
                        ->get();

        if(!count($acadmic_info)) {
            $response = array(
                "status" => '0',
                "message" => 'No Accademic Found...'
            );
    
            return $response;
        } else {
            $response = array(
                "status" => '1',
                "data" => $acadmic_info
            );
    
            return $response;
        
        }
       
    }

    public function getAcademicYearBasedCourse($acadmic_id) { 

        $response = [];
        
        $cource_info = DB::table('academic_course')
                        ->where('academic_id', $acadmic_id)
                        ->orderby('updated_at','desc')
                        ->get();

        if(!count($cource_info)) {
            $response = array(
                "status" => '0',
                "message" => 'No Accademic Course Found...'
            );
    
            return $response;
        } 

        $courceid = [];
        foreach($cource_info as $value){
            $courceid[] = $value->course_id;            
        }   

        $record = DB::table('courses')
                    ->whereIn('id', $courceid)
                    ->orderby('updated_at','desc')
                    ->get();
            
        $response = array(
            "status" => '1',
            "data" => $record
        );

        return $response;
       
    }

    public function getCourseBasedStudentList(Request $request) {    

        $academic_id = $request->academic_year_id;
        $course_id = $request->course_id;
     
        $response = array();

        $records_student = DB::table('students as stud')
                                ->where('stud.academic_id', $academic_id)
                                ->where('stud.course_id', $course_id)
                                ->orderBy('stud.updated_at','desc')
                                ->distinct()
                                ->get();

        $response = array(
            "status" => '1',
            "data" => $records_student
        );

        return $response;

      
    }

    public function assignStudentsEvent(Request $request) {
        
        $academic_year_id = $request->academic_year_id;
        $event = $request->event_id;
        $course_id = $request->course_id;
        $selected_students = $request->selected_students;

        $event_students_count = DB::table('event_students')
                                ->where('academic_year_id', '=', $academic_year_id)
                                ->where('course_id', '=', $course_id)
                                ->where('event_id', '=', $event) 
                                ->get()
                                ->count();

        $input_count = 0;
                            
        foreach($selected_students as $record) {
            $selected = $record['student_id'];

            if($selected) {
                $input_count++;        
            }
        }    

        $total_count = $event_students_count + $input_count;

        $event_maximum_students_count = DB::table('event')
                                            ->where('id', '=', $event)
                                            ->select(['maximum_students_count'])
                                            ->first()->maximum_students_count;

       if($total_count > $event_maximum_students_count) {
            $response = array(
                'message' => 'Event has reached maximum students !!',
                'status' => 0
            );
        } else {
            foreach($selected_students as $record) {
                $selected = $record['student_id'];
                if($selected) {
                    $student_id = $record['student_id'];
                    $table = array(
                        'event_id'   => $event,
                        'course_id'   => $course_id,
                        'academic_year_id'   => $academic_year_id,
                        'student_id'   => $student_id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    );

                    DB::table('event_students')
                        ->insert($table);

                $notificationusers = DB::table('users as usr')
                                    ->where('usr.id', $student_id)
                                    ->select(['usr.push_web_id','usr.push_app_id','usr.os_type','usr.name','usr.id','usr.app_type'])
                                    ->get();

                $aptyp = DB::table('users')
                        ->where('id',$student_id)
                        ->first(['app_type']);

                $this->ServiceProvider->data = $notificationusers;
                $this->ServiceProvider->aptyp = $aptyp->app_type;
                $this->ServiceProvider->message = $this->json_val['NOWmsg0003A'].$student_id.$this->json_val['NOWmsg0003B'];
                // hai {{student_name}} event has been assigned to you
                $this->ServiceProvider->pageId = "UNKNOWN";
                $this->ServiceProvider->insertNotification($this->ServiceProvider);

               }
            }

            $response = array(
                'message' => 'Events assigned successfully !!',
                'status' => 1
            );
        }  
        return $response;
    }

    public function getEventsBasedStudents(Request $request) {

        $academic_id = $request->academic_year_id;
        $course_id = $request->course_id;
        $event_id = $request->event_id;

        $data = DB::table('students')
                    ->where('academic_id', '=', $academic_id)
                    ->where('course_id', '=', $course_id)->get();
     
        $record = array();

        foreach($data as $result) {
           
            $student_id = $result->id;

            $event_data = DB::table('event_students')
                                ->where('student_id', '=', $student_id)
                                ->where('academic_year_id', '=', $academic_id)    
                                ->where('course_id', '=', $course_id)
                                ->where('event_id', '=', $event_id)    
                                ->first();

            if($event_data) {
                $result->event = $event_data? $event_data->event_id: 0;

                if($result->event) {
                    $result->event_name =  DB::table('event')
                                    ->where('id', '=', $event_data->event_id)->first();
                }
                array_push($record, $result);
            }
        }

        $response = array(
            "status" => '1',
            "data" => $record
        );

        return $response;
    }

    public function getStaffRequestedEvents($staff_id) {

        $response = array();

        $staff_info = DB::table('event_incharges')
                        ->whereIn('staff_user_id', [$staff_id])
                        ->get();

        if(!count($staff_info)) {
            $response = array(
                "status" => '0',
                "message" => 'No Assign Events...'
            );
            return $response;
        } 

        $eventid = [];
        foreach($staff_info as $value){
            $eventid[] = $value->event_id;            
        }   

        $record = DB::table('event as eve')
                    ->join('event_category as eve_cat', 'eve.event_category_id', 'eve_cat.id')
                    ->join('event_join_requests as eve_join', 'eve_join.event_id', 'eve.id')
                    ->leftjoin('students as stud', 'stud.id', 'eve_join.student_id')
                    ->leftjoin('academics as acad', 'acad.id', 'eve_join.academic_id')
                    ->leftjoin('courses as cour', 'cour.id', 'eve_join.course_id')
                    ->leftjoin('users as use', 'use.id', 'stud.user_id')
                    ->whereIn('eve.id', $eventid)
                    ->select('eve_join.id as event_join_requests_id', 'eve.id as event_id', 'eve_cat.id as category_id', 'eve_cat.name as category_name', 'eve.name as event_name',
                                'eve.maximum_students_count', 'eve.has_fee', 'eve.fee', 'eve.weekly_class_count', 'eve.created_at',
                                'eve_join.requested_by', 'eve_join.status as request_status', 'eve_join.payment_status', 
                                'stud.first_name', 'stud.middle_name', 'stud.last_name', 'stud.user_id', 'stud.roll_no', 'use.image',
                                'acad.academic_year_title as academic_year', 'cour.course_title', 'cour.course_code')
                    ->orderby('eve_join.updated_at', 'desc')
                    ->paginate(getRecordsPerPage());

        $response = array(
            "status" => '1',
            "data" => $record
        );
        return $response;
    }


    /* Student Side Process */

    public function getStudentEvents($student_id) {

        $data = DB::table('students')
                    ->where('user_id', '=', $student_id)
                    ->first();

        $student_id = $data->id;
     
        $record = array();
        $event_list = array();
        
        $event_list = DB::table('event_students')
                            ->where('student_id', '=', $student_id)
                            ->get();
 
        foreach($event_list as $result) {
            $result->event_name = DB::table('event as eve')
                                    ->join('event_category as eve_cat', 'eve.event_category_id', 'eve_cat.id')
                                    ->join('event_incharges as eve_inc', 'eve_inc.event_id', 'eve.id')
                                    ->leftjoin('staff as stf', 'stf.user_id', 'eve_inc.staff_user_id')                                    
                                    ->where('eve.id',$result->event_id)
                                    ->select('eve.id', 'eve_cat.name as event_category_name', 'eve.name as event_name', 
                                    'eve.has_fee', 'eve.fee', 'eve.weekly_class_count',
                                    'eve.event_starting_time', 'eve.event_ending_time', 'eve.payment_status',
                                    DB::raw('CONCAT(stf.first_name, stf.middle_name, stf.last_name) AS staff_incharge'))
                                    ->paginate(getRecordsPerPage());
                                    // ->first();

            array_push($record, $result);
        }

        $response = array(
            "status" => '1',
            "data" => $record
        );

        return $response;
    }

    public function getEventsCategory() { 

        $response = array();
        $data = DB::table('event_category')
                    ->get();

        $response = array(
            "status" => 1,
            "data" => $data
        );

        return $response;
       
    }

    public function getCategoryBasedEvents($category_id) {
        
        $response = array();
        $data = DB::table('event as eve')
                        ->join('event_category as eve_cat', 'eve.event_category_id', 'eve_cat.id')
                        ->where('event_category_id', $category_id)
                        ->select('eve.id as event_id','event_category_id', 'eve_cat.name as event_category_name', 'eve.name as event_name', 
                                    'eve.has_fee', 'eve.fee', 'eve.weekly_class_count')
                                    ->orderby('eve.updated_at','desc')
                        ->get();

        foreach ($data as $result) {
            $result->staff_incharge = DB::table('event as eve')
                    ->join('event_incharges as eve_inc', 'eve_inc.event_id', 'eve.id')
                    ->leftjoin('staff as stf', 'stf.user_id', 'eve_inc.staff_user_id')                                    
                    ->where('eve.id', '=', $result->event_id)
                    ->select(DB::raw('CONCAT(stf.first_name, stf.middle_name, stf.last_name) AS staff_incharge'))
                    ->first();
        }

        $response = array(
            "status" => 1,
            "data" => $data
        );

        return $response;
    }

    public function sendEventReuest(Request $request) {
       
        $response = array();
        $student_data = DB::table('students')
                 ->where('user_id', '=', $request->student_id)
                ->first();

        $ifevent_exits = DB::table('event_students')
                        ->where('academic_year_id', $student_data->academic_id)
                        ->where('course_id', $student_data->course_id)
                        ->where('event_id', $request->event_id)
                        ->where('student_id', $student_data->id)
                        ->first();

        if($ifevent_exits) {
            $response = array(
                'status' => 0,
                'message' => 'Already You have Participates the Event'
            );
            return $response;
        } else {
            $ifevent_request_accept = DB::table('event_join_requests')
                                        ->where('academic_id', $student_data->academic_id)
                                        ->where('course_id', $student_data->course_id)
                                        ->where('event_id', $request->event_id)
                                        ->where('student_id', $student_data->id)
                                        ->where('status', '1')
                                        ->first();

            if($ifevent_request_accept) {
                $response = array(
                    'status' => 0,
                    'message' => 'Already You have Participates the Event'
                );

                return $response; 
            } 
            $ifevent_request_waiting = DB::table('event_join_requests')
                                        ->where('academic_id', $student_data->academic_id)
                                        ->where('course_id', $student_data->course_id)
                                        ->where('event_id', $request->event_id)
                                        ->where('student_id', $student_data->id)
                                        ->where('status', '0')
                                        ->first();


            if($ifevent_request_waiting) {
                $response = array(
                    'status' => 0,
                    'message' => 'You Already Request the Event.. If have waiting'
                );

                return $response; 
            } else {
                $eventreq_array = array(
                    // 'event_category_id' => $request->event_category_id,
                    'event_id' => $request->event_id,
                    'academic_id' => $student_data->academic_id,
                    'course_id' => $student_data->course_id,
                    'student_id' => $student_data->id,
                    'parent_id' => $request->parent_id,
                    'requested_by' => $request->requested_by,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                );
    
                DB::table('event_join_requests')
                        ->insert($eventreq_array);

                $staff = DB::table('event_incharges')
                        ->where('event_id', $request->event_id)
                        ->select('staff_id', 'staff_user_id')
                        ->first();
                print_r($staff);


                $notificationusers = DB::table('users as usr')
                                ->where('usr.id', $staff->staff_id)
                                ->select(['usr.push_web_id','usr.push_app_id','usr.os_type','usr.name','usr.id','usr.app_type'])
                                ->get();

                print_r($notificationusers);
                

                $aptyp = DB::table('users')
                        ->where('id', $staff->staff_id)
                        ->first(['app_type']);

                $this->ServiceProvider->data = $notificationusers;
                $this->ServiceProvider->aptyp = $aptyp->app_type;
                $this->ServiceProvider->message = $this->json_val['NOWmsg0004A'].$request->student_id.$this->json_val['NOWmsg0004C'];
                // hai {{student_name}} has been sent a event request
                $this->ServiceProvider->pageId = "UNKNOWN";
                $this->ServiceProvider->insertNotification($this->ServiceProvider);
    
                $response = array(
                    'status' => 1,
                    'message' => 'Event Requested applied successfully'
                );
    
                return $response;
            }
        }
    }

    public function getStudentRequestedEvents($student_id) {

        $data = DB::table('students')
                    ->where('user_id', '=', $student_id)
                    ->first();

        $student_id = $data->id;
     
        $record = array();
        $event_list = array();
        
        $event_list = DB::table('event_join_requests')
                            ->where('student_id', '=', $student_id)
                            ->orderby('updated_at','desc')
                            ->get();

        foreach($event_list as $result) {
            $result->event_name = DB::table('event as eve')
                                    ->join('event_category as eve_cat', 'eve.event_category_id', 'eve_cat.id')
                                    ->join('event_incharges as eve_inc', 'eve_inc.event_id', 'eve.id')
                                    ->leftjoin('staff as stf', 'stf.user_id', 'eve_inc.staff_user_id')                                    
                                    ->where('eve.id', $result->event_id)
                                    ->select('eve.id', 'eve_cat.name as event_category_name', 'eve.name as event_name', 
                                    'eve.has_fee', 'eve.fee', 'eve.weekly_class_count',
                                    'eve.event_starting_time', 'eve.event_ending_time',
                                    DB::raw('CONCAT(stf.first_name, stf.middle_name, stf.last_name) AS staff_incharge'))
                                    // ->get(getRecordsPerPage());
                                // ->first();
                                ->paginate(getRecordsPerPage());
                                    // ->get();
                                    // ->links();

            array_push($record, $result);
        
        }

        $response = array(
            "status" => '1',
            "data" => $record
        );

        return $response;
    }

     // Accept Reject Request Event
    public function updateRequestEventStatus(Request $request) {

        if($request->request_status == '1') {

            $event = DB::table('event_join_requests')
                    ->where('id',$request->event_join_requests_id)
                    ->first();
            $student_id = $event->student_id;
            print_r($student_id);
            // exit();

            $event_students_count = DB::table('event_students')
                                    ->where('academic_year_id', $event->academic_id)
                                    ->where('course_id', $event->course_id)
                                    ->where('event_id', $event->event_id) 
                                    ->get()
                                    ->count();

                                    print_r($event_students_count);


            $event_maximum_students_count = DB::table('event')
                                                ->where('id', '=', $event->event_id)
                                                ->select(['maximum_students_count'])
                                                ->first()->maximum_students_count;
                                                 print_r($event_maximum_students_count);

                                    

            if($event_students_count > $event_maximum_students_count) {

             $notificationusers = DB::table('users as usr')
                                ->join('event_join_requests as ejr', 'ejr.student_id', 'usr.id')
                                ->where('usr.id', $student_id)
                                ->select(['usr.push_web_id','usr.push_app_id','usr.os_type','usr.name','usr.id','usr.app_type'])
                                ->get();
            print_r($notificationusers);

                $aptyp = DB::table('users')
                        ->where('id',$student_id)
                        ->first(['app_type']);

                $this->ServiceProvider->data = $notificationusers;
                $this->ServiceProvider->aptyp = $aptyp->app_type;
                $this->ServiceProvider->message = $student_id.$this->json_val['NOWmsg0005E'];
                // {student_name} Sorry Event has reached maximum students
                $this->ServiceProvider->pageId = "UNKNOWN";
                $this->ServiceProvider->insertNotification($this->ServiceProvider);

                $response = array(
                    'message' => 'Event has reached maximum students !!',
                    'status' => 0
                );
            } else {

                $update_values = array(
                    'status' => $request->request_status,
                    'updated_at' => date('Y-m-d H:i:s')
                    );

                DB::table('event_join_requests')
                    ->where('id',$request->event_join_requests_id)
                    ->update($update_values);

                $notificationusers = DB::table('users as usr')
                                    ->join('event_join_requests as ejr', 'ejr.student_id', 'usr.id')
                                    ->where('usr.id', $student_id)
                                    ->select(['usr.push_web_id','usr.push_app_id','usr.os_type','usr.name','usr.id','usr.app_type'])
                                    ->get();

                $aptyp = DB::table('users')
                        ->where('id',$student_id)
                        ->first(['app_type']);

                $this->ServiceProvider->data = $notificationusers;
                $this->ServiceProvider->aptyp = $aptyp->app_type;
                $this->ServiceProvider->message = $this->json_val['NOWmsg0006A'].$student_id.$this->json_val['NOWmsg0006B'];
                // hai {student_name} yours Event Request has Accepted
                $this->ServiceProvider->pageId = "UNKNOWN";
                $this->ServiceProvider->insertNotification($this->ServiceProvider);
                
                $response = array(
                    "status" => '1',
                    "message" => 'Event Request has Accepted'
                );
            }
           
        
        } else {
        
            $update_values = array(
                    'status' => $request->request_status,
                    // 'reject_reason' => $request->reject_reason,
                    'updated_at' => date('Y-m-d H:i:s')
                    );

            DB::table('event_join_requests')
                ->where('id',$request->event_join_requests_id)
                ->update($update_values);

            $notificationusers = DB::table('users as usr')
                        ->join('event_join_requests as ejr', 'ejr.student_id', 'usr.id')
                        ->where('usr.id', $student_id)
                        ->select(['usr.push_web_id','usr.push_app_id','usr.os_type','usr.name','usr.id','usr.app_type'])
                        ->get();

            $aptyp = DB::table('users')
                    ->where('id',$student_id)
                    ->first(['app_type']);

            $this->ServiceProvider->data = $notificationusers;
            $this->ServiceProvider->aptyp = $aptyp->app_type;
            $this->ServiceProvider->message = $student_id.$this->json_val['NOWmsg0007A'];
            // {{student_name}} Sorry yours event Request has Rejected 
            $this->ServiceProvider->pageId = "UNKNOWN";
            $this->ServiceProvider->insertNotification($this->ServiceProvider);
    
            $response = array(
                "status" => '1',
                "message" => 'Event Request has Rejected'
            );
            
        }
        return $response;
    }
}