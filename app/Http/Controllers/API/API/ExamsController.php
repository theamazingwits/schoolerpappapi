<?php

namespace App\Http\Controllers\API;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use DB;
use Image;
use ImageSettings;


class ExamsController extends Controller
{
    
     public function bookmarks($user_id)
   {
    $response['status'] = 1;
    $response['message'] = '';

      $records = \App\Bookmark::join('questionbank', 'questionbank.id', '=', 'bookmarks.item_id')
            ->select(['question_type', 'question','marks','bookmarks.id','bookmarks.user_id'])
            ->where('user_id','=',$user_id)
            ->orderBy('bookmarks.updated_at', 'desc')->get();
            
      $response['data'] = $records;
      return $response;
   }

            /**
     * Delete Record based on the provided slug
     * @param  [string] $slug [unique slug]
     * @return Boolean 
     */
    public function deleteBookmarkById(Request $request, $item_id)
    {
      // $user     = \App\User::where('id', $request->user_id)->first();
      $response['status'] = 0;
      // if(!$user)
      // {
      //   $response['message'] = 'Invalid Userid';
      //   return $response;
      // }
// return $item_id;
      // $record = \App\Bookmark::where('item_id', '=', $item_id )
      //             ->where('user_id', '=', $user->id)
      //             ->where('item_type', '=', 'questions')
      //             ->first();

       $record = \App\Bookmark::find($item_id);

        if(!$record)
        {
          $response['status'] = 0;
          $response['message'] = 'Invalid Bookmark record';  
          return $response;
        }
        
        $record->delete();
        $response['status'] = 1;
        $response['message'] = 'Bookmark removed';
        return json_encode($response);
    }



    /**
     * List the categories available
     * @param  [type] $slug [description]
     * @return [type]       [description]
     */
    public function exams(Request $request, $slug='')
    {
      $category = FALSE;

      $user_id = $request->user_id;
      $user    =  \App\User::where('id','=', $user_id)->first();

      $interested_categories      = null;

      $response['status'] = 0;
      $response['message'] = null;
      $response['exams'] = null;

      

      if(!$user)
      {
        $response['message'] = 'Invalid User';
        return $response;
      }

      if (isset($user->settings))
      {

        $interested_categories =  json_decode($user->settings)->user_preferences;
      }

      // $response['user'] = $user;
      // $response['settings'] = $user->settings;
      // $response['interested_categories'] = $interested_categories;

      


      $records = null;
      if($slug=='all')
        {
             
          
           // dd($interested_categories);
             /*$cats  = $interested_categories->quiz_categories;
            
            $records = \App\Quiz::join('quizcategories', 'quizzes.category_id', '=', 'quizcategories.id')
            ->select(['title', 'dueration', 'category', 'is_paid', 'total_marks','tags','quizzes.slug','quizzes.validity','quizzes.cost','quizzes.id as id' ])
            ->where('total_marks', '!=', 0)
            ->where('start_date','<=',date('Y-m-d'))
            ->where('end_date','>=',date('Y-m-d'))
            ->whereIn('quizzes.category_id',$cats)
            ->get();*/

            $query = \App\Quiz::join('quizcategories', 'quizzes.category_id', '=', 'quizcategories.id');
           

            $query->select(['title', 'dueration', 'category', 'is_paid', 'total_marks','tags','quizzes.slug','quizzes.validity','quizzes.cost','quizzes.id as id'])
            ->where('total_marks', '!=', 0)
            ->where('quizzes.total_questions','!=',0)
            ->where('start_date','<=',date('Y-m-d H:i:s'))
            ->where('end_date','>=',date('Y-m-d H:i:s'));
            if($interested_categories)
            $query = $query->whereIn('category_id',(array) $interested_categories->quiz_categories);
            else
            $query = $query->where('category_id','=','-1');          
            
            $general_query = $query;
            $records = $general_query->where('applicable_to_specific', '=',0)->get();

        }
        else {
          $category = \App\QuizCategory::getRecordWithSlug($slug);

          // $response['category'] = $category;

          if ($category) {

            /*$records = \App\Quiz::join('quizcategories', 'quizzes.category_id', '=', 'quizcategories.id')
                ->select(['title', 'dueration', 'category', 'is_paid', 'total_marks','quizzes.slug', 'quizzes.validity','quizzes.cost','quizzes.id as id' ])
                ->where('quizzes.category_id', '=', $category->id)
                ->where('total_marks', '!=', 0)
                ->where('start_date','<=',date('Y-m-d'))
                ->where('end_date','>=',date('Y-m-d'))
                ->get();*/

              $query = \App\Quiz::join('quizcategories', 'quizzes.category_id', '=', 'quizcategories.id')
              ->select(['title', 'dueration', 'category', 'is_paid', 'total_marks','quizzes.slug', 'quizzes.validity','quizzes.cost','quizzes.id as id' ])
              ->where('applicable_to_specific', '=',0)
              ->where('quizzes.category_id', '=', $category->id)
              ->where('quizzes.total_questions','!=',0)
              ->where('total_marks', '!=', 0)
              ->where('start_date','<=',date('Y-m-d H:i:s'))
              ->where('end_date','>=',date('Y-m-d H:i:s'));
              if($interested_categories)
              $query = $query->whereIn('category_id',(array) $interested_categories->quiz_categories);
              else
                $query = $query->where('category_id','=','-1');
              $records = $query->get();


              // $response['records'] = $records;

                

                 $response['status'] = 1;
            }
            else{
              $response['message'] = 'Invalid category';
            }
        }

     
      // $response['exams'] = $records;
      $exams =[];
      // $record = $records[2];
      foreach($records as $item)
      {
        
        $temp['title'] = $item->title;
        $temp['dueration'] = $item->dueration;
        $temp['category'] = $item->category;
        $temp['is_paid'] = $item->is_paid;

        $temp['is_purchased'] = (int)\App\Payment::isItemPurchased($item->id, 'exam', $user->id);
        $temp['total_marks'] = $item->total_marks;
        $temp['slug'] = $item->slug;
        $temp['validity'] = $item->validity;
        $temp['cost'] = $item->cost;
        $temp['id'] = $item->id;
        $temp['total_questions'] = count($item->getQuestions());

    

        $exams[] = $temp;

      }
      
      $response['exams'] = $exams;
      return $response;
    }



     public function scheduledExams($user_id)
     {
       
        $student  =  \App\Student::where('user_id',$user_id)->first();
        if($student){

              $records = \App\Quiz::join('quizcategories', 'quizzes.category_id', '=', 'quizcategories.id')
                ->join('quizapplicability', 'quizapplicability.quiz_id', '=', 'quizzes.id')
                ->where('quizzes.type','=','online')
                ->where('quizapplicability.academic_id', '=', $student->academic_id)
                ->where('quizapplicability.course_parent_id', '=', $student->course_parent_id)
                ->where('quizapplicability.course_id', '=', $student->course_id)
                ->where('quizapplicability.year', '=', $student->current_year)
                ->where('quizapplicability.semister', '=', $student->current_semister)
                ->where('applicable_to_specific', '=',1)
                ->where('total_marks', '!=', 0)
                ->where('quizzes.total_questions','!=',0)
                ->where('start_date','<=',date('Y-m-d'))
                ->where('end_date','>=',date('Y-m-d'))
                ->select(['title', 'dueration', 'category', 'is_paid', 'total_marks','quizzes.slug', 'quizzes.validity','quizzes.cost','quizzes.id as id' ])
                // ->get();
                ->paginate(getRecordsPerPage());

                if(count($records) > 0){


                    // $response['exams'] = $records;
                      $exams =[];
                      // $record = $records[2];
                      foreach($records as $item)
                      {
                        
                        $temp = [];
                        $temp['title']        = $item->title;
                        $temp['dueration']    = $item->dueration;
                        $temp['category']     = $item->category;
                        $temp['is_paid']      = $item->is_paid;

                        /*$temp['is_purchased'] = (int)\App\Payment::isItemPurchased($item->id, 'exam', $student->user_id);*/

                        $temp['is_purchased'] = (int)\App\Payment::isItemPurchased($item->id, 'exam', $user_id);//56,'exam',2172

                        $temp['total_marks']  = $item->total_marks;
                        $temp['slug']         = $item->slug;
                        $temp['validity']     = $item->validity;
                        $temp['cost']         = $item->cost;
                        $temp['id']           = $item->id;
                        $temp['total_questions'] = count($item->getQuestions());
                        $exams[] = $temp;
                        unset($temp);

                      }
                      
                      $response['exams'] = $exams;
                      $response['status']  = 1;
                    

                }else{

                     $response['status']  = 0;
                     $response['message']  = "No exams are avaialble";
                }
  
        }else{

           $response['status']  = 0;
           $response['message']  = "User not available";
        }


        return $response;

     } 




    /**
     * This method displays all the details of selected exam series
     * @param  [type] $slug [description]
     * @return [type]       [description]
     */
    public function viewSeriesItem(Request $request, $slug='')
    {
     
        $record = \App\ExamSeries::getRecordWithSlug($slug); 
        $response['status'] = 0;
        $response['message']='';

        $user_id = $request->user_id;
        $user =   $user = \App\User::where('id','=', $user_id)->first();

        if(!$record)
        {
         $response['message'] = 'Invalid Exam Series';
         return $response;
        }

        $r = $record;
          $temp['title']    = $r->title;
          $temp['id']       = $r->id;
          $temp['slug']       = $r->slug;
          $temp['category_id']       = $r->category_id;
          $temp['is_paid']       = $r->is_paid;
          $temp['cost']       = $r->cost;
          $temp['validity']       = $r->validity;
          $temp['total_exams']       = $r->total_exams;
          $temp['total_questions']       = $r->total_questions;
          $temp['image']       = $r->image;
          $temp['short_description']       = $r->short_description;
          $temp['start_date']       = $r->start_date;
          $temp['end_date']       = $r->end_date;
          $temp['end_date']       = $r->end_date;
          $temp['is_purchased'] = (int)\App\Payment::isItemPurchased($r->id, 'combo', $user->id);

         
        $response['content_record']     = FALSE;
        $response['item']               = $temp;
        $response['total_exams']          = $record->total_exams;
        $response['items_list']          = $record->itemsListService();
        $response['status'] = 1;
        return $response;
    }

    public function examSeries($user_id)
    {
      
      //$user_id = $request->user_id;

      $user    = \App\User::where('id','=', $user_id)->first();

      $interested_categories  = null;
      $categories             = null;

      $response['status']     = 0;
      $response['message']    = null;
      $response['records']    = null;

       if($user->settings)
       {
         $interested_categories =  json_decode($user->settings)->user_preferences;
       }

       if( $interested_categories ) {

        if( count( $interested_categories->quiz_categories ) ){

             $categories   = \App\QuizCategory::whereIn('id',(array) $interested_categories->quiz_categories)
                                            ->pluck('id')
                                            ->toArray();
             }                               
        }

        // return $response['data']  = $categories;
        
        if( count( $categories ) > 0){

          $recs    = \App\ExamSeries::where('start_date','<=',date('Y-m-d'))
                                        ->where('end_date','>=',date('Y-m-d'))
                                        ->whereIn('category_id',$categories)
                                        // ->get();
                                        ->paginate(getRecordsPerPage());
                                        
          $records = [];

        // foreach($recs as $r)
        // {

        //   $temp['title']             = $r->title;
        //   $temp['id']                = $r->id;
        //   $temp['slug']              = $r->slug;
        //   $temp['category_id']       = $r->category_id;
        //   $temp['is_paid']           = $r->is_paid;
        //   $temp['cost']              = $r->cost;
        //   $temp['validity']          = $r->validity;
        //   $temp['total_exams']       = $r->total_exams;
        //   $temp['total_questions']   = $r->total_questions;
        //   $temp['image']             = $r->image;
        //   $temp['short_description'] = $r->short_description;
        //   $temp['start_date']        = $r->start_date;
        //   $temp['end_date']          = $r->end_date;
        //   $temp['is_purchased']      = (int)\App\Payment::isItemPurchased($r->id, 'combo', $user->id);
        //   $records[] = $temp;

        //   }

          foreach($recs as $i => $r)  {
          $recs[$i]->is_purchased = (int)\App\Payment::isItemPurchased($r->id, 'combo', $user->id); 
        }

          $response['status'] = 1;
          $response['records'] = $recs;
          return $response;

        }else{
           
           $response['status'] = 0;
          $response['message'] = 'Please Update Your Settings';
          return $response;

        }
       
  }




    public function getQuestions(Request $request, $slug)
    {

      // return $data['test']  = "ghaa";

        $quiz                = \App\Quiz::getRecordWithSlug($slug);

        $questions = [];
        $data['status'] = 0;
        $sno = 1;
        if(!$quiz)
        {
          $data['message'] = 'Invalid quiz';
          return $data;
        }


       $user_id = $request->user_id;

        $user =   $user = \App\User::where('id','=', $user_id)->first();


        if(!$user)
        {
          $data['message'] = 'Invalid UserID';
          return $data;
        }

     

        $time                = $this->convertToHoursMins($quiz->dueration);
     

          $prepared_records   = (object) $quiz->prepareWebQuestions($quiz->getQuestions(),'',$user_id);
        
        $data['time_hours']         = makeNumber($time['hours'],2,'0','left');
        $data['time_minutes']       = makeNumber($time['minutes'],2,'0','left');
        $data['time_seconds']       = makeNumber($time['seconds'],2,'0','left');

      

        $data['quiz']               = $quiz;
        $data['user']               = $user;
        $data['title']              = $quiz->title;
       
       
        $final_questions             = $prepared_records->questions;
        $final_subjects              = $prepared_records->subjects;
        $data['questions']           = $final_questions;
        $data['subjects']            = $final_subjects;
        $bookmarks                   = array_pluck($final_questions, 'id');
        $data['bookmarks']           = $bookmarks;
      
        $temp_answers = [];
        foreach ($final_questions as $question) {
            
            if($question->question_type == 'para' || $question->question_type == 'audio' 
              || $question->question_type == 'video'){
             

                 $questions_data  = json_decode($question->answers);
                $ftemp  = [];
                 $t = $this->decodeQuestionsData($questions_data);
                // dd($t);
                foreach($t as $temp ){

                  if(isset($temp['options']))
                  {
                   if(count($temp['options']))
                   {
                     $temp['options'] = $temp['options'][0];
                   }
                  }

                 if(isset($temp['optionsl2']))
                 {
                   if(count($temp['optionsl2']))
                   {
                     $temp['optionsl2'] = $temp['optionsl2'][0];

                   }
                 }
                 // if(count($temp['options']))
                  $ftemp[] = $temp;
                }
                 // if($question->id==67)
                 //  dd(($t));
                  // dd(($temp));
               
                // dd('asdf');

             $question->answers = json_encode($t);
            }

        }

         // $data['all_options']  = $all_options;
       
        

       $data['right_bar_data']     = array(
                                              'questions'      => $final_questions, 
                                            
                                              'quiz'           => $quiz, 
                                              'time_hours'     => $data['time_hours'], 
                                              'time_minutes'   => $data['time_minutes'],
                                              
                                              );

        

       
      
        return $data;

    }

    public function decodeQuestionsData($questions_data)
    {
      $ftemp = [];
      $i=0;
       foreach ($questions_data as $answer) {
                  $temp=[];
                  $options = [];
                  $temp['question'] = $answer->question;
                  $temp['questionl2'] = '';
                  if(isset($answer->questionl2))
                  $temp['questionl2'] = $answer->questionl2;
                  $temp['total_options'] = $answer->total_options;
                  
                   $options  = ($answer->options);
                   $optionsl2  = null;
                   if(isset($answer->questionl2))
                   $optionsl2  = ($answer->optionsl2);

                   if(is_array($options))
                   {
                  if(count($options))
                    $options = $options[0];
                }
                if(is_array($optionsl2))
                {
                  if(count($optionsl2))
                    $optionsl2 = $optionsl2[0];
                
                }
               

                $temp_options = [];
                foreach($options as $o)
                {
                  $temp_options[] = $o;
                }
              $temp_optionsl2 = [];
              if($optionsl2)
                foreach($optionsl2 as $o)
                {
                  $temp_optionsl2[] = $o;
                }

                if(count($temp_options))
                  $temp_options = $temp_options;
                
                if(count($temp_optionsl2))
                {
                  $temp_optionsl2 = $temp_optionsl2;
                }


                $temp['options'] = $temp_options;
                if($i>0)
                {
                  if(isset($temp_options[0]))
                  {
                    if(is_array($temp_options[0]))
                      $temp['options'] = $temp_options[0];
                  }

                  if(isset($temp_optionsl2[0]))
                  {
                    if(is_array($temp_optionsl2[0]))
                      $temp['optionsl2'] = $temp_optionsl2[0];
                  }



                }

                $ftemp[] = $temp;
                
                 $i++;
                }

                return $ftemp;
    }

    public function saveQuizQuestions($questions, $quiz, $user_id)
    {
         $record                        = new \App\QuizQuestions();
         $record->quiz_id               = $quiz->id;
         $record->student_id            = $user_id;
         $record->questions_data        = json_encode($questions);
         $record->save();
    }

        /**
     * Convert minutes to Hours and minutes
     */
     function convertToHoursMins($time, $format = '%02d:%02d') 
    {
        if ($time < 1) {
            return;
        }
        $hours = floor($time / 60);
        $minutes = ($time % 60);
        $result['hours'] = $hours;
        $result['minutes'] = $minutes;
        $result['seconds'] = 0;
        return $result;
    }



     /**
     * After the exam complets the data will be submitted to this method
     * @param  Request $request [description]
     * @param  [type]  $slug    [description]
     * @return [type]           [description]
     */
    public function finishExam(Request $request, $slug)
    {
      
      // $alldata = json_decode(file_get_contents('php://input'), true);

      // header('Content-Type: application/json');
      $alldata = \Input::all();
      
      /*$response['record']  =  $alldata['time_spent'];
      $response['message'] = 'testing';
      return $response;*/

      $formated_data = [];
      $formated_data['time_spent'] = (array)json_decode($alldata['time_spent']);
      $formated_data['quiz_id'] = json_decode($alldata['quiz_id']);
      $formated_data['student_id'] = json_decode($alldata['student_id']);
      $formated_data['answers'] = (array)json_decode($alldata['answers']);



      // dd($formated_data);
      // dd($formated_data);
      // $time_spent = $alldata['time_spent'];
      // $answers = $alldata['answers'];
      // dd(json_decode($answers));
      // dd(($request->quiz_id));
      // // dd(($request->time_spent));
        $quiz = \App\Quiz::getRecordWithSlug($slug);

       $user_record = \App\User::where('id', $formated_data['student_id'])->first();
       $user = $user_record;
       $response['status'] = 0;
       $response['message'] = '';
        if(!$quiz)
        {
          $response['message'] = 'Invalid Quiz Record';
            return $response;
        }
        if(!$user)
        {
            $response['message'] = 'Invalid User Record';
            return $response;
        }

        $input_data = $formated_data;
        // $input_data = \Input::all();
        $answers = array();
        $time_spent = $request->time_spent;

       
       
        //Remove _token key from answers data prepare the list of answers at one place
        // foreach ($input_data as $key => $value) {
        //     if($key=='_token' || $key=='time_spent')
        //         continue;
        //     $answers[$key] = $value;
        // }
        $temp_answers = $formated_data['answers'];
        foreach($temp_answers as $key=>$ans)
        {
          // $key = (int)$key;
          // dd($key);
          $answers[(int)$key]=$ans;
        }

        // return $response['data'] = $answers;
        // $qution = $this->getQuestionRecord(72);
        // return $response['data'] = $qution['time_to_spend'];


        // dd($answers);

        // $recorded_questions    = QuizQuestions::where('quiz_id',$quiz->id)
        //                                       ->where('student_id',$user_record->id)
        //                                       ->where('is_exam_completed',0)
        //                                       ->first();


        // $recorded_questions->is_exam_completed  = 1;
        // $recorded_questions->save();   


        //Get the list of questions and prepare the list at one place
        //This is to find the unanswered questions
        //List the unanswered questions list at one place
        $questions = \DB::table('questionbank_quizzes')->select('questionbank_id', 'subject_id')
                     ->where('quize_id','=',$quiz->id)
                     ->get();
        
       /* return $response['data'] = array('input_time_spent'=>$time_spent,
                                          'get_questions'=>$questions);*/

        $subject                  = [];
        $time_spent_not_answered  = [];
        $not_answered_questions   = [];

        foreach($questions as $q)
        {
          
          // dd(json_decode($time_spent));
          $subject_id = $q->subject_id;
           if(! array_key_exists($q->subject_id, $subject)) {
              $subject[$subject_id]['subject_id']       = $subject_id;
              $subject[$subject_id]['correct_answers']  = 0;
              $subject[$subject_id]['wrong_answers']    = 0;
              $subject[$subject_id]['not_answered']     = 0;
              $subject[$subject_id]['time_spent']       = 0;
              $subject[$subject_id]['time_to_spend']       = 0;
              $subject[$subject_id]['time_spent_correct_answers']    = 0;
              $subject[$subject_id]['time_spent_wrong_answers']    = 0;
            }
            if(! array_key_exists($q->questionbank_id, $answers)){
              $subject[$subject_id]['not_answered']     += 1;
              $not_answered_questions[] = $q->questionbank_id;
              $time_spent_not_answered[$q->questionbank_id]['time_to_spend'] = 0;

             

              $time_spent_not_answered[$q->questionbank_id]['time_spent'] = 0;//$time_spent[$q->questionbank_id];

              $subject[$subject_id]['time_spent'] += 0;//$time_spent[$q->questionbank_id];
              
              
            }
        }
        
        $result =   $this->processAnswers($answers, $subject, $time_spent, $quiz->negative_mark);
        $result['not_answered_questions'] = json_encode($not_answered_questions);
        $result['time_spent_not_answered_questions'] = json_encode($time_spent_not_answered);
        
        $result = (object) $result;
        $answers = json_encode($answers);
        
        $record = new \App\QuizResult();
        $record->quiz_id = $quiz->id;
        $record->quiz_type = $quiz->type;
        $record->user_id = $user->id;
        $record->marks_obtained = $result->marks_obtained;
        $record->total_marks = $quiz->total_marks;
        $record->percentage = $this->getPercentage($result->marks_obtained, $quiz->total_marks);
        
        $exam_status = 'pending';
        if($record->percentage >= $quiz->pass_percentage)
            $exam_status = 'pass';
        else
            $exam_status = 'fail';

        $record->exam_status = $exam_status;
        $record->answers = $answers;
        $record->subject_analysis = $result->subject_analysis;
        $record->correct_answer_questions = $result->correct_answer_questions;
        $record->wrong_answer_questions = $result->wrong_answer_questions;
        $record->not_answered_questions = $result->not_answered_questions;
        $record->time_spent_correct_answer_questions = $result->time_spent_correct_answer_questions;
        $record->time_spent_wrong_answer_questions = $result->time_spent_wrong_answer_questions;
        $record->time_spent_not_answered_questions = $result->time_spent_not_answered_questions;


        $student_record = $user->student()->first();
        $record->academic_id = $student_record->academic_id;
        $record->course_parent_id = $student_record->course_parent_id;
        $record->course_id = $student_record->course_id;
        $record->year = $student_record->current_year;
        $record->semister = $student_record->current_semister;

        $record->slug = getHashCode();
       
        
        $content = 'You have attempted exam. The score percentage is '.formatPercentage($record->percentage);
       
        $record->save();

    
       
        // $template    = new \App\EmailTemplate();
        //   $content_data =  $template->sendEmailNotification('exam-result', 
        //  array('username'    =>$user_record->name, 
        //           'content'  => $content,
        //           'to_email' => $user_record->email));
      
      try {
        
  $user_record->notify(new \App\Notifications\StudentExamResult($user_record,$exam_status,$quiz->title,$quiz->pass_percentage)); 

      } catch (\Exception $e) {
        // dd($e->getMessage());
      }

        $topperStatus = false;
        $data['isUserTopper']       = $topperStatus;
        $data['rank_details']       = FALSE;
        $data['quiz']               = $quiz;
        $data['active_class']       = 'exams';
        $data['title']              = $quiz->title;
        $data['record']             = $record;

        $data['user']               = $user_record;
         
        //Chart Data START
        // $color_correct = getColor('background', rand(1,999));
        // $color_wrong = getColor('background', rand(1,999));
        // $color_not_attempted = getColor('background', rand(1,999));

        // $labels_marks = [getPhrase('correct'), getPhrase('wrong'), getPhrase('not_answered')];
        // $dataset_marks = [count(json_decode($record->correct_answer_questions)),
        //                   count(json_decode($record->wrong_answer_questions)), 
        //                   count(json_decode($record->not_answered_questions))];

        // $dataset_label_marks = "Marks";
        // $bgcolor  = [$color_correct,$color_wrong,$color_not_attempted];
        // $border_color = [$color_correct,$color_wrong,$color_not_attempted];
        // $chart_data['type'] = 'doughnut';
        //  $chart_data['data']   = (object) array(
        //     'labels'            => $labels_marks,
        //     'dataset'           => $dataset_marks,
        //     'dataset_label'     => $dataset_label_marks,
        //     'bgcolor'           => $bgcolor,
        //     'border_color'      => $border_color
        //     );
        
        // $data['marks_data'][] = (object)$chart_data; 

 
        $time_spent = 0;
        foreach(json_decode($record->time_spent_correct_answer_questions) as $rec)
        {
          $time_spent += 0;//$rec->time_spent;
        }
        foreach(json_decode($record->time_spent_wrong_answer_questions) as $rec)
        {
          $time_spent += 0;//$rec->time_spent;
        }
        foreach(json_decode($record->time_spent_not_answered_questions) as $rec)
        {
          $time_spent += 0;//$rec->time_spent;
        }

        //Time Chart Data
        // $color_correct       = getColor('background', rand(1,999));
        // $color_wrong          = getColor('background', rand(1,999));
        // $color_not_attempted  = getColor('background', rand(1,999));
        // $total_time           = $quiz->dueration*60;
        // $total_time_spent     = ($time_spent);
 
        // $labels_time          = [getPhrase('total_time').' (sec)', getPhrase('consumed_time').' (sec)'];
        // $dataset_time         = [ $total_time, $time_spent];

        // $dataset_label_time   = "Time in sec";
        // $bgcolor              = [$color_correct,$color_wrong,$color_not_attempted];
        // $border_color         = [$color_correct,$color_wrong,$color_not_attempted];
        // $chart_data['type']   = 'pie';
        //  $chart_data['data']  = (object) array(
        //                                         'labels'          => $labels_time,
        //                                         'dataset'         => $dataset_time,
        //                                         'dataset_label'   => $dataset_label_time,
        //                                         'bgcolor'         => $bgcolor,
        //                                         'border_color'    => $border_color
        //                                         );
        
        // $data['time_data'][]  = (object)$chart_data; 
 
        //Chart Data END

        // $quizrecordObject     = new \App\QuizResult();
        // $history              = array();
        // $history              = $quizrecordObject->getHistory();

        

        
        $notificationusers = DB::table('users as usr')
                            ->where('usr.id', $formated_data['student_id'])
                            ->select(['usr.push_web_id','usr.push_app_id','usr.os_type','usr.name','usr.id','usr.app_type'])
                            ->get();

        $aptyp = DB::table('users')
                ->where('id',$formated_data['student_id'])
                ->first(['app_type']);

        $this->ServiceProvider->data = $notificationusers;
        $this->ServiceProvider->aptyp = $aptyp->app_type;
        $this->ServiceProvider->message = $content;
        $this->ServiceProvider->pageId = "UNKNOWN";
        $this->ServiceProvider->insertNotification($this->ServiceProvider);
        // return view('student.exams.results', $data);
        $response['status'] = 1;
        $response['message'] = 'Successfully completed';
        $response['record'] = $data;
        return $response;
        //  $view_name = getTheme().'::student.exams.results';
        // return view($view_name, $data);


    }

    /**
     * Pick grade record based on percentage from grades table
     * @param  [type] $percentage [description]
     * @return [type]             [description]
     */
    public function getPercentageRecord($percentage)
    {
        return \DB::table('grades')
                ->where('percentage_from', '<=',$percentage)
                ->where('percentage_to', '>=',$percentage)
                ->get();
    }

    /**
     * This below method process the submitted answers based on the 
     * provided answers and quiz questions
     * @param  [type] $answers [description]
     * @return [type]          [description]
     */
    public function processAnswers($answers, $subject, $time_spent, $negative_mark = 0)
    {
        $obtained_marks     = 0;
        $correct_answers    = 0;
        $obtained_negative_marks = 0;

        $corrent_answer_question            = [];
        $wrong_answer_question              = [];
        $time_spent_correct_answer_question = [];
        $time_spent_wrong_answer_question   = [];


        
        foreach ($answers as $key => $value) {
          if( is_numeric( $key ))
         {
            $question_record  = (object) $this->getQuestionRecord($key);

            $question_type    = $question_record->question_type;
            $actual_answer    = $question_record->correct_answers;
          
            $subject_id       = $question_record->subject_id;
            if(! array_key_exists($subject_id, $subject)) {
              $subject[$subject_id]['subject_id']       = $subject_id;
              $subject[$subject_id]['correct_answers']  = 0;
              $subject[$subject_id]['wrong_answers']    = 0;
              $subject[$subject_id]['time_spent_correct_answers']    = 0;
              $subject[$subject_id]['time_spent_wrong_answers']    = 0;
              $subject[$subject_id]['time_spent']       = 0;
              $subject[$subject_id]['time_to_spend']       = 0;
             
             
            }

             $subject[$subject_id]['time_spent']       += 0;//$time_spent[$question_record->id];
             $subject[$subject_id]['time_to_spend']    += $question_record->time_to_spend;

             

            switch ($question_type) {
                case 'radio':
                                if($value[0] == $actual_answer)
                                {
                                    $correct_answers++;
                                    $obtained_marks                 += $question_record->marks;
                                    $corrent_answer_question[]       = $question_record->id;
                                    $subject[$subject_id]['correct_answers'] +=1;
                                    $subject[$subject_id]['time_spent_correct_answers'] += 0;//$time_spent[$question_record->id];

                                    $time_spent_correct_answer_question[$question_record->id]['time_to_spend'] 
                                                                    = $question_record->time_to_spend;
                                    $time_spent_correct_answer_question[$question_record->id]['time_spent'] 
                                                                    = 0;//$time_spent[$question_record->id];
                                    
                                }
                                else {
                                  
                                    $wrong_answer_question[]          = $question_record->id;
                                    $subject[$subject_id]['wrong_answers'] += 1;
                                    $obtained_marks                   -= $negative_mark;
                                    $obtained_negative_marks          += $negative_mark;
                                    $subject[$subject_id]['time_spent_wrong_answers']    
                                                                += 0;//$time_spent[$question_record->id];
                                    $time_spent_wrong_answer_question[$question_record->id]['time_to_spend'] 
                                                                    = $question_record->time_to_spend;
                                    $time_spent_wrong_answer_question[$question_record->id]['time_spent'] 
                                                                     = 0;//$time_spent[$question_record->id];
                                }
                               
                                break;

                case 'checkbox':
                                $actual_answer = json_decode($actual_answer);
                                $i=0;
                                $flag= 1;
                                foreach($value as $answer_key => $answer_value )
                                {
                                    if(isset($actual_answer[$answer_key]))
                                    {
                                        if( $actual_answer[$answer_key]->answer != 
                                            $answer_value )
                                        {
                                            $flag = 0; break;
                                        }
                                    }
                                    else {
                                        $flag = 0; break;
                                    }

                                }

                                if($flag)
                                {
                                    $correct_answers++;
                                    $obtained_marks += $question_record->marks;
                                    $corrent_answer_question[] = $question_record->id;
                                    $subject[$subject_id]['correct_answers'] +=1;
                                    $subject[$subject_id]['time_spent_correct_answers'] 
                                                                += 0;//$time_spent[$question_record->id];
                                    $time_spent_correct_answer_question[$question_record->id]['time_to_spend'] 
                                                                    = $question_record->time_to_spend;
                                    $time_spent_correct_answer_question[$question_record->id]['time_spent'] 
                                                                    = 0;//$time_spent[$question_record->id];

                                }
                                else {
                                    $wrong_answer_question[]          = $question_record->id;
                                    $subject[$subject_id]['wrong_answers'] += 1;
                                     $subject[$subject_id]['time_spent_wrong_answers']    
                                                                += 0;//$time_spent[$question_record->id];
                                    $obtained_marks                   -= $negative_mark;
                                    $obtained_negative_marks          += $negative_mark;
                                    $time_spent_wrong_answer_question[$question_record->id]['time_to_spend'] 
                                                                       = $question_record->time_to_spend;
                                    $time_spent_wrong_answer_question[$question_record->id]['time_spent'] 
                                                                       = 0;//$time_spent[$question_record->id];
                                }
                                
                                break;
                case 'blanks': 
                                $actual_answer = json_decode($actual_answer);
                                 $i=0;
                                $flag= 1;
                                foreach($actual_answer as $answer)
                                {
                                    if(strcasecmp(
                                        trim($answer->answer),
                                        trim($value[$i++])) != 0)
                                    {
                                        $flag = 0; break;
                                    }
                                }

                                if($flag)
                                {
                                    $correct_answers++;
                                    $obtained_marks += $question_record->marks;
                                    $corrent_answer_question[] = $question_record->id;
                                    $subject[$subject_id]['correct_answers'] +=1;
                                     $subject[$subject_id]['time_spent_correct_answers'] 
                                                                += 0;//$time_spent[$question_record->id];
                                    $time_spent_correct_answer_question[$question_record->id]['time_to_spend'] 
                                                                    = $question_record->time_to_spend;
                                    $time_spent_correct_answer_question[$question_record->id]['time_spent'] 
                                                                    = 0;// $time_spent[$question_record->id];


                                }
                                else
                                {
                                    $wrong_answer_question[] = $question_record->id;
                                    $subject[$subject_id]['wrong_answers'] += 1;
                                    $subject[$subject_id]['time_spent_wrong_answers']    
                                                                += 0;//$time_spent[$question_record->id];
                                    $obtained_marks                   -= $negative_mark;
                                    $obtained_negative_marks          += $negative_mark;
                                    $time_spent_wrong_answer_question[$question_record->id]['time_to_spend'] 
                                                                       = $question_record->time_to_spend;
                                    $time_spent_wrong_answer_question[$question_record->id]['time_spent'] 
                                                                       = 0;//$time_spent[$question_record->id];
                                }
                                
                                break;
                    case (  $question_type == 'para'  || 
                            $question_type == 'audio' || 
                            $question_type == 'video' 
                          ):
                                 $actual_answer = json_decode($actual_answer);
                                 $indidual_marks = $question_record->marks/$question_record->total_correct_answers;
                                $i=0;
                                $flag= 0;
                                foreach($value as $answer_key => $answer_value )
                                {
                                    if($actual_answer[$answer_key]->answer == $answer_value)
                                    {
                                        $flag=1;
                                        $obtained_marks += $indidual_marks;    
                                    }
                                }

                                if($flag)
                                {
                                    $correct_answers++;
                                    $corrent_answer_question[] = $question_record->id;
                                    $subject[$subject_id]['correct_answers'] +=1;
                                    $subject[$subject_id]['time_spent_correct_answers'] 
                                                                += 0;//$time_spent[$question_record->id];
                                    $time_spent_correct_answer_question[$question_record->id]['time_to_spend'] 
                                                                    = $question_record->time_to_spend;
                                    $time_spent_correct_answer_question[$question_record->id]['time_spent'] 
                                                                    = 0;//$time_spent[$question_record->id];

                                }
                                else
                                {
                                    $wrong_answer_question[] = $question_record->id;
                                    $subject[$subject_id]['wrong_answers'] += 1;
                                     $subject[$subject_id]['time_spent_wrong_answers']    
                                                                += 0;//$time_spent[$question_record->id];
                                    $obtained_marks                   -= $negative_mark;
                                    $obtained_negative_marks          += $negative_mark;
                                    $time_spent_wrong_answer_question[$question_record->id]['time_to_spend'] 
                                                                       = $question_record->time_to_spend;
                                    $time_spent_wrong_answer_question[$question_record->id]['time_spent'] 
                                                                       = 0;//$time_spent[$question_record->id];
                                }
                              
                                break;
                case 'match':
                                $actual_answer = json_decode($actual_answer);
                                $indidual_marks = $question_record->marks/$question_record->total_correct_answers;
                                $i=0;
                                 $flag= 0;
                                foreach($actual_answer as $answer)
                                {
                                  if(isset($value[$i]))
                                  {
                                    if($answer->answer == $value[$i])
                                    {
                                  
                                       $flag=1;
                                        $obtained_marks += $indidual_marks;
                                    }
                                  }
                                  $i++;
                                }

                                if($flag)
                                {
                                    $correct_answers++;
                                    $corrent_answer_question[] = $question_record->id;
                                    $subject[$subject_id]['correct_answers'] +=1;
                                    $subject[$subject_id]['time_spent_correct_answers'] 
                                                                += 0;//$time_spent[$question_record->id];
                                    $time_spent_correct_answer_question[$question_record->id]['time_to_spend'] 
                                                                    = $question_record->time_to_spend;
                                    $time_spent_correct_answer_question[$question_record->id]['time_spent'] 
                                                                    = 0;//$time_spent[$question_record->id];

                                }
                                else
                                {
                                    $wrong_answer_question[] = $question_record->id;
                                    $subject[$subject_id]['wrong_answers'] += 1;
                                     $subject[$subject_id]['time_spent_wrong_answers']    
                                                                += 0;//$time_spent[$question_record->id];
                                    $obtained_marks                   -= $negative_mark;
                                    $obtained_negative_marks          += $negative_mark;
                                    $time_spent_wrong_answer_question[$question_record->id]['time_to_spend'] 
                                                                       = $question_record->time_to_spend;
                                    $time_spent_wrong_answer_question[$question_record->id]['time_spent'] 
                                                                       = 0;//$time_spent[$question_record->id];
                                }
                    break;
                
            }

          }
  

        }
        // dd($time_spent_correct_answer_question);
          return array(
                        'total_correct_answers' => $correct_answers,
                        'marks_obtained'        => $obtained_marks,
                        'negative_marks'        => $obtained_negative_marks,
                        'subject_analysis'      => json_encode($subject),
                        'correct_answer_questions' => json_encode($corrent_answer_question),
                        'wrong_answer_questions' => json_encode($wrong_answer_question),
                        'time_spent_correct_answer_questions' => json_encode($time_spent_correct_answer_question),
                        'time_spent_wrong_answer_questions' => json_encode($time_spent_wrong_answer_question),
                        );

    }

    /**
     * Returns the percentage of the number
     * @param  [type] $total [description]
     * @param  [type] $goal  [description]
     * @return [type]        [description]
     */
    public function getPercentage($total, $goal)
    {
        return ($total / $goal) * 100;
    }

    /**
     * Returns the specific question record based on question_id
     * @param  [type] $question_id [description]
     * @return [type]              [description]
     */
    function getQuestionRecord($question_id)
    {
        return \App\QuestionBank::where('id','=',$question_id)->first();
    }



    public function getExamKey($slug)
    {


        $quiz_result         = \App\QuizResult::where('slug',$slug)->first();

        $quiz                = \App\Quiz::where('id',$quiz_result->quiz_id)->first();
        $questions = [];
        $data['status'] = 0;
        $sno = 1;
        if(!$quiz)
        {
          $data['message'] = 'Invalid quiz';
          return $data;
        }


        $user_id = $quiz_result->user_id;

        $user =   $user = \App\User::where('id','=', $user_id)->first();

        if(!$user)
        {
          $data['message'] = 'Invalid UserID';
          return $data;
        }

       
        $time                = $this->convertToHoursMins($quiz->dueration);
       
        

          $prepared_records   = (object) $quiz->prepareQuestions($quiz->getQuestions(),'',$user_id);
         
       
        $data['time_hours']         = makeNumber($time['hours'],2,'0','left');
        $data['time_minutes']       = makeNumber($time['minutes'],2,'0','left');
        $data['time_seconds']       = makeNumber($time['seconds'],2,'0','left');

        $data['quiz']               = $quiz;
        $data['user']               = $user;
        $data['title']              = $quiz->title;



   
        $final_questions             = $prepared_records->questions;
        $final_subjects              = $prepared_records->subjects;
        $data['questions']           = $final_questions;
        $data['subjects']            = $final_subjects;
        $bookmarks                   = array_pluck($final_questions, 'id');
        $data['bookmarks']           = $bookmarks;
        $temp_answers = [];
        



            $submitted_answers = [];

            $answers = (array)json_decode($quiz_result->answers);
            
            foreach ($answers as $key => $value) {

                $submitted_answers[$key] = $value;

            }

            // $response['submitted_answers'] = $submitted_answers;

            // $response['final_questions']   = $final_questions;
            // $response['submit'] = $submitted_answers[598];
            // return $response;

        foreach ($final_questions as $question) {
 
            if(array_key_exists($question->id, $submitted_answers)) {
                
                foreach ($submitted_answers[$question->id] as $user_key => $user_value) {
                     
                if($question->question_type == 'radio'){
                   
                     $question->user_submitted = $user_value;
               
                  }
                elseif($question->question_type == 'para' || $question->question_type == 'audio' || $question->question_type == 'video' || $question->question_type == 'match'){


                      $list = array();

                      for($index = 0; $index < $question->total_correct_answers; $index++)
                      {
                        if (isset($submitted_answers[$question->id][$index]))
                        $list[$index]['answer']   = $submitted_answers[$question->id][$index];
                        // $list[$index]['answer']   = $submitted_answers[$question->id][0];
                      }

                      $question->user_submitted  =  json_encode($list);
                     
                      
                  }

                  elseif($question->question_type == 'checkbox') {

                      $list = array();

                      for($index = 0; $index < $question->total_answers; $index++)
                      {
                        if (isset($submitted_answers[$question->id][$index]))
                        $list[$index]['answer']   = $submitted_answers[$question->id][$index];
                        // $list[$index]['answer']   = $submitted_answers[$question->id][0];
                      }

                      $question->user_submitted  =  json_encode($list);
                  }
                elseif($question->question_type == 'blanks') {


                      $list = array();

                      for($index = 0; $index < $question->total_correct_answers; $index++)
                      {

                        if (isset($submitted_answers[$question->id][$index]))
                        $list[$index]['answer']   = $submitted_answers[$question->id][$index];
                        
                        // $list[$index]['answer']   = $submitted_answers[$question->id][0];
                      }

                      $question->user_submitted  =  json_encode($list);

                      // $question->user_submitted = $user_value;
                  }  
                
                   
                }

            }     
     

            
            if($question->question_type == 'para' || $question->question_type == 'audio' 
              || $question->question_type == 'video'){
             

                 $questions_data  = json_decode($question->answers);
                $ftemp  = [];
                 $t = $this->decodeQuestionsData($questions_data);
                // dd($t);
                foreach($t as $temp ){

                  if(isset($temp['options']))
                  {
                   if(count($temp['options']))
                   {
                     $temp['options'] = $temp['options'][0];
                   }
                  }

                 if(isset($temp['optionsl2']))
                 {
                   if(count($temp['optionsl2']))
                   {
                     $temp['optionsl2'] = $temp['optionsl2'][0];

                   }
                 }
                 // if(count($temp['options']))
                  $ftemp[] = $temp;
                }
                 // if($question->id==67)
                 //  dd(($t));
                  // dd(($temp));
               
                // dd('asdf');

             $question->answers = json_encode($t);
            }

        }

        // $response['final_questions'] = $final_questions;
        // return $response;

         // $data['all_options']  = $all_options;
       
        

       $data['right_bar_data']     = array(
                                              'questions'      => $final_questions, 
                                            
                                              'quiz'           => $quiz, 
                                              'time_hours'     => $data['time_hours'], 
                                              'time_minutes'   => $data['time_minutes'],
                                            
                                              );

        

       
      
        return $data;

    }



     public function instructions($exam_slug)
 {
      
      $instruction_page = '';
      $record = \App\Quiz::where('slug',$exam_slug)->first();

      if(!$record)
      {
         $response['status'] = 0;        
         $response['message'] = 'Exam is not existed';
         return $response;
      }

      if($record->instructions_page_id)
      $instruction_page = \App\Instruction::where('id',$record->instructions_page_id)->first();
      
      $response['instruction_data'] = '';
      
      if($instruction_page){
        $response['instruction_data']  = $instruction_page->content;
        $response['instruction_title'] = $instruction_page->title;
      }

     
    
        $response['status']  = 1;        
        $response['message'] = '';
        // $response['data']    = $response;
         return $response;

      
 }





       public function  saveBookmarks(Request $request) 
      {
          $validator = Validator::make($request->all(), [
              'item_id'       => 'required',
              'user_id'       => 'required'
          ]);

          if ($validator->fails()) {

              $response['status']  = 0;
              $response['message'] = 'Invalid input';
              $response['errors']  = $validator->errors()->messages();
              return $response;
          }

          $user_id        = $request->user_id;

          $user = User::join('role_user', 'users.id', 'role_user.user_id')
                          ->select(['users.*'])
                          ->where('users.id', $user_id)
                          ->where('users.role_id', getRoleData('student'))
                          ->where('users.login_enabled', 1)
                          ->get();

          if (count($user)) {

              $user = $user[0];

              //check bookmark already exist
              $bkmark_existed = \App\Bookmark::where('user_id',$user_id)
                                             ->where('item_id',$request->item_id)
                                             ->get();

              if (count($bkmark_existed)) {

                $bkmark_existed = $bkmark_existed[0];
                $bkmark_existed->delete();
                $response['status'] = 0;
                $response['message'] = 'Bookmark removed ';
                return json_encode($response);

              } else {  

                $record = new \App\Bookmark();

                $record->user_id = $user_id;
                $record->item_id = $request->item_id;
                $record->item_type = 'questions';
                   
                $record->save();
                $response['status'] = 1;
                $response['message'] = 'Bookmark saved ';
                return json_encode($response);
              }
              
          } else {

              $response['status'] = 0;
              $response['message'] = 'Loggedin User record not found';
              return $response;

          }
      }



        /**
   * This method will return the list of lms categories for this student
   * @param  Request $request [description]
   * @return [type]           [description]
   */
  public function examCategories($user_id) {

    $response = array();
    $user = DB::table('users')
              ->where('id','=', $user_id)->first();
 
    $interested_categories = null;

    $interested_categories = null;
 
    if($user->settings) {
      $interested_categories =  json_decode($user->settings)->user_preferences;
    }
        
    if($interested_categories) {
       if(count($interested_categories->quiz_categories))
            $categories  = \App\QuizCategory::whereIn('id',(array) $interested_categories->quiz_categories)
                            ->paginate(getRecordsPerPage());
    }
       
    foreach ($categories as $key => $value) {
      $total_quiz = count($value->quizzes());
      $categories[$key]->total_quiz_count = $total_quiz;
    }

    if(!$categories)
    {
      $response = array(
        'status' => 0,
        'message' => 'No preferences selected'
      );
    } else {
      $response = array(
        'status' => 1,
        'categories' => $categories
      );
    }
    return $response;
  }
  
  public function quizzes() {   

      $quiz_count = $this->hasMany('App\Quiz', 'category_id')
                  ->where('start_date','<=',date('Y-m-d H:i:s'))
                  ->where('end_date','>=',date('Y-m-d H:i:s'))
                  ->where('total_questions','>','0')
                  ->where('applicable_to_specific', '=', 0)
                  ->get();

        return $quiz_count; 
  }


  public function examIndCategories($id)
    {
      $category = \App\QuizCategory::where('id',$id)
                                     ->get();
      
      $response['status'] = 1;
      $response['data'] = $category;
      
      return $response;
    }


    public function subjectAnalysis($user_id)
  {
      $user = \App\User::find($user_id);

      if(!$user)
      {
         $response['status'] = 0;        
         $response['message'] = 'User is not found';
         return $response;
      }

      $records = array();
      $records = ( new \App\QuizResult())->getOverallSubjectsReport($user);
      
      $result=array();
      foreach ($records as $key=>$record) {
          $record['subject_id'] = $key;
          array_push($result, $record);
      }


      if(!$result)
      {
          $response['status'] = 0;        
          $response['message'] = 'No records are not available';
          return $response;
      }
        
      $response['subjects']   = $result;
      $response['user']       = $user;
      $response['status']  = 1;        
      $response['message'] = '';
      
      return $response;        
  }


  public function examAnalysis($user_id)
 {
      
       $user = \App\User::find($user_id);

      if(!$user)
      {
         $response['status'] = 0;        
         $response['message'] = 'User is not found';
         return $response;
      }


       $records = array();

        $records = \App\Quiz::join('quizresults', 'quizzes.id', '=', 'quizresults.quiz_id')
            ->select(['title','is_paid' ,'dueration', 'quizzes.total_marks',  \DB::raw('count(quizresults.user_id) as attempts, quizzes.slug, user_id') ])
            ->where('user_id', '=', $user->id)
            ->where('quizzes.type', '=', 'online')
            ->groupBy('quizresults.quiz_id')
            ->get();
        
        $response['records']   = $records;
        $response['user']       = $user;
        $response['status']  = 1;        
        $response['message'] = '';
        
        return $response;


 }


       public function historyAnalysis($user_id, $exam_id ='')
       {
            $user = \App\User::find($user_id);

            if(!$user)
            {
               $response['status'] = 0;        
               $response['message'] = 'User is not found';
               return $response;
            }

              $exam_record = FALSE;
              if($exam_id)
              {
                $exam_record = \App\Quiz::find($exam_id);
              
              }
               $records = array();
                if(!$exam_id) {

                  $records = \App\Quiz::join('quizresults', 'quizzes.id', '=', 'quizresults.quiz_id')
                  ->select(['title','is_paid' , 'marks_obtained', 'exam_status','quizresults.created_at', 'quizzes.total_marks','quizzes.slug', 'quizresults.slug as resultsslug','user_id','quizresults.quiz_type' ])
                  ->where('user_id', '=', $user->id)
                  ->where('quizzes.type', '=', 'online')
                  // ->where('quizresults.quiz_type','=','online')
                  ->orderBy('quizresults.updated_at', 'desc')
                  ->get();

                } else {

                  $records = \App\Quiz::join('quizresults', 'quizzes.id', '=', 'quizresults.quiz_id')
                  ->select(['title','is_paid' , 'marks_obtained', 'exam_status','quizresults.created_at', 'quizzes.total_marks','quizzes.slug', 'quizresults.slug as resultsslug','user_id','quizresults.quiz_type' ])
                  ->where('user_id', '=', $user->id)
                  ->where('quiz_id', '=', $exam_record->id )
                  ->where('quizzes.type', '=', 'online')
                  // ->where('quizresults.quiz_type','=','online')
                  ->orderBy('quizresults.updated_at', 'desc')
                  ->get();
                }

              $response['records']   = $records;
              $response['user']       = $user;
              $response['status']  = 1;        
              $response['message'] = '';
              
              return $response;
      }


// For Student Purpose
// Fetch Student Subject Books Details

  public function getStudentSubjectBooks($student_id) {

    $coursedetails = DB::table('students')
            ->select('course_parent_id','academic_id', 'course_id')
            ->where('user_id',$student_id)
            ->first();

    if($coursedetails){
            
      $data =  DB::table('course_subject as cousub')
          ->where('course_parent_id',$coursedetails->course_parent_id)
          ->where('course_id',$coursedetails->course_id)
          ->get(['subject_id']);


      $subjectid = [];
        foreach ($data as $value) {
            $subjectid[] = $value->subject_id;
        }

        $records =  DB::table('subject_books as subbo')
                ->join('subjects as subj', 'subbo.subject_id', 'subj.id')
                ->whereIn('subbo.subject_id',$subjectid)
                ->select('subj.subject_title', 'subj.subject_code','subbo.book_name','subbo.description','subbo.file_name','subbo.slug', 'subbo.created_at')
                ->orderby('subbo.updated_at','desc')
                ->paginate(getRecordsPerPage());
    
        if(count($records) > 0){
            $response['status'] = 1;
            $response['data']   = $records;
        }else{
            $response['status'] = 0;
            $response['message'] = 'No Subject Books Available';
        } 

    } else {
          $response['status'] = 0;
          $response['message'] = 'No Subject Books Available';
        }
    return $response;
  }

  public function getScheduledExams($slug) {
   
    $attempts = 0;
    $first_records = array();
    $second_records = array();
    $final_records = array();

    $user_record = prepareStudentSessionRecord($slug);
    $student_record  = $user_record->student;
        
    $first_records = DB::table('generalexamsquizzes')
                        ->join('generalexamsquizapplicability', 'generalexamsquizapplicability.quiz_id', '=', 'generalexamsquizzes.id')
                        ->where('generalexamsquizapplicability.academic_id', '=',$student_record->academic_id)
                        ->where('generalexamsquizapplicability.course_id', '=',$student_record->course_id)
                        ->where('generalexamsquizapplicability.year', '=',$student_record->current_year)
                        ->where('generalexamsquizapplicability.semister', '=',$student_record->current_semister)
                        ->where('applicable_to_specific', '=',0)
                        ->where('total_marks', '!=', 0)
                        ->where('start_date','<=',date('Y-m-d H:i:s'))
                        ->where('end_date','>=',date('Y-m-d H:i:s'))
                        ->select(['generalexamsquizzes.id as quizeid','title', 'dueration','is_paid','attempts', 'total_marks','tags','generalexamsquizzes.slug','generalexamsquizzes.validity','generalexamsquizzes.cost' ])
                        ->orderby('generalexamsquizzes.updated_at','desc')
                        // ->get();
                        ->paginate(getRecordsPerPage());

    $second_records = DB::table('generalexamsquizzes')
                        ->where('applicable_to_specific', '=',0)
                        ->where('total_marks', '!=', 0)
                        ->where('start_date','<=',date('Y-m-d H:i:s'))
                        ->where('end_date','>=',date('Y-m-d H:i:s'))
                        ->orderby('generalexamsquizzes.updated_at','desc')
                        // ->get();
                        ->paginate(getRecordsPerPage());
    
    foreach ($second_records as $key => $value) {
      
        $examhistoryrecord = DB::table('generalexamsquizresults')
                                ->where('quiz_id',$value->id)
                                ->where('user_id',$student_record->user_id)
                                // ->get();
                                ->paginate(getRecordsPerPage());
        
        $attempts = count($examhistoryrecord);
        $second_records[$key]->count_attempts = $attempts;
    }

    $final_records = $second_records->merge($first_records);
    
    if(count($final_records) > 0){
      $response = array(
        "status" => '1',
        "data" => $final_records
      );
    }else{
      $response = array(
        "status" => '0',
        "message" => 'No recordes available'
      );
    } 
    return $response;

  }


  public function getGeneralExamAnalysis($slug) {
    
      $user_record = prepareStudentSessionRecord($slug);

      $student_record  = $user_record->student;
      $final_records = array();

      $final_records = DB::table('generalexamsquizzes')
                        ->join('generalexamsquizresults', 'generalexamsquizzes.id', '=', 'generalexamsquizresults.quiz_id')
                        ->select(['generalexamsquizzes.title','dueration', 'generalexamsquizzes.total_marks',  \DB::raw('count(generalexamsquizresults.user_id) as attempts, generalexamsquizzes.slug, user_id'),
                        'generalexamsquizresults.exam_status' ])
                        ->where('user_id', '=', $student_record->user_id)
                        ->groupBy('generalexamsquizresults.quiz_id')
                        ->orderby('generalexamsquizzes.updated_at','desc')
                        ->paginate(getRecordsPerPage());


      if(count($final_records) > 0){
        $response = array(
          "status" => '1',
          "data" => $final_records
        );
      }else{
        $response = array(
          "status" => '0',
          "message" => 'No recordes available'
        );
      } 
      return $response;
  }

  public function getExamAttemptsRecords($slug, $exam_slug = '')
  {

      $final_records = array();
      $user_record = prepareStudentSessionRecord($slug);
      $student_record  = $user_record->student;

      $exam_record = FALSE;
      if($exam_slug) {
        $exam_record = DB::table('generalexamsquizzes')
                            ->where('slug', '=', $exam_slug)->first();
      
      }
        
      if(!$exam_slug) {
        $final_records = DB::table('generalexamsquizzes') 
                        ->join('generalexamsquizresults', 'generalexamsquizzes.id', '=', 'generalexamsquizresults.quiz_id')
                        ->select(['title', 'marks_obtained', 'exam_status','generalexamsquizresults.created_at', 'generalexamsquizzes.total_marks','generalexamsquizzes.slug', 'generalexamsquizresults.slug as resultsslug','user_id',
                                  'generalexamsquizresults.percentage', 'generalexamsquizresults.updated_at'])
                        ->where('user_id', '=', $student_record->user_id)
                        ->orderBy('generalexamsquizresults.updated_at', 'desc')
                        ->get();
        $marks = DB::table('generalexamsquizresults')
                        ->where('user_id', '=', $student_record->user_id)
                        ->orderBy('updated_at','desc')
                        ->get();
        } else {
        $final_records = DB::table('generalexamsquizzes')
                        ->join('generalexamsquizresults', 'generalexamsquizzes.id', '=', 'generalexamsquizresults.quiz_id')
                        ->select(['title', 'marks_obtained', 'exam_status','generalexamsquizresults.created_at', 'generalexamsquizzes.total_marks','generalexamsquizzes.slug', 'generalexamsquizresults.slug as resultsslug','user_id',
                        'generalexamsquizresults.percentage', 'generalexamsquizresults.updated_at' ])
                        ->where('user_id', '=', $student_record->user_id)
                        ->where('quiz_id', '=', $exam_record->id )
                        ->orderBy('generalexamsquizresults.updated_at', 'desc')
                        ->get();
        $marks = DB::table('generalexamsquizresults')
                      ->where('user_id', '=', $student_record->user_id)
                      ->where('quiz_id', '=', $exam_record->id)
                      ->orderBy('updated_at','desc')
                      ->get();
        }

        if(count($final_records) > 0){
            $response = array(
              "status" => '1',
              "data" => $final_records,
              "chart_response" => $marks
            );
        } else {
            $response = array(
              "status" => '0',
              "message" => 'No recordes available'
            );
        } 
        return $response;

    }
} 