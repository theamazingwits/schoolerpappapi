<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use \App;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\LogHelper;
use Yajra\Datatables\Datatables;
use DB;
use AWS;
use Auth;
use Carbon\Carbon;
use Exception;
use Response;
use App\ServiceProvider;

class MeetingController extends Controller
{ 
  private $ServiceProvider;

  private $json_val;

  public function __construct()
  {
        if(!isset(self::$this->ServiceProvider)){
          $this->ServiceProvider = new ServiceProvider();
      }

      $path = 'public/NotifyMessage.json';
      $this->json_val = json_decode(file_get_contents($path), true);
  }

     /**
     * This method adds Class Notes to DB
     * @param  Request $request [Request Object]
     * @return void
     */
  public function store(Request $request)
  {

      $table = array(
                      'staff_id'  => $request->staff_id,
                      'meeting_title'   => $request->meeting_title,
                      'meeting_date'   => $request->meeting_date,
                      'acedemic_year'   => $request->acedemic_year,
                      'course'   => $request->course,
                      'class'   => $request->class,
                      'description'   => $request->description,
                      'meeting_room_link'   => $request->meeting_room_link,
                      'meeting_status' => $request->meeting_status,
                      'created_at' => date('Y-m-d H:i:s'),
                      'updated_at' => date('Y-m-d H:i:s')
                  );

        DB::table('meeting')
        ->insert($table);

        $student = DB::table('students')
                  ->where('course_parent_id',$request->course)
                  ->where('academic_id', $$request->acedemic_year)
                  ->where('course_id', $request->class)
                  ->select('user_id')
                  ->first();

            $notificationusers = DB::table('students as stud')
                                ->leftjoin('users as use','stud.user_id','use.id')
                                ->where('stud.academic_id',$request->acedemic_year)
                                ->where('stud.course_parent_id',$request->course)
                                ->where('stud.course_id',$request->class)
                                ->select(['use.push_web_id','use.push_app_id','use.os_type','use.name','use.id','use.app_type'])
                                ->get();

                $aptyp = DB::table('users as usr')
                          ->join('students as stud', 'stud.user_id', 'usr.id')
                          ->first(['app_type']);

            $this->ServiceProvider->data = $notificationusers;
            $this->ServiceProvider->aptyp = $aptyp->app_type;
            $this->ServiceProvider->message = $this->json_val['NOWmsg0012A'].$student->user_id.$this->json_val['NOWmsg0012B'];
            $this->ServiceProvider->pageId = "UNKNOWN";
            $this->ServiceProvider->insertNotification($this->ServiceProvider);

        $response = array(
                'message' => 'Meeting Added successfully !!',
                'status' => 1
            );  
      return $response;
  }
  public function getStudentMeeting($student_id)
  {
      $records = array();

      $student_det = DB::table('students')
                    ->where('user_id',$student_id)
                    ->select('academic_id','course_parent_id','course_id')
                    ->first();

      $records = DB::table('meeting as meet')
                    ->join('academics as acad', 'acad.id','meet.acedemic_year')
                    ->join('courses as cour', 'cour.id','meet.class')
                    ->where('meet.meeting_status','0')
                    ->where('meet.acedemic_year',$student_det->academic_id)
                    ->where('meet.course',$student_det->course_parent_id)
                    ->where('meet.class',$student_det->course_id)
                    ->select(['meet.meeting_title', 'meet.meeting_date', 'acad.academic_year_title', 'cour.course_title','meet.meeting_room_link'])
                    ->orderBy('meet.updated_at', 'desc')
                    ->paginate(getRecordsPerPage());
  
        $response = array(
          "status" => '1',
          "data" => $records
        );  
      return $response;
  }
  
  public function getStaffMeeting($staff_id)
  {
      $staff_meeting_info = DB::table('meeting as meet')
                            ->join('academics as acad', 'acad.id', 'meet.acedemic_year')
                            ->join('courses as cour', 'meet.course', 'cour.parent_id')  
                            ->where('staff_id', $staff_id)
                            ->orderBy('meet.updated_at', 'desc')
                            ->select(['meeting_title','meeting_date', 'meeting_room_link','acad.academic_year_title', 'cour.course_title'])
                            ->paginate(getRecordsPerPage());
      $response['data']   = $staff_meeting_info;
      return $response; 
    }
    public function getCourseIdbasedClass($academic_id)
  {
      $getCourses = DB::table('courses')
                    ->where('parent_id',0)
                    ->select('course_title', 'id')
                    ->get();
        $response['status']= "1";
        $response['data'] = $getCourses;
      return $response;

  }
  public function getClassSection($parent_id)
  {
    $getClass = DB::table('courses')
                ->where('parent_id', $parent_id)
                ->select('course_title','id')
                ->get();
        $response['status']= "1";
        $response['data'] = $getClass;
      return $response;
  }
}