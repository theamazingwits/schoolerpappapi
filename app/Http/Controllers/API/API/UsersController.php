<?php

namespace App\Http\Controllers\API;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Image;
use ImageSettings;


class UsersController extends Controller
{
    
     public function profile(Request $request, $id)
   {	
   		$response['status'] = 0;
   		$response['message'] = '';

   		$user             = \App\User::where('id', '=', $id)->first();
      $role = $user->role_id;

      if($role == 3){

        $staff = \App\Staff::where('user_id', '=', $id)->first();
        
        $data['id'] = $user->id;
        $data['name'] = $user->name;
        $data['username'] = $user->username;
        $data['email']        = $user->email;
        $data['address']      = $user->address;
        $data['image']        = $user->image;
        $data['phone']        = $user->phone;

        $response['status'] = 1;
        $response['user'] = $data;

        return $response;

      }else{

      $student          = \App\Student::where('user_id', '=', $id)->first();
      $academic_details = \App\Academic::where('id',$student->academic_id)->first();
      $course_details   = \App\Course::where('id',$student->course_id)->first();
      $year             = $student->current_year;
      $semister         = $student->current_semister;

        if($course_details->course_dueration>1 && $course_details->is_having_semister==1){
         $data['class_title']     = $academic_details->academic_year_title.' '.$course_details->course_title.' '.$year.' '.'year'.' '.$semister.' '.'semester ';
         }
        elseif ($course_details->course_dueration>1 && $course_details->is_having_semister==0) {
         $data['class_title']     = $academic_details->academic_year_title.' '.$course_details->course_title.' '.$year.' '.'year';
        }
        else{
          $data['class_title']     = $academic_details->academic_year_title.' '.$course_details->course_title;
        }


   		if(!$user)
   		{
   			$response['message'] = 'Invalid Userid';
   			return $response;
   		}

      $data['id']           = $user->id;
      $data['name']         = $user->name;
      $data['username']     = $user->username;
      $data['email']        = $user->email;
      $data['slug']         = $user->slug;
      $data['role_id']      = $user->role_id;
      $data['status']       = $user->status;
      $data['parent_id']    = $user->parent_id;
      $data['image']        = $user->image;
      $data['phone']        = $user->phone;
      $data['address']      = $user->address;
      $data['subscription_ends_at']   = $user->subscription_ends_at;
      $data['settings']     = $user->settings;
      $data['created_at']   = $user->created_at;
      $data['updated_at']   = $user->updated_at;
      $data['admission_no'] = $student->admission_no;
      $data['roll_no']      = $student->roll_no;

   		$response['status'] = 1;
   		$response['user'] = $data;

   		return $response;
    }
   } 


    public function update(Request $request, $id)
     {  

        $record     = \App\User::where('id', $id)->first();

      if(!$record)
      {
        $response['message'] = 'Invalid Userid';
        return $response;
      }
        $columns = [
        'name'      => 'bail|required|max:20|',
        'phone'     => 'bail|required|max:10',
        // 'image'     => 'bail|mimes:png,jpg,jpeg|max:2048',

        ];

          
          $response['status'] = 0;        
          $response['message'] = '';

         $validated =  \Validator::make($request->all(),$columns);
            if(count($validated->errors()))
            {
                $response['status'] = 0;        
                $response['message'] = 'Validation Errors';
                $response['errors'] = $validated->errors()->messages();
                return $response;
            }    


        $name = $request->name;
        if($name != $record->name)
            $record->slug = $record::makeSlug($name);

        $record->name = $name;
        

       $record->phone = $request->phone;
       $record->address = $request->address;
       $record->save();

       if ($record->role_id==3) {
            $staff   = \App\Staff::where('user_id', $record->id)->first();
            if (!empty($staff)) {
              $staff->first_name   = $name;
              $staff->save();
            }
       }
       
         $response['status'] = 1;        
         $response['message'] = 'Record updated successfully';
         return $response;
      }


        protected function processUpload(Request $request, User $user)
     {

       
         if ($request->hasFile('image')) {
          
          $imageObject = new ImageSettings();
          
          $destinationPath      = $imageObject->getProfilePicsPath();
          $destinationPathThumb = $imageObject->getProfilePicsThumbnailpath();
          
          $fileName = $user->id.'.'.$request->image->guessClientExtension();
          ;
          $request->file('image')->move($destinationPath, $fileName);
          $user->image = $fileName;
         
          Image::make($destinationPath.$fileName)->fit($imageObject->getProfilePicSize())->save($destinationPath.$fileName);
         
          Image::make($destinationPath.$fileName)->fit($imageObject->getThumbnailSize())->save($destinationPathThumb.$fileName);
          $user->save();
        }
     }


     /**
    * This methos will return the list of settings available and the user selected settings
    */

   public function settings($user_id)
   {
   	  $response['status'] = 0;
   		$response['message'] = '';
   		$user = \App\User::where('id', '=', $user_id)->first();
   		if(!$user)
   		{
   			$response['message'] = 'Invalid Userid';
   			return $response;
   		}

   		$response['quiz_categories']   = \App\QuizCategory::get();
        $response['lms_category']      = \App\LmsCategory::get();

        $response['selected_options'] = [
                                        'quiz_categories'=>[],
                                        'lms_categories'=>[],
                                        ];
        if(isset($user->settings))
          $response['selected_options'] = json_decode($user->settings)->user_preferences;
        $response['status'] = 1;
        return $response;


   }


        /**
      * [uploadUserProfileImage description]
      * @param  Request $request [description]
      * @return [type]           [description]
      */
    public function uploadUserProfileImage(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'user_id'       => 'required',
            'image'         => 'required'
        ]);

        if ($validator->fails()) {

            $response['status']  = 0;
            $response['message'] = 'Invalid input';
            $response['errors']  = $validator->errors()->messages();
            return $response;
        }

        $user_id        = $request->user_id;
        //echo $user_id;
        $user = User::join('role_user', 'users.id', 'role_user.user_id')
                        ->select(['users.*'])
                        ->where('users.id', $user_id)
                        ->where('users.login_enabled', 1)
                        ->get();

        // $record     = \App\User::where('id', $id)->first();

        if (count($user)) {
          
            $user = $user[0];

            if ($user->role_id==6 || $user->role_id==5 || $user->role_id==3) {

              $previous_image = $user->image;

               $base64_string = $request->input('image');  
               $base64_string = str_replace('data:image/jpeg;base64,', '', $base64_string);
               $base64_string = str_replace(' ', '+', $base64_string);

               $img_name = 'profile_image_'.time().'.'.'jpeg';
              
              //orginial image
              file_put_contents('public/uploads/users/'.$img_name, base64_decode($base64_string));
              $user->image      = $img_name;
              $user->save();

              //echo $base64_string;
              //thumb
              Image::make(IMAGE_PATH_PROFILE.$img_name)->fit(100,100)->save('public/uploads/users/thumbnail/'.$img_name);

              $image_path =   public_path("uploads/users/".$previous_image);
                  
              if ($previous_image && file_exists($image_path)) {
                  //File::delete($image_path);
                  unlink($image_path);

                  $image_thumbpath = public_path('uploads/users/thumbnail/'.$previous_image);

                  if (file_exists($image_thumbpath)) {
                      unlink($image_thumbpath);
                  }
              }

              $response['status']  = 1;
              $response['message'] = 'Profile Image uploaded successfully..';
              $response['image_name'] = $img_name;
              return $response;

          } else {

              $response['status']  = 0;
              $response['message'] = 'Unathenticated user';
              return $response;
          }

        } else {

            $response['status'] = 0;
            $response['message'] = 'Loggedin User record not found';
            return $response;
        }
    }


    public function updateSettings(Request $request)
    {
      // dump('you boy');
      // dd($request->quiz_categories);
         $record = \App\User::where('id', $request->user_id)->first();
   
       // dd($record);
  
       $UserOwnAccount = FALSE;
       if($request->user_id == $record->id)
        $UserOwnAccount = TRUE;
      
        if(!$UserOwnAccount)  {
          $current_user_role = getRoleData($record->role_id);

          if((($current_user_role=='admin' || $current_user_role == 'owner') ))
          {
            if(!checkRole(getUserGrade(1))) {
              prepareBlockUserMessage();
              return back();
            }
          }
        }

        $options = [];
        if($record->settings)
        {
          $options =(array) json_decode($record->settings)->user_preferences;
          
        }
        $tquiz_categories = json_decode($request->quiz_categories);
        $tlms_categories = json_decode($request->lms_categories);
    
    
        // dd($tquiz_categories);
        $options['quiz_categories'] = [];
        $options['lms_categories']  = [];
       
     
        if($request->has('quiz_categories')) {
        foreach($tquiz_categories as $key => $value)
          $options['quiz_categories'][] = $value;
        }
        if($request->has('lms_categories')) {
          foreach($tlms_categories as $key => $value)
            $options['lms_categories'][] = $value;
        }
    
        $record->settings = json_encode(array('user_preferences'=>$options));
        
        $record->save();

        $response['status'] = 1;        
        $response['message'] = 'settings are updated successfully';
        return $response;

    }



    public function parentProfile(Request $request, $id)
    {  

      $response['status']  = 0;
      $response['message'] = '';

      $user             = \App\User::where('id', '=', $id)->first();
      // $student          = \App\Student::where('user_id', '=', $id)->first();
      
      // $academic_details = \App\Academic::where('id',$student->academic_id)->first();
      // $course_details   = \App\Course::where('id',$student->course_id)->first();
      // $year             = $student->current_year;
      // $semister         = $student->current_semister;

      /*if ($course_details->course_dueration>1 && $course_details->is_having_semister==1) {
       $data['class_title']     = $academic_details->academic_year_title.' '.$course_details->course_title.' '.$year.' '.'year'.' '.$semister.' '.'semester ';
      }
      elseif ($course_details->course_dueration>1 && $course_details->is_having_semister==0) {
       $data['class_title']     = $academic_details->academic_year_title.' '.$course_details->course_title.' '.$year.' '.'year';
      }
      else {
        $data['class_title']     = $academic_details->academic_year_title.' '.$course_details->course_title;
      }*/


      if(!$user)
      {
        $response['message'] = 'Invalid Userid';
        return $response;
      }

      $data['id']           = $user->id;
      $data['name']         = $user->name;
      $data['username']     = $user->username;
      $data['email']        = $user->email;
      $data['slug']         = $user->slug;
      $data['role_id']      = $user->role_id;
      $data['status']       = $user->status;
      // $data['parent_id']    = $user->parent_id;
      $data['image']        = $user->image;
      $data['phone']        = $user->phone;
      $data['address']      = $user->address;
      // $data['subscription_ends_at']   = $user->subscription_ends_at;
      $data['settings']     = $user->settings;
      $data['created_at']   = $user->created_at;
      $data['updated_at']   = $user->updated_at;
      // $data['admission_no'] = $student->admission_no;
      // $data['roll_no']      = $student->roll_no;

      $response['status'] = 1;
      $response['user'] = $data;

      return $response;
   } 



    public function staffProfile(Request $request, $id)
    {  

      $response['status']  = 0;
      $response['message'] = '';

      $user             = \App\User::where('id', '=', $id)->first();
      // $student          = \App\Student::where('user_id', '=', $id)->first();
      
      // $academic_details = \App\Academic::where('id',$student->academic_id)->first();
      // $course_details   = \App\Course::where('id',$student->course_id)->first();
      // $year             = $student->current_year;
      // $semister         = $student->current_semister;

      /*if ($course_details->course_dueration>1 && $course_details->is_having_semister==1) {
       $data['class_title']     = $academic_details->academic_year_title.' '.$course_details->course_title.' '.$year.' '.'year'.' '.$semister.' '.'semester ';
      }
      elseif ($course_details->course_dueration>1 && $course_details->is_having_semister==0) {
       $data['class_title']     = $academic_details->academic_year_title.' '.$course_details->course_title.' '.$year.' '.'year';
      }
      else {
        $data['class_title']     = $academic_details->academic_year_title.' '.$course_details->course_title;
      }*/


      if(!$user)
      {
        $response['message'] = 'Invalid Userid';
        return $response;
      }

      $data['id']           = $user->id;
      $data['name']         = $user->name;
      $data['username']     = $user->username;
      $data['email']        = $user->email;
      $data['slug']         = $user->slug;
      $data['role_id']      = $user->role_id;
      $data['status']       = $user->status;
      // $data['parent_id']    = $user->parent_id;
      $data['image']        = $user->image;
      $data['phone']        = $user->phone;
      $data['address']      = $user->address;
      // $data['subscription_ends_at']   = $user->subscription_ends_at;
      // $data['settings']     = $user->settings;
      $data['created_at']   = $user->created_at;
      $data['updated_at']   = $user->updated_at;
      // $data['admission_no'] = $student->admission_no;
      // $data['roll_no']      = $student->roll_no;

      $response['status'] = 1;
      $response['user'] = $data;

      return $response;
   } 


}