<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\User;
use DB;
use Auth;
use Exception;
use Response;
use App\ServiceProvider;

class UserAssignmentsController extends Controller
{
  private $ServiceProvider;

  private $json_val;

  public function __construct()
  {
        if(!isset(self::$this->ServiceProvider)){
          $this->ServiceProvider = new ServiceProvider();
      }

      $path = 'public/NotifyMessage.json';
      $this->json_val = json_decode(file_get_contents($path), true);
  }

     /**
     * This method adds Class Notes to DB
     * @param  Request $request [Request Object]
     * @return void
     */
    public function getAssignments($user_id)
    {
        
      
     
        $user     = \App\User::where('id', $user_id)->first();

        $student  = \App\Student::where('user_id',$user->id)->first();

        $records = array();
        
        $records = \App\Assignments::join('assignments_allocate','assignments_allocate.assignment_id','=','assignments.id')
                                      ->join('users','users.id','=','assignments.user_id')
                                      ->join('subjects','subjects.id','=','assignments.subject_id')
                                      ->where('academic_id',$student->academic_id)
                                      ->where('course_parent_id',$student->course_parent_id)
                                      ->where('course_id',$student->course_id)
                                      ->where('year',$student->current_year)
                                      ->where('semister',$student->current_semister)
                                      ->select(['assignments.title','subject_title','users.name','deadline','assignments_allocate.id as allocate_id','assignments_allocate.is_submitted','assignments_allocate.is_approved'])
                                      ->where('assignments_allocate.student_id',$user->id)
                                      ->orderby('assignments_allocate.updated_at','desc')
                                      // ->get();
                                      ->paginate(getRecordsPerPage());

        
        if( count( $records ) > 0 ){

            $response['data']    = $records;
            $response['status']  = 1;
        }else{

            $response['message']  = 'No assignments available';
            $response['status']   = 0;
        }                                
        

        return $response;
    }


     public function viewAssignment($allocate_id)
    {
      // dd($allocate_id);

        

        $details  = \App\Assignments::join('assignments_allocate','assignments_allocate.assignment_id','=','assignments.id')
                                      ->join('users','users.id','=','assignments.user_id')
                                      ->join('subjects','subjects.id','=','assignments.subject_id')
                                      ->where('assignments_allocate.id',$allocate_id)
                                      ->select(['assignments.title','subject_title','users.name','deadline','assignments_allocate.id as allocate_id','file_name','assignments.description','assignments.slug','assignments_allocate.user_file'])
                                      ->first();
       


       
        if( $details  ){

            $response['data']    = $details;
            $response['status']  = 1;
        }else{

            $response['message']  = 'No data available';
            $response['status']   = 0;
        }                                
        

        return $response;
        
    }


    public function uploadAssignment(Request $request)
    {
        $record      = \App\AssignmentAllocate::find($request->allocate_id);
        $assignment  = \App\Assignments::find($record->assignment_id);
        $user        = \App\User::find($assignment->user_id);
        $student     = \App\User::find($record->student_id);

        // dd($user);
        // $file_upload  =  $this->processUpload($request, $record,'catimage');

        $base64_string = base64_decode($request->input('assignment'));
        $assignment_name = $request->allocate_id.'_'.$assignment->user_id.'_'.time().'.'.'pdf';
         
        file_put_contents('public/uploads/assignments/'.$assignment_name, $base64_string);


        $record->user_file    =  $assignment_name;
        $record->is_submitted = 1;
        $record->save();

        /*$stud = DB::table('students')
                ->where('user_id', $record->student_id)
                ->select('id','course_id', 'course_parent_id','first_name')
                ->first();*/

        $notificationusers = DB::table('assignments as ass')
                ->join('users as use','ass.user_id','use.id')
                ->where('ass.id', $record->assignment_id)
                ->select(['use.push_web_id','use.push_app_id','use.os_type','use.name','ass.id','use.app_type'])
                ->get();


        $aptyp = DB::table('assignments as ass')
                ->join('users as use','ass.user_id','use.id')
                ->where('ass.id', $record->assignment_id)
                ->first(['use.app_type']);

        $this->ServiceProvider->data = $notificationusers;
        $this->ServiceProvider->aptyp = $aptyp;
        $this->ServiceProvider->message = $this->json_val['NOWmsg0010A'].$student->first_name.$this->json_val['NOWmsg0015A'];
        $this->ServiceProvider->pageId = "UNKNOWN";
        $this->ServiceProvider->insertNotification($this->ServiceProvider);
        
        $response['message']  = 'Assignment uploaded';
        $response['status']   = 1;
 
        return $response;

    }


     /* public function processUpload(Request $request, $record, $file_name)
     {
       
         if ($request->hasFile($file_name)) {

          $destinationPath  = "public/uploads/assignments/";
          
          $fileName = $record->id.'-'.$request->$file_name->getClientOriginalName();
          // dd($fileName);
          
          $request->file($file_name)->move($destinationPath, $fileName);

          $record->user_file  =  $fileName;

          $record->save();
           
          return TRUE; 
      
        }else{

            return FALSE;
        }

     }*/

     
}