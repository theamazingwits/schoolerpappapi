<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App;
use App\Http\Requests;
use DB;
use Auth;
use Exception;
use Softon\Indipay\Facades\Indipay;  
use Carbon;
use App\Paypal;
use Input;
use App\GeneralSettings as Settings;

class FeePayStudentController extends Controller
{
   
   
   /**
  This method save fee pay data in feepayments_online table
  with pending status
  **/
    public function preserveBeforeSave($gateway, $amount, $user_id, $student_id , $fee_cat_id,$other_details)
    {
        $record                  = new \App\FeePaymentsOnline();
        $record->slug            = $record->makeSlug(getHashCode());
        $record->user_id         = $user_id;
        $record->feecategory_id  = $fee_cat_id;
        $record->plan_type       = 'fee';
        $record->payment_gateway = $gateway;
        $record->paid_by         = $user_id;
        $record->paid_amount     = $amount;
        $record->payment_status  = PAYMENT_STATUS_PENDING;
        $record->other_details   = $other_details;

        $record->save();
        return $record->slug;

    }
    

   
   


    public function updateFeePayment(Request $request)
    {    

          $response['status'] = 0;
          $response['message'] = 'Oops..! something went wrong';

          DB::beginTransaction();

          try {

            


              $payment_record                  = new \App\FeePaymentsOnline();
              $payment_record->slug            = $payment_record->makeSlug(getHashCode());
              $payment_record->user_id         = $request->user_id;
              $payment_record->feecategory_id  = $request->fee_cat_id;
              $payment_record->plan_type       = 'fee';
              $payment_record->payment_gateway = $request->gateway;
              $payment_record->paid_by         = $request->user_id;
              $payment_record->paid_amount     = $request->amount;
              $payment_record->payment_status  = $request->payment_status;
              $payment_record->transaction_id  = $request->transaction_id;
              $payment_record->save();

             

              $my_paymentlug   = $payment_record->slug;


              $user_id          = $payment_record->user_id;
              $studentdata      = \App\Student::where('user_id','=',$user_id)->first();
              $student_id       = $studentdata->id;
              $category_id      = $payment_record->feecategory_id;
              $paid_amount      = $payment_record->paid_amount;
              $discount_amount  = 0;
              $payment_mode_name   = $payment_record->payment_gateway;
                
              
              if($payment_record->payment_gateway=='payu' || $payment_record->payment_gateway=='paypal' ){
                $payment_mode     = 'online';
              }
              else{
                $payment_mode     = 'offline';
              }
              $notes            = $payment_record->notes;

              $total_amount     = $paid_amount+$discount_amount;//afterdiscount amount + discount amount
              // dd($total_amount);
              
             



              $this->remaining_amount = $paid_amount;
              $this->paid_percentage  = 100;
              $other_payments   = [];
              $schedule_payments= [];
              $feepayment = null;



               //Get information
              $payment_details = (object)\App\FeeParticularPayment::getFeePaymentParticulars($student_id, $category_id);

             

              //Access the last record in the payment details which contains all payments need to be paid
              $payment_records = (object)end($payment_details->payment_details);
              $payment_records = $payment_records->payment_record;
              // dd($payment_records);
                  
                foreach ($payment_records as $record) {
                  if($record->is_schedule)
                    $schedule_payments[] = $record;
                  else
                    $other_payments[] = $record;
                }



                $previous_fee= [];
                //Access The Previous Unpaid Fee particulars
                if($payment_details->previous_particulars!=null) {
                  $previous_fee_particulars = $payment_details->previous_particulars;  
                  foreach ($previous_fee_particulars as $mydata) {
                    $previous_fee[]  = $mydata;
                  }
                }


                $feepayment = $this->insertFeePaymentRecord($student_id, $category_id, $paid_amount,$payment_mode,$notes,$discount_amount, $my_paymentlug,$payment_mode_name);

               
                $feepayment_id = $feepayment->id;

                if($payment_details->previous_particulars!=null){
                  $this->updatePaymentRecords($previous_fee, $feepayment_id,$total_amount,$discount_amount);
                }//previous fee


              $this->updatePaymentRecords($other_payments, $feepayment_id,$total_amount,$discount_amount);//current non-term fee
              $this->updatePaymentRecords($schedule_payments, $feepayment_id,$total_amount,$discount_amount);// current term-fee
          
              DB::commit();

              $response['status'] = 1;
              $response['message'] = 'Payment updated successfully';
         }
         catch(\Exception $ex)
         {      
               DB::rollBack();
               $response['status'] = 0;
               $response['message'] = $ex->getMessage();
         }

        return $response;
    }



        /**
     * Common method to handle success payments and updates in feepaymets_online 
     table and insert a new record in feepayments table after that update
     the records in feeparticular_paymets table
     * @return [type] [description]
     */
    protected function paymentSuccess(Request $request)
    {

      $params = explode('?token=',$_SERVER['REQUEST_URI']) ;
    
     if(!count($params))
      return FALSE;
    
      $slug = $params[1];

      $payment_record = \App\FeePaymentsOnline::where('slug', '=', $slug)->first();

      if($this->processPaymentRecord($payment_record))
      {
        $payment_record->payment_status = PAYMENT_STATUS_SUCCESS;
        $payment_record->save();
        $my_paymentlug   = $payment_record->slug;

        $user_id          = $payment_record->user_id;
        $studentdata      = \App\Student::where('user_id','=',$user_id)->first();
        $student_id       = $studentdata->id;
        $category_id      = $payment_record->feecategory_id;
        $paid_amount      = $payment_record->paid_amount;
        $discount_amount  = 0;
        $payment_mode_name   = $payment_record->payment_gateway;
      
       
        if($payment_record->payment_gateway=='payu' || $payment_record->payment_gateway=='paypal' ){
          $payment_mode     = 'online';
        }
        else{
          $payment_mode     = 'offline';
        }
        $notes            = $payment_record->notes;

        $total_amount     = $paid_amount+$discount_amount;//afterdiscount amount + discount amount
        // dd($total_amount);

        $this->remaining_amount = $paid_amount;
        $this->paid_percentage  = 100;
        $other_payments   = [];
        $schedule_payments= [];
        $feepayment = null;
         //Get information
        $payment_details = (object)\App\FeeParticularPayment::getFeePaymentParticulars($student_id, $category_id);

        //Access the last record in the payment details which contains all payments need to be paid
        $payment_records = (object)end($payment_details->payment_details);
        $payment_records = $payment_records->payment_record;
        // dd($payment_records);
        
        foreach ($payment_records as $record) {
          if($record->is_schedule)
            $schedule_payments[] = $record;
          else
            $other_payments[] = $record;
        }

        //Access The Previous Unpaid Fee particulars
        if($payment_details->previous_particulars!=null){
        $previous_fee_particulars = $payment_details->previous_particulars;  
        foreach ($previous_fee_particulars as $mydata) {
          $previous_fee[]  = $mydata;
        }
      }
        // $feepayment_id = 0;
         DB::beginTransaction();
        try{

          $feepayment = $this->insertFeePaymentRecord($student_id, $category_id, $paid_amount,$payment_mode,$notes,$discount_amount, $my_paymentlug,$payment_mode_name);
          $feepayment_id = $feepayment->id;
          if($payment_details->previous_particulars!=null){
          $this->updatePaymentRecords($previous_fee, $feepayment_id,$total_amount,$discount_amount);
          }//previous fee

          $this->updatePaymentRecords($other_payments, $feepayment_id,$total_amount,$discount_amount);//current non-term fee
          $this->updatePaymentRecords($schedule_payments, $feepayment_id,$total_amount,$discount_amount);// current term-fee
          
          DB::commit();
          

          }
        catch(Exception $ex)
        {
          DB::rollBack();
          // dd($ex->getMessage());
           if(getSetting('show_foreign_key_constraint','module'))
           {

              flash('oops...!',$ex->getMessage(), 'error');
           }
           else {
            flash('oops...!',$ex->getMessage(), 'error');
              // flash('oops...!','improper_data', 'error');
            }
          return back();
        }
       

       
        if($payment_record->payment_gateway=='paypal') {
          $payment_record->paid_amount    = $request->mc_gross;
          $payment_record->transaction_id = $request->txn_id;
          $payment_record->paid_by        = $request->payer_email;
        }

        //Capcture all the response from the payment.
        //In case want to view total details, we can fetch this record
        $payment_record->transaction_record = json_encode($request->request->all());
        
        $payment_record->save();

       


        return TRUE;
      }
      return FALSE;
    }



      /**
     * This method Process the payment record by validating through 
     * the payment status and the age of the record and returns boolen value
     * @param  Payment $payment_record [description]
     * @return [type]                  [description]
     */
    protected  function processPaymentRecord(\App\FeePaymentsOnline $payment_record)
    {
      if(!$this->isValidPaymentRecord($payment_record))
      {
        flash('Oops','invalid_record', 'error');
        return FALSE;
      }

      if($this->isExpired($payment_record))
      {
        flash('Oops','time_out', 'error');
        return FALSE;
      }

      return TRUE;
    }


    /**
     * This method validates the FeePaymentsOnline record before update the FeePaymentsOnline status
     * @param  [type]  $payment_record [description]
     * @return boolean                 [description]
     */
    protected function isValidPaymentRecord(\App\FeePaymentsOnline $payment_record)
    {
      $valid = FALSE;
      if($payment_record)
      {
        if($payment_record->payment_status == PAYMENT_STATUS_PENDING || $payment_record->payment_gateway=='offline')
          $valid = TRUE;
      }
      return $valid;
    }


      /**
     * This method checks the age of the FeePaymentsOnline record
     * If the age is > than MAX TIME SPECIFIED (30 MINS), it will update the record to aborted state
     * @param  FeePaymentsOnline $payment_record [description]
     * @return boolean                 [description]
     */
    protected function isExpired(\App\FeePaymentsOnline $payment_record)
    {

      $is_expired = FALSE;
      $to_time = strtotime(Carbon\Carbon::now());
    $from_time = strtotime($payment_record->updated_at);
    $difference_time = round(abs($to_time - $from_time) / 60,2);

    if($difference_time > PAYMENT_RECORD_MAXTIME)
    {
      $payment_record->payment_status = PAYMENT_STATUS_CANCELLED;
      $payment_record->save();
      return $is_expired =  TRUE;
    }
    return $is_expired;
    }



    public function insertFeePaymentRecord($student_id, $feecategory_id, $paid_amount, $payment_mode="cash", $payment_notes,$discount_amount,$payment_slug='',$payment_mode_name='')
    {
        $category_record = \App\FeeCategory::where('id','=',$feecategory_id)->first(); 
        $student_record = \App\Student::where('id','=',$student_id)->first();



        $feePayment          = new \App\FeePayment();

        if($payment_slug=='') {

          $feePayment->transaction_id       = getHashCode();
        }
        else {
          $feePayment->transaction_id       = $payment_slug;
        }

        $feePayment->academic_id          = $category_record->academic_id;
        $feePayment->course_parent_id     = $category_record->course_parent_id;
        $feePayment->course_id            = $category_record->course_id;
        $feePayment->year                 = $category_record->year;
        $feePayment->semister             = $category_record->semister;

        $feePayment->feecategory_id       = $category_record->id;
        $feePayment->feecategory_title    = $category_record->title;
        
        $feePayment->student_id           = $student_record->id;
        $feePayment->user_id              = $student_record->user_id;
        $feePayment->amount               = \App\FeeCategoryParticular::getTotalFee($feecategory_id);
          
       
          
          
        $any_paidamount    = \App\FeeParticularPayment::where('student_id','=',$student_id)
                                                    ->where('feecategory_id','=',$feecategory_id)
                                                    ->sum('paid_amount');

        $any_discount_paid = \App\FeeParticularPayment::where('student_id','=',$student_id)
                                                      ->where('feecategory_id','=',$feecategory_id)
                                                      ->sum('discount'); 

          
        $any_previous_balance  = \App\FeeParticularPayment::where('student_id','=',$student_id)
                                                         ->where('feecategory_id','=',$feecategory_id)
                                                         ->where('previous_feecategory_id','>',0)
                                                         ->where('paid_percentage','=',0)
                                                         ->sum('amount');
                                    
         
        $any_previous_balance_paid = \App\FeeParticularPayment::where('student_id','=',$student_id)
                                                         ->where('feecategory_id','=',$feecategory_id)
                                                         ->where('previous_feecategory_id','>',0)
                                                         ->where('paid_percentage','=',100)
                                                         ->sum('amount');

       

                                                         
          // dd($any_previous_balance_paid);
          $feePayment->total_amount       =   $feePayment->amount + $any_previous_balance;         
          $feePayment->previous_balance   =   $any_previous_balance;                                             
          $feePayment->paid_amount          = $paid_amount;
          $feePayment->discount_amount      = $discount_amount;
          
          $feePayment->balance              = ($feePayment->amount + $any_previous_balance + $any_previous_balance_paid) - ($feePayment->paid_amount+$any_paidamount+$discount_amount+$any_discount_paid);

          $feePayment->payment_mode         = $payment_mode;
          if($payment_mode_name!=''){
            $feePayment->payment_mode_name  = $payment_mode_name;
          }
          $feePayment->payment_notes        = $payment_notes;
          $today   = date('Y-m-d');
          $feePayment->recevied_on          = $today;

          // if($request->has('other_payment_mode')){
           
          //  $feePayment->payment_mode_name         = $request->other_payment_mode;

          // }

          $payment_recivd_by = \App\User::first()->id;
          $feePayment->payment_recevied_by = $payment_recivd_by;//\App\Auth::user()->id;
          $feePayment->save();
          return $feePayment;
  }



    /**
   * This method will update all the other amount records
   * @param  [type] $other_payments [description]
   * @return [type]                 [description]
   */
  public function updatePaymentRecords($payment_records, $feepayment_id,$total_amount = 0,$discount_amount = 0)
  {
    
    //Amount pending is the sum of amount of payment records
    //Each time after amount is being paid, we decrease the amount pending with the paid amount
    //This can be used for distribution of amount for remaining records if remaining amount 
    //is not sufficient
    $amount_pending = $this->getTotalPendingAmount($payment_records);
    $total_records = count($payment_records);
    $percentage = 0;
    $amount_needed = 0;
    foreach($payment_records as $payment)
    {
      
      if($payment->paid_percentage==100)
        continue;
      if($this->isPaymentCompleted($payment) <= 0)
        continue;

      if($this->remaining_amount > 0 )
      {
          $amount_needed = $payment->amount;
          if($payment->paid_percentage>0 && $payment->paid_percentage<100)
          {

          
            $amount_needed = $this->isPaymentCompleted($payment);
            // dd($amount_needed);
            $newPaymentRecord = $this->getNewPaymentRecord($payment);
            $payment = $newPaymentRecord;
          }

          if($amount_needed <= ($this->remaining_amount+$discount_amount))
          { 
            if($total_amount==0){
            $payment->paid_amount = round($amount_needed);
            $payment->balance = $payment->amount - $payment->paid_amount;
            $payment->paid_percentage = 100;
           
            $amount_pending -= $payment->paid_amount;
            $total_records--;

            }
            else{
            $paid_amount         =  round($amount_needed);
            $discount_percentage = getPercentage($paid_amount,$total_amount);
            $discount_value      = calculatePercentage($discount_amount,$discount_percentage);
            
            $payment->paid_amount = $paid_amount-$discount_value;
            $payment->discount    = $discount_value;
            $payment->balance     = $payment->amount - $paid_amount;
            $payment->paid_percentage = 100;
           
            $amount_pending -= $payment->paid_amount;
            $total_records--;

            }
          }
          else {
            //Remaining amount is not sufficient for current payment
            //And check if there are more than 1 records available
            //If only single record exists, simply allocate all the amount to available record and update it
            if(!$percentage)
            {
              $percentage = getPercentage($this->remaining_amount, $amount_pending);
            }
            
            if($total_records>1)
            {
              $amount_to_pay = calculatePercentage($amount_needed, $percentage);

              $payment->paid_amount = $amount_to_pay;
              $payment->balance = $payment->amount - $payment->paid_amount;
              $payment->paid_percentage = $percentage;
              
            }
            else {
              //Only one record exist, so directly update the available amount 
               if($total_amount==0){

              $payment->paid_amount = $this->remaining_amount;
              $payment->balance     = $payment->amount - $payment->paid_amount;
              $payment->paid_percentage = getPercentage($payment->paid_amount, $amount_needed);

              }
              else{
              
              $paid_amount         =  round($amount_needed);
              $discount_percentage = getPercentage($paid_amount,$total_amount);
              $discount_value      = calculatePercentage($discount_amount,$discount_percentage); 
              
              $payment->paid_amount = $this->remaining_amount;
              $payment->balance     = $payment->amount - $payment->paid_amount;
              $payment->discount    = $discount_value;
              $payment->paid_percentage = getPercentage($payment->paid_amount, $paid_amount);

              }
              
            }
   
          }
           $this->updateRemainingAmount($payment);

           $payment_recivd_by = \App\User::first()->id;

           $payment->received_on         = currentDateTime();
           $payment->payment_received_by = $payment_recivd_by;//Auth::user()->id;
           $payment->feepayment_id       = $feepayment_id;
           // if($payment->is_schedule) {
           //    $this->updateScheduleRecord($payment);
           // }


          $payment->save();
        
      }

    }
    // if($percentage){
    //   echo $this->remaining_amount;
    //   dd($payment_records);
    // }
    return 1;
  }


   public function updateRemainingAmount($payment)
  {
     $this->remaining_amount = round($this->remaining_amount) - round($payment->paid_amount);
  }


  public function getTotalPendingAmount($payment_records)
  {
    $sum = 0;
    foreach($payment_records as $payment)
      $sum += $payment->amount;
    return $sum;
  }


   public function isPaymentCompleted($payment)
  {
    $amount_paid = \App\FeeParticularPayment::where('feecategory_id', '=', $payment->feecategory_id)
                                          ->where('feeparticular_id', '=', $payment->feeparticular_id)
                                          ->where('feeschedule_particular_id', '=', $payment->feeschedule_particular_id)
                                          ->where('feeschedule_id','=',$payment->feeschedule_id)
                                          ->where('student_id','=',$payment->student_id)
                                          ->sum('paid_amount');
    $total_amount = \App\FeeParticularPayment::where('feecategory_id', '=', $payment->feecategory_id)
                                          ->where('feeparticular_id', '=', $payment->feeparticular_id)
                                          ->where('feeschedule_particular_id', '=', $payment->feeschedule_particular_id)
                                          ->where('feeschedule_id','=',$payment->feeschedule_id)
                                          ->where('student_id','=',$payment->student_id)
                                          ->sum('amount');
    // echo 'Paid: '.$amount_paid;
    
    // dd($amount_paid);
    // if($amount_paid >= $total_amount)
    //   return 1;
    // return 0;
    return $total_amount - $amount_paid;
  }


    public function getNewPaymentRecord($payment)
  {
      $newPaymentRecord               = new \App\FeeParticularPayment();
      $newPaymentRecord->amount       = 0;
      $newPaymentRecord->paid_amount  = 0;
      $newPaymentRecord->balance      = 0;
      $newPaymentRecord->net_balance  = 0;
      $newPaymentRecord->paid_percentage  = 0;
      $newPaymentRecord->feepayment_id    = $payment->feepayment_id;
      $newPaymentRecord->feecategory_id   = $payment->feecategory_id;
      $newPaymentRecord->feeparticular_id = $payment->feeparticular_id;
      $newPaymentRecord->feecategory_particular_id = $payment->feecategory_particular_id;
      $newPaymentRecord->is_schedule      = $payment->is_schedule;
      $newPaymentRecord->feeschedule_particular_id = $payment->feeschedule_particular_id;
      $newPaymentRecord->feeschedule_id   = $payment->feeschedule_id;
      $newPaymentRecord->term_number      = $payment->term_number;
      $newPaymentRecord->user_id          = $payment->user_id;
      $newPaymentRecord->student_id       = $payment->student_id;
      $newPaymentRecord->academic_id      = $payment->academic_id;
      $newPaymentRecord->course_parent_id = $payment->course_parent_id;
      $newPaymentRecord->course_id        = $payment->course_id;
      $newPaymentRecord->year             = $payment->year;
      $newPaymentRecord->semister         = $payment->semister;
      return $newPaymentRecord;

  }


   /**
  This method insert the record in feepayments_online table
  if the selected payment gateway is offline payment gateway
  **/
   public function offlinePayment(Request $request)
   {


        $record                  = new \App\FeePaymentsOnline();
        $record->slug            = $record->makeSlug(getHashCode());
        $record->user_id         = $request->user_id;
        $record->feecategory_id  = $request->fee_cat_id;
        $record->plan_type       = 'fee';
        $record->payment_gateway = $request->gateway;
        $record->paid_by         = $request->user_id;
        $record->paid_amount     = $request->amount;
        $record->payment_status  = 'pending';
        $record->other_details   = $request->payment_details;
        $record->save();

        $response['status'] = 1;
        $response['message'] = 'Your offline payment details are submitted to admin';

        return $response;


   
   }




}