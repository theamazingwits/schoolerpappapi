<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use \App;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Vroutes;
use App\Vdrivers;
use App\Vechicle;
use App\VehicleAssign;
use App\VdriverDocuments;
use App\LogHelper;
use Yajra\Datatables\Datatables;
use DB;
use AWS;
use Auth;
use Carbon\Carbon;
use Exception;
use Response;


class DriverController extends Controller
{
  //Driver Login OTP Insert
  public function driverLogin(Request $request)
  { 
      //Check Vaild Driver (Mobile number)
      $response = [];
      $mobnoverify =  DB::table('vdrivers')
                      ->where('phone_number', $request->mobile_no)
                      ->orderBy('updated_at', 'DESC')
                      ->first();

      if($mobnoverify){
        $otp = substr(str_shuffle("0123456789"), 0, 4);
        
        $values = array('login_otp' => $otp,
        'updated_at' => date('Y-m-d H:i:s')
        );

        DB::table('vdrivers')
        ->where('phone_number', $request->mobile_no)
        ->update($values);

        $message = 'Your Login OTP is '.$otp;

        //SMS send function placed Here
        $this->smsSend($mobnoverify->phone_number,$message);

        $response['status']  = 1;
        $response['message'] = 'OTP sent successfully';

      } else{
         $response['status']  = 0;
         $response['message'] = 'Please enter valid mobile number';
      }

      return $response;
    }

    //Resend Driver OTP
    public function driverLoginResendOTP(Request $request)
  { 
      //Check Vaild Driver (Mobile number)
      $response = [];
      $mobnoverify =  DB::table('vdrivers')
                      ->where('phone_number', $request->mobile_no)
                      ->orderBy('updated_at', 'DESC')
                      ->first();

      if($mobnoverify){
        $otp = substr(str_shuffle("0123456789"), 0, 4);
        
        $values = array('login_otp' => $otp,
        'updated_at' => date('Y-m-d H:i:s')
        );

        DB::table('vdrivers')
        ->where('phone_number', $request->mobile_no)
        ->update($values);

        $message = 'Your Login OTP is '.$otp;

        //SMS send function placed Here
        $this->smsSend($mobnoverify->phone_number,$message);

        $response['status']  = 1;
        $response['message'] = 'OTP resent successfully';

      } else{
         $response['status']  = 0;
         $response['message'] = 'Please enter valid mobile number';
      }

      return $response;
    }

  //Verify Login Otp
  public function verifyLoginOTP(Request $request)
  {   
      $now = Carbon::now();

      $result =  DB::table('vdrivers')
                ->where('phone_number', $request->mobile_no)
                ->where('login_otp',$request->otp)
                ->first(['updated_at']);
               
      if(isset($result) > 0) 
      {
        $updated_at = Carbon::parse($result->updated_at);
        $diff = $updated_at->diffInMinutes($now);

        if ($diff <= 30)
        {
          //OTP Verified
          $values = array('login_otp' => 'verified');
            
          DB::table('vdrivers')
          ->where('phone_number',$request->mobile_no)
          ->update($values);

          $data =  DB::table('vdrivers')
                ->where('phone_number', $request->mobile_no)
                ->get();

          $response['status']  = 1;
          $response['message'] = 'Vaild OTP';  
          $response['data'] = $data;                
        } else
          {
            //OTP Invalid
            DB::table('vdrivers')
            ->where('phone_number',$request->mobile_no)
            ->update(['login_otp' => NULL]);

            $response['status']  = 0;
            $response['message'] = 'OTP expired please resend it';
          }
      } 
      else
      { 
          $response['status']  = 0;
          $response['message'] = 'Please Enter Vaild OTP';
      }
    return $response;
  }

  //Fetch Driver Details
  public function driverDetaileFetch($id)
 {
        $result = DB::table('vdrivers')
                  ->where('id',$id)
                  /*->join('vechicles', 'vechicles.id','=','vdrivers.vehicle_id')*/
                  ->get();

        $response['status']   = 1;
        $response['data']    = $result;
        return $response;
 }


  //Fetch Driver Details
  public function driverTripStudent($id)
 {    

      $Driver_data = DB::table('vdrivers')
                    ->where('id',$id)
                    ->first();

      $vechicles_data =   DB::table('vechicles')
                  ->where('id',$Driver_data->vehicle_id)
                  ->first();

      $route_data =   DB::table('vechicle_assign as va')
                  ->join('vroutes as vr', 'va.route_id', 'vr.id')
                  ->where('va.vechicle_id',$Driver_data->vehicle_id)
                  ->get();

      $student_data = DB::table('vehicle_user as vc')
                      ->join('students as us', 'vc.user_id', 'us.user_id')
                      //->join('vroutes as vr', 'vc.route_id', 'vr.id')
                      ->where('vc.vehicle_id',$Driver_data->vehicle_id)
                      ->where('vc.is_active',1)
                      ->get();

        $response['status']   = 1;
        $response['Driver_data']    = $Driver_data;
        $response['vechicles_data']    = $vechicles_data;
        $response['route_data']    = $route_data;
        $response['student_data']    = $student_data;
        return $response;
 }

 //Fetch Driver Route Details
  public function driverRoute($id)
 {    

      $Driver_data = DB::table('vdrivers')
                    ->where('id',$id)
                    ->first();

      $vechicles_data =   DB::table('vechicles')
                  ->where('id',$Driver_data->vehicle_id)
                  ->first();

      $vechicleassign_data =   DB::table('vechicle_assign')
                  ->where('vechicle_id',$Driver_data->vehicle_id)
                  ->get();


      $new = [];
      foreach ($vechicleassign_data as $route) {
          $new[] = $route->route_id;
      }

      $route_data  =  DB::table('vroutes')
                      ->whereIn('id', $new)
                      ->get();

      /*$vechicleassign_data =   DB::table('vechicle_assign')
                  ->where('id',$Driver_data->vehicle_id)
                  ->get();
*/
        $response['status']   = 1;
        $response['Driver_data']    = $Driver_data;
        $response['vechicles_data']    = $vechicles_data;
        //$response['vechicleassign_data']    = $vechicleassign_data;
        $response['route_data']    = $route_data;
        return $response;
 }  

 //Fetch Driver Emergency
  public function driverEmergency()
 {
        $result = DB::table('users')
                  ->where('role_id',1)
                  ->get();

        $response['status']   = 1;
        $response['data']    = $result;
        return $response;
 }

    //SMS USING AMAZON S3
    public function smsSend($mobile,$message){

        $sms = AWS::createClient('sns');    
        $sms->publish([
                'Message' => $message,
                'PhoneNumber' => '+91'.$mobile,    
                'MessageAttributes' => [
                    'AWS.SNS.SMS.SMSType'  => [
                        'DataType'    => 'String',
                        'StringValue' => 'Transactional',
                ]
            ],
        ]);
    }
    
}