<?php

namespace App\Http\Controllers\API;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;



class PaymentsController extends Controller
{
    
      public function paymentsHistory($user_id)
   {

    $response['status'] = 1;
    $response['message'] = '';
    
    $records = \App\Payment::select(['item_name', 'plan_type', 'start_date', 'end_date', 'payment_gateway', 'updated_at','payment_status','id','cost', 'after_discount', 'paid_amount'])
     ->where('user_id', '=', $user_id)
            ->orderBy('updated_at', 'desc')->get();
     $response['data'] = $records;
     return $response;
   }



    public function gatewayDetails()
    {
        
              
             $payu['payu_merchant_key']  = env('PAYU_MERCHANT_KEY', '');
             $payu['payu_salt']          = env('PAYU_SALT', '');
             $payu['payu_working_key']   = env('PAYU_WORKING_KEY', '');
             $payu['payu_testmode']      = env('PAYU_TESTMODE', '');

             $paytm['paytm_merchant_id']   = env('PAYTM_MERCHANT_ID', '');
             $paytm['paytm_merchant_key']  = env('PAYTM_MERCHANT_KEY', '');
             $paytm['paytm_website']       = env('PAYTM_WEBSITE', '');
             $paytm['paytm_channel']       = env('PAYTM_CHANNEL', '');
             $paytm['paytm_industry_type'] = env('PAYTM_INDUSTRY_TYPE', '');

             $stripe['stripe_key']     = env('STRIPE_KEY', '');
             $stripe['stripe_secret']  = env('STRIPE_SECRET', '');
             
             $paypal['email']        = getSetting('email','paypal');
             $paypal['currency']     = getSetting('currency','paypal');
             $paypal['account_type'] = getSetting('account_type','paypal');
             
            return json_encode(array('payu'=>$payu, 'paypal'=>$paypal,'paytm'=>$paytm,'stripe'=>$stripe ));   
        
    }


    public function saveTransaction(Request $request)
    {

       $response['status']  = 0;
       $response['message'] = 'Oops..! something went wrong';
       $response['record']  = null;

       try {

          $user_id                  = $request->user_id; // if student app - student user_id
          //if parent app - parent user_id

          $user = getUserRecord($user_id);//if student purchasing himself
          if(isset($request->paid_by_parent) && $request->paid_by_parent=="yes") 
          $user = getUserRecord($request->selected_child_id);

          $payment                  = new \App\Payment();
          $payment->slug            = getHashCode();
          // $payment->user_id         = $user_id;
          $payment->user_id         = $user->id;//child id
          $payment->item_id         = $request->item_id;
          $payment->item_name       = $request->item_name;
          $payment->start_date      = $request->start_date;
          $payment->end_date        = $request->end_date;
          $payment->plan_type       = $request->plan_type;
          $payment->payment_gateway = $request->payment_gateway; 
          $payment->transaction_id  = $request->transaction_id; 


          $payment->paid_by_parent  = 0; 
          if (isset($request->paid_by_parent) && $request->paid_by_parent=="yes")
          $payment->paid_by_parent  = 1; 

          $payment->paid_by         = $request->user_id;


          $payment->actual_cost     = $request->actual_cost;
          $payment->coupon_applied  = $request->coupon_applied;
          $payment->coupon_id       = $request->coupon_id;
          $payment->discount_amount = $request->discount_amount;
          $payment->cost            = $request->cost;
          $payment->after_discount  = $request->after_discount;
          $payment->paid_amount     = $request->after_discount;
          $payment->payment_status  = $request->payment_status;


          $other_details = array();
          $other_details['is_coupon_applied'] = $request->coupon_applied;
          $other_details['coupon_applied']    = $request->coupon_applied;
          $other_details['actual_cost']       = $request->actual_cost;
          $other_details['after_discount']    = $request->after_discount;
          $other_details['coupon_id']         = $request->coupon_id;
          $other_details['payment_details']   = $request->payment_details;
          $other_details['discount_availed']  = $request->discount_amount;

          if ($payment->paid_by_parent==1) {
            $other_details['paid_by_parent']  = $request->user_id; 
            $other_details['child_id']        = $request->selected_child_id;
          }


          $payment->other_details  = json_encode($other_details);


          $payment->save();

          if ($payment->coupon_applied)
          {
              $this->couponcodes_usage($payment);
          }

          $response['status'] = 1;
          $response['record'] = $payment;
          $response['message'] = 'Payment updated successfully';

      } catch(\Exception $ex) {

          $response['status'] = 0;
          $response['message'] = $ex->getMessage();
      }

        return $response;
    }


    public function couponcodes_usage($payment_record)
    {
        $coupon_usage['user_id']      = $payment_record->user_id;
        $coupon_usage['item_id']      = $payment_record->item_id;
        $coupon_usage['item_type']    = $payment_record->plan_type;
        $coupon_usage['item_cost']    = $payment_record->actual_cost;
        $coupon_usage['total_invoice_amount'] = $payment_record->paid_amount;
        $coupon_usage['discount_amount']      = $payment_record->discount_amount;
        $coupon_usage['coupon_id']            = $payment_record->coupon_id;
        $coupon_usage['updated_at']           =  new \DateTime();
        \DB::table('couponcodes_usage')->insert($coupon_usage);
        return TRUE;
    } 

    public function getCurrencyCode()
    {
        $response['status']   = 1;
        $response['message']  = 'Currency code';
        $response['record']   = getCurrencyCode();
        return $response;
        // return getCurrencyCode();
    }


    public function saveOfflinePayment(Request $request)
    {  

        try{

          $user_id   = $request->user_id; // if student app - student user_id
          //if parent app - parent user_id

          $user = getUserRecord($user_id);//if student purchasing himself
          if(isset($request->paid_by_parent) && $request->paid_by_parent=="yes") 
          $user = getUserRecord($request->selected_child_id);


        $payment                  = new \App\Payment();

        // $payment->user_id         = $request->user_id;
        
        $payment->user_id         = $user->id;//child id
        $payment->item_id         = $request->item_id;
        $payment->item_name       = $request->item_name;
        $payment->plan_type       = $request->plan_type;
        $payment->start_date      = $request->start_date;
        $payment->end_date        = $request->end_date;
        $payment->slug            = getHashCode();
        $payment->payment_gateway = "offline";

        $payment->paid_by_parent  = 0; 
        if (isset($request->paid_by_parent) && $request->paid_by_parent=="yes")
            $payment->paid_by_parent  = 1; 

        $payment->paid_by         = $request->user_id;


        $payment->actual_cost     = $request->actual_cost;
        $payment->coupon_applied  = $request->coupon_applied;
        $payment->coupon_id       = $request->coupon_id;
        $payment->discount_amount = $request->discount_amount;
        $payment->cost            = ($request->cost) ? $request->cost : $request->actual_cost;
        $payment->after_discount  = $request->after_discount;
        $payment->paid_amount     = $request->after_discount;
        $payment->payment_status  = PAYMENT_STATUS_PENDING;
        $payment->notes           = $request->payment_details;

        $other_details = array();

        $other_details['is_coupon_applied'] = $request->coupon_applied;
        $other_details['coupon_applied']    = $request->coupon_applied;
        $other_details['actual_cost']       = $request->actual_cost;
        $other_details['after_discount']    = $request->after_discount;
        $other_details['coupon_id']         = $request->coupon_id;
        $other_details['payment_details']   = $request->payment_details;
        $other_details['discount_availed']  = $request->discount_amount;
        
        if ($payment->paid_by_parent==1) {
          $other_details['paid_by_parent'] = $request->user_id;
          $other_details['child_id']     = $request->selected_child_id;
        }

        $payment->other_details  = json_encode($other_details);

        $payment->save();

        if ($payment->coupon_applied)
        {
            $this->couponcodes_usage($payment);
        }

         $response['status'] = 1;
         $response['message'] = 'Your Payment Request Submitted To Admin Successfully';

        }

        catch(\Exception $ex)
        {
            $response['status'] = 0;
            $response['message'] = $ex->getMessage();
        }

        return $response;
    }



    public function saveStripePayment(Request $request)
    {

       /* $response['status']   = 0;
        $response['message']  = 'test';
        $response['data']     = $request->all();
        $response['token']    = $request->token;

        return $response;*/


        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

     
        $paid_amount  = (int)$request->after_discount * 100;

        // $paid_amount = (int)$request->after_discount;

        try {

          try {
              $charge =   \Stripe\Charge::create ( array (
                    "amount"      => $paid_amount,//$paid_amount,
                    "currency"    => 'INR',//getCurrencyCode(),
                    "source"      => $request->token,//$request->token,//$request->input ( 'stripeToken' ), // obtained with Stripe.js
                    "description" => 'School app student payment'
              ) );

              if ($charge!=null) {

                  // $response['charge']         = $charge;
                  $response['response']       = "Success";
                  $response['stripe_message'] = "Charge has been completed";
              }

          } catch(\Stripe\Error\Card $e) {

              $response['status']    = 0;
              $response['response']  = "Error";
              $response['message']   = $e->getMessage();

              return $response;
           }
          


          $user_id                  = $request->user_id; // if student app - student user_id
          //if parent app - parent user_id

          $user = getUserRecord($user_id);//if student purchasing himself
          if(isset($request->paid_by_parent) && $request->paid_by_parent=="yes") 
          $user = getUserRecord($request->selected_child_id);

         
          $payment                  = new \App\Payment();
          // $payment->user_id         = $request->user_id;
          $payment->user_id         = $user->id;//child id
          $payment->item_id         = $request->item_id;
          $payment->item_name       = $request->item_name;
          $payment->plan_type       = $request->plan_type;
          $payment->start_date      = $request->start_date;
          $payment->end_date        = $request->end_date;
          $payment->slug            = getHashCode();
          $payment->payment_gateway = "stripe";

          $payment->paid_by_parent  = 0; 
          if (isset($request->paid_by_parent) && $request->paid_by_parent=="yes")
              $payment->paid_by_parent  = 1; 

          $payment->paid_by = $request->user_id;


          $payment->actual_cost     = $request->actual_cost;
          $payment->coupon_applied  = $request->coupon_applied;
          $payment->coupon_id       = $request->coupon_id;
          $payment->discount_amount = $request->discount_amount;
          $payment->cost            = ($request->cost) ? $request->cost : $request->actual_cost;
          $payment->after_discount  = $request->after_discount;
          $payment->paid_amount     = $request->after_discount;
          $payment->payment_status  = PAYMENT_STATUS_SUCCESS;
          

          $other_details = array();

          $other_details['is_coupon_applied'] = $request->coupon_applied;
          $other_details['coupon_applied']    = $request->coupon_applied;
          $other_details['actual_cost']       = $request->actual_cost;
          $other_details['after_discount']    = $request->after_discount;
          $other_details['coupon_id']         = $request->coupon_id;
          $other_details['discount_availed']  = $request->discount_amount;
          
          if ($payment->paid_by_parent==1) {
            $other_details['paid_by_parent']  = $request->user_id;
            $other_details['child_id']        = $request->selected_child_id;
          }

          $payment->other_details  = json_encode($other_details);

          $payment->save();


          if ($payment->coupon_applied)
          {
              $this->couponcodes_usage($payment);
          }

          $response['status']   = 1;
          $response['message']  = 'Your Payment Details Saved Successfully';

      }
      catch(Exception $e) {

          $response['status'] = 0;
          $response['message'] = $ex->getMessage();
      }

      return $response;
   }




    public function savePaytmPayment(Request $request)
    {

       try {

         $user_id                  = $request->user_id; // if student app - student user_id
          //if parent app - parent user_id

          $user = getUserRecord($user_id);//if student purchasing himself
          if(isset($request->paid_by_parent) && $request->paid_by_parent=="yes") 
          $user = getUserRecord($request->selected_child_id);


        $payment                  = new \App\Payment();

        // $payment->user_id      = $request->user_id;
        $payment->user_id         = $user->id;//child id
        $payment->item_id         = $request->item_id;
        $payment->item_name       = $request->item_name;
        $payment->plan_type       = $request->plan_type;
        $payment->start_date      = $request->start_date;
        $payment->end_date        = $request->end_date;
        // $payment->slug            = getHashCode();
        $payment->payment_gateway = "paytm";

        $payment->paid_by_parent  = 0; 
        if (isset($request->paid_by_parent) && $request->paid_by_parent=="yes")
            $payment->paid_by_parent  = 1; 

        $payment->paid_by = $request->user_id;


        $payment->actual_cost     = $request->actual_cost;
        $payment->coupon_applied  = $request->coupon_applied;
        $payment->coupon_id       = $request->coupon_id;
        $payment->discount_amount = $request->discount_amount;
        $payment->cost            = ($request->cost) ? $request->cost : $request->actual_cost;
        $payment->after_discount  = $request->after_discount;
        $payment->paid_amount     = $request->after_discount;
        $payment->payment_status  = PAYMENT_STATUS_SUCCESS;
        $payment->transaction_id  = $request->transaction_id;
        // $payment->notes           = $request->payment_details;

        $other_details = array();

        $other_details['is_coupon_applied'] = $request->coupon_applied;
        $other_details['coupon_applied']    = $request->coupon_applied;
        $other_details['actual_cost']       = $request->actual_cost;
        $other_details['after_discount']    = $request->after_discount;
        $other_details['coupon_id']         = $request->coupon_id;
        $other_details['payment_details']   = $request->payment_details;
        $other_details['discount_availed']  = $request->discount_amount;
        
        if ($payment->paid_by_parent==1) {
          $other_details['paid_by_parent']  = $request->user_id;
          $other_details['child_id']        = $request->selected_child_id;
        }

        $payment->other_details  = json_encode($other_details);

        $payment->save();

        $payment->slug           = $payment->id;
        $payment->save();


        if ($payment->coupon_applied)
        {
            $this->couponcodes_usage($payment);
        }

         $response['status'] = 1;
         $response['message'] = 'Your Payment Details Saved Successfully';

        }

        catch(\Exception $ex)
        {
            $response['status'] = 0;
            $response['message'] = $ex->getMessage();
        }

        return $response;
   }


}   