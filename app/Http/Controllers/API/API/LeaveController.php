<?php

namespace App\Http\Controllers\API;

use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Exception;
use DB;
use App\ServiceProvider;

// $path = 'public/NotifyMessage.json';
// $json_val = json_decode(file_get_contents($path), true);
// print_r($json_val['NOWmsg0017']);
// exit();


class LeaveController extends Controller
{
  private $ServiceProvider;

  private $json_val;

  public function __construct()
  {
        if(!isset(self::$this->ServiceProvider)){
          $this->ServiceProvider = new ServiceProvider();
      }

      $path = 'public/NotifyMessage.json';
      $this->json_val = json_decode(file_get_contents($path), true);

  }

     /**
     * This method adds Class Notes to DB
     * @param  Request $request [Request Object]
     * @return void
     */
 public function applyLeave(Request $request) {
 	// Fetch Student Basic Details
     $studentdetails = DB::table('students')
              ->select('academic_id','course_parent_id','course_id', 'first_name')
              ->where('user_id',$request->student_id)
              ->first();

    // Fetch Staff Basic Details
     $staffdetails = DB::table('staff')
      		->where('course_parent_id',$studentdetails->course_parent_id)
      		->where('course_id',$studentdetails->course_id)
      		->first(['staff_id','user_id']);

    // Insert the Leave Request
    $record = array(
				    'student_id' => $request->student_id,
				    'parent_id' => $request->parent_id,
				    'staff_id' => $staffdetails->staff_id,
				    'staff_user_id' => $staffdetails->user_id,
				    'course_id' => $studentdetails->course_id,
				    'course_parent_id' => $studentdetails->course_parent_id,
				    'academic_id' => $studentdetails->academic_id,
				    'student_name' => $request->student_name,
				    'role' => $request->type,
				    'leave_type' => $request->leave_type,
				    'leave_time' => $request->leave_time,
				    'session' => $request->session,
				    'leave_start_date' => $request->leave_start_date,
				    'leave_end_date' => $request->leave_end_date,
				    'leave_reason' => $request->leave_reason,
				    'created_at' => date('Y-m-d H:i:s'),
				    'updated_at' => date('Y-m-d H:i:s')
    			);
    // print_r($record);

    DB::table('student_leave')
    		->insert($record);

   	$notificationusers = DB::table('users')
					->where('id',$staffdetails->user_id)
					->select(['push_web_id','push_app_id','os_type','name','id','app_type'])
		            ->get();

	$aptyp = DB::table('users')
					->where('id',$staffdetails->user_id)
		            ->first(['app_type']);

    $this->ServiceProvider->data = $notificationusers;
    $this->ServiceProvider->aptyp = $aptyp;
    $this->ServiceProvider->message = $this->json_val['NOWmsg0009A'].$studentdetails->first_name.$this->json_val['NOWmsg0009B'];
    // student {{student_name}} has send Leave Request.
    $this->ServiceProvider->pageId = "UNKNOWN";
    $this->ServiceProvider->insertNotification($this->ServiceProvider);

    $response['status']  = 1;
    $response['message'] = 'Leave Applied Successfully';
    return $response;
}


// Fetch Parent Side Children Leave Request

public function getChildrenLeaveRequest($parent_id) {

    $records = DB::table('student_leave as stud_leave')
  			->join('staff as stf','stud_leave.staff_user_id','stf.user_id')
  			->where('stud_leave.parent_id',$parent_id)
		  	->select('stud_leave.id as student_leave_id', 'stf.staff_id', 'stud_leave.staff_user_id', 
		  		'stud_leave.student_id',
		  		'stf.first_name', 'stf.middle_name', 'stf.last_name', 'stud_leave.student_name', 
		  		'stud_leave.role', 'stud_leave.leave_type', 
		  		'stud_leave.leave_time', 'stud_leave.session', 'stud_leave.leave_status',
		  		'stud_leave.reject_reason', 'stud_leave.role', 'stud_leave.leave_start_date', 
		  		'stud_leave.leave_end_date', 'stud_leave.leave_reason', 'stud_leave.reject_reason')
		  	->orderby('stud_leave.updated_at','desc')
		  	->paginate(getRecordsPerPage());

    if(count($records) > 0){
         $response['status'] = 1;
         $response['data']   = $records;
    }else{
       $response['status'] = 0;
       $response['message'] = 'Leave Request not found';
    } 
    return $response;

  }

// Fetch Student Side Leave Request

public function getStudentLeaveRequest($student_id) {

    $records = DB::table('student_leave as stud_leave')
  			->join('staff as stf','stud_leave.staff_user_id','stf.user_id')
  			->join('users as use','stud_leave.parent_id','use.id')
  			->where('stud_leave.student_id',$student_id)
		  	->select('stud_leave.id as student_leave_id', 'stf.staff_id', 'stud_leave.staff_user_id', 'stud_leave.parent_id', 
		  		'stf.first_name', 'stf.middle_name', 'stf.last_name', 'use.name', 
		  		'stud_leave.role', 'stud_leave.leave_type', 'stud_leave.leave_time', 
		  		'stud_leave.session', 'stud_leave.leave_status',
		  		'stud_leave.reject_reason', 'stud_leave.role', 'stud_leave.leave_start_date', 
		  		'stud_leave.leave_end_date', 'stud_leave.leave_reason', 'stud_leave.reject_reason')
		  	->orderby('stud_leave.updated_at','desc')
		  	->paginate(getRecordsPerPage());

    if(count($records) > 0){
         $response['status'] = 1;
         $response['data']   = $records;
    }else{
       $response['status'] = 0;
       $response['message'] = 'Leave Request not found';
    } 
    return $response;

  }

// Fetch Staff Side Student Leave Request

public function getStaffLeaveRequest($staff_id) {

    $records = DB::table('student_leave as stud_leave')
  			->join('users as use','stud_leave.parent_id','use.id')
  			->where('stud_leave.staff_user_id',$staff_id)
		  	->select('stud_leave.id', 'stud_leave.parent_id', 'stud_leave.student_id',
		  		'use.name', 'stud_leave.student_name', 
		  		'stud_leave.role', 'stud_leave.leave_type', 'stud_leave.leave_time', 
		  		'stud_leave.session', 'stud_leave.leave_status',
		  		'stud_leave.reject_reason', 'stud_leave.role', 'stud_leave.leave_start_date', 
		  		'stud_leave.leave_end_date', 'stud_leave.leave_reason', 'stud_leave.reject_reason')
		  	->orderby('stud_leave.updated_at','desc')
		  	->paginate(getRecordsPerPage());

    if(count($records) > 0){
         $response['status'] = 1;
         $response['data']   = $records;
    }else{
       $response['status'] = 0;
       $response['message'] = 'Leave Request not found';
    } 
    return $response;

  }

  // Leave Accept Reject Change Status
public function updateLeaveStatus(Request $request) {

    if($request->leave_status == '1') {

      $update_values = array(
                'leave_status' => $request->leave_status,
                'updated_at' => date('Y-m-d H:i:s')
                );

      DB::table('student_leave')
          ->where('id',$request->student_leave_id)
          ->update($update_values);
          // ->select('student_id')stud_leave.id as student_leave_id
      $student = DB::table('student_leave')
      			->where('id', $request->student_leave_id)
      			->select('student_id')
      			->first();

      $notificationusers = DB::table('users as usr')
      						->join('student_leave as std_leave', 'std_leave.student_id', 'usr.id')
							->select(['usr.push_web_id','usr.push_app_id','usr.os_type','usr.name','usr.id','usr.app_type'])
		        			->get();

	  $aptyp = DB::table('users as usr')
	  			->join('student_leave as std_leave', 'std_leave.student_id', 'usr.id')
		        ->first(['app_type']);

	  $this->ServiceProvider->data = $notificationusers;
	  $this->ServiceProvider->aptyp = $aptyp;
	  $this->ServiceProvider->message = $this->json_val['NOWmsg0010A'].$student->student_id.$this->json_val['NOWmsg0010B'];
	  // Hai {{student_name}} Yours leave request has accepted
	  $this->ServiceProvider->pageId = "UNKNOWN";
	  $this->ServiceProvider->insertNotification($this->ServiceProvider);
  
      $response['status']  = 1;
      $response['message'] = 'Leave Request has Accepted';
    
    } else {
      
        $update_values = array(
                'leave_status' => $request->leave_status,
                'reject_reason' => $request->reject_reason,
                'updated_at' => date('Y-m-d H:i:s')
                );

        DB::table('student_leave')
            ->where('student_id',$request->student_leave_id)
            ->update($update_values);

        $student = DB::table('student_leave')
      			->where('id', $request->student_leave_id)
      			->select('student_id')
      			->first();

      $notificationusers = DB::table('users as usr')
      						->join('student_leave as std_leave', 'std_leave.student_id', 'usr.id')
							->select(['usr.push_web_id','usr.push_app_id','usr.os_type','usr.name','usr.id','usr.app_type'])
		        			->get();

	  $aptyp = DB::table('users as usr')
	  			->join('student_leave as std_leave', 'std_leave.student_id', 'usr.id')
		        ->first(['usr.app_type']);

	  $this->ServiceProvider->data = $notificationusers;
	  $this->ServiceProvider->aptyp = $aptyp;
	  $this->ServiceProvider->message = $student->student_id.$this->json_val['NOWmsg0011A'];
	  // {{student_name}} Yours leave request has Rejected
	  $this->ServiceProvider->pageId = "UNKNOWN";
	  $this->ServiceProvider->insertNotification($this->ServiceProvider);
    
        $response['status']  = 1;
        $response['message'] = 'Leave Request has Rejected';
        
      }
    return $response;
  }

	/* Hostal Leave Management */
	public function applyHostalLeave(Request $request) {
	 	
	 	// Fetch Student Room Details
	     $studentroomdetails = DB::table('hostel_user_room')
	              ->select('user_id','hostel_id','room_id')
	              ->where('user_id',$request->student_id)
	              ->first();

	    // Fetch Hostal Details
	    $hostaldetails = DB::table('hostel')
	              ->select('name','type')
	              ->where('id',$studentroomdetails->hostel_id)
	              ->first();

	    // Fetch Room Details
	    $roomdetails = DB::table('hostel_rooms')
	              ->select('room_number','id')
	              ->where('id',$studentroomdetails->room_id)
	              ->first();

	    // Insert the Hostal Leave Request
	    $record = array(
					    'student_id' => $request->student_id,
					    'hostel_id' => $studentroomdetails->hostel_id,
					    'room_id' => $studentroomdetails->room_id,
					    'room_number' => $roomdetails->room_number,
					    // 'student_name' => $request->student_name,
					    'leave_for' => $request->leave_for,
					    'leave_session' => $request->leave_session,
					    'leave_start_date' => $request->leave_start_date,
					    'leave_end_date' => $request->leave_end_date,
					    'leave_reason' => $request->leave_reason,
					    'leave_requested_date' => date('Y-m-d H:i:s'),
					    'created_at' => date('Y-m-d H:i:s'),
					    'updated_at' => date('Y-m-d H:i:s')
	    			);

	    DB::table('student_hostel_leave_details')
	    		->insert($record);

	    $notificationusers = DB::table('users as usr')
						->where('usr.id', $request->student_id)
						->select(['usr.push_web_id','usr.push_app_id','usr.os_type','usr.name','usr.id','usr.app_type'])
						->get();

		$aptyp = DB::table('users')
				->where('id',$request->student_id)
		        ->first(['app_type']);

	    $this->ServiceProvider->data = $notificationusers;
	    $this->ServiceProvider->aptyp = $aptyp->app_type;
	    $this->ServiceProvider->message = $request->student_id.$this->json_val['NOWmsg0006B'];
	    // {student_id} has sent Leave Request
	    $this->ServiceProvider->pageId = "UNKNOWN";
	    $this->ServiceProvider->insertNotification($this->ServiceProvider);

	    $response['status']  = 1;
	    $response['message'] = 'Hostal Leave Applied Successfully';
	    return $response;
	}

		public function getParentHostalLeaveRequest($parent_id)
    {
        $records = [];

        $students = DB::table('users')
	              ->select('name','id')
	              ->whereIn('parent_id',[$parent_id])
                   ->get();
         
        $studentid = [];
          foreach($students as $value){
            $studentid[] = $value->id;
              }   
              // print_r($studentid);
              // exit;
        $records =  DB::table('student_hostel_leave_details as studhoslev')
              ->join('users as use', 'studhoslev.student_id', 'use.id')
              ->join('hostel as hstl', 'studhoslev.hostel_id', 'hstl.id')
              // ->join('hostel_rooms as hstlrm', 'studhoslev.room_id', 'hstlrm.id')
              ->select('studhoslev.id','use.name','use.image','hstl.type', 'hstl.name as hostal_name', 
              	'studhoslev.room_number','studhoslev.leave_start_date','studhoslev.leave_end_date',
              'studhoslev.leave_for','studhoslev.leave_session','studhoslev.leave_reason','studhoslev.leave_status', 'studhoslev.comments', 'studhoslev.leave_requested_date')
              ->whereIn('studhoslev.student_id',$studentid)
              ->orderby('studhoslev.updated_at','desc')
		  	  ->paginate(getRecordsPerPage());

	    if(count($records) > 0){
	         $response['status'] = 1;
	         $response['data']   = $records;
	    }else{
	       $response['status'] = 0;
	       $response['message'] = 'Hostal Leave Request not found';
	    } 
	    return $response;
    }

	public function getStudentHostalLeaveRequest($student_id)
    {
        $records = [];

        $records =  DB::table('student_hostel_leave_details as studhoslev')
              ->join('users as use', 'studhoslev.student_id', 'use.id')
              ->join('hostel as hstl', 'studhoslev.hostel_id', 'hstl.id')
              ->select('studhoslev.id','use.name','use.image','hstl.type', 'hstl.name as hostal_name', 
              	'studhoslev.room_number','studhoslev.leave_start_date','studhoslev.leave_end_date',
              'studhoslev.leave_for','studhoslev.leave_session','studhoslev.leave_reason','studhoslev.leave_status','studhoslev.comments', 'studhoslev.leave_requested_date')
              ->where('studhoslev.student_id',$student_id)
              ->orderby('studhoslev.updated_at','desc')
		  	  ->paginate(getRecordsPerPage());

	    if(count($records) > 0){
	         $response['status'] = 1;
	         $response['data']   = $records;
	    }else{
	       $response['status'] = 0;
	       $response['message'] = 'Hostal Leave Request not found';
	    } 
	    return $response;
    }

//  public function updateLeaveStatusHostel(Request $request) {

  //   if($request->leave_status == '1') {

  //     $update_values = array(
  //               'leave_status' => $request->leave_status,
  //               'updated_at' => date('Y-m-d H:i:s')
  //               );

  //     DB::table('student_hostel_leave_details')
  //         ->where('id',$request->student_leave_id)
  //         ->update($update_values);

  //     $studentdetail = DB::table('student_hostel_leave_details')
  //     					->select('student_id')
  //     					->where('id', $request->student_leave_id )
  //     					->first();

  //     $studid = DB::table('users')
  //      			->select('name')
  //      			->where('id', $studentdetail->student_id)
  //      			->first();

	 //  $notificationusers = DB::table('users as usr')
	 //  						->join('student_hostel_leave_details as studs','usr.id','studs.student_id')
	 //  						->where('studs.id', $request->student_leave_id)
		// 					->select(['usr.push_web_id','usr.push_app_id','usr.os_type','usr.name','usr.id','usr.app_type'])
		//         			->get();
		// print_r($notificationusers);
		// exit();

	 //  $this->ServiceProvider->data = $notificationusers;
	 //  $this->ServiceProvider->message = $studid->name.",yours leave request has accepted.";
	 //  $this->ServiceProvider->pageId = "UNKNOWN";
	 //  $this->ServiceProvider->insertNotification($this->ServiceProvider);
  
  //     $response['status']  = 1;
  //     $response['message'] = 'Leave Request has Accepted';
    
  //   } else {
      
  //         $update_values = array(
  //               'leave_status' => $request->leave_status,
  //               'reject_reason' => $request->reject_reason,
  //               'updated_at' => date('Y-m-d H:i:s')
  //               );


	 //      DB::table('student_hostel_leave_details')
	 //          ->where('id',$request->student_leave_id)
	 //          ->update($update_values);

	 //      $studentdetail = DB::table('student_hostel_leave_details')
	 //      					->select('student_id')
	 //      					->where('id', $request->student_leave_id )
	 //      					->first();

	 //      $studid = DB::table('users')
	 //       			->select('name')
	 //       			->where('id', $studentdetail->student_id)
	 //       			->first();

		//   $notificationusers = DB::table('users as usr')
		//   						->join('student_hostel_leave_details as studs','usr.id','studs.student_id')
		//   						->where('studs.id', $request->student_leave_id)
		// 						->select(['usr.push_web_id','usr.push_app_id','usr.os_type','usr.name','usr.id','usr.app_type'])
		// 	        			->get();
		// 	print_r($notificationusers);
		// 	exit();

		//   $this->ServiceProvider->data = $notificationusers;
		//   $this->ServiceProvider->message = $studid->name.",yours leave request has accepted.";
		//   $this->ServiceProvider->pageId = "UNKNOWN";
		//   $this->ServiceProvider->insertNotification($this->ServiceProvider);
  
    
  //       $response['status']  = 1;
  //       $response['message'] = 'Leave Request has Rejected';
        
  //     }
  //   return $response;
  // }
    public function applyStaffLeave(Request $request) {

    	// Insert the Leave Request
	    $record = array(
					    'staff_id' => $request->staff_id,
					    'leave_type' => $request->leave_type,
					    'leave_time' => $request->leave_time,
					    'session' => $request->session,
					    'start_date' => $request->start_date,
					    'end_date' => $request->end_date,
					    'apply_reason' => $request->apply_reason,
					    'created_at' => date('Y-m-d H:i:s'),
					    'updated_at' => date('Y-m-d H:i:s')
	    			);

	    DB::table('staff_leave_application')
	    		->insert($record);

//   	$notificationusers = DB::table('users as usr')
		// ->where('role_id','1')
		// ->select(['push_web_id','push_app_id','os_type','name','id','app_type'])
//          ->get();
    
	    // $this->ServiceProvider->data = $notificationusers;
	    // $this->ServiceProvider->message = "Staff".$staffdetails->first_name." has send Leave Request.";
	    // $this->ServiceProvider->pageId = "UNKNOWN";
	    // $this->ServiceProvider->insertNotification($this->ServiceProvider);

	    $response['status']  = 1;
	    $response['message'] = 'Leave Applied Successfully';
	    return $response;
	}

	// Fetch Staff Side Student Leave Request

	public function getStaffNewLeaveRequest($staff_id) {

    $records = DB::table('staff_leave_application as stf_lv')
  			->join('users as use','stf_lv.staff_id','use.id')
  			->where('stf_lv.staff_id',$staff_id)
		  	->select('stf_lv.id', 'stf_lv.staff_id', 
		  		'use.name as staff_name', 
		  		'stf_lv.leave_type', 'stf_lv.leave_time', 'stf_lv.session', 
		  		'stf_lv.leave_status','stf_lv.apply_reason', 'stf_lv.reject_reason', 
		  		'stf_lv.start_date', 'stf_lv.end_date', 'stf_lv.created_at', 'stf_lv.updated_at')
		  	->orderby('stf_lv.updated_at','desc')
		  	->paginate(getRecordsPerPage());

    	if(count($records) > 0){
	         $response['status'] = 1;
	         $response['data']   = $records;
	    }else{
	       $response['status'] = 0;
	       $response['message'] = 'Leave Request not found';
	    } 
	    return $response;

  }


}