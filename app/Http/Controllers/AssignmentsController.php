<?php
namespace App\Http\Controllers;
use \App;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\CourseSubject;
use App\Assignments;
use App\AssignmentAllocate;
use App\Subject;
use App\Course;
use App\Academic;
use App\GeneralSettings;
use App\User;
use Yajra\Datatables\Datatables;
use DB;
use Auth;
use Exception;
use Response;


class AssignmentsController extends Controller
{
    
    public function __construct()
    {
    	$this->middleware('auth');
    }

    /**
     * Course listing method
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function index($slug = "")
    {

      if(!checkRole(getUserGrade(3)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $user  = Auth::user();

        if($slug){

          $user  = User::where('slug',$slug)->first();
        }
         
        $data['active_class']       = 'assignments';
         if(checkRole(getUserGrade(2)))
        $data['active_class']       = 'users';
        $data['layout']             = getLayout();
        $data['title']              = getPhrase('assignments');
        $data['user']               = $user;

        return view('assignments.list', $data);

          
    }

    /**
     * This method returns the datatables data to view
     * @return [type] [description]
     */
    public function getDatatable($user_id)
    {

      if(!checkRole(getUserGrade(3)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $records = array();

        
        $records = Assignments::select(['title','subject_id','course_subject_id','deadline','file_name','slug','id','description'])->where('user_id',$user_id);
        
        $records->orderBy('updated_at', 'desc');
             

        return Datatables::of($records)
        ->addColumn('action', function ($records) {
         
          $link_data = '<div class="dropdown more">
                        <a id="dLabel" type="button" class="more-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-ellipsis-v"></i>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <li>

                            <a href="'.URL_VIEW_ASSIGNMENTS_UPLOADS.$records->slug.'"><i class="fa fa-eye"></i>'.getPhrase("view").'</a>
                            </li>';
                            
                           $temp = '';
                           if(checkRole(getUserGrade(11))) {
                    $temp .= ' <li>
                    <a href="'.URL_ASSIGNMENTS_EDIT.$records->slug.'"><i class="fa fa-pencil"></i>'.getPhrase("edit").'</a>
                    <a href="javascript:void(0);" onclick="deleteRecord(\''.$records->id.'\');"><i class="fa fa-trash"></i>'. getPhrase("delete").'</a></li>';
                      }
                    
                    $temp .='</ul></div>';

                    $link_data .=$temp;
            return $link_data;
            })

        ->editColumn('subject_id', function($records)
        {
           $subject  = Subject::find($records->subject_id);

           if($subject)
            return ucwords($subject->subject_title);

          return '-';

        })

         ->editColumn('title', function($records)
        {
           return '<a href="'.URL_VIEW_ASSIGNMENTS_UPLOADS.$records->slug.'"><span data-toggle="tooltip" data-placement="bottom" title="'.$records->description.'">'.ucwords($records->title).'<span></a>';
           
        })

          ->editColumn('course_subject_id', function($records)
        {
            
               $class  = CourseSubject::join('academics', 'academics.id','=','course_subject.academic_id')
                                  ->join('courses', 'courses.id','=','course_subject.course_id')
                                  ->where('course_subject.id',$records->course_subject_id)
                                  ->select(DB::raw("CONCAT(academic_year_title,'-',course_title) AS class_name"),'course_subject.id')
                                  ->first();

               return $class->class_name;
        })

        ->editColumn('file_name', function($records)
        {  
            if($records->file_name){

             return '<img src="'.IMAGE_PATH_UPLOAD_ASSIGNMENS_DEFAULT.'" height="30" width="30" />
             <p><i class="fa fa-cloud-download"> <a href="'.URL_DOWNLOAD_ASSIGNMENT_FILE.$records->slug.'">'. $records->file_name.'</a></i></p>';
            }
            return '-';
           
        })

        ->removeColumn('id')
        ->removeColumn('slug')
        ->removeColumn('description')
        ->make();
    }

    /**
     * This method loads the create view
     * @return void
     */
    public function create()
    {
      if(!checkRole(getUserGrade(3)))
      {
        prepareBlockUserMessage();
        return back();
      }
        $data['record']         = FALSE;
        $data['uploaded']       = FALSE;
        $data['active_class']   = 'assignments';
        $data['layout']         = getLayout();
        $data['title']          = getPhrase('add_assignment');
        $data['assignment_id']  = 0; 

        $staff        = Auth::user();
        $academic_id  = getDefaultAcademicId();
        $subjects     = CourseSubject::getStaffSubjetcs($staff->id,$academic_id);
        $classes      = CourseSubject::getStaffClasses($staff->id,$academic_id);


        $data['subjects']   = $subjects;
        $data['classes']    = $classes;
        $data['add_record'] = 1;
        // dd('herer');

         return view('assignments.add-edit', $data);

       
    }

    /**
     * This method loads the edit view based on unique slug provided by user
     * @param  [string] $slug [unique slug of the record]
     * @return [view with record]       
     */
    public function edit($slug)
    {
      if(!checkRole(getUserGrade(3)))
      {
        prepareBlockUserMessage();
        return back();
      }
        
        $record = Assignments::where('slug',$slug)->first();
        $assignment_upload   = AssignmentAllocate::where('assignment_id',$record->id)
                                                  ->where('is_submitted',1)
                                                  ->get()->count();
        $data['uploaded']   = FALSE;
        if($assignment_upload > 0){

            $data['uploaded']   = TRUE;
        }                                          

        $data['active_class'] = 'assignments';
        $data['layout']       = getLayout();
        $data['title']        = getPhrase('edit_assignment');

        $staff        = Auth::user();
        $academic_id  = getDefaultAcademicId();
        $subjects     = CourseSubject::getStaffSubjetcs($staff->id,$academic_id);
        $classes      = CourseSubject::getStaffClasses($staff->id,$academic_id);

        $data['subjects'] = $subjects;
        $data['classes']  = $classes;
        $data['record']   = $record;
        $data['assignment_id']  = $record->id; 
        $data['add_record'] = 2;

         return view('assignments.add-edit', $data);

        
    }

    /**
     * Update record based on slug and reuqest
     * @param  Request $request [Request Object]
     * @param  [type]  $slug    [Unique Slug]
     * @return void
     */
    public function update(Request $request, $slug)
    {
      // dd($request);

      if(!checkRole(getUserGrade(3)))
      {
        prepareBlockUserMessage();
        return back();
      }

         $record      = Assignments::where('slug',$slug)->first();
         $any_upload  = AssignmentAllocate::where('assignment_id','=',$record->id)
                                            ->where('is_submitted',1)
                                            ->get()->count();
        if($any_upload > 0){

            flash('Oops..!','student_is_already_uploaded_assignment_submission_file_so_you_are_unable_to_modify','overlay');
            return redirect( URL_ASSIGNMENTS );
         }

       if( $request->type == 1){
        
         if(!$request->has('users_id')){

            flash('Oops..!','please_select_the_students','overlay');
            return back();
         }

      }


         //Delete Previous Records
         
         DB::beginTransaction();

         try {
            
           AssignmentAllocate::where('assignment_id','=',$record->id)->delete();                          

            $name = $request->title;
           if($name != $record->title)
            $record->slug = $record->makeSlug($name);

           $record->user_id       = Auth::user()->id;
           $record->title         = $request->title;
           $record->subject_id    = $request->subject_id;
           $record->course_subject_id   = $request->course_subject_id;
           $record->type          = $request->type;
           $record->deadline      = $request->deadline;
           $record->description   = $request->description;
           $record->save();
// dd('herere');
           $this->processUpload($request, $record, 'catimage');
// dd($record);
           
           if($request->type == 0){

              $record->addAllStudents($request);
           }else{
              

              $record->addSelectedStudents($request);
           }

           flash('success','assignment_is_updated_successfully', 'success');
           DB::commit();
         
       } 
       catch (Exception $e) {
          
          DB::rollBack();
          // dd($e->getMessage());

         flash('Oops...!','Error! Please Try again', 'error');
       }
         return redirect(URL_ASSIGNMENTS);
    }

    /**
     * This method adds record to DB
     * @param  Request $request [Request Object]
     * @return void
     */
    public function store(Request $request)
    { 
      
      if(!checkRole(getUserGrade(3)))
      {
        prepareBlockUserMessage();
        return back();
      }

      if( $request->type == 1){
        
         if(!$request->has('users_id')){

            flash('Oops..!','please_select_the_students','overlay');
            return back();
         }

      }

      DB::beginTransaction();

       try {

           $record                = new Assignments();
           $record->user_id       = Auth::user()->id;
           $record->title         = $request->title;
           $record->slug          = $record->makeSlug($request->title);
           $record->subject_id    = $request->subject_id;
           $record->course_subject_id   = $request->course_subject_id;
           $record->type          = $request->type;
           $record->deadline      = $request->deadline;
           $record->description   = $request->description;
           $record->save();
// dd('herere');
           $this->processUpload($request, $record, 'catimage');
           
           if($request->type == 0){

              $record->addAllStudents($request);
           }else{
            
              $record->addSelectedStudents($request);
           }

           flash('success','assignment_is_added_successfully', 'success');
           DB::commit();
         
       } 
       catch (Exception $e) {
          
          DB::rollBack();
          // dd($e->getMessage());

         flash('Oops...!','Error! Please Try again', 'error');
       }
         return redirect(URL_ASSIGNMENTS);

    }
 
    /**
     * Delete Record based on the provided slug
     * @param  [string] $slug [unique slug]
     * @return Boolean 
     */
    public function delete($id)
    {
      if(!checkRole(getUserGrade(3)))
      {
        prepareBlockUserMessage();
        return back();
      }
      /**
       * Delete the questions associated with this quiz first
       * Delete the quiz
       * @var [type]
       */
        $record      = Assignments::where('id', $id)->first();

        $any_upload  = AssignmentAllocate::where('assignment_id','=',$record->id)
                                            ->where('is_submitted',1)
                                            ->get()->count();

        if($any_upload > 0){
           
            
            $response['status'] = 0;
            $response['message'] = getPhrase('student_is_already_uploaded_assignment_submission_file_so_you_are_unable_to_delete');
         }
         else{
            
            if(!env('DEMO_MODE')) {

                AssignmentAllocate::where('assignment_id','=',$record->id)->delete();    
                $record->delete();
            }

            $response['status'] = 1;
            $response['message'] = getPhrase('record_deleted_successfully'); 

         }

        return json_encode($response);
    }

    public function isValidRecord($record)
    {
        if ($record === null) {

            flash('Ooops...!', getPhrase("page_not_found"), 'error');
            return $this->getRedirectUrl();
        }

        return FALSE;
    }

    public function getReturnUrl()
    {
        return URL_HOSTEL;
    }

    public function getClassStudents(Request $request)
    {

       
       $course_subject_id = $request->course_subject_id;
       $record_type       = $request->record_type;

       $record  = CourseSubject::find($course_subject_id);

       $all_students  = App\Student::join('users','users.id','=','students.user_id')
                                ->where('academic_id',$record->academic_id)
                                ->where('course_parent_id',$record->course_parent_id) 
                                ->where('course_id',$record->course_id) 
                                ->where('current_year',$record->year) 
                                ->where('current_semister',$record->semister)
                                ->select(['users.name','roll_no','users.id'])
                                ->get();
         
        $temp     = [];
        $students = [];

        foreach ($all_students as $student) 
        {
           
           $assigned           = AssignmentAllocate::where('student_id',$student->id)->first();
           $temp['user']       = $student; 
           $temp['is_assined'] = 0; 

           if( $assigned && $record_type == 2 )
           {

              $temp['is_assined'] = 1; 
            
           }

           $students[] = $temp;

        }                          

       return $students;                         


    }

     public function processUpload(Request $request, $record, $file_name)
     {
        if(env('DEMO_MODE')) {
            return;
        }

        
         if ($request->hasFile($file_name)) {

          $examSettings     = new GeneralSettings();
          // dd($examSettings);
          
          $destinationPath  = $examSettings->assignmentPath();
          // dd($destinationPath);
          
          $fileName = ucwords($record->slug).'-'.$request->$file_name->getClientOriginalName();
          // dd($fileName);
          
          $request->file($file_name)->move($destinationPath, $fileName);

          $record->file_name  =  $fileName;
          $record->save();
         
      
        }
     }

     public function downloadFile($slug)
     {
        
        try {
          
           $record  = App\Assignments::where('slug',$slug)->first();

         $path   = public_path()."/uploads/assignments/".$record->file_name;
       
         return Response::download($path);

        } catch (Exception $e) {
           
           flash('Ooops..!','file_is_not_found_please_upload_new_one','overlay');
           return back();
        } 
        
     }

     public function viewAssignment($slug)
     {
        if(!checkRole(getUserGrade(3)))
      {
        prepareBlockUserMessage();
        return back();
      }
        
        $record    = Assignments::where('slug',$slug)->first();


        $students  = Assignments::join('assignments_allocate','assignments_allocate.assignment_id','=','assignments.id')
                                  ->join('users','users.id','=','assignments_allocate.student_id')
                                  ->join('students','students.user_id','=','users.id')
                                  ->join('subjects','subjects.id','=','assignments.subject_id')
                                  ->where('assignments_allocate.assignment_id',$record->id)
                                  ->select(['assignments.title','subject_title','users.name','deadline','assignments_allocate.id as allocate_id','assignments.id as assignment_id','assignments_allocate.user_file','assignments_allocate.updated_at as submitted_on','students.roll_no','credits','assignments_allocate.student_id','is_submitted','is_approved'])
                                  ->get();
        // dd($students);                          
        $data['active_class'] = 'assignments';
        if(checkRole(getUserGrade(2)))
        $data['active_class']       = 'users';
        $data['layout']       = getLayout();
        $data['title']        = getPhrase('view_assignment');
        $data['students']     = $students;
        $data['record']       = $record;

           $class  = CourseSubject::join('academics', 'academics.id','=','course_subject.academic_id')
                                  ->join('courses', 'courses.id','=','course_subject.course_id')
                                  ->where('course_subject.id',$record->course_subject_id)
                                  ->select(DB::raw("CONCAT(academic_year_title,'-',course_title) AS class_name"),'course_subject.id')
                                  ->first();

        $data['class_name']  =   ucwords($class->class_name);
        $data['user']        =   User::find($record->user_id);
        
        return view('assignments.assignment-marks', $data);
     }

     public function appoveAssignment(Request $request)
     {  
        // dd($request);
        $credits    = $request->credits; 
        $record     = Assignments::where('id',$request->assignment_id)->first();
        $allocates  = AssignmentAllocate::where('assignment_id',$record->id)->get();
         
         foreach ($allocates as $assignment) {
           
            $assignment->is_approved  = 1;
            $assignment->credits      = $credits[$assignment->id];
            $assignment->save();

            $user        = User::find($assignment->student_id);

             try {
             
               $user->notify(new \App\Notifications\StaffApproveAssignment($user,$record));
             
           } catch (Exception $e) {
              
              // dd($e->getMessage());
           }
         }

          flash('success','assignment_is_approved_successfully', 'success');
          return redirect(URL_ASSIGNMENTS);
     }

     public function appoveSingleAssignment(Request $request)
     {
         
         $assignment               = AssignmentAllocate::find($request->allocation_id);
         $assignment->is_approved  = 1;
         $assignment->credits      = $request->credits;
         $assignment->save();

         $user        = User::find($assignment->student_id);
         $assignment  = Assignments::find($assignment->assignment_id);

          try {
             
               $user->notify(new \App\Notifications\StaffApproveAssignment($user,$assignment));
             
           } catch (Exception $e) {
              
              // dd($e->getMessage());
           }

          flash('success','assignment_is_approved_successfully', 'success');
          return redirect(URL_ASSIGNMENTS);

     }

    /**
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function classnotesindex()
    {


      $user = getUserWithSlug();

     if(!checkRole(getUserGrade(3)))
     {
       prepareBlockUserMessage();
       return back();
     }

      if(!isEligible($user->slug))
       return back();

      $data['records']      = FALSE;
      $data['user']         = $user;
      $data['title']        = getPhrase('class_notes');
      $data['active_class'] = 'class_notes';
      $data['layout']       = getLayout();
      return view('staff.classnotes.list', $data);
    }
    public function classnotescreate()
    {
       
      $user = getUserWithSlug();

      if(!checkRole(getUserGrade(3)))
      {
        prepareBlockUserMessage();
        return back();
      }

       if(!isEligible($user->slug))
        return back();

      $students = User::whereIn('parent_id',[$user->id])
        ->select('name','id')
        ->get(); 

      //  print_r($students);

       $data['records']         = FALSE;
       $data['user']            = $user;
       $data['students']        = $students;
       $data['title']           = getPhrase('basic_health_details');
       $data['active_class']    = 'basic_health_details';
       $data['layout']          = getLayout();
       return view('parent.basichealth.basic_studhealth', $data);
    }

        /**
     * This method adds record to DB
     * @param  Request $request [Request Object]
     * @return void
     */
    public function classnotesstore(Request $request)
    {
      $user = getUserWithSlug();


      print_r($request->student_name);
          
    	  $record = new BasicStudhealth();
        $record->height 		      	= $request->height;
        $record->weight 		      	= $request->weight;
        $record->blood_group 		    = $request->blood_group;
        $record->student_id 			  = $request->student_name;
        $record->parent_id 			    = $user->id;
        $record->save();
        flash('success','record_added_successfully', 'success');
    	return redirect('children/basicstudenthealth');
    }

    public function classnotesedit($slug)
    {

      $id = $slug;
     
      $user = getUserWithSlug();

      if(!checkRole(getUserGrade(3)))
      {
        prepareBlockUserMessage();
        return back();
      }

        if(!isEligible($user->slug))
        return back();

      $students = User::whereIn('parent_id',[$user->id])
        ->select('name','id')
        ->get(); 
      
      $records =   DB::table('basic_studhealth as a')
                  ->join('users as b', 'a.student_id', 'b.id')
                  ->select('b.name','a.student_id','a.height','a.weight','a.blood_group','b.slug','a.id')
                  ->where('a.id',$id)
                  ->first();

        $data['records']         = $records;
        $data['user']            = $user;
        $data['students']        = $students;
        $data['title']           = getPhrase('basic_health_details');
        $data['active_class']    = 'basichealthdetails';
        $data['layout']          = getLayout();
        return view('parent.basichealth.basic_studhealth', $data);
    }

    public function classnotesupdate(Request $request, $slug)
    {
      $record = BasicStudhealth::find($slug);
      $record->height 		     	= $request->height;
      $record->weight 			    = $request->weight;
      $record->blood_group 			= $request->blood_group;
      $record->student_id 			= $request->student_name;
      $record->save();
      flash('success','record_updated_successfully', 'success');
    	return redirect('children/basicstudenthealth');
    }


}