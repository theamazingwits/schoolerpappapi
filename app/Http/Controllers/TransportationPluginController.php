<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App;
use App\Http\Requests;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\User;
use Exception;
use Schema;
use DB;


class TransportationPluginController extends Controller
{
    
  public function addTransportation()
  {
  	  

  	  DB::beginTransaction();

      try { 
            
            // Table1
      	    $query =   "CREATE TABLE `vroutes` (
						  `id` bigint(20) UNSIGNED NOT NULL,
						  `branch_id` int(10) DEFAULT NULL,
						  `name` varchar(255) DEFAULT NULL,
						  `cost` int(10) NOT NULL DEFAULT '0',
						  `parent_id` int(10) NOT NULL DEFAULT '0',
						  `sort_order` int(10) DEFAULT '0',
						  `description` text,
						  `created_at` timestamp NULL DEFAULT NULL,
						  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
						)"; 
						DB::statement($query);

		      $query  = "ALTER TABLE `vroutes` ADD PRIMARY KEY (`id`)";  
		                 DB::statement($query);

		      $query  = "ALTER TABLE `vroutes` MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5";  
		                 DB::statement($query); 

		      //Table2
		      
		       $query  = "CREATE TABLE `vechicles` (
						  `id` bigint(20) UNSIGNED NOT NULL,
						  `branch_id` int(10) DEFAULT NULL,
						  `number` varchar(100) DEFAULT NULL,
						  `model` varchar(50) DEFAULT NULL,
						  `year_made` varchar(100) DEFAULT NULL,
						  `driver_name` varchar(100) DEFAULT NULL,
						  `driver_license` varchar(100) DEFAULT NULL,
						  `driver_contact` varchar(100) DEFAULT NULL,
						  `seats` int(10) DEFAULT '0',
						  `description` text,
						  `created_at` timestamp NULL DEFAULT NULL,
						  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
						)";  
		                 DB::statement($query);
 
		     $query  = "ALTER TABLE `vechicles` ADD PRIMARY KEY (`id`)";  
		                 DB::statement($query);

		    $query =   "ALTER TABLE `vechicles` MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4";       DB::statement($query);           


		      //Table3
		      $query  = "CREATE TABLE `vechicle_assign` (
						  `id` bigint(20) UNSIGNED NOT NULL,
						  `vechicle_id` bigint(20) UNSIGNED NOT NULL,
						  `route_id` bigint(20) UNSIGNED NOT NULL,
						  `parent_route_id` bigint(20) UNSIGNED NOT NULL,
						  `created_at` timestamp NULL DEFAULT NULL,
						  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
						)";  
		                 DB::statement($query);

		      $query  = "ALTER TABLE `vechicle_assign` ADD PRIMARY KEY (`id`),
						  ADD KEY `vechicle_id` (`vechicle_id`),
						  ADD KEY `route_id` (`route_id`),
						  ADD KEY `parent_route_id` (`parent_route_id`)";  
						 
		                 DB::statement($query);


		    $query =  "ALTER TABLE `vechicle_assign`MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25";       DB::statement($query);


		     $query =  "ALTER TABLE `vechicle_assign`
						  ADD CONSTRAINT `vechicle_assign_ibfk_1` FOREIGN KEY (`route_id`) REFERENCES `vroutes` (`id`),
						  ADD CONSTRAINT `vechicle_assign_ibfk_2` FOREIGN KEY (`vechicle_id`) REFERENCES `vechicles` (`id`),
						  ADD CONSTRAINT `vechicle_assign_ibfk_3` FOREIGN KEY (`parent_route_id`) REFERENCES `vroutes` (`id`)";       

		               DB::statement($query);


		      //Table 4
		      $query  = "CREATE TABLE `vehicle_user` (
						  `id` bigint(20) UNSIGNED NOT NULL,
						  `user_id` bigint(20) UNSIGNED NOT NULL,
						  `vehicle_id` bigint(20) UNSIGNED NOT NULL,
						  `route_id` bigint(20) UNSIGNED NOT NULL,
						  `parent_route_id` bigint(20) UNSIGNED NOT NULL,
						  `is_active` tinyint(2) NOT NULL DEFAULT '1',
						  `is_stoped` tinyint(2) NOT NULL DEFAULT '0',
						  `created_at` timestamp NULL DEFAULT NULL,
						  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
						)";  
		                 DB::statement($query);

		      $query  = "ALTER TABLE `vehicle_user`
						  ADD PRIMARY KEY (`id`),
						  ADD KEY `user_id` (`user_id`),
						  ADD KEY `vehicle_id` (`vehicle_id`),
						  ADD KEY `route_id` (`route_id`),
						  ADD KEY `parent_route_id` (`parent_route_id`)";  
		                 DB::statement($query);

		    $query  = "ALTER TABLE `vehicle_user` MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5";      DB::statement($query); 

		     $query  = "ALTER TABLE `vehicle_user`
					  ADD CONSTRAINT `vehicle_user_ibfk_1` FOREIGN KEY (`vehicle_id`) REFERENCES `vechicle_assign` (`vechicle_id`),
					  ADD CONSTRAINT `vehicle_user_ibfk_2` FOREIGN KEY (`route_id`) REFERENCES `vechicle_assign` (`route_id`)";      

		             DB::statement($query);          

              
               //Table5
             
		       $query  = "CREATE TABLE `transport_fee_type` (
							  `id` bigint(20) UNSIGNED NOT NULL,
							  `route_id` bigint(20) UNSIGNED NOT NULL,
							  `type` tinyint(2) NOT NULL DEFAULT '0',
							  `created_at` timestamp NULL DEFAULT NULL,
							  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
							)";  
		                 DB::statement($query);

           
		    $query =   "ALTER TABLE `transport_fee_type` ADD PRIMARY KEY (`id`), ADD KEY `route_id` (`route_id`)"; 
                       DB::statement($query);

		      $query  = "ALTER TABLE `transport_fee_type` MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3";  
		                 DB::statement($query);
		     
		  

              //Table6
              
                $query  = "CREATE TABLE `transport_fee` (
						  `id` bigint(20) UNSIGNED NOT NULL,
						  `title` varchar(100) DEFAULT NULL,
						  `user_id` int(10) DEFAULT NULL,
						  `branch_id` int(10) DEFAULT NULL,
						  `vehicle_id` bigint(20) UNSIGNED NOT NULL,
						  `route_id` bigint(20) UNSIGNED NOT NULL,
						  `parent_route_id` bigint(20) UNSIGNED NOT NULL,
						  `vehicle_user_id` bigint(20) UNSIGNED NOT NULL,
						  `is_paid` tinyint(2) NOT NULL DEFAULT '0',
						  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
						  `paid_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
						  `paid_date` date DEFAULT NULL,
						  `discount` decimal(10,2) NOT NULL DEFAULT '0.00',
						  `balance` decimal(10,2) NOT NULL DEFAULT '0.00',
						  `year` varchar(20) DEFAULT NULL,
						  `month` varchar(20) DEFAULT NULL,
						  `start_date` date DEFAULT NULL,
						  `end_date` date DEFAULT NULL,
						  `created_at` timestamp NULL DEFAULT NULL,
						  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
						  `added_by` int(10) DEFAULT NULL,
						  `comments` text,
						  `payment_mode` varchar(20) DEFAULT NULL
						)";  
		                 DB::statement($query);

           
		    $query =   "ALTER TABLE `transport_fee`
						  ADD PRIMARY KEY (`id`),
						  ADD KEY `vehicle_id` (`vehicle_id`),
						  ADD KEY `route_id` (`route_id`),
						  ADD KEY `vehicle_user_id` (`vehicle_user_id`),
						  ADD KEY `parent_route_id` (`parent_route_id`)"; 
                       DB::statement($query);

		      $query  = "ALTER TABLE `transport_fee_type` MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3";  
		                 DB::statement($query);


		      $query  = "ALTER TABLE `transport_fee`
						  ADD CONSTRAINT `transport_fee_ibfk_1` FOREIGN KEY (`vehicle_id`) REFERENCES `vechicles` (`id`),
						  ADD CONSTRAINT `transport_fee_ibfk_2` FOREIGN KEY (`route_id`) REFERENCES `vroutes` (`id`),
						  ADD CONSTRAINT `transport_fee_ibfk_3` FOREIGN KEY (`vehicle_user_id`) REFERENCES `vehicle_user` (`id`)";  
		                 DB::statement($query);
             
           
           DB::commit();

           flash('success','hostel_plugin_added_successfully', 'overlay');

      }

       catch ( Exception $e ) {

            DB::rollBack();
              // dd($e->getMessage());
            flash('success','hostel_plugin_added_successfully', 'overlay');
             
        }

        return redirect( URL_USERS_LOGIN );
  }

}