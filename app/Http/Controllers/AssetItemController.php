<?php
namespace App\Http\Controllers;
use \App;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Assets;
use App\AssetCategory;
use App\AssetLocation;
use App\LogHelper;
use Yajra\Datatables\Datatables;
use DB;
use Auth;
use Carbon\Carbon;


class AssetItemController extends Controller
{
    
    public function __construct()
    {
    	$this->middleware('auth');
    }

    /**
     * Course listing method
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
      if(!checkRole(getUserGrade(17)))
      {
        prepareBlockUserMessage();
        return back();
      }
        $data['active_class']       = 'assets';
        $data['layout']             = getLayout();
        $data['title']              = getPhrase('assets');

        return view('assets.items.list', $data);

          
    }

    /**
     * This method returns the datatables data to view
     * @return [type] [description]
     */
    public function getDatatable($slug = '')
    {

      if(!checkRole(getUserGrade(17)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $records = array();
 
      
        $records = Assets::select(['serial_no','title','status','category_id','location_id','added_by','slug']);
    
            
        $records->orderBy('updated_at', 'desc');
             

        return Datatables::of($records)
        ->addColumn('action', function ($records) {
         
          $link_data = '<div class="dropdown more">
                        <a id="dLabel" type="button" class="more-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-ellipsis-v"></i>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <li><a href="'.URL_ASSET_ITEMS_EDIT.$records->slug.'"><i class="fa fa-pencil"></i>'.getPhrase("edit").'</a></li>';
                            
                           $temp = '';
                           if(checkRole(getUserGrade(2))) {
                    $temp .= ' <li><a href="javascript:void(0);" onclick="deleteRecord(\''.$records->slug.'\');"><i class="fa fa-trash"></i>'. getPhrase("delete").'</a></li>';
                      }
                    
                    $temp .='</ul></div>';

                    $link_data .=$temp;
            return $link_data;
            })

        ->editColumn('status', function($records){

             if($records->status == 0)
                return 'In Storage';
            return 'Checkout';
        })

         ->editColumn('category_id', function($records){
             
             $record  = AssetCategory::find($records->category_id);
             if($record)
                return ucwords($record->name);
            return '-';
        })

           ->editColumn('location_id', function($records){
             
             $record  = AssetLocation::find($records->location_id);
             if($record)
                return ucwords($record->location);
            return '-';
        })

        ->removeColumn('slug')
        ->make();
    }

    /**
     * This method loads the create view
     * @return void
     */
    public function create()
    {
      if(!checkRole(getUserGrade(17)))
      {
        prepareBlockUserMessage();
        return back();
      }
        $data['record']       = FALSE;
        $data['active_class'] = 'assets';
        $data['layout']       = getLayout();
        $data['title']        = getPhrase('add_asset');
        $data['status']       = array('0'=>'In Storage','1'=>'Check Out');
        $data['condition']    = array('0'=>'New','1'=>'Used');
        $data['categories']   = AssetCategory::pluck('name','id')->toArray();
        $data['locations']    = AssetLocation::pluck('location','id')->toArray();

         return view('assets.items.add-edit', $data);

       
    }

    /**
     * This method loads the edit view based on unique slug provided by user
     * @param  [string] $slug [unique slug of the record]
     * @return [view with record]       
     */
    public function edit($slug)
    {
      if(!checkRole(getUserGrade(17)))
      {
        prepareBlockUserMessage();
        return back();
      }
    
    // dd($slug);
        $record = Assets::where('slug',$slug)->first();

        if($isValid = $this->isValidRecord($record))
            return redirect($isValid);

        $data['record']       = $record;
        $data['active_class'] = 'assets';
        $data['layout']       = getLayout();
        $data['title']        = getPhrase('edit_asset');
        $data['status']       = array('0'=>'In Storage','1'=>'Check Out');
        $data['condition']    = array('0'=>'New','1'=>'Used');
        $data['categories']   = AssetCategory::pluck('name','id')->toArray();
        $data['locations']    = AssetLocation::pluck('location','id')->toArray();

         return view('assets.items.add-edit', $data);

        
    }

    
    /**
     * [update description]
     * @param  Request $request [description]
     * @param  [type]  $slug    [description]
     * @return [type]           [description]
     */
    public function update(Request $request, $slug)
    {
        // dd($request);
      if(!checkRole(getUserGrade(17)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $record = Assets::where('slug',$slug)->first();

        if($isValid = $this->isValidRecord($record))
            return redirect($isValid);

            $title = $request->title;
        if($title != $record->title)
            $record->slug = $record->makeSlug($title);

        $record->update($request->all());


        $record->flag        = 'Update';
        $record->action      = 'Assets';
        $record->object_id   =  $record->slug;
        $logs = new LogHelper();
        $logs->storeLogs($record);

        $this->processUpload($request, $record, 'image');

     
       
        flash('success','asset_updated_successfully', 'success');
        return redirect(URL_ASSET_ITEMS);
    }

    /**
     * This method adds record to DB
     * @param  Request $request [Request Object]
     * @return void
     */
    public function store(Request $request)
    {
      if(!checkRole(getUserGrade(17)))
      {
        prepareBlockUserMessage();
        return back();
      }
    
     // dd($request);
      $record        =   Assets::create($request->all());
      $record->slug  = $record->makeSlug($request->title);
      $record->save();

      $record->flag        = 'Insert';
      $record->action      = 'Assets';
      $record->object_id   =  $record->slug;
      $logs = new LogHelper();
      $logs->storeLogs($record);

      $this->processUpload($request, $record, 'image');
        
        flash('success','asset_added_successfully', 'success');
        return redirect(URL_ASSET_ITEMS);
    }
 
    /**
     * Delete Record based on the provided slug
     * @param  [string] $slug [unique slug]
     * @return Boolean 
     */
    public function delete($slug)
    {
      if(!checkRole(getUserGrade(17)))
      {
        prepareBlockUserMessage();
        return back();
      }
      /**
       * Delete the questions associated with this quiz first
       * Delete the quiz
       * @var [type]
       */
         $record = Assets::where('slug',$slug)->first();
        try{
            if(!env('DEMO_MODE')) {
                $record->delete();
            }

            $record->flag        = 'Delete';
            $record->action      = 'Assets';
            $record->object_id   =  $slug;
            $logs = new LogHelper();
            $logs->storeLogs($record);

            $response['status'] = 1;
            $response['message'] = getPhrase('record_deleted_successfully');
        }
         catch ( Exception $e) {
                 $response['status'] = 0;
           if(getSetting('show_foreign_key_constraint','module'))
            $response['message'] =  $e->getMessage();
           else
            $response['message'] =  getPhrase('this_record_is_in_use_in_other_modules');
       }
        return json_encode($response);
    }

    public function isValidRecord($record)
    {
        if ($record === null) {

            flash('Ooops...!', getPhrase("page_not_found"), 'error');
            return $this->getRedirectUrl();
        }

        return FALSE;
    }

    public function getReturnUrl()
    {
        return URL_ASSET_ITEMS;
    }

     public function processUpload(Request $request, $record, $file_name)
     {
        if(env('DEMO_MODE')) {
            return;
        }

        
         if ($request->hasFile($file_name)) {

        
          $destinationPath  = "public/uploads/assets/";
          
          $fileName = ucwords($record->slug).'-'.$request->$file_name->getClientOriginalName();
          
          $request->file($file_name)->move($destinationPath, $fileName);

          $record->image  =  $fileName;

          $record->save();
         
      
        }
     }

}