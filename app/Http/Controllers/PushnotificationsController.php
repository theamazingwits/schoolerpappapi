<?php
namespace App\Http\Controllers;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Dacastro4\LaravelGmail;
use Illuminate\Http\Request;

use App\Http\Requests;
use App;
use Auth;
use App\User;

use SMS;
use DB;
use Charts;
use Artisan;
 
// use Codedge\Fpdf\Facades\Fpdf;

// use Illuminate\Support\Facades\App;

class PushnotificationsController extends Controller
{

  function getGmail(){
    return view('chat.gmail');
  }
  function sendTrackPushNotification(Request $request){
     $parent = App\User::where('id',$request->student_id)
                                ->select(['parent_id','name'])
                                ->first();
      $push_token = App\User::where('id',$parent->parent_id)
                                ->select(['push_web_id'])
                                ->first();

    $content = array(
      "en" => $request->message,
    );
    $fields = array(

      'app_id' => "db1e0bb3-0974-41d4-b542-424642a3776f",
      'include_player_ids' => array($push_token->push_web_id),
      'large_icon' => "http://clsweb.amazingwits.com/images/logocls.png",
      'contents' => $content
      
    );

    $fields = json_encode($fields);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                           'Authorization: Basic MDVhOTY3Y2UtZTBkZS00NzAzLThhOTQtZjMzNWFmZDM4NWI1'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);    
  
    $response = curl_exec($ch);
    echo "Response-->  ".$response;
    curl_close($ch);
    $values = array(

        'user_id' => $parent->parent_id, 
        'message' => $request->message , 
        'payload' => $fields ,
        'created_at' => date('Y-m-d H:i:s') , 
        'updated_at' => date('Y-m-d H:i:s')
      );
      DB::table('push_notifications')
      ->insert($values);
    return response()->json(['status' => 'success', 'message' => 'Notification Send Successfully']);
  }


  function sendTripPushNotification(Request $request){

    if($request->status == "1"){
      $tripvalues = array(
        'vehicle_id' => $request->vehicle_id, 
        'driver_id' => $request->driver_id, 
        'trip_status' => $request->status,
        'trip_start' =>  date('Y-m-d H:i:s'),
        'created_at' => date('Y-m-d H:i:s') , 
        'updated_at' => date('Y-m-d H:i:s')
      );
    }else if($request->status == "2"){
      $tripvalues = array(
        'vehicle_id' => $request->vehicle_id, 
        'driver_id' => $request->driver_id, 
        'trip_status' => $request->status,
        'trip_end' =>  date('Y-m-d H:i:s'),
        'created_at' => date('Y-m-d H:i:s') , 
        'updated_at' => date('Y-m-d H:i:s')
      );
    }

    DB::table('trip_details')
      ->insert($tripvalues);
     
    $studentId = DB::table('vehicle_user as vehicle')
                ->join('users','users.id','=','vehicle.user_id')
                ->where('vehicle.vehicle_id',$request->vehicle_id)
                ->select(['users.parent_id' ,'users.name'])
                ->get();
    $parentId = [];
        foreach ($studentId as $parents) {
          $parentId[] = $parents->parent_id;
        } 
    $all_parents = App\User::whereIn('id',$parentId)
                    ->select(['push_web_id','id'])
                    ->get();
    $pushwebID = [];
        foreach ($all_parents as $pushweb) {
          $pushwebID[] = $pushweb->push_web_id;
        } 
    if($request->status == "1"){
      $message = "Your children school bus trip has been started."; 
      
    }else if($request->status == "2"){
      $message = "Your children school bus trip has been completed.";
    }
    $content = array(
        "en" => $message,
      );
    $fields = array(

      'app_id' => "db1e0bb3-0974-41d4-b542-424642a3776f",
      'include_player_ids' => $pushwebID,
      'large_icon' => "http://clsweb.amazingwits.com/images/logocls.png",
      'contents' => $content
      
    );

    $fields = json_encode($fields);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                           'Authorization: Basic MDVhOTY3Y2UtZTBkZS00NzAzLThhOTQtZjMzNWFmZDM4NWI1'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);    
  
    $response = curl_exec($ch);
    //echo "Response-->  ".$response;
    curl_close($ch);

    $values = [];
    foreach ($all_parents as $parents) {
      $values[] = [
        'user_id' => $parents->id, 
        'message' => $message , 
        'payload' => $fields ,
        'created_at' => date('Y-m-d H:i:s') , 
        'updated_at' => date('Y-m-d H:i:s')
      ];
    }
    
      DB::table('push_notifications')
      ->insert($values);

    return response()->json(['status' => 'success', 'message' => 'Trip Started Successfully']);
  }

  function getTripData(Request $request){
    $tripdata = DB::table('trip_details')
                ->where('vehicle_id',$request->vehicle_id)
                ->where('driver_id',$request->driver_id)
                ->where('trip_status','1')
                ->get();
    if(count($tripdata) > 0){
      $response = response()->json(['status' => 'success', 'message' => 'Trip Started Successfully','tripdetails'=>$tripdata]);  
    }else{
      $response = response()->json(['status' => 'failure', 'message' => 'No details found']);  
    }
    
    return $response;
  }
}
