<?php
namespace App\Http\Controllers;
use \App;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\RoomType;
use App\HostelRoom;
use App\Hostel;
use Yajra\Datatables\Datatables;
use DB;
use Auth;
use Carbon\Carbon;


class HostelRoomController extends Controller
{
    
    public function __construct()
    {
    	$this->middleware('auth');
    }

    /**
     * Course listing method
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
      if(!checkRole(getUserGrade(21)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $data['active_class']       = 'hostel';
        $data['layout']             = getLayout();
        $data['title']              = getPhrase('hostel_rooms');

             return view('hostel.hostelroom.list', $data);

          
    }

    /**
     * This method returns the datatables data to view
     * @return [type] [description]
     */
    public function getDatatable($slug = '')
    {

      if(!checkRole(getUserGrade(21)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $records = array();
 
        if(checkRole('owner') && Hostel::isMultiBranch() ) {
            $records = HostelRoom::select(['branch_id', 'room_number','hostel_id','room_type_id','cost','beds','id']);
        } else {
            // $records = HostelRoom::select(['room_number','hostel_id','room_type_id','cost','beds','id'])->where('branch_id', getUserRecord()->branch_id);

             $records = HostelRoom::select(['room_number','hostel_id','room_type_id','cost','beds','id']);
        }
            
        $records->orderBy('updated_at', 'desc');
             

        return Datatables::of($records)

          ->addColumn('available_beds', function ($records) {
             
              $avaialbe_rums  = App\HostelUser::where('room_id',$records->id)->where('is_vacate',0)->get()->count();
              return $records->beds - $avaialbe_rums;
          })
        ->addColumn('action', function ($records) {
         
          $link_data = '<div class="dropdown more">
                        <a id="dLabel" type="button" class="more-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-ellipsis-v"></i>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <li><a href="'.URL_HOSTEL_ROOMS_EDIT.$records->id.'"><i class="fa fa-pencil"></i>'.getPhrase("edit").'</a></li>';
                            
                           $temp = '';
                           if(checkRole(getUserGrade(2))) {
                    $temp .= ' <li><a href="javascript:void(0);" onclick="deleteRecord(\''.$records->id.'\');"><i class="fa fa-trash"></i>'. getPhrase("delete").'</a></li>';
                      }
                    
                    $temp .='</ul></div>';

                    $link_data .=$temp;
            return $link_data;
            })

        // ->editColumn('branch_id', function($records){
        
        //   $branch_rec = App\Branch::getBranchRecord($records->branch_id);

        //   return $branch_rec['branch_name'].' ('.$branch_rec['address'].')';
        // })

        ->editColumn('hostel_id', function($records){

            $hstl  = Hostel::findOrFail($records->hostel_id);
            if($hstl)
            return '<a href="'.URL_HOSTEL.'">'.$hstl->name.'</a>';

            return '-';
        })

        ->editColumn('room_type_id', function($records){

            $rum  = RoomType::findOrFail($records->room_type_id);
            if($rum)
            return $rum->room_type;

            return '-';
        })

         ->editColumn('cost', function($records){
          
          return getCurrencyCode().' '.(int)$records->cost;
           
        })
         
        ->removeColumn('id')
        ->make();
    }

    /**
     * This method loads the create view
     * @return void
     */
    public function create()
    {
        
      if(!checkRole(getUserGrade(21)))
      {
        prepareBlockUserMessage();
        return back();
      }
        $data['record']             = FALSE;
         if( Hostel::isMultiBranch() ){

        $data['branches']           = addSelectToList(getBranches());
        }else{

        $data['branches']           = null;
        }

        $data['hostels_list']       = Hostel::pluck('name','id')->toArray();
        $data['roomtype_list']      = RoomType::pluck('room_type','id')->toArray();

        $data['active_class']       = 'hostel';
        $data['layout']             = getLayout();
        $data['title']              = getPhrase('create_hostel_room');

         return view('hostel.hostelroom.add-edit', $data);


       
    }

    /**
     * This method loads the edit view based on unique slug provided by user
     * @param  [string] $slug [unique slug of the record]
     * @return [view with record]       
     */
    public function edit($id)
    {
      if(!checkRole(getUserGrade(21)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $record = HostelRoom::findOrFail($id);
        if($isValid = $this->isValidRecord($record))
            return redirect($isValid);

        $data['record']              = $record;
         if( Hostel::isMultiBranch() ){

        $data['branches']           = addSelectToList(getBranches());
        }else{

        $data['branches']           = null;
        }
        $data['active_class']        = 'hostel';
        $data['hostels_list']       = Hostel::pluck('name','id')->toArray();
        $data['roomtype_list']      = RoomType::pluck('room_type','id')->toArray();
      
        $data['layout']             = getLayout();
        $data['title']              = getPhrase('edit_room_type');

          return view('hostel.hostelroom.add-edit', $data);

        
    }

    /**
     * Update record based on slug and reuqest
     * @param  Request $request [Request Object]
     * @param  [type]  $slug    [Unique Slug]
     * @return void
     */
    public function update(Request $request, $id)
    {
      if(!checkRole(getUserGrade(21)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $record = HostelRoom::findOrFail($id);
        $record->update($request->all());
       
        flash('success','room_type_updated_successfully', 'success');
        return redirect(URL_HOSTEL_ROOMS);
    }

    /**
     * This method adds record to DB
     * @param  Request $request [Request Object]
     * @return void
     */
    public function store(Request $request)
    {
      if(!checkRole(getUserGrade(21)))
      {
        prepareBlockUserMessage();
        return back();
      }

        HostelRoom::create($request->all());
        
        flash('success','room_is_added_successfully', 'success');
        return redirect(URL_HOSTEL_ROOMS);
    }
 
    /**
     * Delete Record based on the provided slug
     * @param  [string] $slug [unique slug]
     * @return Boolean 
     */
    public function delete($id)
    {
      if(!checkRole(getUserGrade(21)))
      {
        prepareBlockUserMessage();
        return back();
      }
      /**
       * Delete the questions associated with this quiz first
       * Delete the quiz
       * @var [type]
       */
        $record = HostelRoom::where('id', $id)->first();
        try{
            if(!env('DEMO_MODE')) {
                $record->delete();
            }
            $response['status'] = 1;
            $response['message'] = getPhrase('record_deleted_successfully');
        }
         catch ( Exception $e) {
                 $response['status'] = 0;
           if(getSetting('show_foreign_key_constraint','module'))
            $response['message'] =  $e->getMessage();
           else
            $response['message'] =  getPhrase('this_record_is_in_use_in_other_modules');
       }
        return json_encode($response);
    }

    public function isValidRecord($record)
    {
        if ($record === null) {

            flash('Ooops...!', getPhrase("page_not_found"), 'error');
            return $this->getRedirectUrl();
        }

        return FALSE;
    }

    public function getReturnUrl()
    {
        return URL_HOSTEL_ROOMS;
    }
}