<?php

namespace App\Http\Controllers;
use \App;
use Illuminate\Http\Request;
use App\Http\Requests;
use Yajra\Datatables\Datatables;
use DB;
use App\User;
use App\Staff;
use App\StaffAttendance;
use Auth;

class StaffAttendanceController extends Controller{
   
     public function __construct()
    {
    	$this->middleware('auth');
    }

    /**
     * Select the details
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
       
        if(!checkRole(getUserGrade(2)))
        {
          prepareBlockUserMessage();
          return back();
        }

       
        $data['branches']            = addSelectToList(getBranches());
        $others                      = array('2','3','7','8','9');
        $data['roles']               = App\Role::whereIn('id', $others)->pluck('display_name','id');
        $data['current_academic_id'] = getDefaultAcademicId();
        $data['layout']              = getLayout();
        $data['active_class']        = 'staff_attendance';
        $data['title']               = getPhrase('staff_attendance');
        $data['module_helper']       = getModuleHelper('staff_attendance');
        // dd($data);

        // $view_name = getTheme().'::staff-attendance.select-particulars';
        // return view($view_name,$data);

        return view('staff-attendance.select-particulars', $data);

    }

    /**
     * View the branch staff based selections
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function viewBranchStaff(Request $request)
    {
       
       $branch_id          = $request->branch_id;
       $course_parent_id   = null;
       if($request->has('course_parent_id'))
       $course_parent_id   = $request->course_parent_id;

       $role_id            = $request->role_id;
       $attendance_date    = $request->attendance_date;

        if($request->branch_id==''|| $request->role_id=='')
        {

            flash('Oops...!','Please Select The Details', 'overlay');
            return redirect()->back()->withInput($request->except('_token'));

        }

       
        $this->validate($request, [
         'attendance_date'       => 'bail|required',
         ]);
         
      
        /**
         * Find wether the attendance is already added for the day or not
         * @var [type]
         */
        $att_records = $this->isAttendanceAlreadyTaken( $request );
        
        $data['attendance_taken'] = FALSE;
        if(count($att_records))
            $data['attendance_taken'] = TRUE;

        // dd($att_records);
        
        $data['attendance_records'] = $att_records;
        $submitted_data = array(
                               
                                'attendance_date'   => $attendance_date,
                                'branch_name'       => getbranchname($branch_id,'yes'),
                                'branch_id'         => $branch_id, 
                                'role_id'           => $role_id, 
                                'course_parent_id'  => $course_parent_id, 

                                );

        $staffObject         = new App\Staff;
        $staff               = $staffObject->getStaff( $branch_id, $role_id ,$course_parent_id );
        
        // dd($staff); 
        $data['submitted_data'] = (object)$submitted_data;

        $data['staff']          = $staff;
        $data['title']          = getbranchname($branch_id,'yes');
        $data['role_name']      = ucwords(getRoleData($role_id));
        $data['layout']         = getLayout();
        $data['record']         = FALSE;
        $data['active_class']   = 'staff_attendance';
         
        if(count($staff)){
         
           $view_name = getTheme().'::staff-attendance.list';
        return view($view_name,$data);
      }
       
       else
        flash('Oops...!','no_staff_available', 'overlay');
        return redirect( URL_STAFF_ATTENDENCE_SELECTS );


    }


   

    /**
     * This method get branch courses based on 
     * branch id
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getBranchParentCourses(Request $request)
    {
       	
    	$records   = getBranchParentCourses();
        
        if($records){

    	return $records;

       }

    }

    
    /**
     * Update attendance for Staff
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function updateAttendance(Request $request)
    {
        // dd($request);

        if( count( $this->isAttendanceAlreadyTaken( $request ) ) ) 
        {
            //Data exists, remove all data
            $this->isAttendanceAlreadyTaken($request, TRUE );
        }



        $course_parent_id   = null;
        if($request->course_parent_id)
        $course_parent_id   = $request->course_parent_id;

        $branch_id               = $request->branch_id;
        $role_id                 = $request->role_id;
        $attendance_date         = $request->attendance_date;
        $attendance_code_records = $request->attendance_code;
        $remarks                 = $request->remarks;
        $notes                   = $request->notes;
        $user_id                 = Auth::User()->id;
        

        foreach($attendance_code_records as $key => $value)
        {
           
            $attendance                     = new StaffAttendance();  
            $attendance->branch_id          = $branch_id;
            $attendance->role_id            = $role_id;
            $attendance->employee_id        = getEmployeeId($key);
            if($role_id == 3){
             
             $record   = getStaffCourse($branch_id,$key);
             $attendance->course_parent_id   = $record->course_parent_id;
           }

            $attendance->date               = $attendance_date;
            $attendance->user_id            = $key; 
            $attendance->attendance_code    = $value; 
            $attendance->remarks            = $remarks[$key]; 
            $attendance->notes              = $notes[$key]; 
            $attendance->updated_by         = $user_id;
            $attendance->save();
            
        }


        flash('success','staff_attendance_updated_successfully', 'success');
        return redirect( URL_STAFF_ATTENDENCE_SELECTS );

    }

    /**
     * If attendance is alredy available delete and update it
     * @param  [type]  $request [description]
     * @param  boolean $delete  [description]
     * @return boolean          [description]
     */
    public function isAttendanceAlreadyTaken($request,$delete = FALSE)
    {
        

        $branch_id          = $request->branch_id;

        $course_parent_id   = null;
        if($request->course_parent_id)
        $course_parent_id   = $request->course_parent_id;

        $role_id            = $request->role_id;
        $attendance_date    = $request->attendance_date;
        
        if($course_parent_id){
         
          $data   =  StaffAttendance::where('branch_id','=', $branch_id)
                                      ->where('role_id','=', $role_id)
                                      ->where('course_parent_id','=', $course_parent_id)
                                      ->where('date','=', $attendance_date);
         }
         else{
            
              $data   =  StaffAttendance::where('branch_id','=', $branch_id)
                                      ->where('role_id','=', $role_id)
                                      ->where('date','=', $attendance_date);
         }                              
         // dd($data);
        if(!$delete) {
            return $data->get();
        }

        return $data->delete();

    }


}