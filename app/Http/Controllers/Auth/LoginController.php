<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use \Auth;
use App\User;
use DB;
use Exception;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    //use AuthenticatesUsers;
    use AuthenticatesUsers {
        logout as performLogout;
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
   protected $redirectTo = PREFIX;
    protected $dbuser = '';
    protected $provider = '';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);     
    }
    
    public function getRegistration(){
      return view('registration.registration');
    }
       
     //this view the login page     
     public function getLogin()
    {

        $data['app_name'] = env('APP_NAME');
        return view('auth.login',$data);
    }


// This method auhtenticate the registered user
    public function postLogin(Request $request)
    {       
       
       // dd($request);

        if($request->auth_id == "false"){

          $message = getPhrase("you_need_admin_approval_to_access_your_account");
          flash('Ooops...!', $message, 'overlay');
           return redirect()->back();
        }
        $login_status = FALSE;
        if (Auth::attempt(['username' => $request->email, 'password' => $request->password,'status'=>1])) {
                // return redirect(PREFIX);
                $login_status = TRUE;
                $values = array(
                    'push_web_id' => $request->push_web_id
                );
                $record = User::where('username', $request->email)
                ->update($values);
        } 

        elseif (Auth::attempt(['email'=> $request->email, 'password' => $request->password,'status'=>1])) {
            $login_status = TRUE;
             $values = array(
                    'push_web_id' => $request->push_web_id
                );
                DB::table('users')
                ->where('email', $request->email)
                ->update($values);
        }

        if(!$login_status) 
        {
            $message = getPhrase("Please Check Your Details");
            flash('Ooops...!', $message, 'error');
               return redirect()->back();


            //    return redirect()->back()
            // ->withInput($request->only($this->loginUsername(), 'remember'))
            // ->withErrors([
            //     $this->loginUsername() => $this->getFailedLoginMessage(),
            // ]);
        }
        if($login_status){
             
             $user  = Auth::user();
             
             if($user->login_enabled == 0){

                  $message = getPhrase("you_need_admin_approval_to_access_your_account");
                  flash('Ooops...!', $message, 'overlay');
                   return redirect()->back();
            }

               
        }

        /**
         * Check if the logged in user is parent or student
         * if parent check if admin enabled the parent module
         * if not enabled show the message to user and logout the user
         */
        
        if($login_status) {
            if(checkRole(getUserGrade(7)))  {
               if(!getSetting('parent', 'module')) {
                return redirect(URL_PARENT_LOGOUT);
               }
            } 
        }

        /**
         * The logged in user is student/admin/owner
         */
            if($login_status)
            {
                session()->put('is_student', '0');
                if(checkRole(getUserGrade(5)))
                {

                    $user = User::where('email','=',$request->email)->first();
                    //echo json_encode($user);
                    if($user)
                    {
                        session()->put('is_student', '1');
                        session()->put('user_record', prepareStudentSessionRecord($user->slug));
                        
                    }
                }
                
                return redirect(PREFIX);
            } 
    }
    
    public function forgotpassword()
    {
        return view('auth.passwords.email');
    }
    
    public function forgotpasswordEmail( Request $request )
    {
        $details = User::where('email', '=', $request->email)->first();
        if( $details )
        {
            if( $details->status == 0 )
            {
                flash('Ooops...!', 'Admin has suspended your account. Please contact administrator', 'error');
            }
            else
            { 

                $forgot_token = str_random(30);
                $details->remember_token = $forgot_token;
                $random_password = str_random(10);
                $details->save();
                $login_link = URL_USERS_LOGIN;
                $changepassword_link = URL_USERS_RESETPASSWORD . '/' . $forgot_token;
                $site_title = getSetting('site_title', 'site_settings');
                try{
                    sendEmail('forgotpassword', array('user_name'=>$details->name, 'to_email' => $details->email, 'password' => $random_password, 'login_link' => $login_link, 'changepassword_link' =>  $changepassword_link, 'site_title' => $site_title));
                    flash('success...!', 'reset_password_sent_to_your_mail', 'success');
                }
                catch(Exception $ex)
                {
                    flash('Ooops...!', 'there_was_a_error : ' . $ex->getMessage(), 'error');
                }
            }           
            return redirect( URL_USERS_LOGIN );
        }
        else
        {
            flash('Ooops...!', 'We have not found your email address', 'error');
            return redirect( URL_USERS_LOGIN );
        }
    }
    
    public function resetpassword( $forgot_token )
    {
        $details = User::where('remember_token', '=', $forgot_token)->first();
        if( $details )
        {
            $data['token'] = $forgot_token;
            $data['main_active']    = 'register';
            return view('auth.passwords.reset', $data);
        }
        else
        {
            flash('Ooops...!', 'link is not valid. please check your email for details', 'error');
            return redirect( URL_USERS_LOGIN );
        }
    }
    
    public function resetmypassword(Request $request)
    {
        $this->validate($request, [
        'password'  => 'required|min:6|confirmed',
        'password_confirmation'  => 'required|min:6',
        ]);
        $details = User::where('remember_token', '=', $request->token)->first();
        if( $details )
        {
            $details->password = bcrypt($request->password);
            $details->remember_token = null;
            $details->status = 1;
            $details->save();
            flash('Congrulations...!', 'You have successfully reset your password. Please login here.', 'success');
            return redirect( URL_USERS_LOGIN);
        }
        else
        {
            flash('Ooops...!', 'link is not valid. please check your email for details', 'error');
            return redirect( URL_USERS_LOGIN );
        }
    }



     public function resetUsersPassword(Request $request)
     {
        // dd($request);
         $user  = User::where('email','=',$request->email)->first();
          $request_url = $_SERVER['HTTP_REFERER'];
// dd($request_url);
         
          DB::beginTransaction();

         try{

         if($user!=null){

           $password       = str_random(8);
           $user->password = bcrypt($password);

           $user->save();
           
           DB::commit();

           $user->notify(new \App\Notifications\UserForgotPassword($user,$password));

           flash('Success', 'new_password_is_sent_to_your_email_account', 'success');

         }

         else{

            flash('Ooops','email_is_not_existed','error');
            
         }
      }

      catch(Exception $ex){
          DB::rollBack();
// dd($e->getMessage());
         flash('oops...!', $ex->getMessage(), 'error');

      }
       

       
        return redirect($request_url);
         
       
         
     }

        public function authenticate(Request $request)
    {  
       
        $response = [];
        $response['status']  = 0;
        $response['message'] = 'Email/Password is incorrect';
        $user_object = null;
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                // return redirect(PREFIX);
            $user_object = Auth::user();                
        }
        else if(Auth::attempt(['username' => $request->email, 'password' => $request->password]))
        {
            $user_object = Auth::user();
        }
     
        if($user_object)
        {
            if( $user_object->role_id == 6 || $user_object->role_id == 5 || $user_object->role_id == 3 )
            {
               
                if ($user_object->status==0) {

                    $response = [];
                    $response['status']  = 0;
                    $response['account'] = 0;
                    $response['message'] = 'Account is inactive';
                    return $response;
                }
           
                if ($request->device_id) {

                   $user_object->device_id = $request->device_id;
                   $user_object->save();
                }


                $user['id'] = $user_object->id;
                $user['name'] = $user_object->name;
                $user['email'] = $user_object->email;
                $user['phone'] = $user_object->phone;
                $user['image'] = $user_object->image;
                $login_status = TRUE;

               
                $response['status'] = 1;
                if ($user_object->role_id == 6) {
                    $response['user_type'] = "parent";
                }
                elseif ($user_object->role_id == 5) {

                    $response['user_type'] = "student";
                    $student_record = \App\Student::where('user_id','=',$user_object->id)->first();

                    if (!$student_record->academic_id) {

                        $response = [];
                        $response['status']  = 0;
                        $response['message'] = 'Academic details not updated';
                        return $response;
                    }
                }  else {
                    $response['user_type'] = "staff";
                }

                $tab_status = array(
                        'os_type' => $request->os_type,
                        'push_app_id' => $request->push_token,
                        'updated_at' => date('Y-m-d H:i:s')
                        );
                DB::table('users')
                ->where('email',$request->email)
                ->update($tab_status);

                $response['user'] = $user;
                $response['message'] = 'Login Success';

            }
            else{


                $response['message'] = 'This credentials are not allowed';
                return $response;
            }
        }

        return $response;
    }

    public function logout($id)
 {
        $values = array(
                    'push_web_id' => null
                );
        $record = DB::table('users')
        ->where('id', $id)
        ->update($values);

        $response['status']   = 1;
        $response['message']    = 'successfully Logout';
        return $response;
 }



}
