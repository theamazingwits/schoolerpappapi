<?php
namespace App\Http\Controllers;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Illuminate\Http\Request;

use App\Http\Requests;
use App;
use Auth;
use App\User;

use SMS;
use DB;
use Charts;
use Artisan;
 
// use Codedge\Fpdf\Facades\Fpdf;

// use Illuminate\Support\Facades\App;

class TrackingController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth');
    }
    public function getTrack(){
      $data['active_class']       = 'tracking';
      $data['layout']             = getLayout();
      $data['title']              = getPhrase('tracking');
      $slug = Auth::user()->slug;
      $user = App\User::where('slug','=',$slug)->first();
      $role = getRoleData($user->role_id);
        if($role == 'parent'){
            $students = App\User::whereIn('parent_id',[$user->id])
                                ->select(['id'])
                                ->get();
             $vehiclelist = App\VehicleUser::join('users', 'users.id','=','vehicle_user.user_id')
                ->join('vdrivers','vdrivers.vehicle_id','=','vehicle_user.vehicle_id')
                ->join('vechicles','vechicles.id','=','vehicle_user.vehicle_id')
                ->whereIn('users.id',$students)
                ->where('vehicle_user.is_active','1')
                 ->select(['vehicle_user.vehicle_id','vdrivers.name as drivername','users.id','vdrivers.id','users.name as studentname','vechicles.number','vechicles.trip_status'])
                ->get();
        $data['vehiclelist'] = $vehiclelist;

        }else{
            $vehiclelist = App\Vechicle::join('vdrivers', 'vdrivers.vehicle_id','=','vechicles.id')
                // ->join('vdrivers','vdrivers.vehicle_id','=','vehicle_user.vehicle_id')
                // ->join('vechicles','vechicles.id','=','vehicle_user.vehicle_id')
                // ->whereIn('users.id',$students)
                // ->where('vehicle_user.is_active','1')
                 ->select(['vdrivers.vehicle_id','vdrivers.name as drivername','vdrivers.id','vechicles.number','vechicles.trip_status'])
                ->get();
            $data['vehiclelist'] = $vehiclelist;
        }
     
      // DB::table('users as u')
      //                ->join('vehicle_user as vu', 'vu.user_id','=','u.id') 
      //                 ->join('vehicle_user as vu','vu.vehicle_id','=','vdrivers','vdrivers.vehicle_id')     

      
      // exit;
     return view('tracking.tracking',$data);
    }
    public function getChat()
    {	
    	$slug = Auth::user()->slug;
      $academic_id = getDefaultAcademicId();
    	$data['active_class']       = 'chatone';
        $data['layout']             = getLayout();
        $data['title']              = getPhrase('chatone');
        $data['module_helper']      = getModuleHelper('coupans-list');
        $user = App\User::where('slug','=',$slug)->first();
        // echo $user;
        // exit;
      if($isValid = $this->isValidRecord($user))
        return redirect($isValid);

      // if(!checkRole(getUserGrade(11)))
      // {
      //   prepareBlockUserMessage();
      //   return back();
      // }
      $chatlist = App\CourseSubject::join('subjects', 'subjects.id','=','course_subject.subject_id')
				      ->join('courses','courses.id','=','course_subject.course_id')
				      ->where('staff_id','=',$user->id)
				      ->where('course_subject.academic_id','=',getDefaultAcademicId())
				      ->select(['course_subject.id as id','course_subject.slug as slug', 'subject_title', 'course_title', 'year', 'semister', 'subject_id','staff_id','course_dueration','course_subject.academic_id as academic_id','course_subject.course_parent_id as course_parent_id','course_subject.course_id as course_id'])
				      ->orderBy('year')
				      ->orderBy('semister')
				      ->get();
      if(is_numeric($academic_id)){

        $records  = App\CourseSubject::join('academics', 'academics.id','=','course_subject.academic_id')
                                  ->join('courses', 'courses.id','=','course_subject.course_id')
                                  ->where('course_subject.staff_id',$user->id)
                                  ->where('course_subject.academic_id',$academic_id)
                                  ->groupby('course_subject.course_id')
                                  ->select(DB::raw("CONCAT(academic_year_title,'-',course_title) AS class"),'course_subject.id')
                                  ->get();
                                  
        $classes = $records;                         
        }
        else{

        $records  = App\CourseSubject::join('academics', 'academics.id','=','course_subject.academic_id')
                                  ->join('courses', 'courses.id','=','course_subject.course_id')
                                  ->where('course_subject.staff_id',$user->id)
                                  ->groupby('course_subject.course_id')
                                  ->select(DB::raw("CONCAT(academic_year_title,'-',course_title) AS class"),'course_subject.id')
                                  ->get();
        $classes = $records;
        }
        $role = getRoleData($user->role_id);
        if($role == 'parent'){
            $students = App\User::whereIn('parent_id',[$user->id])
                                ->select(['name','id'])
                                ->get();
        }else{
          $students = [];
        }
      $data['user'] = $user;
      $data['classes'] = $classes;
		$data['chatlists'] = $chatlist;
    $data['students'] = $students;
    $data['leaveapplystaff'] = [];
    $data['add_record'] = 1;
    	return view('chat.chatone',$data);
    }

    public function isValidRecord($record)
    {
      if ($record === null) {

        flash('Ooops...!', getPhrase("page_not_found"), 'error');
        return $this->getRedirectUrl();
    	}
	}

  public function getStudentsandParents(Request $request){
      $course_subject_id = $request->course_subject_id;
       $record  = App\CourseSubject::find($course_subject_id);
       $data = [];
       $all_students  = App\Student::join('users','users.id','=','students.user_id')
                                ->where('academic_id',$record->academic_id)
                                ->where('course_parent_id',$record->course_parent_id) 
                                ->where('course_id',$record->course_id) 
                                ->where('current_year',$record->year) 
                                ->where('current_semister',$record->semister)
                                ->select(['users.name','roll_no','users.id','users.parent_id'])
                                ->get();
         $studentId = [];
        foreach ($all_students as $students) {
          $studentId[] = $students->parent_id;
        } 
        $all_parents = App\User::whereIn('id',$studentId)
                                ->select(['name','id'])
                                ->get();
        $data['students'] = $all_students;
        $data['parents'] = $all_parents;
       return $data;                         
  }
  
  public function getStaffs(Request $request){
       $data = [];
       $academicid = App\Student::where('user_id',$request->studentid)
                                ->get(['academic_id']);
       $course_details  = App\Student::join('courses','courses.id','=','students.course_id')
                                ->where('academic_id',$academicid[0]->academic_id)
                                ->where('user_id',$request->studentid) 
                                ->select(['courses.course_title'])
                                ->get();
        $course_details1  = App\Student::join('courses','courses.id','=','students.course_parent_id')
                                ->where('academic_id',$academicid[0]->academic_id)
                                ->where('user_id',$request->studentid) 
                                ->select(['courses.course_title'])
                                ->get();

        $staffids = App\Student::join('course_subject','course_subject.course_id','=','students.course_id')
                                ->where('course_subject.academic_id',$academicid[0]->academic_id)
                                ->where('user_id',$request->studentid) 
                                ->select(['course_subject.staff_id'])
                                ->get();
         $staffId = [];
        foreach ($staffids as $staff) {
          $staffId[] = $staff->staff_id;
        } 
        $all_staffs = App\User::whereIn('id',$staffId)
                                ->select(['name','id'])
                                ->get();
        $data['staff'] = $all_staffs;
        $data['coursedetails'] = $course_details;
        $data['parentcourse'] = $course_details1;
       return $data;                         
  }

  public function getQRCODE(Request $request){
    //$data['qrcode'] = QrCode::size(100)->generate('MyNotePaper');
      $slug = Auth::user()->slug;
      $academic_id = getDefaultAcademicId();
      $data['active_class']       = 'chatone';
        $data['layout']             = getLayout();
        $data['title']              = getPhrase('chatone');
        $data['module_helper']      = getModuleHelper('coupans-list');
        $user = App\User::where('slug','=',$slug)->first();
        // echo $user;
        // exit;
      if($isValid = $this->isValidRecord($user))
        return redirect($isValid);

      // if(!checkRole(getUserGrade(11)))
      // {
      //   prepareBlockUserMessage();
      //   return back();
      // }
      $chatlist = App\CourseSubject::join('subjects', 'subjects.id','=','course_subject.subject_id')
              ->join('courses','courses.id','=','course_subject.course_id')
              ->where('staff_id','=',$user->id)
              ->where('course_subject.academic_id','=',getDefaultAcademicId())
              ->select(['course_subject.id as id','course_subject.slug as slug', 'subject_title', 'course_title', 'year', 'semister', 'subject_id','staff_id','course_dueration','course_subject.academic_id as academic_id','course_subject.course_parent_id as course_parent_id','course_subject.course_id as course_id'])
              ->orderBy('year')
              ->orderBy('semister')
              ->get();
      if(is_numeric($academic_id)){

        $records  = App\CourseSubject::join('academics', 'academics.id','=','course_subject.academic_id')
                                  ->join('courses', 'courses.id','=','course_subject.course_id')
                                  ->where('course_subject.staff_id',$user->id)
                                  ->where('course_subject.academic_id',$academic_id)
                                  ->groupby('course_subject.course_id')
                                  ->select(DB::raw("CONCAT(academic_year_title,'-',course_title) AS class"),'course_subject.id')
                                  ->get();
                                  
        $classes = $records;                         
        }
        else{

        $records  = App\CourseSubject::join('academics', 'academics.id','=','course_subject.academic_id')
                                  ->join('courses', 'courses.id','=','course_subject.course_id')
                                  ->where('course_subject.staff_id',$user->id)
                                  ->groupby('course_subject.course_id')
                                  ->select(DB::raw("CONCAT(academic_year_title,'-',course_title) AS class"),'course_subject.id')
                                  ->get();
        $classes = $records;
        }
        $role = getRoleData($user->role_id);
        if($role == 'parent'){
            $students = App\User::whereIn('parent_id',[$user->id])
                                ->select(['name','id'])
                                ->get();
        }else{
          $students = [];
        }
      $data['user'] = $user;
      $data['classes'] = $classes;
    $data['chatlists'] = $chatlist;
    $data['students'] = $students;
    $data['leaveapplystaff'] = [];
    $data['add_record'] = 1;
    $data['qrcode'] = QrCode::size(250)->generate('http://192.168.0.117/');
       return view('chat.qrcode',$data);                         
  }
}
