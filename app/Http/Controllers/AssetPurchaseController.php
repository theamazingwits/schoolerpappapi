<?php
namespace App\Http\Controllers;
use \App;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Assets;
use App\AssetPurchase;
use App\AssetVendor;
use App\User;
use App\LogHelper;
use Yajra\Datatables\Datatables;
use DB;
use Auth;
use Carbon\Carbon;


class AssetPurchaseController extends Controller
{
    
    public function __construct()
    {
    	$this->middleware('auth');
    }

    /**
     * Course listing method
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
      if(!checkRole(getUserGrade(17)))
      {
        prepareBlockUserMessage();
        return back();
      }
        $data['active_class']       = 'assets';
        $data['layout']             = getLayout();
        $data['title']              = getPhrase('purchases');

        return view('assets.purchase.list', $data);

          
    }

    /**
     * This method returns the datatables data to view
     * @return [type] [description]
     */
    public function getDatatable($slug = '')
    {

      if(!checkRole(getUserGrade(17)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $records = array();
 
      
        $records = AssetPurchase::select(['asset_id','quantity','unit','price','purchase_date','vendor_id','added_by','id']);
    
            
        $records->orderBy('updated_at', 'desc');
             

        return Datatables::of($records)
        ->addColumn('action', function ($records) {
         
          $link_data = '<div class="dropdown more">
                        <a id="dLabel" type="button" class="more-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-ellipsis-v"></i>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <li><a href="'.URL_ASSET_PURCHASE_EDIT.$records->id.'"><i class="fa fa-pencil"></i>'.getPhrase("edit").'</a></li>';
                            
                           $temp = '';
                           if(checkRole(getUserGrade(2))) {
                    $temp .= ' <li><a href="javascript:void(0);" onclick="deleteRecord(\''.$records->id.'\');"><i class="fa fa-trash"></i>'. getPhrase("delete").'</a></li>';
                      }
                    
                    $temp .='</ul></div>';

                    $link_data .=$temp;
            return $link_data;
            })

        

         ->editColumn('asset_id', function($records){
             
             $record  = Assets::find($records->asset_id);
             if($record)
                return ucwords($record->title);
            return '-';
        })

           ->editColumn('vendor_id', function($records){
             
             $record  = AssetVendor::find($records->vendor_id);
             if($record)
                return ucwords($record->name);
            return '-';
        })

         ->editColumn('added_by', function($records){
             
             $record  = User::find($records->added_by);
             if($record)
                return ucwords($record->name);
            return '-';
        })

         ->editColumn('unit', function($records){
             
            return $records->unit.' - '.$records->price;
        })

        ->removeColumn('id')
        ->removeColumn('price')
        ->make();
    }

    /**
     * This method loads the create view
     * @return void
     */
    public function create()
    {
      if(!checkRole(getUserGrade(17)))
      {
        prepareBlockUserMessage();
        return back();
      }
        $data['record']       = FALSE;
        $data['active_class'] = 'assets';
        $data['layout']       = getLayout();
        $data['title']        = getPhrase('new_purchase');
       
        $data['assets']   = Assets::pluck('title','id')->toArray();
        $data['vendors']  = AssetVendor::pluck('name','id')->toArray();
        $data['units']    = array('Kg'=>'Kg','Price'=>'Price','Other'=>'Other');

         return view('assets.purchase.add-edit', $data);

       
    }

    /**
     * This method loads the edit view based on unique slug provided by user
     * @param  [string] $slug [unique slug of the record]
     * @return [view with record]       
     */
    public function edit($id)
    {
      if(!checkRole(getUserGrade(17)))
      {
        prepareBlockUserMessage();
        return back();
      }
    
    // dd($id);
        $record = AssetPurchase::where('id',$id)->first();

        if($isValid = $this->isValidRecord($record))
            return redirect($isValid);

        $data['record']       = $record;
        $data['active_class'] = 'assets';
        $data['layout']       = getLayout();
        $data['title']        = getPhrase('edit_asset');
        
        $data['assets']   = Assets::pluck('title','id')->toArray();
        $data['vendors']  = AssetVendor::pluck('name','id')->toArray();
        $data['units']    = array('Kg'=>'Kg','Price'=>'Price','Other'=>'Other');

         return view('assets.purchase.add-edit', $data);

        
    }

    
    /**
     * [update description]
     * @param  Request $request [description]
     * @param  [type]  $slug    [description]
     * @return [type]           [description]
     */
    public function update(Request $request, $id)
    {
        // dd($request);
      if(!checkRole(getUserGrade(17)))
      {
        prepareBlockUserMessage();
        return back();
      }

       $rules = [
         'quantity'        => 'bail|required|numeric|min:1' ,
         'price'           => 'bail|required|numeric|min:1' ,
         'purchase_date'   => 'bail|required' ,
         'service_date'    => 'bail|required' ,
         'expire_date'     => 'bail|required' ,
            ];
            
        $this->validate($request, $rules);


        $record = AssetPurchase::where('id',$id)->first();

        if($isValid = $this->isValidRecord($record))
            return redirect($isValid);


        $record->update($request->all());


        $record->flag        = 'Update';
        $record->action      = 'Asset_purchases';
        $record->object_id   =  $record->id;
        $logs = new LogHelper();
        $logs->storeLogs($record);

        flash('success','purchase_updated_successfully', 'success');

        return redirect(URL_ASSET_PURCHASE);
    }

    /**
     * This method adds record to DB
     * @param  Request $request [Request Object]
     * @return void
     */
    public function store(Request $request)
    {
      if(!checkRole(getUserGrade(17)))
      {
        prepareBlockUserMessage();
        return back();
      }


        $rules = [
         'quantity'        => 'bail|required|numeric|min:1' ,
         'price'           => 'bail|required|numeric|min:1' ,
         'purchase_date'   => 'bail|required' ,
         'service_date'    => 'bail|required' ,
         'expire_date'     => 'bail|required' ,
            ];
        $this->validate($request, $rules);

    
        $record = AssetPurchase::create($request->all());

        $record->flag        = 'Insert';
        $record->action      = 'Asset_purchases';
        $record->object_id   =  $record->id;
        $logs = new LogHelper();
        $logs->storeLogs($record);

        flash('success','purchase_added_successfully', 'success');

        return redirect(URL_ASSET_PURCHASE);
    }
 
    /**
     * Delete Record based on the provided slug
     * @param  [string] $slug [unique slug]
     * @return Boolean 
     */
    public function delete($id)
    {
      if(!checkRole(getUserGrade(17)))
      {
        prepareBlockUserMessage();
        return back();
      }
      /**
       * Delete the questions associated with this quiz first
       * Delete the quiz
       * @var [type]
       */
         $record = AssetPurchase::where('id',$id)->first();
        try{
            if(!env('DEMO_MODE')) {
                $record->delete();
            }

            $record->flag        = 'Delete';
            $record->action      = 'Asset_purchases';
            $record->object_id   =  $id;
            $logs = new LogHelper();
            $logs->storeLogs($record);

            $response['status'] = 1;
            $response['message'] = getPhrase('record_deleted_successfully');
        }
         catch ( Exception $e) {
                 $response['status'] = 0;
           if(getSetting('show_foreign_key_constraint','module'))
            $response['message'] =  $e->getMessage();
           else
            $response['message'] =  getPhrase('this_record_is_in_use_in_other_modules');
       }
        return json_encode($response);
    }

    public function isValidRecord($record)
    {
        if ($record === null) {

            flash('Ooops...!', getPhrase("page_not_found"), 'error');
            return $this->getRedirectUrl();
        }

        return FALSE;
    }

    public function getReturnUrl()
    {
        return URL_ASSET_PURCHASE;
    }


}