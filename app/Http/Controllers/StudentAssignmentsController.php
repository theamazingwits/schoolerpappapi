<?php
namespace App\Http\Controllers;
use \App;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\CourseSubject;
use App\Assignments;
use App\AssignmentAllocate;
use App\Subject;
use App\Course;
use App\Academic;
use App\GeneralSettings;
use App\User;
use Yajra\Datatables\Datatables;
use DB;
use Auth;
use Exception;
use Response;


class StudentAssignmentsController extends Controller
{
     public function __construct()
    {
    	$this->middleware('auth');
    }

    public function index($slug=0)
    {
      // dd($slug);
        $data['active_class'] = 'assignments';
         if(checkRole(getUserGrade(2)))
        $data['active_class'] = 'users';
         elseif(checkRole(getUserGrade(7)))
    	  $data['active_class'] = 'children';

        $data['layout']       = getLayout();
        $data['title']        = getPhrase('assignments');
        $data['slug']         = $slug;
         $user     = Auth::user();
        if($slug)
        $user     = User::where('slug', $slug)->first();

        $data['user']         = $user;
        return view('assignments.student-list', $data);                         

    }


    public function getDataTable($slug="")
    {
         if(!checkRole(getUserGrade(14)))
      {
        prepareBlockUserMessage();
        return back();
      }
 
        $user     = Auth::user();
        if($slug)
        $user     = User::where('slug', $slug)->first();

        $student  = App\Student::where('user_id',$user->id)->first();

        $records = array();
        
        $records = Assignments::join('assignments_allocate','assignments_allocate.assignment_id','=','assignments.id')
                                      ->join('users','users.id','=','assignments.user_id')
                                      ->join('subjects','subjects.id','=','assignments.subject_id')
                                      ->where('academic_id',$student->academic_id)
                                      ->where('course_parent_id',$student->course_parent_id)
                                      ->where('course_id',$student->course_id)
                                      ->where('year',$student->current_year)
                                      ->where('semister',$student->current_semister)
                                      ->select(['assignments.title','subjects.subject_title','users.name','deadline','assignments_allocate.id as allocate_id','assignments_allocate.is_submitted','assignments_allocate.is_approved'])
                                      ->where('assignments_allocate.student_id',$user->id)
                                      ->orderby('assignments_allocate.updated_at','desc');
        
             

        return Datatables::of($records)
        ->addColumn('action', function ($records) {
         
          return '<a href="'.URL_VIEW_ASSIGNMENT.$records->allocate_id.'" class="btn btn-primary btn-sm">View</a>';
        
        })

          ->editColumn('is_submitted', function($records)
        {
            
            if($records->is_submitted == 0)
             return '<span class="label label-warning">No</span>';

             return '<span class="label label-success">Yes</span>';


        })

         ->editColumn('is_approved', function($records)
        {
            
            if($records->is_approved == 0)
             return '<span class="label label-warning">No</span>';

             return '<span class="label label-success">Yes</span>';

        })

           ->editColumn('title', function($records)
        {  
           if(checkRole(getUserGrade(2)))
           return ucwords($records->title);
          elseif(checkRole(getUserGrade(7)))
           return ucwords($records->title);
           else 
           return '<a href="'.URL_VIEW_ASSIGNMENT.$records->allocate_id.'">'.ucwords($records->title).'</a>';
           
        })

        ->removeColumn('allocate_id')
        ->make();
    }


    public function viewAssignment($allocate_id)
    {
    	// dd($allocate_id);

    	  $data['active_class'] = 'assignments';
        $data['layout']       = getLayout();
        $data['title']        = getPhrase('assignment_details');

        $details  = Assignments::join('assignments_allocate','assignments_allocate.assignment_id','=','assignments.id')
                                      ->join('users','users.id','=','assignments.user_id')
                                      ->join('subjects','subjects.id','=','assignments.subject_id')
                                      ->where('assignments_allocate.id',$allocate_id)
                                      ->select(['assignments.title','subject_title','users.name','deadline','assignments_allocate.id as allocate_id','file_name','assignments.description','assignments.slug','assignments_allocate.user_file'])
                                      ->first();
        
        $data['reached']  = FALSE;

        $today  = date('Y-m-d');

        if ( $today > $details->deadline){

          $data['reached']  = TRUE;
        }

        $data['details']   = $details;

        return view('assignments.details', $data);
    }

    public function uploadAssignment(Request $request)
    {
      
        $record      = AssignmentAllocate::find($request->allocate_id);
        $assignment  = Assignments::find($record->assignment_id);
        $user        = User::find($assignment->user_id);
        $student     = User::find($record->student_id);
        // dd($user);

        $file_upload  =  $this->processUpload($request, $record,'catimage');

        if($file_upload){

            $record->is_submitted = 1;
            $record->save();
            flash('success','assignment_uploaded_successfully','success');

          try {
             
               $user->notify(new \App\Notifications\StudentUploadAssignment($user,$student,$assignment));
             
           } catch (Exception $e) {
              
              // dd($e->getMessage());
           }

        }
        else{

            flash('Oops..!','please_upload_the_assignment','overlay');
            return back();
        }

        return redirect(URL_STUDENT_ASSIGNMENT);
    }

     public function processUpload(Request $request, $record, $file_name)
     {
        if(env('DEMO_MODE')) {
            return;
        }

        
         if ($request->hasFile($file_name)) {

          $examSettings     = new GeneralSettings();
          
          $destinationPath  = $examSettings->assignmentPath();
          
          $fileName = $record->id.'-'.$request->$file_name->getClientOriginalName();
          // dd($fileName);
          
          $request->file($file_name)->move($destinationPath, $fileName);

          $record->user_file  =  $fileName;

          $record->save();
           
          return TRUE; 
      
        }else{

            return FALSE;
        }

     }

       public function downloadFile($id)
     {
        
        try {
          
           $record  = App\AssignmentAllocate::where('id',$id)->first();

           $path   = public_path()."/uploads/assignments/".$record->user_file;
       
           return Response::download($path);

        } catch (Exception $e) {
           
           flash('Ooops..!','file_is_not_found_please_upload_new_one','overlay');
           return back();
        } 
        
     }

}  