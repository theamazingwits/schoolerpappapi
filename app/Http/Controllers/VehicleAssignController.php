<?php
namespace App\Http\Controllers;
use \App;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\VehicleAssign;
use App\Vroutes;
use App\Vechicle;
use App\User;
use App\VehicleUser;
use App\VehicleStaffUser;
use App\LogHelper;
use Yajra\Datatables\Datatables;
use DB;
use Auth;
use Exception;
use Carbon\Carbon;
use SimpleSoftwareIO\QrCode\Facades\QrCode;


class VehicleAssignController extends Controller
{
    
    public function __construct()
    {
    	$this->middleware('auth');
    }

    /**
     * Course listing method
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
      if(!checkRole(getUserGrade(20)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $data['active_class']       = 'transport';
        $data['title']              = getPhrase('assign_vehicle_to_student');
        
       if( Vroutes::isMultiBranch() ){

        $data['branches']           = addSelectToList(getBranches());
        }else{

        $data['branches']           = null;
        }

        $data['is_multibranch']     = Vroutes::isMultiBranch();
        $data['academic_years']     = addSelectToList(getAcademicYears());
        $list                       = App\Course::getCourses(0);
        
        $data['layout']             = getLayout();
       
        return view('transport.studentassign.list',$data);

        //    $view_name = getTheme().'::transport.studentassign.list';
        // return view($view_name,$data);
    }


  



     /**
     * This method returns the list of students avaialbe with the requested 
     * combination
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getStudents(Request $request)
    {
        $branch_id          = $request->branch_id;
        $academic_id        = $request->academic_id;
        $course_parent_id   = $request->parent_course_id;
        $course_id          = $request->course_id;
        $year               = $request->year;
        $semister           = $request->semister;

        // return $request;
        // $result1 = DB::table('users as use')
        //       ->join('roles as ro', 'use.role_id', 'ro.id')
        //       ->select('use.name','use.image','ro.name','ro.display_name')
        //       ->whereNotIn('use.role_id',[1,2,5,6,14])
        //       ->get();

        $records = User::join('students', 'users.id' ,'=', 'students.user_id')  
        ->join('branches','branches.id','=','students.branch_id') 
        ->join('academics','academics.id','=','students.academic_id') 
        ->join('courses','courses.id','=','students.course_id')
        ->where('students.branch_id','=',$branch_id)
        ->where('academic_id','=',$academic_id)
        ->where('course_parent_id','=',$course_parent_id)
        ->where('course_id','=',$course_id)
        ->where('current_year','=',$year)
        ->where('current_semister','=',$semister)
        ->select(['students.user_id as id','users.name', 'roll_no','admission_no', 'course_title','blood_group','mobile','home_phone','image','branch_name','branches.address','academic_year_title', 'email', 'current_year', 'current_semister','course_dueration','students.branch_id as branch_id','students.academic_id as academic_id', 'students.course_id as course_id', 'students.user_id as user_id','users.slug'])
        ->get();
        return $records;
    }


      /**
     * This method returns the list of students avaialbe with the requested 
     * combination
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getSingleBranchStudents(Request $request)
    {
        $academic_id        = $request->academic_id;
        $course_parent_id   = $request->course_parent_id;
        $course_id          = $request->course_id;
        $year               = $request->year;
        $semister           = $request->semister;

        $records = User::join('students', 'users.id' ,'=', 'students.user_id')  
        ->join('academics','academics.id','=','students.academic_id') 
        ->join('courses','courses.id','=','students.course_id')
        ->where('academic_id','=',$academic_id)
        // ->where('course_parent_id','=',$course_parent_id)
        ->where('course_id','=',$course_id)
        ->where('current_year','=',$year)
        ->where('current_semister','=',$semister)
        ->select(['students.user_id as id','users.name', 'roll_no','admission_no', 'course_title','blood_group','mobile','home_phone','image','academic_year_title', 'email', 'current_year', 'current_semister','course_dueration','students.academic_id as academic_id', 'students.course_id as course_id', 'students.user_id as user_id','users.slug'])
        ->get();
        return $records;
    } 


      /**
     * This method returns the list of parent courses available with the specific 
     * academic year
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getParentCourses(Request $request)
    {
        $records = App\AcademicCourse::join('courses', 'academic_course.course_parent_id', '=' ,'courses.id')
         ->select([  
            'course_title','courses.id'])
         ->where('academic_id', '=', $request->academic_id)
         ->groupBy('course_parent_id')->get();
         return $records;
    }

   
    /**
     * This method will return the child courses based on the request
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getChildCourses(Request $request)
    {
     
        $current_user = Auth::user();
        $user_role = getRoleData($current_user->role_id);
        $student_record = '';
        if($user_role=='student')
        {
          $student_record = App\Student::where('user_id','=',$current_user->id)->first();
        }


        $course_id =$request->course_id;
        $academic_id = $request->academic_id;
        $parent_course_id = $request->parent_course_id;
        $records = App\AcademicCourse::join('courses', 'academic_course.course_id', '=' ,'courses.id')
         ->select([  
            'course_title','courses.id', 'course_dueration','is_having_semister','academic_id', 'course_parent_id'])
         ->where('academic_course.academic_id', '=', $academic_id)
         ->where('academic_course.course_parent_id', '=', $parent_course_id);

         if($user_role=='student')
         {
            $records = $records->where('academic_course.course_id','=',$student_record->course_id);
         }
         else {
           $records = $records->groupBy('course_id');
        }
        $records = $records->get();
         $final_records = [];
         foreach($records as $key => $value)
         {
          $temp['course'] = $value;
          $temp['semister'] = $value->semisters();

          $final_records[] = $temp;
         }
         return $final_records;
    }

    public function assignVechicle($slug)
    {
        
        $user                       = User::getRecordWithSlug($slug);
        // dd($user);

        $data['vehicle_details']    = VehicleUser::where('user_id',$user->id)->get();
        $data['details']            = VehicleUser::where('user_id',$user->id)->get()->count();
        $data['user']               = $user;
        $data['record']             = $user;
        $data['active_class']       = 'transport';
        $data['title']              = ucwords($user->name).' '.getPhrase('vehicle_details');
        $data['layout']             = getLayout();
        $data['vehicles_list']      = Vroutes::join('vechicle_assign','vechicle_assign.route_id','=','vroutes.id')
                                               ->pluck('name','vroutes.id')
                                               ->toArray();

        return view('transport.studentassign.student-transport',$data);
                                  

        //    $view_name = getTheme().'::transport.studentassign.student-transport';
        // return view($view_name,$data);
    }

    public function getRoutes(Request $request)
    { 
        // return $request;
        
        $route_id  = $request->route_id;
      
        $vehicles  = VehicleAssign::where('route_id',$route_id)->get();
        $records   = array();

        foreach ($vehicles as $vehicle) {
          
          $rvehicle        = Vechicle::find($vehicle->vechicle_id);
          $avaialbe_seats  = VehicleUser::where('route_id',$route_id)
                                          ->where('vehicle_id',$rvehicle->id)
                                          ->where('is_stoped',0)
                                          ->get()
                                          ->count();

          if( $rvehicle->seats > $avaialbe_seats ){

            $free_seats      = $rvehicle->seats - $avaialbe_seats;

            $records[] = Vechicle::select(DB::raw("CONCAT(number,' ( Total ',seats,' Seats, Available $free_seats Seats)') AS vehicle_number"),'id')
                                    ->where('id',$rvehicle->id)
                                    ->first(); 
           }

        }
        
       return json_encode($records);

    }


    public function store(Request $request)
    {
        
// dd($request);
        $columns = array(

        'route_id'     => 'bail|required',
        'vehicle_id'       => 'bail|required',
       
        );

        $this->validate($request,$columns);

         //Any Active Records
         $user_id  = $request->user_id;

         $pre_records  = VehicleUser::where('user_id',$user_id)->get();

         foreach ($pre_records as $record) {
            
            $record->is_stoped = 1;
            $record->is_active = 0;
            $record->save();
         }
         
         VehicleUser::create($request->all());
        
        flash('success','user_added_successfully', 'success');
        return redirect(URL_ASSIGN_STUDENT_VEHICLE);
    }

    public function stopUser(Request $request)
    {
      
      $user_id  = $request->stop_user_id;

      $record  = VehicleUser::where('user_id',$user_id)
                             ->where('is_active',1)
                             ->first();
      $record->is_stoped = 1;  
      $record->is_active = 0;  
      $record->save();

      flash('success','user_stop_from_vehicle_successfully', 'success');
      return redirect(URL_ASSIGN_STUDENT_VEHICLE);   

    }


    public function loadStudents(Request $request)
    {
        // dd($request);
        $data['record']             = FALSE;
        $data['active_class']       = 'transport';
        $data['loadYears']          = TRUE;

        $academic_id        = $request->academic_id;
        $course_parent_id   = $request->course_parent_id;
        $course_id          = $request->course_id;
        $year               = 1;
        if($request->has('year'))
        $year               = $request->year;

        $semister           = 0;
        if($request->has('semister'))
        $semister           = $request->semister;
   
   // dd($course_parent_id);
        // return $request;

        $students = User::join('students', 'users.id' ,'=', 'students.user_id')  
                            ->where('academic_id','=',$academic_id)
                            ->where('course_parent_id','=',$course_parent_id)
                            ->where('course_id','=',$course_id)
                            ->where('current_year','=',$year)
                            ->where('current_semister','=',$semister)
                            ->select(['users.id as id','users.name as name'])
                            ->get();
        // $students = DB::table('users as use')
        //       ->join('roles as ro', 'use.role_id', 'ro.id')
        //       ->select('use.id','use.name','use.image','ro.display_name')
        //       ->whereNotIn('use.role_id',[1,2,5,6,14])
        //       ->get();
// print_r($students);
// exit;

// dd($records);
        
        $data['right_bar']          = TRUE;
        $data['right_bar_path']     = 'transport.studentassign.new.right-bar';
        $data['right_bar_data']     = array(
                                            'item' => $students,
                                            );
 
        $data['layout']             = getLayout();


        $academic_id               = $request->academic_id;
        $course_id                 = $request->course_id;
        $academic_title            = App\Academic::where('id','=',$academic_id)->first()->academic_year_title;
        $course_record             = App\Course::where('id', '=', $course_id)->first();

        $available_data = "";

        $available_data = VehicleUser::join('users','users.id','=','vehicle_user.user_id')
                                       ->select(['users.id as id','users.name as name','vehicle_id'])
                                       ->where('vehicle_user.is_active',1)
                                       ->get();
// dd($available_data);
        $data['items']              = json_encode(array('source_items'=>$students,'target_items'=>$available_data));
 
        $course_title               = $course_record->course_title;
        $data['academic_id']        = $academic_id;
        $data['course_id']          = $course_id;
        $data['title']              = getPhrase('transportation').' - '.$academic_title.' - '.$course_title;
        $data['record']             = $course_record;

        $data['vehicles']  = Vechicle::orderby('created_at','desc')->get();
        // dd($vehicles);

        return view('transport.studentassign.new.list', $data);
    }

    public function loadRoutes(Request $request)
    {
        $vehicle_id   = $request->vehicle_id;

        $vehicle_routes = VehicleAssign::join('vroutes','vroutes.id','=','vechicle_assign.route_id')
                                          ->select(['name','cost','vroutes.id'])
                                          ->where('vechicle_id',$vehicle_id)
                                          ->get();
        
         return json_encode( array( 'vehicle_routes'=>$vehicle_routes ) );

    }

    public function storeStudent(Request $request)
    {
       $user_id    = $request->user_id;
        $vehicle_id = $request->vehicle_id;
        $route_id   = $request->route_id;
        $vehicle_records = Vechicle::where('id',$vehicle_id)->first();
        $pre_records  = VehicleUser::where('user_id',$user_id)->where('is_active','1')->get();
        
        if(count($pre_records) > 0 ){

          // foreach ($pre_records as $record) {
            
          //   $record->is_stoped = 1;
          //   $record->is_active = 0;
          //   $record->save();

          //  }
            $data['status'] = '0';
        }else{
       
            $route_data  = Vroutes::where('id',$route_id)->first();
            $qrfilename = "";
            if($vehicle_records->track_student == "1"){
            $qrdata = $user_id."&".$vehicle_id."&".$route_id."&".$route_data->parent_id;


            //$date =date('m/d/Y h:i:s a', time());
            $qrfilename = $user_id.$vehicle_id.$route_id.$route_data->parent_id.".svg";
            $qrcode = QrCode::size(1000)
                    ->format('svg')
                    ->generate($qrdata, public_path('qrcode/'.$qrfilename));
            }

            $assign_user                  = new VehicleUser();
            $assign_user->user_id         = $user_id;
            $assign_user->vehicle_id      = $vehicle_id;
            $assign_user->route_id        = $route_id;
            $assign_user->parent_route_id = $route_data->parent_id;
            $assign_user->track_device = $qrfilename;
            $assign_user->save();
            $data['status'] = '1';
            $data['route'] = $route_data;
            $data['assign_user'] = $assign_user;
            $data['vehiclerecords'] = $vehicle_records;
            
        }
        return $data;

    }


    public function removeStudent(Request $request)
    {
        $user_id      = $request->user_id;
        // $assign_user  = VehicleUser::where('user_id',$user_id)->delete();
        $assign_user  = VehicleUser::where('user_id',$user_id)  
                                     ->where('is_active',1)
                                     ->get();

        foreach ($assign_user as $record) {
         
           $record->is_active = 0;
           $record->is_stoped = 1;
           $record->save();
        }                             
    }

    public function showUsers(Request $request)
    {
        $vehicle_id  = $request->vehicle_id;

        $users = VehicleUser::join('users','users.id','=','vehicle_user.user_id')
                               ->join('vroutes','vroutes.id','=','vehicle_user.route_id')
                               ->select(['users.id as id','users.name as name','vehicle_id','vroutes.name as route_name','cost'])
                               ->where('vehicle_user.vehicle_id',$vehicle_id)
                               ->where('vehicle_user.is_active',1)
                               ->get();
        
         return json_encode( array( 'users'=>$users ) );
    }


    // staff 
        /**
     * Course listing method
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function staffindex()
    {
      if(!checkRole(getUserGrade(20)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $data['active_class']       = 'transport';
        $data['title']              = getPhrase('assign_vehicle_to_staff');
        
       if( Vroutes::isMultiBranch() ){

        $data['branches']           = addSelectToList(getBranches());
        }else{

        $data['branches']           = null;
        }

        $data['is_multibranch']     = Vroutes::isMultiBranch();
        $data['academic_years']     = addSelectToList(getAcademicYears());
        $list                       = App\Course::getCourses(0);
        
        $data['layout']             = getLayout();
       
        return view('transport.staffassign.list',$data);

        //    $view_name = getTheme().'::transport.studentassign.list';
        // return view($view_name,$data);
    }

    public function staffassignVechicle($slug)
    {
        
        $user                       = User::getRecordWithSlug($slug);
// dd($user);

        $data['vehicle_details']    = VehicleStaffUser::where('user_id',$user->id)->get();
        $data['details']            = VehicleStaffUser::where('user_id',$user->id)->get()->count();
        $data['user']               = $user;
        $data['record']             = $user;
        $data['active_class']       = 'transport';
        $data['title']              = ucwords($user->name).' '.getPhrase('vehicle_details');
        $data['layout']             = getLayout();
        $data['vehicles_list']      = Vroutes::join('vechicle_assign','vechicle_assign.route_id','=','vroutes.id')
                                               ->pluck('name','vroutes.id')
                                               ->toArray();

        return view('transport.staffassign.student-transport',$data);
                                  

        //    $view_name = getTheme().'::transport.studentassign.student-transport';
        // return view($view_name,$data);
    }
    public function getstaffRoutes(Request $request)
    { 
        // return $request;
        
        $route_id  = $request->route_id;
      
        $vehicles  = VehicleAssign::where('route_id',$route_id)->get();
        $records   = array();

        foreach ($vehicles as $vehicle) {
          
          $rvehicle        = Vechicle::find($vehicle->vechicle_id);
          $avaialbe_seats  = VehicleStaffUser::where('route_id',$route_id)
                                          ->where('vehicle_id',$rvehicle->id)
                                          ->where('is_stoped',0)
                                          ->get()
                                          ->count();

          if( $rvehicle->seats > $avaialbe_seats ){

            $free_seats      = $rvehicle->seats - $avaialbe_seats;

            $records[] = Vechicle::select(DB::raw("CONCAT(number,' ( Total ',seats,' Seats, Available $free_seats Seats)') AS vehicle_number"),'id')
                                    ->where('id',$rvehicle->id)
                                    ->first(); 
           }

        }
        
       return json_encode($records);

    }

    public function staffstore(Request $request)
    {
        
// dd($request);
        $columns = array(

        'route_id'     => 'bail|required',
        'vehicle_id'       => 'bail|required',
       
        );

        $this->validate($request,$columns);

         //Any Active Records
         $user_id  = $request->user_id;

         $pre_records  = VehicleStaffUser::where('user_id',$user_id)->get();

         foreach ($pre_records as $record) {
            
            $record->is_stoped = 1;
            $record->is_active = 0;
            $record->save();
         }
         
         VehicleStaffUser::create($request->all());
        
        flash('success','user_added_successfully', 'success');
        return redirect(URL_ASSIGN_STAFF_VEHICLE);
    }

    public function stopstaffUser(Request $request)
    {
      
      $user_id  = $request->stop_user_id;

      $record  = VehicleStaffUser::where('user_id',$user_id)
                             ->where('is_active',1)
                             ->first();
      $record->is_stoped = 1;  
      $record->is_active = 0;  
      $record->save();

      flash('success','user_stop_from_vehicle_successfully', 'success');
      return redirect(URL_ASSIGN_STAFF_VEHICLE);   

    }

    public function loadStaffs(Request $request)
    {
        // dd($request);
        $data['record']             = FALSE;
        $data['active_class']       = 'transport';
        $data['loadYears']          = TRUE;

        // $academic_id        = $request->academic_id;
        // $course_parent_id   = $request->course_parent_id;
        // $course_id          = $request->course_id;
        // $year               = 1;
        // if($request->has('year'))
        // $year               = $request->year;

        // $semister           = 0;
        // if($request->has('semister'))
        // $semister           = $request->semister;
   
   // dd($course_parent_id);
        // return $request;

        // $students = User::join('students', 'users.id' ,'=', 'students.user_id')  
        //                     ->where('academic_id','=',$academic_id)
        //                     ->where('course_parent_id','=',$course_parent_id)
        //                     ->where('course_id','=',$course_id)
        //                     ->where('current_year','=',$year)
        //                     ->where('current_semister','=',$semister)
        //                     ->select(['users.id as id','users.name as name'])
        //                     ->get();
        $students = DB::table('users as use')
              ->join('roles as ro', 'use.role_id', 'ro.id')
              ->select('use.id','use.name','use.image','ro.display_name')
              ->whereNotIn('use.role_id',[1,2,5,6,14])
              ->get();
// print_r($students);
// exit;

// dd($records);
        
        $data['right_bar']          = TRUE;
        $data['right_bar_path']     = 'transport.staffassign.new.right-bar';
        $data['right_bar_data']     = array(
                                            'item' => $students,
                                            );
 
        $data['layout']             = getLayout();


        // $academic_id               = $request->academic_id;
        // $course_id                 = $request->course_id;
        // $academic_title            = App\Academic::where('id','=',$academic_id)->first()->academic_year_title;
        // $course_record             = App\Course::where('id', '=', $course_id)->first();

        $available_data = "";

        $available_data = VehicleStaffUser::join('users','users.id','=','vehicle_staff_user.user_id')
                                       ->select(['users.id as id','users.name as name','vehicle_id'])
                                       ->where('vehicle_staff_user.is_active',1)
                                       ->get();
// dd($available_data);
        $data['items']              = json_encode(array('source_items'=>$students,'target_items'=>$available_data));
 
        // $course_title               = $course_record->course_title;
        // $data['academic_id']        = $academic_id;
        // $data['course_id']          = $course_id;
        $data['title']              = getPhrase('transportation');
        // $data['record']             = $course_record;

        $data['vehicles']  = Vechicle::orderby('created_at','desc')->get();
        // dd($vehicles);

        return view('transport.staffassign.new.list', $data);
    }


    public function loadStaffRoutes(Request $request)
    {
        $vehicle_id   = $request->vehicle_id;

        $vehicle_routes = VehicleAssign::join('vroutes','vroutes.id','=','vechicle_assign.route_id')
                                          ->select(['name','cost','vroutes.id'])
                                          ->where('vechicle_id',$vehicle_id)
                                          ->get();
        
         return json_encode( array( 'vehicle_routes'=>$vehicle_routes ) );

    }

    public function storeStaffs(Request $request)
    {
       $user_id    = $request->user_id;
        $vehicle_id = $request->vehicle_id;
        $route_id   = $request->route_id;
        $vehicle_records = Vechicle::where('id',$vehicle_id)->first();
        $pre_records  = VehicleStaffUser::where('user_id',$user_id)->where('is_active','1')->get();
        
        if(count($pre_records) > 0 ){

          // foreach ($pre_records as $record) {
            
          //   $record->is_stoped = 1;
          //   $record->is_active = 0;
          //   $record->save();

          //  }
            $data['status'] = '0';
        }else{
       
            $route_data  = Vroutes::where('id',$route_id)->first();
            $qrfilename = "";
            if($vehicle_records->track_student == "1"){
            $qrdata = $user_id."&".$vehicle_id."&".$route_id."&".$route_data->parent_id;


            //$date =date('m/d/Y h:i:s a', time());
            $qrfilename = $user_id.$vehicle_id.$route_id.$route_data->parent_id.".svg";
            $qrcode = QrCode::size(1000)
                    ->format('svg')
                    ->generate($qrdata, public_path('qrcode/'.$qrfilename));
            }

            $assign_user                  = new VehicleStaffUser();
            $assign_user->user_id         = $user_id;
            $assign_user->vehicle_id      = $vehicle_id;
            $assign_user->route_id        = $route_id;
            $assign_user->parent_route_id = $route_data->parent_id;
            $assign_user->track_device = $qrfilename;
            $assign_user->save();
            $data['status'] = '1';
            $data['route'] = $route_data;
            $data['assign_user'] = $assign_user;
            $data['vehiclerecords'] = $vehicle_records;
            
        }
        return $data;

    }

    public function removeStaffs(Request $request)
    {
        $user_id      = $request->user_id;
        // $assign_user  = VehicleUser::where('user_id',$user_id)->delete();
        $assign_user  = VehicleStaffUser::where('user_id',$user_id)  
                                     ->where('is_active',1)
                                     ->get();

        foreach ($assign_user as $record) {
         
           $record->is_active = 0;
           $record->is_stoped = 1;
           $record->save();
        }                             
    }

    public function showstaffUsers(Request $request)
    {
        $vehicle_id  = $request->vehicle_id;

        $users = VehicleStaffUser::join('users','users.id','=','vehicle_staff_user.user_id')
                               ->join('vroutes','vroutes.id','=','vehicle_staff_user.route_id')
                               ->select(['users.id as id','users.name as name','vehicle_id','vroutes.name as route_name','cost'])
                               ->where('vehicle_staff_user.vehicle_id',$vehicle_id)
                               ->where('vehicle_staff_user.is_active',1)
                               ->get();
        
         return json_encode( array( 'users'=>$users ) );
    }


}