<?php

namespace App\Http\Controllers;

use \App;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use App\Visitors;
use Yajra\Datatables\Datatables;
use DB;
use Auth;
use Exception;


class VisitorManagementController extends Controller
{
    
     public function __construct()
    {
    	$this->middleware('auth');
    }



      /**
     * Course listing method
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function index($id = "")
    {

      if(!checkRole(getUserGrade(19)))
      {
        prepareBlockUserMessage();
        return back();
      }
// dd($id);
       
        $data['active_class']  = 'visitors';
        $data['layout']        = getLayout();
        $data['title']         = getPhrase('visitors');
        $data['record']        = FALSE;
        if($id){
        $data['record']        = Visitors::find($id);
          
        }
        $data['roles']         = App\Role::pluck('display_name','id')->toArray();
        $data['representing']   = array(
                                 'Friend'=>'Friend',
                                 'Family'=>'Family',
                                 'Interview'=>'Interview',
                                 'Meeting'=>'Meeting',
                                 'Other'=>'Other',
                                 'Vendor'=>'Vendor'
                               );

       $data['visitors'] = Visitors::join('users','users.id','=','visitors.user_id')
                           ->where('visitors.status',1)
                           ->select(['visitors.name as visitor_name','visitors.phone_number','users.name as user_name','visitors.created_at','visitors.id'])
                           ->get(); 

       // dd($visitors);                    

        return view('visitors.list', $data);

          
    }


    public function getDatatable()
    {
      
      if(!checkRole(getUserGrade(19)))
      {
        prepareBlockUserMessage();
        return back();
      }

        $records = array();
 
        $records = Visitors::select(['id as visitor_id','name','user_id','phone_number','created_at','updated_at','status']);
                             // ->where('status',0);                              
        $records->orderBy('created_at', 'desc');
             

        return Datatables::of($records)

        ->addColumn('action', function ($records) {
         
          $link_data = '<div class="dropdown more">
                        <a id="dLabel" type="button" class="more-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-ellipsis-v"></i>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">';
                           
                           $temp = '';

                           if($records->status == 1){

                                    $temp .= '<li><a href="'.URL_VISITORS.'/'.$records->visitor_id.'"><i class="fa fa-pencil"></i>'.getPhrase("edit").'</a></li>';
                           }

                       
                            
                           if(checkRole(getUserGrade(2))) {
                    $temp .= ' <li><a href="javascript:void(0);" onclick="deleteVisitor(\''.$records->visitor_id.'\');"><i class="fa fa-trash"></i>'. getPhrase("delete").'</a></li>';
                      }
                    
                    $temp .='</ul></div>';

                    $link_data .=$temp;
            return $link_data;
            })

     
          ->editColumn('name', function($records){
        
           return ucwords($records->name);
        })

        ->editColumn('user_id', function($records){
        
           $user   = User::find($records->user_id);
           if($user)
             return ucwords($user->name);
           return '-';
        })

         ->editColumn('status', function($records){
        
            if( $records->status == 0 ){
               return '<span class="label label-success">Out</span>';
            }
             return '<span class="label label-warning">In</span>';
        })

            ->editColumn('updated_at', function($records){
        
            if( $records->status == 0 ){
               return $records->updated_at;
            }
             return '-';
        })
         
        ->make();
    }


    public function store(Request $request)
    {


       if(!checkRole(getUserGrade(19)))
      {
        prepareBlockUserMessage();
        return back();
      }

      $rules = [
         'name'          => 'bail|required|max:100' ,
         'email'         => 'bail|required|email',
         'phone_number'  => 'bail|required',
         'coming_from'   => 'bail|required',
         'role_id'       => 'bail|required',
         'user_id'       => 'bail|required',
         'representing'  => 'bail|required',
            ];

        $this->validate($request, $rules);

        Visitors::create($request->all());
        
        flash('success','visitor_id_created_successfully', 'success');
        return redirect(URL_VISITORS);
    }

      public function update(Request $request ,$id)
    {
       
        if(!checkRole(getUserGrade(19)))
      {
        prepareBlockUserMessage();
        return back();
      }

       $rules = [
         'name'          => 'bail|required|max:100' ,
         'email'         => 'bail|required|email',
         'phone_number'  => 'bail|required',
         'coming_from'   => 'bail|required',
         'role_id'       => 'bail|required',
         'user_id'       => 'bail|required',
         'representing'  => 'bail|required',
            ];

        $this->validate($request, $rules);
        
        $visitor  = Visitors::find($id);
        $visitor->update($request->all());
        
        flash('success','visitor_id_updated_successfully', 'success');
        return redirect(URL_VISITORS);
    }

    public function getUsers(Request $request)
    {
        
        $users  = User::where('role_id',$request->role_id)
                        ->select(['name','id'])
                        ->get();

        return json_encode( array( 'users'=>$users ) );                
    }

    public function visitorDetails(Request $request)
    {
          $visitor = Visitors::join('users','users.id','=','visitors.user_id')
                               ->join('roles','roles.id','=','visitors.role_id')
                               ->where('visitors.id',$request->visitor_id)
                               ->select(['visitors.id','visitors.name as visitor_name','visitors.email as visitor_email','coming_from','visitors.phone_number','representing','users.name as user_name','visitors.created_at','roles.display_name'])
                               ->first(); 
          return $visitor;                     
    }


    public function logoutVisitor(Request $request)
    {
       
       $record  = Visitors::find($request->visitor_id);
       $record->status  = 0;
       $record->save();

       flash('success','visitor_logout_successfully', 'success');
        return redirect(URL_VISITORS);
    }

     public function delete(Request $request)
    {
      // dd($request);
      if(!checkRole(getUserGrade(19)))
      {
        prepareBlockUserMessage();
        return back();
      }
      
         $record  = Visitors::find($request->myvisitor_id);
         if( $record->status == 1 ){

            flash('Oops..!','visitor_not_checked_out','overlay');
         }else{

          $record->delete();
          flash('success','visitor_deleted_successfully', 'success');
         }

        return redirect(URL_VISITORS);
       
    }

}