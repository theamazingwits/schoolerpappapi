<?php

namespace App\Http\Controllers;
use \App;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\Student;
use App\StudentAllergy;
use App\StudentMedicaltreatment;
use App\BasicStudhealth;
use App\StudentLeave;
use App\StudentHostalLeave;
use App\StudentHostalAttendance;
use App\StudentHostalAttendanceList;
use Yajra\Datatables\Datatables;
use DB;
use Spatie\Activitylog\Models\Activity;

class ParentsController extends Controller
{
     public function __construct()
    {
         $currentUser = \Auth::user();
      
      
      $this->middleware('auth');
    
    }


    
    /**
    * Display a listing of the resource.
    *
    * @return Response
    */
     public function index()
     {
       
       $user = getUserWithSlug();

      if(!checkRole(getUserGrade(7)))
      {
        prepareBlockUserMessage();
        return back();
      }

       if(!isEligible($user->slug))
        return back();
 
       $data['records']      = FALSE;
       $data['user']       = $user;
       $data['title']        = getPhrase('children');
       $data['active_class'] = 'children';
       $data['layout']       = getLayout();
       return view('parent.list-users', $data);
     }

     /**
     * This method returns the datatables data to view
     * @return [type] [description]
     */
    
    public function getDatatable($slug)
    {
        $records = array();
        $user = getUserWithSlug($slug);
        
        $records = User::select(['name', 'image','email',  'slug', 'id'])->where('parent_id', '=', $user->id)->get();
        
        return Datatables::of($records)
        ->addColumn('action', function ($records) {
         $buy_package = '';
        
          if(!isSubscribed('main',$records->slug)==1)
           
            return '<div class="dropdown more">
                        <a id="dLabel" type="button" class="more-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-ellipsis-v"></i>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                           <li><a href="'.URL_USERS_EDIT.$records->slug.'"><i class="fa fa-pencil"></i>'.getPhrase("edit").'</a></li>

                        </ul>
                    </div>';
            })
            
         ->editColumn('name', function($records)
         {
          return '<a href="'.URL_USER_DETAILS.$records->slug.'" title="'.$records->name.'">'.ucfirst($records->name).'</a>';
         })       
         ->editColumn('image', function($records){
            return '<img src="'.getProfilePath($records->image).'"  />';
        })
         ->removeColumn('slug')
         ->removeColumn('id')

        ->make();
    }

    public function getAllergyDataTable()
    {
        $records = array();
        $user = getUserWithSlug();

        $records =   DB::table('allergy_food as a')
                  ->join('users as b', 'a.student_id', 'b.id')
                  ->select('b.name','b.image','a.allergy_food','b.slug','a.id')
                  ->where('a.parent_id',$user->id)
                  ->orderBy('id','DESC')
                  ->get();

        return Datatables::of($records)
        ->addColumn('action', function ($records) {
         $buy_package = '';
        
          if(!isSubscribed('main',$records->slug)==1)
           
            return '<div class="dropdown more">
                        <a id="dLabel" type="button" class="more-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-ellipsis-v"></i>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                           <li><a href="'.URL_PARENT_CHILDREN_HEALTHRECORD_EDIT.$records->id.'"><i class="fa fa-pencil"></i>'.getPhrase("edit").'</a></li>
                  
                   </ul></div>';
              
            })
            
         ->editColumn('name', function($records)
         {
          return '<a href="'.URL_USER_DETAILS.$records->slug.'" title="'.$records->name.'">'.ucfirst($records->name).'</a>';
         })       
         ->editColumn('image', function($records){
            return '<img src="'.getProfilePath($records->image).'"  />';
        })
         ->removeColumn('slug')
         ->removeColumn('id')

        ->make();
    }
    public function getMedicaltreatmentDataTable()
    {
        $records = array();
        $user = getUserWithSlug();

        // $records =   DB::table('medical_treatment as a')
        //           ->join('users as b', 'a.student_id', 'b.id')
        //           ->select('b.name','b.image','a.treatment_name','a.medicine_name','b.slug','a.id')
        //           ->get();

        $records = DB::table('medical_treatment as a')
                  ->join('users as b', 'a.student_id', 'b.id')
                  ->select('b.name','b.image','a.treatment_name','a.medicine_name','b.slug','a.id')
                  ->where('a.parent_id',$user->id)
                  ->orderBy('id','DESC')
                  ->get();

        return Datatables::of($records)
        ->addColumn('action', function ($records) {
         $buy_package = '';
        
          if(!isSubscribed('main',$records->slug)==1)
           
            return '<div class="dropdown more">
                        <a id="dLabel" type="button" class="more-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-ellipsis-v"></i>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                           <li><a href="'.URL_PARENT_CHILDREN_MEDICALTREATMENT_EDIT.$records->id.'"><i class="fa fa-pencil"></i>'.getPhrase("edit").'</a></li>
                        </ul>

                     
                    </div>';
            })
            
         ->editColumn('name', function($records)
         {
          return '<a href="'.URL_USER_DETAILS.$records->slug.'" title="'.$records->name.'">'.ucfirst($records->name).'</a>';
         })       
         ->editColumn('image', function($records){
            return '<img src="'.getProfilePath($records->image).'"  />';
        })
         ->removeColumn('slug')
         ->removeColumn('id')

        ->make();
    }
    public function getBasicstudhealthDataTable()
    {
        $records = array();
        $user = getUserWithSlug();

        $records =   DB::table('basic_studhealth as a')
                  ->join('users as b', 'a.student_id', 'b.id')
                  ->select('b.name','b.image','a.height','a.weight','a.blood_group','b.slug','a.id')
                  ->where('a.parent_id',$user->id)
                  ->orderBy('id','DESC')
                  ->get();


        return Datatables::of($records)
        ->addColumn('action', function ($records) {
         $buy_package = '';
        
          if(!isSubscribed('main',$records->slug)==1)
           
            return '<div class="dropdown more">
                        <a id="dLabel" type="button" class="more-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-ellipsis-v"></i>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                           <li><a href="'.URL_PARENT_CHILDREN_BASICSTUDENTHEALTH_EDIT.$records->id.'"><i class="fa fa-pencil"></i>'.getPhrase("edit").'</a></li>
                  
                   </ul></div>';
              
            })
            
         ->editColumn('name', function($records)
         {
          return '<a href="'.URL_USER_DETAILS.$records->slug.'" title="'.$records->name.'">'.ucfirst($records->name).'</a>';
         })       
         ->editColumn('image', function($records){
            return '<img src="'.getProfilePath($records->image).'"  />';
        })
         ->removeColumn('slug')
         ->removeColumn('id')

        ->make();
    }
    public function getLeaveDataTable()
    {
        $records = array();
        $user = getUserWithSlug();

        $students = User::whereIn('parent_id',[$user->id])
                     ->select('name','id')
                     ->get();
         
        //print_r($students);

        $studentid = [];
          foreach($students as $value){
            $studentid[] = $value->id;
              }   
              // print_r($studentid);
              // exit;
        $records =  DB::table('student_leave as studlev')
              ->join('users as use', 'studlev.student_id', 'use.id')
              ->join('staff as staf', 'studlev.staff_id', 'staf.staff_id')
              ->select('use.name','use.image','staf.first_name','studlev.leave_start_date','studlev.leave_end_date',
              'studlev.leave_type','studlev.leave_time','studlev.leave_reason','studlev.role','studlev.leave_status')
              ->whereIn('studlev.student_id',$studentid)
              // ->orderBy('updated_at', 'desc')
              ->get();

              // print_r('test'.$records);
           return Datatables::of($records)
           ->addColumn('leave_type', function ($records) {

            if($records){
 
             if($records->leave_type== 0){
 
                  return '<span>Sick Leave</span>';
             }
             elseif ($records->leave_type == 1) {
 
                return '<span>Personal leave</span>';
             }else{
                 
            }
 
           }
         }) 
         ->addColumn('leave_time', function ($records) {

            if($records){
 
             if($records->leave_time== 0){
 
                  return '<span class="label label-success">Half day</span>';
             }
             elseif ($records->leave_time == 1) {
 
                return '<span class="label label-warning">Full day</span>';
             }else{
              }
 
           }
         }) 
         ->addColumn('role', function ($records) {

            if($records){
 
             if($records->role== 1){
 
                  return '<span class="label label-success">Parent</span>';
             }
             elseif ($records->role == 2) {
 
                return '<span class="label label-primary">Student</span>';
             }else{
                 
          
             }
 
           }
         }) 

          ->addColumn('leave_status', function ($records) {

           if($records){

            if($records->leave_status== 1){

                 return '<span class="label label-success">Accepted</span>';
            }
            elseif ($records->leave_status == 2) {

               return '<span class="label label-danger">Rejected</span>';
            }else{
                
               return '<span class="label label-info">Waiting</span>';
            }

          }
        }) 
    
         ->editColumn('image', function($records){
            return '<img src="'.getProfilePath($records->image).'"  />';
        })
        //  ->removeColumn('slug')
        //  ->removeColumn('id')

        ->make();
    }

    public function childrenAnalysis()
    {
       
       $user = getUserWithSlug();

      if(!checkRole(getUserGrade(7)))
      {
        prepareBlockUserMessage();
        return back();
      }

       if(!isEligible($user->slug))
        return back();
 
       $data['records']      = FALSE;
       $data['user']       = $user;
       $data['title']        = getPhrase('children_analysis');
       $data['active_class'] = 'analysis';
       $data['layout']       = getLayout();
       return view('parent.list-users', $data);
    }

  /**
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function allergyfoodindex()
    {

      $allergyData =  DB::table('allergy_food as a')
                  ->join('users as b', 'a.student_id', 'b.id')
                  ->select('a.id','a.allergy_food','a.student_id','b.name','b.image')
                  ->get();

      $user = getUserWithSlug();

     if(!checkRole(getUserGrade(7)))
     {
       prepareBlockUserMessage();
       return back();
     }

      if(!isEligible($user->slug))
       return back();

      $data['records']      = FALSE;
      $data['user']         = $user;
      $data['data']         = $allergyData;
      $data['title']        = getPhrase('Food Details');
      $data['active_class'] = 'healthrecorddetails';
      $data['layout']       = getLayout();
      return view('parent.healthrecorddetails.list', $data);
    }
    public function allergyfoodcreate()
    {
       
      $user = getUserWithSlug();

      if(!checkRole(getUserGrade(7)))
      {
        prepareBlockUserMessage();
        return back();
      }

       if(!isEligible($user->slug))
        return back();

      $students = User::whereIn('parent_id',[$user->id])
        ->select('name','id')
        ->get(); 

      //  print_r($students);

       $data['records']         = FALSE;
       $data['user']            = $user;
       $data['students']        = $students;
       $data['title']           = getPhrase('health_record');
       $data['active_class']    = 'healthrecord';
       $data['layout']          = getLayout();
       return view('parent.healthrecorddetails.health-record', $data);
    }

        /**
     * This method adds record to DB
     * @param  Request $request [Request Object]
     * @return void
     */
    public function allergyfoodstore(Request $request)
    {
      $user = getUserWithSlug();
      //  $this->validate($request, [
      //    'allergy_food'          => 'bail|required|max:30|,allergy_food'
      //       ]);

      print_r($request->student_name);
          
    	  $record = new StudentAllergy();
        $record->allergy_food 			= $request->allergy_food;
        $record->student_id 			  = $request->student_name;
        $record->parent_id 			    = $user->id;
        $record->save();
        flash('success','record_added_successfully', 'success');
    	return redirect('children/healthrecord');
    }

    public function edit($slug)
    {

      $id = $slug;
     
      $user = getUserWithSlug();

      if(!checkRole(getUserGrade(7)))
      {
        prepareBlockUserMessage();
        return back();
      }

        if(!isEligible($user->slug))
        return back();

      $students = User::whereIn('parent_id',[$user->id])
        ->select('name','id')
        ->get(); 
      
      $records =   DB::table('allergy_food as a')
                  ->join('users as b', 'a.student_id', 'b.id')
                  ->select('b.name','a.student_id','a.allergy_food','b.slug','a.id')
                  ->where('a.id',$id)
                  ->first();

        $data['records']         = $records;
        $data['user']            = $user;
        $data['students']        = $students;
        $data['title']           = getPhrase('health_record');
        $data['active_class']    = 'healthrecord';
        $data['layout']          = getLayout();
        return view('parent.healthrecorddetails.health-record', $data);
    }

    public function update(Request $request, $slug)
    {
      $record = StudentAllergy::find($slug);
      $record->allergy_food 			= $request->allergy_food;
      $record->student_id 			  = $request->student_name;
      $record->save();
      flash('success','record_updated_successfully', 'success');
    	return redirect('children/healthrecord');
    }

    public function delete(Request $request, $slug)
    {
      $user = getUserWithSlug();
      $record = StudentAllergy::find($slug);
      $record->allergy_food 			= $request->allergy_food;
      $record->student_id 			  = $request->student_name;
      $record->parent_id 			    = $user->id;
      $record->delete();
      flash('success','record_deleted_successfully', 'success');
    	return redirect('children/healthrecord');
    }

   

  
  /**
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function medicaltreatmentindex()
    {
      
      $user = getUserWithSlug();

     if(!checkRole(getUserGrade(7)))
     {
       prepareBlockUserMessage();
       return back();
     }

      if(!isEligible($user->slug))
       return back();

      $data['records']      = FALSE;
      $data['user']         = $user;
      $data['title']        = getPhrase('Medical Treatment Details');
      $data['active_class'] = 'healthrecorddetails';
      $data['layout']       = getLayout();
      return view('parent.medicaltreatment.list', $data);
    }
  
    public function medicaltreatmentcreate()
    {
       
       $user = getUserWithSlug();

      if(!checkRole(getUserGrade(7)))
      {
        prepareBlockUserMessage();
        return back();
      }

       if(!isEligible($user->slug))
        return back();
        $students = User::whereIn('parent_id',[$user->id])
        ->select('name','id')
        ->get(); 
 
       $data['records']         = FALSE;
       $data['user']            = $user;
       $data['students']        = $students;
       $data['title']           = getPhrase('medical treatment details');
       $data['active_class']    = 'medical treatment';
       $data['layout']          = getLayout();
       return view('parent.medicaltreatment.medicaltreatment', $data);
    }  

      /**
     * This method adds record to DB
     * @param  Request $request [Request Object]
     * @return void
     */
    public function medicaltreatmentstore(Request $request)
    {
      $user = getUserWithSlug();
  

      print_r($request->student_name);
          
    	  $record = new StudentMedicaltreatment();
        $record->treatment_name 		= $request->treatment_name;
        $record->medicine_name 		  = $request->medicine_name;
        $record->student_id 			  = $request->student_name;
        $record->parent_id 			    = $user->id;
        $record->save();
        flash('success','record_added_successfully', 'success');
    	return redirect('children/medicaltreatment');
    }

    public function medicaltreatmentedit($slug)
    {

      $id = $slug;
     
      $user = getUserWithSlug();

      if(!checkRole(getUserGrade(7)))
      {
        prepareBlockUserMessage();
        return back();
      }

        if(!isEligible($user->slug))
        return back();

      $students = User::whereIn('parent_id',[$user->id])
        ->select('name','id')
        ->get(); 
      
      $records = DB::table('medical_treatment as a')
        ->join('users as b', 'a.student_id', 'b.id')
        ->select('b.name','b.image','a.treatment_name','a.medicine_name','b.slug','a.id')
        ->where('a.id',$id)
        ->first();

        $data['records']         = $records;
        $data['user']            = $user;
        $data['students']        = $students;
        $data['title']           = getPhrase('medical treatment details');
        $data['active_class']    = 'medical treatment';
        $data['layout']          = getLayout();
        return view('parent.medicaltreatment.medicaltreatment', $data);
    }

    public function medicaltreatmentupdate(Request $request, $slug)
    {
      $record = StudentMedicaltreatment::find($slug);
      $record->treatment_name 		= $request->treatment_name;
      $record->medicine_name 		  = $request->medicine_name;
      $record->student_id 			  = $request->student_name;
      $record->save();
      flash('success','record_updated_successfully', 'success');
    	return redirect('children/medicaltreatment');
    }

    /**
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function basicStudhealthindex()
    {


      $user = getUserWithSlug();

     if(!checkRole(getUserGrade(7)))
     {
       prepareBlockUserMessage();
       return back();
     }

      if(!isEligible($user->slug))
       return back();

      $data['records']      = FALSE;
      $data['user']         = $user;
      $data['title']        = getPhrase('basic_health_Details');
      $data['active_class'] = 'basichealthdetails';
      $data['layout']       = getLayout();
      return view('parent.basichealth.list', $data);
    }
    public function basicStudhealthcreate()
    {
       
      $user = getUserWithSlug();

      if(!checkRole(getUserGrade(7)))
      {
        prepareBlockUserMessage();
        return back();
      }

       if(!isEligible($user->slug))
        return back();

      $students = User::whereIn('parent_id',[$user->id])
        ->select('name','id')
        ->get(); 

      //  print_r($students);

       $data['records']         = FALSE;
       $data['user']            = $user;
       $data['students']        = $students;
       $data['title']           = getPhrase('basic_health_details');
       $data['active_class']    = 'basic_health_details';
       $data['layout']          = getLayout();
       return view('parent.basichealth.basic_studhealth', $data);
    }

        /**
     * This method adds record to DB
     * @param  Request $request [Request Object]
     * @return void
     */
    public function basicStudhealthstore(Request $request)
    {
      $user = getUserWithSlug();


      print_r($request->student_name);
          
    	  $record = new BasicStudhealth();
        $record->height 		      	= $request->height;
        $record->weight 		      	= $request->weight;
        $record->blood_group 		    = $request->blood_group;
        $record->student_id 			  = $request->student_name;
        $record->parent_id 			    = $user->id;
        $record->save();
        flash('success','record_added_successfully', 'success');
    	return redirect('children/basicstudenthealth');
    }

    public function basicStudhealthedit($slug)
    {

      $id = $slug;
     
      $user = getUserWithSlug();

      if(!checkRole(getUserGrade(7)))
      {
        prepareBlockUserMessage();
        return back();
      }

        if(!isEligible($user->slug))
        return back();

      $students = User::whereIn('parent_id',[$user->id])
        ->select('name','id')
        ->get(); 
      
      $records =   DB::table('basic_studhealth as a')
                  ->join('users as b', 'a.student_id', 'b.id')
                  ->select('b.name','a.student_id','a.height','a.weight','a.blood_group','b.slug','a.id')
                  ->where('a.id',$id)
                  ->first();

        $data['records']         = $records;
        $data['user']            = $user;
        $data['students']        = $students;
        $data['title']           = getPhrase('basic_health_details');
        $data['active_class']    = 'basichealthdetails';
        $data['layout']          = getLayout();
        return view('parent.basichealth.basic_studhealth', $data);
    }

    public function basicStudhealthupdate(Request $request, $slug)
    {
      $record = BasicStudhealth::find($slug);
      $record->height 		     	= $request->height;
      $record->weight 			    = $request->weight;
      $record->blood_group 			= $request->blood_group;
      $record->student_id 			= $request->student_name;
      $record->save();
      flash('success','record_updated_successfully', 'success');
    	return redirect('children/basicstudenthealth');
    }


  /**
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function leaveindex()
    {

      $user = getUserWithSlug();

     if(!checkRole(getUserGrade(7)))
     {
       prepareBlockUserMessage();
       return back();
     }

      if(!isEligible($user->slug))
       return back();

      $data['records']      = FALSE;
      $data['user']         = $user;
      $data['title']        = getPhrase('Leave Details');
      $data['active_class'] = 'leavedetails';
      $data['layout']       = getLayout();
      return view('parent.leaverequest.list', $data);
    }
    public function leavecreate()
    {
       
      $user = getUserWithSlug();

      if(!checkRole(getUserGrade(7)))
      {
        prepareBlockUserMessage();
        return back();
      }

       if(!isEligible($user->slug))
        return back();

      $students = User::whereIn('parent_id',[$user->id])
        ->select('name','id')
        ->get(); 
    //  echo('test'.$studentdetails);
      //  exit;
      //  print_r($studentdetails);

       $data['records']         = FALSE;
       $data['user']            = $user;
       $data['students']        = $students;
       $data['title']           = getPhrase('apply_leave');
       $data['active_class']    = 'applyleave';
       $data['layout']          = getLayout();
       return view('parent.leaverequest.leaverequest', $data);
    }

        /**
     * This method adds record to DB
     * @param  Request $request [Request Object]
     * @return void
     */
    public function leavestore(Request $request)
    {
      $user = getUserWithSlug();


      // print_r($request->student_id);
          
      $studentdetails = Student::whereIn('user_id',[$request->student_id])
      ->select('academic_id','course_parent_id','course_id')
      ->first();
      // echo('test'.$studentdetails->academic_id);
      // exit;
      $records =   DB::table('staff')
      		->where('course_parent_id',$studentdetails->course_parent_id)
      		->where('course_id',$studentdetails->course_id)
      		->first(['staff_id']);
      //  print_r($records->staff_id);
      //  exit;

        $record = new StudentLeave();
        $record->student_id 		  	= $request->student_id;
        $record->parent_id 			    = $user->id;
        $record->staff_id 			    = $records->staff_id;
        $record->course_id 			    = $studentdetails->course_id;
        $record->course_parent_id 	= $studentdetails->course_parent_id;
        $record->academic_id 			  = $studentdetails->academic_id;
        $record->leave_type 		  	= $request->leave_type;
        $record->leave_time 			  = $request->leave_time;
        $record->leave_start_date   = $request->start_date;
        $record->leave_end_date 		= $request->end_date;
        $record->leave_reason 	  	= $request->leave_reason;
        // print_r($record);
        // exit;
        $record->save();
        flash('success','leave_applyed_successfully', 'success');
    	return redirect('children/leave');
    }
    
  /**
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function hostalleaveindex()
    {

      $user = getUserWithSlug();

     if(!checkRole(getUserGrade(7)))
     {
       prepareBlockUserMessage();
       return back();
     }

      if(!isEligible($user->slug))
       return back();

      $data['records']      = FALSE;
      $data['user']         = $user;
      $data['title']        = getPhrase('Hostal Leave Details');
      $data['active_class'] = 'hostalleavedetails';
      $data['layout']       = getLayout();
      return view('parent.hostalleave.list', $data);
    }
    public function gethostalLeaveDataTable()
    {
        $records = array();
        $user = getUserWithSlug();

        $students = User::whereIn('parent_id',[$user->id])
                     ->select('name','id')
                     ->get();
         
        //print_r($students);

        $studentid = [];
          foreach($students as $value){
            $studentid[] = $value->id;
              }   
              // print_r($studentid);
              // exit;
        $records =  DB::table('student_hostel_leave_details as studhoslev')
              ->join('users as use', 'studhoslev.student_id', 'use.id')
              ->join('hostel as hstl', 'studhoslev.hostel_id', 'hstl.id')
              // ->join('hostel_rooms as hstlrm', 'studhoslev.room_id', 'hstlrm.id')
              ->select('use.name','use.image','hstl.type','studhoslev.room_number','studhoslev.leave_start_date','studhoslev.leave_end_date',
              'studhoslev.leave_for','studhoslev.leave_session','studhoslev.leave_reason','studhoslev.leave_status')
              ->whereIn('studhoslev.student_id',$studentid)
              // ->orderBy('updated_at', 'desc')
              ->get();

              // print_r($records->type);
              // exit;
           return Datatables::of($records)

           ->editColumn('hostel_name', function ($records) {

            if($records){
 
             if($records->type == 0){
 
                  return '<span>Girls hostal</span>';
             }
             elseif ($records->type == 1) {
 
                return '<span>Boys hostal 1</span>';
             }else{
                 
            }
 
           }
         }) 
    

          ->addColumn('leave_status', function ($records) {

           if($records){

            if($records->leave_status== 1){

                 return '<span class="label label-success">Accepted</span>';
            }
            elseif ($records->leave_status == 2) {

               return '<span class="label label-danger">Rejected</span>';
            }else{
                
               return '<span class="label label-info">Waiting</span>';
            }

          }
        }) 
    
         ->editColumn('image', function($records){
            return '<img src="'.getProfilePath($records->image).'"  />';
        })
        //  ->removeColumn('slug')
        //  ->removeColumn('id')

        ->make();
    }

    /**
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function hostalattendanceindex()
    {

      $user = getUserWithSlug();

     if(!checkRole(getUserGrade(7)))
     {
       prepareBlockUserMessage();
       return back();
     }

      if(!isEligible($user->slug))
       return back();

      $data['records']      = FALSE;
      $data['user']         = $user;
      $data['title']        = getPhrase('Hostal Attendance Details');
      $data['active_class'] = 'hostalattendancedetails';
      $data['layout']       = getLayout();
      return view('parent.hostalattendance.list', $data);
    }
    public function gethostalAttendanceDataTable()
    {
        $records = array();
        $user = getUserWithSlug();

        $students = User::whereIn('parent_id',[$user->id])
                     ->select('name','id')
                     ->get();
         
        // print_r($students);

        $studentid = [];
          foreach($students as $value){
            $studentid[] = $value->id;
              }   
              // print_r($studentid);
              // exit;
        $records =  DB::table('hostel_attendance_list as hosatnlst')
              ->join('users as use', 'hosatnlst.user_id', 'use.id')
              ->join('hostel_attendance as hosatn', 'hosatnlst.attendance_id', 'hosatn.id')
              ->select('hosatn.date','use.name','use.image','hosatnlst.status','hosatn.session')
              ->whereIn('hosatnlst.user_id',$studentid)
              ->get();

              // print_r($records);
              // exit;
           return Datatables::of($records)
    
         ->editColumn('image', function($records){
            return '<img src="'.getProfilePath($records->image).'"  />';
        })
         // ->removeColumn('status')
        //  ->removeColumn('id')

        ->make();
    }
}

