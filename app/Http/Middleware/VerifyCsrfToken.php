<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
     protected $except = [
      'payments/paypal/status-success',
       'payments/payu/status-success',
       'payments/payu/status-cancel',
       'paynow/fee/pau-success',
       'paynow/fee/pau-cancel',
       'paynow/fee/paypal-success',
       'paynow/fee/paypal-cancel',
       'paytm-callback',
       'donation/success',
       'donation/fail',
       'getStudentParentList',
       'getStaffs',
       'sendTrackPushNotification',
       'sendTripPushNotification',
       'emailNotification/promotions/emaillist',
       'emailNotification/promotions/add',
       'emailNotification/promotions/sendMail',
       'emailNotification/eventnotification/sendMail',
       'emailNotification/communication/sendMail'
    ];
}
