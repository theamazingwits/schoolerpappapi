<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventoryItem extends Model
{
    protected $table="inventory_item";
   

    public static function getRecordWithSlug($slug)
    {
        return InventoryItem::where('slug', '=', $slug)->first();
    }
}