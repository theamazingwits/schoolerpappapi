<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlumniEvent extends Model
{
      protected $table="almni_events";

      protected $fillable = ['title','short_description','long_description','date','volunteer_id','image','added_by','eaddress'];
}
