<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;
// use Spatie\Activitylog\LogsActivityInterface;
// use Spatie\Activitylog\Traits\LogsActivity;

class QuizResult extends Model  
{
    protected $table = 'quizresults';
    
    public static function getRecordWithSlug($slug)
    {
        return QuizResult::where('slug', '=', $slug)->first();
    }


	

	/**
	 * Returns the history of exam attempts based on the current logged in user
	 * @return [type] [description]
	 */
    public function getHistory()
    {
    	return QuizResult::where('user_id', '=', Auth::user()->id)->orderBy('id', 'DESC')->limit(5)->get();
    }

    /**
     * Returns the list of toppers based on the highest 
     * percentage scored and the current quiz
     * @param  string $quiz_id [description]
     * @return [type]          [description]
     */
    public function getToppersList($quiz_id='')
    {
    	$list = array();
    	if($quiz_id=='')
    		return $list;
 
    	return QuizResult::
                         where('quiz_id', '=', $quiz_id)
    					->where('exam_status', '=', 'pass')
    					->orderBy('percentage', 'DESC')->limit(5)
    					->groupBy('user_id')
    					->get();
    }
 	
 	/**
 	 * Returns the current result quiz record
 	 * @return [type] [description]
 	 */
    public function quizName()
    {
    	return $this->belongsTo('App\Quiz', 'quiz_id');
    }

    /**
     * Returns the current quiz user record
     * @return [type] [description]
     */
    public function getUser()
    {
    	return $this->belongsTo('App\User', 'user_id');
    }

    public function getOverallSubjectsReport($user)
    {
       $overallSubjectAnalysis = [];
            $records = Quiz::join('quizresults', 'quizzes.id', '=', 'quizresults.quiz_id')
            ->select(['subject_analysis','quizresults.user_id'])
            ->where('quizresults.user_id', '=', $user->id)
            ->get();

       

       foreach ($records as $result) {
        if(!isset($result->subject_analysis))
            continue;
         foreach(json_decode($result->subject_analysis) as $subject)
         {

            $subject_id = $subject->subject_id;
          if(!array_key_exists($subject_id, $overallSubjectAnalysis)){
            $overallSubjectAnalysis[$subject_id]['subject_name'] = Subject::where('id','=',$subject_id)->first()->subject_title;
            $overallSubjectAnalysis[$subject_id]['correct_answers'] = 0;
            $overallSubjectAnalysis[$subject_id]['wrong_answers']   = 0;
            $overallSubjectAnalysis[$subject_id]['not_answered']    = 0;
            $overallSubjectAnalysis[$subject_id]['time_to_spend']   = 0;
            $overallSubjectAnalysis[$subject_id]['time_spent']      = 0;
            $overallSubjectAnalysis[$subject_id]['time_spent_on_correct_answers']    = 0;
            $overallSubjectAnalysis[$subject_id]['time_spent_on_wrong_answers']      = 0;
            $overallSubjectAnalysis[$subject_id]['time_spent_on_wrong_answers']      = 0;
            $overallSubjectAnalysis[$subject_id]['time_spent_on_wrong_answers']      = 0;
          }
            $overallSubjectAnalysis[$subject_id]['correct_answers'] += $subject->correct_answers;
            $overallSubjectAnalysis[$subject_id]['wrong_answers']   += $subject->wrong_answers;
            $overallSubjectAnalysis[$subject_id]['not_answered']    += $subject->not_answered;
            $overallSubjectAnalysis[$subject_id]['time_to_spend']   += $subject->time_to_spend;
            $overallSubjectAnalysis[$subject_id]['time_spent']      +=  $subject->time_spent;
            $overallSubjectAnalysis[$subject_id]['time_spent_on_correct_answers']    +=  $subject->time_spent_correct_answers;
            $overallSubjectAnalysis[$subject_id]['time_spent_on_wrong_answers']      +=  $subject->time_spent_wrong_answers;
         }
       }

       return $overallSubjectAnalysis;
    }

    /**
     * Returns the overall performanance of the user
     * @param  [type] $user [description]
     * @return [type]       [description]
     */
    public function getOverallQuizPerformance($user)
    {
        $overallQuizPerformance = [];
            $records = Quiz::join('quizresults', 'quizzes.id', '=', 'quizresults.quiz_id')
            ->select(['quiz_id', 'quizzes.title',DB::raw('Max(percentage) as percentage'), 'quizresults.user_id'])
            ->where('quizresults.user_id', '=', $user->id)
            ->groupBy('quizresults.quiz_id')

            ->get();
        return $records;
    }


    public function getQuizzesUsage($type='', $user_id='', $year='')
    {
         $query = 'select count(qr.quiz_id) as total, q.title as quiz_title from quizzes q, quizresults qr where qr.quiz_id = q.id group by qr.quiz_id';

         if($type=='paid')
         {
              $query = 'select count(qr.quiz_id) as total, q.title as quiz_title from quizzes q, quizresults qr where qr.quiz_id = q.id and q.is_paid=1 group by qr.quiz_id';              
         }
         $result = DB::select($query);
 
         return $result;
    }

    public static function latestQuizAttempts($limit = 5)
    {
        $role_id = getRoleData('student');
        $users = QuizResult::join('users', 'users.id','=','quizresults.user_id')
                            ->join('quizzes','quizzes.id','=','quizresults.quiz_id')
                            ->where('quizzes.type','=','online')
                            ->where('users.role_id','=',$role_id)
                            ->orderBy('quizresults.id','desc')
                            ->select(['users.slug as slug','users.id as user_id', 'users.name','percentage', 'image','quizzes.title'])
                            ->limit($limit)
                            ->groupBy('users.id')
                            ->get();
        return $users;
    }


       // This method return the consolidate marks for the student
   // with the parameters of student academic years and user id 

    public static function getConsolidateReport($academic_data,$user_id)
    {
// dd($academic_data);
    $offline_results = array();
    foreach ($academic_data as $adata) {
      // dd($adata);
       $offline_results[]    = QuizResult::join('quizzes','quizzes.id','=','quizresults.quiz_id')
                                ->where('user_id','=',$user_id)
                                ->where('quizresults.quiz_type','!=','online')
                                ->where('academic_id','=',$adata->from_academic_id)
                                ->where('course_id','=',$adata->from_course_id)
                                ->where('year','=',$adata->from_year)
                                ->where('semister','=',$adata->from_semister)
                                ->select(['subject_id','quiz_type','marks_obtained'])
                                ->orderBy('quizresults.created_at','asc')
                                ->get();
    }
    // dd($offline_results);
    $offline_list   = array();
    foreach ($offline_results as $key => $value) {
       
       // dd($value);
      foreach ($value as $result) {
        $subject_title   = Subject::where('id','=',$result->subject_id)->first()->subject_title;
        $offline_list[$key][$result->subject_id]['subject']  = $subject_title;
        $offline_list[$key][$result->subject_id]['offline_marks']  = $result->marks_obtained;
        // $offline_list[$key]['quiz_type'][]    = $result->quiz_type;
        // $offline_list[$key]['marks'][]    = $result->marks_obtained;
      }
    }
    // dd($offline_list);


    $online_results = array();
    foreach ($academic_data as $adata) {
      
       $result_type  =  getSetting('online_exam_results', 'exam_settings');
       
       //Averae
       if($result_type == 3){

           $online_results[]    = QuizResult::join('quizzes','quizzes.id','=','quizresults.quiz_id')
                                ->where('user_id','=',$user_id)
                                ->where('quizresults.quiz_type','=','online')
                                ->where('academic_id','=',$adata->from_academic_id)
                                ->where('course_id','=',$adata->from_course_id)
                                ->where('year','=',$adata->from_year)
                                ->where('semister','=',$adata->from_semister)
                                ->select(['subject_id','quiz_type',DB::raw('AVG(marks_obtained) as marks')])
                                ->orderBy('quizresults.created_at','asc')
                                ->get();

       }elseif( $result_type == 2 )//Last Attempt
       {
         
         $online_results[]    = QuizResult::join('quizzes','quizzes.id','=','quizresults.quiz_id')
                                ->where('user_id','=',$user_id)
                                ->where('quizresults.quiz_type','=','online')
                                ->where('academic_id','=',$adata->from_academic_id)
                                ->where('course_id','=',$adata->from_course_id)
                                ->where('year','=',$adata->from_year)
                                ->where('semister','=',$adata->from_semister)
                                ->select(['subject_id','quiz_type','marks_obtained as marks'])
                                ->orderBy('quizresults.created_at','asc')
                                ->get();

       }elseif( $result_type == 1 )//First Attempt
       {
             
               $online_results[]    = QuizResult::join('quizzes','quizzes.id','=','quizresults.quiz_id')
                                ->where('user_id','=',$user_id)
                                ->where('quizresults.quiz_type','=','online')
                                ->where('academic_id','=',$adata->from_academic_id)
                                ->where('course_id','=',$adata->from_course_id)
                                ->where('year','=',$adata->from_year)
                                ->where('semister','=',$adata->from_semister)
                                ->select(['subject_id','quiz_type','marks_obtained as marks'])
                                ->orderBy('quizresults.created_at','desc')
                                ->get();
       }

     
    }
    // dd($online_results);
    $online_list   = array();
    foreach ($online_results as $key => $value) {
       
       // dd($value);
      foreach ($value as $result) {
        // $subject_title   = Subject::where('id','=',$result->subject_id)->first()->subject_title;
       
        $subject_title  = Subject::where('id','=',$result->subject_id)->first()->subject_title;
        $online_list[$key][$result->subject_id]['subject']       = $subject_title;
        $online_list[$key][$result->subject_id]['online_marks']  = (int)$result->marks;
        // $offline_list[$key]['quiz_type'][]    = $result->quiz_type;
        // $offline_list[$key]['marks'][]    = $result->marks_obtained;
      }
    }

    // dd($online_list);
    $total_list  = array();
     
    foreach ($offline_list as $key => $value) {

      foreach ($online_list as $key1 => $value1) {
       
       // check the same academic ids 
        
        if($key == $key1){

          // Start of results of each academic year

          foreach ($value as $key2=> $value2) {
            // dd($value);
              foreach ($value1 as $key3 => $value3) {
                // dd($value1);
                
                //check the subject is exist or not 

                if($key2  == $key3){    
                   // dd($key2);
                 $total_list[$key][$key2]['subject']       = $value3['subject'];

                 if(array_key_exists("offline_marks", $value2)){

                 $total_list[$key][$key2]['offline_marks'] = $value2['offline_marks']; }

                else{ 

                  $total_list[$key][$key2]['offline_marks'] = '-'; }
               

                if(array_key_exists("online_marks", $value3)){

                 $total_list[$key][$key2]['online_marks']  = $value3['online_marks']; }
                

                else{

                  $total_list[$key][$key2]['online_marks']  = '-'; }
                
                }
                // dd($total_list);

              }
          }
          //end of results of each academic year

        }

        // else{
        //    foreach ($value as $key4 => $value4) {
        //      // dd($value4);
                 
        //          $total_list[$key][$key4]['subject']       = $value4['subject'];
        //          if(array_key_exists("offline_marks", $value4)){
                
        //           $total_list[$key][$key4]['offline_marks'] = $value4['offline_marks'];
        //          }
        //          else{

        //           $total_list[$key][$key4]['offline_marks'] = '-';

        //          }
        //           if(array_key_exists("online_marks", $value4)){

        //          $total_list[$key][$key4]['online_marks']  = $value4['online_marks']; 

        //          }
        //          else{

        //           $total_list[$key][$key4]['online_marks'] = '-';

        //          }
        //        }
        //    }
       }
    }
// dd($total_list);
    return $total_list;

    }

  
  
   public static function getCourseTitle($academic_data)
   {
     // dd($academic_data);
     $titles  = array(); 
     foreach ($academic_data as $adata){
        
        $titles []  = getcoursetitle($adata->from_course_id, $adata->from_year, $adata->from_semister);
     }

     return $titles;

   }



   //This method return student passing academic year
// Except present academic details 
  public static function getStudentAcademicYears($user_id){
   
     $from_academic_details  = StudentPromotion::where('user_id','=',$user_id)
                                           ->select(['from_academic_id','from_course_parent_id','from_course_id','from_year','from_semister','user_id'])
                                            ->groupBy('from_academic_id','from_course_parent_id','from_course_id','from_year','from_semister','user_id')
                                            ->get();
      return $from_academic_details;                            
    }


  public static function getPresentAcademicYear($user_id){


    $academic_details  = Student::where('user_id','=',$user_id)
                                          ->select(['academic_id as from_academic_id','course_parent_id as from_course_parent_id','course_id as from_course_id','current_year as from_year','current_semister as from_semister','user_id'])
                                            ->get();
     

      return $academic_details;

  }



    
}


