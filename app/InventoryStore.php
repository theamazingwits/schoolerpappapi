<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventoryStore extends Model
{
    protected $table="inventory_store";
   

    public static function getRecordWithSlug($slug)
    {
        return InventoryStore::where('slug', '=', $slug)->first();
    }
}