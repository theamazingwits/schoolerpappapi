<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StoryComments extends Model
{
      protected $table="alumni_story_comments";

      protected $fillable = ['user_id','story_id','comment'];
}
