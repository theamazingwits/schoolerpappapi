<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class UserCertificates extends Model
{
   protected $table="user_certificates";

   public static function DocumetsSubmitted($user_role_id = '', $user_id = '')
   {   
   	   
   	   $user     = getUserRecord();

   	   $role_id  = $user->role_id;
   	   if($user_role_id)
   	   $role_id  = $user_role_id;

   	   $id  = $user->id;
   	   if($user_id)
   	   $id  = $user_id;

   	   if($role_id  == 5 ){

   	   	   $student  = Student::where('user_id','=',$id)->first();

	        if(!$student)
	        {
	           return FALSE;       
	        }  

	        $academic_id        = $student->academic_id;       
	        $course_parent_id   = $student->course_parent_id;       
	        $course_id          = $student->course_id;       
	        $year               = $student->current_year;       
	        $semister           = $student->current_semister;  
         
           $notifications   = AdminNotification::where('role_id',5)
                                                ->where('academic_id',$academic_id)
                                                ->where('course_id',$course_id)
                                                ->where('current_year',$year)
                                                ->where('current_semister',$semister)
                                                ->get();
    
           if(count($notifications) > 0 ){

                 $final_notifys  = [];
                 $temp  = [];

           	    foreach ($notifications as $notification) {
               
                   $temp['id']            = $notification->id;
                   $temp['title']         = $notification->title;
                   $temp['description']   = $notification->description;
                   $final_notifys[]       = $temp;
              
               }  

               return $final_notifys;
           }
           else{

           	  return FALSE;
           }

                                                       
  
   	   }
   	   else{
            
             $notifications   = AdminNotification::where('role_id',$role_id)
                                                    ->get();
    
           if(count($notifications) > 0 ){

                 $final_notifys  = [];
                 $temp  = [];

           	    foreach ($notifications as $notification) {
               
                   $temp['id']            = $notification->id;
                   $temp['title']         = $notification->title;
                   $temp['description']   = $notification->description;
                   $final_notifys[]       = $temp;
              
               }  

               return $final_notifys;
           }
           else{

           	  return FALSE;
           }


   	   }

   	   
   }

    
   
}
