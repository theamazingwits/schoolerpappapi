<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\User;
use App\SalaryTemplate;

class UserSalaryGrade extends Model
{
    protected $table = "user_salary_templates";

    
    public static function getUserTemplate($user_id)
    {
    	$record   = UserSalaryGrade::where('user_id',$user_id)->first();
    	if($record){
           
           $grade  = SalaryTemplate::find($record->template_id);
           return $grade;
    	}
    	else{
    		return FALSE;
    	}
    }
}
