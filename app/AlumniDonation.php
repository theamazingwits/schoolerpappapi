<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlumniDonation extends Model
{
      protected $table="alumni_donations";

      protected $fillable = ['title','description','date','image','added_by','amount','status'];
}
