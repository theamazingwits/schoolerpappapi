<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use \App\User;


class StudentUploadAssignment extends Notification
{
    use Queueable;
    protected $user = '';
    protected $student = '';
    protected $assignment_name = '';

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user, $student, $assignment)
    {
        // dd($assignment);
         $this->user       = $user;
         $this->student    = $student;
         $this->assignment_name = $assignment->title;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

  /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'title' => 'Student is Submitted Assignment',
            'description' => 'Student '.$this->student->getUserTitle().' uploaded assignment file for '.ucwords($this->assignment_name).' please check and approve it',
            'url' => URL_ASSIGNMENTS,
            'image' => null
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            
        ];
    }
}
