<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use \App\User;


class StaffApproveAssignment extends Notification
{
    use Queueable;
    protected $user = '';
    protected $assignment_name = '';

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user, $assignment)
    {
        // dd($assignment);
         $this->user       = $user;
         $this->assignment_name = $assignment->title;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

  /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'title' => 'Staff is Approved Assignment',
            'description' => 'Hi '.$this->user->getUserTitle().' staff is your approved assignment for '.ucwords($this->assignment_name),
            'url' => URL_STUDENT_ASSIGNMENT,
            'image' => null
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            
        ];
    }
}
