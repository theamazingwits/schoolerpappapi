<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\User;


class UserContactUs extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */

    protected $user     = null;
    protected $name     = null;
    protected $email    = null;
    protected $message  = null;

    public function __construct(User $user,$data)
    { 
       $this->user     = $user;
       $this->name     = $data['name'];
       $this->email    = $data['email'];
       $this->message  = $data['message'];
       // dd($this->message);
     
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        
        return (new MailMessage)
        ->subject('User Contact')
        ->view('emails.contact-us', 
        [

         'user_name' => $this->user->getUserTitle(),
         'name'      => $this->name,
         'email'     => $this->email,
         'user_message'   => $this->message,

        ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
