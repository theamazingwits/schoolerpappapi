<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventoryCategory extends Model
{
    protected $table="inventory_categories";
   

    public static function getRecordWithSlug($slug)
    {
        return InventoryCategory::where('slug', '=', $slug)->first();
    }
}