<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hostel extends Model
{
   protected $table="hostel";

   protected $fillable = ['name', 'branch_id', 'type', 'intake','description','address'];

   public static function isMultiBranch()
   {
   	  return FALSE;
   }
    
   
}
