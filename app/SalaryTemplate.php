<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class SalaryTemplate extends Model
{
    protected $table = "salary_templates";

    public function totalAllownses()
    {
    	
    	return DB::table('salarytemplate_allowances')
    	           ->where('salarytemplate_id',$this->id)
    	           ->sum('allowance_value');
    }

    public function totalDeductions()
    {
    	return DB::table('salarytemplate_deductions')
    	           ->where('salarytemplate_id',$this->id)
    	           ->sum('deduction_value');
    }

    public function Particulars($type='')
    {
        if($type == 'allowances'){

          $records   = DB::table('salarytemplate_allowances')
                           ->where('salarytemplate_id',$this->id)
                           ->get();

        }
        elseif ($type == 'deductions') {
              
          $records   = DB::table('salarytemplate_deductions')
                           ->where('salarytemplate_id',$this->id)
                           ->get();             
        }                   
         
         return $records;
         // dd($records);                       
    }

}
