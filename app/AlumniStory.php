<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlumniStory extends Model
{
      protected $table="alumni_stories";

      protected $fillable = ['title','description','date','image','added_by'];
}
