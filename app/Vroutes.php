<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vroutes extends Model
{
   protected $table="vroutes";

   protected $fillable = ['branch_id','name', 'cost','description'];

     public static function isMultiBranch()
   {
   	  return FALSE;
   }
    
    
 
}
