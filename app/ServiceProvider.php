<?php

namespace App;
use Illuminate\Support\Facades\Log;
use DB;
use Auth;
class ServiceProvider 
{
    public function insertNotification($data){
        $notifydata = $data->data;
        $webPushTokens = [];
        $androidPushTokens = [];
        $iosPushTokens = [];
        $values = [];
        foreach ($notifydata as $pushTokens) {
            //For GET Web Push Tokens
            if($pushTokens->push_web_id != NULL){
                $webPushTokens[] = $pushTokens->push_web_id;      
            }
            //For Get App Push Tokens
            if($pushTokens->push_app_id != NULL && $pushTokens->os_type == 1){
                $androidPushTokens[] = $pushTokens->push_app_id;      
            }else if($pushTokens->push_app_id != NULL && $pushTokens->os_type == 2){
                $iosPushTokens[] = $pushTokens->push_app_id;      
            }
            $values[] = [
                'user_name' => $pushTokens->name,
                'notification_user_id' => $pushTokens->id,
                'message' => $data->message,
                'page_id' => $data->pageId,
                'app_type' => $data->app_type,
                'created_at' => date('Y-m-d H:i:s') , 
                'updated_at' => date('Y-m-d H:i:s')
            ];
        }
        DB::table('push_notifications')
           ->insert($values);
           
        if(count($webPushTokens) > 0){
            $pushData = array(
                'pushToken' => $webPushTokens,
                'message' => $data->message,
                'type' => 1
            );
            $this->sendPush($pushData);
        }
        if(count($androidPushTokens) > 0){
            $pushData = array(
                'pushToken' => $androidPushTokens,
                'message' => $data->message,
                'os_type' => 1,
                'pageId' => $data->pageId,
                'app_type' => $data->app_type,
                'type' => 2
            );
            $this->sendPush($pushData);
        }
        if(count($iosPushTokens) > 0){
            $pushData = array(
                'pushToken' => $iosPushTokens,
                'message' => $data->message,
                'os_type' => 2,
                'pageId' => $data->pageId,
                'app_type' => $data->app_type,
                'type' => 2
            );
            $this->sendPush($pushData);
        }
    }
    public function sendPush($data){
        //Type 1 = Web Push, 2 = App push
        if($data['type'] == 1){
            $some_data = array(
            'tag' => "pushNotifications",
            'projectcode' => "now_erp",
            'pushtype' => 1,
            'pushToken' => $data['pushToken'],
            'message' => $data['message']
          ); 
        }else if($data['type'] == 2){
            $some_data = array(
            'tag' => "pushNotifications",
            'projectcode' => "now_erp",
            'pushtype' => 2,
            'pushToken' => $data['pushToken'],
            'message' => $data['message'],
            'os_type' => $data['os_type'],
            'app_type' => $data['app_type'],
            'pageId' => $data['page_id'],
            'iconUrl' => '' //Notification Icon Url
          ); 
        }        
        $fields = json_encode($some_data);
        print_r($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://localhost.cmsnew/api.php");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);    
        $response = curl_exec($ch);        
        curl_close($ch);        
        Log::useFiles(storage_path().'logs/pushnotification_service.log');
        Log::info($response);
        return 1;
    }

    public function uploadFiles($data){
        $userId = $data->userId;
        $file = $data->original_filename;
        $tmpfile = $_FILES[$file]['tmp_name'];
        $filename = $data->file_name;
        $record = $data;
        // echo $data->file;
        // exit;
        $some_data = array(
            'parentId' => "",
            'file' => curl_file_create($tmpfile, $_FILES[$file]['type'], $filename),
            'userId' => $userId
          ); 
        $fields = $some_data;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, DRIVE_HOST."secure1/uploads");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: multipart/form-data; charset=utf-8'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);    
        $response = curl_exec($ch);        
        curl_close($ch);        
        Log::useFiles(storage_path().'logs/fileupload_service.log');
        Log::info($response);
        $response_data = json_decode($response);
        print_r($response_data);
        print_r($response_data->url);
        $update_column = $record->update_column_name;
        $values = array(
           $update_column  => $response_data->url
        );
        DB::table($record->table)
        ->where($record->column_name,$record->record->id)
        ->update($values);
        return 1;
    }
    
}
