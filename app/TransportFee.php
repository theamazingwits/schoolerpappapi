<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransportFee extends Model
{
   protected $table="transport_fee";

   public static function userBalance($user_id)
   {
   	  
   	  $balance  =  TransportFee::where('user_id',$user_id)
   	                         ->sum('balance');

   	  return (int)$balance;                       

   }

}
