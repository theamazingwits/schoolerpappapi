<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventoryIssueItem extends Model
{
    protected $table = "inventory_issue_item";

   // protected $fillable = ['role_id','user_id','added_by','issue_date','return_date','notes','category_id','item_id','quantity','description'];
   

    public static function getRecordWithSlug($id)
    {
        return InventoryIssueItem::where('id', '=', $id)->first();
    }
}