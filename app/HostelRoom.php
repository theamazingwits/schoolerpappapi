<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HostelRoom extends Model
{
   protected $table="hostel_rooms";

   protected $fillable = ['branch_id','room_number','hostel_id','room_type_id','beds','cost','description'];
    
   
}
