<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransportFeeType extends Model
{
   protected $table="transport_fee_type";

   protected $fillable = ['id','route_id','type'];

   public static function getMonths()
   {
   	   
   	   // $months = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December');

   	    $months = array('January' => 'January', 'February' => 'February', 'March' => 'March', 'April' => 'April', 'May' => 'May', 'June' => 'June', 'July' => 'July', 'August' => 'August', 'September' => 'September', 'October' => 'October', 'November' => 'November', 'December' => 'December');

   	    return $months;
   }
    
   
}
