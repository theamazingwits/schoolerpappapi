<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
     protected $table= "expenses";

    public static function getRecordWithSlug($slug)
    {
        return Expense::where('slug', '=', $slug)->first();
    }




}
