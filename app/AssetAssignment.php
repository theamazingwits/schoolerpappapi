<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssetAssignment extends Model
{
      protected $table="asset_assignment";

      protected $fillable = ['asset_id','quantity','role_id','user_id','due_date','checkout_date','checkin_date','location_id','status','notes','added_by'];
}
