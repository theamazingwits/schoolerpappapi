<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventoryItemSupplier extends Model
{
    protected $table="inventory_items_supplier";
   

    public static function getRecordWithSlug($slug)
    {
        return InventoryItemSupplier::where('slug', '=', $slug)->first();
    }
}