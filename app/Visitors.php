<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visitors extends Model
{
   protected $table="visitors";

   protected $fillable = ['name', 'email','phone_number','coming_from','role_id','user_id','representing','added_by'];

 
}
