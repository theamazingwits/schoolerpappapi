<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Exception;

class Assignments extends Model
{
    protected $table = 'assignments';

    public function addAllStudents($request)
    {
    	// dd($request);
    	$course_data   = CourseSubject::find($request->course_subject_id);

    	 $students  = Student::join('users','users.id','=','students.user_id')
                                ->where('academic_id',$course_data->academic_id)
                                ->where('course_parent_id',$course_data->course_parent_id) 
                                ->where('course_id',$course_data->course_id) 
                                ->where('current_year',$course_data->year) 
                                ->where('current_semister',$course_data->semister)
                                ->pluck('users.id')
                                ->toArray();

        foreach ($students as $key=>$value) 
        {
           
           $record                    = new AssignmentAllocate();
           $record->assignment_id     = $this->id;
           $record->student_id        = $value;
           $record->academic_id       = $course_data->academic_id;
           $record->course_parent_id  = $course_data->course_parent_id;
           $record->course_id         = $course_data->course_id;
           $record->year              = $course_data->year;
           $record->semister          = $course_data->semister;
           $record->save();

            $user  = User::find($value);
          
           try {
             
               $user->notify(new \App\Notifications\AssignmentAllocate($user));
             
           } catch (Exception $e) {
              
              dd($e->getMessage());
           }

        }                        

    }

    public function addSelectedStudents($request)
    {
    	 

    	 $course_data   = CourseSubject::find($request->course_subject_id);

    	 $students  = $request->users_id;

        foreach ($students as $key=>$value) 
        {
           
           $record                    = new AssignmentAllocate();
           $record->assignment_id     = $this->id;
           $record->student_id        = $value;
           $record->academic_id       = $course_data->academic_id;
           $record->course_parent_id  = $course_data->course_parent_id;
           $record->course_id         = $course_data->course_id;
           $record->year              = $course_data->year;
           $record->semister          = $course_data->semister;
           $record->save();

           $user  = User::find($value);
          
           try {
             
               $user->notify(new \App\Notifications\AssignmentAllocate($user));
             
           } catch (Exception $e) {
              
              // dd($e->getMessage());
           }

        }          
    	
    }


     public function assignedUsers()
    {
        $records   = AssignmentAllocate::where('assignment_id',$this->id)
                                        ->pluck('student_id')
                                        ->toArray();

        return $records;

    }
    
}	
