<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
 

class SubjectBooks extends Model
{
   
    protected $table = 'subject_books';
    
    protected $subjectBookspath  = "public/uploads/subjectbooks/";

    public function subjectbookPath()
    {
        return $this->subjectBookspath;
    }
}