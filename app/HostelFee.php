<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HostelFee extends Model
{
   protected $table="hostel_fee";

   public static function userBalance($user_id)
   {
   	  
   	  $balance  =  HostelFee::where('user_id',$user_id)
   	                         ->sum('balance');

   	  return (int)$balance;                       

   }

}
