<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vechicle extends Model
{
   protected $table="vechicles";

   protected $fillable = ['branch_id','number', 'model','year_made','driver_name','driver_license','driver_contact','seats','description','driver_id'];

 
}
