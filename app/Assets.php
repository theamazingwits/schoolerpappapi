<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assets extends Model
{
    protected $table="assets";

    protected $fillable = ['serial_no','title','status','asset_condition','category_id','location_id','added_by'];
}
