<?php

namespace App;

// use Illuminate\Database\Eloquent\Model;

class LogHelper 
{
    public function storeLogs($data){

        $some_data = array(
            'app_name' => env('APP_NAME') ,
            'object_id' => $data['object_id'],
            'flag' => $data['flag'],
            'json' => json_encode($data),
            'action' => $data['action']
          ); 
    
        //print_r($some_data);
        $fields_string = http_build_query($some_data);
        $curl_handle = curl_init();
    
        
        // curl_setopt($curl_handle, CURLOPT_POST, 1);
        
        // curl_setopt($curl_handle,CURLOPT_URL,'http://192.168.0.113:4004/serviceerpapi/log');
        // curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $fields_string);
        // curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,2);
        // //curl_setopt($ch, CURLOPT_TIMEOUT, 400); //timeout in seconds
        // curl_setopt($curl_handle,CURLOPT_RETURNTRANSFER,1);

        // //create the multiple cURL handle
        // $mh = curl_multi_init();
        // curl_multi_add_handle($mh,$curl_handle);
        // $active = null;

        // //execute the handles
        // do {
        //     $buffer = curl_multi_exec($mh, $active);
        // } while ($buffer == CURLM_CALL_MULTI_PERFORM);

        // while ($active && $buffer == CURLM_OK) {
        //     if (curl_multi_select($mh) != -1) {
        //         do {
        //             $buffer = curl_multi_exec($mh, $active);
        //         } while ($buffer == CURLM_CALL_MULTI_PERFORM);
        //     }
        // }

        // //close the handles
        // curl_multi_remove_handle($mh, $curl_handle);
        // curl_multi_close($mh);
    
        // $buffer = json_decode($buffer);

        // return $buffer;


        curl_setopt($curl_handle, CURLOPT_POST, 1);
			
        curl_setopt($curl_handle,CURLOPT_URL,'http://192.168.0.117:4004/serviceerpapi/log');
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,2);
        curl_setopt($curl_handle, CURLOPT_TIMEOUT, 400); //timeout in seconds
        curl_setopt($curl_handle,CURLOPT_RETURNTRANSFER,1);
        $buffer = curl_exec($curl_handle);
        curl_close($curl_handle);
    
        $buffer = json_decode($buffer);

        return 1;
    }
    
}
