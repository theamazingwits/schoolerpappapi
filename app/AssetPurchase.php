<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssetPurchase extends Model
{
    protected $table="asset_purchase";

    protected $fillable = ['asset_id','vendor_id','added_by','quantity','unit','price','purchase_date','service_date','expire_date'];
}
