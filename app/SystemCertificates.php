<?php

namespace App;

use DB;

use Illuminate\Database\Eloquent\Model;

class SystemCertificates extends Model
{

    protected $table = 'system_certificates';

    public static function getRecordWithSlug($slug)
    {
        return SystemCertificates::where('slug', '=', $slug)->first();
    }

}
