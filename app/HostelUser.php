<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HostelUser extends Model
{
   protected $table="hostel_user_room";

   protected $fillable = ['user_id', 'hostel_id', 'room_id'];


   public function hostelname()
   {
   	  $hostel   = Hostel::find($this->hostel_id);
   	  if($hostel)
   	  	return ucwords($hostel->name);

   	   return '-';
   }

   public function roomname()
   {
   	  $room   = HostelRoom::find($this->room_id);
   	  $room_details   = RoomType::find($room->room_type_id);
   	  $data =array();
   	  if($room && $room_details){
   	  	$data['number']  = $room->room_number;
   	  	$data['cost']  = $room->cost;
   	  	$data['type']  = $room_details->room_type;
   	  	return $data;
   	  }
   	  	

   	   return '-';
   }

 
    
   
}
