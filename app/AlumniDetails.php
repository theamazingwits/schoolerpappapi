<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlumniDetails extends Model
{
      protected $table="alumni_details";

      protected $fillable = ['blood_group','quote','date_of_birth','location'];
}
