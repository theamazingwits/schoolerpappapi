<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class CourseSubject extends Model
{
    protected $table = 'course_subject';

    public static function getCourseSavedSubjects($academicId, $courseId, $year, $semister = 0)
    {
    	return CourseSubject::join('subjects', 'subjects.id','=','course_subject.subject_id')
                                ->where('course_id', '=', $courseId)
            					->where('year', '=', $year)
            					->where('academic_id', '=', $academicId)
            					->where('semister', '=', $semister)
            					->get();
    }

    public static function isStaffAllocatedToCourse($staff_id)
    {
        return CourseSubject::where('staff_id','=',$staff_id)->count();
    }

     public static function getStaffSubjetcs($staff_id, $academic_id)
    {  
        if(is_numeric($academic_id)){

        $records  = CourseSubject::join('subjects', 'subjects.id','=','course_subject.subject_id')
                                  ->where('staff_id',$staff_id)
                                  ->where('academic_id',$academic_id)
                                  ->groupby('course_subject.subject_id')
                                  ->pluck('subject_title','subjects.id')
                                  ->toArray();
                                  
        }else{

        $records  = CourseSubject::join('subjects', 'subjects.id','=','course_subject.subject_id')
                                  ->where('staff_id',$staff_id)
                                   ->groupby('course_subject.subject_id')
                                  ->pluck('subject_title','subjects.id')
                                  ->toArray();
        }
         return $records;
    }

    public static function getStaffClasses($staff_id, $academic_id)
    {
       if(is_numeric($academic_id)){

        $records  = CourseSubject::join('academics', 'academics.id','=','course_subject.academic_id')
                                  ->join('courses', 'courses.id','=','course_subject.course_id')
                                  ->where('course_subject.staff_id',$staff_id)
                                  ->where('course_subject.academic_id',$academic_id)
                                  ->groupby('course_subject.course_id')
                                  ->select(DB::raw("CONCAT(academic_year_title,'-',course_title) AS class"),'course_subject.id')
                                  ->pluck('class', 'id')
                                  ->toArray();
                                  
                                  
        }
        else{

        $records  = CourseSubject::join('academics', 'academics.id','=','course_subject.academic_id')
                                  ->join('courses', 'courses.id','=','course_subject.course_id')
                                  ->where('course_subject.staff_id',$staff_id)
                                  ->groupby('course_subject.course_id')
                                  ->select(DB::raw("CONCAT(academic_year_title,'-',course_title) AS class"),'course_subject.id')
                                  ->pluck('class', 'id')
                                  ->toArray();
        }
         return $records;
    }
}
