<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlumniNotice extends Model
{
      protected $table="alumni_notices";

      protected $fillable = ['title','description','date','image','added_by'];
}
