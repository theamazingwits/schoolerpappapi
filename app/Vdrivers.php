<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vdrivers extends Model
{
   protected $table="vdrivers";

   protected $fillable = ['name','spouse_name','licence_number','phone_number','alternate_mobile_number','number_of_children','address','experience','added_by'];

 
}
