<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssetVendor extends Model
{
   protected $table="asset_vendor";

   protected $fillable = ['name','email','phone','address','supply'];
    
   
}
