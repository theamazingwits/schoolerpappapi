<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Carbon\Carbon;
use DB;
// use Log;
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        '\App\Console\Commands\Email',
        '\App\Console\Commands\InvoiceGenerate',
        '\App\Console\Commands\DocExpire',
        '\App\Console\Commands\EmailNotification'
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        // Log::info('Cron Job Started');
        $schedule->command('EmailSend:Notifi')
         ->everyMinute()
         -> appendOutputTo (storage_path().'/logs/laravel_output.log');
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        // $this->load(__DIR__.'/Commands');
        require base_path('routes/console.php');
    }
}
