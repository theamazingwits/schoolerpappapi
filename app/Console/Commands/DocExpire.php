<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Mail;
use PDF;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class DocExpire extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Document:Verification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automated Email Notification For User Document Expired Alert';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        //Establishment Document
        $exp_date = DB::table('he_users as use')
                ->where('use.user_role','1')
                ->where('use.user_status','1')
                ->whereNotIn('use.user_type',['0'])
                ->join('he_company_info as hci','use.company_info_id','hci.company_info_id')
                ->get(['establishment_doc_validtill','use.email_id']);
                
        foreach($exp_date as $value){

            $find_diff = Carbon::now()->diffInDays($value->establishment_doc_validtill);

            if($find_diff == 30)
            {
                $data = array('days'=>'Your Establishment Document Will be Expired 30 Days left');
                $email = $value->email_id;
                Mail::send('docexp_email_notification', $data, function($message) use($email) {           
                    $message->to($email)->subject('You Have New Notification from CLS');
                    $message->from('mailtomailserviceforcls@gmail.com','Click Logistic Services');
                });
                
            } else if($find_diff == 15) {
                $data = array('days'=>'Your Establishment Document Will be Expired 15 Days left');
                $email = $value->email_id;
                Mail::send('docexp_email_notification', $data, function($message) use($email) {           
                    $message->to($email)->subject('You Have New Notification from CLS');
                    $message->from('mailtomailserviceforcls@gmail.com','Click Logistic Services');
                });
                
            } else if($find_diff == 7) {
                $data = array('days'=>'Your Establishment Document Will be Expired 7 Days left');
                $email = $value->email_id;
                Mail::send('docexp_email_notification', $data, function($message) use($email) {           
                    $message->to($email)->subject('You Have New Notification from CLS');
                    $message->from('mailtomailserviceforcls@gmail.com','Click Logistic Services');
                });
                
            } else if($find_diff == 1) {
                $data = array('days'=>'Your Establishment Document Will be Expired 1 Days left');
                $email = $value->email_id;
                Mail::send('docexp_email_notification', $data, function($message) use($email) {           
                    $message->to($email)->subject('You Have New Notification from CLS');
                    $message->from('mailtomailserviceforcls@gmail.com','Click Logistic Services');
                });
                
            } else if($find_diff == 0) {
                $data = array('days'=>'Your Establishment Document Has been Expired Today');
                $email = $value->email_id;
                Mail::send('docexp_email_notification', $data, function($message) use($email) {           
                    $message->to($email)->subject('You Have New Notification from CLS');
                    $message->from('mailtomailserviceforcls@gmail.com','Click Logistic Services');
                });
                
            }
        }

        //Chamber of Commerce Number Document
        $exp_date = DB::table('he_users as use')
                ->where('use.user_role','1')
                ->where('use.user_status','1')
                ->whereNotIn('use.user_type',['0'])
                ->join('he_company_info as hci','use.company_info_id','hci.company_info_id')
                ->get(['comm_no_vaildtill','use.email_id']);
                
        foreach($exp_date as $value){

            $find_diff = Carbon::now()->diffInDays($value->comm_no_vaildtill);

            if($find_diff == 30)
            {
                $data = array('days'=>'Your Chamber of Commerce Document Will be Expired 30 Days left');
                $email = $value->email_id;
                Mail::send('docexp_email_notification', $data, function($message) use($email) {           
                    $message->to($email)->subject('You Have New Notification from CLS');
                    $message->from('mailtomailserviceforcls@gmail.com','Click Logistic Services');
                });
                
            } else if($find_diff == 15) {
                $data = array('days'=>'Your Chamber of Commerce Will be Expired 15 Days left');
                $email = $value->email_id;
                Mail::send('docexp_email_notification', $data, function($message) use($email) {           
                    $message->to($email)->subject('You Have New Notification from CLS');
                    $message->from('mailtomailserviceforcls@gmail.com','Click Logistic Services');
                });
                
            } else if($find_diff == 7) {
                $data = array('days'=>'Your Chamber of Commerce Will be Expired 7 Days left');
                $email = $value->email_id;
                Mail::send('docexp_email_notification', $data, function($message) use($email) {           
                    $message->to($email)->subject('You Have New Notification from CLS');
                    $message->from('mailtomailserviceforcls@gmail.com','Click Logistic Services');
                });
                
            } else if($find_diff == 1) {
                $data = array('days'=>'Your Chamber of Commerce Will be Expired 1 Days left');
                $email = $value->email_id;
                Mail::send('docexp_email_notification', $data, function($message) use($email) {           
                    $message->to($email)->subject('You Have New Notification from CLS');
                    $message->from('mailtomailserviceforcls@gmail.com','Click Logistic Services');
                });
                
            } else if($find_diff == 0) {
                $data = array('days'=>'Your Chamber of Commerce Has been Expired Today');
                $email = $value->email_id;
                Mail::send('docexp_email_notification', $data, function($message) use($email) {           
                    $message->to($email)->subject('You Have New Notification from CLS');
                    $message->from('mailtomailserviceforcls@gmail.com','Click Logistic Services');
                });
                
            }
        }

        //ISO  Document
        $exp_date = DB::table('he_users as use')
                ->where('use.user_role','1')
                ->where('use.user_status','1')
                ->whereNotIn('use.user_type',['0'])
                ->join('he_company_info as hci','use.company_info_id','hci.company_info_id')
                ->get(['iso_ceritific_vaildtill','use.email_id']);
                
        foreach($exp_date as $value){

            $find_diff = Carbon::now()->diffInDays($value->iso_ceritific_vaildtill);

            if($find_diff == 30)
            {
                $data = array('days'=>'Your ISO Document Will be Expired 30 Days left');
                $email = $value->email_id;
                Mail::send('docexp_email_notification', $data, function($message) use($email) {           
                    $message->to($email)->subject('You Have New Notification from CLS');
                    $message->from('mailtomailserviceforcls@gmail.com','Click Logistic Services');
                });
                
            } else if($find_diff == 15) {
                $data = array('days'=>'Your ISO Document Will be Expired 15 Days left');
                $email = $value->email_id;
                Mail::send('docexp_email_notification', $data, function($message) use($email) {           
                    $message->to($email)->subject('You Have New Notification from CLS');
                    $message->from('mailtomailserviceforcls@gmail.com','Click Logistic Services');
                });
                
            } else if($find_diff == 7) {
                $data = array('days'=>'Your ISO Document Will be Expired 7 Days left');
                $email = $value->email_id;
                Mail::send('docexp_email_notification', $data, function($message) use($email) {           
                    $message->to($email)->subject('You Have New Notification from CLS');
                    $message->from('mailtomailserviceforcls@gmail.com','Click Logistic Services');
                });
                
            } else if($find_diff == 1) {
                $data = array('days'=>'Your ISO Document Will be Expired 1 Days left');
                $email = $value->email_id;
                Mail::send('docexp_email_notification', $data, function($message) use($email) {           
                    $message->to($email)->subject('You Have New Notification from CLS');
                    $message->from('mailtomailserviceforcls@gmail.com','Click Logistic Services');
                });
                
            } else if($find_diff == 0) {
                $data = array('days'=>'Your ISO Document Has been Expired Today');
                $email = $value->email_id;
                Mail::send('docexp_email_notification', $data, function($message) use($email) {           
                    $message->to($email)->subject('You Have New Notification from CLS');
                    $message->from('mailtomailserviceforcls@gmail.com','Click Logistic Services');
                });
                
            }
        }

        //JSRS  Document
        $exp_date = DB::table('he_users as use')
                ->where('use.user_role','1')
                ->where('use.user_status','1')
                ->whereNotIn('use.user_type',['0'])
                ->join('he_company_info as hci','use.company_info_id','hci.company_info_id')
                ->get(['jsrs_ceritific_vaildtill','use.email_id']);
                
        foreach($exp_date as $value){

            $find_diff = Carbon::now()->diffInDays($value->jsrs_ceritific_vaildtill);

            if($find_diff == 30)
            {
                $data = array('days'=>'Your JSRS Document Will be Expired 30 Days left');
                $email = $value->email_id;
                Mail::send('docexp_email_notification', $data, function($message) use($email) {           
                    $message->to($email)->subject('You Have New Notification from CLS');
                    $message->from('mailtomailserviceforcls@gmail.com','Click Logistic Services');
                });
                
            } else if($find_diff == 15) {
                $data = array('days'=>'Your JSRS Document Will be Expired 15 Days left');
                $email = $value->email_id;
                Mail::send('docexp_email_notification', $data, function($message) use($email) {           
                    $message->to($email)->subject('You Have New Notification from CLS');
                    $message->from('mailtomailserviceforcls@gmail.com','Click Logistic Services');
                });
                
            } else if($find_diff == 7) {
                $data = array('days'=>'Your JSRS Document Will be Expired 7 Days left');
                $email = $value->email_id;
                Mail::send('docexp_email_notification', $data, function($message) use($email) {           
                    $message->to($email)->subject('You Have New Notification from CLS');
                    $message->from('mailtomailserviceforcls@gmail.com','Click Logistic Services');
                });
                
            } else if($find_diff == 1) {
                $data = array('days'=>'Your JSRS Document Will be Expired 1 Days left');
                $email = $value->email_id;
                Mail::send('docexp_email_notification', $data, function($message) use($email) {           
                    $message->to($email)->subject('You Have New Notification from CLS');
                    $message->from('mailtomailserviceforcls@gmail.com','Click Logistic Services');
                });
                
            } else if($find_diff == 0) {
                $data = array('days'=>'Your JSRS Document Has been Expired Today');
                $email = $value->email_id;
                Mail::send('docexp_email_notification', $data, function($message) use($email) {           
                    $message->to($email)->subject('You Have New Notification from CLS');
                    $message->from('mailtomailserviceforcls@gmail.com','Click Logistic Services');
                });
                
            }
        }

        //OPAL  Document
        $exp_date = DB::table('he_users as use')
                ->where('use.user_role','1')
                ->where('use.user_status','1')
                ->whereNotIn('use.user_type',['0'])
                ->join('he_company_info as hci','use.company_info_id','hci.company_info_id')
                ->get(['opal_certific_vaildtill','use.email_id']);
                
        foreach($exp_date as $value){

            $find_diff = Carbon::now()->diffInDays($value->opal_certific_vaildtill);

            if($find_diff == 30)
            {
                $data = array('days'=>'Your OPAL Document Will be Expired 30 Days left');
                $email = $value->email_id;
                Mail::send('docexp_email_notification', $data, function($message) use($email) {           
                    $message->to($email)->subject('You Have New Notification from CLS');
                    $message->from('mailtomailserviceforcls@gmail.com','Click Logistic Services');
                });
                
            } else if($find_diff == 15) {
                $data = array('days'=>'Your OPAL Document Will be Expired 15 Days left');
                $email = $value->email_id;
                Mail::send('docexp_email_notification', $data, function($message) use($email) {           
                    $message->to($email)->subject('You Have New Notification from CLS');
                    $message->from('mailtomailserviceforcls@gmail.com','Click Logistic Services');
                });
                
            } else if($find_diff == 7) {
                $data = array('days'=>'Your OPAL Document Will be Expired 7 Days left');
                $email = $value->email_id;
                Mail::send('docexp_email_notification', $data, function($message) use($email) {           
                    $message->to($email)->subject('You Have New Notification from CLS');
                    $message->from('mailtomailserviceforcls@gmail.com','Click Logistic Services');
                });
                 
            } else if($find_diff == 1) {
                $data = array('days'=>'Your OPAL Document Will be Expired 1 Days left');
                $email = $value->email_id;
                Mail::send('docexp_email_notification', $data, function($message) use($email) {           
                    $message->to($email)->subject('You Have New Notification from CLS');
                    $message->from('mailtomailserviceforcls@gmail.com','Click Logistic Services');
                });
                
            } else if($find_diff == 0) {
                $data = array('days'=>'Your OPAL Document Has been Expired Today');
                $email = $value->email_id;
                Mail::send('docexp_email_notification', $data, function($message) use($email) {           
                    $message->to($email)->subject('You Have New Notification from CLS');
                    $message->from('mailtomailserviceforcls@gmail.com','Click Logistic Services');
                });
                
            }
        }

    }
}
