<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Mail;
// use Log;
class Email extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'EmailSend:Notifi';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automated Email Notification For Reports';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Reeport Mail Send
        // Log::info('Cron Job Started');
        $result = DB::table('email_queue')
                ->where('is_sent',0)
                ->get();
                 echo "**********".date('Y-m-d H:i:s');
        if(count($result)){

            foreach ($result as $data) {

            echo "**********".date('Y-m-d H:i:s');
            //print_r(json_decode($data->email_template_data));
            echo "########---".$data->email_template;
                // $content =  json_decode($data->email_template_data)
                print_r(json_decode($data->email_template_data));
                // Mail::send($data->email_template,json_decode($data->email_template_data, true), function($message) use($data) {

                //     $message->to($data->email_to)->subject($data->email_subject);
              
                //     $message->from('mailservicesnowschoolerp@gmail.com','Now');
                // });
                if($data->email_cc == "" && $data->email_bcc == ""){
                    Mail::send($data->email_template,json_decode($data->email_template_data,true), function($message) use($data) {

                        $message->to(explode (",", $data->email_to))->subject($data->email_subject);
                  
                        $message->from('mailservicesnowschoolerp@gmail.com','Now');
                    });

                }else if($data->email_cc != "" && $data->email_bcc != ""){
                    Mail::send($data->email_template,json_decode($data->email_template_data,true), function($message) use($data) {

                        $message->to(explode (",", $data->email_to))->cc(explode (",", $data->email_cc))->bcc(explode (",", $data->email_bcc))->subject($data->email_subject);
                  
                        $message->from('mailservicesnowschoolerp@gmail.com','Now');
                    });

                }else if($data->email_cc != ""){
                    Mail::send($data->email_template,json_decode($data->email_template_data,true), function($message) use($data) {

                        $message->to(explode (",", $data->email_to))->cc(explode (",", $data->email_cc))->subject($data->email_subject);
                  
                        $message->from('mailservicesnowschoolerp@gmail.com','Now');
                    });

                }else if($data->email_bcc != ""){
                    Mail::send($data->email_template,json_decode($data->email_template_data,true), function($message) use($data) {

                        $message->to(explode (",", $data->email_to))->bcc(explode (",", $data->email_bcc))->subject($data->email_subject);
                  
                        $message->from('mailservicesnowschoolerp@gmail.com','Now');
                    });

                }
                

            echo "aaaa";
                 DB::table('email_queue')
                    ->where('id', $data->id)
                    ->update([
                        'is_sent' => 1, 
                        'updated_at' => date('Y-m-d H:i:s')
                        ]);

            }
        }



    }
}
