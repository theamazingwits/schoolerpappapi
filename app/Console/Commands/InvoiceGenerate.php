<?php

namespace App\Console\Commands;

use DB;
use Mail;
use PDF;
use Illuminate\Support\Facades\Storage;
use Illuminate\Console\Command;
use Carbon\Carbon;

class InvoiceGenerate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:InvoiceGenerate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automatically Generate Invoice For The % Based Users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function handle()
    {
        //Invoice Generate For Admin 
        $com_name = DB::table('he_admin_payments')
                ->where('created_at', '<=', Carbon::now()->subMonth())
                ->groupBy('company_name')
                ->get(['company_name']);

        $new = [];
        if(count($com_name)){

          foreach ($com_name as $comname) {

          $bids_details = DB::table('he_admin_payments')
                    ->where('company_name',$comname->company_name)
                    ->where('created_at', '<=', Carbon::now()->subMonth())
                    ->get();

          $tot_amount = DB::table('he_admin_payments')
                    ->where('company_name',$comname->company_name)
                    ->where('created_at', '<=', Carbon::now()->subMonth())
                    ->sum('amount');

          $percen = DB::table('he_admin_payments')
                    ->where('company_name',$comname->company_name)
                    ->first(['percentage','type','contact']);

          $invoice = substr(str_shuffle("123456789"), 0, 4);

              view()->share('doc_name','Invocie:');
              view()->share('shipper_name',$comname->company_name);   
              view()->share('total_amount',$tot_amount);
              view()->share('invoice_no',$invoice);
              view()->share('sno','1');
              view()->share('date',Carbon::now());
              view()->share('alldatas',$bids_details);

              $customPaper = array(0,0,767.00,883.80);

              $pdf = PDF::loadView('admininvoice')
              ->setPaper($customPaper, 'landscape');
              $pdf->save(public_path($invoice.'.pdf'));
               
              $file_name = $invoice.'.pdf';
              $name = $file_name;
              $filePath = 'admininvoice/' . $name;
              Storage::disk('s3')->put($filePath, file_get_contents(public_path($name)));

          }
          
          $adminInvoicevalue = array(

              'company_name'=> $comname->company_name,
              'type' => $percen->type,
              'amount'=>$tot_amount,
              'percentage' => $percen->percentage,
              'contact' => $percen->contact,
              'payment_status' => 2,
              'invoice_number' => $invoice,
              'invoice_docs' => $file_name,
              'created_at' => date('Y-m-d H:i:s') , 
              'updated_at' => date('Y-m-d H:i:s')
              
          );

          $adminIn = array(
              'invoice_number' => $invoice,
              'updated_at' => date('Y-m-d H:i:s')
              
          );

          DB::table('he_admin_payments')
          ->where('created_at', '>=', Carbon::now()->subMonth())
          ->where('company_name',$comname->company_name)
          ->update($adminIn);

          DB::table('he_percentage_invoice')    
          ->insert($adminInvoicevalue);

          $this->info('Invoice Generated Successfully');
      }
    }

}

