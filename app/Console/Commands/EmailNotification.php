<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Mail;
use PDF;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;


class EmailNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
     protected $signature = 'email:notification';

    /**
     * The console command description.
     *
     * @var string
     */
protected $description = 'Automated Email Notification For Trail And Subscribed Users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $exp_date = DB::table('he_users')
                ->where('user_role','1')
                ->where('user_status','1')
                ->whereNotIn('user_type',['0'])
                ->whereIn('payment_status',['1'])
                ->get(['package_exp_date','email_id']);
                
    foreach($exp_date as $value){

        $find_diff = Carbon::now()->diffInDays($value->package_exp_date);

        if($find_diff == 30)
        {
            $data = array('days'=>'Your Plan Will be Expired 30 Days left');
            $email = $value->email_id;
            Mail::send('email_notification', $data, function($message) use($email) {           
                $message->to($email)->subject('You Have New Notification from CLS');
                $message->from('mailtomailserviceforcls@gmail.com','Click Logistic Services');
            });
            
        } else if($find_diff == 15) {
            $data = array('days'=>'Your Plan Will be Expired 15 Days left');
            $email = $value->email_id;
            Mail::send('email_notification', $data, function($message) use($email) {           
                $message->to($email)->subject('You Have New Notification from CLS');
                $message->from('mailtomailserviceforcls@gmail.com','Click Logistic Services');
            });
            
        } else if($find_diff == 7) {
            $data = array('days'=>'Your Plan Will be Expired 7 Days left');
            $email = $value->email_id;
            Mail::send('email_notification', $data, function($message) use($email) {           
                $message->to($email)->subject('You Have New Notification from CLS');
                $message->from('mailtomailserviceforcls@gmail.com','Click Logistic Services');
            });
            
        } else if($find_diff == 1) {
            $data = array('days'=>'Your Plan Will be Expired 1 Days left');
            $email = $value->email_id;
            Mail::send('email_notification', $data, function($message) use($email) {           
                $message->to($email)->subject('You Have New Notification from CLS');
                $message->from('mailtomailserviceforcls@gmail.com','Click Logistic Services');
            });
            
        } else if($find_diff == 0) {
            $data = array('days'=>'Your Plan Has been Expired Today');
            $email = $value->email_id;
            Mail::send('email_notification', $data, function($message) use($email) {           
                $message->to($email)->subject('You Have New Notification from CLS');
                $message->from('mailtomailserviceforcls@gmail.com','Click Logistic Services');
            });
            
        }
    }

        $trail_exp_date = DB::table('he_users')
            ->where('user_role','1')
            ->where('user_status','1')
            ->whereNotIn('user_type',['0'])
            ->whereIn('payment_status',['3'])
            ->get(['package_exp_date','email_id']);
                
        foreach($trail_exp_date as $value){

            $find_diff = Carbon::now()->diffInDays($value->package_exp_date);

            if($find_diff == 30)
            {
                $data = array('days'=>'Your Trial Period Will be Expired 30 Days left');
                $email = $value->email_id;
                Mail::send('email_notification', $data, function($message) use($email) {           
                $message->to($email)->subject('You Have New Notification from CLS');
                $message->from('mailtomailserviceforcls@gmail.com','Click Logistic Services');
                });

            } else if($find_diff == 15) {
                $data = array('days'=>'Your Trial Period Will be Expired 15 Days left');
                $email = $value->email_id;
                Mail::send('email_notification', $data, function($message) use($email) {           
                $message->to($email)->subject('You Have New Notification from CLS');
                $message->from('mailtomailserviceforcls@gmail.com','Click Logistic Services');
                });

            } else if($find_diff == 7) {
                $data = array('days'=>'Your Trial Period Will be Expired 7 Days left');
                $email = $value->email_id;
                Mail::send('email_notification', $data, function($message) use($email) {           
                $message->to($email)->subject('You Have New Notification from CLS');
                $message->from('mailtomailserviceforcls@gmail.com','Click Logistic Services');
                });

            } else if($find_diff == 1) {
                $data = array('days'=>'Your Trial Period Will be Expired 1 Days left');
                $email = $value->email_id;
                Mail::send('email_notification', $data, function($message) use($email) {           
                $message->to($email)->subject('You Have New Notification from CLS');
                $message->from('mailtomailserviceforcls@gmail.com','Click Logistic Services');
                });

            } else if($find_diff == 0) {
                $data = array('days'=>'Your Trial Period Has been Expired Today');
                $email = $value->email_id;
                Mail::send('email_notification', $data, function($message) use($email) {           
                $message->to($email)->subject('You Have New Notification from CLS');
                $message->from('mailtomailserviceforcls@gmail.com','Click Logistic Services');
                });

            }
        }

    }
}
