<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Auth;
use App\User;
use App\SalaryTemplate;
use App\Expense;
use Exception;
use App\LogHelper;

class SalaryPayments extends Model
{
    
    protected $table = "salay_payments";
    
    
    public static function addSalaryRecord($request)
    {
       
         DB::beginTransaction();
        try{

      $record  			               = new SalaryPayments();
    	$record->user_id   		       = $request->user_id;
    	// $record->branch_id   	= $request->branch_id;
    	$record->role_id   		       = $request->role_id;
    	$record->employee_id   	     = $request->employee_id;
    	$record->template_id   	     = $request->template_id;
    	$record->net_salary   	     = $request->net_salary;
    	$record->paid_amount   	     = $request->paid_amount;
    	$record->payment_method	     = $request->payment_method;
    	$record->comments   	       = $request->comments;
    	$record->updated_by   	     = Auth::user()->id;
    	$record->month   		         = $request->month;
    	$record->expense_category_id = $request->expense_category_id;
      $record->save();
      
      

    	$expense_id           = SalaryPayments::addToExpense($request);
    	$record->expense_id   = $expense_id;
      $record->save();

      $record->flag        = 'Insert';
      $record->action      = 'Salay_payments';
      $record->object_id   =  $record->id;
      $logs = new LogHelper();
      $logs->storeLogs($record);
      
         DB::commit();

      }
      catch(Exception $e){
          
          DB::rollBack();
      }


    	

    }

    public static function addToExpense($request)
    {   
    	  $user   = getUserRecord($request->user_id);
    	  $name   = ucwords($user->name).' - '.getRole($user->id).' - '.$request->month.' Salary';

    	  $expense_record                      = new Expense();
		    $expense_record->title 	             = $name;
       	$expense_record->slug 				       = $expense_record->makeSlug($name);
        // $expense_record->branch_id           = $request->branch_id;
        $expense_record->expense_category_id = $request->expense_category_id;
        $expense_record->expense_amount      = $request->paid_amount;
        $expense_record->expense_date        = $request->month;
        $expense_record->save();

        $expense_record->flag        = 'Insert';
        $expense_record->action      = 'Expenses';
        $expense_record->object_id   =  $expense_record->id;
        $logs = new LogHelper();
        $logs->storeLogs($expense_record);

        return $expense_record->id;
    }


    public static function MonthPaid($user_id)
    {
         $first_day_this_month = date('Y-m-01'); 
         $last_day_this_month  = date('Y-m-t');
         $today                = date('Y-m-d');

        $records  = SalaryPayments::where('user_id',$user_id)
                                   ->where('month','>=',$first_day_this_month)
                                   ->where('month','<=',$last_day_this_month)
                                   ->get();

        return $records;                           
    }

}
