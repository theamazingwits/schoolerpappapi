<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpenseCategory extends Model
{
     protected $table= "expense_categories";

    public static function getRecordWithSlug($slug)
    {
        return ExpenseCategory::where('slug', '=', $slug)->first();
    }




}
