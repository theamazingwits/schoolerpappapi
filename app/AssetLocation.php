<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssetLocation extends Model
{
   protected $table="asset_location";

   protected $fillable = ['location','added_by','description'];
    
   
}
