<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventoryItemStock extends Model
{
    protected $table="inventory_item_stock";
   

    public static function getRecordWithSlug($slug)
    {
        return InventoryItemStock::where('id', '=', $slug)->first();
    }
}