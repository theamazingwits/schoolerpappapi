<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//Clear all details
// echo "DB".env('DB_DATABASE');
// exit;

Route::get('/oauth/gmail', function (){
    return LaravelGmail::redirect();
});

Route::get('/oauth/gmail/callback', function (){
    LaravelGmail::makeToken();
    return redirect()->to('/');
});

Route::get('/oauth/gmail/logout', function (){
    LaravelGmail::logout(); //It returns exception if fails
    return redirect()->to('/');
});
Route::get('/', function () {
 
    if(Auth::check())
    {
        return redirect('dashboard');
    }
    
    return redirect(URL_USERS_LOGIN);
});


// if(env('DB_DATABASE')=='')
// {

//    Route::get('/', 'InstallatationController@index');

//    Route::get('/install', 'InstallatationController@index');
//    Route::post('/update-details', 'InstallatationController@updateDetails');
//    Route::post('/install', 'InstallatationController@installProject');
// }


if(env('DEMO_MODE')) {
    Event::listen('eloquent.saving: *', function ($model) {
      return false;
    });
    Event::listen('eloquent.deleting: *', function ($model) {
      return false;
    });
     
}
Route::get('gmail', 'PushnotificationsController@getGmail');
Route::post('sendTrackPushNotification', 'PushnotificationsController@sendTrackPushNotification');
Route::post('sendTripPushNotification', 'PushnotificationsController@sendTripPushNotification');
Route::get('emailNotification', 'EmailNotificationsController@index');
Route::get('emailNotification/promotions', 'EmailNotificationsController@getPromotions');
Route::get('emailNotification/eventnotification', 'EmailNotificationsController@getEventNotifications');

Route::post('emailNotification/promotions/emaillist', 'EmailNotificationsController@getPromotionsemails');
Route::post('emailNotification/promotions/add', 'EmailNotificationsController@addemaillist');
Route::post('emailNotification/promotions/import', 'EmailNotificationsController@importList');
Route::post('emailNotification/promotions/sendMail', 'EmailNotificationsController@sendPromotionmail');
Route::post('emailNotification/eventnotification/sendMail','EmailNotificationsController@sendEventmail');
Route::post('emailNotification/communication/sendMail','EmailNotificationsController@sendCommunicationmail');
Route::get('emailNotification/dueremainder/sendMail/{name}/{rollno}/{feetitle}/{startdate}/{enddate}/{amount}/{userid}/{parentid}','EmailNotificationsController@sendDueremaindermail');

Route::get('emailNotification/communicationmail', 'EmailNotificationsController@getCommunicationMail');
Route::get('emailNotification/dueremainder', 'EmailNotificationsController@getDueRemainder');
Route::get('emailNotification/dueremainder/list', 'EmailNotificationsController@getDueRemainderList');

Route::get('chatone','ChatController@getChat');
Route::get('tracking','TrackingController@getTrack');

Route::get('qrcode','ChatController@getQRCODE');
Route::post('getStudentParentList', 'ChatController@getStudentsandParents');
Route::post('getStaffs', 'ChatController@getStaffs');
Route::get('plandetails', 'DashboardController@getAdminprofile');
Route::get('adminprofiledetails', 'DashboardController@getAdminprofileDetails');





 // Route::get('install/reg', 'InstallatationController@reg');
Route::post('install/register', 'InstallatationController@registerUser');
Route::get('dashboard','DashboardController@index');
Route::get('dashboard/testlang','DashboardController@testLanguage');
Route::get('auth/{slug}','Auth\AuthController@redirectToProvider');
Route::get('auth/{slug}/callback','Auth\AuthController@handleProviderCallback');

Route::get('registration', 'Auth\LoginController@getRegistration');
// Authentication Routes...
Route::get('login', 'Auth\LoginController@getLogin');

Route::post('login', 'Auth\LoginController@postLogin');
Route::get('mail', 'GmailController@getChat');

Route::get('logout', function(){
   
   $user  = Auth::user();

     $values = array(
                    'push_web_id' => null
                );
    $record = DB::table('users')
    ->where('id', $user->id)
    ->update($values);

    if(Auth::check())
        flash(getPhrase('success'),getPhrase('logged_out_successfully'),'success');

    Auth::logout();
    if($user->role_id == 14)
    return redirect(URL_ALUMINI_LOGIN);

    return redirect(URL_USERS_LOGIN);
});

Route::get('parent-logout', function(){
    if(Auth::check())
        flash('Oops..!',getPhrase('parents_module_is_not_available'),'error');
    Auth::logout();
    return redirect(URL_USERS_LOGIN);
});


Route::post('users/forgot-password', 'Auth\LoginController@resetUsersPassword');
Route::get('reset-password/{token}', 'Auth\LoginController@resetpassword');
Route::post('reset-my-password', 'Auth\LoginController@resetmypassword');



Route::get('password/reset/{slug?}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');


Route::get('languages/list', 'NativeController@index');
Route::get('languages/getList', [ 'as'   => 'languages.dataTable',
     'uses' => 'NativeController@getDatatable']);
 
Route::get('languages/add', 'NativeController@create');
Route::post('languages/add', 'NativeController@store');
Route::get('languages/edit/{slug}', 'NativeController@edit');
Route::patch('languages/edit/{slug}', 'NativeController@update');
Route::delete('languages/delete/{slug}', 'NativeController@delete');
 
Route::get('languages/make-default/{slug}', 'NativeController@changeDefaultLanguage');
Route::get('languages/update-strings/{slug}', 'NativeController@updateLanguageStrings');
Route::patch('languages/update-strings/{slug}', 'NativeController@saveLanguageStrings');


//Departments
Route::get('departments', 'DepartmentsController@index');
Route::get('departments/add-department', 'DepartmentsController@createDepartment');
Route::post('departments/add-department', 'DepartmentsController@storeDepartment');
Route::get('departments/edit-department/{slug}', 'DepartmentsController@editDepartment');
Route::patch('departments/edit-department/{slug}', 'DepartmentsController@updateDepartment');
Route::delete('departments/delete/{id}', 'DepartmentsController@delete');
Route::get('departments/getDepartments', [ 'as'   => 'departments.dataTable',
    'uses' => 'DepartmentsController@getDatatable']);


//Users
//Users ---dashboard
Route::get('users/dashboard', 'UsersDashboardController@index');
Route::get('users/import','UsersController@importUsers');
Route::post('users/import','UsersController@readExcel');
Route::get('users/create', 'UsersController@create');
Route::get('users/staff-inactive/{role}', 'UsersController@staff_inactivelist');
Route::delete('users/delete/{slug}', 'UsersController@delete');
Route::post('users/create/{role?}', 'UsersController@store');
Route::get('users/edit/{slug}', 'UsersController@edit');
Route::patch('users/edit/{slug}', 'UsersController@update');
Route::get('users/profile/{slug}', 'UsersController@show');
Route::get('users/{users?}', 'UsersController@index');
Route::get('users/profile/{slug}', 'UsersController@show');
Route::get('users/details/{slug}', 'UsersController@details');
Route::post('users/change-status', 'UsersController@changeStatus');
Route::get('users/details/promotions/{slug}', 'UsersController@transfers');
Route::get('users/details/library/{slug}', 'UsersController@student_librarydetails');
Route::get('users/details/health/{slug}', 'UsersController@student_healthdetails');
Route::get('users/details/library/get-list/{id}', 'UsersController@getlibraryhistory');
Route::get('users/details/health/get-list/{id}', 'UsersController@gethealthdetails');
Route::get('staff/details/{slug}', 'UsersController@staffDetails');
Route::get('users/settings/{slug}', 'UsersController@settings');
Route::patch('users/settings/{slug}', 'UsersController@updateSettings');
Route::get('users/change-password/{slug}', 'UsersController@changePassword');
Route::patch('users/change-password/{slug}', 'UsersController@updatePassword');
Route::get('users/import','UsersController@importUsers');
Route::post('users/import','UsersController@readExcel');
Route::get('users/import-report','UsersController@importResult');
Route::get('users/{role?}', 'UsersController@index');
Route::post('users/import/get-excel-information', 'UsersController@getExcelUploadInformation');
Route::get('users/list/getList/{role_name?}', [ 'as'   => 'users.dataTable',
    'uses' => 'UsersController@getDatatable']);
Route::get('users/staff-inactive/getList/{slug}', [ 'as'   => 'users-staff_inactive.dataTable',
    'uses' => 'UsersController@getStaffInactiveList']);

Route::get('users/fee-schedules/{student_id}','UsersController@feeShedulesView');
Route::get('users/fee-paid-history/{student_id}','UsersController@feePaidHistoryView');

// Route::get('users/list/getList/{role_name?}', 'UsersController@getDatatable');


//Staff
Route::patch('staff/profile/edit/general/{id}','StaffController@updateGeneralDetails');
Route::patch('staff/profile/edit/personal/{id}','StaffController@updatePersonalDetails');
Route::patch('staff/profile/edit/contact/{id}','StaffController@updateContactDetails');
Route::get('staff/profile/edit/{slug}/{tab?}', 'StaffController@edit');
Route::post('staff/profile/edit/status', 'StaffController@editstatus');
Route::get('staff/profile/{slug}', 'StaffController@show');

//Staff subject preference
Route::get('staff/subjects/preferences/{slug}','SubjectPreferencesController@subjectPreferences');
Route::post('staff/subjects/preferences/{slug}','SubjectPreferencesController@update');

            //////////////////////
            //Parent Controller //
            //////////////////////
Route::get('parent/children', 'ParentsController@index');
Route::get('parent/children/list', 'ParentsController@index');
Route::get('parent/children/getList/{slug}', 'ParentsController@getDatatable');
Route::get('children/analysis', 'ParentsController@childrenAnalysis');

Route::get('parent/children/getAllergyFoodList', 'ParentsController@getAllergyDataTable');
Route::get('children/healthrecord', 'ParentsController@allergyfoodindex');
Route::get('children/healthrecord/add', 'ParentsController@allergyfoodcreate');
Route::post('children/healthrecord/add', 'ParentsController@allergyfoodstore');
Route::get('children/healthrecord/edit/{slug}', 'ParentsController@edit');
Route::post('children/healthrecord/update/{slug}', 'ParentsController@update');
Route::post('children/healthrecord/delete/{slug}', 'ParentsController@delete');

Route::get('children/medicaltreatment', 'ParentsController@medicaltreatmentindex');
Route::get('children/medicaltreatment/add', 'ParentsController@medicaltreatmentcreate');
Route::get('children/medicaltreatment/edit/{slug}', 'ParentsController@medicaltreatmentedit');
Route::post('children/medicaltreatment/update/{slug}', 'ParentsController@medicaltreatmentupdate');
Route::post('children/medicaltreatment/add', 'ParentsController@medicaltreatmentstore');
Route::get('parent/children/getMedicalTreatmentList', 'ParentsController@getMedicaltreatmentDataTable');

Route::get('parent/children/getLeaveList', 'ParentsController@getLeaveDataTable');
Route::get('children/leave', 'ParentsController@leaveindex');
Route::get('children/leave/add', 'ParentsController@leavecreate');
Route::post('children/leave/add', 'ParentsController@leavestore');

Route::get('parent/children/getBasicstudhealthList', 'ParentsController@getBasicstudhealthDataTable');
Route::get('children/basicstudenthealth', 'ParentsController@basicStudhealthindex');
Route::get('children/basicstudhealth/add', 'ParentsController@basicStudhealthcreate');
Route::post('children/basicstudhealth/add', 'ParentsController@basicStudhealthstore');
Route::get('children/basicstudhealth/edit/{slug}', 'ParentsController@basicStudhealthedit');
Route::post('children/basicstudhealth/update/{slug}', 'ParentsController@basicStudhealthupdate');

//children hostal leave
Route::get('parent/children/gethostalLeaveList', 'ParentsController@gethostalLeaveDataTable');
Route::get('children/hostalleave', 'ParentsController@hostalleaveindex');
//children hostal attendance
Route::get('parent/children/gethostalAttendanceList', 'ParentsController@gethostalAttendanceDataTable');
Route::get('children/hostalattendance', 'ParentsController@hostalattendanceindex');

   
                    /////////////////////
                    // Master Settings //
                    /////////////////////
 
//Religions
Route::get('mastersettings/religions', 'ReligionsController@index');
Route::get('mastersettings/religions/add', 'ReligionsController@create');
Route::post('mastersettings/religions/add', 'ReligionsController@store');
Route::get('mastersettings/religions/edit/{slug}', 'ReligionsController@edit');
Route::patch('mastersettings/religions/edit/{slug}', 'ReligionsController@update');
Route::delete('mastersettings/religions/delete/{id}', 'ReligionsController@delete');
Route::get('mastersettings/religions/getList', [ 'as'   => 'religions.dataTable',
    'uses' => 'ReligionsController@getDatatable']);


//Categories
Route::get('mastersettings/categories', 'CategoriesController@index');
Route::get('mastersettings/categories/add', 'CategoriesController@create');
Route::post('mastersettings/categories/add', 'CategoriesController@store');
Route::get('mastersettings/categories/edit/{slug}', 'CategoriesController@edit');
Route::patch('mastersettings/categories/edit/{slug}', 'CategoriesController@update');
Route::delete('mastersettings/categories/delete/{id}', 'CategoriesController@delete');
Route::get('mastersettings/categories/getList', [ 'as'   => 'categories.dataTable',
    'uses' => 'CategoriesController@getDatatable']);


//Academics
Route::get('mastersettings/academics', 'AcademicsController@index');
Route::get('mastersettings/academics/add', 'AcademicsController@create');
Route::post('mastersettings/academics/add', 'AcademicsController@store');
Route::get('mastersettings/academics/edit/{slug}', 'AcademicsController@edit');
Route::patch('mastersettings/academics/edit/{slug}', 'AcademicsController@update');
Route::delete('mastersettings/academics/delete/{id}', 'AcademicsController@delete');
Route::get('mastersettings/academics/getList', [ 'as'   => 'academics.dataTable',
    'uses' => 'AcademicsController@getDatatable']);
Route::get('mastersettings/academics/get-academics', 'AcademicsController@getAcademics');


//Academic Courses
Route::post('mastersettings/academic-courses/check-status', 'AcademicCoursesController@checkStatus');

Route::get('mastersettings/academic-courses/{slug}', 'AcademicCoursesController@academicCoursesArrangment');
Route::post('mastersettings/academic-courses/{slug}', 'AcademicCoursesController@updateAcademicCourses');


Route::post('academic-courses/get-parent-courses', 'AcademicCoursesController@getParentCourses');
Route::post('academic-courses/get-child-courses', 'AcademicCoursesController@getChildCourses');



//courses

//Courese ---dashboard
Route::get('courses/dashboard', 'CourseController@coursesdashboard');
Route::get('mastersettings/course', 'CourseController@index');
Route::get('mastersettings/course/add', 'CourseController@create');
Route::post('mastersettings/course/add', 'CourseController@store');
Route::get('mastersettings/course/edit/{slug}', 'CourseController@edit');
Route::patch('mastersettings/course/edit/{slug}', 'CourseController@update');
Route::delete('mastersettings/course/delete/{id}', 'CourseController@delete');
Route::get('mastersettings/course/getList', [ 'as'   => 'course.dataTable',
    'uses' => 'CourseController@getDatatable']);

Route::get('mastersettings/course/editSemisters/{slug}', 'CourseController@editSemisters');
Route::patch('mastersettings/course/editSemisters', 'CourseController@updateSemisters');


//Course Subjects 
Route::get('mastersettings/course-subjects/add', 'CourseSubjectsController@create');

Route::post('mastersettings/course-subjects/add', 'CourseSubjectsController@store');
Route::patch('mastersettings/course-subjectsd/edit/{slug}', 'CourseSubjectsController@update');
Route::delete('mastersettings/course-subjects/delete', 'CourseSubjectsController@delete');
Route::get('mastersettings/course-subjects/getList/{slug}', 'CourseSubjectsController@getDatatable');
Route::get('mastersettings/course-subjects/{slug?}', 'CourseSubjectsController@index');

Route::post('mastersettings/course-subjects/getCourseYears', 'CourseSubjectsController@getCourseYears');
Route::post('mastersettings/course-subjects/getSemisters', 'CourseSubjectsController@getSemisters');
Route::post('mastersettings/course-subjects/getSavedSubjects', 'CourseSubjectsController@getSavedSubjects');
Route::get('mastersettings/course-subjects/show/{academic_id}/{course_id}', 'CourseSubjectsController@show');
Route::post('academic-courses/remove-subject', 'CourseSubjectsController@removeSubject');

Route::get('subjects/view-topics/{subject_slug}', 'SubjectsController@viewTopics');

Route::post('mastersettings/course-subjects/load', 'CourseSubjectsController@loadYearDetails');
Route::get('mastersettings/course-subjects/load', 'CourseSubjectsController@create');

Route::get('course-subjects/add-staff/{academic_id}/{course_id}', 'CourseSubjectsController@staffAllotment');
Route::post('course-subjects/update-staff', 'CourseSubjectsController@updateStaffAllotment');
Route::post('course-subjects/is-staff-allocated', 'CourseSubjectsController@isSatffAllocatedToTimetable');

//Certificate Templates
Route::get('mastersettings/certificate_templates', 'CertificateTemplatesController@index');
Route::get('mastersettings/certificate_templates/add', 'CertificateTemplatesController@create');
Route::post('mastersettings/certificate_templates/add', 'CertificateTemplatesController@store');
Route::get('mastersettings/certificate_templates/edit/{slug}', 'CertificateTemplatesController@edit');
Route::patch('mastersettings/certificate_templates/edit/{slug}', 'CertificateTemplatesController@update');
Route::delete('mastersettings/certificate_templates/delete/{id}', 'CertificateTemplatesController@delete');
Route::get('mastersettings/certificate_templates/getList', [ 'as'   => 'certificate_templates.dataTable',
    'uses' => 'CertificateTemplatesController@getDatatable']);

//Certificate Generation links

Route::get('certificates/dashboard', 'CertificatesController@certificatesDashboard');
Route::get('certificates/id-cards', 'CertificatesController@idCards');
Route::post('students/get-users', 'StudentController@getStudents');
Route::post('students/get-users/completed', 'StudentController@getCompletedStudents');
Route::post('students/get-users/detained', 'StudentController@getDetainedStudents');
Route::get('certificates/bonafide-certificates', 
    'CertificatesController@bonafideCertificates');

Route::post('students/feepaid/history', 'StudentController@getFeePaidHistoryDetails');


Route::post('certificates/id-cards', 'CertificatesController@printCards');

Route::post('certificate-issues/is-issued', 'CertificatesIssuesController@isIssued');
Route::post('certificate/issue', 'CertificatesIssuesController@issueCertificate');
Route::post('certificate/issue/tc', 'CertificatesIssuesController@tcDetails');


//Academic Holidays
Route::get('mastersettings/holidays', 'AcademicHolidaysController@index');
Route::get('mastersettings/holidays/add', 'AcademicHolidaysController@create');
Route::post('mastersettings/holidays/add', 'AcademicHolidaysController@store');
Route::get('mastersettings/holidays/edit/{slug}', 'AcademicHolidaysController@edit');
Route::patch('mastersettings/holidays/edit/{slug}', 'AcademicHolidaysController@update');
Route::delete('mastersettings/holidays/delete/{id}', 'AcademicHolidaysController@delete');
Route::get('mastersettings/holidays/getList', [ 'as'   => 'academicholidays.dataTable',
    'uses' => 'AcademicHolidaysController@getDatatable']);

//Student
Route::get('student/profile/edit/{slug}/{tab?}', 'StudentController@edit');
Route::patch('student/profile/edit/general/{slug}','StudentController@updateGeneralDetails');
Route::patch('student/profile/edit/personal/{slug}','StudentController@updatePersonalDetails');
Route::patch('student/profile/edit/contact/{slug}','StudentController@updateContactDetails');
Route::patch('student/profile/edit/parent/{slug}','StudentController@updateParentDetails');
Route::get('student/profile/{slug}', 'StudentController@show');
Route::post('student/get-parent-records', 'StudentController@getParentsOnSearch');

Route::post('student/courses', 'StudentController@courses');

Route::get('student/course-semister/{courseId}/{semisterId}', 'StudentController@coursesSemister');

//student Leave apply
Route::get('student/getLeaveList', 'StudentController@getLeaveDataTable');
Route::get('student/leave', 'StudentController@leaveindex');
Route::get('student/leave/add', 'StudentController@leavecreate');
Route::post('student/leave/add', 'StudentController@leavestore');

 

//subjects
Route::get('mastersettings/subjects', 'SubjectsController@index');
Route::get('mastersettings/subjects/add', 'SubjectsController@create');
Route::post('mastersettings/subjects/add', 'SubjectsController@store');
Route::get('mastersettings/subjects/edit/{slug}', 'SubjectsController@edit');
Route::patch('mastersettings/subjects/edit/{slug}', 'SubjectsController@update');
Route::delete('mastersettings/subjects/delete/{id}', 'SubjectsController@delete');
Route::get('mastersettings/subjects/getList', [ 'as'   => 'subjects.dataTable',
    'uses' => 'SubjectsController@getDatatable']);

Route::get('mastersettings/subjects/import', 'SubjectsController@import');
Route::post('mastersettings/subjects/import', 'SubjectsController@readExcel');
 
//Topics 
Route::get('mastersettings/topics', 'TopicsController@index');
Route::get('mastersettings/topics/add', 'TopicsController@create');
Route::post('mastersettings/topics/add', 'TopicsController@store');
Route::get('mastersettings/topics/edit/{slug}', 'TopicsController@edit');
Route::patch('mastersettings/topics/edit/{slug}', 'TopicsController@update');
Route::delete('mastersettings/topics/delete/{id}', 'TopicsController@delete');
Route::get('mastersettings/topics/getList', [ 'as'   => 'topics.dataTable',
    'uses' => 'TopicsController@getDatatable']);

Route::get('mastersettings/topics/get-parents-topics/{subject_id}', 'TopicsController@getParentTopics');

Route::get('mastersettings/topics/import', 'TopicsController@import');
Route::post('mastersettings/topics/import', 'TopicsController@readExcel');

//Subject Books Admin Module
Route::get('mastersettings/subjectbooks', 'TopicsController@subjectbookindex');
Route::get('mastersettings/subjectbooks/add', 'TopicsController@subjectbookcreate');
Route::post('mastersettings/subjectbooks/add', 'TopicsController@subjectbookstore');
Route::get('mastersettings/subjectbooks/edit/{slug}', 'TopicsController@subjectbookedit');
Route::patch('mastersettings/subjectbooks/edit/{slug}', 'TopicsController@subjectbookupdate');
Route::delete('mastersettings/subjectbooks/delete/{id}', 'TopicsController@subjectbookdelete');
Route::get('mastersettings/subjectbook/getList', 'TopicsController@getsubjectbookDatatable');
Route::get('download/assignments/{slug}', 'TopicsController@downloadFile');



//Users
Route::get('users/staff/{role?}', 'UsersController@index');
Route::get('users/create', 'UsersController@create');
Route::post('users/create/{role?}', 'UsersController@store');
Route::get('users/edit/{slug}', 'UsersController@edit');
Route::patch('users/edit/{slug}', 'UsersController@update');
Route::get('users/profile/{slug}', 'UsersController@show');
Route::get('users', 'UsersController@index');

Route::get('users/list/{user_type}', 'UsersController@listUsers');

                     ///////////////////////////////
                    // LIBRARY MANAGEMENT SYSTEM //
                    ///////////////////////////////

//Library dashboard
Route::get('library/librarydashboard', 'LibraryDashboardController@index');
Route::get('library/librarydashboard/books', 'LibraryDashboardController@books');
Route::get('library/librarydashboard/books/staffbooks', 'LibraryDashboardController@staffbooks');

Route::get('library/librarydashboard/getList', 
      ['as'  =>'librarydashboard.datatable',
      'uses' =>'LibraryDashboardController@getDatatable']);

Route::get('library/librarydashboard/getList/staff', 
    ['as'  =>'librarydashboard.staffdatatable',
    'uses' =>'LibraryDashboardController@getStaffDatatable']);




//Library Assets
Route::get('library/assets', 'LibraryAssetTypeController@index');
Route::get('library/assets/add', 'LibraryAssetTypeController@create');
Route::get('library/assets/view/{slug}', 'LibraryAssetTypeController@show');

Route::post('library/assets/add', 'LibraryAssetTypeController@store');
Route::get('library/assets/edit/{slug}', 'LibraryAssetTypeController@edit');
Route::patch('library/assets/edit/{slug}', 'LibraryAssetTypeController@update');
Route::delete('library/assets/delete/{id}', 'LibraryAssetTypeController@delete');
Route::get('library/assets/getList', [ 'as'   => 'libraryassets.dataTable',
    'uses' => 'LibraryAssetTypeController@getDatatable']);


//Library Masters
Route::get('library/master', 'LibraryMastersController@index');
Route::get('library/master/add', 'LibraryMastersController@create');
Route::get('library/master/view/{slug}', 'LibraryMastersController@show');
Route::get('library/master/details/{slug}', 'LibraryMastersController@master_assetDetails');
Route::get('library/master/details/print/{slug}', 'LibraryMastersController@printMasterAssetDetails');

Route::post('library/master/add', 'LibraryMastersController@store');
Route::get('library/master/edit/{slug}', 'LibraryMastersController@edit');
Route::patch('library/master/edit/{slug}', 'LibraryMastersController@update');
Route::delete('library/master/delete/{id}', 'LibraryMastersController@delete');
Route::get('library/master/getList', [ 'as'   => 'librarymasters.dataTable',
    'uses' => 'LibraryMastersController@getDatatable']);

//Library Collections
Route::get('library/master/collections/{slug}', 'LibraryMastersController@viewCollections');
Route::get('library/master/get-collection-list/{slug}', 'LibraryMastersController@getInstanceDatatable');
Route::get('library/master/collections/add/{slug}', 'LibraryMastersController@addCollection');
Route::post('library/master/collections/add/{slug}', 'LibraryMastersController@storeCollection');
Route::post('library/master/change-status', 'LibraryMastersController@statusChange');
Route::get('library/master/collections/barcode/{asset_no}', 'LibraryMastersController@generateBarCode');
Route::delete('library/master/collections/delete/{id}', 'LibraryMastersController@deleteRecord');


//Authors
Route::get('library/authors', 'AuthorsController@index');
Route::get('library/authors/add', 'AuthorsController@create');
Route::get('library/authors/view/{slug}', 'AuthorsController@show');
Route::post('library/authors/add', 'AuthorsController@store');
Route::get('library/authors/edit/{slug}', 'AuthorsController@edit');
Route::patch('library/authors/edit/{slug}', 'AuthorsController@update');
Route::delete('library/authors/delete/{id}', 'AuthorsController@delete');
Route::get('library/authors/getList', [ 'as'   => 'authors.dataTable',
    'uses' => 'AuthorsController@getDatatable']);

//Authors
Route::get('library/publishers', 'PublishersController@index');
Route::get('library/publishers/add', 'PublishersController@create');
Route::get('library/publishers/view/{slug}', 'PublishersController@show');
Route::post('library/publishers/add', 'PublishersController@store');
Route::get('library/publishers/edit/{slug}', 'PublishersController@edit');
Route::patch('library/publishers/edit/{slug}', 'PublishersController@update');
Route::delete('library/publishers/delete/{id}', 'PublishersController@delete');
Route::get('library/publishers/getList', [ 'as'   => 'publishers.dataTable',
    'uses' => 'PublishersController@getDatatable']);



//Asset Issues
Route::get('library/users/{role_name}', 'LibraryIssuesController@users');
Route::get('library/getUsersList/{role_name}', 'LibraryIssuesController@getUsers');
Route::get('library/issue/{slug}', 'LibraryIssuesController@issueAsset');
Route::get('library/issues/get-reference', 'LibraryIssuesController@getReference');
Route::get('library/issues/get-reference/staff', 'LibraryIssuesController@getstaffReference');
Route::post('library/issues/get-master-details', 'LibraryIssuesController@getMasterDetails');
Route::post('library/issues/issue-asset', 'LibraryIssuesController@store');
Route::post('library/returns/return-asset', 'LibraryIssuesController@returnAsset');
Route::post('library/returns/return-asset/staff', 'LibraryIssuesController@returnStaffAsset');




                    ////////////////////////
                    // EXAMINATION SYSTEM //
                    ////////////////////////

//Question bank
Route::get('exams/questionbank', 'QuestionBankController@index');
Route::get('exams/questionbank/add-question/{slug}', 'QuestionBankController@create');
Route::get('exams/questionbank/view/{slug}', 'QuestionBankController@show');

Route::post('exams/questionbank/add', 'QuestionBankController@store');
Route::get('exams/questionbank/edit-question/{slug}', 'QuestionBankController@edit');
Route::patch('exams/questionbank/edit/{slug}', 'QuestionBankController@update');
Route::delete('exams/questionbank/delete/{id}', 'QuestionBankController@delete');
Route::get('exams/questionbank/getList',  'QuestionBankController@getDatatable');

Route::get('exams/questionbank/getquestionslist/{slug}', 
     'QuestionBankController@getQuestions');
Route::get('exams/questionbank/import',  'QuestionBankController@import');
Route::post('exams/questionbank/import',  'QuestionBankController@readExcel');
Route::get('exams/questionbank/delete/option-file/{id}/{imagename}', 'QuestionBankController@deleteOptionFile');
Route::post('exams/questionbank/delete/question-file', 'QuestionBankController@deleteQuestionFile');




//Quiz Categories
Route::get('exams/categories', 'QuizCategoryController@index');
Route::get('exams/categories/add', 'QuizCategoryController@create');
Route::post('exams/categories/add', 'QuizCategoryController@store');
Route::get('exams/categories/edit/{slug}', 'QuizCategoryController@edit');
Route::patch('exams/categories/edit/{slug}', 'QuizCategoryController@update');
Route::delete('exams/categories/delete/{slug}', 'QuizCategoryController@delete');
Route::get('exams/categories/getList', [ 'as'   => 'quizcategories.dataTable',
    'uses' => 'QuizCategoryController@getDatatable']);

// Quiz Student Categories 
Route::get('exams/student/categories', 'StudentQuizController@index');
Route::get('exams/student/exams/{slug?}', 'StudentQuizController@exams');
Route::get('exams/student/quiz/getList/{slug?}', 'StudentQuizController@getDatatable');
Route::get('exams/student/quiz/take-exam/{slug?}', 'StudentQuizController@instructions');
Route::post('exams/student/start-exam/{slug}', 'StudentQuizController@startExam');
Route::get('exams/student/start-exam/{slug}', 'StudentQuizController@index');

Route::get('exams/student/get-scheduled-exams/{slug}', 'StudentQuizController@getScheduledExams');
Route::get('exams/student/load-scheduled-exams/{slug}', 'StudentQuizController@loadScheduledExams');


Route::post('exams/student/finish-exam/{slug}', 'StudentQuizController@finishExam');
Route::get('exams/student/reports/{slug}', 'StudentQuizController@reports');


Route::get('exams/student/exam-attempts/{user_slug}/{exam_slug?}', 'StudentQuizController@examAttempts');
Route::get('exams/student/get-exam-attempts/{user_slug}/{exam_slug?}', 'StudentQuizController@getExamAttemptsData');

Route::get('student/analysis/by-exam/{user_slug}', 'StudentQuizController@examAnalysis');
Route::get('student/analysis/get-by-exam/{user_slug}', 'StudentQuizController@getExamAnalysisData');

Route::get('student/analysis/by-subject/{user_slug}/{exam_slug?}/{results_slug?}', 'StudentQuizController@subjectAnalysisInExam');
Route::get('student/analysis/subject/{user_slug}', 'StudentQuizController@overallSubjectAnalysis');

//Student Reports
Route::get('student/exam/answers/{quiz_slug}/{result_slug}', 'ReportsController@viewExamAnswers');
Route::get('student/results/{slug}', 'ReportsController@viewAcademicResultsOfStudent');
Route::post('student/results/get-exam-categories', 'ReportsController@getExamCategories');
Route::post('student/results/get-exams', 'ReportsController@getExamsByCategory');


//Quiz 

//Exams ---dashboard
Route::get('exams/dashboard', 'QuizController@quizdashboard');
Route::get('exams/quizzes', 'QuizController@index');
Route::get('exams/quiz/add', 'QuizController@create');
Route::post('exams/quiz/add', 'QuizController@store');
Route::get('exams/quiz/edit/{slug}', 'QuizController@edit');
Route::patch('exams/quiz/edit/{slug}', 'QuizController@update');
Route::delete('exams/quiz/delete/{slug}', 'QuizController@delete');
Route::get('exams/quiz/getList/{slug?}', 'QuizController@getDatatable');

Route::get('exams/quiz/update-questions/{slug}', 'QuizController@updateQuestions');
Route::post('exams/quiz/update-questions/{slug}', 'QuizController@storeQuestions');


Route::post('exams/quiz/get-questions', 'QuizController@getSubjectData');

//Certificates controller
Route::get('result/generate-certificate/{slug}', 'CertificatesController@getCertificate');


//Exam Series 
Route::get('exams/exam-series', 'ExamSeriesController@index');
Route::get('exams/exam-series/add', 'ExamSeriesController@create');
Route::post('exams/exam-series/add', 'ExamSeriesController@store');
Route::get('exams/exam-series/edit/{slug}', 'ExamSeriesController@edit');
Route::patch('exams/exam-series/edit/{slug}', 'ExamSeriesController@update');
Route::delete('exams/exam-series/delete/{slug}', 'ExamSeriesController@delete');
Route::get('exams/exam-series/getList', 'ExamSeriesController@getDatatable');

//EXAM SERIES STUDENT LINKS
Route::get('exams/student-exam-series/list', 'ExamSeriesController@listSeries');
Route::get('exams/student-exam-series/{slug}', 'ExamSeriesController@viewItem');




Route::get('exams/exam-series/update-series/{slug}', 'ExamSeriesController@updateSeries');
Route::post('exams/exam-series/update-series/{slug}', 'ExamSeriesController@storeSeries');
Route::post('exams/exam-series/get-exams', 'ExamSeriesController@getExams');
Route::get('payment/cancel', 'ExamSeriesController@cancel');
Route::post('payment/success', 'ExamSeriesController@success');

            /////////////////////
            // PAYMENT REPORTS //
            /////////////////////
Route::get('payments-report/', 'PaymentsController@overallPayments');

 Route::get('payments-report/online/', 'PaymentsController@onlinePaymentsReport');
 Route::get('payments-report/online/{slug}', 'PaymentsController@listOnlinePaymentsReport');
Route::get('payments-report/online/getList/{slug}', 'PaymentsController@getOnlinePaymentReportsDatatable');

Route::get('payments-report/offline/', 'PaymentsController@offlinePaymentsReport');
Route::get('payments-report/offline/{slug}', 'PaymentsController@listOfflinePaymentsReport');
Route::get('payments-report/offline/getList/{slug}', 'PaymentsController@getOfflinePaymentReportsDatatable');
Route::get('payments-report/export', 'PaymentsController@exportPayments');
Route::post('payments-report/export', 'PaymentsController@doExportPayments');

Route::post('payments-report/getRecord', 'PaymentsController@getPaymentRecord');
Route::post('payments/approve-reject-offline-request', 'PaymentsController@approveOfflinePayment');

            //////////////////
            // INSTRUCTIONS  //
            //////////////////
            
Route::get('exam/instructions/list', 'InstructionsController@index');
Route::get('exam/instructions', 'InstructionsController@index');
Route::get('exams/instructions/add', 'InstructionsController@create');
Route::post('exams/instructions/add', 'InstructionsController@store');
Route::get('exams/instructions/edit/{slug}', 'InstructionsController@edit');
Route::patch('exams/instructions/edit/{slug}', 'InstructionsController@update');
Route::delete('exams/instructions/delete/{slug}', 'InstructionsController@delete');
Route::get('exams/instructions/getList', 'InstructionsController@getDatatable');

 
//BOOKMARKS MODULE
Route::get('student/bookmarks/{slug}', 'BookmarksController@index');
Route::post('student/bookmarks/add', 'BookmarksController@create');
Route::delete('student/bookmarks/delete/{id}', 'BookmarksController@delete');
Route::delete('student/bookmarks/delete_id/{id}', 'BookmarksController@deleteById');
Route::get('student/bookmarks/getList/{slug}',  'BookmarksController@getDatatable');
Route::post('student/bookmarks/getSavedList',  'BookmarksController@getSavedBookmarks');


                //////////////////////////
                // Notifications Module //
                /////////////////////////
Route::get('admin/notifications/list', 'NotificationsController@index');
Route::get('admin/notifications', 'NotificationsController@index');
Route::get('admin/notifications/add', 'NotificationsController@create');
Route::post('admin/notifications/add', 'NotificationsController@store');
Route::get('admin/notifications/edit/{slug}', 'NotificationsController@edit');
Route::patch('admin/notifications/edit/{slug}', 'NotificationsController@update');
Route::delete('admin/notifications/delete/{slug}', 'NotificationsController@delete');
Route::get('admin/notifications/getList', 'NotificationsController@getDatatable');

// NOTIFICATIONS FOR STUDENT
Route::get('notifications/list', 'NotificationsController@usersList');
Route::get('notifications/show/{slug}', 'NotificationsController@display');

 
//BOOKMARKS MODULE
Route::get('toppers/compare-with-topper/{user_result_slug}/{compare_slug?}', 'ExamToppersController@compare');

               
                        ////////////////
                        // LMS MODULE //
                        ////////////////

//LMS Categories
//Lms ---dashboard
Route::get('lms/dashboard', 'LmsCategoryController@lmsdashboard');
Route::get('lms/categories', 'LmsCategoryController@index');
Route::get('lms/categories/add', 'LmsCategoryController@create');
Route::post('lms/categories/add', 'LmsCategoryController@store');
Route::get('lms/categories/edit/{slug}', 'LmsCategoryController@edit');
Route::patch('lms/categories/edit/{slug}', 'LmsCategoryController@update');
Route::delete('lms/categories/delete/{slug}', 'LmsCategoryController@delete');
Route::get('lms/categories/getList', [ 'as'   => 'lmscategories.dataTable',
    'uses' => 'LmsCategoryController@getDatatable']);

//LMS Contents
Route::get('lms/content', 'LmsContentController@index');
Route::get('lms/content/add', 'LmsContentController@create');
Route::post('lms/content/add', 'LmsContentController@store');
Route::get('lms/content/edit/{slug}', 'LmsContentController@edit');
Route::patch('lms/content/edit/{slug}', 'LmsContentController@update');
Route::delete('lms/content/delete/{slug}', 'LmsContentController@delete');
Route::get('lms/content/getList', [ 'as'   => 'lmscontent.dataTable',
    'uses' => 'LmsContentController@getDatatable']);



//LMS Series 
Route::get('lms/series', 'LmsSeriesController@index');
Route::get('lms/series/add', 'LmsSeriesController@create');
Route::post('lms/series/add', 'LmsSeriesController@store');
Route::get('lms/series/edit/{slug}', 'LmsSeriesController@edit');
Route::patch('lms/series/edit/{slug}', 'LmsSeriesController@update');
Route::delete('lms/series/delete/{slug}', 'LmsSeriesController@delete');
Route::get('lms/series/getList', 'LmsSeriesController@getDatatable');

//LMS SERIES STUDENT LINKS
Route::get('lms/exam-series/list', 'LmsSeriesController@listSeries');
Route::get('lms/exam-series/{slug}', 'LmsSeriesController@viewItem');




Route::get('lms/series/update-series/{slug}', 'LmsSeriesController@updateSeries');
Route::post('lms/series/update-series/{slug}', 'LmsSeriesController@storeSeries');
Route::post('lms/series/get-series', 'LmsSeriesController@getSeries');
Route::get('payment/cancel', 'LmsSeriesController@cancel');
Route::post('payment/success', 'LmsSeriesController@success');


//LMS Student view
Route::get('learning-management/categories', 'StudentLmsController@index');
Route::get('learning-management/view/{slug}', 'StudentLmsController@viewCategoryItems');
Route::get('learning-management/series', 'StudentLmsController@series');
Route::get('learning-management/series/{slug}/{content_slug?}', 'StudentLmsController@viewItem');
Route::get('learning-management/content/{req_content_type}', 'StudentLmsController@content');
Route::get('learning-management/content/show/{slug}', 'StudentLmsController@showContent');
Route::get('user/paid/{slug}/{content_slug}', 'StudentLmsController@verifyPaidItem');


 

//Payments Controller
Route::get('payments/list/{slug}', 'PaymentsController@index');
Route::get('payments/getList/{slug}', 'PaymentsController@getDatatable');

Route::get('payments/checkout/{type}/{slug}', 'PaymentsController@checkout');

Route::post('payments/paynow/{slug}', 'PaymentsController@paynow');
Route::post('payments/paypal/status-success','PaymentsController@paypal_success');
Route::get('payments/paypal/status-cancel', 'PaymentsController@paypal_cancel');

Route::post('payments/payu/status-success','PaymentsController@payu_success');
Route::post('payments/payu/status-cancel', 'PaymentsController@payu_cancel');
Route::post('payments/offline-payment/update', 'PaymentsController@updateOfflinePayment');

                    
 

                        ////////////////////////////
                        // SETTINGS MODULE //
                        ///////////////////////////


//LMS Categories

//Settings ---dashboard
Route::get('settings/dashboard', 'SettingsController@settingsDashboard');
Route::get('mastersettings/settings/', 'SettingsController@index');
Route::get('mastersettings/settings/index', 'SettingsController@index');
Route::get('mastersettings/settings/certificates', 'SettingsController@certificatesdashboard');
Route::get('mastersettings/settings/timetable', 'SettingsController@timetabledashboard');
Route::get('mastersettings/settings/add', 'SettingsController@create');
Route::post('mastersettings/settings/add', 'SettingsController@store');
Route::get('mastersettings/settings/edit/{slug}', 'SettingsController@edit');
Route::patch('mastersettings/settings/edit/{slug}', 'SettingsController@update');
Route::get('mastersettings/settings/view/{slug}', 'SettingsController@viewSettings');
Route::get('mastersettings/settings/add-sub-settings/{slug}', 'SettingsController@addSubSettings');
Route::post('mastersettings/settings/add-sub-settings/{slug}', 'SettingsController@storeSubSettings');
Route::patch('mastersettings/settings/add-sub-settings/{slug}', 'SettingsController@updateSubSettings');
 
Route::get('mastersettings/settings/getList', [ 'as'   => 'mastersettings.dataTable',
     'uses' => 'SettingsController@getDatatable']);


                        ////////////////////////////
                        // MODULE HELPERS  MODULE //
                        ///////////////////////////

Route::get('mastersettings/module-helpers', 'ModuleHelperController@index');
Route::get('mastersettings/module-helpers/index', 'ModuleHelperController@index');
Route::get('mastersettings/module-helpers/add', 'ModuleHelperController@create');
Route::post('mastersettings/module-helpers/add', 'ModuleHelperController@store');
Route::get('mastersettings/module-helpers/edit/{slug}', 'ModuleHelperController@edit');
Route::patch('mastersettings/module-helpers/edit/{slug}', 'ModuleHelperController@update');
Route::get('mastersettings/module-helpers/view/{slug}', 'ModuleHelperController@viewSettings');
Route::get('mastersettings/module-helpers/add-sub-settings/{slug}', 'ModuleHelperController@addSubSettings');
// Route::post('mastersettings/module-helpers/add-sub-settings/{slug}', 'ModuleHelperController@storeSubSettings');
Route::patch('mastersettings/module-helpers/add-steps/{slug}', 'ModuleHelperController@updateSteps');
 
Route::get('mastersettings/module-helpers/getList', [ 'as'   => 'mastersettings.module-helper.dataTable',
     'uses' => 'ModuleHelperController@getDatatable']);

                        ////////////////////////////
                        // EMAIL TEMPLATES MODULE //
                        ///////////////////////////

//LMS Categories
Route::get('email/templates', 'EmailTemplatesController@index');
Route::get('email/templates/add', 'EmailTemplatesController@create');
Route::post('email/templates/add', 'EmailTemplatesController@store');
Route::get('email/templates/edit/{slug}', 'EmailTemplatesController@edit');
Route::patch('email/templates/edit/{slug}', 'EmailTemplatesController@update');
Route::delete('email/templates/delete/{slug}', 'EmailTemplatesController@delete');
Route::get('email/templates/getList', [ 'as'   => 'emailtemplates.dataTable',
    'uses' => 'EmailTemplatesController@getDatatable']);


//Coupons Module
Route::get('coupons/list', 'CouponcodesController@index');
Route::get('coupons/add', 'CouponcodesController@create');
Route::post('coupons/add', 'CouponcodesController@store');
Route::get('coupons/edit/{slug}', 'CouponcodesController@edit');
Route::patch('coupons/edit/{slug}', 'CouponcodesController@update');
Route::delete('coupons/delete/{slug}', 'CouponcodesController@delete');
Route::get('coupons/getList/{slug?}', 'CouponcodesController@getDatatable');

Route::get('coupons/get-usage', 'CouponcodesController@getCouponUsage');
Route::get('coupons/get-usage-data', 'CouponcodesController@getCouponUsageData');
Route::post('coupons/update-questions/{slug}', 'CouponcodesController@storeQuestions');


Route::post('coupons/validate-coupon', 'CouponcodesController@validateCoupon');


//Feedback Module
Route::get('feedback/list', 'FeedbackController@index');
Route::get('feedback/view-details/{slug}', 'FeedbackController@details');
Route::get('feedback/send', 'FeedbackController@create');
Route::post('feedback/send', 'FeedbackController@store');
Route::delete('feedback/delete/{slug}', 'FeedbackController@delete');
Route::get('feedback/getlist', 'FeedbackController@getDatatable');

//SMS Module

Route::get('sms/index', 'SMSAgentController@index');
Route::post('sms/send', 'SMSAgentController@sendSMS');

                        /////////////////////
                        // MESSAGES MODULE //
                        /////////////////////


Route::group(['prefix' => 'messages'], function () {
    Route::get('/', ['as' => 'messages', 'uses' => 'MessagesController@index']);
    Route::get('create', ['as' => 'messages.create', 'uses' => 'MessagesController@create']);
    Route::post('/', ['as' => 'messages.store', 'uses' => 'MessagesController@store']);
    Route::get('{id}', ['as' => 'messages.show', 'uses' => 'MessagesController@show']);
    Route::put('{id}', ['as' => 'messages.update', 'uses' => 'MessagesController@update']);
});


                        //////////////////////
                        //Attendance Module //
                        //////////////////////
//Student attendance
Route::get('student/attendance/report', 'StudentAttendanceReportController@index');
Route::get('student/attendance/{slug}', 'StudentAttendanceController@index');
Route::get('student/attendance/add/{slug}', 'StudentAttendanceController@index');
Route::post('student/attendance/add/{slug}', 'StudentAttendanceController@create');
Route::post('student/attendance/update/{slug}', 'StudentAttendanceController@updateAtt');
Route::get('student/attendance/edit/{slug}', 'StudentAttendanceController@edit');
Route::patch('student/attendance/edit/{slug}', 'StudentAttendanceController@update');
Route::delete('student/attendance/delete/{id}', 'StudentAttendanceController@delete');
Route::get('student/attendance/getList', [ 'as'   => 'attendance.dataTable',
    'uses' => 'StudentAttendanceController@getDatatable']);    





//Student Attendence Report


Route::get('student/attendance/reports/{slug}', 'StudentAttendanceReportController@viewStudentAttendance');
Route::post('student/attendance/reports/get-attendance', 'StudentAttendanceReportController@getAttendance');
Route::post('student/attendance/reports/count-attendance', 'StudentAttendanceReportController@countAttendance');

Route::get('student/class-attendance', 'StudentAttendanceReportController@classAttendance');
Route::post('student/class-attendance', 'StudentAttendanceReportController@getClassAttendance');
Route::post('student/class-attendance/print', 'StudentAttendanceReportController@printClassAttendance');
Route::post('lessionplan/by-attendance', 'StudentAttendanceReportController@getLessionPlansLatestRecords');                   


                        //////////////////////////
                        // STUDENT MARKS MODULE //
                        //////////////////////////

Route::get('student/class-marks', 'StudentMarksReportController@classMarks');
Route::post('student/class-offline-exams-list', 'StudentMarksReportController@offlineExamsList');
Route::post('student/class-marks', 'StudentMarksReportController@getClassMarks');
Route::post('student/class-marks/offline-exams/print', 'StudentMarksReportController@printClassMarks');

//Student List According to the class

Route::get('student/list', 'StudentListController@index');
Route::post('student/list/classwise/print', 'StudentListController@printStudentList');
Route::get('student/completed/list', 'StudentListController@courseCompltedStudentsList');
Route::post('student/completed/list/print', 'StudentListController@printCourseCompltedStudentsList');
Route::get('student/detained/list', 'StudentListController@courseDetainedStudentsList');
Route::post('student/detained/list/print', 'StudentListController@printCourseDetainedStudentsList');
Route::get('class/feepaid-history', 'StudentListController@feePaidHistory');
Route::post('students/feepaid/history/print', 'StudentListController@printFeePaidHistory');
// Route::post('student/class-offline-exams-list', 'StudentMarksReportController@offlineExamsList');
// Route::post('student/class-marks', 'StudentMarksReportController@getClassMarks');


// LESSION PLANS MODULE
Route::get('staff/lession-plans/{slug}', 'LessionPlansController@index');
Route::get('staff/lession-plans/view-topics/{userSlug}/{courseSlug}', 'LessionPlansController@viewTopics');
Route::post('staff/lession-plans/update-topic', 'LessionPlansController@updateTopic');
Route::post('staff/lession-plans/get-last-updated-records', 'LessionPlansController@getLastUpdatedRecords');
Route::get('staff/lession-plans/student-list/{slug}', 'LessionPlansController@studentlist');
Route::post('staff/lession-plans/view-students', 'LessionPlansController@viewStudents');

Route::get('staff/lession-plans/view-students/get-list/{academic_id}/{course_parent_id}/{course_id}/{year}/{semister}', 'LessionPlansController@getDatatable');    



//STUDENT TRANSFERS
Route::get('student/transfers', 'StudentPromotionsController@index');
Route::post('student/transfers', 'StudentPromotionsController@transferStudents');

Route::post('student/transfer-single-student', 'StudentPromotionsController@transferSingleStudent');

//SEARCH STUDENT
Route::post('student/search', 'StudentController@globalStudentSearch');

//OFFLINE MARKS UPLOAD
Route::get('marks/upload', 'QuizResultsController@index');

//TIMETABLE MODULE
Route::get('timetable', 'TimetableController@index');


//Timingset 
Route::get('timetable/timing-set', 'TimingsetController@index');
Route::get('timetable/timing-set/add', 'TimingsetController@create');
Route::post('timetable/timing-set/add', 'TimingsetController@store');
Route::get('timetable/timing-set/edit/{slug}', 'TimingsetController@edit');
Route::patch('timetable/timing-set/edit/{slug}', 'TimingsetController@update');
Route::delete('timetable/timing-set/delete/{slug}', 'TimingsetController@delete');
Route::delete('timetable/timing-set/delete-timingset-record/{slug}', 'TimingsetController@deleteTimingsetRecord');
// Route::get('timetable/timing-set/getList', 'TimingsetController@getDatatable');
Route::get('timetable/timing-set/getList', [ 'as'   => 'timingset.dataTable',
    'uses' => 'TimingsetController@getDatatable']);


//Allot time table
Route::get('timetable/allot-timetable', 'TimetableController@viewTimetable');
// Route::get('timetable/print', 'TimetableController@printTimetable');
Route::post('timetable/print', 'TimetableController@printTimetable');

Route::post('timetable/get-timetable-details', 'TimetableController@getAllocatedStaffAndTimings');

Route::post('timetable/update-timetable', 'TimetableController@updateTimetable');
Route::post('timetable/is-available', 'TimetableController@checkStaffIsAvailable');

Route::get('timetable/user/{slug}', 'TimetableController@staffTimetable');
Route::get('timetable/print/{slug}', 'TimetableController@printStaffStudentTimetable');


//Timing Map
Route::get('timetable/map-timingset/{slug}', 'MapTimingsetController@create');
Route::post('timetable/map-timingset/add/{slug}', 'MapTimingsetController@store');


//Timing Map Parent list
Route::get('maptimingset/parent', 'MapTimingSetParentController@index');
Route::get('maptimingset/parent/add', 'MapTimingSetParentController@create');
Route::post('maptimingset/parent/add', 'MapTimingSetParentController@store');
Route::get('maptimingset/parent/edit/{slug}', 'MapTimingSetParentController@edit');
Route::patch('maptimingset/parent/edit/{slug}', 'MapTimingSetParentController@update');
Route::delete('maptimingset/parent/delete/{id}', 'MapTimingSetParentController@delete');
Route::get('maptimingset/parent/getList', [ 'as'   => 'maptimingsetparent.dataTable',
    'uses' => 'MapTimingSetParentController@getDatatable']);
Route::get('maptimingset/parent/get-academics', 'MapTimingSetParentController@getAcademics');



//Transportation Vechicles
Route::get('transportation/vehicles', 'TransportationController@index');
Route::get('transportation/vehicles/add', 'TransportationController@create');
Route::post('transportation/vehicles/add', 'TransportationController@store');
Route::get('transportation/vehicles/edit/{slug}', 'TransportationController@edit');
Route::patch('transportation/vehicles/edit/{slug}', 'TransportationController@update');
Route::delete('transportation/vehicles/delete/{id}', 'TransportationController@delete');
Route::get('transportation/vehicles/getlist', [ 'as'   => 'transportationvehicles.dataTable',
    'uses' => 'TransportationController@getDatatable']);


//Transportation Vechicles types
Route::get('transportation/vehicles/types', 'TransportationVehicletypeController@index');
Route::get('transportation/vehicles/types/add', 'TransportationVehicletypeController@create');
Route::post('transportation/vehicles/types/add', 'TransportationVehicletypeController@store');
Route::get('transportation/vehicles/types/edit/{slug}', 'TransportationVehicletypeController@edit');
Route::patch('transportation/vehicles/types/edit/{slug}', 'TransportationVehicletypeController@update');
Route::delete('transportation/vehicles/types/delete/{id}', 'TransportationVehicletypeController@delete');
Route::get('transportation/vehicles/types/getlist', [ 'as'   => 'transportationvehicletypes.dataTable',
    'uses' => 'TransportationVehicletypeController@getDatatable']);


//Offline Exams
Route::get('academicoperations/offline-exams', 'OfflineExamsController@index');
Route::get('academicoperations/offline-exams/exams/{slug}','OfflineExamsController@selectionview');
Route::post('academicoperations/offline-exams/add','OfflineExamsController@entermarks');
Route::post('academicoperations/offline-exams/store', 'OfflineExamsController@store');
Route::get('academicoperations/offline-exams/getlist', [ 'as'   => 'offlineexams.dataTable',
    'uses' => 'OfflineExamsController@getDatatable']);
 Route::delete('academicoperations/offline-exams/delete/{id}', 'OfflineExamsController@delete');


Route::get('academicoperations/offline-exams/import-excel', 'OfflineExamsController@import');
Route::post('academicoperations/offline-exams/import-excel', 'OfflineExamsController@readExcel');
Route::post('academicoperations/offline-exams/get-information', 'OfflineExamsController@getOfflineExamsInformation');



//OfflineExams Quiz Categories
Route::get('offlineexmas/quiz/categories', 'OfflineQuizCategoriesController@index');
Route::get('offlineexmas/quiz/categories/add', 'OfflineQuizCategoriesController@create');
Route::post('offlineexmas/quiz/categories/add', 'OfflineQuizCategoriesController@store');
Route::get('offlineexmas/quiz/categories/edit/{slug}', 'OfflineQuizCategoriesController@edit');
Route::patch('offlineexmas/quiz/categories/edit/{slug}', 'OfflineQuizCategoriesController@update');
Route::delete('offlineexmas/quiz/categories/delete/{id}', 'OfflineQuizCategoriesController@delete');
Route::get('offlineexmas/quiz/categories/getList', [ 'as'   => 'offlinequizcategories.dataTable',
    'uses' => 'OfflineQuizCategoriesController@getDatatable']);


//academic operations ---dashboard
Route::get('academicoperations/dashboard', 'AcademicOperationsDashboardController@index');

//Student Quiz dashboard
Route::get('student/quiz/dashboard', 'StudentQuizController@dashboard');

Route::get('user/search', 'SearchController@index');

Route::post('html/print-data', 'PrinterController@printHtml');

                          ////////////////////
                         // UPDATE PATCHES //
                         ////////////////////
 Route::get('updates/patch1', 'UpdatesController@patch1');
 Route::get('updates/patch2', 'UpdatesController@patch2');
 
                     ///////////////////////
                     ///FEE MODULE//////////
                     ///////////////////////
 
//Fee Categories
Route::get('fee/dashboard', 'FeeCategoryController@feeDashboard');
Route::get('fee/categories', 'FeeCategoryController@index');
Route::get('fee/categories/add', 'FeeCategoryController@create');
Route::post('fee/categories/add', 'FeeCategoryController@store');
Route::get('fee/categories/edit/{slug}', 'FeeCategoryController@edit');
Route::patch('fee/categories/edit/{slug}', 'FeeCategoryController@update');
Route::delete('fee/categories/delete/{slug}', 'FeeCategoryController@delete');
Route::get('fee/categories/getList', [ 'as'   => 'feecategories.dataTable',
    'uses' => 'FeeCategoryController@getDatatable']);

//Fee Schedules
Route::get('fee/category/schedule/{slug}', 'FeeScheduleController@createSchedules');
Route::post('fee/category/schedule-add', 'FeeScheduleController@storeSchedules');
Route::post('student/feeschedule/details', 'FeeScheduleController@getStudentScheduleDetails');
Route::post('student/feepaid-history', 'FeeScheduleController@getStudentFeePaidDetails');
Route::post('fee/schedule/delete', 'FeeScheduleController@deleteSchedules');


//Fee Category and Academic Courses Allotment
// Route::get('fee/categories/allot-category', 'FeeCategoryController@categoryAllotment');
// Route::post('fee/categories/get_alloted-categories', 'FeeCategoryController@getAllotedCategories');
// Route::post('fee/categories/allot-category', 'FeeCategoryController@allotFeeCategory');

//Add Particular To Fee Category
Route::get('fee/category-particulars/{slug}', 'FeeCategoryParticularsController@categoryAllotment');
Route::post('fee/category-particulars/update/{slug}', 'FeeCategoryParticularsController@updateParticulars');


//Fee Particulars
Route::get('fee/particulars', 'FeeparticularController@index');
Route::get('fee/particulars/add', 'FeeparticularController@create');
Route::post('fee/particulars/add', 'FeeparticularController@store');
Route::get('fee/particulars/edit/{slug}', 'FeeparticularController@edit');
Route::patch('fee/particulars/edit/{slug}', 'FeeparticularController@update');
Route::delete('fee/particulars/delete/{id}', 'FeeparticularController@delete');
Route::get('fee/particulars/getList', [ 'as'   => 'feeparticulars.dataTable',
    'uses' => 'FeeparticularController@getDatatable']);



//Fee Pay
Route::get('fee/payfee','FeePayController@index');
Route::post('fee/category/students','FeePayController@getFeeCategoryStudents');
Route::post('fee/student/feepay','FeePayController@getStudentPayFee');
Route::post('fee/feepay/particular','FeePayController@payFeeToParticular');

//Fee Pay online
Route::post('feepay/online/checkout','FeePayController@feeCheckout');
Route::post('paynow/fee','FeePayController@payNow');
Route::post('paynow/fee/pau-success','FeePayController@payuSuccess');
Route::post('paynow/fee/pau-cancel','FeePayController@payuCancel');
Route::post('paynow/fee/paypal-success','FeePayController@paypalSuccess');
Route::get('paynow/fee/paypal-cancel','FeePayController@paypalCancel');
Route::post('paynow/fee/offline','FeePayController@offlinePayment');
Route::get('fee/offline/payments','FeePayController@adminOfflineFeePayment');
Route::get('fee/offline/payments/getlist', [ 'as'   => 'feeofflinepayments.dataTable',
    'uses' => 'FeePayController@getOfflientPaymetnsList']);
Route::post('fee/offline/get-payment','FeePayController@getOfflinePaymentRecord');
Route::post('fee/offline/approve/payment','FeePayController@approveOfflinePaymentRecord');


//Fee Reports
Route::get('fee/reports','FeeRreportsController@index');
Route::get('fee/reports/daily','FeeRreportsController@getReports');
Route::get('fee/reports/daily/getList', [ 'as'   => 'feedailyreports.dataTable',
    'uses' => 'FeeRreportsController@getDatatable']);
Route::post('fee/reports/getdaily/payments','FeeRreportsController@getPayments');
Route::post('fee/reports/dates/payments','FeeRreportsController@getDatePayments');
Route::post('fee/reports/lastweek/payments','FeeRreportsController@getLastWeekPayments');
Route::post('fee/reports/lastmonth/payments','FeeRreportsController@getLastMonthPayments');


Route::get('fee/print-receipt/{slug}','FeePayController@generateReceipt');



            ////////////////////////
            ///Hostel Management////
            ///////////////////////
//Hostel
Route::get('hostel', 'HostelController@index');
Route::get('hostel/add', 'HostelController@create');
Route::post('hostel/add', 'HostelController@store');
Route::get('hostel/edit/{slug}', 'HostelController@edit');
Route::patch('hostel/edit/{slug}', 'HostelController@update');
Route::delete('hostel/delete/{id}', 'HostelController@delete');
Route::get('hostel/getList', 'HostelController@getDatatable');

//Room Type
Route::get('hostel/room_type', 'RoomTypeController@index');
Route::get('hostel/room_type/add', 'RoomTypeController@create');
Route::post('hostel/room_type/add', 'RoomTypeController@store');
Route::get('hostel/room_type/edit/{slug}', 'RoomTypeController@edit');
Route::patch('hostel/room_type/edit/{slug}', 'RoomTypeController@update');
Route::delete('hostel/room_type/delete/{id}', 'RoomTypeController@delete');
Route::get('hostel/room_type/getList', 'RoomTypeController@getDatatable');

//Hostel Rooms
Route::get('hostel-room', 'HostelRoomController@index');
Route::get('hostel-room/add', 'HostelRoomController@create');
Route::post('hostel-room/add', 'HostelRoomController@store');
Route::get('hostel-room/edit/{slug}', 'HostelRoomController@edit');
Route::patch('hostel-room/edit/{slug}', 'HostelRoomController@update');
Route::delete('hostel-room/delete/{id}', 'HostelRoomController@delete');
Route::get('hostel-room/getList', 'HostelRoomController@getDatatable');

//Assign Hostel
Route::get('student/hostel/assign', 'HostelAssignController@index');
Route::post('assign/hostel/getstudents', 'HostelAssignController@getStudents');
Route::post('assign-signle/hostel/getstudents', 'HostelAssignController@getSingleBranchStudents');
Route::post('signle-branch/get-parent-courses', 'HostelAssignController@getParentCourses');
Route::post('signle-branch/get-child-courses', 'HostelAssignController@getChildCourses');
Route::get('assign-hostel-to/{slug}', 'HostelAssignController@assignHostel');
Route::post('hostel-rooms', 'HostelAssignController@getRooms');
Route::post('hostel-details/store', 'HostelAssignController@store');
Route::post('vacate-hostel', 'HostelAssignController@vacateHostel');

//Assign Hostel Fee
Route::get('hostel-fee-assign', 'HostelFeeAssignController@index');
Route::post('store/hostel-fee-assign', 'HostelFeeAssignController@storeFee');

//Pay Hostel Fee
Route::get('hostel-fee-pay', 'HostelFeeAssignController@payHostelFee');
Route::post('hostel-get/rooms', 'HostelFeeAssignController@getHostelRooms');
Route::post('hostel-room/users', 'HostelFeeAssignController@getRoomUsers');
Route::post('hostel-fee/student-data', 'HostelFeeAssignController@getStudentData');
Route::post('hostel-fee/discount', 'HostelFeeAssignController@discount');
Route::post('hostel-fee-add', 'HostelFeeAssignController@payFee');


//Hostel Fee Reports
Route::get('hostel-fee-reports', 'HostelFeeAssignController@reports');
Route::post('hostel-months', 'HostelFeeAssignController@getmonths');
Route::post('hostel-year-reports', 'HostelFeeAssignController@getFeeReports');
Route::post('hostel-fee-print', 'HostelFeeAssignController@printFeeReports');

//Plugin
Route::get('hostel-plugin-add', 'HostelPluginController@addHostel');



                 //////////////////////////
                 ////////////Payroll////////
                 /////////////////////////
                
                
Route::get('payroll/salary_templates', 'SalaryTemplatesController@index');
Route::get('payroll/salary_templates/add', 'SalaryTemplatesController@create');
Route::post('payroll/salary_templates/add', 'SalaryTemplatesController@store');
Route::get('payroll/salary_templates/edit/{slug}', 'SalaryTemplatesController@edit');
Route::patch('payroll/salary_templates/edit/{slug}', 'SalaryTemplatesController@update');
Route::delete('payroll/salary_templates/delete/{id}', 'SalaryTemplatesController@delete');
Route::get('payroll/salary_templates/getList', [ 'as'   => 'salary_templates.dataTable',
    'uses' => 'SalaryTemplatesController@getDatatable']);

Route::get('payroll/hourly_templates', 'HourlyTemplatesController@index');
Route::get('payroll/hourly_templates/add', 'HourlyTemplatesController@create');
Route::post('payroll/hourly_templates/add', 'HourlyTemplatesController@store');
Route::get('payroll/hourly_templates/edit/{slug}', 'HourlyTemplatesController@edit');
Route::patch('payroll/hourly_templates/edit/{slug}', 'HourlyTemplatesController@update');
Route::delete('payroll/hourly_templates/delete/{id}', 'HourlyTemplatesController@delete');
Route::get('payroll/hourly_templates/getList', [ 'as'   => 'hourly_templates.dataTable',
    'uses' => 'HourlyTemplatesController@getDatatable']);

Route::get('payroll/manage_salary', 'ManageSalaryController@index');
Route::get('payroll/manage_salary/add', 'ManageSalaryController@create');
Route::post('payroll/manage_salary/add', 'ManageSalaryController@store');
Route::get('payroll/manage_salary/edit/{id}', 'ManageSalaryController@edit');
Route::patch('payroll/manage_salary/edit/{id}', 'ManageSalaryController@update');
Route::delete('payroll/manage_salary/delete/{id}', 'ManageSalaryController@delete');
Route::get('payroll/manage_salary/getList', [ 'as'   => 'manage_salary.dataTable',
    'uses' => 'ManageSalaryController@getDatatable']);
Route::get('payroll/manage_salary/ajaxGetUsers', 'ManageSalaryController@ajaxGetUsers');
Route::get('payroll/manage_salary/ajaxGetTemplates', 'ManageSalaryController@ajaxGetTemplates');
Route::get('payroll/manage_salary/ajaxGetUserDetails', 'ManageSalaryController@ajaxGetUserDetails');


//Staff Attendance
Route::get('staff/attendance', 'StaffAttendanceController@index');
Route::post('branch/parentcourses', 'StaffAttendanceController@getBranchParentCourses');
Route::post('branch/staff', 'StaffAttendanceController@viewBranchStaff');
Route::post('branch/update/staff/attendance', 'StaffAttendanceController@updateAttendance');

//Pay Salary
Route::get('pay-salary', 'PaySalaryController@index');
Route::post('pay-salary/staff', 'PaySalaryController@viewStaff');
Route::get('getstaff/{role_id}/{course_parent_id}', 'PaySalaryController@getDatatable');
Route::get('salary/pays/{slug}', 'PaySalaryController@payToStaff');
Route::post('pay/salary/staff', 'PaySalaryController@paySalary');
Route::get('get/staff/salary-history/{user_id}', 'PaySalaryController@staffSalaryHistory');
Route::delete('delete/staff/salary/{id}', 'PaySalaryController@delete');
Route::get('view/salary-details/{id}', 'PaySalaryController@details');
Route::get('salary/reports', 'PaySalaryController@reports');
Route::post('monthly/reports', 'PaySalaryController@getMonthlyReports');
Route::post('print/salary/reports', 'PaySalaryController@printReports');

Route::get('staff/salary-details/{slug}', 'PaySalaryController@staffDetails');
Route::get('staff/salary-history/{id}', 'PaySalaryController@SalaryHistory');
Route::get('staff/pay-slip/{pay_id}', 'PaySalaryController@staffPaySlip');

                         /////////////////////////////////
                         /////////Expenses///////////////
                         ////////////////////////////////
                        

//Expense Categories Module
Route::get('expense-categories/list', 'ExpenseCategoryController@index');
Route::get('expense-categories/add', 'ExpenseCategoryController@create');
Route::post('expense-categories/add', 'ExpenseCategoryController@store');
Route::get('expense-categories/edit/{slug}', 'ExpenseCategoryController@edit');
Route::patch('expense-categories/edit/{slug}', 'ExpenseCategoryController@update');
Route::delete('expense-categories/delete/{slug}', 'ExpenseCategoryController@delete');
Route::get('expense-categories/getList/{slug?}', 'ExpenseCategoryController@getDatatable');



//Expenses Module
Route::get('expenses/list', 'ExpensesController@index');
Route::get('expenses/add', 'ExpensesController@create');
Route::post('expenses/add', 'ExpensesController@store');
Route::get('expenses/edit/{slug}', 'ExpensesController@edit');
Route::patch('expenses/edit/{slug}', 'ExpensesController@update');
Route::delete('expenses/delete/{slug}', 'ExpensesController@delete');
Route::get('expenses/getList/{slug?}', 'ExpensesController@getDatatable');


Route::get('overall-reports','OverallRreportsController@index');
Route::post('overall-reports','OverallRreportsController@index');                        


             ///////////////////////////////////
            ////////////Certificates//////////
            //////////////////////////////////

  //Genrate Certificate
Route::get('certificate', 'GenrateCertificateController@index');
Route::get('certificate/add', 'GenrateCertificateController@create');
Route::post('certificate/add', 'GenrateCertificateController@store');
Route::get('certificate/edit/{slug}', 'GenrateCertificateController@edit');
Route::patch('certificate/edit/{slug}', 'GenrateCertificateController@update');
Route::delete('certificate/delete/{id}', 'GenrateCertificateController@delete');
Route::get('certificate/getList', [ 'as'   => 'genratecertificate.dataTable',
    'uses' => 'GenrateCertificateController@getDatatable']);
Route::post('certificate/data', 'GenrateCertificateController@certificateData');


//Stripe
Route::get ( 'stripe-payment', 'PaymentsController@loadStripeForm');
Route::post ( 'stripe-payment', 'PaymentsController@payStripe');

//Paytm
Route::post ( 'paytm-callback', 'PaymentsController@paymentCallback');

//Consolidate Reports Module
Route::get('student/online-exam/reports', 'OnlineMarksReportsController@index');
Route::post('student/online-exam/categories', 'OnlineMarksReportsController@onlineExamCategoryList');
Route::post('student/online-exam/marks', 'OnlineMarksReportsController@getClassMarks');
Route::post('student/online-exam/marks/print', 'OnlineMarksReportsController@printClassMarks');
Route::get('student/reports', 'OnlineMarksReportsController@dashboard');
Route::get('student/view', 'OnlineMarksReportsController@studentList');
Route::post('student/consolidate_report', 'OnlineMarksReportsController@viewStudentConsolidateReport');
Route::post('student/print/consolidate_report', 'OnlineMarksReportsController@printConsolidate');


                        ///////////////////////////
                        //////// Transport////////
                        /////////////////////////

//Route Type
Route::get('vehicle/routes', 'VRoutesController@index');
Route::get('vehicle/routes/add', 'VRoutesController@create');
Route::post('vehicle/routes/add', 'VRoutesController@store');
Route::get('vehicle/routes/edit/{slug}', 'VRoutesController@edit');
Route::patch('vehicle/routes/edit/{slug}', 'VRoutesController@update');
Route::delete('vehicle/routes/delete/{id}', 'VRoutesController@delete');
Route::get('vehicle/routes/getList', 'VRoutesController@getDatatable');
Route::post('vehicle/vai-routes', 'VRoutesController@viaRoutes');


//Vechicles
Route::get('vehicles', 'VechiclesController@index');
Route::get('vehicles/add', 'VechiclesController@create');
Route::post('vehicles/add', 'VechiclesController@store');
Route::get('vehicles/edit/{slug}', 'VechiclesController@edit');
Route::patch('vehicles/edit/{slug}', 'VechiclesController@update');
Route::delete('vehicles/delete/{id}', 'VechiclesController@delete');
Route::get('vehicles/getList', 'VechiclesController@getDatatable');

// Vehicle fuel details
Route::get('vehicles/fueldetails', 'VechiclesController@vehiclefuelindex');
Route::get('vehicles/getfuelList', 'VechiclesController@getfuelDatatable');


//Vechicle Route Assign
Route::get('route-assigned/vehicles','RouteVechicleController@vehicleList');
Route::get('route-assigned/vehicles/getList', 'RouteVechicleController@getDatatable');
Route::get('assign/vehicle/{id}','RouteVechicleController@index');
Route::post('assigned/routes/store','RouteVechicleController@update');

//Assign Vechicle Student
Route::get('student/vehicle/assign', 'VehicleAssignController@index');
Route::post('assigned-vehicle/getstudents', 'VehicleAssignController@getStudents');
Route::post('assign-signle/vehicle/getstudents', 'VehicleAssignController@getSingleBranchStudents');
Route::post('transport/signle-branch/get-parent-courses', 'VehicleAssignController@getParentCourses');
Route::post('transport/signle-branch/get-child-courses', 'VehicleAssignController@getChildCourses');
Route::get('assign-vehicle-to/{slug}', 'VehicleAssignController@assignVechicle');
Route::post('vehicle-routes', 'VehicleAssignController@getRoutes');
Route::post('vehicle-details/store', 'VehicleAssignController@store');
Route::post('stop/user/vehicle', 'VehicleAssignController@stopUser');

Route::post('view/students', 'VehicleAssignController@loadStudents');
Route::post('vehicle-routes/students', 'VehicleAssignController@loadRoutes');
Route::post('store/selected-student', 'VehicleAssignController@storeStudent');
Route::post('remove/selected-student', 'VehicleAssignController@removeStudent');
Route::post('show/assigned_users', 'VehicleAssignController@showUsers');

//Assign Vechicle Staff
Route::get('staff/vehicle/assign', 'VehicleAssignController@staffindex');
Route::get('staff/assign-vehicle-to/{slug}', 'VehicleAssignController@staffassignVechicle');
Route::post('staff-vehicle-routes', 'VehicleAssignController@getstaffRoutes');
Route::post('staff-vehicle-details/store', 'VehicleAssignController@staffstore');
Route::post('stop/staffuser/vehicle', 'VehicleAssignController@stopstaffUser');

Route::get('view/staffs', 'VehicleAssignController@loadStaffs');
Route::post('vehicle-routes/staffs', 'VehicleAssignController@loadStaffRoutes');
Route::post('store/selected-staffs', 'VehicleAssignController@storeStaffs');
Route::post('remove/selected-staffs', 'VehicleAssignController@removeStaffs');
Route::post('show/assigned_staffusers', 'VehicleAssignController@showstaffUsers');

//Assign Transport Fee
Route::get('transport-fee-assign', 'TransportFeeAssignController@index');
Route::post('store/transport-fee-assign', 'TransportFeeAssignController@storeFee');


//Pay Transport Fee
Route::get('transport-fee-pay', 'TransportFeeAssignController@payHostelFee');
Route::post('transport-route/vechicles', 'TransportFeeAssignController@getRouteVehicles');
Route::post('transport/users', 'TransportFeeAssignController@getVechicleUsers');
Route::post('transport-fee/student-data', 'TransportFeeAssignController@getStudentData');
Route::post('transport-fee/discount', 'TransportFeeAssignController@discount');
Route::post('transport-fee-add', 'TransportFeeAssignController@payFee');

//Transport Fee Reports
Route::get('transport-fee-reports', 'TransportFeeAssignController@reports');
Route::post('transport-months', 'TransportFeeAssignController@getmonths');
Route::post('transport-year-reports', 'TransportFeeAssignController@getFeeReports');
Route::post('transport-fee-print', 'TransportFeeAssignController@printFeeReports');

//Transport Plugin
Route::get('transportation-plugin-add', 'TransportationPluginController@addTransportation');


//Drivers Type
Route::get('driver', 'DriversController@index');
Route::get('driver/add', 'DriversController@create');
Route::post('driver/add', 'DriversController@store');
Route::get('driver/edit/{slug}', 'DriversController@edit');
Route::patch('driver/edit/{slug}', 'DriversController@update');
Route::delete('driver/delete/{id}', 'DriversController@delete');
Route::get('driver/getList', 'DriversController@getDatatable');
Route::post('get/driver-documents', 'DriversController@getDocuments');
Route::get('download/driver-document/{id}', 'DriversController@downloadFile');

            ////////////////////////
            ///Asssignments////////
            ///////////////////////

//Asssignments

Route::get('assignments/add', 'AssignmentsController@create');
Route::post('assignments/add', 'AssignmentsController@store');
Route::get('assignments/{user_id?}', 'AssignmentsController@index');
Route::get('assignments/edit/{slug}', 'AssignmentsController@edit');
Route::patch('assignments/edit/{slug}', 'AssignmentsController@update');
Route::delete('assignments/delete/{id}', 'AssignmentsController@delete');
Route::get('assignments/getList/{user_id}', 'AssignmentsController@getDatatable');
Route::post('get/class/students', 'AssignmentsController@getClassStudents');
Route::get('download/assignment/{slug}', 'AssignmentsController@downloadFile');
Route::get('view-assignment/uploads/{slug}', 'AssignmentsController@viewAssignment');
Route::post('approve/assignment', 'AssignmentsController@appoveAssignment');
Route::post('approve/single-assignment', 'AssignmentsController@appoveSingleAssignment');

//STAFF MODULE CLASS NOTES

Route::get('staff/getClassnotesList', 'AssignmentsController@getClassnotesDataTable');
Route::get('staff/classnotes', 'AssignmentsController@classnotesindex');
Route::get('staff/classnotes/add', 'AssignmentsController@classnotescreate');
Route::post('staff/classnotes/add', 'AssignmentsController@classnotesstore');
Route::get('staff/classnotes//edit/{slug}', 'AssignmentsController@classnotesedit');
Route::post('staff/classnotes//update/{slug}', 'AssignmentsController@classnotesupdate');

//Student Side Assignment Module
Route::get('student/assignments/{slug?}', 'StudentAssignmentsController@index');
Route::get('get/student/assignments/{slug}', 'StudentAssignmentsController@getDataTable');
Route::get('vew/student/assignment/{allocate_id}', 'StudentAssignmentsController@viewAssignment');
Route::post('student/upload/assignment', 'StudentAssignmentsController@uploadAssignment');
Route::get('student/download/assignment/{id}', 'StudentAssignmentsController@downloadFile');

//Student module subject book
Route::get('student/getstudSubjectookList', 'StudentController@getstudsubjectbookDataTable');
Route::get('student/subjectbooks', 'StudentController@studsubjectbookindex');
Route::get('student/download/subjectbooks/{slug}', 'StudentController@downloadFile');

         //////////////////////////
        // User Notifications Module
        /////////////////////////

Route::get('user/notifications', 'UserNotificationsController@index');
Route::delete('user/notifications/delete/{slug}', 'UserNotificationsController@delete');
Route::get('user/notifications/getList', 'UserNotificationsController@getDatatable');
Route::get('user/notifications/show/{slug}', 'UserNotificationsController@display');

//Visitor Management
Route::get('all-visitors/{id?}', 'VisitorManagementController@index');
Route::get('visitors/history', 'VisitorManagementController@getDatatable');
Route::post('add-visitor', 'VisitorManagementController@store');
Route::post('get-meet/users', 'VisitorManagementController@getUsers');
Route::patch('update/visitor-details/{id}', 'VisitorManagementController@update');
Route::post('delete/visitor-details', 'VisitorManagementController@delete');
Route::post('visitor/details', 'VisitorManagementController@visitorDetails');
Route::post('visitor/logout', 'VisitorManagementController@logoutVisitor');


             ////////////////////////
            ///Asset Management////
            ///////////////////////
            
//Asset Location
Route::get('asset-location', 'AssetLocationController@index');
Route::get('asset-location/add', 'AssetLocationController@create');
Route::post('asset-location/add', 'AssetLocationController@store');
Route::get('asset-location/edit/{slug}', 'AssetLocationController@edit');
Route::patch('asset-location/edit/{slug}', 'AssetLocationController@update');
Route::delete('asset-location/delete/{id}', 'AssetLocationController@delete');
Route::get('asset-location/getList', 'AssetLocationController@getDatatable');

//Asset Category
Route::get('asset-category', 'AssetCategoryController@index');
Route::get('asset-category/add', 'AssetCategoryController@create');
Route::post('asset-category/add', 'AssetCategoryController@store');
Route::get('asset-category/edit/{slug}', 'AssetCategoryController@edit');
Route::patch('asset-category/edit/{slug}', 'AssetCategoryController@update');
Route::delete('asset-category/delete/{id}', 'AssetCategoryController@delete');
Route::get('asset-category/getList', 'AssetCategoryController@getDatatable');


//Asset Vendor
Route::get('asset-vendor', 'AssetVendorController@index');
Route::get('asset-vendor/add', 'AssetVendorController@create');
Route::post('asset-vendor/add', 'AssetVendorController@store');
Route::get('asset-vendor/edit/{slug}', 'AssetVendorController@edit');
Route::patch('asset-vendor/edit/{slug}', 'AssetVendorController@update');
Route::delete('asset-vendor/delete/{id}', 'AssetVendorController@delete');
Route::get('asset-vendor/getList', 'AssetVendorController@getDatatable');

//Asset Items
Route::get('asset-items', 'AssetItemController@index');
Route::get('asset-items/add', 'AssetItemController@create');
Route::post('asset-items/add', 'AssetItemController@store');
Route::get('asset-items/edit/{slug}', 'AssetItemController@edit');
Route::patch('asset-items/edit/{slug}', 'AssetItemController@update');
Route::delete('asset-items/delete/{id}', 'AssetItemController@delete');
Route::get('asset-items/getList', 'AssetItemController@getDatatable');

//Asset Purchase
Route::get('asset-purchase', 'AssetPurchaseController@index');
Route::get('asset-purchase/add', 'AssetPurchaseController@create');
Route::post('asset-purchase/add', 'AssetPurchaseController@store');
Route::get('asset-purchase/edit/{slug}', 'AssetPurchaseController@edit');
Route::patch('asset-purchase/edit/{slug}', 'AssetPurchaseController@update');
Route::delete('asset-purchase/delete/{id}', 'AssetPurchaseController@delete');
Route::get('asset-purchase/getList', 'AssetPurchaseController@getDatatable');

//Asset Assign
Route::get('asset-assign', 'AssetAssignController@index');
Route::get('asset-assign/add', 'AssetAssignController@create');
Route::post('asset-assign/add', 'AssetAssignController@store');
Route::get('asset-assign/edit/{slug}', 'AssetAssignController@edit');
Route::patch('asset-assign/edit/{slug}', 'AssetAssignController@update');
Route::delete('asset-assign/delete/{id}', 'AssetAssignController@delete');
Route::get('asset-assign/getList', 'AssetAssignController@getDatatable');
Route::post('get/role-users', 'AssetAssignController@getUsers');



             /////////////////////////////
            ///Certificate Verification////
            //////////////////////////////
            
//Admin Notifiacations
Route::get('certificate-notification', 'CNotificationController@index');
Route::get('certificate-notification/add', 'CNotificationController@create');
Route::post('certificate-notification/add', 'CNotificationController@store');
Route::get('certificate-notification/edit/{slug}', 'CNotificationController@edit');
Route::patch('certificate-notification/edit/{slug}', 'CNotificationController@update');
Route::delete('certificate-notification/delete/{id}', 'CNotificationController@delete');
Route::get('certificate-notification/getList', 'CNotificationController@getDatatable');

//Certificates Manage
Route::get('upload/certificates/{notification_id}/{user_id?}', 'CertificatesManageController@index');
Route::post('store/certificates', 'CertificatesManageController@store');
Route::get('user/uploads/{role_id}/{user_id}', 'CertificatesManageController@userUploads');
Route::get('get/uploaded-certificates/{role_id}/{user_id}', 'CertificatesManageController@getDatatable');
Route::get('view/uploaded-certificates/{slug}/{user_id}', 'CertificatesManageController@viewCertificates');
Route::get('download/user-certificate/{image}', 'CertificatesManageController@downloadCertificate');
Route::post('approve/user-certificates', 'CertificatesManageController@approveCertificates');


//Inventory Category

Route::get('inventory/category/list', 'InventoryCategoryController@index');
Route::get('inventory/category', 'InventoryCategoryController@index');
Route::get('inventory/category/add', 'InventoryCategoryController@create');
Route::post('inventory/category/add', 'InventoryCategoryController@store');
Route::get('inventory/category/edit/{slug}', 'InventoryCategoryController@edit');
Route::patch('inventory/category/edit/{slug}', 'InventoryCategoryController@update');
Route::delete('inventory/category/delete/{slug}', 'InventoryCategoryController@delete');
Route::get('inventory/category/getList', 'InventoryCategoryController@getDatatable');

Route::get('inventory/store/list', 'InventoryStoreController@index');
Route::get('inventory/store', 'InventoryStoreController@index');
Route::get('inventory/store/add', 'InventoryStoreController@create');
Route::post('inventory/store/add', 'InventoryStoreController@store');
Route::get('inventory/store/edit/{slug}', 'InventoryStoreController@edit');
Route::patch('inventory/store/edit/{slug}', 'InventoryStoreController@update');
Route::delete('inventory/store/delete/{slug}', 'InventoryStoreController@delete');
Route::get('inventory/store/getList', 'InventoryStoreController@getDatatable');


//Inventory ItemSupplier
Route::get('inventory/itemssupplier/list', 'InventoryItemSupplierController@index');
Route::get('inventory/itemssupplier', 'InventoryItemSupplierController@index');
Route::get('inventory/itemssupplier/add', 'InventoryItemSupplierController@create');
Route::post('inventory/itemssupplier/add', 'InventoryItemSupplierController@store');
Route::get('inventory/itemssupplier/edit/{slug}', 'InventoryItemSupplierController@edit');
Route::patch('inventory/itemssupplier/edit/{slug}', 'InventoryItemSupplierController@update');
Route::delete('inventory/itemssupplier/delete/{slug}', 'InventoryItemSupplierController@delete');
Route::get('inventory/itemssupplier/getList', 'InventoryItemSupplierController@getDatatable');

//Inventory Item
Route::get('inventory/item/list', 'InventoryItemController@index');
Route::get('inventory/item', 'InventoryItemController@index');
Route::get('inventory/item/add', 'InventoryItemController@create');
Route::post('inventory/item/add', 'InventoryItemController@store');
Route::get('inventory/item/edit/{slug}', 'InventoryItemController@edit');
Route::patch('inventory/item/edit/{slug}', 'InventoryItemController@update');
Route::delete('inventory/item/delete/{slug}', 'InventoryItemController@delete');
Route::get('inventory/item/getList', 'InventoryItemController@getDatatable');

//Inventory Item_Stock
Route::get('inventory/itemstock/list', 'InventoryItemStockController@index');
Route::get('inventory/itemstock', 'InventoryItemStockController@index');
Route::get('inventory/itemstock/add', 'InventoryItemStockController@create');
Route::post('inventory/itemstock/add', 'InventoryItemStockController@store');
Route::get('inventory/itemstock/edit/{slug}', 'InventoryItemStockController@edit');
Route::patch('inventory/itemstock/edit/{slug}', 'InventoryItemStockController@update');
Route::delete('inventory/itemstock/delete/{slug}', 'InventoryItemStockController@delete');
Route::get('inventory/itemstock/getList', 'InventoryItemStockController@getDatatable');
//Route::post('public/uploads/inventory', 'InventoryItemStockController@itemFile');

Route::get('inventory/issueitem/list', 'InventoryIssueItemController@index');
Route::get('inventory/issueitem', 'InventoryIssueItemController@index');
Route::get('inventory/issueitem/add', 'InventoryIssueItemController@create');
Route::post('inventory/issueitem/add', 'InventoryIssueItemController@store');
Route::get('inventory/issueitem/edit/{slug}', 'InventoryIssueItemController@edit');
Route::patch('inventory/issueitem/edit/{slug}', 'InventoryIssueItemController@update');
Route::delete('inventory/issueitem/delete/{slug}', 'InventoryIssueItemController@delete');
Route::get('inventory/issueitem/getList', 'InventoryIssueItemController@getDatatable');
Route::post('inventory/get-items', 'InventoryIssueItemController@getItems');
Route::post('inventory/get-users', 'InventoryIssueItemController@getUsers');
Route::post('inventory/item/return', 'InventoryIssueItemController@returnItem');


                     //////////////////////
                     ////////Alumini//////
                    //////////////////////


//Events
Route::get('alumni/events/list', 'AlumniEventController@index');
Route::get('alumni/events/add', 'AlumniEventController@create');
Route::post('alumni/events/add', 'AlumniEventController@store');
Route::get('alumni/events/edit/{slug}', 'AlumniEventController@edit');
Route::patch('alumni/events/edit/{slug}', 'AlumniEventController@update');
Route::delete('alumni/events/delete/{slug}', 'AlumniEventController@delete');
Route::get('alumni/events/getList', 'AlumniEventController@getDatatable');

//Stories
Route::get('alumni/stories/list', 'AlumniStoryController@index');
Route::get('alumni/stories/add', 'AlumniStoryController@create');
Route::post('alumni/stories/add', 'AlumniStoryController@store');
Route::get('alumni/stories/edit/{slug}', 'AlumniStoryController@edit');
Route::patch('alumni/stories/edit/{slug}', 'AlumniStoryController@update');
Route::delete('alumni/stories/delete/{slug}', 'AlumniStoryController@delete');
Route::get('alumni/stories/getList', 'AlumniStoryController@getDatatable');


//Notices
Route::get('alumni/notice-board/list', 'AlunmiNoticeController@index');
Route::get('alumni/notice-board/add', 'AlunmiNoticeController@create');
Route::post('alumni/notice-board/add', 'AlunmiNoticeController@store');
Route::get('alumni/notice-board/edit/{slug}', 'AlunmiNoticeController@edit');
Route::patch('alumni/notice-board/edit/{slug}', 'AlunmiNoticeController@update');
Route::delete('alumni/notice-board/delete/{slug}', 'AlunmiNoticeController@delete');
Route::get('alumni/notice-board/getList', 'AlunmiNoticeController@getDatatable');


//Gallery
Route::get('alumni/gallery/list', 'AlumniGalleryController@index');
Route::get('alumni/gallery/add', 'AlumniGalleryController@create');
Route::post('alumni/gallery/add', 'AlumniGalleryController@store');
Route::get('alumni/gallery/edit/{slug}', 'AlumniGalleryController@edit');
Route::patch('alumni/gallery/edit/{slug}', 'AlumniGalleryController@update');
Route::delete('alumni/gallery/delete/{slug}', 'AlumniGalleryController@delete');
Route::get('alumni/gallery/getList', 'AlumniGalleryController@getDatatable');
Route::get('alumni/gallery/view/{id}', 'AlumniGalleryController@viewPhotos');
Route::get('alumni/gallery-delete-image/{id}', 'AlumniGalleryController@deleteGalImage');

//Notices
Route::get('alumni/donations/list', 'AlumniDonationController@index');
Route::get('alumni/donations/add', 'AlumniDonationController@create');
Route::post('alumni/donations/add', 'AlumniDonationController@store');
Route::get('alumni/donations/edit/{slug}', 'AlumniDonationController@edit');
Route::patch('alumni/donations/edit/{slug}', 'AlumniDonationController@update');
Route::delete('alumni/donations/delete/{slug}', 'AlumniDonationController@delete');
Route::get('alumni/donations/getList', 'AlumniDonationController@getDatatable');

//Alumni Users
Route::get('alumni/users/list', 'AlumniUsersController@index');
Route::get('alumni/users/add', 'AlumniUsersController@create');
Route::post('alumni/users/add', 'AlumniUsersController@store');
Route::get('alumni/users/edit/{slug}', 'AlumniUsersController@edit');
Route::patch('alumni/users/edit/{slug}', 'AlumniUsersController@update');
Route::delete('alumni/users/delete/{slug}', 'AlumniUsersController@delete');
Route::get('alumni/users/getList', 'AlumniUsersController@getDatatable');


//Alumni Users Side
Route::get('alumni/login', 'SiteController@login');
Route::post('alumni/register', 'SiteController@register');
Route::get('alumini-profile/{user_id}', 'AluminiController@profile');
Route::post('alumini-update-profile', 'AluminiController@updateProfile');

Route::get('events', 'AluminiController@alumniEvents');
Route::get('event-info/{id}', 'AluminiController@eventInfo');
Route::get('stories', 'AluminiController@stories');
Route::get('story-info/{id}', 'AluminiController@storyInfo');
Route::post('user-comment', 'AluminiController@userComment');
Route::get('gallery', 'AluminiController@gallery');
Route::get('alumni-notices', 'AluminiController@notices');
Route::get('volunteers', 'AluminiController@volunteers');
Route::get('alumni-contact', 'AluminiController@contact');
Route::post('alumni-sendcontact', 'AluminiController@ContactUs');
Route::get('search/alumni-users', 'AluminiController@search');
Route::get('alumni-details/{user_id}', 'AluminiController@viewAlumniDetails');
Route::post('get/search/alumni-users', 'AluminiController@getAlumni');

Route::get('donations', 'AluminiController@donations');
Route::get('donate/amount/{donation_id}', 'AluminiController@paynow');
Route::post('donation/success', 'AluminiController@donationSuccess');
Route::get('donation/fail', 'AluminiController@donationFail');
Route::get('user-donations/{user_id}', 'AluminiController@subscriptions');

Route::post('status-alumni', 'AlumniUsersController@changeStatus');

Route::get('user-hostel-details/{user_id}', 'HostelAssignController@hostelDetails');
Route::get('user-health-details/{user_id}', 'UsersController@healthDetails');
Route::get('user-transport-details/{user_id}', 'TransportFeeAssignController@transportDetails');

Route::get('donations-list', 'AluminiController@userDonations');
Route::get('donations-list/getList', 'AluminiController@getUserDoantions');