<?php

use Illuminate\Http\Request;

header('Access-Control-Allow-Origin:  *');
header('Access-Control-Allow-Methods:  POST, GET, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Headers:  Content-Type, X-Auth-Token, Origin, Authorization');

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::get('/user', function (Request $request) {
//     return $request->user();
// })->middleware('auth:api');


Route::group([ 'prefix' => 'v1/'], function () {



    //Driver Trip Process
    Route::post('sendTripPushNotification', 'PushnotificationsController@sendTripPushNotification');
    Route::post('getTripData', 'PushnotificationsController@getTripData');
    //END
    Route::post('login', 'Auth\LoginController@authenticate');
    Route::post('logout', 'Auth\LoginController@applogout');
    Route::post('users/reset-password', 'API\OthersController@resetUsersPasswordAPI');
    Route::post('user/update-password', 'API\OthersController@updatePassword');
    Route::post('feedback/send', 'API\OthersController@saveFeedBack');

    //Notification
    Route::get('notifications', 'API\OthersController@notifications');    
    Route::get('getAllNotifications/{id}', 'API\OthersController@getAllNotifications');
    Route::get('getIndNotifications/{id}', 'API\OthersController@getIndNotifications');
    Route::delete('deleteNotifications/{id}', 'API\OthersController@deleteNotifications');
    Route::delete('deleteAllNotifications/{id}', 'API\OthersController@deleteAllNotifications');
    Route::get('updateReadNotifications/{id}', 'API\OthersController@updateReadNotification');

    //Online Class
    Route::get('student/get_online_class_list/{student_id}', 'API\OthersController@getStudentDatatable');
    
    Route::get('user/profile/{id}', 'API\UsersController@profile');
    Route::post('users/edit/{id}', 'API\UsersController@update');

    //Settings
    Route::get('user/settings/{user_id}', 'API\UsersController@settings');
    // Route::post('users/settings/{slug}', 'API\UsersController@updateSettings');
    Route::post('users/settings', 'API\UsersController@updateSettings');
    
    Route::get('user/bookmarks/{id}', 'API\ExamsController@bookmarks');
    Route::post('bookmarks/delete/{bookmark_id}', 'API\ExamsController@deleteBookmarkById');
    Route::post('profile-image', 'API\UsersController@uploadUserProfileImage');
    Route::get('user/subscriptions/{id}', 'API\PaymentsController@paymentsHistory');
    //LMS
    Route::get('library/history/{id}', 'API\LibraryController@issueHistory');
    Route::get('library/history-returns/{id}', 'API\LibraryController@libraryReturns');
    Route::get('library/book-details/{issue_id}', 'API\LibraryController@getBookDetails');

    Route::get('library/Ebook-details', 'API\LibraryController@getEbookLibDetails');

    // Route::get('user/day-timetable/{id}', 'API\GeneralController@getStudentDayClasses');

    Route::post('user/day-timetable', 'API\GeneralController@getStudentDayClasses');

    Route::get('user/staff-profile/{user_id}', 'API\GeneralController@getStaffDetails');
    Route::post('subject/lesson-plans', 'API\GeneralController@SubjectLessonPlans');
    Route::get('users/fee-schedules/{student_id}/{feecat_id}', 'API\FeeController@feeShedulesView');
    Route::get('users/fee-history/{student_id}', 'API\FeeController@feePaidHistoryView');

    Route::get('users/fee-details/{student_id}/{type}', 'API\FeeController@feeUserData');
    Route::get('user/test-timetable/{id}', 'API\GeneralController@testStudentTimetable');
    Route::post('bookmark-remove', 'API\GeneralController@deleteBookMark');

    //LMS Categories and Series
    Route::post('lms-IndCategories', 'API\LMSController@lmsIndCategories');
    Route::post('lms-categories', 'API\LMSController@lmsCategories');
    Route::post('lms-series', 'API\LMSController@lmsSeries');
    
    Route::post('lms/series/{slug}/{content_slug?}', 'API\LMSController@viewItem');
    Route::get('lms/series-items/{slug}', 'API\LMSController@SeriesItems');


    // New
    Route::post('lms/{slug}', 'API\LMSController@viewCategoryItems');


    Route::post('exams/{type}', 'API\ExamsController@exams');
    Route::get('exams/student/get-scheduled-exams/{user_id}', 'API\ExamsController@scheduledExams');

    Route::get('exam-series/{user_id}', 'API\ExamsController@examSeries');
    
    Route::post('exams/student-exam-series/{slug}', 'API\ExamsController@viewSeriesItem');

    Route::get('instructions/{exam_slug}', 'API\ExamsController@instructions');
    Route::post('get-exam-questions/{slug}', 'API\ExamsController@getQuestions');
    Route::post('finish-exam/{slug}', 'API\ExamsController@finishExam');
    Route::get('get-exam-key/{slug}', 'API\ExamsController@getExamKey');
    Route::post('bookmarks/save', 'API\ExamsController@saveBookmarks');

    //Exam categories
    Route::get('exam/categories/{user_id}', 'API\ExamsController@examCategories');
    Route::get('exam/indCategories/{id}', 'API\ExamsController@examIndCategories');
    
    Route::get('analysis/history/{user_id}', 'API\ExamsController@historyAnalysis');
    Route::get('analysis/exam/{user_id}', 'API\ExamsController@examAnalysis');
    Route::get('analysis/subject/{user_id}', 'API\ExamsController@subjectAnalysis');

    Route::get('gateway/details', 'API\PaymentsController@gatewayDetails');

    Route::post('validate/coupon', 'API\OthersController@validateCoupon');
    Route::post('save-transaction', 'API\PaymentsController@saveTransaction');

    //paypal,payu
    Route::post('save-offline-payment', 'API\PaymentsController@saveOfflinePayment');
    //offline
    Route::post('save-stripe-payment', 'API\PaymentsController@saveStripePayment');//stripe
    Route::post('save-patm-payment', 'API\PaymentsController@savePaytmPayment');//paytm

    Route::get('user-assignments/{user_id}', 'API\UserAssignmentsController@getAssignments');
    Route::get('assignment-details/{id}', 'API\UserAssignmentsController@viewAssignment');
    Route::post('upload-assignment', 'API\UserAssignmentsController@uploadAssignment');

    Route::post('academic-category-results', 'API\OthersController@getExamCategories');
    Route::post('get-category-examresults', 'API\OthersController@getExamsByCategory');
    Route::post('get-user-subjects', 'API\OthersController@getStudentSubjects');
    Route::get('completed/subject-topics/{slug}', 'API\OthersController@getSubjectTopics');
    Route::post('student-attendance', 'API\OthersController@getAttendance');

    Route::post('get-student-attendance', 'API\OthersController@getStudentAttendance');

    Route::get('get-offline-exams-categories/{id}','API\OthersController@getOfflineExamsCategories');
    Route::post('get-class-marks','API\OthersController@getClassMarks');

    Route::post('student-fee-update','API\FeePayStudentController@updateFeePayment');
    Route::post('student-offline-fee-update','API\FeePayStudentController@offlinePayment');

    //Tracking
    Route::get('tracking/{user_id}','API\ParentsController@getTrack');


 //PARENT API CALLS
    Route::get('parent/getchildren/{user_id}','API\ParentsController@getChildren');
    Route::get('parent/subscriptions/{user_id}','API\ParentsController@subscriptions');

    Route::get('parent/profile/{id}', 'API\UsersController@parentProfile');

    Route::post('parent/getpaychildren','API\ParentsController@getPayChildren');

    Route::get('get-currency', 'API\GeneralController@getCurrency');

    Route::get('get-payment-gateways', 'API\GeneralController@getPaymentGateways');

    Route::get('parent/getStaff/{user_id}','API\ParentsController@getStaffs');
    Route::get('parent/getchildrenId/{user_id}','API\ParentsController@getchildrenId');
    Route::get('parent/getchildrenHosattendance/{user_id}','API\ParentsController@getchildrenHosattendance');
    //New Call in(07-08)
        //Student Medical Treatment Call's
        Route::post('parent/add_std_medical_treat','API\ParentsController@add_std_medical_treat');
        Route::post('parent/update_std_medical_treat','API\ParentsController@update_std_medical_treat');
        Route::get('parent/remove_std_medical_treat/{treatment_id}','API\ParentsController@remove_std_medical_treat');
        Route::get('parent/get_std_medical_treat/{parent_id}','API\ParentsController@get_std_medical_treat');
        Route::get('parent/get_std_all_medical_treat/{student_id}','API\ParentsController@get_std_all_medical_treat');
        Route::get('parent/get_ind_std_medical_treat/{treatment_id}','API\ParentsController@get_ind_std_medical_treat');
        //Student Allergy food Call's
        Route::post('parent/add_std_allergy_food','API\ParentsController@add_std_allergy_food');
        Route::post('parent/update_std_allergy_food','API\ParentsController@update_std_allergy_food');
        Route::get('parent/remove_std_allergy_food/{allergy_food_id}','API\ParentsController@remove_std_allergy_food');
        Route::get('parent/get_std_allergy_food/{parent_id}','API\ParentsController@get_std_allergy_food');
        Route::get('parent/get_std_all_allergy_food/{student_id}','API\ParentsController@get_std_all_allergy_food');
        Route::get('parent/get_ind_std_allergy_food/{allergy_food_id}','API\ParentsController@get_ind_std_allergy_food');
        //STUDENT HEALTH INFO Call's
        Route::post('parent/add_std_health','API\ParentsController@add_std_health');
        Route::post('parent/update_std_health','API\ParentsController@update_std_health');
        Route::get('parent/remove_std_health/{studhealth_id}','API\ParentsController@remove_std_health');
        Route::get('parent/get_std_health/{parent_id}','API\ParentsController@get_std_health');
        Route::get('parent/get_std_all_health/{student_id}','API\ParentsController@get_std_all_health');
        Route::get('parent/get_ind_std_health/{studhealth_id}','API\ParentsController@get_ind_std_health');
 //STAFF API CALLS
    Route::get('staff_classes/{user_id}', 'API\StaffController@staffClasses');
    Route::get('staff/lession-plans/view-students/get-list/{academic_id}/{course_parent_id}/{course_id}/{year}/{semister}', 'API\StaffController@getDatatable');
    //Staff subject preference
    Route::get('subjects','API\OthersController@getAllSubjects');
    Route::get('staff/subjects/preferences/{user_id}','API\StaffController@subjectPreferences');
    Route::post('staff/subjects/update_preferences/{user_id}','API\StaffController@update');
    //Staff Attendance Enter Student Attendance
    Route::post('get/staff_class_students', 'API\StaffController@getClassStudents');
    Route::post('add/class_attendance', 'API\StaffController@updateAttendance');
    //Lesson  plan
    Route::get('staff/lesson_plans/{user_id}', 'API\StaffController@lessonPlans');
    Route::get('staff/lession-plans/view-topics/{user_id}/{subject_slug}', 'API\StaffController@viewTopics');
    Route::post('staff/lession-plans/update-topic', 'API\StaffController@updateLessionTopic');
    Route::post('staff/update-topic', 'API\StaffController@updateTopic');
  
    //Assignment
    Route::get('staff/assignments/{user_id}', 'API\StaffController@staffAssignments');
    Route::get('staff/getStaffClassSubject/{user_id}', 'API\StaffController@getStaffClassSubject');
    Route::post('staff/assignment-allocated-students', 'API\StaffController@getAssignmentStudents');
    Route::post('staff/store-assignment', 'API\StaffController@storeAssignment');
    Route::get('staff/view-assignment/{assignment_id}', 'API\StaffController@viewAssignment');
    Route::post('staff/single-assignment-approve', 'API\StaffController@appoveSingleAssignment');
    Route::post('staff/approve-assignment', 'API\StaffController@appoveAssignment');
    Route::get('staff/edit-assignment/{id}', 'API\StaffController@editAssignment');
    Route::post('staff/udpate-assignment/{id}', 'API\StaffController@updateAssignment');
    Route::delete('staff/delete-assignment/{id}', 'API\StaffController@deleteAssignment');


    //Time Table
    Route::get('user/timetable/{id}', 'API\GeneralController@staffStudentTimetable');

    Route::get('staff/add-assignment/{user_id}', 'API\StaffController@addAssignment');
    //Certificate
    Route::post('store/certificates', 'API\StaffController@store');
    Route::get('user/uploadsDoc/{role_id}/{user_id}', 'API\StaffController@userUploadsdoc');

    //Chatting For Staff
    Route::post('chat/getStudentsandParentsId', 'API\StaffController@getStudentsandParentsId');

    Route::get('staff/profile/{id}', 'API\UsersController@staffProfile');


    //Driver API CALLS
    /*
    Driver login and trip and profile update and manual entry and emergency and report api calls..
    */
    Route::post('driverLogin', 'API\DriverController@driverLogin');
    Route::post('verifyLoginOTP', 'API\DriverController@verifyLoginOTP');
    Route::get('driverDetaileFetch/{id}', 'API\DriverController@driverDetaileFetch');
    Route::get('driverTripStudent/{id}', 'API\DriverController@driverTripStudent');
    Route::get('driverRoute/{id}', 'API\DriverController@driverRoute');

    // New API Calls

    /* Fetch Children With Pagination */
    Route::get('parent/get_parent_children/{user_id}','API\ParentsController@getParentChildren');

    // Student
    
    /* Class Leave Management */
    Route::post('applyLeaveRequest', 'API\LeaveController@applyLeave');
    Route::get('parent/get_leavelist/{user_id}','API\LeaveController@getChildrenLeaveRequest');
    Route::get('student/get_leavelist/{user_id}','API\LeaveController@getStudentLeaveRequest');
    Route::get('staff/get_leavelist/{user_id}','API\LeaveController@getStaffLeaveRequest');

    Route::post('acceptOrRejectLeave', 'API\LeaveController@updateLeaveStatus');
    
    /* Hostal Leave Management */
    Route::post('applyHostalLeaveRequest', 'API\LeaveController@applyHostalLeave');

    Route::get('parent/get_hostal_leave/{user_id}','API\LeaveController@getParentHostalLeaveRequest');
    Route::get('student/get_hostal_leave/{user_id}','API\LeaveController@getStudentHostalLeaveRequest');

    /* Subject Books Management */
    Route::get('student/get_subject_books/{user_id}','API\ExamsController@getStudentSubjectBooks');
    Route::get('getNotificationCount/{id}', 'API\OthersController@getNotificationCount');

    /* Class Leave Management */
    // In Staff Side
    Route::post('staff/apply_leave', 'API\LeaveController@applyStaffLeave');
    Route::get('staff/get_leave/{user_id}','API\LeaveController@getStaffNewLeaveRequest');
    Route::post('staff/add_class_notes', 'API\ClassNotesController@addStaffClassNotes');
    Route::get('staff/get_class_notes/{staff_id}', 'API\ClassNotesController@getStaffClassNotes');
    Route::post('staff/update_class_notes/{class_notes_id}', 'API\ClassNotesController@updateStaffClassNotes');
    Route::get('staff/delete_class_notes/{class_notes_id}', 'API\ClassNotesController@deleteStaffClasNotes');

    // In Student Side
    Route::get('student/get_class_notes/{student_id}', 'API\ClassNotesController@getStudentClassNotes');
    Route::get('student/get_all_allocated_students/{class_notes_id}', 'API\ClassNotesController@getAllocatedStudentClassNotes');

    // Fetch Student Hostal Attendance in Parent Side
    Route::get('parnet/get_student_hostal_attendance/{parent_id}', 'API\ClassNotesController@getHostalAttendance');

    /* Event Management */
    
    // In Staff Side
    Route::get('staff/get_events_category/{staff_id}', 'API\EventsController@getStaffEventsCategory');
    Route::get('staff/get_events/{category_id}', 'API\EventsController@getStaffEvents');
    Route::get('get_all_academic', 'API\EventsController@getAllAcademicYear');
    Route::get('staff/get_academic_course/{acadminc_id}', 'API\EventsController@getAcademicYearBasedCourse');
    Route::post('staff/get_student_list', 'API\EventsController@getCourseBasedStudentList');
    Route::post('staff/assign_student_event', 'API\EventsController@assignStudentsEvent');
    Route::post('staff/get_students', 'API\EventsController@getEventsBasedStudents');
    Route::get('staff/get_staff_requested_events/{staff_id}', 'API\EventsController@getStaffRequestedEvents');
    Route::post('staff/accept_reject_event_request', 'API\EventsController@updateRequestEventStatus');
    
    
    
    // In Student Side
    Route::get('student/get_assigned_events/{student_id}', 'API\EventsController@getStudentEvents');
    Route::get('get_all_catagory', 'API\EventsController@getEventsCategory');
    Route::get('get_category_events/{category_id}', 'API\EventsController@getCategoryBasedEvents');
    Route::post('send_event_requests', 'API\EventsController@sendEventReuest');
    Route::get('get_student_requested_events/{student_id}', 'API\EventsController@getStudentRequestedEvents');
    
    
    
    
    Route::post('staff/students_count', 'API\StaffController@getStudentsCount');
    
    
});
