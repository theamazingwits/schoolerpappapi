var chaturl = "http://localhost:3001/";
	var serverurl = "http://localhost/";

	var socket = io.connect(chaturl);

//socket.connect(); 

// Add a connect listener
	socket.on('connect',function() {
	  console.log('Client has connected to the server!');
	});
	// Add a connect listener
	socket.on('message',function(data) {
	  console.log('Received a message from the server!',data);
	});
	// Add a disconnect listener
	socket.on('disconnect',function() {
	  console.log('The client has disconnected!');
	});
	function newmessage(){
		var userdetails = JSON.parse($('#user').text());
		var receiverId = $('#groupid').text();
		var msg = document.getElementById('message').value;
		var data = {
			senderId: userdetails.id,
		    senderName:userdetails.name,
		    //receiverId:Array,
		    groupId: receiverId, // Group Id for Reference
		    message: msg,
		    type: "1", //1-text msg, 2-image, 3-audio, 4-video, 5-pdf or doc
		    messageTime : moment().format("DD-MM-YY hh:mm A"),
		    filename: "",
		    filepath:"",
		}
		socket.emit("chating", data);  
	}
	function getStudents(){
		//console.log("type---->"+type);
		var courseid = document.getElementById("classlist").value;
		$.ajax({
              url: serverurl+"getStudentParentList",
              headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
              },
              type: "POST",
              data: {"course_subject_id":courseid},
              success: function(success) {
              	$('#members').html(JSON.stringify(success));
              	console.clear();
              },
              dataType: "json",
              timeout: 2000
        })
	}
	function getChatList(){
		var userdetails = JSON.parse($('#user').text());
		var data = {
			userid : userdetails.id
		}
		$.ajax({
              url: chaturl+"getChatList1",
              headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
              },
              type: "POST",
              data: data,
              success: function(success) {
              	var data = success.data;
              	var length = data.length;
              	$('#chatlist').empty();
              	for(var i =0;i<length;i++){
              		$('#chatlist').append('<div class="kt-widget__item"> <span class="kt-media kt-media--circle"> <img src="{{CHAT}}/assets/media/users/300_9.jpg" alt="image"> </span><div class="kt-widget__info"><div class="kt-widget__section"> <a href="javascript:void(0);" class="kt-widget__username" onclick="viewchat(\''+data[i]._id+'\',\''+data[i].groupName+'\')">'+data[i].groupName+'</a> <span class="kt-badge kt-badge--success kt-badge--dot"></span></div> <span class="kt-widget__desc"> </span></div><div class="kt-widget__action"> <span class="kt-widget__date">36 Mines</span></div></div>');
              	}
              	//$('#members').html(JSON.stringify(success));
              	//console.clear();
              	//swal(success.msg);
              	//$('#attendance_summary').modal('hide');
              },
              dataType: "json",
              timeout: 2000
        })
	}
	function viewchat(id,name){
		var userdetails = JSON.parse($('#user').text());
		$('#groupid').html(id);
		//var groupdetails = JSON.parse(id);
		var data = {
			senderId : id
		}
		$.ajax({
              url: chaturl+"getChatHistory",
              headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
              },
              type: "POST",
              data: data,
              success: function(success) {
              	var data = success.data;
              	var length = data.length;
              	$('#chatmessages').empty();
              	for(var i =0;i<length;i++){
              		if(data[i].senderId == userdetails.id){
              			$('#chatmessages').append('<div class="kt-chat__message kt-chat__message--right"><div class="kt-chat__user"> <span class="kt-media kt-media--circle kt-media--sm"> <img src="{{CHAT}}/assets/media/users/100_12.jpg" alt="image"> </span> <a href="#" class="kt-chat__username">You</span></a> <span class="kt-chat__datetime">'+data[i].createdAt+'</span></div><div class="kt-chat__text kt-bg-light-success"> '+data[i].message+'</div></div>');
              		}else{
              			$('#chatmessages').append('<div class="kt-chat__message"><div class="kt-chat__user"> <span class="kt-media kt-media--circle kt-media--sm"> <img src="{{CHAT}}/assets/media/users/100_12.jpg" alt="image"> </span> <a href="#" class="kt-chat__username">'+data[i].senderName+'</span></a> <span class="kt-chat__datetime">'+data[i].createdAt+'</span></div><div class="kt-chat__text kt-bg-light-success"> '+data[i].message+'</div></div>');
              		}
              	}
              	//$('#members').html(JSON.stringify(success));
              	//console.clear();
              	//swal(success.msg);
              	//$('#attendance_summary').modal('hide');
              },
              dataType: "json",
              timeout: 2000
        })
	}
	socket.on('refresh_feed',function(data){
		var userdetails = JSON.parse($('#user').text());
		if(data.senderId == userdetails.id){
  			$('#chatmessages').append('<div class="kt-chat__message kt-chat__message--right"><div class="kt-chat__user"> <span class="kt-media kt-media--circle kt-media--sm"> <img src="{{CHAT}}/assets/media/users/100_12.jpg" alt="image"> </span> <a href="#" class="kt-chat__username">You</span></a> <span class="kt-chat__datetime">'+data.createdAt+'</span></div><div class="kt-chat__text kt-bg-light-success"> '+data.message+'</div></div>');
  		}else{
  			$('#chatmessages').append('<div class="kt-chat__message"><div class="kt-chat__user"> <span class="kt-media kt-media--circle kt-media--sm"> <img src="{{CHAT}}/assets/media/users/100_12.jpg" alt="image"> </span> <a href="#" class="kt-chat__username">'+data.senderName+'</span></a> <span class="kt-chat__datetime">'+data.createdAt+'</span></div><div class="kt-chat__text kt-bg-light-success"> '+data.message+'</div></div>');
  		}	
	  
	});
	function createGroup(){
		//console.log("type---->"+type);
		//console.log($("#members").text());
		var groupname = document.getElementById('groupname').value;
		if(groupname == ""){
			swal("Please enter group name");
			return false;
		}
		var datalist = JSON.parse($("#members").text());		
		var userdetails = JSON.parse($('#user').text());
		var groupdetails = {
			groupName : groupname,
			createdByname: userdetails.name,
			createdById: userdetails.id, 
		}
		var groupmembers = [{
			memberid : userdetails.id,
			name : userdetails.name,
			role:"staff"
		}];
		for(var i =0;i<datalist.students.length;i++){
			groupmembers.push({
				memberid : datalist.students[i].id,
				name : datalist.students[i].name,
				role:"students"
			});
		}
		for(var i =0;i<datalist.parents.length;i++){
			groupmembers.push({
				memberid : datalist.parents[i].id,
				name : datalist.parents[i].name,
				role:"parents"
			});
		}
		if(groupmembers.length <= 1){
			swal("No Members in this class");
			return false;
		}
		var data = {
			groupdetails : groupdetails,
			groupmembers : groupmembers
		}
		//console.log(datalist);
		//return false;
		$.ajax({
              url: chaturl+"createGroup",
              headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
              },
              type: "POST",
              data: data,
              success: function(success) {
              	//$('#members').html(JSON.stringify(success));
              	//console.clear();
              	swal(success.msg);
              	$('#attendance_summary').modal('hide');
              	getChatList();
              },
              dataType: "json",
              timeout: 2000
        })

	}
	getChatList();