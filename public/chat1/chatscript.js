$('#eventstartdate').datetimepicker({
            todayHighlight: true,
            autoclose: true,
            start : moment(),
            format: 'yyyy.mm.dd hh:ii'
        });
	$('#eventenddate').datetimepicker({
            todayHighlight: true,
            autoclose: true,
            start : moment($('#eventstartdate').val()),
            format: 'yyyy.mm.dd hh:ii'
        });
	document.getElementById('kt_chat_content').style.display = 'none';
	document.getElementById('kt_chat_content1').style.display = 'block';
	document.getElementById('imgfoot').style.display = 'none';
	document.getElementById('textfoot').style.display = 'block';
	document.getElementById('eventoptionmenu').style.display = 'none';
	var chaturl = "http://192.168.0.117:3001/";
	var serverurl = "http://192.168.0.117/";
	var s3url = "https://s3.ap-south-1.amazonaws.com/dev-cls/Chat/";

	var socket = io.connect(chaturl);

//socket.connect(); 

// Add a connect listener
	socket.on('connect',function() {
	  console.log('Client has connected to the server!');
	});
	// Add a connect listener
	socket.on('message',function(data) {
	  console.log('Received a message from the server!',data);
	});
	// Add a disconnect listener
	socket.on('disconnect',function() {
	  console.log('The client has disconnected!');
	});
	function trigger(){
		$("#chatfile").trigger("click");
	}
	function fileget(event){
		console.log(event.target.files[0].name);
		var output = document.getElementById('chatfileview');
        output.src = URL.createObjectURL(event.target.files[0]);
        document.getElementById('imgfoot').style.display = 'block';
		document.getElementById('textfoot').style.display = 'none';
	}
	function cancelfile(){
		document.getElementById('imgfoot').style.display = 'none';
		document.getElementById('textfoot').style.display = 'block';
		$("#chatfile").val(null);
	}
	function newmessage(){
		var userdetails = JSON.parse($('#user').text());
		var receiverId = $('#groupid').text();
		var msg = document.getElementById('message').value;
		if(msg == ""){
			swal("Please enter msg");
			return false;
		}
		var data = {
			senderId: userdetails.id,
		    senderName:userdetails.name,
		    //receiverId:Array,
		    groupId: receiverId, // Group Id for Reference
		    message: msg,
		    type: "1", //1-text msg, 2-image, 3-audio, 4-video, 5-pdf or doc
		    messageTime : moment().format("DD-MM-YY hh:mm A"),
		    filename: "",
		    filepath:"",
		}
		socket.emit("chating", data);  
		$("#message").val('').empty();
		$(".customscroll").animate({ scrollTop: $('.customscroll').prop("scrollHeight")}, "fast");
	}
	function newmediamessage(){
		var userdetails = JSON.parse($('#user').text());
		var receiverId = $('#groupid').text();
		var doc = document.getElementById('chatfile');
		var file = doc.files[0];
        var reader = new FileReader();
        //console.log(document.getElementById('output_img').src());
        reader.onloadend = function() {
        console.log('RESULT', reader.result)
    		var data = {
				senderId: userdetails.id,
			    senderName:userdetails.name,
			    //receiverId:Array,
			    groupId: receiverId, // Group Id for Reference
			    message: "",
			    type: "2", //1-text msg, 2-image, 3-audio, 4-video, 5-pdf or doc
			    messageTime : moment().format("DD-MM-YY hh:mm A"),
			    filename: reader.result,
			    filepath:"",
			}
	      //console.log(message);
	      socket.emit("chating", data);  
	      
	      }
        reader.readAsDataURL(file); 
		$(".customscroll").animate({ scrollTop: $('.customscroll').prop("scrollHeight")}, "fast");
	}
	function getStudents(){
		//console.log("type---->"+type);
		var courseid = document.getElementById("classlist").value;
		$.ajax({
              url: "{{URL_GET_STUDENTS_PARENTS}}",
              headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
              },
              type: "POST",
              data: {"course_subject_id":courseid},
              success: function(success) {
              	$('#members').html(JSON.stringify(success));
              	console.clear();
              },
              dataType: "json",
              timeout: 2000
        })
	}
	function getStudents1(){
		//console.log("type---->"+type);
		var courseid = document.getElementById("classlist1").value;
		$.ajax({
              url: "{{URL_GET_STUDENTS_PARENTS}}",
              headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
              },
              type: "POST",
              data: {"course_subject_id":courseid},
              success: function(success) {
              	var data = [];
              	var data1 = [];
              	data = success.students;
              	data1 = success.parents;
              	length = data.length;
              	length1 = data1.length;
              	$('#chatstudentlist').empty();
              	for(var i =0;i<length;i++){
              		$('#chatstudentlist').append('<a href="javascript:void(0);" onclick="createChat(\''+data[i].id+'\',\''+data[i].name+'\')" class="kt-notification-v2__item"><div class="kt-notification-v2__item-icon"> <i class="flaticon-user kt-font-success"></i></div><div class="kt-notification-v2__itek-wrapper"><div class="kt-notification-v2__item-title"> '+data[i].name+'</div><div class="kt-notification-v2__item-desc"> '+data[i].roll_no+'</div></div> </a>');
              	}
              	$('#chatparentlist').empty();
              	for(var i =0;i<length1;i++){
              		$('#chatparentlist').append('<a href="javascript:void(0);" class="kt-notification-v2__item" onclick="createChat(\''+data[i].id+'\',\''+data[i].name+'\')"><div class="kt-notification-v2__item-icon"> <i class="flaticon-user kt-font-success"></i></div><div class="kt-notification-v2__itek-wrapper"><div class="kt-notification-v2__item-title"> '+data1[i].name+'</div></div> </a>');
              	}

              	//$('#members1').html(JSON.stringify(success));
              	console.clear();
              },
              dataType: "json",
              timeout: 2000
        })
	}
	function viewgroup(){
		var userdetails = JSON.parse($('#user').text());
		var groupid = $('#groupid').text();
		var type = $('#type').text();
		if(type == "1"){

			
			var data = {
			userid : userdetails.id,
			groupid : groupid,
			type : type
			}
			$.ajax({
	              url: chaturl+"getGroupMembers",
	              headers: {
	              'Content-Type': 'application/x-www-form-urlencoded'
	              },
	              type: "POST",
	              data: data,
	              success: function(success) {
	              	var data = success.data[0].groupdetails;
	              	var length = data.length;
	              	var checked;
	              	$('#chatmembers').empty();
	              	if(userdetails.role_id == "3"){
		              	for(var i=0;i<length;i++){
		              		if(data[i].status == "1"){
		              			checked = "checked";
		              		}else{
		              			checked = "";
		              		}
		              		if(data[i].role == "staff"){
		              			$('#chatmembers').append('<div class="kt-widget4__item"><div class="kt-widget4__pic kt-widget4__pic--logo"> <img src="{{CHAT}}/assets/media/users/300_9.jpg" alt=""></div><div class="kt-widget4__info"> <a href="#" class="kt-widget4__title"> '+data[i].name+' </a><p class="kt-widget4__text"> '+data[i].role+'</p></div> <span class="kt-widget4__number kt-font-brand">Group Admin </span></div>')

		              		}else{
		              			$('#chatmembers').append('<div class="kt-widget4__item"><div class="kt-widget4__pic kt-widget4__pic--logo"> <img src="{{CHAT}}/assets/media/users/300_9.jpg" alt=""></div><div class="kt-widget4__info"> <a href="#" class="kt-widget4__title"> '+data[i].name+' </a><p class="kt-widget4__text"> '+data[i].role+'</p></div> <span class="kt-widget4__number kt-font-brand"> <span class="kt-switch kt-switch--sm"> <label> <input type="checkbox" '+checked+' name="" id="checkbox'+i+'" onchange="activeblockgroup(\''+data[i].groupId+'\',\''+data[i].memberid+'\',\''+i+'\')"> <span></span> </label> </span> </span></div>')
		              		}
		              	}
	              	}else{
	              		for(var i=0;i<length;i++){
		              		if(data[i].status == "1"){
		              			checked = "checked";
		              		}else{
		              			checked = "";
		              		}
		              		if(data[i].role == "staff"){
		              			$('#chatmembers').append('<div class="kt-widget4__item"><div class="kt-widget4__pic kt-widget4__pic--logo"> <img src="{{CHAT}}/assets/media/users/300_9.jpg" alt=""></div><div class="kt-widget4__info"> <a href="#" class="kt-widget4__title"> '+data[i].name+' </a><p class="kt-widget4__text"> '+data[i].role+'</p></div> <span class="kt-widget4__number kt-font-brand">Group Admin </span></div>')

		              		}else{
		              			$('#chatmembers').append('<div class="kt-widget4__item"><div class="kt-widget4__pic kt-widget4__pic--logo"> <img src="{{CHAT}}/assets/media/users/300_9.jpg" alt=""></div><div class="kt-widget4__info"> <a href="#" class="kt-widget4__title"> '+data[i].name+' </a><p class="kt-widget4__text"> '+data[i].role+'</p></div> <span class="kt-widget4__number kt-font-brand"></span></div>')
		              		}
		              	}
	              	}
	              	$('#chatinfo').modal();
	              	
	              },
	              dataType: "json",
	              timeout: 2000
	        })

		}
	}
	function activeblockgroup(groupid,userid,status){
		var update;
		var userdetails = JSON.parse($('#user').text());

		if($('#checkbox'+status).is(":checked")){
			update = "1";
		}else{
			update = "2";
		}
		var data = {
			userid : userdetails.id,
			groupid : groupid,
			memberid : userid,
			updateobj : update
		}
		$.ajax({
              url: chaturl+"activeBlockGroup",
              headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
              },
              type: "POST",
              data: data,
              success: function(success) {
              	swal(success.msg);              	
              },
              dataType: "json",
              timeout: 2000
        })
	}
	function getChatList(){
		var userdetails = JSON.parse($('#user').text());
		var data = {
			userid : userdetails.id
		}
		$.ajax({
              url: chaturl+"getChatList1",
              headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
              },
              type: "POST",
              data: data,
              success: function(success) {
              	var data = success.chatdetails[0].groupchat;
              	var length = data.length;
              	var data1 = success.chatdetails[1].individualchat;
              	var length1 = data1.length;
              	$('#chatlist').empty();
              	for(var i =0;i<length;i++){
              		$('#chatlist').append('<div class="kt-widget__item"> <span class="kt-media kt-media--circle"> <img src="{{CHAT}}/assets/media/users/300_9.jpg" alt="image"> </span><div class="kt-widget__info"><div class="kt-widget__section"> <a href="javascript:void(0);" class="kt-widget__username" onclick="viewchat(\''+data[i]._id+'\',\''+data[i].groupName+'\',\''+data[i].type+'\')">'+data[i].groupName+'</a> <span class="kt-badge kt-badge--success kt-badge--dot"></span></div> <span class="kt-widget__desc"> </span></div><div class="kt-widget__action"> <span class="kt-widget__date">36 Mines</span></div></div>');
              	}
              	for(var i =0;i<length1;i++){
              		if(data1[i].senderId == userdetails.id){
              			$('#chatlist').append('<div class="kt-widget__item"> <span class="kt-media kt-media--circle"> <img src="{{CHAT}}/assets/media/users/300_9.jpg" alt="image"> </span><div class="kt-widget__info"><div class="kt-widget__section"> <a href="javascript:void(0);" class="kt-widget__username" onclick="viewchat(\''+data1[i]._id+'\',\''+data1[i].receiverName+'\',\''+data1[i].type+'\')">'+data1[i].receiverName+'</a> <span class="kt-badge kt-badge--success kt-badge--dot"></span></div> <span class="kt-widget__desc"> </span></div><div class="kt-widget__action"> <span class="kt-widget__date">36 Mines</span></div></div>');
              		}else{
              			$('#chatlist').append('<div class="kt-widget__item"> <span class="kt-media kt-media--circle"> <img src="{{CHAT}}/assets/media/users/300_9.jpg" alt="image"> </span><div class="kt-widget__info"><div class="kt-widget__section"> <a href="javascript:void(0);" class="kt-widget__username" onclick="viewchat(\''+data1[i]._id+'\',\''+data1[i].senderName+'\',\''+data1[i].type+'\')">'+data1[i].senderName+'</a> <span class="kt-badge kt-badge--success kt-badge--dot"></span></div> <span class="kt-widget__desc"> </span></div><div class="kt-widget__action"> <span class="kt-widget__date">36 Mines</span></div></div>');
              		}
              		
              	}
              	//$('#members').html(JSON.stringify(success));
              	//console.clear();
              	//swal(success.msg);
              	//$('#attendance_summary').modal('hide');
              },
              dataType: "json",
              timeout: 2000
        })
	}
	function viewchat(id,name,type){
		document.getElementById('kt_chat_content').style.display = 'block';
		document.getElementById('kt_chat_content1').style.display = 'none';
		
		$('#chattitle').html(name);
		var userdetails = JSON.parse($('#user').text());
		if(userdetails.role_id == "3"){
			if(type == "1"){
				document.getElementById('eventoptionmenu').style.display = 'block';
			}else{
				document.getElementById('eventoptionmenu').style.display = 'none';
			}
		}else{
			document.getElementById('eventoptionmenu').style.display = 'none';
		}
		$('#groupid').html(id);
		$('#type').html(type);
		//var groupdetails = JSON.parse(id);
		var data = {
			senderId : id
		}
		$.ajax({
              url: chaturl+"getChatHistory",
              headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
              },
              type: "POST",
              data: data,
              success: function(success) {
              	var data = success.data;
              	var length = data.length;
              	$('#chatmessages').empty();
              	for(var i =0;i<length;i++){
              		if(data[i].type == "1"){
	              		if(data[i].senderId == userdetails.id){
	              			$('#chatmessages').append('<div class="kt-chat__message kt-chat__message--right"><div class="kt-chat__user"> <span class="kt-media kt-media--circle kt-media--sm"> <img src="{{CHAT}}/assets/media/users/100_12.jpg" alt="image"> </span> <a href="#" class="kt-chat__username">You</span></a> <span class="kt-chat__datetime">'+data[i].messageTime+'</span></div><div class="kt-chat__text kt-bg-light-success" style="text-align:justify"> '+data[i].message+'</div></div>');
	              		}else{
	              			$('#chatmessages').append('<div class="kt-chat__message"><div class="kt-chat__user"> <span class="kt-media kt-media--circle kt-media--sm"> <img src="{{CHAT}}/assets/media/users/100_12.jpg" alt="image"> </span> <a href="#" class="kt-chat__username">'+data[i].senderName+'</span></a> <span class="kt-chat__datetime">'+data[i].messageTime+'</span></div><div class="kt-chat__text kt-bg-light-success" style="text-align:justify"> '+data[i].message+'</div></div>');
	              		}
	              	}else if(data[i].type == "2"){
	              		if(data[i].senderId == userdetails.id){
	              			$('#chatmessages').append('<div class="kt-chat__message kt-chat__message--right"><div class="kt-chat__user"> <span class="kt-media kt-media--circle kt-media--sm"> <img src="{{CHAT}}/assets/media/users/100_12.jpg" alt="image"> </span> <a href="#" class="kt-chat__username">You</span></a> <span class="kt-chat__datetime">'+data[i].messageTime+'</span></div><div class="kt-chat__text kt-bg-light-success" style="text-align:justify"> <img src="'+s3url+data[i].filename+'" style="height:10em;"></div></div>');
	              		}else{
	              			$('#chatmessages').append('<div class="kt-chat__message"><div class="kt-chat__user"> <span class="kt-media kt-media--circle kt-media--sm"> <img src="{{CHAT}}/assets/media/users/100_12.jpg" alt="image"> </span> <a href="#" class="kt-chat__username">'+data[i].senderName+'</span></a> <span class="kt-chat__datetime">'+data[i].messageTime+'</span></div><div class="kt-chat__text kt-bg-light-success" style="text-align:justify"><img src="'+s3url+data[i].filename+'" style="height:10em;"></div></div>');
	              		}

	              	}
              	}
              	$(".customscroll").animate({ scrollTop: $('.customscroll').prop("scrollHeight")}, "fast");
              	//$('#members').html(JSON.stringify(success));
              	//console.clear();
              	//swal(success.msg);
              	//$('#attendance_summary').modal('hide');
              },
              dataType: "json",
              timeout: 2000
        })
	}
	socket.on('refresh_feed',function(data){
		var userdetails = JSON.parse($('#user').text());
		console.log(JSON.stringify(data));
		if(data.groupId == $('#groupid').text()){
			if(data.type == "1"){
				if(data.senderId == userdetails.id){
		  			$('#chatmessages').append('<div class="kt-chat__message kt-chat__message--right"><div class="kt-chat__user"> <span class="kt-media kt-media--circle kt-media--sm"> <img src="{{CHAT}}/assets/media/users/100_12.jpg" alt="image"> </span> <a href="#" class="kt-chat__username">You</span></a> <span class="kt-chat__datetime">'+data.messageTime+'</span></div><div class="kt-chat__text kt-bg-light-success" style="text-align:justify"> '+data.message+'</div></div>');
		  		}else{
		  			$('#chatmessages').append('<div class="kt-chat__message"><div class="kt-chat__user"> <span class="kt-media kt-media--circle kt-media--sm"> <img src="{{CHAT}}/assets/media/users/100_12.jpg" alt="image"> </span> <a href="#" class="kt-chat__username">'+data.senderName+'</span></a> <span class="kt-chat__datetime">'+data.messageTime+'</span></div><div class="kt-chat__text kt-bg-light-success" style="text-align:justify"> '+data.message+'</div></div>');
		  		}
		  	}else if(data.type == "2"){
		  		if(data.senderId == userdetails.id){
		  			$('#chatmessages').append('<div class="kt-chat__message kt-chat__message--right"><div class="kt-chat__user"> <span class="kt-media kt-media--circle kt-media--sm"> <img src="{{CHAT}}/assets/media/users/100_12.jpg" alt="image"> </span> <a href="#" class="kt-chat__username">You</span></a> <span class="kt-chat__datetime">'+data.messageTime+'</span></div><div class="kt-chat__text kt-bg-light-success" style="text-align:justify"><img src="'+data.filename+'" style="height:10em;"></div></div>');
		  		}else{
		  			$('#chatmessages').append('<div class="kt-chat__message"><div class="kt-chat__user"> <span class="kt-media kt-media--circle kt-media--sm"> <img src="{{CHAT}}/assets/media/users/100_12.jpg" alt="image"> </span> <a href="#" class="kt-chat__username">'+data.senderName+'</span></a> <span class="kt-chat__datetime">'+data.messageTime+'</span></div><div class="kt-chat__text kt-bg-light-success" style="text-align:justify"> <img src="'+data.filename+'" style="height:10em;"></div></div>');
		  		}
		  	}
	  		$(".customscroll").animate({ scrollTop: $('.customscroll').prop("scrollHeight")}, "fast");
	  	}	
  		
	  
	});
	function createChat(id,name){
		var userdetails = JSON.parse($('#user').text());
		var data = {
			groupName : "",
			createdByname: userdetails.name,
			createdById: userdetails.id, 
			type : "2",
			senderId :userdetails.id,
		    senderName :userdetails.name,
		    receiverId :id,
		    receiverName :name,
		}
		//console.log(datalist);
		//return false;
		$.ajax({
              url: chaturl+"createChat",
              headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
              },
              type: "POST",
              data: data,
              success: function(success) {
              	//$('#members').html(JSON.stringify(success));
              	//console.clear();
              	swal(success.msg);
              	$('#individualchatmodal').modal('hide');
              	getChatList();
              },
              dataType: "json",
              timeout: 2000
        })

	}
	function createGroup(){
		//console.log("type---->"+type);
		//console.log($("#members").text());
		var groupname = document.getElementById('groupname').value;
		if(groupname == ""){
			swal("Please enter group name");
			return false;
		}
		var datalist = JSON.parse($("#members").text());		
		var userdetails = JSON.parse($('#user').text());
		var groupdetails = {
			groupName : groupname,
			createdByname: userdetails.name,
			createdById: userdetails.id, 
			type : "1"
		}
		var groupmembers = [{
			memberid : userdetails.id,
			name : userdetails.name,
			role:"staff"
		}];
		for(var i =0;i<datalist.students.length;i++){
			groupmembers.push({
				memberid : datalist.students[i].id,
				name : datalist.students[i].name,
				role:"students"
			});
		}
		for(var i =0;i<datalist.parents.length;i++){
			groupmembers.push({
				memberid : datalist.parents[i].id,
				name : datalist.parents[i].name,
				role:"parents"
			});
		}
		if(groupmembers.length <= 1){
			swal("No Members in this class");
			return false;
		}
		var data = {
			groupdetails : groupdetails,
			groupmembers : groupmembers
		}
		//console.log(datalist);
		//return false;
		$.ajax({
              url: chaturl+"createGroup",
              headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
              },
              type: "POST",
              data: data,
              success: function(success) {
              	//$('#members').html(JSON.stringify(success));
              	//console.clear();
              	swal(success.msg);
              	$('#groupchatmodal').modal('hide');
              	getChatList();
              },
              dataType: "json",
              timeout: 2000
        })

	}
	getChatList();