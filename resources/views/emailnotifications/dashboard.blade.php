@extends($layout)
@section('content')


<section id="main" class="main-wrap bgc-white-darkest" role="main">
 <div class="container-fluid content-wrap">
    <div class="row panel-grid grid-stack">

        <section data-gs-min-width="3"  data-gs-min-height="20" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
        <!--Start Panel-->
        <div class="panel pb-0 bgc-white-dark">
            <div class="panel-body pt-2 grid-stack-handle">
                <div class="h-lg pos-r panel-body-p py-0">
                   <h3 class=""><span class="fs-3 fw-light">{{ getPhrase('promotions')}}</span> <i class="pull-right fa fa-telegram fs-1 m-2"></i></h3>
                   <br><br><br><br>
                   <a href="{{URL_GET_EMAILPROMOTIONS}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>

               </div>
               <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80" sparkLineColor="#fafafa"  sparkFillColor="#97d881" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
           </div>
       </div>
       <!--End Panel-->
   </section>
   <section data-gs-min-width="3" data-gs-min-height="20" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
    <!--Start Panel-->
    <div class="panel pb-0 bgc-white-dark">
        <div class="panel-body pt-2 grid-stack-handle">
            <div class="h-lg pos-r panel-body-p py-0">
               <h3 class=""><span class="fs-3 fw-light">{{ getPhrase('due_remainder')}}</span> <i class="pull-right fa fa-telegram fs-1 m-2"></i></h3>
               <br><br><br><br>
               <a href="{{URL_GET_EMAIL_DUEREMAINDER}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>

           </div>
           <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80" sparkLineColor="#fafafa"  sparkFillColor="#f74948" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
       </div>
   </div>
   <!--End Panel-->
</section>



<section data-gs-min-width="3" data-gs-min-height="20" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
    <!--Start Panel-->
    <div class="panel pb-0 bgc-white-dark">
        <div class="panel-body pt-2 grid-stack-handle">
            <div class="h-lg pos-r panel-body-p py-0">
               <h3 class=""><span class="fs-3 fw-light">{{ getPhrase('event_notifications')}}</span> <i class="pull-right fa fa-telegram fs-1 m-2"></i></h3>
               <br><br><br><br>
               <a href="{{URL_GET_EMAIL_EVENTNOTIFICATIONS}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>

           </div>
           <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80" sparkLineColor="#fafafa"  sparkFillColor="#353f4d" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
       </div>
   </div>
   <!--End Panel-->
</section>



<section data-gs-min-width="3" data-gs-min-height="20" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
    <!--Start Panel-->
    <div class="panel pb-0 bgc-white-dark">
        <div class="panel-body pt-2 grid-stack-handle">
            <div class="h-lg pos-r panel-body-p py-0">
               <h3 class=""><span class="fs-3 fw-light">{{ getPhrase('communications')}}</span> <i class="pull-right fa fa-telegram fs-1 m-2"></i></h3>
               <br><br><br><br>
               <a href="{{URL_GET_EMAIL_COMMUNICATION}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>

           </div>
           <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80" sparkLineColor="#fafafa"  sparkFillColor="#f3d74c" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
       </div>
   </div>
   <!--End Panel-->
</section>

</div>
</div>
</section>


<!-- /#page-wrapper -->

@stop

@section('footer_scripts')



@stop
