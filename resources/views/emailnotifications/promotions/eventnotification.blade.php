@extends($layout)
@section('header_scripts')
<link href="{{CHAT}}/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
 <style>
  /* Multiple Checkbox Css */
/**/

/*.select2-container {
  min-width: 200px;
}*/

/*.select2-results__option {
  padding-right: 20px;
  vertical-align: middle;
}*/
.select2-results__option:before {
  content: "";
  display: inline-block;
  position: relative;
  height: 20px;
  width: 20px;
  border: 2px solid #e9e9e9;
  border-radius: 4px;
  background-color: #fff;
  margin-right: 20px;
  vertical-align: middle;
}
.select2-results__option[aria-selected=true]:before {
  font-family:fontAwesome;
  content: "\f00c";
  color: #fff;
  background-color: #f77750;
  border: 0;
  display: inline-block;
  padding-left: 3px;
}
.select2-selection .select2-selection--multiple{
      border: 1px solid #e1e8f8 !important;
    background: #f4f9fc !important;
}

 </style>
@stop
@section('content')
<div id="page-wrapper">
  <section id="main" class="main-wrap bgc-white-darkest" role="main">
    <div class="container-fluid content-wrap">
      <!-- Page Heading -->
      <div class="row">
        <div class="col-lg-12">
          <ol class="breadcrumb">
            <li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>
            <li><a href="{{URL_GET_EMAILDASHBOARD}}">{{getPhrase('email_notifications_dashboard')}}</a></li>  
            <li class="active">{{$title}}</li>
          </ol>
        </div>
      </div>
     <!-- /.row -->
      <section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item">
      <!--Start Panel-->
        <div class="panel bgc-white-dark">
          <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
              <h2 class="pull-left"> {{ $title }} </h2>
              
          </div>
          <div class="panel-body panel-body-p">
            <?php $button_name = getPhrase('send'); ?>
              <fieldset class="form-group col-sm-6">
                  {{ Form::label('time_to_spend', getphrase('select_list')) }} 
                  <span class="text-red">*</span>
                  <select class = "form-control" id="category" onChange="getEmails()" >
                    <option value="" selected disabled>Select Category</option>
                    @foreach($roles as $rolelist)
                    <option value="{{$rolelist->id}}">{{$rolelist->display_name}}</option>
                    @endforeach
                  </select>
              </fieldset>
              <fieldset class="form-group col-sm-6">
                    {{ Form::label('eventtitle', getphrase('event_title')) }} 
                  <span class="text-red">*</span>
                    {{ Form::text('name', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => 'Event Title' , 'id'=> 'eventtitle')) }}
              </fieldset>
              <fieldset class="form-group col-sm-6">
                {{ Form::label('time_to_spend', getphrase('select_event_start_date&time')) }} 
                  <span class="text-red">*</span>
                <div class="input-group date">
                  <input type="text" class="form-control" readonly placeholder="Select event start date & time" id="eventstartdate" />
                  <div class="input-group-append">
                    <span class="input-group-text">
                      <i class="fa fa-calendar-check-o"></i>
                    </span>
                  </div>
                </div>
              </fieldset>
              <fieldset class="form-group col-sm-6">
                {{ Form::label('time_to_spend', getphrase('select_event_end_date&time')) }} 
                  <span class="text-red">*</span>
                <div class="input-group date">
                  <input type="text" class="form-control" readonly placeholder="Select event end date & time" id="eventenddate" />
                  <div class="input-group-append">
                    <span class="input-group-text">
                      <i class="fa fa-calendar-check-o"></i>
                    </span>
                  </div>
                </div>
              </fieldset>
              <fieldset class="form-group col-sm-6">
                  {{ Form::label('time_to_spend', getphrase('description')) }} 
                  <span class="text-red">*</span>
                  {{ Form::textarea('explanation', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => 'Event Description', 'rows' => '5','id'=>'explanation','name'=>'explanation')) }}
              </fieldset>
              <fieldset class="form-group col-sm-6">
                  {{ Form::label('time_to_spend', getphrase('venue')) }} 
                  <span class="text-red">*</span>
                  {{ Form::textarea('venue', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => 'venue', 'rows' => '5','id'=>'venue','name'=>'venue')) }}
              </fieldset>
              <div class="buttons text-center">

                <button type ="button" class="btn btn-lg btn-primary button" onclick="sendmail();">{{ $button_name }}</button>

              </div>
          </div>
        </div>
      </section>
    </div>
  </section>
  <div id="importmodal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" style="text-align:center;">{{getPhrase('import_lists')}}</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
      
          <div class="modal-body">
           
            
            <?php $button_name = getPhrase('upload'); ?>
            
              {!! Form::open(array('url' => URL_IMPORT_EMAILLIST, 'method' => 'POST', 'novalidate'=>'','name'=>'formExcel ', 'files'=>'true')) !!}
            

              <div class="row">
             <fieldset class="form-group col-md-4">
               <a href="{{DOWNLOAD_LINK_PROMOTIONEMAI_LIST_IMPORT_EXCEL}}" class="btn btn-info helper_step2">{{getPhrase('download_template')}}
               </a>
            </fieldset>
             
            <fieldset class='form-group col-md-8'>
              {!! Form::file('excel', array('class'=>'form-control','id'=>'excel_input', 'accept'=>'.xls,.xlsx',

                  
                )) !!}

            </fieldset>
              </div>
            
              <div class="buttons text-center">
                <button class="btn btn-lg btn-primary button helper_step3" 
                  >{{ $button_name }}</button>
              </div>

             
            {!! Form::close() !!}
          </div>

        </div>
    </div>
  </div>
    <div id="createmodal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" style="text-align:center;">{{getPhrase('create_list')}}</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            {!! Form::open(array('url' => URL_ADD_EMAILLIST, 'method' => 'POST', 'files' => TRUE, 'name'=>'formCourses ', 'novalidate'=>'')) !!}
            <fieldset class="form-group col-md-12">
            
            {{ Form::label('list_title', getphrase('list_title')) }}

              <span class="text-red">*</span>

            {{ Form::text('list_title', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => 'List Title',

              'ng-model'=>'list_title', 

              'required'=> 'true', 

              'ng-class'=>'{"has-error": formCourses.list_title.$touched && formCourses.list_title.$invalid}',

              )) }}

            <div class="validation-error" ng-messages="formCourses.list_title.$error" >

                {!! getValidationMessage()!!}
            </div>

          </fieldset>
          <fieldset class="form-group col-md-12">
            
            {{ Form::label('list_des', getphrase('list_des')) }}

              <span class="text-red">*</span>

            {{ Form::text('list_des', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => 'List Description',

              'ng-model'=>'list_des', 
              'required'=> 'true', 

              'ng-class'=>'{"has-error": formCourses.list_des.$touched && formCourses.list_des.$invalid}',

              )) }}

            <div class="validation-error" ng-messages="formCourses.list_des.$error" >

                {!! getValidationMessage()!!}
            </div>

          </fieldset>
         </div>
        
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" id="cancel1">Cancel</button>
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        
          {!!Form::close()!!}
        </div>

        </div>
    </div>
  <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
@endsection

@section('footer_scripts')
@include('common.editor')
@include('common.validations')
<script src="{{CHAT}}/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
 <script >
    $('#eventstartdate').datetimepicker({
            todayHighlight: true,
            autoclose: true,
            start : moment(),
            format: 'yyyy.mm.dd hh:ii'
        });
    $('#eventenddate').datetimepicker({
            todayHighlight: true,
            autoclose: true,
            start : moment(),
            format: 'yyyy.mm.dd hh:ii'
        });
    var emaillistlength = 0;

 function sendmail(){
    var category = $('#category').val();
    var eventtitle = $('#eventtitle').val();
    var eventstartdate = $('#eventstartdate').val();
    var eventenddate = $('#eventenddate').val();
    var explanation = $('#explanation').val();
    var venue = $('#venue').val();
    if(category == "" || eventtitle == "" || eventstartdate === null || eventstartdate == "" 
      || eventenddate == "" || eventenddate === null || explanation == "" || venue == ""){
      swal('Please fill all fields','','error');
      return false;
    }
    var data1 = {
      "title": eventtitle,
      "startdate": moment(eventstartdate).format("dd-mm-yyyy"),
      "enddate":moment(eventenddate).format("dd-mm-yyyy"),
      "starttime":moment(eventstartdate).format("hh:mm a"),
      "endtime":moment(eventenddate).format("hh:mm a"),
      "description":explanation,
      "venue":venue
    }
    var data = {
      "type" : category,
      "title": eventtitle,
      "startdate": moment(eventstartdate).format("dd-mm-yyyy"),
      "enddate":moment(eventenddate).format("dd-mm-yyyy"),
      "starttime":moment(eventstartdate).format("hh:mm a"),
      "endtime":moment(eventenddate).format("hh:mm a"),
      "description":explanation,
      "venue":venue,
      "content" : data1
    }
    $.ajax({
          url: "{{URL_SEND_EVENTEMAIL}}",
          type: "POST",
          data: data,
          dataType : 'json',
          success: function(success) {
            swal(success.message);
          },
          dataType: "json",
          timeout: 2000
    })
}
</script>

@stop