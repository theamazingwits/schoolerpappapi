@extends($layout)
@section('header_scripts')

@stop
@section('content')
<div id="page-wrapper">
  <section id="main" class="main-wrap bgc-white-darkest" role="main">
    <div class="container-fluid content-wrap">
      <!-- Page Heading -->
      <div class="row">
        <div class="col-lg-12">
          <ol class="breadcrumb">
            <li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>
            <li><a href="{{URL_GET_EMAILDASHBOARD}}">{{getPhrase('email_notifications_dashboard')}}</a></li>  
            <li class="active">{{$title}}</li>
          </ol>
        </div>
      </div>
     <!-- /.row -->
      <section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item">
      <!--Start Panel-->
        <div class="panel bgc-white-dark">
          <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
              <h2 class="pull-left"> {{ $title }} </h2>
              
          </div>
          <div class="panel-body panel-body-p">

            <fieldset class="form-group col-sm-12">
              {{ Form::label('', getphrase('TO:')) }} 
              <span class="text-red">*</span>
              {{ Form::text('to', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => 'example@example.com,example1@example.com' , 'id'=> 'to')) }}
            </fieldset>
            <fieldset class="form-group col-sm-6">
              {{ Form::label('', getphrase('CC:')) }} 
              {{ Form::text('cc', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => 'example@example.com,example1@example.com' , 'id'=> 'cc')) }}
            </fieldset>
            <fieldset class="form-group col-sm-6">
              {{ Form::label('bcc', getphrase('BCC:')) }} 
              {{ Form::text('bcc', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => 'example@example.com,example1@example.com' , 'id'=> 'bcc')) }}
            </fieldset>
            <fieldset class="form-group col-sm-12">
              {{ Form::label('subject', getphrase('subject:')) }} 
              <span class="text-red">*</span>
              {{ Form::text('subject', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => 'Subject' , 'id'=> 'subject')) }}
            </fieldset>
            <?php $button_name = getPhrase('send'); ?>
            <fieldset class="form-group col-sm-12">
              {{ Form::label('message', getphrase('message')) }} 
              <span class="text-red">*</span>
              {{ Form::textarea('message', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => 'Message', 'rows' => '5','id'=>'message','name'=>'message')) }}
            </fieldset>
              <div class="buttons text-center">

                <button type ="button" class="btn btn-lg btn-primary button" onclick="sendMail();">{{ $button_name }}</button>

              </div>
          </div>
        </div>
      </section>
    </div>
  </section>
  <div id="importmodal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" style="text-align:center;">{{getPhrase('import_lists')}}</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
      
          <div class="modal-body">
           
            
            <?php $button_name = getPhrase('upload'); ?>
            
              {!! Form::open(array('url' => URL_IMPORT_EMAILLIST, 'method' => 'POST', 'novalidate'=>'','name'=>'formExcel ', 'files'=>'true')) !!}
            

              <div class="row">
             <fieldset class="form-group col-md-4">
               <a href="{{DOWNLOAD_LINK_PROMOTIONEMAI_LIST_IMPORT_EXCEL}}" class="btn btn-info helper_step2">{{getPhrase('download_template')}}
               </a>
            </fieldset>
             
            <fieldset class='form-group col-md-8'>
              {!! Form::file('excel', array('class'=>'form-control','id'=>'excel_input', 'accept'=>'.xls,.xlsx',

                  
                )) !!}

            </fieldset>
              </div>
            
              <div class="buttons text-center">
                <button class="btn btn-lg btn-primary button helper_step3" 
                  >{{ $button_name }}</button>
              </div>

             
            {!! Form::close() !!}
          </div>

        </div>
    </div>
  </div>
    <div id="createmodal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" style="text-align:center;">{{getPhrase('create_list')}}</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            {!! Form::open(array('url' => URL_ADD_EMAILLIST, 'method' => 'POST', 'files' => TRUE, 'name'=>'formCourses ', 'novalidate'=>'')) !!}
            <fieldset class="form-group col-md-12">
            
            {{ Form::label('list_title', getphrase('list_title')) }}

              <span class="text-red">*</span>

            {{ Form::text('list_title', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => 'List Title',

              'ng-model'=>'list_title', 

              'required'=> 'true', 

              'ng-class'=>'{"has-error": formCourses.list_title.$touched && formCourses.list_title.$invalid}',

              )) }}

            <div class="validation-error" ng-messages="formCourses.list_title.$error" >

                {!! getValidationMessage()!!}
            </div>

          </fieldset>
          <fieldset class="form-group col-md-12">
            
            {{ Form::label('list_des', getphrase('list_des')) }}

              <span class="text-red">*</span>

            {{ Form::text('list_des', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => 'List Description',

              'ng-model'=>'list_des', 
              'required'=> 'true', 

              'ng-class'=>'{"has-error": formCourses.list_des.$touched && formCourses.list_des.$invalid}',

              )) }}

            <div class="validation-error" ng-messages="formCourses.list_des.$error" >

                {!! getValidationMessage()!!}
            </div>

          </fieldset>
         </div>
        
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" id="cancel1">Cancel</button>
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        
          {!!Form::close()!!}
        </div>

        </div>
    </div>
  <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
@endsection

@section('footer_scripts')
@include('common.editor')
@include('common.validations')
<script>

function sendMail(){
  var to = $('#to').val();
  to = to.split(',');
  var cc = $('#cc').val();
  cc = cc.split(',');
  var bcc= $('#bcc').val();
  bcc = bcc.split(',');
  
  var tocheck = 0,cccheck = 0,bcccheck = 0;
  var subject = $('#subject').val();
  var message = $('#message').val();
  var length = to.length;
  var length1 = cc.length;
  var length2 = bcc.length;
  var i,j,k;
  var toemail = [];
  var ccemail = "" ,bccemail = "";
  for (i =0;i<length;i++){
    var toemailcheck = emailValidation(to[i]);
    if(toemailcheck == 0 || toemailcheck == -1){
      tocheck++;
    }else if(toemailcheck == 1){
      toemail.push({
        "email" : to[i]
      })
    }
  }
  for (j =0;j<length1;j++){

    var ccemailcheck = emailValidation(cc[j]);
    if(ccemailcheck == -1){
      cccheck++;
    }else if(ccemailcheck == 1){
      ccemail = $('#cc').val();
    }
    
  }
  for (k =0;k<length2;k++){
    var bccemailcheck = emailValidation(bcc[k]);
    if(bccemailcheck == -1){
      bcccheck++;
    }else if(bccemailcheck == 1){
      bccemail = $('#bcc').val();
    }
  }
  // console.log(y);
  if(tocheck != 0){
    swal("Please enter valid to email address");
    return false;
  }else if(cccheck != 0){
    swal("Please enter valid cc email address");
    return false;
  }else if(bcccheck != 0){
    swal("Please enter valid bcc email address");
    return false;
  }else if(subject == ""){
    swal("Please enter subject");
    return false;
  }else if(message == ""){
    swal("Please enter message");
    return false;
  }

  var data = {
    "toemail" : toemail,
    "ccemail" : ccemail,
    "bccemail": bccemail,
    "subject" : subject,
    "message" : message
  }

  $.ajax({
    url: "{{URL_SEND_COMMUNICATIONEMAIL}}",
    headers: {
    'Content-Type': 'application/x-www-form-urlencoded'
    },
    type: "POST",
    data: data,
    success: function(success) {
      swal(success);
    },
    dataType: "json",
    timeout: 2000
  })
}

function emailValidation(email) {
    //var mailformat =/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    var mailformat =/\b[a-zA-Z0-9\u00C0-\u017F._%+-]+@[a-zA-Z0-9\u00C0-\u017F.-]+\.[a-zA-Z]{2,}\b/;
    //var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    
  if (email == '') {
    return 0;
  } else {
    if(email.match(mailformat)){
      return 1;
    }
    else{
      return -1;
    }
  }
  
}
</script>

@stop