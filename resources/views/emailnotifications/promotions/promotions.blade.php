@extends($layout)
@section('header_scripts')
 <style>
  /* Multiple Checkbox Css */
/**/

/*.select2-container {
  min-width: 200px;
}*/

/*.select2-results__option {
  padding-right: 20px;
  vertical-align: middle;
}*/
.select2-results__option:before {
  content: "";
  display: inline-block;
  position: relative;
  height: 20px;
  width: 20px;
  border: 2px solid #e9e9e9;
  border-radius: 4px;
  background-color: #fff;
  margin-right: 20px;
  vertical-align: middle;
}
.select2-results__option[aria-selected=true]:before {
  font-family:fontAwesome;
  content: "\f00c";
  color: #fff;
  background-color: #f77750;
  border: 0;
  display: inline-block;
  padding-left: 3px;
}
.select2-selection .select2-selection--multiple{
      border: 1px solid #e1e8f8 !important;
    background: #f4f9fc !important;
}

 </style>
@stop
@section('content')
<div id="page-wrapper">
  <section id="main" class="main-wrap bgc-white-darkest" role="main">
    <div class="container-fluid content-wrap">
      <!-- Page Heading -->
      <div class="row">
        <div class="col-lg-12">
          <ol class="breadcrumb">
            <li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>
            <li><a href="{{URL_GET_EMAILDASHBOARD}}">{{getPhrase('email_notifications_dashboard')}}</a></li>  
            <li class="active">{{$title}}</li>
          </ol>
        </div>
      </div>
     <!-- /.row -->
      <section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item">
      <!--Start Panel-->
        <div class="panel bgc-white-dark">
          <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
              <h2 class="pull-left"> {{ $title }} </h2>
              <div class="pull-right messages-buttons">
                 <a href="javascript:void(0)" class="btn  btn-primary button panel-header-button" data-toggle="modal" data-target="#importmodal">{{ getPhrase('import_list')}}</a>
                <a href="javascript:void(0)" class="btn  btn-primary button panel-header-button" data-toggle="modal" data-target="#createmodal">{{ getPhrase('create_list')}}</a>
              </div>
          </div>
          <div class="panel-body panel-body-p">
            <?php $button_name = getPhrase('send'); ?>
            <div class="row">
              <fieldset class="form-group col-sm-6">
                  {{ Form::label('time_to_spend', getphrase('select_list')) }} 
                  <span class="text-red">*</span>
                  <select class = "form-control" id="emaillist" onChange="getEmails()" >
                    <option value="" selected disabled>Select List</option>
                    @foreach($record as $classlist)
                    <option value="{{$classlist->id}}">{{$classlist->title}}({{$classlist->id}})</option>
                    @endforeach
                  </select>
              </fieldset>
              <fieldset class="form-group col-sm-6">
                    {{ Form::label('time_to_spend', getphrase('select_emails')) }} 
                  <span class="text-red">*</span>
                    <select id="multi-select2" class="js-select2 form-control" multiple="multiple">
                  </select>
              </fieldset>
              <fieldset class="form-group col-sm-12">
                  {{ Form::label('time_to_spend', getphrase('content')) }} 
                  <span class="text-red">*</span>
                  {{ Form::textarea('explanation', $value = null , $attributes = array('class'=>'form-control ckeditor', 'placeholder' => 'Your explanation', 'rows' => '5','id'=>'explanation','name'=>'explanation')) }}
              </fieldset>
            </div>
              <div class="buttons text-center">

                <button type ="button" class="btn btn-lg btn-primary button" onclick="sendmail();">{{ $button_name }}</button>

              </div>
          </div>
        </div>
      </section>
    </div>
  </section>
  <div id="importmodal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" style="text-align:center;">{{getPhrase('import_lists')}}</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
      
          <div class="modal-body">
           
            
            <?php $button_name = getPhrase('upload'); ?>
            
              {!! Form::open(array('url' => URL_IMPORT_EMAILLIST, 'method' => 'POST', 'novalidate'=>'','name'=>'formExcel ', 'files'=>'true')) !!}
            

              <div class="row">
             <fieldset class="form-group col-md-4">
               <a href="{{DOWNLOAD_LINK_PROMOTIONEMAI_LIST_IMPORT_EXCEL}}" class="btn btn-info helper_step2">{{getPhrase('download_template')}}
               </a>
            </fieldset>
             
            <fieldset class='form-group col-md-8'>
              {!! Form::file('excel', array('class'=>'form-control','id'=>'excel_input', 'accept'=>'.xls,.xlsx',

                  
                )) !!}

            </fieldset>
              </div>
            
              <div class="buttons text-center">
                <button class="btn btn-lg btn-primary button helper_step3" 
                  >{{ $button_name }}</button>
              </div>

             
            {!! Form::close() !!}
          </div>

        </div>
    </div>
  </div>
    <div id="createmodal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" style="text-align:center;">{{getPhrase('create_list')}}</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            {!! Form::open(array('url' => URL_ADD_EMAILLIST, 'method' => 'POST', 'files' => TRUE, 'name'=>'formCourses ', 'novalidate'=>'')) !!}
            <fieldset class="form-group col-md-12">
            
            {{ Form::label('list_title', getphrase('list_title')) }}

              <span class="text-red">*</span>

            {{ Form::text('list_title', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => 'List Title',

              'ng-model'=>'list_title', 

              'required'=> 'true', 

              'ng-class'=>'{"has-error": formCourses.list_title.$touched && formCourses.list_title.$invalid}',

              )) }}

            <div class="validation-error" ng-messages="formCourses.list_title.$error" >

                {!! getValidationMessage()!!}
            </div>

          </fieldset>
          <fieldset class="form-group col-md-12">
            
            {{ Form::label('list_des', getphrase('list_des')) }}

              <span class="text-red">*</span>

            {{ Form::text('list_des', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => 'List Description',

              'ng-model'=>'list_des', 
              'required'=> 'true', 

              'ng-class'=>'{"has-error": formCourses.list_des.$touched && formCourses.list_des.$invalid}',

              )) }}

            <div class="validation-error" ng-messages="formCourses.list_des.$error" >

                {!! getValidationMessage()!!}
            </div>

          </fieldset>
         </div>
        
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" id="cancel1">Cancel</button>
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        
          {!!Form::close()!!}
        </div>

        </div>
    </div>
  <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
@endsection

@section('footer_scripts')
@include('common.editor')
@include('common.validations')
 <script >
    var emaillistlength = 0;
     $('select').on('select2:opening select2:closing', function( event ) {
                        var $searchfield = $( '#'+event.target.id ).parent().find('.select2-search__field');
                        $searchfield.prop('disabled', true);
                        });
                        $(".js-select2").select2({
                        closeOnSelect : false,
                        placeholder : "Trabsport Capablity",
                        allowHtml: true,
                        allowClear: true,
                        tags: true // создает новые опции на лету
                        });

                        $('.icons_select2').select2({
                            width: "100%",
                            templateSelection: iformat,
                            templateResult: iformat,
                            allowHtml: true,
                            placeholder: "Trabsport Capablity",
                            dropdownParent: $( '.select-icon'),//обавили класс
                            allowClear: true,
                            multiple: false
                        });
                        $("#multi-select2").on('change', function(e) {

        var data;
        data = $(this).val();
        console.log(data);
        if(data != null){
            var res = data.includes("1"); 
            
              if(res){
                var permiss = document.getElementById("multi-select2");
                for(var i=0;i<emaillistlength;i++){
                    permiss.options[i].selected = true;
                }
            }
    
        }
    });

                        function iformat(icon, badge,) {
                            var originalOption = icon.element;
                            var originalOptionBadge = $(originalOption).data('badge');
                         
                            return $('<span><i class="fa ' + $(originalOption).data('icon') + '"></i> ' + icon.text + '<span class="badge">' + originalOptionBadge + '</span></span>');
                        }
  </script>
<script>
  
  function getEmails(){
    var listid = $('#emaillist').val();
    $.ajax({
          url: "{{URL_GET_PROMOTIONEMAILLIST}}",
          headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
          },
          type: "POST",
          data: {"id":listid},
          success: function(success) {
           console.log(JSON.stringify(success));
           var data = success.emaillist;
           var length = data.length;
           emaillistlength = length + 1;
           $('#multi-select2').empty();
           if(length > 0){
           $('#multi-select2').append('<option id="1"  value="1" data-badge="">All</option>')
            }
           for(i =0;i<length;i++){
           $('#multi-select2').append('<option id="emaillist'+i+'"  value="'+data[i].email_id+'" data-badge="">'+data[i].name+'('+data[i].email_id+')</option>')
            }
          },
          dataType: "json",
          timeout: 2000
    })
  }

 function sendmail(){
  var content = CKEDITOR.instances['explanation'].getData();
  var emails = [];
   $("#multi-select2 :selected").each(function(){
        emails.push({"emailid":$(this).val()}); 
    });

   if(emails.length == 0){
    swal('Please select Emails','','error');
    return false;
   }

    $.ajax({
          url: "{{URL_SEND_PROMOTIONEMAIL}}",
          headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
          },
          type: "POST",
          data: {"emails":emails,"content" : content},
          success: function(success) {
            swal(success);
          },
          dataType: "json",
          timeout: 2000
    })
}
</script>

@stop