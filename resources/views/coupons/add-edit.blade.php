@extends('layouts.admin.adminlayout')
@section('header_scripts')
<link href="{{CSS}}bootstrap-datepicker.css" rel="stylesheet">
@stop
@section('content')
<div id="page-wrapper">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
	    <div class="container-fluid content-wrap">
			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="/"><i class="fa fa-home"></i></a> </li>
						<li><a href="{{URL_COUPONS}}">{{ getPhrase('coupons')}}</a></li>
						<li class="active">{{isset($title) ? $title : ''}}</li>
					</ol>
				</div>
			</div>
				@include('errors.errors')
			<!-- /.row -->
			
			<section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item " >
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                        <h2 class="pull-left"> {{ $title }} </h2>
                        
                        <div class="pull-right messages-buttons">
							<a href="{{URL_COUPONS}}" class="btn  btn-primary button panel-header-button" >{{ getPhrase('list')}}</a>
						</div>
                        <!--End panel icons-->
                    </div>
                    <div class="panel-body panel-body-p packages">
						<?php $button_name = getPhrase('create'); ?>
						@if ($record)
						 <?php $button_name = getPhrase('update'); ?>
							{{ Form::model($record, 
							array('url' => URL_COUPONS_EDIT.$record->slug, 
							'method'=>'patch', 'name'=>'formQuiz ', 'novalidate'=>'')) }}
						@else
							{!! Form::open(array('url' => URL_COUPONS_ADD, 'method' => 'POST', 'name'=>'formQuiz ', 'novalidate'=>'')) !!}
						@endif
						

						 @include('coupons.form_elements', 
						 array('button_name'=> $button_name),
						 array('record' 		=> $record, 'categories' => $categories))
						 		
						{!! Form::close() !!}
					</div>

				</div>
		</div>
		<!-- /.container-fluid -->
	</section>
</div>
<!-- /#page-wrapper -->
@stop

@section('footer_scripts')
 @include('common.validations');
 
<script src="{{JS}}datepicker.min.js"></script>
 <script src="{{JS}}bootstrap-toggle.min.js"></script>   
 <script>
 	  $('.input-daterange').datepicker({
        autoclose: true,
        startDate: "0d",
         format: '{{getDateFormat()}}',
    });
 </script>
@stop
 
 