 <?php $settings = getSettings('lms');?>
 			
				<div class="row">
 					 <fieldset class="form-group col-md-6">

						{{ Form::label('title', getphrase('certificate_title')) }}
						<span class="text-red">*</span>
						{{ Form::text('title', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('certificate_title'),
						'ng-model'=>'title',
							'required'=> 'true', 
							'ng-minlength' => '2',
							'id'=>'title',
							'ng-maxlength' => '60',
							'ng-class'=>'{"has-error": formCertificates.title.$touched && formCertificates.title.$invalid}',

						)) }}
						<div class="validation-error" ng-messages="formCertificates.title.$error" >
	    					{!! getValidationMessage()!!}
	    					{!! getValidationMessage('minlength')!!}
	    					{!! getValidationMessage('maxlength')!!}
						</div>
					</fieldset>

					<fieldset class="form-group col-md-6">

						<?php $status = array('1' =>'Active', '0' => 'Inactive', );?>

						{{ Form::label('status', getphrase('status')) }}

						<span class="text-red">*</span>

						{{Form::select('status', $status, null, ['class'=>'form-control'])}}
						

					</fieldset>
					
				</div>
				<div class="row">
					<fieldset class="form-group col-md-12">

						{{ Form::label('content', getphrase('content')) }}
                         
                         	<span class="text-red">*</span>
                     {{ Form::textarea('content', $value = null , $attributes = array('class'=>'form-control ckeditor', 'rows'=>'5', 'placeholder' => 'Fine content','id'=>'content', )) }}
					</fieldset>
				</div>

 			<div class="row">
 					<fieldset class="form-group col-md-6">
                       
                       {{ Form::label('left_sign_name', getphrase('left_sign_name')) }}
						<span class="text-red">*</span>
						{{ Form::text('left_sign_name', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getphrase('left_sign_name'),
						'ng-model'=>'left_sign_name',
							'required'=> 'true', 
							'ng-minlength' => '2',
							'id'=>'left_sign_name',
							'ng-maxlength' => '60',
							'ng-class'=>'{"has-error": formCertificates.left_sign_name.$touched && formCertificates.left_sign_name.$invalid}',

						)) }}
						<div class="validation-error" ng-messages="formCertificates.	left_sign_name.$error" >
	    					{!! getValidationMessage()!!}
	    					{!! getValidationMessage('minlength')!!}
	    					{!! getValidationMessage('maxlength')!!}
						</div>

				    </fieldset>


				    <fieldset class="form-group col-md-6">
                       
                       {{ Form::label('right_sign_name', getphrase('right_sign_name')) }}
						<span class="text-red">*</span>
						{{ Form::text('right_sign_name', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getphrase('right_sign_name'),
						'ng-model'=>'right_sign_name',
							'required'=> 'true', 
							'ng-minlength' => '2',
							'id'=>'right_sign_name',
							'ng-maxlength' => '60',
							'ng-class'=>'{"has-error": formCertificates.right_sign_name.$touched && formCertificates.right_sign_name.$invalid}',

						)) }}
						<div class="validation-error" ng-messages="formCertificates.	right_sign_name.$error" >
	    					{!! getValidationMessage()!!}
	    					{!! getValidationMessage('minlength')!!}
	    					{!! getValidationMessage('maxlength')!!}
						</div>

				    </fieldset>

             </div>



 			<div class="row">
 					<fieldset class="form-group col-md-6">
                       
                       {{ Form::label('left_sign_designation', getphrase('left_sign_designation')) }}
						<span class="text-red">*</span>
						{{ Form::text('left_sign_designation', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getphrase('left_sign_designation'),
						'ng-model'=>'left_sign_designation',
							'required'=> 'true', 
							'ng-minlength' => '2',
							'id'=>'left_sign_designation',
							'ng-maxlength' => '60',
							'ng-class'=>'{"has-error": formCertificates.left_sign_designation.$touched && formCertificates.left_sign_designation.$invalid}',

						)) }}
						<div class="validation-error" ng-messages="formCertificates.left_sign_designation.$error" >
	    					{!! getValidationMessage()!!}
	    					{!! getValidationMessage('minlength')!!}
	    					{!! getValidationMessage('maxlength')!!}
						</div>

				    </fieldset>


				    <fieldset class="form-group col-md-6">
                       
                       {{ Form::label('right_sign_designation', getphrase('right_sign_designation')) }}
						<span class="text-red">*</span>
						{{ Form::text('right_sign_designation', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getphrase('right_sign_designation'),
						'ng-model'=>'right_sign_designation',
							'required'=> 'true', 
							'ng-minlength' => '2',
							'id'=>'right_sign_designation',
							'ng-maxlength' => '60',
							'ng-class'=>'{"has-error": formCertificates.right_sign_designation.$touched && formCertificates.right_sign_designation.$invalid}',

						)) }}
						<div class="validation-error" ng-messages="formCertificates.right_sign_designation.$error" >
	    					{!! getValidationMessage()!!}
	    					{!! getValidationMessage('minlength')!!}
	    					{!! getValidationMessage('maxlength')!!}
						</div>

				    </fieldset>

             </div>
              
              <div class="row">

                   <fieldset class="form-group col-md-3" >
				   {{ Form::label('left_sign', getphrase('left_sign')) }}
				   <span class="text-red">*</span>
				         <input type="file" class="form-control" name="left_sign"
				          accept=".png,.jpg,.jpeg" id="left_sign">
				    </fieldset>

				     <fieldset class="form-group col-md-3" >
					@if($record)	
				   		@if($record->left_sign)
				         
				         <img src="{{ IMAGE_PATH_UPLOAD_CERTIFICATES.$record->left_sign }}" height="100" width="100">
				         @endif
				     @endif
		          </fieldset>

		          <fieldset class="form-group col-md-3" >
				   {{ Form::label('right_sign', getphrase('right_sign')) }}
				   <span class="text-red">*</span>
				         <input type="file" class="form-control" name="right_sign"
				          accept=".png,.jpg,.jpeg" id="right_sign">

				    </fieldset>

				     <fieldset class="form-group col-md-3" >
					@if($record)	
				   		@if($record->right_sign)
				         
				         <img src="{{ IMAGE_PATH_UPLOAD_CERTIFICATES.$record->right_sign }}" height="100" width="100">
				         @endif
				     @endif
		          </fieldset>
		    </div>


						<div class="buttons text-center">
							<button class="btn btn-lg btn-primary button"
							ng-disabled='!formCertificates.$valid'>{{ $button_name }}</button>
						</div>
