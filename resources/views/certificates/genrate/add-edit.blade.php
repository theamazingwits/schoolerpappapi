@extends($layout)
 
@section('content')

<div id="page-wrapper">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
        <div class="container-fluid content-wrap">
    		<!-- Page Heading -->
    		<div class="row">
    			<div class="col-lg-12">
    				<ol class="breadcrumb">
    					<li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>
                        @if(checkRole(getUserGrade(2)))
    					<li><a  href="{{URL_ACADEMICOPERATIONS_DASHBOARD}}">{{ getPhrase('academic_dashboard')}}</a></li>
                        @endif
    					<li><a href="{{URL_CERTIFICATES}}">{{ getPhrase('certificates')}}</a></li>
    					<li class="active">{{isset($title) ? $title : ''}}</li>
    				</ol>
    			</div>
    		</div>
    			@include('errors.errors')
    		<!-- /.row -->
    		

    		<section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item " >
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                        <h2 class="pull-left"> {{ $title }} </h2>   
                        <div class="pull-right messages-buttons">
                            <a href="#" onclick="showRefrences()" class="btn  btn-warning button panel-header-button" >{{ getPhrase('content_refrence')}}</a>
                            <a href="{{URL_CERTIFICATES}}" class="btn  btn-primary button panel-header-button" >{{ getPhrase('list')}}</a>
                        
                        </div>                  
                        <!--End panel icons-->
                    </div>
                    <div class="panel-body panel-body-p packages">
            			<?php $button_name = getPhrase('create'); ?>
            			@if ($record)
            			 <?php $button_name = getPhrase('update'); ?>
            				{{ Form::model($record, 
            				array('url' => URL_CERTIFICATES_EDIT. $record->slug, 'novalidate'=>'','name'=>'formCertificates ',
            				'method'=>'patch', 'files' => true)) }}
            			@else
            				{!! Form::open(array('url' => URL_CERTIFICATES_ADD, 
            					'novalidate'=>'','name'=>'formCertificates ',
            				'method' => 'POST', 'files' => true)) !!}
            			@endif
            			 @include('certificates.genrate.form_elements', 
            			 array('button_name'=> $button_name),
            			 array('record'=>$record))
            			 	 	
            			{!! Form::close() !!}
			         </div> 

    		      </div>
            </section>
    	</div>
    </section>



				 <div id="myRefrenceModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="text-align:center;color:#44a1ef;">{{getPhrase('content_refrences')}}</h4>
      </div>
    
     
      <div class="modal-body">
        <h4 align="center"><u>{{getPhrase('use_the_below_format_variables_in_content_to_get_user_data_in_certificates')}}</u></h4>
        <table class="table">
        	<thead>
        		<th><strong>{{getPhrase('user_data')}}</strong></th>
        		<th><strong>{{getPhrase('use_variable')}}</strong></th>
        	</thead>
        	<tbody>
        		<tr>
        			<td>{{getPhrase('student_name')}}</td>
        			<td><?php echo '{{'; ?>$student_name<?php echo '}}'; ?></td>
        		</tr>
                <tr>
                	<td>{{getPhrase('father_name')}}</td>
                	<td><?php echo '{{'; ?>$fathers_name<?php echo '}}'; ?></td>
                </tr>
                
                <tr>
                	<td>{{getPhrase('course_title')}}</td>
                	<td><?php echo '{{'; ?>$course_id<?php echo '}}'; ?></td>
                </tr>

                <tr>
                	<td>{{getPhrase('joined_course_title')}}</td>
                	<td><?php echo '{{'; ?>$joined_course_title<?php echo '}}'; ?></td>
                </tr>

                <tr>
                	<td>{{getPhrase('joined_date')}}</td>
                	<td><?php echo '{{'; ?>$date_of_join<?php echo '}}'; ?></td>
                </tr>

                <tr>
                	<td>{{getPhrase('religion')}}</td>
                	<td><?php echo '{{'; ?>$religion<?php echo '}}'; ?></td>
                </tr>

                <tr>
                	<td>{{getPhrase('category')}}</td>
                	<td><?php echo '{{'; ?>$category<?php echo '}}'; ?></td>
                </tr>

                <tr>
                	<td>{{getPhrase('address')}}</td>
                	<td><?php echo '{{'; ?>$address<?php echo '}}'; ?></td>
                </tr>

               


        	</tbody>
        </table>  
      
      </div>
    
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">{{getPhrase('ok')}}</button>
      </div>
    
    </div>

  </div>
</div>
			<!-- /.container-fluid -->
		</div>
		<!-- /#page-wrapper -->
@stop
@section('footer_scripts')  
@include('common.validations')

 <script >
   
    function showRefrences()
    {    
      $('#myRefrenceModal').modal('show');
    }
</script> 
@include('common.editor')
 
@stop
 