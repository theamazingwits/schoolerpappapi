<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>{{$certificate_data->title}}</title>
    <style>
        {font-family: sans-serif;font-size: 16px;color: #333;margin-bottom: 0; text-transform: uppercase}
        {padding: 5px 10px; text-align: left; margin: 0;}
    </style>
</head>
<body style="margin: 0;">
    <div style="border:1px solid #111;margin:5px;border-radius:5px;margin-bottom:50px;padding-bottom: 80px;">

@if(getSetting('show_watermark', 'bonafide_settings')) 
<div style="background-image: url({{IMAGE_PATH_SETTINGS.getSetting('watermark_image', 'bonafide_content')}}); background-repeat: none; background-size: 80% auto; background-position: center center; ">
@else
<div>
@endif


 
<table cellpadding="0" width="100%" cellspacing="0" border="0" style="font-family: sans-serif;  font-size: 14px; color: #999; line-height:24px;">
   
    <tr class="">
        <td>
            <table cellpadding="0" width="100%" cellspacing="0" border="0">
                <tr>
                    <td  style="padding: 5px 10px 0px 25px; text-align: left; font-size:16px;">
                    {{getPhrase('reference_no')}}: <strong style="color:#000;">{{$reference_no}}</strong>
                    
                    </td>
                    <td style="padding: 5px 10px; text-align: center;">
                        <table cellpadding="0" width="100%" cellspacing="0" border="0">

                            <tr>
                                <td align="center" style="padding-right: 90px;">
                                    <img src="{{IMAGE_PATH_SETTINGS.getSetting('site_logo', 'site_settings')}}" width="150" alt="">
                                </td>
                            </tr>
                             <tr>
                                <td align="center">
                                    
                                    <h4 style="font-size: 15px; margin:5px 80px 20px 0;color: #333;"> {{$certificate_data->title}}
                                    </h4>
                                </td>
                            </tr>
                            
                        </table>
                    </td>
                    <td style="padding: 5px 25px 0px 0px; text-align: right; font-size:16px;">
                        <?php
                          $date  = date('d M Y',strtotime(date('Y-m-d')))
                        ?>
                      {{getPhrase('date')}}: <strong style="color:#000;">{{$date}}</strong> 
                    
                    </td>
                </tr>
            </table>
        </td>
    </tr>


    <tr>
        <td align="center">
            <hr>
        </td>
    </tr>


    <tr>
       <td style="padding: 5px 10px 0px 25px;  font-size:16px; margin-left: 5px;">
          <p style="font-size: 16px;  line-height:30px;">
              {!! $content !!}
          </p>
        </td>
    </tr>
    <tr>
        <td  style="padding: 5px 25px 0px 0px;  font-size:16px; float: right;" >
      @if($certificate_data->right_sign!=null) 
        <div style=" text-align:right; margin:10px 0 0 ; color:#333; font-size:20px;">
        <img src="{{IMAGE_PATH_UPLOAD_CERTIFICATES.$certificate_data->right_sign}}" height="40" alt="">
        </div>
       @endif
        @if($certificate_data->right_sign_name!='') 
            <div style=" text-align:right; color:#333; font-size:15px;">
            <strong>{{$certificate_data->right_sign_name}}</strong>
            </div>
        @endif
        @if($certificate_data->right_sign_designation!='') 
               <div style=" text-align:right; margin:5px 0 0 ; color:#333; font-size:13px;"><strong>
               {{$certificate_data->right_sign_designation}}</strong></div>
        @endif
            
        </td>
        

        <td  style="padding: 5px 10px 0px 25px;  font-size:16px; float: left;">
      @if($certificate_data->left_sign!=null) 
        <div style=" text-align:left; margin:10px 0 0 ; color:#333; font-size:20px;">
        <img src="{{IMAGE_PATH_UPLOAD_CERTIFICATES.$certificate_data->left_sign}}" height="40" alt="">
        </div>
       @endif
        @if($certificate_data->left_sign_name!='') 
            <div style=" text-align:left; color:#333; font-size:15px;">
            <strong>{{$certificate_data->left_sign_name}}</strong>
            </div>
        @endif
        @if($certificate_data->left_sign_designation!='') 
               <div style=" text-align:left; margin:5px 0 0 ; color:#333; font-size:13px;"><strong>{{$certificate_data->left_sign_designation}}</strong></div>
        @endif
            
        </td>

    </tr>
</table>
</div>
</div>
</body>
 
</html>