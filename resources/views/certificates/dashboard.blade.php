@extends($layout)
@section('content')

<div id="page-wrapper">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
    	<div class="container-fluid content-wrap">
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						 <li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>
						 <li>
	                <a href="{{URL_ACADEMICOPERATIONS_DASHBOARD}}">
	                    {{getPhrase('academic_operations')}}
	                </a>
	            </li>
						 <li>{{ $title}}</li>
					</ol>
				</div>
			</div>

		 	<div class="row panel-grid grid-stack">

		 		<section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-body pt-2 grid-stack-handle">
                                <div class="h-lg pos-r panel-body-p py-0">
									<h3 class=""><span class="fs-3 fw-light">{{ getPhrase('id_cards')}}</span> <i class="pull-right fa fa-external-link-square fs-1 m-2"></i></h3>
									<br><br>
                                    <a href="{{URL_CERTIFICATES_GENERATE_IDCARD}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
								
                                </div>
                                    <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80" sparkLineColor="#fafafa"  sparkFillColor="#97d881" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>
					

						<section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-body pt-2 grid-stack-handle">
                                <div class="h-lg pos-r panel-body-p py-0">
									<h3 class=""><span class="fs-3 fw-light">{{ getPhrase('bonafide /_transfer_certificates')}}</span> <i class="pull-right fa fa-street-view fs-1 m-2"></i></h3>
									<br><br>
                                    <a href="{{URL_CERTIFICATE_BONAFIDE}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
								
                                </div>
                                    <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80" sparkLineColor="#fafafa"  sparkFillColor="#46aaf9" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>
		 		
			</div>
		</div>
	</section>
</div>
                    
				
		<!-- /#page-wrapper -->

@stop

@section('footer_scripts')
 
 

@stop
