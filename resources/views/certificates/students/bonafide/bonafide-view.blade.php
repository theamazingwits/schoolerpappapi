@extends($layout)

@section('header_scripts')
 

@stop

@section('custom_div')
<div ng-controller="TabController">
@stop
<?php
    if($right_bar === TRUE){
        $column = "col-xl-8";
        $column1 = "col-xl-6";
    }else{
        $column = "col-xl-12";
        $column1 = "col-xl-4";
    }
?>
@section('content')
<div id="page-wrapper">
    <section id="main" class="main-wrap bgc-white-darkest" role="main">
        <div class="container-fluid content-wrap">
            <div class="row">
                <div class="col-lg-12">
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{PREFIX}}">
                                <i class="bc-home fa fa-home">
                                </i>
                            </a>
                        </li>
                        <li>
                            <a href="{{URL_ACADEMICOPERATIONS_DASHBOARD}}">
                                {{getPhrase('academic_operations')}}
                            </a>
                        </li>


                        <li>
                            <a href="{{URL_CERTIFICATES_DASHBOARD}}">
                                {{getPhrase('certificates_dashboard')}}
                            </a>
                        </li>
                        <li>
                            
                                {{$title}}
                            
                        </li>
                    </ol>
                </div>
            </div>
            <div class=" panel-grid" id="panel-grid"> 
                <section class="col-sm-12 col-md-12 col-lg-12 {{$column}} panel-wrap panel-grid-item">
                    <!--Start Panel-->
                    <div class="panel bgc-white-dark">
                        <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                            <h2 class="pull-left"> {{ $title }} </h2>
                            
                            <!--Start panel icons-->
                            <div class="panel-icons panel-icon-slide bgc-white-dark">
                                <ul>
                                    <li><a href=""><i class="fa fa-angle-left"></i></a>
                                        <ul>
                                            <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                                            <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                                            <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
                                            <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
                                            <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li>
                                            <!-- <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li> -->
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <!--End panel icons-->
                        </div>
                        <div class="panel-body panel-body-p instruction">
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset class="form-group col-md-6">

                                        {{ Form::label ('type', getphrase('certificate_type')) }}
                                        {{ Form::select('certificate_type', $certificate_types, null, 
                                        [   'class'     => 'form-control', 
                                            "id"        => "certificate_type",
                                            "ng-model"  => "selected_certificate_type",
                                            "ng-change" => "certificateTypeChanged(selected_certificate_type)"
                                        ])}}
                                    </fieldset>
                                    <fieldset class="form-group  col-md-6">
                                        {{ Form::label ('search', getphrase('search')) }}
                                        <input type="text" class="form-control" name="search" id="enter-details" ng-model="search" placeholder="{{'search'}}" ng-change="textChanged(search)">
                                    </fieldset>
                                </div>
                            </div>
                            <hr>
                            <div class="row vertical-scroll table-responsive" >
                                <table class="table">
                                
                                    <thead>
                                        <th>{{getPhrase('image')}}</th>
                                        <th>{{getPhrase('name')}}</th>
                                        <th>{{getPhrase('roll_no')}}</th>
                                        <th>{{getPhrase('admission_no')}}</th>
                                        <th>{{getPhrase('class')}}</th>
                                        <th ng-if="selected_certificate_type!='tc'">{{getPhrase('year')}}-{{getPhrase('semester')}}</th>
                                       <th ng-if="selected_certificate_type=='tc'">{{getPhrase('status')}}</th>

                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="user in users" id="@{{'selected_'+user.id}}" ng-click="getUserDetails(user)">
                                            <td>
                                            
                                            <img ng-if="user.image!=null && user.image!=''" class="thumb" src="{{IMAGE_PATH_PROFILE}}@{{user.image}}" height="60">
                                            
                                            <img ng-if="user.image==null || user.image==''" class="thumb" src="{{IMAGE_PATH_USERS_DEFAULT_THUMB}}">
                                            </td>
                                            <td>@{{user.name}}</td>
                                            <td>@{{user.roll_no}}</td>
                                            <td>@{{user.admission_no}}</td>
                                            <td> @{{user.academic_year_title+' '+user.course_title}} </td>
                                            <td> <span ng-if = "user.course_dueration<=1">-</span>
                                                <span ng-if="user.current_year!=-1"><span ng-if="user.course_dueration>1"> @{{user.current_year}}</span></span>
                                                <span ng-if="user.current_semister!=0"><span ng-if="user.current_semister!=-1"> - @{{user.current_semister}}</span> </span> 
                                                <span ng-show="user.current_year==-1" >{{getPhrase('completed')}}</span>
                                            </td>
                                            
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <hr>
                        </div>
                    </div>
                </section>
            <div> 
            @if(isset($right_bar))
            <div class=" panel-grid" id="panel-grid"> 
                <section class="col-sm-12 col-md-12 col-lg-12 col-xl-4 panel-wrap panel-grid-item">
                    <!--Start Panel-->
                    <div class="panel bgc-white-dark">
                        <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                            <h2 class="pull-left"> {{ $title }} </h2>
                            
                            <!--Start panel icons-->
                            <div class="panel-icons panel-icon-slide bgc-white-dark">
                                <ul>
                                    <li><a href=""><i class="fa fa-angle-left"></i></a>
                                        <ul>
                                            <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                                            <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                                            <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
                                            <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
                                            <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li>
                                           <!--  <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li -->>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <!--End panel icons-->
                        </div>
                        <div class="panel-body panel-body-p">
                            <?php $data = '';

                            if(isset($right_bar_data))

                                $data = $right_bar_data;

                            ?>

                            @include($right_bar_path, array('data' => $data))
                        </div>
                    </div>
                </section>
             </div> 
            @endif
        </div>
    </section>
</div>

@stop
 
 

@section('footer_scripts')

    @include('certificates.students.scripts.js-scripts')
    
@stop

@section('custom_div_end')
</div>
@stop