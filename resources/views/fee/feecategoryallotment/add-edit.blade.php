@extends($layout)

@section('header_scripts')
<link href="{{CSS}}animate.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{CSS}}select2.css">

@stop
@section('custom_div')
<div ng-controller="academicCourses" ng-init="ingAngData({{$items}})">
  @stop
<?php
  if($right_bar === TRUE){
    $column = "col-xl-8";
    $column1 = "col-xl-6";
  }else{
    $column = "col-xl-12";
    $column1 = "col-xl-4";
  }
?>
  @section('content')
  <div id="page-wrapper">
    <section id="main" class="main-wrap bgc-white-darkest" role="main">
      <div class="container-fluid content-wrap">
        <!-- Page Heading -->
        <div class="row">
          <div class="col-lg-12">
            <ol class="breadcrumb">
              <li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>

              <li> <a href="{{URL_FEE_CATEGORIES}}"> {{getPhrase('fee_categories')}}</a></li>
              <li class="active">{{isset($title) ? $title : ''}}</li>
            </ol>
          </div>
        </div>
        @include('errors.errors')
        <!-- /.row -->

        <section class="col-sm-12 col-md-12 col-lg-12 {{$column}} panel-wrap panel-grid-item">
                <!--Start Panel-->
          <div class="panel bgc-white-dark">
            <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                <h2 class="pull-left"> {{ $title }} </h2>
                <!--End panel icons-->
            </div>
            <div class="panel-body panel-body-p">
              <?php $button_name = getPhrase('create'); ?>
              
              <?php $button_name = getPhrase('update'); ?>
              {{ Form::model($record, 
                array('url' => URL_ADD_FEE_PARTICULARS_TO_CATEGORY.$record->slug, 
                'method'=>'post')) }}
                <input type="hidden" name="feecategories[]" value="{{$record->id}}">
                <input type="hidden" name="feecategory" value="{{$record->id}}">
                <div class="row">
                  <div class="col-md-12">

                    <h2 class="selected-item-title">{{$record->title }}</h2>
                    <div class='containerVertical' id="target"  ng-drop="true" ng-drop-success="onDropComplete($data,$event)">
                      <div ng-if="!target_items.length" class="subject-placeholder"> No Item Selected</div>
                      <div ng-repeat="item in target_items"  id="target_items-@{{item.id}}">
                        <ul class="cs-inline-list">
                          <li>
                            <div class="items-sub no-pad-right">
                              @{{item.title}}
                            </div>
                            <input type="hidden" name="selected_list[]" data-myname="@{{item.title}}" value="@{{item.id}}">
                          </li>
                          <li>
                            <input type="number" name="amount[]" data-myname="@{{item.title}}"   value="@{{item.amount}}" min="0">
                          </li>
                          <li>

                            <input ng-if="item.is_term_applicable != 1 && item.is_term_applicable != 0" type="checkbox" name="is_term[]" id="is_term_@{{item.id}}" checked value="@{{item.id}}">


                            <input ng-if="item.is_term_applicable==1" type="checkbox" name="is_term[]" id="is_term_@{{item.id}}" checked value="@{{item.id}}">

                            <input ng-if="item.is_term_applicable==0" type="checkbox" name="is_term[]" id="is_term_@{{item.id}}" value="@{{item.id}}" >
                            <label for="is_term_@{{item.id}}"> <span class="fa-stack checkbox-button"> <i class="fa fa-check active"></i> </span> Is Term </label>
                          </li>
                          <li>
                            <i class="fa fa-trash text-danger " ng-click="removeItem(item, target_items, 'target_items')"></i>
                          </li>
                        </ul>

                      </div>

                    </div>


                  </div>

                </div>
                <br>
                @if(count($fee_categories))  
                <fieldset class="form-group">    
                  {!! Form::label('Add FeeCategories', 'Add FeeCategories', ['class' => 'control-label']) !!}
                  {{Form::select('feecategories[]', $fee_categories, null, ['class'=>'form-control select2', 'name'=>'feecategories[]', 'multiple'=>'true'])}}
                </fieldset>
                @endif

                <div class="text-center">
                  <button class="btn btn-primary btn-lg">{{getPhrase('update')}}</button>
                </div>

                {!! Form::close() !!}




              </div>
            </div>
          </section>
           @if(isset($right_bar))
          <section class="col-sm-12 col-md-12 col-lg-12 col-xl-4 panel-wrap panel-grid-item">
            <!--Start Panel-->
            <div class="panel bgc-white-dark">
              <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                <h2 class="pull-left"> {{ $title }} </h2>
                    
              </div>
              <div class="panel-body panel-body-p">
                <?php $data = '';

                if(isset($right_bar_data))

                $data = $right_bar_data;

                ?>

                @include($right_bar_path, array('data' => $data))
              </div>
            </div>
          </section>
          @endif
        </div>
        <!-- /.container-fluid -->
      </section>
     
    </div>
    <!-- /#page-wrapper -->


    @stop

    @section('custom_div_end')
  </div>
  @stop

  @section('footer_scripts')
  @include('fee.feecategoryallotment.scripts.js-scripts')
  @include('common.affix-window-size-script', array('newId'=>'app'))
  @include('common.alertify')
  <script src="{{JS}}select2.js"></script>

  <script>
    $('.select2').select2({
     placeholder: "Add Fee Category",
   });
 </script>
 @stop