 <div class="row">
	 <fieldset class="form-group col-md-6">
		
		{{ Form::label('title', getphrase('title')) }}

		<span class="text-red">*</span>
		
		{{ Form::text('title', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => '2015-2016-CSE-1st Year-1st Semister')) }}
	</fieldset>

	<fieldset class="form-group col-md-6">

		<?php $status = array('1' =>'Active', '0' => 'Inactive', );?>

		{{ Form::label('status', getphrase('status')) }}

		<span class="text-red">*</span>

		{{Form::select('status', $status, null, ['class'=>'form-control'])}}
		

	</fieldset> 



	 <fieldset class="form-group col-md-12">
		
		{{ Form::label('description', getphrase('description')) }}
		
		{{ Form::textarea('description', $value = null, $attributes = array('class'=>'form-control', 'placeholder' => 'I Year Inter 2016-2017', 'rows'=>'5')) }}
	</fieldset>

	    
		 
		
	
		<!-- </div> -->
</div>		
 @include('fee.feecategories.year-selection')
					
						<div style="text-align: center;">
							<button class="btn btn-lg btn-success button">{{ $button_name }}</button>
						</div>