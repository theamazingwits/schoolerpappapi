@extends($layout)
@section('header_scripts')
<link href="{{CSS}}ajax-datatables.css" rel="stylesheet">
@stop
@section('content')


<div id="page-wrapper">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
	    <div class="container-fluid content-wrap">
			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>
						<li><a href="{{URL_FEE_REPORTS}}">{{getPhrase('fee_dashboard')}}</a></li>
						<li>{{ $title }}</li>
					</ol>
				</div>
			</div>
							
			<!-- /.row -->
			<section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item " >
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                        <h2 class="pull-left"> {{ $title }} </h2>
                        
                       	<div class="pull-right messages-buttons">
							<a href="{{URL_FEE_CATEGORIES_ADD}}" class="btn  btn-primary button panel-header-button" >{{ getPhrase('create')}}</a>
						</div>
                        <!--End panel icons-->
                    </div>
                    <div class="panel-body panel-body-p packages">
						<div> 
						<table class="table table-striped table-bordered datatable" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>{{ getPhrase('name')}}</th>
									<th>{{ getPhrase('installments')}}</th>
									<th>{{ getPhrase('installment_amount')}}</th>
									<th>{{ getPhrase('other_amount')}}</th>
									 <th>{{ getPhrase('status')}}</th>
									<th>{{ getPhrase('action')}}</th>
								  
								</tr>
							</thead>
							 
						</table>
						</div>
					</div>
				</div>
			</section>
		</div>
		<!-- /.container-fluid -->
	</section>
</div>
@endsection
 

@section('footer_scripts')
 	<script src="{{JS}}bootstrap-toggle.min.js"></script>
 	<script src="{{JS}}jquery.dataTables.min.js"></script>
	<script src="{{JS}}dataTables.bootstrap.min.js"></script>
	<!-- <script src="/js/scripts/datatables.js"></script> -->
  <script>
  var tableObj;
    $(document).ready(function(){
   		 tableObj = $('.datatable').DataTable({
	            processing: true,
	            serverSide: true,
	            ajax: '{{ route('feecategories.dataTable') }}',
	    });
    });
  </script>

<script>

	function deleteRecord(slug) {
	swal({
		  title: "Are you sure?",
		  text: "You will not be able to recover this record!",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonClass: "btn-danger",
		  confirmButtonText: "Yes, delete it!",
		  cancelButtonText: "No, cancel pls!",
		  closeOnConfirm: false,
		  closeOnCancel: false
		},
		function(isConfirm) {
		  if (isConfirm) {
		  	  var token = '{{ csrf_token()}}';
		  	route = '{{URL_FEE_CATEGORIES_DELETE}}'+slug;  
		    $.ajax({
		        url:route,
		        type: 'post',
		        data: {_method: 'delete', _token :token},
		        success:function(msg){
		        	tableObj.ajax.reload();
		        	swal("Deleted!", "Your record has been deleted. ", "success");
		        }
		    });

		  } else {
		    swal("Cancelled", "Your record is safe :)", "error");
		  }
	});
	}
</script>

 

@stop
