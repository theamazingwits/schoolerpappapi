@extends($layout)
<!-- @section('header_scripts')
<link href="{{CSS}}plugins/datetimepicker/css/bootstrap-datetimepicker.css" rel="stylesheet">   

@stop -->
@section('content')
<div id="page-wrapper" ng-controller="schedule_controller">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
	    <div class="container-fluid content-wrap">
			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>
						<li><a href="{{URL_FEE_CATEGORIES}}">{{ getPhrase('fee_categories')}}</a> </li>
						<li class="active">{{isset($title) ? $title : ''}}</li>
					</ol>
				</div>
			</div>
				@include('errors.errors')
			<!-- /.row -->
			
			<section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item " >
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                        <h2 class="pull-left"> {{ $title }} </h2>
                        
                       	<div class="pull-right messages-buttons">
							<a href="{{URL_FEE_CATEGORIES}}" class="btn  btn-primary button panel-header-button" >{{ getPhrase('list')}}</a>
						</div>
                        <!--End panel icons-->
                    </div>
                    <div class="panel-body panel-body-p packages">
						<?php $button_name = getPhrase('create'); ?>
						
							{!! Form::open(array('url' => URL_FEE_CATEGORIES_SHEDULES_ADD, 'method' => 'POST','name'=>'formSchedule', 'novalidate'=>'')) !!}

						 @include('fee.schedules.schedule-form-elements', array('button_name'=> $button_name,'feecategory'=>$feeCategory,'records'=>$records))
						 
						{!! Form::close() !!}
					</div>
				</div>
			</section>
		</div>
	</section>
	<!-- /.container-fluid -->
	<div id="myModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	      	<h4 class="modal-title" style="text-align:center;">{{getPhrase('delete_schedules')}}</h4>
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	      </div>
		  
		   {!!Form::open(array('url'=> URL_FEE_DELETE_FEESCHEDULES,'method'=>'POST','name'=>'deleteschedules'))!!} 
		   
	      <div class="modal-body">
	      <span id="message"></span>
	         
	          <h4 style="color:#44a1ef;text-align: center;">{{getPhrase('are_you_sure_to_delete_schedules')}}</h4 >

	        <input type="hidden" name="feecategory_id" id="feecategory_id" >

	      </div>
		  
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
	        <button type="submit" class="btn btn-primary" >Yes</button>
	      </div>
		  
	      {!!Form::close()!!}
	    </div>

	  </div>
	</div>
</div>
		<!-- /#page-wrapper -->
@stop
@section('footer_scripts')

@include('fee.schedules.scripts.schedule-scripts')

 <script >
 	 
 		function deleteSchedule(feecat_id)
 		{    
			var ci = feecat_id;
			$('#feecategory_id').val(ci);
			
 			$('#myModal').modal('show');
 		}
</script>
 @stop