@extends($layout)
@section('header_scripts')
<link href="{{CSS}}ajax-datatables.css" rel="stylesheet">
@stop
@section('content')


<div id="page-wrapper" ng-controller="fee_payments_report">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
	    <div class="container-fluid content-wrap">
			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>
						<li><a href="{{URL_FEE_REPORTS}}">{{getPhrase('fee_dashboard')}}</a></li>
						<li>{{ $title }}</li>
					</ol>
				</div>
			</div>
							
			<!-- /.row -->
			<section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item " >
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                        <h2 class="pull-left"> {{ $title }} </h2>
                    </div>
                    <div class="panel-body panel-body-p packages">
						<div> 
						<table class="table table-striped table-bordered datatable" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>{{ getPhrase('fee_category')}}</th>
									<th>{{ getPhrase('name')}}</th>
									<th>{{ getPhrase('paid_amount')}}</th>
									<th>{{ getPhrase('payment_details')}}</th>
									<th>{{ getPhrase('status')}}</th>
									<th>{{ getPhrase('paid_date')}}</th>
									<th>{{ getPhrase('action')}}</th>
								  
								</tr>
							</thead>
							 
						</table>
						</div>

					</div>
				</div>
			</section>
		</div>
		<!-- /.container-fluid -->
	</section>
	<div id="myModal" class="modal fade" role="dialog">
  		<div class="modal-dialog">
		{!! Form::open(array('url' => URL_PAYMENT_APPROVE_OFFLINE_FEE_PAYMENT, 'method' => 'POST', 'name'=>'formQuiz ',  )) !!}
		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		      	<h4 class="modal-title" align="center">{{getPhrase('offline_fee_payment_details')}}</h4>
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		      </div>
		      <div class="modal-body">
		        <div class="row">
		           <div class="col-md-8 col-md-offset-2">
		               <p><strong>{{getPhrase('fee_category')}}</strong> : @{{feecategory_title}}</p>
		               <p><strong>{{getPhrase('student_name')}}</strong> : @{{user.name}}</p>
		            <p><strong>{{getPhrase('paid_amount')}}</strong> : {{getCurrencyCode().' '}} @{{payment_record.paid_amount}}</p>
		               <p><strong>{{getPhrase('paid_date')}}</strong> :  @{{payment_record.created_at}}</p>
		              
		               <p><strong>{{getPhrase('comments')}}</strong> : <textarea class="form-control" name="admin_comment"></textarea></p>
		           </div>
		           <input type="hidden" name="payment_slug" value="@{{payment_record.slug}}">
		        </div>
		      </div>
		      <div class="modal-footer">
		      <button class="btn btn-lg btn-success button" name="submit" value="approve">{{ getPhrase('approve') }}</button>
		      <button class="btn btn-lg btn-danger button" name="submit" value="reject">{{ getPhrase('reject') }}</button>
		      <button class="btn btn-lg btn-default button" data-dismiss="modal" type="button">{{ getPhrase('close')}}</button>
		      </div>
		    </div>
			{!! Form::close() !!}
  		</div>
	</div>
</div>
@endsection
 

@section('footer_scripts')
 @include('common.datatables', array('route'=>URL_FEE_OFFLINE_REPORTS_AJAXLIST, 'route_as_url' => TRUE))
  @include('fee.payments.scripts.js-scripts')
  <script>
function viewFeePaymentDetails(record_id)
{
    angular.element('#page-wrapper').scope().setDetails(record_id);
    angular.element('#page-wrapper').scope().$apply() 
 $('#myModal').modal('show');
}
</script>

@stop
