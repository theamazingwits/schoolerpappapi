@extends($layout)

@section('header_scripts')
<link href="{{CSS}}bootstrap-datepicker.css" rel="stylesheet">
@stop


@section('content')
<div id="page-wrapper" ng-controller="studentFeePaidDetails" ng-init="ingAngData()">
  <section id="main" class="main-wrap bgc-white-darkest" role="main">
    <div class="container-fluid content-wrap">
      <div class="row">
        <div class="col-lg-12">
          <ol class="breadcrumb">
            <li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>
            <li><a href="{{URL_FEE_REPORTS}}">{{getPhrase('fee_dashboard')}}</a></li>
            <li>{{ $title }}</li>
          </ol>
        </div>
      </div> 
      

      {!! Form::open(array('url' => URL_PRINT_STUDENTS_PAIDFEE_CLASSWISE, 'method' => 'POST', 'name'=>'htmlform ','target'=>'_blank', 'id'=>'htmlform', 'novalidate'=>'')) !!}

      <section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item " >
                <!--Start Panel-->
        <div class="panel bgc-white-dark">
          <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
            <h2 class="pull-left"> {{ $title }} </h2>
          </div>
          <div class="panel-body panel-body-p packages">

             <div class="row input-daterange" id="dp">

              <?php 

              $date_from = date('Y/m/d');

              $date_to = date('Y/m/d');


              ?>

              <fieldset class="form-group col-md-6">


                {{ Form::label('date_from', getphrase('date_from')) }}

                {{ Form::text('date_from', $value = $date_from , 
                [
                'class'=>'input-sm form-control',
                'placeholder' => '2015/7/17',
                'ng-model'=>'date_from',

                ])}}



              </fieldset>


              <fieldset class="form-group col-md-6">


                {{ Form::label('date_to', getphrase('date_to')) }}


                {{ Form::text('date_to', $value = $date_to , 
                [
                'class'=>'input-sm form-control', 
                'placeholder' => '2015/7/17',
                'ng-model'=>'date_to',
                'ng-change' =>'datesSelectd(date_from,date_to)',
                ]) }}



              </fieldset>



              <div>
                <fieldset class="form-group col-md-6">
                  <a class="btn btn-primary btn-lg" ng-click="getLastWeekReports()" href="#">{{getPhrase('weekly_reports')}}</a>
                </fieldset>

                <fieldset class="form-group col-md-6">

                  <a class="btn btn-primary btn-lg" ng-click="getLastMonthReports()" href="#">{{getPhrase('monthly_reports')}}</a>           </fieldset>
                </div>
              </div>

              <div ng-show="result_data.length>0" class="row">
               <div class="col-sm-4 col-sm-offset-8">
                <div class="input-group">
                  <input type="text" ng-model="search" class="form-control input-lg" placeholder="{{getPhrase('search')}}" name="search" />
                  <!-- <span class="input-group-btn">
                    <button class="btn btn-primary btn-lg" type="button">
                      <i class="glyphicon glyphicon-search"></i>
                    </button>
                  </span> -->
                </div>
              </div>
            </div>
            <br>

            <div ng-if="result_data.length!=0">

              <div class="row vertical-scroll">


                <table class="table panel-primary">
                  <thead class="panel-heading">
                    <th style="text-align: center;"><b>{{getPhrase('sno')}}</b></th>
                    <th style="text-align: center;"><b>{{getPhrase('paid_date')}}</b></th>
                    <!-- <th style="text-align: center;"><b>{{getPhrase('payment_mode')}}</b></th> -->
                    <th style="text-align: center;"><b>{{getPhrase('fee_category')}}</b></th>
                    <th style="text-align: center;"><b>{{getPhrase('name')}}</b></th>
                    <!-- <th style="text-align: center;"><b>{{getPhrase('roll_no')}}</b></th> -->
                    <th style="text-align: center;"><b>{{getPhrase('amount')}}</b></th>
                    <th style="text-align: center;"><b>{{getPhrase('discount')}}</b></th>
                    <th style="text-align: center;"><b>{{getPhrase('paid_amount')}}</b></th>
                    <th style="text-align: center;"><b>{{getPhrase('balance')}}</b></th>


                  </thead>
                  <tbody>


                    <?php $currency  = getCurrencyCode();
                    ?>

                    <tr ng-repeat="user in result_data | filter:search track by $index" class="panel-content">


                      <td style="text-align: center;" >@{{$index+1}}</td>
                      <td style="text-align: center;" >@{{user.recevied_on}}</td>
                      <!-- <td style="text-align: center;" >@{{user.payment_mode}}</td> -->
                      <td style="text-align: center;"> @{{user.feecategory_title}}<br><strong>{{getPhrase('roll_no : ')}}</strong>@{{user.roll_no}}</td>
                      <td style="text-align: center;">@{{user.name}}</td>
                      <!-- <td style="text-align: center;">@{{user.roll_no}}</td> -->
                      <td style="text-align: center;">{{$currency}} @{{user.total_amount}}</td>
                      <td style="text-align: center;">{{$currency}} @{{user.discount_amount}}</td>
                      <td style="text-align: center;">{{$currency}} @{{user.paid_amount}}</td>
                      <td style="text-align: center;">{{$currency}} @{{user.balance}}</td>




                    </tr> 

                  </tbody>
                </table>
              </div>
            </div>


            <br>
            <!-- <a ng-if="result_data.length!=0" class="btn btn-primary" ng-click="printIt()">Print</a> -->
            <div ng-if="result_data.length==0" class="text-center" style="font-size:20px"><strong>{{getPhrase('no_data_available')}}</strong></div>                         
          </div>
        </div>
      </section>


  </div>
  </section>
</div>

{!! Form::close() !!}

@stop



@section('footer_scripts')


@include('fee.reports.scripts.getdailyreports-script')

<script src="{{JS}}datepicker.min.js"></script>
<script src="{{JS}}bootstrap-toggle.min.js"></script>   
<script>
  $('.input-daterange').datepicker({
    autoclose: true,
    format: '{{getDateFormat()}}',
  });
</script>

@stop