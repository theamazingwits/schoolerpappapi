 				
					<div class="row">

 					 <fieldset class="form-group col-md-12">

						{{ Form::label('title', getphrase('title')) }}

						<span class="text-red">*</span>

						{{ Form::text('title', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('title'),

							'ng-model'=>'title', 

							'ng-pattern'=>getRegexPattern('name'), 

							'required'=> 'true', 

							'ng-class'=>'{"has-error": formQuiz.title.$touched && formQuiz.title.$invalid}',

							'ng-minlength' => '4',

							'ng-maxlength' => '60',

							)) }}

						<div class="validation-error" ng-messages="formQuiz.title.$error" >

	    					{!! getValidationMessage()!!}

	    					{!! getValidationMessage('pattern')!!}

	    					{!! getValidationMessage('minlength')!!}

	    					{!! getValidationMessage('maxlength')!!}

						</div>

					</fieldset>	

					<fieldset class="form-group col-md-6">

						<?php $status = array('1' =>'Active', '0' => 'Inactive', );?>

						{{ Form::label('status', getphrase('status')) }}

						<span class="text-red">*</span>

						{{Form::select('status', $status, null, ['class'=>'form-control'])}}
						

					</fieldset> 

					<fieldset class="form-group col-md-6">

						<?php $is_income = array('1' =>'Yes', '0' => 'No', );?>

						{{ Form::label('is_income', getphrase('is_income')) }}

						<span class="text-red">*</span>

						{{Form::select('is_income', $is_income, null, ['class'=>'form-control'])}}
						

					</fieldset> 

					<fieldset class="form-group col-md-12">
						
						{{ Form::label('description', getphrase('description')) }}
						
						{{ Form::textarea('description', $value = null , $attributes = array('class'=>'form-control', 'rows'=>'5', 'placeholder' => 'Description','id'=>'description')) }}
					</fieldset>					

					</div>


						<div class="buttons text-center">

							<button class="btn btn-lg btn-primary button"

							ng-disabled='!formQuiz.$valid'>{{ $button_name }}</button>

						</div>

		 