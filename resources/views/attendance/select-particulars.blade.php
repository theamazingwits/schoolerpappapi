@extends($layout)
@section('header_scripts')
<link href="{{CSS}}plugins/datetimepicker/css/bootstrap-datetimepicker.css" rel="stylesheet">   

@stop

@section('content')
<div id="page-wrapper" ng-controller="academicAttendance">
    <section id="main" class="main-wrap bgc-white-darkest" role="main">
        <div class="container-fluid content-wrap">
            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{PREFIX}}">
                                <i class="fa fa-home">
                                </i>
                            </a>
                        </li>
                        @if($role_name!='staff')
                        <li>
                            <a href="{{URL_ACADEMICOPERATIONS_DASHBOARD}}">
                                {{getPhrase('academic_operations')}}
                            </a>
                        </li>
                        @endif
                        <li>{{getphrase('particulars')}}</li>
                    </ol>
                </div>
            </div>
            <!-- /.row -->
            <section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item">
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                        <h2 class="pull-left"> {{ getPhrase('select_details')}} </h2>
                        <!--Start panel icons-->
                        <div class="panel-icons panel-icon-slide bgc-white-dark">
                            <ul>
                              <li><a href=""><i class="fa fa-angle-left"></i></a>
                                  <ul>
                                      <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                                      <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                                      <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
                                      <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
                                      <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li>
                                      <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li>
                                  </ul>
                                </li>
                            </ul>
                        </div>
                      <!--End panel icons-->
                    </div>
                    <div class="panel-body panel-body-p instruction">
                        <div class="row">
                            <div class="col-md-6">
                                <h3>{{ getPhrase('general_instructions') }}</h3>
                                <ul class="guide">
                                    <li>
                                        <span class="answer">
                                            <i class="fa fa-check">
                                            </i>
                                        </span>
                                        {{getPhrase('present')}}
                                    </li>
                                    <li>
                                        <span class="notanswer">
                                            <i class="fa fa-close">
                                            </i>
                                        </span>
                                        {{getPhrase('absent')}}
                                    </li>
                                    <li>
                                        <span class="marked">
                                            <i class="fa fa-eye">
                                            </i>
                                        </span>
                                        {{getPhrase('leave')}}
                                    </li>
                                </ul>
                            </div>
                            {!! Form::open(array('url' => URL_STUDENT_ATTENDENCE_ADD.$userdata->slug, 'method' => 'POST')) !!}
                             @if($role_name!='staff')
                            <div class="col-md-6">
                            @include('common.year-selection-view')
                            </div>
                            @else
                            <div class ="col-md-6">
                                <h3></h3>
                                <fieldset class="form-group col-sm-12">
                                    {{ Form::label('select_subject', getphrase('select_subject')) }}
                                    
                                    <span class="text-red">*</span>

                                    {{Form::select('course_subject_id',$subjects,  null, 
                                    [   'class'     => 'form-control',
                                        
                                        'id'        =>'select_academic_year'
                                       
                                    ])}}
                                </fieldset>
                                <?php 
                                $number_of_class=[];
                                $maximum_classes = 8;
                                for($class_number = 1; $class_number<=$maximum_classes; $class_number++)
                                $number_of_class[$class_number]=$class_number; ?>

                                <fieldset  class="form-group col-sm-12">
                                    {{ Form::label('class', getphrase('total_class')) }}
                                    <span class="text-red">*</span>               
                                    {{Form::select('total_class',$number_of_class,  null, 
                                                    ['class'=>'form-control'])}}
                                </fieldset>

                                <fieldset class="form-group col-sm-12">
                                    {{ Form::label('attendance_date', getphrase('attendance_date')) }}
                                    <div class="input-group date" data-date="{{date('Y/m/d')}}" data-provide="datepicker" data-date-format="{{getDateFormat()}}">
                                    {{ Form::text('attendance_date', $value = date('Y/m/d') , $attributes = array('class'=>'form-control', 'placeholder' => '2015/7/17', 'id'=>'dp')) }}
                                        <div class="input-group-addon">
                                            <span class="fa fa-calendar"></span>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            @endif
                        </div>
                        <br>
                        <div class="row">
                            <div class="text-center" style="margin-left: 40%;">
                                <button type="submit" class="btn button btn-lg btn-primary">
                                    {{getPhrase('get_details')}}
                                </button>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>
</div>
@stop
 
 

@section('footer_scripts')
 
 <script src="{{JS}}moment.min.js"></script>

  <script src="{{JS}}plugins/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
 <script>
    
     $(function () {
        $('#dp').datetimepicker({
               format: 'YYYY-MM-DD',
                maxDate: 'now'
            
            });
    });
 </script>
    @include('attendance.scripts.js-scripts')
    
@stop