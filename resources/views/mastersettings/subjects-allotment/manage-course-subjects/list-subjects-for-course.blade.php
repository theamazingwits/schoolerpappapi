@extends($layout)

@section('header_scripts')
<link href="{{CSS}}animate.css" rel="stylesheet">
@stop

@section('custom_div')
<div ng-controller="courseSubjectsController" ng-init="ingAngData({{$items}})">
@stop
<?php
	if($right_bar === TRUE){
		$column = "col-xl-8";
		$column1 = "col-xl-6";
	}else{
		$column = "col-xl-12";
		$column1 = "col-xl-4";
	}
?>
@section('content')


<div id="page-wrapper">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
	    <div class="container-fluid content-wrap">
			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>
						<li><a href="{{URL_COURSES_DASHBOARD}}">{{getPhrase('master_setup_dashboard')}}</a></li>
						<li><a href="{{URL_MASTERSETTINGS_COURSE_SUBJECTS."staff"}}">{{ getPhrase('allocate_staff_to_courses')}}</a> </li>
						<li>{{ $title }}</li>
					</ol>
				</div>
			</div>
				<?php 
				$subjects_list = [];
				$angular_keys = [];
				?>		

			<!-- /.row -->
				{!! Form::open(array('url' => URL_COURSE_SUBJECTS_UPDATE_STAFF, 'method' => 'POST', 'name'=>'formQuiz ', 'novalidate'=>'')) !!}

				<input type="hidden" name="academic_id" value="{{$academic_id}}">
				<input type="hidden" name="course_id" value="{{$record->id}}">
				<input type="hidden" name="course_parent_id" value="{{$record->parent_id}}">

			<section class="col-sm-12 col-md-12 col-lg-12 {{$column}} panel-wrap panel-grid-item">
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                        <h2 class="pull-left"> {{ $title }} </h2>

						<div class="pull-right messages-buttons">
							  
							<button type="submit" class="btn  btn-success button helper_step1 panel-header-button" >{{ getPhrase('update')}}</button>
							<a href="{{URL_MASTERSETTINGS_COURSE_SUBJECTS."staff"}}" class="btn  btn-primary button helper_step2 panel-header-button" >{{ getPhrase('list')}}</a>
							 
						</div>
                    </div>
                        
                    <div class="panel-body panel-body-p vertical-scroll">
				
						 @for($yearno = 1; $yearno <= $record->course_dueration; $yearno++)
							<h4> {{ getPhrase('year').' '. $yearno }} </h4>
							<div class="sem-parent-container">
							
							
							@if($record->is_having_semister)
							<?php 
							$semisters = App\CourseSemister::getCourseYearSemisters($course_id, $yearno);
							
							$total_data = [];
							

							?>

								@for($semister=1; $semister <= $semisters->total_semisters; $semister++)
								
								<?php 
								$subjects_list = App\CourseSubject::getCourseSavedSubjects($academic_id, $course_id, $yearno, $semister);
								$angular_key = $yearno.'_'.$semister;

								?>
								<small >SEM {{ $semister }}</small>

								@include('mastersettings.subjects-allotment.manage-course-subjects.course-list-subview', 
								array(	
									'academic_id' 	=> $academic_id,
									'course_id' 	=> $course_id,
									'yearno' 		=> $yearno,
									'semister' 		=> $semister,
									'subjects' 		=> $subjects_list,
									'angular_key'   => $angular_key,
									))
									 <?php $angular_keys[] = $angular_key;?>
								
								@endfor
								
								@if($semisters->total_semisters == 0)
								<?php 
								$subjects = App\CourseSubject::getCourseSavedSubjects($academic_id, $course_id, $yearno, 0);
								$angular_key = $yearno.'_0';
								 
								?>
								 

								 @include('mastersettings.subjects-allotment.manage-course-subjects.course-list-subview', 
								array(	
									'academic_id' 	=> $academic_id,
									'course_id' 	=> $course_id,
									'yearno' 		=> $yearno,
									'semister' 		=> 0,
									'subjects' 		=> $subjects_list,
									'angular_key'   => $angular_key,
									))
									 <?php $angular_keys[] = $angular_key;?>
								@endif

							@else
							<?php 
								$subjects = App\CourseSubject::getCourseSavedSubjects($academic_id, $course_id, $yearno, 0);
								$angular_key = $yearno.'_0';
								?>
								 
								 @include('mastersettings.subjects-allotment.manage-course-subjects.course-list-subview', 
								array(	
									'academic_id' 	=> $academic_id,
									'course_id' 	=> $course_id,
									'yearno' 		=> $yearno,
									'semister' 		=> 0,
									'subjects' 		=> $subjects_list,
									'angular_key'   => $angular_key,
									))
									 <?php $angular_keys[] = $angular_key;?>
							@endif
							 </div>
						@endfor
					</div>
				</div>
				{!! Form::close() !!}
			</section>
			@if(isset($right_bar))
			<section class="col-sm-12 col-md-12 col-lg-12 col-xl-4 panel-wrap panel-grid-item">
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                        <h2 class="pull-left"> {{ $title }} </h2>
                        
                        <!--Start panel icons-->
                        <!--End panel icons-->
                    </div>
                    <div class="panel-body panel-body-p">
                    	<?php $data = '';

						if(isset($right_bar_data))

							$data = $right_bar_data;

						?>

						@include($right_bar_path, array('data' => $data))
                    </div>
                </div>
            </section>
            @endif
		</div>
		<!-- /.container-fluid -->
	</section>
</div>

		
@stop
 
@section('footer_scripts')
	@include('mastersettings.subjects-allotment.scripts.js-scripts', array('keys'=>$keys))
	@include('common.alertify')
@include('common.affix-window-size-script')	
@stop 
