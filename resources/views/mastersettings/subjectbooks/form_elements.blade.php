                <div class="row">
 					<fieldset class="form-group col-md-12">
						{{ Form::label('subject_id', getphrase('subject')) }}
						<span class="text-red">*</span>
						{{Form::select('subject_id', $subjects, null, ['class'=>'form-control', 'id'=>'subject',
							'ng-model'=>'subject_id',
							'required'=> 'true', 
							'ng-class'=>'{"has-error": formSubjectbooks.subject_id.$touched && formSubjectbooks.subject_id.$invalid}'
						])}}
						 <div class="validation-error" ng-messages="formSubjectbooks.subject_id.$error" >
	    					{!! getValidationMessage()!!}
						</div>
					</fieldset>



					 <fieldset class="form-group col-md-12">
						
						{{ Form::label('book_name', getphrase('book_name')) }}
						<span class="text-red">*</span>
						{{ Form::text('book_name', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => 'Introduction',
							'ng-model'=>'book_name',
							'required'=> 'true', 
							'ng-class'=>'{"has-error": formSubjectbooks.book_name.$touched && formSubjectbooks.book_name.$invalid}',
						 ))}}
						  <div class="validation-error" ng-messages="formSubjectbooks.book_name.$error" >
	    					{!! getValidationMessage()!!}
	    					</div>
					</fieldset>

					<fieldset class="form-group col-md-6 ">
				        {{ Form::label('file', getphrase('file')) }}
				         <input type="file" class="form-control" name="catimage">
                   </fieldset>
				  
                      @if($record)

				      <fieldset class="form-group col-md-6">
				      	
				          <img src="{{IMAGE_PATH_UPLOAD_SUBJECTBOOKS_DEFAULT}}" width="40px" height="40px"><p><a href="{{URL_DOWNLOAD_SUBJECTBOOKS_FILE.$record->slug}}">{{$record->file_name}}</a></p>
				       

				      </fieldset>

				        @endif

					<fieldset class="form-group col-md-12">
						
						{{ Form::label('description', getphrase('description')) }}
						
						{{ Form::textarea('description', $value = null , $attributes = array('class'=>'form-control', 'rows'=>'5', 'placeholder' => 'Description of the book')) }}
					</fieldset>

				</div>

					 <div class="buttons text-center">
							<button class="btn btn-lg btn-primary button" 
							ng-disabled='!formSubjectbooks.$valid'
							>{{ $button_name }}</button>
						</div>
					
		 