@extends('layouts.admin.adminlayout')
@section('header_scripts')
 <link href="{{CSS}}ajax-datatables.css" rel="stylesheet">
@stop
@section('content')


<div id="page-wrapper">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
	    <div class="container-fluid content-wrap">
			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>
						<li><a  href="{{URL_COURSES_DASHBOARD}}">{{ getPhrase('master_setup_dashboard')}}</a></li>
						<li>{{ $title }}</li>
					</ol>
				</div>
			</div>
							
			<!-- /.row -->
			<section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item">
            <!--Start Panel-->
	            <div class="panel bgc-white-dark">
	                <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
	                    <h2 class="pull-left"> {{ $title }} </h2>
	                    <div class="pull-right messages-buttons">
						 
							<!-- <a href="{{URL_TOPICS_IMPORT}}" class="btn  btn-primary button helper_step4 panel-header-button" >{{ getPhrase('import')}}</a> -->
							<a href="{{URL_SUBJECT_BOOKS_ADD}}" class="btn  btn-primary button helper_step1 panel-header-button" >{{ getPhrase('create')}}</a>
							 
						</div>
	                    <!--Start panel icons-->
	                    <!--End panel icons-->
	                </div>
	                <div class="panel-body panel-body-p">
						<div > 
						<table class="table table-striped table-bordered datatable" cellspacing="0" width="100%">
							<thead>
								<tr>
									 
								 
									<th id="helper_step2">{{ getPhrase('subject')}}</th>
									<th>{{ getPhrase('book_name')}}</th>
									<th>{{ getPhrase('file_name')}}</th>
									<th>{{ getPhrase('description')}}</th>
									 
									<th id="helper_step3">{{ getPhrase('action')}}</th>
								  
								</tr>
							</thead>
							 
						</table>
						</div>

					</div>
				</div>
			</section>
		</div>
		<!-- /.container-fluid -->
	</section>
</div>
@endsection
 
<?php $url = URL_SUBJECT_BOOKS_GET_LIST;
 
 ?>
@section('footer_scripts')
  
 @include('common.datatables', array('route'=>$url, 'route_as_url' => TRUE))
 @include('common.deletescript', array('route'=>URL_SUBJECT_BOOKS_DELETE))

@stop
