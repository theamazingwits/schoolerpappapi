@extends('layouts.admin.adminlayout')

@section('header_scripts')
<link rel="stylesheet" type="text/css" href="{{CSS}}select2.css">
@stop

@section('content')
<div id="page-wrapper">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
	    <div class="container-fluid content-wrap">
			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="/"><i class="fa fa-home"></i></a> </li>
						<li><a  href="{{URL_COURSES_DASHBOARD}}">{{ getPhrase('master_setup_dashboard')}}</a></li>
						<li><a href="{{URL_TOPICS}}">{{ getPhrase('subject_books')}}</a> </li>
						<li class="active">{{isset($title) ? $title : ''}}</li>
					</ol>
				</div>
			</div>
				@include('errors.errors')
			<!-- /.row -->
			
			<section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item">
            <!--Start Panel-->
	            <div class="panel bgc-white-dark">
	                <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
	                    <h2 class="pull-left"> {{ $title }} </h2>
	                    <div class="pull-right messages-buttons">
							<a href="{{URL_SUBJECT_BOOKS}}" class="btn  btn-primary button helper_step1 panel-header-button" >{{ getPhrase('list')}}</a>
						</div>
	                    <!--Start panel icons-->
	                    <!--End panel icons-->
	                </div>
	                <div class="panel-body panel-body-p" ng-controller="angTopicsController">
					<?php $button_name = getPhrase('create'); ?>
					@if ($record)
					 <?php $button_name = getPhrase('update'); ?>
						{{ Form::model($record, 
						array('url' => URL_SUBJECT_BOOKS_EDIT.'/'.$record->slug, 
						'method'=>'patch' ,'novalidate'=>'','name'=>'formSubjectbooks','files'=>TRUE)) }}
					@else
						{!! Form::open(array('url' => URL_SUBJECT_BOOKS_ADD, 'method' => 'POST', 
						'novalidate'=>'','name'=>'formSubjectbooks','files'=>TRUE)) !!}
					@endif

					 @include('mastersettings.subjectbooks.form_elements', 
					 array('button_name'=> $button_name,
					 'record'=> $record,),
					 array('subjects'=>$subjects))
					 
					{!! Form::close() !!}
					 

					</div>
				</div>
			</section>
		</div>
		<!-- /.container-fluid -->
	</section>
</div>
<!-- /#page-wrapper -->
@stop
@section('footer_scripts')
	@include('mastersettings.topics.scripts.js-scripts');
	@include('common.validations', array('isLoaded'=>TRUE));
@stop
 