@extends('layouts.admin.adminlayout')
@section('content')
<div id="page-wrapper">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
        <div class="container-fluid content-wrap">
			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="/"><i class="fa fa-home"></i></a> </li>
						<li><a  href="{{URL_COURSES_DASHBOARD}}">{{ getPhrase('master_setup_dashboard')}}</a></li>
						<li><a href="{{URL_MASTERSETTINGS_COURSE}}">{{ getPhrase('courses')}}</a> </li>
						<li class="active">{{isset($title) ? $title : ''}}</li>
					</ol>
				</div>
			</div>
				@include('errors.errors')
			<!-- /.row -->
			
			<section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item">
            <!--Start Panel-->
	            <div class="panel bgc-white-dark">
	                <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
	                    <h2 class="pull-left"> {{ $title }} </h2>
	                    <div class="pull-right messages-buttons">
							<a href="{{URL_MASTERSETTINGS_COURSE}}" class="btn  btn-primary button panel-header-button" >{{ getPhrase('list')}}</a>
						</div>
	                    <!--Start panel icons-->
	                    <!--End panel icons-->
	                </div>
	                <div class="panel-body panel-body-p">
				 
						 <?php $button_name = getPhrase('update'); ?>
							{{ Form::model($records, 
							array('url' => ['mastersettings/course/editSemisters'], 
							'method'=>'patch')) }}
						
						@foreach($records as $record)
							 <fieldset class="form-group">
								
								{{ Form::label('year'.$record->year, ucfirst('year '.$record->year)) }}
								
								{{ Form::text($record->id, $value = $record->total_semisters , $attributes = array('class'=>'form-control', 'placeholder' => '2')) }}
							</fieldset>
						@endforeach
							
								<div class="buttons text-center">
								<button class="btn btn-lg btn-primary button">{{ $button_name }}</button>
							</div>
						 
						{!! Form::close() !!}
				 

					</div>
				</div>
			</section>
		</div>
		<!-- /.container-fluid -->
	</section>
</div>
<!-- /#page-wrapper -->
@stop

 