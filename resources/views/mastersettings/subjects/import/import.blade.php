@extends($layout)

@section('content')
<div id="page-wrapper">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
	    <div class="container-fluid content-wrap">
			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>
						<li><a href="{{URL_COURSES_DASHBOARD}}">{{getPhrase('master_setup_dashboard')}}</a></li>
						@if(checkRole(getUserGrade(2)))
						<li><a href="{{URL_SUBJECTS}}">{{ getPhrase('subjects')}}</a> </li>
						<li class="active">{{isset($title) ? $title : ''}}</li>
						@else
						<li class="active">{{$title}}</li>
						@endif
					</ol>
				</div>
			</div>
				@include('errors.errors')
			<!-- /.row -->
			
			<section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item">
            <!--Start Panel-->
	            <div class="panel bgc-white-dark">
	                <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
	                    <h2 class="pull-left"> {{ $title }} </h2>
	                    @if(checkRole(getUserGrade(2))) 
						<div class="pull-right messages-buttons">
							 
							<a href="{{URL_SUBJECTS}}" class="btn  btn-primary button helper_step1 panel-header-button" >{{ getPhrase('list')}}</a>
							 
						</div>
						@endif
	                    <!--Start panel icons-->
	                    <!--End panel icons-->
	                </div>
	                <div class="panel-body panel-body-p text-center">
				
						<a href="{{DOWNLOAD_LINK_SUBJECTS_IMPORT_EXCEL}}" class="btn btn-info helper_step2">{{getPhrase('download_template')}}
						</a>
						
						<?php $button_name = getPhrase('upload'); ?>
						
							{!! Form::open(array('url' => URL_SUBJECTS_IMPORT, 'method' => 'POST', 'novalidate'=>'','name'=>'formExcel ', 'files'=>'true')) !!}
						

							<div class="row">
					 
						<fieldset class='col-sm-4 col-sm-offset-4 form-group margintop30'>
							{{ Form::label('excel', getphrase('upload').' Excel') }}
							{!! Form::file('excel', array('class'=>'form-control','id'=>'excel_input', 'accept'=>'.xls,.xlsx',

							    
								)) !!}

						</fieldset>
						  </div>
						
							<div class="buttons text-center">
								<button class="btn btn-lg btn-primary button helper_step3" 
									>{{ $button_name }}</button>
							</div>

						 
						{!! Form::close() !!}
					</div>
				</div>
			</section>
		</div>
		<!-- /.container-fluid -->
	</section>
</div>
<!-- /#page-wrapper -->
@endsection

@section('footer_scripts')
 @include('common.validations')
  @include('common.alertify')
 <script>
 	var file = document.getElementById('excel_input');

file.onchange = function(e){
    var ext = this.value.match(/\.([^\.]+)$/)[1];
    switch(ext)
    {
        case 'xls':
        case 'xlsx':
            
            break;
        default:
               alertify.error("{{getPhrase('file_type_not_allowed')}}");
            this.value='';
    }
};
 </script>
@stop