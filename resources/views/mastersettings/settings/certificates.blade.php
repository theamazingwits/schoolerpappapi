@extends($layout)
@section('content')

<div id="page-wrapper">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
        <div class="container-fluid content-wrap">
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						 <li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>
						 <li><a href="{{URL_SETTINGS_DASHBOARD}}">{{getPhrase('mastersettings_dashboard')}}</a></li>
						 <li>{{ $title}}</li>
					</ol>
				</div>
			</div>

		 	<div class="row panel-grid grid-stack">
				<section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
	                <!--Start Panel-->
	                <div class="panel pb-0 bgc-white-dark">
	                    <div class="panel-body pt-2 grid-stack-handle">
	                        <div class="h-lg pos-r panel-body-p py-0">
	                            <h3 class=""><span class="fs-2 fw-light">{{ getPhrase('bonafide__contents')}}</span> <i class="pull-right fa fa-list-alt fs-1 m-2"></i></h3>
	                            <h6 class="c-primary lh-5"></h6>
	                            <br><br>
	                            <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{URL_SETTINGS_VIEW."bonafide-content"}}">
	                                <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
	                                    <i class="icon-directions fs-4"></i>
	                                </span>
	                                <span class="d-inline-block align-top lh-7">
	                                    <span class="fs-6 d-block">View All</span>
	                                    <span class="c-gray fs-7">Click to View more details</span>
	                                </span>
	                                <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
	                            </a>
	                        </div>
	                    </div>
	                </div>
	                <!--End Panel-->
	            </section>
	            <section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
	                <!--Start Panel-->
	                <div class="panel pb-0 bgc-white-dark">
	                    <div class="panel-body pt-2 grid-stack-handle">
	                        <div class="h-lg pos-r panel-body-p py-0">
	                            <h3 class=""><span class="fs-2 fw-light">{{ getPhrase('bonafide_settings')}}</span> <i class="pull-right fa fa-cogs fs-1 m-2"></i></h3>
	                            <h6 class="c-primary lh-5"></h6>
	                            <br><br>
	                            <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{URL_SETTINGS_VIEW."bonafide-settings"}}">
	                                <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
	                                    <i class="icon-directions fs-4"></i>
	                                </span>
	                                <span class="d-inline-block align-top lh-7">
	                                    <span class="fs-6 d-block">View All</span>
	                                    <span class="c-gray fs-7">Click to View more details</span>
	                                </span>
	                                <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
	                            </a>
	                        </div>
	                    </div>
	                </div>
	                <!--End Panel-->
	            </section>
	            <section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
	                <!--Start Panel-->
	                <div class="panel pb-0 bgc-white-dark">
	                    <div class="panel-body pt-2 grid-stack-handle">
	                        <div class="h-lg pos-r panel-body-p py-0">
	                            <h3 class=""><span class="fs-2 fw-light">{{ getPhrase('transfer_certificate_fields')}}</span> <i class="pull-right fa fa-newspaper-o fs-1 m-2"></i></h3>
	                            <h6 class="c-primary lh-5"></h6>
	                            <br><br>
	                            <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{URL_SETTINGS_VIEW."transfer-certificate-fields"}}">
	                                <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
	                                    <i class="icon-directions fs-4"></i>
	                                </span>
	                                <span class="d-inline-block align-top lh-7">
	                                    <span class="fs-6 d-block">View All</span>
	                                    <span class="c-gray fs-7">Click to View more details</span>
	                                </span>
	                                <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
	                            </a>
	                        </div>
	                    </div>
	                </div>
	                <!--End Panel-->
	            </section>
	            <section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
	                <!--Start Panel-->
	                <div class="panel pb-0 bgc-white-dark">
	                    <div class="panel-body pt-2 grid-stack-handle">
	                        <div class="h-lg pos-r panel-body-p py-0">
	                            <h3 class=""><span class="fs-2 fw-light">{{ getPhrase('transfer_certificate_settings')}}</span> <i class="pull-right fa fa-wrench fs-1 m-2"></i></h3>
	                            <h6 class="c-primary lh-5"></h6>
	                            <br><br>
	                            <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{URL_SETTINGS_VIEW."transfer-certificate-settings"}}">
	                                <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
	                                    <i class="icon-directions fs-4"></i>
	                                </span>
	                                <span class="d-inline-block align-top lh-7">
	                                    <span class="fs-6 d-block">View All</span>
	                                    <span class="c-gray fs-7">Click to View more details</span>
	                                </span>
	                                <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
	                            </a>
	                        </div>
	                    </div>
	                </div>
	                <!--End Panel-->
	            </section>
	            <section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
	                <!--Start Panel-->
	                <div class="panel pb-0 bgc-white-dark">
	                    <div class="panel-body pt-2 grid-stack-handle">
	                        <div class="h-lg pos-r panel-body-p py-0">
	                            <h3 class=""><span class="fs-2 fw-light">{{ getPhrase('id_card_fields')}}</span> <i class="pull-right fa fa-id-card fs-1 m-2"></i></h3>
	                            <h6 class="c-primary lh-5"></h6>
	                            <br><br>
	                            <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{URL_SETTINGS_VIEW."id-card-fields"}}">
	                                <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
	                                    <i class="icon-directions fs-4"></i>
	                                </span>
	                                <span class="d-inline-block align-top lh-7">
	                                    <span class="fs-6 d-block">View All</span>
	                                    <span class="c-gray fs-7">Click to View more details</span>
	                                </span>
	                                <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
	                            </a>
	                        </div>
	                    </div>
	                </div>
	                <!--End Panel-->
	            </section>
	            <section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
	                <!--Start Panel-->
	                <div class="panel pb-0 bgc-white-dark">
	                    <div class="panel-body pt-2 grid-stack-handle">
	                        <div class="h-lg pos-r panel-body-p py-0">
	                            <h3 class=""><span class="fs-2 fw-light">{{ getPhrase('id_card_settings')}}</span> <i class="pull-right fa fa-gear fs-1 m-2"></i></h3>
	                            <h6 class="c-primary lh-5"></h6>
	                            <br><br>
	                            <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{URL_SETTINGS_VIEW."id-card-settings"}}">
	                                <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
	                                    <i class="icon-directions fs-4"></i>
	                                </span>
	                                <span class="d-inline-block align-top lh-7">
	                                    <span class="fs-6 d-block">View All</span>
	                                    <span class="c-gray fs-7">Click to View more details</span>
	                                </span>
	                                <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
	                            </a>
	                        </div>
	                    </div>
	                </div>
	                <!--End Panel-->
	            </section>

			</div>
		</div>
	</section>
</div>

				
@stop

@section('footer_scripts')
 
@stop
