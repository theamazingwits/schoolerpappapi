@extends($layout)

@section('header_scripts')
<link href="{{CSS}}animate.css" rel="stylesheet">
@stop
@section('custom_div')
<div ng-controller="academicCourses" ng-init="ingAngData({{$items}})">
@stop
<?php
	if($right_bar === TRUE){
		$column = "col-xl-8";
		$column1 = "col-xl-6";
	}else{
		$column = "col-xl-12";
		$column1 = "col-xl-4";
	}
?>
@section('content')
<div id="page-wrapper" >
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
        <div class="container-fluid content-wrap">
			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>
						<li><a href="{{URL_COURSES_DASHBOARD}}">{{getphrase('master_setup_dashboard')}}</a></li>
						<li><a href="{{URL_MASTERSETTINGS_ACADEMICS}}">{{ getPhrase('academics')}}</a> </li>
						<li class="active">{{isset($title) ? $title : ''}}</li>
					</ol>
				</div>
			</div>
				@include('errors.errors')
			<!-- /.row -->
			
			<section class="col-sm-12 col-md-12 col-lg-12 {{$column}} panel-wrap panel-grid-item">
                <!--Start Panel-->
                <div class="panel bgc-white-dark academia_visiblelist_fix" data-spy="affix" data-offset-top="0" >
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                        <h2 class="pull-left"> {{ $title }} </h2>
                        <div class="pull-right messages-buttons helper_step1">
							<a href="{{URL_MASTERSETTINGS_ACADEMICS}}" class="btn  btn-primary button panel-header-button" >{{ getPhrase('list')}}</a>
						</div>
                    </div>
                    <div class="panel-body panel-body-p vertical-scroll" id="app">
					<?php $button_name = getPhrase('create'); ?>
				
				 	<?php $button_name = getPhrase('update'); ?>
					{{ Form::model($record, 
					array('url' => URL_MASTERSETTINGS_ACADEMICS_COURSES.$record->slug, 
					'method'=>'post')) }}
			        <div class="row">
			        	<div class="col-md-12 helper_step2">
							<h2 class="selected-item-title">{{$record->academic_year_title}}</h2>
					       	<div class='containerVertical' id="target"  ng-drop="true" ng-drop-success="onDropComplete($data,$event)">
					   			<div ng-if="!allocated_courses.length" class="subject-placeholder"> {{getPhrase('no_item_selected')}}</div>
					       		<div ng-repeat="item in allocated_courses" class="items-sub" id="allocated_courses-@{{item.id}}">@{{item.course_title}}
					           		<input type="hidden" name="selected_list[]" data-myname="@{{item.course_title}}" value="@{{item.id}}">
					           		<input type="hidden" name="parent_list[]" value="@{{item.parent_id}}">
					           		<i class="fa fa-trash text-danger pull-right" ng-click="removeItem(item,'{{$record->id}}')"></i>
					       		</div>

					 		</div>

			   		 	</div>
			        </div>
						<p>&nbsp;</p>
				    <div class="text-center">
				    	<button class="btn btn-primary btn-lg helper_step3">{{getPhrase('update')}}</button>

				    </div>

				 
					{!! Form::close() !!}
				</div>
			</div>
		</section>
		@if(isset($right_bar))
		<section class="col-sm-12 col-md-12 col-lg-12 col-xl-4 panel-wrap panel-grid-item">
            <!--Start Panel-->
            <div class="panel bgc-white-dark">
                <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                    <h2 class="pull-left"> {{ $title }} </h2>
                    
                    <!--Start panel icons-->
                    <!--End panel icons-->
                </div>
                <div class="panel-body panel-body-p">
                	<?php $data = '';

					if(isset($right_bar_data))

						$data = $right_bar_data;

					?>

					@include($right_bar_path, array('data' => $data))
                </div>
            </div>
        </section>
        @endif
	</div>
	<!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

	
@stop

@section('custom_div_end')
</div>
@stop

@section('footer_scripts')
@include('mastersettings.academic-courses.scripts.js-scripts')
@include('common.alertify')

@include('common.affix-window-size-script', array('newId'=>'app'))
 @stop