@extends($layout)
@section('content')

<div id="page-wrapper" >
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
	    <div class="container-fluid content-wrap">
			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>
						<li><a href="{{URL_COURSES_DASHBOARD}}">{{getPhrase('master_setup_dashboard')}}</a></li>
						<li><a href="{{URL_MASTERSETTINGS_COURSE_SUBJECTS."staff"}}">{{ getPhrase('allocate_staff_to_subject')}}</a> </li>
						
						<li>{{ $title }}</li>
					</ol>
				</div>
			</div>
					 	
			<!-- /.row -->	
			<section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item " >
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                        <h2 class="pull-left"> {{ $title }} </h2>
                        
                        <div class="pull-right messages-buttons">
						 
							<a href="{{URL_MASTERSETTINGS_COURSE_SUBJECTS_ADD}}" class="btn  btn-primary button helper_step1 panel-header-button" >{{ getPhrase('allocate_subject_to_course')}}</a>
							 
						</div>
                        <!--End panel icons-->
                    </div>
                    <div class="panel-body panel-body-p packages">

						@if(!count($topics))
						<h4>{{getPhrase('no_topics_available')}}</h4>
						@endif

						@foreach($topics as $topic)

						<h4 class="title" id="helper_step2">{{$topic->topic_name}}</h4>
						<ul class="row topic-list">

						@foreach($topic->childs as $child_topic)

						@if(!count($child_topic->topic_name))
						<h4>{{getPhrase('no_topics_available')}}</h4>
						@endif

							<li class="col-md-6" >
								<div class="topics clearfix">
								 
								<h4 id="helper_step3">{{$child_topic->topic_name}}</h4>
								
								</div>

							</li>
						@endforeach
						
						</ul>

						@endforeach
			  
					</div>


				</div>
			</section>


		</div>
		<!-- /.container-fluid -->
	</section>
</div>
@stop
@section('footer_scripts')

 @stop
