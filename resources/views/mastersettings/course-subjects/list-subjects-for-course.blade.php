@extends($layout)
 
@section('content')

<div id="page-wrapper">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
	    <div class="container-fluid content-wrap">
			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>
						<li><a href="{{URL_COURSES_DASHBOARD}}">{{getPhrase('master_setup_dashboard')}}</a></li>
						
						<li><a href="{{URL_MASTERSETTINGS_COURSE_SUBJECTS."staff"}}">{{ getPhrase('allocate_staff_to_subject')}}</a> </li>
						<li>{{ $title }}</li>
					</ol>
				</div>
			</div>
							
			<!-- /.row -->
			<section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item " >
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                        <h2 class="pull-left"> {{$subject->subject_title}} </h2>
                        <!--End panel icons-->
                    </div>
                    <div class="panel-body panel-body-p packages">
				
						 @for($yearno = 1; $yearno <= $record->course_dueration; $yearno++)
							<h4> {{ getPhrase('year').' '. $yearno }} </h4>
							
							@if($record->is_having_semister)
							<?php 
							$semisters = App\CourseSemister::getCourseYearSemisters($course_id, $yearno);
							?>

								@for($semister=0; $semister < $semisters->total_semisters; $semister++)
								<small>SEM {{ $semister+1 }}</small>
								@include('mastersettings.course-subjects.course-list-subview', 
								array(	
									'academic_id' 	=> $academic_id,
									'course_id' 	=> $course_id,
									'yearno' 		=> $yearno,
									'semister' 		=> $semister+1,
									))
								
						
								@endfor
								
								@if($semisters->total_semisters == 0)
								 @include('mastersettings.course-subjects.course-list-subview', 
								array(	
									'academic_id' 	=> $academic_id,
									'course_id' 	=> $course_id,
									'yearno' 		=> $yearno,
									'semister' 		=> 0,
									))
								@endif

							@else
								 @include('mastersettings.course-subjects.course-list-subview', 
								array(	
									'academic_id' 	=> $academic_id,
									'course_id' 	=> $course_id,
									'yearno' 		=> $yearno,
									'semister' 		=> 0,
									))
							@endif
						@endfor
					</div>
				</div>
			</section>
		</div>
		<!-- /.container-fluid -->
	</section>
</div>
@stop
 
 
