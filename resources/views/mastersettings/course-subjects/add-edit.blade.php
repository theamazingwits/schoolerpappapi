@extends($layout)

@section('header_scripts')
<link href="{{CSS}}animate.css" rel="stylesheet">
@stop

@section('custom_div')
<div ng-controller="courseSubjectsController" ng-init="ingAngData({{$items}})">
@stop

@section('content')
<div id="page-wrapper">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
	    <div class="container-fluid content-wrap">
			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>
						<li><a  href="{{URL_COURSES_DASHBOARD}}">{{ getPhrase('master_setup_dashboard')}}</a></li>
						<li class="active">{{isset($title) ? $title : ''}}</li>
					</ol>
				</div>
			</div>
				@include('errors.errors')
			<!-- /.row -->
			
			<section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item">
            <!--Start Panel-->
	            <div class="panel bgc-white-dark">
	                <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
	                    <h2 class="pull-left"> {{ $title }} </h2>
	                    <div class="pull-right messages-buttons">
						 	<div class="pull-right messages-buttons">
								<a href="{{URL_MASTERSETTINGS_COURSE}}" class="btn  btn-primary button panel-header-button" >{{ getPhrase('courses_list')}}</a>
							</div>
							 
						</div>
	                    <!--Start panel icons-->
	                    <!--End panel icons-->
	                </div>
	                <div class="panel-body panel-body-p" ng-controller="courseSubjectsController">
						<?php $button_name = getPhrase('update'); ?>
					 
							{!! Form::open(array('url' => URL_MASTERSETTINGS_COURSE_SUBJECTS_LOAD, 'method' => 'POST')) !!}
					 

						 @include('mastersettings.course-subjects.form_elements', 
						 array('button_name'=> $button_name),
						 array(
								'academic_years'		=> $academic_years,
						))
						 
						{!! Form::close() !!}


						@if($loadYears)
						{!! Form::open(array('url' => URL_MASTERSETTINGS_COURSE_SUBJECTS_ADD, 'method' => 'POST')) !!}
					 

						 @include('mastersettings.course-subjects.form_elements', 
						 array('button_name'=> $button_name),
						 array(
								'academic_years'		=> $academic_years,
						))
						 
						{!! Form::close() !!}

						@endif
				 

					</div>
				</div>
			</section>
		</div>
		<!-- /.container-fluid -->
	</section>
</div>
<!-- /#page-wrapper -->
@stop

 
@section('footer_scripts')
	<script src="{{JS}}angular.js"></script>
	@include('mastersettings.course-subjects.scripts.js-scripts')
@stop

@section('custom_div_end')
</div>
@stop