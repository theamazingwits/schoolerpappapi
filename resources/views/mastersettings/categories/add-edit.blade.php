@extends($layout)
@section('content')
<div id="page-wrapper">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
        <div class="container-fluid content-wrap">
			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>
						<li><a  href="{{URL_SETTINGS_DASHBOARD}}">{{ getPhrase('master_settings')}}</a></li>
						<li><a href="{{URL_MASTERSETTINGS_CATEGORIES}}">{{ getPhrase('categories')}}</a> </li>
						<li class="active">{{isset($title) ? $title : ''}}</li>
					</ol>
				</div>
			</div>
				@include('errors.errors')
			<!-- /.row -->
			
			<section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item">
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                        <h2 class="pull-left"> {{$title}} </h2>
                        <div class="pull-right messages-buttons">
							<a href="{{URL_MASTERSETTINGS_CATEGORIES}}" class="btn  btn-primary button panel-header-button" >{{ getPhrase('list')}}</a>
						</div>
                        <!--End panel icons-->
                    </div>
                    <div class="panel-body panel-body-p packages">
						<?php $button_name = getPhrase('create'); ?>
						@if ($record)
						 <?php $button_name = getPhrase('update'); ?>
							{{ Form::model($record, 
							array('url' => URL_MASTERSETTINGS_CATEGORIES_EDIT. $record->slug, 
							'method'=>'patch','name'=>'formReligion ', 'novalidate'=>'')) }}
						@else
							{!! Form::open(array('url' => URL_MASTERSETTINGS_CATEGORIES_ADD, 'method' => 'POST','name'=>'formReligion ', 'novalidate'=>'')) !!}
						@endif

						 @include('mastersettings.categories.form_elements', array('button_name'=> $button_name),
						 array())
						 
						{!! Form::close() !!}
				 

					</div>
				</div>
			</section>
		</div>
		<!-- /.container-fluid -->
	</section>
</div>
	<!-- /#page-wrapper -->
@stop

 @section('footer_scripts')
@include('common.validations')
@stop
