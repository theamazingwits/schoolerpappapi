@extends($layout)
@section('content')


<div id="page-wrapper">

    <div id="app" class="reactive-app">
       
        <!--Begin Content-->
        <section id="main" class="main-wrap bgc-white-darkest" role="main">

        		
            <div class="container-fluid content-wrap">
                <div class="row">
                    <div class="col-lg-12">
                        <ol class="breadcrumb">
                            <li><a href="{{PREFIX}}"><i class="menu-icon1 fa fa-home"></i></a> </li>
                            <li class="active"> {{ $title }} </li>
                        </ol>
                    </div>
                </div>

				<!-- 	<div class="panel-heading">
						<h4>{{$title}}</h4>
					</div> -->
                <div class="row panel-grid grid-stack">
                	

  				   <!-- NEED TO BE MUST HERE THE SECTION BUT IT DOESN'T WORK -->
                    <!-- <section data-gs-min-width="3" data-gs-height="50" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                    
                        <div class="panel bgc-white-dark pb-0">
                            <div class="panel-body panel-body-p text-center grid-stack-handle">
                                <div class="text-center">
                                    <h2 class="lh-0 fw-light">Block Title</h2>
                                    <h4 class="c-info">Sub Title</h4>
                                    <span class="d-block pt-4 pb-3"><i class="ion-ios-world-outline display-1 c-gray-light"></i></span>
                                   
                                    <a href="" class="btn btn-info btn-lg my-4">Action2</a>
                                </div>
                            </div>
                        </div>
                    </section> -->
                    <!--END  -->
					<?php $settings = getSettings('lms'); ?>
                	@if(count($categories))
                    @foreach($categories as $c)
                    <section data-gs-min-width="3" data-gs-height="44" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel bgc-white-dark pb-0">

                            <div class="panel-body panel-body-p text-center grid-stack-handle">

                        		<div class="item-image">
									<?php 
			                            $image = IMAGE_PATH_UPLOAD_LMS_DEFAULT;
			                            if($c->image)
			                            $image = IMAGE_PATH_UPLOAD_LMS_CATEGORIES.$c->image;
			                         ?>
									<img class="item-image" src="{{ $image}}" alt="" style="width: 20em;height: 22em;">
								</div>
                                <div class="mt-1">
                                	<div class="pull-left">
                                    	<h4>{{ $c->category }}</h4>
                                	</div>
                                </div>
                            </div>

                            <div class="text-center">
                                <a href="{{URL_STUDENT_LMS_CATEGORIES_VIEW.$c->slug}}" class="btn btn-primary btn-lg mt-2">{{ getPhrase('view_details')}}</a>
                            </div>
                        </div>
                        <!--End Panel-->
                    </section>
                    @endforeach
                        @else
                        Ooops...! {{getPhrase('No_Categories_available')}}
						
						<a href="{{URL_USERS_SETTINGS.$user->slug}}" >{{getPhrase('click_here_to_change_your_preferences')}}</a>
                        @endif 
						</div>
						 @if(count($categories))
                        {!! $categories->links() !!}
                        @endif


                </div>
            </div>
        </section>
        <!--End Content-->
    </div>
</div>

@stop