@extends($layout)
@section('content')

<div id="page-wrapper">
 	<section id="main" class="main-wrap bgc-white-darkest" role="main">
    	<div class="container-fluid content-wrap">
			<!-- Page Heading -->
			<div class="">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="{{PREFIX}}"><i class="bc-home fa fa-home"></i></a> </li>
						 
						<li class="active"> {{ $title }} </li>
					</ol>
				</div>
			</div>
			<div class=" panel-grid" id="panel-grid">
				<section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item">
	                <!--Start Panel-->
	                <div class="panel bgc-white-dark">
	                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
	                        <h2 class="pull-left">{{ getPhrase('lms series')}}</h2>
	                        
	                        <!--Start panel icons-->
	                        <div class="panel-icons panel-icon-slide bgc-white-dark">
	                            <ul>
	                                <li><a href=""><i class="fa fa-angle-left"></i></a>
	                                    <ul>
	                                        <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
	                                        <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
	                                        <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
	                                        <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
	                                        <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li>
	                                        <!-- <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li> -->
	                                    </ul>
	                                </li>
	                            </ul>
	                        </div>
	                        <!--End panel icons-->
	                    </div>
	                    <div class="panel-body panel-body-p">
							<div class="row library-items">
								<?php $settings = getSettings('lms'); ?>
								@if(count($series))
								<?php $entry_count = 0;?>
								@foreach($series as $c)
								@if($c->total_items)
								<div class="col-md-3">
									<div class="library-item mouseover-box-shadow">
										<div class="">
											<div class="item-image">
												@if($c->is_paid)
												<div class="label-primary label-band">{{getPhrase('premium')}}</div>
												@else
												<div class="label-danger  label-band">{{getPhrase('free')}}</div>
												@endif	
												<?php $image = $settings->defaultCategoryImage;
												if(isset($c->image) && $c->image!='')
													$image = $c->image;
												?>
												<img src="{{ IMAGE_PATH_UPLOAD_LMS_SERIES.$image}}" alt="{{$c->title}}">
												<div class="hover-content"> 
													<div class="buttons">
														<a href="{{URL_STUDENT_LMS_SERIES_VIEW.$c->slug}}" class="btn btn-primary">{{getPhrase('view_more')}}</a> 
													 
													</div>
												</div>
											</div>
											<div class="item-details">
												<h3>{{ $c->title }}</h3>
												<div class="quiz-short-discription">
												{!!$c->short_description!!}
												</div>
												<ul>
													<li><i class="icon-bookmark"></i> {{ $c->total_items.' '.getPhrase('items')}}</li>
												</ul>
											
											</div>
										</div>
									</div>
								</div>
								@endif
								@endforeach
								@else 
								Ooops...! {{getPhrase('No_series_available')}}
								<a href="{{URL_USERS_SETTINGS.$user->slug}}" >{{getPhrase('click_here_to_change_your_preferences')}}</a>
								@endif
							</div>
							@if(count($series))
							{!! $series->links() !!}
							@endif
						</div>
					</div>
				</section>
			</div>
		</div>
	</section>
</div>
		<!-- /#page-wrapper -->

@stop