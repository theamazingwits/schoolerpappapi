@extends($layout)

@section('header_scripts')

@stop

@section('content')
<div id="page-wrapper" ng-controller="TabController">
    <section id="main" class="main-wrap bgc-white-darkest" role="main">
        <div class="container-fluid content-wrap">
            <div class="row">
                <div class="col-lg-12">
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{PREFIX}}">
                                <i class="fa fa-home">
                                </i>
                            </a>
                        </li>
                        @if(checkRole(getUserGrade(2)))
                          <li>
                            <a href="{{URL_ACADEMICOPERATIONS_DASHBOARD}}">
                                {{getPhrase('academic_operations')}}
                            </a>
                        </li>
                        @endif
                       
                        <li>
                            
                                {{$title}}
                            
                        </li>
                    </ol>
                </div>
            </div>
        
            
           {!! Form::open(array('url' => URL_PRINT_STUDENTS_PAIDFEE_CLASSWISE, 'method' => 'POST', 'name'=>'htmlform ','target'=>'_blank', 'id'=>'htmlform', 'novalidate'=>'')) !!}

            <section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item">
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                        <h2 class="pull-left"> {{ getPhrase('select_details') }} </h2>
                        <!--End panel icons-->
                    </div>
                    <div class="panel-body panel-body-p">
                        <div class="row">
                        <fieldset class="form-group col-md-6">

                             {{ Form::label('feecategories', getphrase('fee_category')) }}

                             <span class="text-red">*</span>

                             {{ Form::select('feecategories', $feecategories, null, 
                             ['class'=>'form-control',
                             "id"=>"feecategories", 
                             "ng-model"=>"feecategories", 
                             "ng-change" => "categoryChanged(feecategories)",
                             'required'=> 'true', 

                             ])}}



                        </fieldset>
                        </div>

                        <div ng-show="result_data.length>0" class="row">

                             <div class="col-sm-4 col-sm-offset-8">
                                <div class="input-group">
                                    <input type="text" ng-model="search" class="form-control input-lg" placeholder="{{getPhrase('search')}}" name="search" />
                                </div>
                            </div>
                        </div>
                        <br>

                        <div ng-if="result_data.length!=0">
                            <div>
                                <div class="row">
                                    <div class="col-md-3">

                                        {{getPhrase('add_total_blank_columns')}}: 


                                        <input type="number" name="extracols" id="extracols"  ng-model="total_blank_columns" ng-change="addColumns(total_blank_columns)" class="form-control " ng-init="total_blank_columns=0;addColumns(0); " value="0" >
                                    </div>
                                </div>

                                <br>
                                <br>



                                <div class="row vertical-scroll">

                                    <h4 ng-if="result_data[0].course_dueration<=1" style="text-align: center;"><u>@{{class_title}}</u></h4>
                                    <h4 ng-if="result_data[0].course_dueration>1" style="text-align: center;"><u>@{{class_title_yer_sem}}</u></h4>

                                    <table class="table table-bordered" style="border-collapse: collapse;">
                                        <thead>
                                            <th style="border:1px solid #000;text-align: center;"><b>{{getPhrase('sno')}}</b></th>
                                            <th style="border:1px solid #000;text-align: center;"><b>{{getPhrase('name')}}</b></th>

                                            <th style="border:1px solid #000;text-align: center;"><b>{{getPhrase('roll_no')}}</b></th>
                                            <th style="border:1px solid #000;text-align: center;"><b>{{getPhrase('amount')}}</b></th>
                                            <th style="border:1px solid #000;text-align: center;"><b>{{getPhrase('paid_amount')}}</b></th>
                                            <th style="border:1px solid #000;text-align: center;"><b>{{getPhrase('discount')}}</b></th>
                                            <th style="border:1px solid #000;text-align: center;"><b>{{getPhrase('balance')}}</b></th>
                                            <th style="border:1px solid #000;text-align: center;"><b>%</b></th>
                                            <th style="border:1px solid #000;text-align: center;" ng-repeat="col in blank_columns"></th>


                                        </thead>
                                        <tbody>


                                            <?php $currency  = getCurrencyCode();
                                            ?>

                                            <tr ng-repeat="user in result_data | filter:search track by $index">


                                               <td style="border:1px solid #000;text-align: center;" >@{{$index+1}}</td>
                                               <td style="border:1px solid #000;text-align: center;"><a target="_blank" href="{{URL_USER_DETAILS}}@{{user.slug}}">@{{user.name}}</a></td>

                                               <td style="border:1px solid #000;text-align: center;">@{{user.roll_no}}</td>
                                               <td style="border:1px solid #000;text-align: center;">{{$currency}} @{{user.amount}}</td>
                                               <td style="border:1px solid #000;text-align: center;">{{$currency}} @{{user.paid_amount}}</td>
                                               <td style="border:1px solid #000;text-align: center;">{{$currency}} @{{user.discount_amount}}</td>
                                               <td style="border:1px solid #000;text-align: center;">{{$currency}} @{{user.balance}}</td>
                                               <td style="border:1px solid #000;">

                                                <div class="progress">
                                                  <div  ng-class="{'progress-bar progress-bar-success':user.paid_percentage>=75, 'progress-bar progress-bar-warning':user.paid_percentage<75 && user.paid_percentage>=50, 'progress-bar progress-bar-danger':user.paid_percentage<50 && user.paid_percentage>=0}" role="progressbar" aria-valuenow="@{{user.paid_percentage}}"
                                                      aria-valuemin="0" aria-valuemax="100" style="width:@{{user.paid_percentage}}%">
                                                      @{{user.paid_percentage}}%
                                                  </div>
                                              </div>

                                              </td>
                                              <td style="border:1px solid #000;text-align: center;" ng-repeat="col in blank_columns">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                              </td>



                                            </tr> 

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div ng-if="result_data.length==0" class="text-center" >{{getPhrase('no_data_available')}}</div> 
                            <br>
                            <a ng-if="result_data.length!=0" class="btn btn-primary btn-lg panel-header-button" ng-click="printIt()">Print</a>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>
</div>

 
{!! Form::close() !!}

@stop
 
 

@section('footer_scripts')

  
    @include('student.fee.scripts.feepaid-history-script')
    
@stop