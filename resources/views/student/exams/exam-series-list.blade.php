@extends($layout)
@section('content')

<div id="page-wrapper">
	<div id="app" class="reactive-app">
       
        <!--Begin Content-->
        <section id="main" class="main-wrap bgc-white-darkest" role="main">
            <div class="container-fluid content-wrap">
				<div class="panel-heading">
					<h4>{{$title}}</h4>
				</div>
                <div class="row panel-grid grid-stack">
					@if(count($series))
					@foreach($series as $c)
                    <section data-gs-min-width="3" data-gs-height="44" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel bgc-white-dark pb-0">


                            <div class="panel-body panel-body-p text-center grid-stack-handle">

                        		<div class="item-image">
									@if($c->is_paid)
									<div class="label-primary label-band">{{getPhrase('premium')}}</div>
									@else
									<div class="label-danger  label-band">{{getPhrase('free')}}</div>
									@endif	

									<?php $image = IMAGE_PATH_UPLOAD_EXAMSERIES_DEFAULT;
									if(isset($c->image) && $c->image!='')
										$image = IMAGE_PATH_UPLOAD_SERIES.$c->image;
									?>
									<img src="{{$image}}" alt="{{$c->title}}">
									
									<div class="hover-content"> 
									<div class="buttons">
										<a href="{{URL_STUDENT_EXAM_SERIES_VIEW_ITEM.$c->slug}}" class="btn btn-primary">{{getPhrase('view_more')}}</a> 
									 
										</div>
									</div>
									
								</div>
                                <div class="mt-1">
                                	<h3>{{ $c->title }}</h3>
										<div class="quiz-short-discription">
										{!!$c->short_description!!}
										</div>
										<ul>
											<li><i class="icon-bookmark"></i> {{ $c->total_exams.' '.getPhrase('quizzes')}}</li>
											<li><i class="icon-eye"></i> {{ $c->total_questions.' '.getPhrase('questions')}}</li>
										</ul>
                                    <div class="pull-right">
                                    	<ul style="list-style: none; display:inline-flex;">
											<li><i class="icon-bookmark"></i> {{ $c->total_exams.' '.getPhrase('quizzes')}}</li>
											<li><i class="icon-eye"></i> {{ $c->total_questions.' '.getPhrase('questions')}}</li>
										</ul>
                                    </div>
                                </div>
                            </div>

                            <div class="text-center">
                                	<a href="{{URL_STUDENT_EXAMS.$c->slug}}" class="btn btn-primary btn-lg mt-2">{{ getPhrase('view_details')}}</a>
                            </div>
                        </div>
                        <!--End Panel-->
                    </section>
                   @endforeach
				 	@else 
					<p>&nbsp;&nbsp;&nbsp;Ooops...! {{getPhrase('No_series_available')}}</p>
					<p style="margin-top:-1em;">&nbsp;&nbsp;&nbsp;<a href="{{URL_USERS_SETTINGS.$user->slug}}" class="btn btn-primary btn-lg mt-2" >{{getPhrase('click_here_to_change_your_preferences')}}</a></p>
					@endif
			    </div>
				@if(count($series))
				{!! $series->links() !!}
				@endif
            </div>
        </section>
        <!--End Content-->
    </div>
			
</div>
		<!-- /#page-wrapper -->

@stop