@extends($layout)
@section('content')

<div id="page-wrapper">

    <div id="app" class="reactive-app">
       
        <!--Begin Content-->
        <section id="main" class="main-wrap bgc-white-darkest" role="main">

            

            <div class="container-fluid content-wrap">
                <div class="row">
                <div class="col-lg-12 ">
                <ol class="breadcrumb">
                    <li><a href="{{PREFIX}}"><i class="fa fa-home bc-home"></i> </a> </li>
                     @if(checkRole(getUserGrade(2)))
                    <li><a href="{{URL_USERS_DASHBOARD}}">{{getPhrase('users_dashboard')}}</a></li>
                      @endif
                    @if(checkRole(getUserGrade(2)))
                    <li><a href="{{URL_USERS."student"}}">{{ getPhrase('student_users') }}</a> </li>
                    @endif
                    
                    @if(checkRole(getUserGrade(7)))
                    <li><a href="{{URL_PARENT_CHILDREN}}">{{ getPhrase('children') }}</a> </li>
                    @endif

                    <li>{{ $title }} </li>
                </ol>
            </div>
        </div>



                <div class="row panel-grid grid-stack">
                	

  				   <!-- NEED TO BE MUST HERE THE SECTION BUT IT DOESN'T WORK -->
                    <!-- <section data-gs-min-width="3" data-gs-height="50" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                    
                        <div class="panel bgc-white-dark pb-0">
                            <div class="panel-body panel-body-p text-center grid-stack-handle">
                                <div class="text-center">
                                    <h2 class="lh-0 fw-light">Block Title</h2>
                                    <h4 class="c-info">Sub Title</h4>
                                    <span class="d-block pt-4 pb-3"><i class="ion-ios-world-outline display-1 c-gray-light"></i></span>
                                   
                                    <a href="" class="btn btn-info btn-lg my-4">Action2</a>
                                </div>
                            </div>
                        </div>
                    </section> -->
                    <!--END  -->

					<?php $settings = getExamSettings(); ?>
							@if(count($categories))
							@foreach($categories as $c)
                    <section data-gs-min-width="3" data-gs-height="44" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel bgc-white-dark pb-0">


                            <div class="panel-body panel-body-p text-center grid-stack-handle">

                        		<div class="item-image">
									<?php $image = $settings->defaultCategoryImage;
									if(isset($c->image) && $c->image!='')
										$image = $c->image;
									?>
									<img src="{{ PREFIX.$settings->categoryImagepath.$image}}" alt="">
								</div>
                                <div class="mt-1">
                                	<div class="pull-left">
                                    	<h4>{{ $c->category }}</h4>
                                	</div>
                                    <div class="pull-right">
                                    	<ul style="list-style: none; display:inline-flex;">
											<li class="mr-2"><i class="icon-bookmark"></i> {{ count($c->quizzes()).' '.getPhrase('quizzes')}}</li>
											<li class=""><i class="icon-eye"></i> {{getPhrase('view')}}</li>
										</ul>
                                    </div>
                                </div>
                            </div>

                            <div class="text-center">
                                	<a href="{{URL_STUDENT_EXAMS.$c->slug}}" class="btn btn-primary btn-lg mt-2">{{ getPhrase('view_details')}}</a>
                            </div>
                        </div>
                        <!--End Panel-->
                    </section>
                    @endforeach
							@else
						Ooops...! {{getPhrase('No_Categories_available')}}
						
						<a href="{{URL_USERS_SETTINGS.Auth::user()->slug}}" >{{getPhrase('click_here_to_change_your_preferences')}}</a>
						@endif 
						</div>
						@if(count($categories))
						{!! $categories->links() !!}
						@endif


                </div>
            </div>
        </section>
        <!--End Content-->
    </div>
</div>



@stop


