@extends('layouts.student.studentlayout')

@section('header_scripts')

@stop

@section('content')

  <div id="page-wrapper ">
        <!--Begin Content-->
        <section id="main" class="main-wrap bgc-white-darkest" role="main">

    
            <div class="container-fluid content-wrap">

                  	<!-- Page Heading -->

				<div class="row">

					<div class="col-lg-12">

						<ol class="breadcrumb">

							<li><a href="{{PREFIX}}"><i class="fa fa-home bc-home"></i></a> </li>

							<li> <a href="{{URL_STUDENT_EXAM_CATEGORIES}}"> {{ getPhrase('exams') }} </a></li>

							<li class="active"> {{$title}} </li>

						</ol>

					</div>

				</div>



                <div class="row panel-grid" id="panel-grid">
                    <div class="col-sm-12 col-md-12 col-lg-12 panel-wrap panel-grid-item grid-stack-item">
                        <!--Start Panel-->
                        <div class="panel bgc-white-dark">
                            <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                                <h2 class="pull-left">
                                {{getPhrase('result_for'). ' '.$title}}</h2>
                                <!--Start panel icons-->
                                <div class="panel-icons panel-icon-slide ">
                                    <ul>
                                        <li><a href=""><i class="fa fa-angle-left"></i></a>
                                            <ul>
                                                <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                                                <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                                                <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
                                                <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
                                                <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li>
                                                <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                <!--End panel icons-->
                            </div>
                           	<div class="panel-body">

							<div class="profile-details text-center">
								<div class="profile-img"><img src="{{ getProfilePath($user->image,'profile')}}" alt=""></div>

								<div class="aouther-school">

									<h2>{{ $user->name}}</h2>

									<p><span>{{$user->email}}</span></p>

								</div>

							</div>

						<hr>
 

						<div class="panel-body">

						<ul class="library-statistic">

							<li class="total-books">

								{{getPhrase('score') }} <span><?php echo sprintf('%0.2f', $record->marks_obtained, 2, '.', '');?> / {{$record->total_marks}}</span>

							</li>

							<li class="total-journals">

								{{getPhrase('percentage')}} <span><?php echo sprintf('%0.2f', $record->percentage); ?></span>

							</li>

							<li class="digital-items">

							<?php $grade_system = getSettings('general')->gradeSystem; ?>

								{{ getPhrase('result')}} <span>{{  ucfirst($record->exam_status) }}</span>

							</li>

						</ul>

				 

					<div class="row" >

					<div class="col-md-6">

					 
						 @if(isset($marks_data))

	 						<div class="row">

						

							<?php $ids=[];?>

							@for($i=0; $i<count($marks_data); $i++)

							<?php 

							$newid = 'myMarksChart'.$i;

							$mark_ids[] = $newid; ?>

							

							 

								<canvas id="{{$newid}}" width="100" height="60"></canvas>

							 



							@endfor

							</div>

						@endif



					</div>

					<div class="col-md-6">

						

					@if(isset($time_data))

	 						<div class="row">

						

							<?php $ids=[];?>

							@for($i=0; $i<count($time_data); $i++)

							<?php 

							$newid = 'myTimeChart'.$i;

							$time_ids[] = $newid; ?>

								<canvas id="{{$newid}}" width="100" height="60"></canvas>

							@endfor

							</div>

						@endif



					</div>

					</div>

					<br/>

					<div class="row">

						<div class="col-lg-12 text-center">

							<a onClick="setLocalItem('{{URL_RESULTS_VIEW_ANSWERS.$quiz->slug.'/'.$record->slug}}')" href="javascript:void(0);" class="btn t btn-primary">{{ getPhrase('view_key') }}</a>

						</div>

					</div>	


					</div>





					</div>

                        </div>
                        <!--End Panel-->
                    </div>
                </div>
            </div>
        </section>
        <!--End Content-->
      
 
    </div>

@stop



@section('footer_scripts')

   <script src="{{JS}}chart-vue.js"></script>



@if(isset($marks_data))

	@include('common.chart', array('chart_data'=>$marks_data,'ids' => $mark_ids));

@endif

@if(isset($time_data))

	@include('common.chart', array('chart_data'=>$time_data,'ids' => $time_ids));

@endif


<script>
function setLocalItem(url) {
	localStorage.setItem('redirect_url',url);
	window.close();
}
</script>

@stop
