 @extends($layout)

@section('header_scripts')



@stop

@section('content')


<div id="page-wrapper">

				

	  <section id="main" class="main-wrap bgc-white-darkest" role="main">
            <div class="container-fluid content-wrap o-hidden">
            	<div class="row bgc-white-darkest bc-row">

					<div class="col-lg-12 bc-row">

						<ol class="breadcrumb">

							<li><a href="{{PREFIX}}"><i class="fa fa-home bc-home"></i></a> </li>

							@if(checkRole(getUserGrade(2)))
                      		 <li><a href="{{URL_USERS_DASHBOARD}}">{{ getPhrase('users_dashboard') }}</a> </li>

							<li><a href="{{URL_USERS."student"}}">{{ getPhrase('student_users') }}</a> </li>
							@endif
	                  
	                   		@if(checkRole(getUserGrade(7)))
	                   		<li><a href="{{URL_PARENT_CHILDREN}}">{{ getPhrase('children') }}</a> </li>
	                   		@endif
	                       
	                  		 <li><a href="{{URL_USER_DETAILS.$user->slug}}">{{ $user->name }} {{getPhrase('details') }}</a> </li> 
	 
							<li>{{ $title}}</li>

						</ol>

					</div>

				</div>
                <div class="row panel-grid" id="panel-grid">
                   
                    <section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item">
                        <!--Start Panel-->
                        <div class="panel bgc-white-dark">
                            <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                                <h2 class="pull-left"> {{ $title.' '.getPhrase('of').' '.$user->name }} </h2>
                                
                                <!--Start panel icons-->
                                <div class="panel-icons panel-icon-slide bgc-white-dark">
                                    <ul>
                                        <li><a href=""><i class="fa fa-angle-left"></i></a>
                                            <ul>
                                                <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                                                <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                                                <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
                                                <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
                                                <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li>
                                                <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                <!--End panel icons-->
                            </div>
                            <div class="panel-body panel-body-p">
                                <!-- Nav tabs -->
                                <div class="mb-5">
                                    <ul class="nav nav-pills bgc-active-danger nav-justified bgc-white-darkest " role="tablist">
                                        <li class="nav-item">
                                            <a class=" nav-link active" data-toggle="tab" href="#academic_details" role="tab">{{getPhrase('marks')}}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#personal_details" role="tab">{{getPhrase('time')}}</a>
                                        </li>
                                    </ul>
                                    <!-- Tab panes -->
												<!--    MARKS    -->
                                    <div class="tab-content p-3 bbw-1 bc-danger">
                                        <div class="tab-pane active clearfix" id="academic_details" role="tabpanel">

											<div class="table-responsive mb-4"> 
												<table class="table table-striped table-bordered  " cellspacing="0" width="100%">

												<thead>
													<tr>
														<th>{{ getPhrase('title')}}</th>

														<th>{{ getPhrase('correct')}}</th>

														<th>{{ getPhrase('wrong')}}</th>

														<th>{{ getPhrase('not_answered')}}</th>

														<th>{{ getPhrase('total')}}</th>

													</tr>

												</thead>

												<?php foreach($subjects_display as  $r) { 

												 	$r = (object)$r;

												 	?>

												 	<tr>
												 		<td>{{$r->subject_name}}</td>

												 		<td>{{$r->correct_answers}}</td>

												 		<td>{{$r->wrong_answers}}</td>

												 		<td>{{$r->not_answered}}</td>

												 		<td> {{$r->correct_answers+$r->wrong_answers+$r->not_answered}} </td>
												 	</tr>

												<?php } ?>

												</table>

											</div>


											 @if(isset($subjects_display))

					 							<div class="row">

													<?php $ids=[];?>

													@for($i=0; $i<count($subjects_display); $i++)

													<?php 

													$newid = 'myChart'.$i;

													$ids[] = $newid; ?>

												

													<div class="col-lg-3 ">

													<canvas id="{{$newid}}" width="100" height="110"></canvas>

												</div>



												@endfor


												@endif
                                        </div>
                                    </div>

													<!--    TIME    -->
									<div class="tab-content bc-danger">
                                           
                                        <div class="tab-pane" id="personal_details" role="tabpanel">
											<div class="table-responsive mb-4"> 
												<table class="table table-striped table-bordered  " cellspacing="0" width="100%">
													<thead>
														<tr>
															<th>{{ getPhrase('title')}}</th>

															<th>{{ getPhrase('spent_on_correct')}}</th>

															<th>{{ getPhrase('spent_on_wrong')}}</th>

															<th>{{ getPhrase('spent_time')}}</th>
															
															<th>{{ getPhrase('total_time')}}</th>

														</tr>
													</thead>
												<?php foreach($subjects_display as  $r) { 

												 	$r = (object)$r;

												 	?>

												 	<tr>

												 		<td>{{$r->subject_name}}</td>

												 		<td>{{getTimeFromSeconds($r->time_spent_on_correct_answers)}}</td>

												 		<td>{{getTimeFromSeconds($r->time_spent_on_wrong_answers)}}</td>

												 		<td> {{getTimeFromSeconds($r->time_spent)}} </td>

												 		<td>{{getTimeFromSeconds($r->time_to_spend)}}</td>

												 	</tr>
												<?php } ?>
												</table>
											</div>

											@if(isset($time_data))

					 						<div class="row">

											 <h4> {{getPhrase('time_is_shown_in_seconds')}}</h4>

												<?php

											 

												 $timeids=[];?>

												@for($i=0; $i<count($time_data); $i++)

												<?php 

												$newid = 'myTimeChart'.$i;

												$timeids[] = $newid; ?>

											

												<div class="col-lg-4 ">

												<canvas id="{{$newid}}" width="100" height="110"></canvas>

											</div>

											@endfor

											@endif	
                                        </div>
                                    </div>

                                 </div>
                            </div>
                        </div>
                        <!--End Panel-->
                    </section>
                </div>
            </div>
        </section>

</div>

@endsection

@section('footer_scripts')

 @if(isset($chart_data))

	@include('common.chart', array('chart_data'=>$chart_data,'ids' => $ids));

@endif

@if(isset($time_data))

	@include('common.chart', array('chart_data'=>$time_data,'ids' => $timeids));

@endif



@stop

