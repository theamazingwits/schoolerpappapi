@extends($layout)

@section('header_scripts')

 

@stop

 

 <?php

/**

 * Varables Used

 * @submitted_answers               -- The answers submitted by the user

 * @correct_answer_questions        -- It contains overall correct answer questions id's

 * @answer_status                   -- It will have a class if the answer is wrong

 * @user_answers                    -- It will hold all the user answers specific to question

 * @time_spent_correct_answers      -- It will maintain the list of time to spend and time spent on 

 *                                     question associated to question id

 * @time_spent_wrong_answers        -- It will maintain the list of time to spend and time spent on 

 *                                     question associated to question id

 * @time_spent_not_answers          -- It will maintain the list of time to spend and time spent on 

 *                                     question associated to question id

 */

 ?>

@section('content')

    <div id="page-wrapper" class="answer-sheet" ng-controller="angExamScript" >
        <section id="main" class="main-wrap bgc-white-darkest" role="main">
        
            <div class="container-fluid content-wrap">

                <!-- Page Heading -->

                <div class="row">

                    <div class="col-lg-12">

                        <ol class="breadcrumb">

                            <li><a href="{{PREFIX}}"><i class="fa fa-home bc-home"></i></a> </li>

                            <li><a href="{{URL_STUDENT_ANALYSIS_BY_EXAM.$user_details->slug}}">{{getPhrase('analysis')}}</a></li>

                            <li>{{$exam_record->title.' '.getPhrase('answers')}}</li>

                        </ol>

                    </div>

                </div>

                <!-- /.statistic -->


                

                <div class="panel panel-custom">
                    <div class="col-sm-12 col-md-12 col-lg-12 panel-wrap panel-grid-item grid-stack-item">
                    <!--Start Panel-->
                        <div class="panel bgc-white-dark">
                            <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                                <h2 class="pull-left">{{$exam_record->title}}</h2>
                                <span class="pull-right mr-7 pt-1">
                                    <strong>{{getPhrase('result').': '.$result_record->exam_status}}</strong>
                                </span>
                                <!--Start panel icons-->
                                <div class="panel-icons panel-icon-slide ">
                                    <ul>
                                        <li><a href=""><i class="fa fa-angle-left"></i></a>
                                            <ul>
                                                <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                                                <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                                                <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
                                                <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
                                                <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li>
                                                <!-- <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li> -->
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                    <!--End panel icons-->
                            </div>
                       

                 <!--    <div class="panel-heading">

                        <h1>{{$exam_record->title}} 



                            <span class="pull-right">{{getPhrase('result').': '.$result_record->exam_status}}
 

                            </span>

                        </h1> 

                        

                        </div> -->

                                    <?php 

                                

                                    $submitted_answers = [];

                                            $answers = (array)json_decode($result_record->answers);



                                            foreach ($answers as $key => $value) {

                                                $submitted_answers[$key] = $value;

                                            }



                                    $correct_answer_questions = [];

                                    $correct_answer_questions = (array) 

                                                                json_decode($result_record->correct_answer_questions);

                                    



                                    $time_spent_correct_answers = 

                                            getArrayFromJson($result_record->time_spent_correct_answer_questions);

                                                                    

                                    $time_spent_wrong_answers = getArrayFromJson($result_record->time_spent_wrong_answer_questions);



                                    $time_spent_not_answers = getArrayFromJson($result_record->time_spent_not_answered_questions);

                                                                



                                    // print_r($time_spent_correct_answers);

                                    $question_number =0;

                                ?>

                                @foreach($questions as $question)

                                    <?php 

                                    $question_number++;

                                            $user_answers   = FALSE;

                                            $time_spent     = array();



                                            //Pull User Answers for this question

                                            if(array_key_exists($question->id, $submitted_answers)) {

                                                $user_answers = $submitted_answers[$question->id];

                                            }

            

                                            //Pull Timing details for this question for correct answers

                                            if(array_key_exists($question->id, $time_spent_correct_answers)) 

                                                $time_spent = $time_spent_correct_answers[$question->id];

                                            

                                            //Pull Timing details for this question for wrong answers

                                            if(array_key_exists($question->id, $time_spent_wrong_answers)) 

                                                $time_spent = $time_spent_wrong_answers[$question->id];

                                            

                                            //Pull Timing details for this question which are not answered

                                            if(array_key_exists($question->id, $time_spent_not_answers)) 

                                                $time_spent = $time_spent_not_answers[$question->id];

                                    



                                ?> 

                                <div class="panel-body question-ans-box" id="{{$question->id}}"  style="display:none;">

                                        <?php 

                                    

                                            $question_type = $question->question_type;



                                            $subject_record = array();

                                            foreach ($subjects as $subject) {

                                                if($subject->id == $question->subject_id) {

                                                    $subject_record = $subject;

                                                    break;

                                                }

                                            }



                                                $inject_data = array(

                                                            'question'      => $question,

                                                            'user_answers'  => $user_answers,

                                                            'subject'      => $subject_record,

                                                            'question_number' => $question_number,

                                                            'time_spent'    => $time_spent,   

                                                        );

                                            ?>
                                        
                                        @include('student.exams.results.question-metainfo',array('meta'=> $inject_data))

                                            @include('student.exams.results.'.$question_type.'-answers', $inject_data)

                                            

                                            @if($question->explanation)

                                    

                                        <div class="answer-status-container">

                                            <div class="row">

                                                <div class="col-md-12">

                                                    <div class="question-status">

                                                        <strong>{{getPhrase('explanation')}}: </strong>

                                                            {!! $question->explanation!!}

                                                    </div>

                                                </div>

                                        

                                            </div>

                                    </div>

                                    @endif

                                    

                                </div>

                                @endforeach


     

                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="d_but">
                                            <button class="btn btn-lg btn-success button prev" type="button">

                                                <i class="fa fa-chevron-left ">

                                                </i>

                                                {{getPhrase('previous')}}

                                            </button>

                                        

                                            <button class="btn btn-lg btn-success button next" type="button">

                                                {{ getPhrase('next')}}

                                                <i class="fa fa-chevron-right">

                                                </i>

                                            </button>
                                        </div>

                                        

                                    </div>

                                </div>

                             </div>
                         </div>
                     </div>

                        </hr>

                    

                </div>

                <!-- /.row -->

            </div>

            <!-- /.container-fluid -->
        </section>
    </div>

          

@stop

 

@section('footer_scripts')
@include('student.exams.results.scripts.js-scripts');
@include('common.editor')
@stop