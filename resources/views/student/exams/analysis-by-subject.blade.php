@extends($layout)

@section('header_scripts')

<link href="{{CSS}}ajax-datatables.css" rel="stylesheet">

@stop

@section('content')

  <div id="page-wrapper ">
        <!--Begin Content-->
        <section id="main" class="main-wrap bgc-white-darkest" role="main">

    
            <div class="container-fluid content-wrap">

                    <!-- Page Heading -->

				<div class="row">

					<div class="col-lg-12">

						<ol class="breadcrumb">

							<li><a href="{{PREFIX}}"><i class="bc-home fa fa-home"></i></a> </li>

							@if(checkRole(getUserGrade(2)))
                       <li><a href="{{URL_USERS_DASHBOARD}}">{{ getPhrase('users_dashboard') }}</a> </li>

					<li><a href="{{URL_USERS."student"}}">{{ getPhrase('student_users') }}</a> </li>
					@endif

                       
                   <li><a href="{{URL_USER_DETAILS.$user->slug}}">{{ $user->name }} {{getPhrase('details') }}</a> </li> 

							<li><a href="{{URL_STUDENT_ANALYSIS_BY_EXAM.$user->slug}}">{{getPhrase('analysis')}}</i></a> </li>

							<li>{{ $title}}</li>

						</ol>

					</div>

				</div>


                <div class="row panel-grid" id="panel-grid">
                    <div class="col-sm-12 col-md-12 col-lg-12 panel-wrap panel-grid-item grid-stack-item">
                        <!--Start Panel-->
                        <div class="panel bgc-white-dark">
                            <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                                <h2 class="pull-left">
                                {{ $title.' '.getPhrase('of').' '.$user->name .' '.getPhrase('in').' '.$exam_record->title.' '.getPhrase('exam') }}</h2>
                                <!--Start panel icons-->
                                <div class="panel-icons panel-icon-slide ">
                                    <ul>
                                        <li><a href=""><i class="fa fa-angle-left"></i></a>
                                            <ul>
                                                <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                                                <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                                                <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
                                                <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
                                                <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li>
                                                <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                <!--End panel icons-->
                            </div>
                            <div class="panel-body packages">

					<ul class="nav nav-tabs add-student-tabs">

							<li class="active"><a data-toggle="tab" href="#academic_details">{{getPhrase('marks')}}</a></li>

							<li><a data-toggle="tab" href="#personal_details">{{getPhrase('time')}}</a></li>

							 

					</ul>

					<div class="tab-content tab-content-style">

							<div id="academic_details" class="tab-pane fade in active">

						<div class="table-responsive"> 

						<table class="table table-striped table-bordered  " cellspacing="0" width="100%">

							<thead>

								<tr>

								 

									<th>{{ getPhrase('title')}}</th>

									<th>{{ getPhrase('correct')}}</th>

									<th>{{ getPhrase('wrong')}}</th>

									<th>{{ getPhrase('not_answered')}}</th>

									<th>{{ getPhrase('total')}}</th>

									 

									

								</tr>

							</thead>

							 <?php 



							 foreach($subjects_display as  $r) { 

							 	$r = (object)$r;

							 	?>

							 	<tr>

							 		<td>{{$r->subject_name}}</td>

							 		<td>{{$r->correct_answers}}</td>

							 		<td>{{$r->wrong_answers}}</td>

							 		<td>{{$r->not_answered}}</td>

							 		<td>{{$r->correct_answers+$r->wrong_answers+$r->not_answered}}</td>

							 	</tr>

							 	<?php } ?>

						</table>

						</div>



						<div class="row">

					

						<?php $ids=[];?>

						@for($i=0; $i<count($chart_data); $i++)

						<?php 

						$newid = 'myChart'.$i;

						$ids[] = $newid; ?>

						

						<div class="col-lg-6">

							<canvas id="{{$newid}}" width="50%" height="30"></canvas>

						</div>



						@endfor

						</div>

 						</div>

 					 	<div id="personal_details" class="tab-pane fade">

									<div class="table-responsive"> 

						<table class="table table-striped table-bordered  " cellspacing="0" width="100%">

							<thead>

								<tr>

								 

									<th>{{ getPhrase('title')}}</th>

									<th>{{ getPhrase('spent_on_correct')}}</th>

									<th>{{ getPhrase('spent_on_wrong')}}</th>

									<th>{{ getPhrase('total_time')}}</th>

									<th>{{ getPhrase('spent_time')}}</th>

									 

									

								</tr>

							</thead>

							<?php 

						 

							$sanalysis = json_decode($quizresult->subject_analysis);

							if($sanalysis)	{

							foreach($sanalysis as  $r) { 

							 	$r = (object)$r;

							  

							 	?>

							 	<tr>

							 		<td>{{App\Subject::getName($r->subject_id)}}</td>

							 		<td>{{getTimeFromSeconds($r->time_spent_correct_answers)}}</td>

							 		<td>{{getTimeFromSeconds($r->time_spent_wrong_answers)}}</td>

							 		<td>{{getTimeFromSeconds($r->time_to_spend)}}</td>

							 		<td> {{getTimeFromSeconds($r->time_spent)}} </td>

							 	</tr>

							<?php } 
							}
							?>
							 

						</table>

						</div>

							@if(isset($time_data))

 						<div class="row">

					 <h4> {{getPhrase('time_is_shown_in_seconds')}}</h4>

						<?php

						 

						 $timeids=[];?>

						@for($i=0; $i<count($time_data); $i++)

						<?php 

						$newid = 'myTimeChart'.$i;

						$timeids[] = $newid; ?>

						

						<div class="col-lg-4 ">

							<canvas id="{{$newid}}" width="100" height="110"></canvas>

						</div>



						@endfor

						</div>

						@endif

 					 </div>
                        </div>
                        <!--End Panel-->
                    </div>
                </div>
            </div>
        </section>
        <!--End Content-->
      
 
    </div>
@endsection

 



@section('footer_scripts')

 

 @include('common.chart', array($chart_data,'ids' => $ids));

@if(isset($time_data))

	@include('common.chart', array('chart_data'=>$time_data,'ids' => $timeids));

@endif

@stop

