@extends($layout)

@section('header_scripts')

<link href="{{CSS}}ajax-datatables.css" rel="stylesheet">

@stop

@section('content')

  <div id="page-wrapper ">
        <!--Begin Content-->
        <section id="main" class="main-wrap bgc-white-darkest" role="main">


            <div class="container-fluid content-wrap">

                <!-- Page Heading -->
                <div class="row">

                    <div class="col-lg-12">

                        <ol class="breadcrumb">

                            <li><a href="{{PREFIX}}"><i class="fa fa-home bc-home"></i></a> </li>

                        @if(checkRole(getUserGrade(2)))
                           <li><a href="{{URL_USERS_DASHBOARD}}">{{ getPhrase('users_dashboard') }}</a> </li>

                            <li><a href="{{URL_USERS."student"}}">{{ getPhrase('student_users') }}</a> </li>
                            @endif

                               @if(checkRole(getUserGrade(12)))
                           <li><a href="{{URL_USER_DETAILS.$user->slug}}">{{ $user->name }} {{getPhrase('details') }}</a> </li> 
                           @endif

                           @if(checkRole(getUserGrade(7)))
                           <li><a href="{{URL_PARENT_CHILDREN}}">{{ getPhrase('children') }}</a> </li>
                                    
                           <li><a href="{{URL_USER_DETAILS.$user->slug}}">{{ $user->name }} {{getPhrase('details') }}</a> </li> 
                            @endif       

                            <li>{{ $title}}</li>

                        </ol>

                    </div>

                </div>



                <div class="row panel-grid" id="panel-grid">
                    <div class="col-sm-12 col-md-12 col-lg-12 panel-wrap panel-grid-item grid-stack-item">
                        <!--Start Panel-->
                        <div class="panel bgc-white-dark">
                            <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                                <h2 class="pull-left">
                                {{ $title.' '.getPhrase('of').' '.$user->name }}</h2>
                                <!--Start panel icons-->
                                <div class="panel-icons panel-icon-slide ">
                                    <ul>
                                        <li><a href=""><i class="fa fa-angle-left"></i></a>
                                            <ul>
                                                <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                                                <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                                                <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
                                                <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
                                                <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li>
                                                <!-- <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li> -->
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                <!--End panel icons-->
                            </div>
                            <div class="panel-body panel-body-p">
                                <div class="page-size-table">
                                    <div> 
                                        <!-- <table class="table table-striped table-bordered datatable" cellspacing="0" width="100%"> -->
                                            <table class=" card-view-no-edit page-size-table table table-striped table-bordered datatable width" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>

                                                    <th>{{ getPhrase('title')}}</th>

                                                    <th>{{ getPhrase('type')}}</th>

                                                    <th>{{ getPhrase('marks')}}</th>

                                                     
                                                    <th>{{ getPhrase('result')}}</th>

                                                    <th>{{ getPhrase('action')}}</th>

                                                </tr>
                                            </thead>
                                             
                                        </table>
                                    </div>
                                    <!-- <table id="table-student-exam-scheduledexam" class="card-view-no-edit page-size-table">
                                        
                                    </table> -->
                                </div>


                                
                                <!-- <div class="page-size-table">
                                    <table id="table-student-analysis-history" class="card-view-no-edit page-size-table">
                                        
                                    </table>
                                </div> -->
                            </div>
                        </div>
                        <!--End Panel-->
                    </div>



                </div>

                <div class="row panel-grid" id="panel-grid">
                    <div class="col-sm-12 col-md-12 col-lg-12 panel-wrap panel-grid-item grid-stack-item">
                    <div class=" bgc-white-dark">
                        <div class="row">

                            <div class="col-md-12 col-md-offset-3" style="margin-left:0px;">

                                <canvas id="myChart1" style="padding:1.5em;" width="100" height="40"></canvas>

                            </div>

                        </div>
                    </div>
                </div>
                </div>

            </div>
        </section>
        <!--End Content-->
      
 
    </div>
@endsection
 

@section('footer_scripts')

 @if(!$exam_record)
 <script>
    localStorage.setItem("tag","0");
    localStorage.setItem('url',"{{URL_STUDENT_EXAM_GETATTEMPTS.$user->slug}}");
 </script>
 @include('common.datatables', array('route'=>URL_STUDENT_EXAM_GETATTEMPTS.$user->slug, 'route_as_url' => 'TRUE'))

 @else
<script>
    localStorage.setItem("tag","1");
    localStorage.setItem('url',"{{URL_STUDENT_EXAM_GETATTEMPTS.$user->slug.'/'.$exam_record->slug}}");
 </script>
 @include('common.datatables', array('route'=>URL_STUDENT_EXAM_GETATTEMPTS.$user->slug.'/'.$exam_record->slug, 'route_as_url' => 'TRUE'))

 @endif

 @include('common.chart', array($chart_data,'ids' => array('myChart1')));

<script>
    // SystemJS.import('scripts/table');
   </script>
<!-- ssss -->

@stop
