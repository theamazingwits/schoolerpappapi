@extends($layout)
@section('header_scripts')
<link href="{{CSS}}ajax-datatables.css" rel="stylesheet">


@stop
@section('content')
    <style>
        
    </style>
  <div id="page-wrapper ">
        <!--Begin Content-->
        <section id="main" class="main-wrap bgc-white-darkest" role="main">

            

            <div class="container-fluid content-wrap">

                    <!-- Page Heading -->
              <div class="row">
                    <div class="col-lg-12">
                        <ol class="breadcrumb">
                            <li><a href="{{PREFIX}}"><i class=" bc-home fa fa-home"></i></a> </li>
                            <li><a href="{{URL_STUDENT_QUIZ_DASHBOARD}}">{{getPhrase('quizzes_dashboard')}}</a></li>
                            
                            <li><a href="{{URL_STUDENT_EXAM_CATEGORIES}}"> {{getPhrase('quiz_categories')}} </a> </li>

                            <li>{{ $title }}</li>
                        </ol>
                    </div>
                </div>


                <div class="row panel-grid" id="panel-grid">
                    <div class="col-sm-12 col-md-12 col-lg-12 panel-wrap panel-grid-item grid-stack-item">
                        <!--Start Panel-->
                        <div class="panel bgc-white-dark">
                            <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                                <h2 class="pull-left">
                                {{$title}}</h2>
                                <!--Start panel icons-->
                                <div class="panel-icons panel-icon-slide ">
                                    <ul>
                                        <li><a href=""><i class="fa fa-angle-left"></i></a>
                                            <ul>
                                                <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                                                <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                                                <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
                                                <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
                                                <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li>
                                                <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                <!--End panel icons-->
                            </div>
                            <div class="panel-body pan">
                                <div class="page-size-table">
                                    <div> 
                                        <table class=" card-view-no-edit page-size-table
                                                table table-striped table-bordered datatable width" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>{{ getPhrase('title')}}</th>
                                                    <th>{{ getPhrase('duration')}}</th>
                                                    <th>{{ getPhrase('category')}}</th>
                                                    <th>{{ getPhrase('type')}}</th>
                                                    <th>{{ getPhrase('total_marks')}}</th>
                                                    <th>{{ getPhrase('action')}}</th>
                                                  
                                                </tr>
                                            </thead>
                                             
                                        </table>
                                    </div>
                                    <!-- <table id="table-student-exam-scheduledexam" class="card-view-no-edit page-size-table">
                                        
                                    </table> -->
                                </div>
                            </div>
                        </div>
                        <!--End Panel-->
                    </div>
                </div>
            </div>
        </section>
        <!--End Content-->
      
 
    </div>
@endsection
 

@section('footer_scripts')
  @if(isset($category))
 @include('common.datatables', array('route'=>URL_STUDENT_QUIZ_GETLIST.$category->slug, 'route_as_url' => TRUE))
 @elseif(isset($user) && $user)
 @include('common.datatables', array('route'=>URL_QUIZ_LOAD_SCHEDULED_EXAMS.$user->slug, 'route_as_url' => TRUE))
 @else
 @include('common.datatables', array('route'=>URL_STUDENT_QUIZ_GETLIST_ALL, 'route_as_url' => TRUE))
 @endif
 @include('common.deletescript', array('route'=>URL_QUIZ_DELETE))

<script >
function showInstructions(url) {
    window.open(url,'_blank',"width=1200,height=800,directories=no,titlebar=no,toolbar=no,location=no,scrollbars=yes");
    runner();
}

function runner()
{
    url = localStorage.getItem('redirect_url');
    if(url) {
      localStorage.clear();
       window.location = url;
    }
    setTimeout(function() {
          runner();
    }, 500);

}
</script>

<script>
    
   // SystemJS.import('scripts/table');
   </script>

@stop
