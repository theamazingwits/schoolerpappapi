@extends($layout)
@section('header_scripts')
<link href="{{CSS}}bootstrap-datepicker.css" rel="stylesheet">
@stop
@section('content')
<div id="page-wrapper" ng-controller="TabController">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
	    <div class="container-fluid content-wrap">
			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="/"><i class="fa fa-home"></i></a> </li>
						<li><a href="{{URL_CERTIFICATE_NOTIFICATION}}">{{ getPhrase('certificate_notifications')}}</a></li>
						<li class="active">{{isset($title) ? $title : ''}}</li>
					</ol>
				</div>
			</div>
				@include('errors.errors')
			<!-- /.row -->
			
			<section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item " >
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                        <h2 class="pull-left"> {{ $title }} </h2>   
                        <div class="pull-right messages-buttons">
							<a href="{{URL_CERTIFICATE_NOTIFICATION}}" class="btn  btn-primary button panel-header-button" >{{ getPhrase('list')}}</a>
						</div>               
                        <!--End panel icons-->
                    </div>
                    <div class="panel-body panel-body-p packages">
						<?php $button_name = getPhrase('create'); ?>
						@if ($record)
						 <?php $button_name = getPhrase('update'); ?>
							{{ Form::model($record, 
							array('url' => URL_CERTIFICATE_NOTIFICATION_EDIT.$record->slug, 
							'method'=>'patch', 'name'=>'formLocations ', 'novalidate'=>'')) }}
						@else
							{!! Form::open(array('url' => URL_CERTIFICATE_NOTIFICATION_ADD, 'method' => 'POST', 'name'=>'formLocations ', 'novalidate'=>'')) !!}
						@endif
						

						 @include('cverification.notifications.form_elements', 
						 array('button_name'=> $button_name),
						 array(
						 	'record'=> $record, 
						 	'roles'=> $roles, 
						 	'academic_years'=> $academic_years, 
						 ))
						 		
						{!! Form::close() !!}
					</div>

				</div>
			</section>
		</div>
		<!-- /.container-fluid -->
	</section>
</div>
<!-- /#page-wrapper -->
@stop

@section('footer_scripts')


@include('common.angular-factory')
<script>

 app.controller('TabController', function ($scope, $http, httpPreConfig)
 {
        @include('common.js-script-year-selection')

	      $scope.to_years             = [];
	      $scope.to_selected_year     = '';
	      $scope.to_semisters         = [];
	      $scope.to_selected_semister = '';
	      $scope.to_course            = [];
	      $scope.to_course_id         = '';
	      $scope.to_selected_course_id='';
	      $scope.to_total_years = 0;
	      $scope.to_total_semisters = 0;
	      $scope.current_user = {};
	      $scope.current_user_remarks = '';


          $scope.doCall     = function () {
        }
  }); 

</script>

 @include('common.validations',array('isLoaded'=>TRUE))
 

@stop
 
 