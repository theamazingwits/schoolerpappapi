 				
               
  <div class="row ">
 					 <fieldset class="form-group col-md-12">

						{{ Form::label('title', getphrase('title')) }}

						<span class="text-red">*</span>

						{{ Form::text('title', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('title'),

							'ng-model'=>'title', 


							'required'=> 'true', 

							'ng-class'=>'{"has-error": formLocations.title.$touched && formLocations.title.$invalid}',

						

							)) }}

						<div class="validation-error" ng-messages="formLocations.title.$error" >

	    					{!! getValidationMessage()!!}


						</div>

					</fieldset>	


					<fieldset class="form-group col-md-12">

                            {{ Form::label('role_id', getphrase('user_type')) }}
                            <span class="text-red">*</span>
                            {{Form::select('role_id', $roles, null, ['class'=>'form-control', 'id'=>'role_id',
                                'placeholder'=>'Select',
                                'ng-model'=>'role_id',
                                'required'=> 'true', 
                                'ng-class'=>'{"has-error": formLocations.role_id.$touched && formLocations.role_id.$invalid}'
                            ])}}
                             <div class="validation-error" ng-messages="formLocations.role_id.$error" >
                                {!! getValidationMessage()!!}
                            </div>

                        
                  </fieldset>

      </div >       

           <div ng-if="role_id == 5">

              @include('cverification.notifications.year-selection')

            </div>
                

			<div class="row">

					<fieldset class="form-group col-md-12">
						
						{{ Form::label('description', getphrase('description')) }}
						
						{{ Form::textarea('description', $value = null , $attributes = array('class'=>'form-control', 'rows'=>'5', 'placeholder' => 'Description','id'=>'description')) }}
					</fieldset>

					</div>	

				

					<input type="hidden" name="added_by" value="{{ Auth::user()->id }}">


						<div class="buttons text-center">

							<button class="btn btn-lg btn-primary button"

							ng-disabled='!formLocations.$valid'>{{ $button_name }}</button>

						</div>

		 