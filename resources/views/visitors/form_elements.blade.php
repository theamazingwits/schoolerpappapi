 				



 					 <fieldset class="form-group col-md-12">

						{{ Form::label('name', getphrase('name')) }}

						<span class="text-red">*</span>

						{{ Form::text('name', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('name'),

							'ng-model'=>'name', 
              
              'required'=> 'true', 

							'ng-class'=>'{"has-error": formQuiz.name.$touched && formQuiz.name.$invalid}',

							'ng-minlength' => '4',

							'ng-maxlength' => '100',

							)) }}

						<div class="validation-error" ng-messages="formQuiz.name.$error" >

	    					{!! getValidationMessage()!!}
                
                {!! getValidationMessage('minlength')!!}

	    					{!! getValidationMessage('maxlength')!!}

						</div>

					</fieldset>	



         <fieldset class="form-group col-md-12">

            {{ Form::label('email', getphrase('email')) }}

            <span class="text-red">*</span>

            {{ Form::email('email', $value = null, $attributes = array(
              
              'class'=>'form-control',

             'placeholder' => 'jack@jarvis.com',

              'ng-model'=>'email',

              'required'=> 'true', 

              'ng-class'=>'{"has-error": formQuiz.email.$touched && formQuiz.email.$invalid}',

             )) }}

             <div class="validation-error" ng-messages="formQuiz.email.$error" >

                {!! getValidationMessage()!!}

                {!! getValidationMessage('email')!!}

            </div>

          </fieldset>

       

          <fieldset class="form-group col-md-12">

            {{ Form::label('phone_number', getphrase('phone_number')) }}

            <span class="text-red">*</span>

            {{ Form::number('phone_number', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => 
            getPhrase('please_enter_10-15_digit_mobile_number'),

              'ng-model'=>'phone_number',

              'required'=> 'true', 
              
              'ng-class'=>'{"has-error": formQuiz.phone_number.$touched && formQuiz.phone_number.$invalid}',


            )) }}

            <div class="validation-error" ng-messages="formQuiz.phone_number.$error" >

                {!! getValidationMessage()!!}

            </div>

          </fieldset>


         <fieldset class="form-group col-md-12">

            {{ Form::label('coming_from', getphrase('coming_from')) }}

            <span class="text-red">*</span>

            {{ Form::text('coming_from', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('coming_from'),

              'ng-model'=>'coming_from', 
              
              'required'=> 'true', 

              'ng-class'=>'{"has-error": formQuiz.coming_from.$touched && formQuiz.coming_from.$invalid}',

              'ng-minlength' => '4',

              'ng-maxlength' => '100',

              )) }}

            <div class="validation-error" ng-messages="formQuiz.coming_from.$error" >

                {!! getValidationMessage()!!}
                
                {!! getValidationMessage('minlength')!!}

                {!! getValidationMessage('maxlength')!!}

            </div>

          </fieldset> 

        <fieldset class="form-group col-md-12 ">
                
                {{ Form::label('role', getphrase('to_meet_user_type')) }}
               
               <span class="text-red">*</span>
          
               {{Form::select('role_id', $roles, null, [

              'placeholder' => getPhrase('select_role'),

              'class'=>'form-control',

              'ng-model'=>'role_id',

              'id'=>'role_id',

              'required'=> 'true', 

              'ng-change'=>'getUsers(role_id)',

              'ng-class'=>'{"has-error": formQuiz.role_id.$touched && formQuiz.role_id.$invalid}'

             ])}}

              <div class="validation-error" ng-messages="formQuiz.role_id.$error" >

                {!! getValidationMessage()!!}

            </div>

        </fieldset>


         <fieldset ng-if = "selected_role_id" class="form-group col-md-12">
                         <label for = "user_id">{{getPhrase('users')}}</label>
                        <select 

                        name      = "user_id" 
                        id        = "user_id" 
                        class     = "form-control" 
                        ng-model  = "user_id" 
                        ng-options= "option.id as option.name for option in users track by option.id">
                        <option value="">{{getPhrase('select')}}</option>
                    
                        </select>
                    </fieldset>


          <fieldset class="form-group col-md-12 " >
                
                {{ Form::label('representing', getphrase('representing')) }}
               
               <span class="text-red">*</span>
          
               {{Form::select('representing', $representing, null, [

              'placeholder' => getPhrase('select'),

              'class'=>'form-control',

              'ng-model'=>'representing',

              'id'=>'representing',

              'required'=> 'true', 


              'ng-class'=>'{"has-error": formQuiz.representing.$touched && formQuiz.representing.$invalid}'

             ])}}

              <div class="validation-error" ng-messages="formQuiz.representing.$error" >

                {!! getValidationMessage()!!}

            </div>

        </fieldset>

        <input type="hidden" name="added_by" value="{{ Auth::user()->id }}">


          


         <div class="buttons text-center">

							<button class="btn btn-lg btn-primary button"

							ng-disabled='!formQuiz.$valid'>{{ $button_name }}</button>

						</div>

		 