@extends($layout)
@section('header_scripts')
<link href="{{CSS}}ajax-datatables.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{CSS}}select2.css">

@stop
@section('content')


<div id="page-wrapper" ng-controller="visitorManage">
 <section id="main" class="main-wrap bgc-white-darkest" role="main">
    <div class="container-fluid content-wrap">

      <!-- Page Heading -->
      <div class="row">
       <div class="col-lg-12">
        <ol class="breadcrumb">
         <li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>

         <li>{{ $title }}</li>
       </ol>
     </div>
    </div>

    @include('errors.errors')

    <!-- /.row -->
    <section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item " >
                <!--Start Panel-->
      <div class="panel bgc-white-dark">
          <div class="panel-body panel-body-p packages">

      <div class="row">

       <div class="col-md-6">


        <?php $button_name = getPhrase('create'); ?>
        @if ($record)
        <?php $button_name = getPhrase('update'); ?>
        {{ Form::model($record, 
          array('url' => URL_UPDATE_VISITOR.$record->id, 
          'method'=>'patch', 'name'=>'formQuiz ', 'novalidate'=>'','files'=>TRUE)) }}
          @else
          {!! Form::open(array('url' => URL_VISITOR_ADD, 'method' => 'POST', 'name'=>'formQuiz ', 'novalidate'=>'','files'=>TRUE)) !!}
          @endif


          @include('visitors.form_elements', 
          array('button_name'=> $button_name),
          array('record'=> $record,
          'roles'=> $roles, 
          'representing'=> $representing, 
          ))

          {!! Form::close() !!}

        </div>

        <div class="col-md-6">

         <h4>{{getPhrase('checked_in_visitors')}}</h4>
         <table class="table responsive" style="max-height: 400px; overflow-y: auto;">

          <thead>

           <th><b>{{getPhrase('visitor_id')}}</b></th>
           <th><b>{{getPhrase('name')}}</b></th>
           <th><b>{{getPhrase('phone')}}</b></th>
           <th><b>{{getPhrase('checked_in')}}</b></th>
           <th><b>{{getPhrase('action')}}</b></th>
           <th></th>

         </thead>

         <tbody>

           @if( count($visitors) > 0 )

           @foreach($visitors as $visitor)

           <tr>
             <td>{{$visitor->id}}</td>
             <td>{{ucwords($visitor->visitor_name)}}</td>
             <td>{{$visitor->phone_number}}</td>
             <td>{{$visitor->created_at}}</td>
             <td><a href="javascript:void(0)" ng-click="getVisitorDetails('{{$visitor->id}}')" class="btn btn-primary btn-sm">Details</a></td>
             <td><a href="javascript:void(0)" onclick="logoutVisitor('{{$visitor->id}}')" class="btn btn-info btn-sm">Checkout</a></td>

           </tr>

           @endforeach

           @endif  	

         </tbody>

       </table>

     </div>

    </div>



    <div> 
      <table class="table table-striped table-bordered datatable" cellspacing="0" width="100%">
       <thead>
        <tr>

         <th>{{ getPhrase('visitor_id')}}</th>
         <th>{{ getPhrase('name')}}</th>
         <th>{{ getPhrase('to_meet')}}</th>
         <th>{{ getPhrase('number')}}</th>
         <th>{{ getPhrase('check_in')}}</th>
         <th>{{ getPhrase('check_out')}}</th>
         <th>{{ getPhrase('status')}}</th>
         <th>{{ getPhrase('action')}}</th>

       </tr>
     </thead>

    </table>
    </div>

    </div>
    </div>
  </section>
</div>
</section>
<!-- /.container-fluid -->


<div id="viewVisitor" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" align="center"><b>{{getPhrase('visitor_details')}}</b></h4>
      </div>
      <div class="modal-body">
        <table class="table responsive">

          <tr>
           <td><b>{{getPhrase('visitor_id')}}</b></td>
           <td>@{{visitor.id}}</td>
         </tr>

         <tr>
           <td><b>{{getPhrase('visitor_name')}}</b></td>
           <td>@{{visitor.visitor_name}}</td>
         </tr>

         <tr>
           <td><b>{{getPhrase('phone')}}</b></td>
           <td>@{{visitor.phone_number}}</td>
         </tr>

         <tr>
           <td><b>{{getPhrase('email')}}</b></td>
           <td>@{{visitor.visitor_email}}</td>
         </tr>

         <tr>
           <td><b>{{getPhrase('coming_from')}}</b></td>
           <td>@{{visitor.coming_from}}</td>
         </tr>

         <tr>
           <td><b>{{getPhrase('to_meet')}}</b></td>
           <td>@{{visitor.user_name}}</td>
         </tr>

         <tr>
           <td><b>{{getPhrase('designation')}}</b></td>
           <td>@{{visitor.display_name}}</td>
         </tr>

         <tr>
           <td><b>{{getPhrase('representing')}}</b></td>
           <td>@{{visitor.representing}}</td>
         </tr>

       </table>
       
     </div>
     <div class="modal-footer">

      <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Ok</button>
    </div>

  </div>

</div>
</div>



<div id="logOutVisitor" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" align="center"><b>{{getPhrase('logout_visitor')}}</b></h4>
      </div>

      {!! Form::open(array('url' => URL_LOGOUT_VISITOR, 'method' => 'POST', 'name'=>'formQuiz ', 'novalidate'=>'')) !!}

      <div class="modal-body">

       <h4 align="center">{{getPhrase('are_you_sure_to_checkout_visitor')}}</h4>

       <input type="hidden" name="visitor_id" id="visitor_id">

     </div>


     <div class="modal-footer">

      <button type="button" class="btn btn-default pull-right" data-dismiss="modal">No</button>&nbsp;&nbsp;&nbsp;
      <button type="submit" class="btn btn-info pull-right" >Yes</button>
    </div>
    {!! Form::close() !!}


  </div>

</div>
</div>


<div id="deleteVisitor" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" align="center"><b>{{getPhrase('delete_visitor')}}</b></h4>
      </div>

      {!! Form::open(array('url' => URL_VISITOR_DELETE, 'method' => 'POST', 'name'=>'formQuiz ', 'novalidate'=>'')) !!}

      <div class="modal-body">

       <h4 align="center">{{getPhrase('are_you_sure_to_delete_visitor')}}</h4>

       <input type="hidden" name="myvisitor_id" id="myvisitor_id">

     </div>


     <div class="modal-footer">

      <button type="button" class="btn btn-default pull-right" data-dismiss="modal">No</button>&nbsp;&nbsp;&nbsp;
      <button type="submit" class="btn btn-info pull-right" >Yes</button>
    </div>
    {!! Form::close() !!}


  </div>

</div>
</div>

</div>
@endsection
 

@section('footer_scripts')
 @include('common.datatables', array('route'=>URL_VISITORS_HISTORY, 'route_as_url' => TRUE ))
 @include('common.validations')

  

  <script src="{{JS}}select2.js"></script>
    
    <script>
      $('#select2').select2({
       placeholder: "Add User",
    });
    </script>


    <script>

 app.controller('visitorManage', function ($scope, $http)
  {

   $scope.getUsers = function(role_id){

            $scope.selected_role_id = role_id;

            route   = '{{ URL_GET_VISITOR_MEET_USERS }}';  
            data    = {   
                   _method: 'post', 
                  '_token': $scope.getToken(),
                  'role_id': role_id, 
               };

        $http.post(route, data).then(function(result){
          // console.log(result.data);
           $scope.users  = result.data.users;

       });
   }

  
   $scope.getToken  = function(){

      return  $('[name="_token"]').val();
    }

    $scope.getVisitorDetails  = function(visitor_id){


            route   = '{{ URL_GET_VISITOR_DETAILS }}';  
            data    = {   
                   _method: 'post', 
                  '_token': $scope.getToken(),
                  'visitor_id': visitor_id, 
               };

        $http.post(route, data).then(function(result){
          // console.log(result.data);
           $scope.visitor  = result.data;

       });


        $('#viewVisitor').modal('show');
    }


    

});


 function logoutVisitor(visitor_id){

 	$('#visitor_id').val(visitor_id);

    $('#logOutVisitor').modal('show');
 }

 function deleteVisitor(visitor_id){

    	$('#myvisitor_id').val(visitor_id);

    $('#deleteVisitor').modal('show');
 }



  
</script>

@stop
