@extends($layout)
<!-- <link href="{{CSS}}bootstrap-datepicker.css" rel="stylesheet"> -->
@section('content')
<div id="page-wrapper">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
			<div class="container-fluid content-wrap">
				<!-- Page Heading -->
				<div class="row">
					<div class="col-lg-12">
						<ol class="breadcrumb">
							<li><a href="/"><i class="fa fa-home bc-home"></i></a> </li>
							<li><a href="{{URL_VECHICLES}}">{{ getPhrase('vehicles')}}</a></li>
							<li class="active">{{isset($title) ? $title : ''}}</li>
						</ol>
					</div>
				</div>
					@include('errors.errors')
				<!-- /.row -->
			<div class="row panel-grid" id="panel-grid">
			        <div class="col-sm-12 col-md-12 col-lg-12 panel-wrap panel-grid-item grid-stack-item">
				<div class="panel panel-custom col-lg-12">
					<div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
						<div class="pull-right messages-buttons mr-6">
							<a href="{{URL_VECHICLES}}" class="btn  btn-primary button" >{{ getPhrase('list')}}</a>
						</div>
						
					<h1>{{ $title }}  </h1>
					 <!--Start panel icons-->
		                        <div class="panel-icons panel-icon-slide ">
		                            <ul>
		                                <li><a href=""><i class="fa fa-angle-left"></i></a>
		                                    <ul>
		                                        <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
		                                        <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
		                                        <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
		                                        <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
		                                        <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li>
		                                       <!--  <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li> -->
		                                    </ul>
		                                </li>
		                            </ul>
		                        </div>
		                        <!--End panel icons-->

					</div>
					<div class="panel-body" >
					<?php $button_name = getPhrase('create'); ?>
					@if ($record)
					 <?php $button_name = getPhrase('update'); ?>
						{{ Form::model($record, 
						array('url' => URL_VECHICLES_EDIT.$record->id, 
						'method'=>'patch', 'name'=>'formQuiz ', 'novalidate'=>'')) }}
					@else
						{!! Form::open(array('url' => URL_VECHICLES_ADD, 'method' => 'POST', 'name'=>'formQuiz ', 'novalidate'=>'')) !!}
					@endif
					

					 @include('transport.vechicles.form_elements', 
					 array('button_name'=> $button_name),
					 array('record'=> $record, 
					 'branches'=>$branches,
					 'drivers'=>$drivers,
					 'trackdevice'=>$trackdevice,
					 'component'=>$component,
					 ))
					 		
					{!! Form::close() !!}
					</div>

				</div>
			</div>
		</div>
			</div>
			<!-- /.container-fluid -->
		</section>
		</div>
		<!-- /#page-wrapper -->
@stop

@section('footer_scripts')
 @include('common.validations')
 

@stop
 
 