 					@if(checkRole(['owner']) && App\Hostel::isMultiBranch())
					<div class="row">
 						<fieldset class="form-group col-md-12">
							{{ Form::label('branch_id', getphrase('branch')) }}
							<span class="text-red">*</span>
							{{Form::select('branch_id', $branches, null, ['class'=>'form-control', 'id'=>'branch_id',
								'ng-model'=>'branch_id',
								'required'=> 'true', 
								'ng-class'=>'{"has-error": formQuiz.branch_id.$touched && formQuiz.branch_id.$invalid}'
							])}}
							 <div class="validation-error" ng-messages="formQuiz.branch_id.$error" >
		    					{!! getValidationMessage()!!}
							</div>
						</fieldset>
 					</div>
 					@else 
						{{ Form::hidden('branch_id', getUserRecord()->branch_id, array('id'=>'branch_id')) }}
					@endif


					<div class="row">


						<fieldset class="form-group col-md-12">

						{{ Form::label('number', getphrase('number')) }}

						<span class="text-red">*</span>

						{{ Form::text('number', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => 
						'AP274541',

							'ng-model'=>'number',

							'required'=> 'true', 
							
                            'ng-class'=>'{"has-error": formQuiz.number.$touched && formQuiz.number.$invalid}',


						)) }}

						<div class="validation-error" ng-messages="formQuiz.number.$error" >

	    					{!! getValidationMessage()!!}
                      </div>

					</fieldset> 

 					 <fieldset class="form-group col-md-12">

						{{ Form::label('model', getphrase('model')) }}

						<span class="text-red">*</span>

						{{ Form::text('model', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('model'),

							'ng-model'=>'model', 


							'required'=> 'true', 

							'ng-class'=>'{"has-error": formQuiz.model.$touched && formQuiz.model.$invalid}',

							'ng-minlength' => '4',

							'ng-maxlength' => '100',

							)) }}

						<div class="validation-error" ng-messages="formQuiz.model.$error" >

	    					{!! getValidationMessage()!!}


	    					{!! getValidationMessage('minlength')!!}

	    					{!! getValidationMessage('maxlength')!!}

						</div>

					</fieldset>	

					<fieldset class="form-group col-md-12">

						{{ Form::label('seats', getphrase('seats')) }}

						<span class="text-red">*</span>

						{{ Form::number('seats', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => 
						getPhrase('5000'),

							'ng-model'=>'seats',

							'required'=> 'true', 

							'min'=>'0',
							
                            'ng-class'=>'{"has-error": formQuiz.seats.$touched && formQuiz.seats.$invalid}',


						)) }}

						<div class="validation-error" ng-messages="formQuiz.seats.$error" >

	    					{!! getValidationMessage()!!}
                      </div>

					</fieldset> 

						<fieldset class="form-group col-md-12">

						{{ Form::label('year_made', getphrase('year_made')) }}

						<span class="text-red">*</span>

						{{ Form::number('year_made', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => 
						getPhrase('5000'),

							'ng-model'=>'year_made',

							'required'=> 'true', 
							'min'=>'0',
							
                            'ng-class'=>'{"has-error": formQuiz.year_made.$touched && formQuiz.year_made.$invalid}',


						)) }}

						<div class="validation-error" ng-messages="formQuiz.year_made.$error" >

	    					{!! getValidationMessage()!!}
                      </div>

					</fieldset> 


					  <fieldset class="form-group col-md-12">

			             {{ Form::label('driver_id', getphrase('drivers')) }}

			            <span class="text-red">*</span>
			          
			           {{Form::select('driver_id', $drivers,null, ['placeholder' => getPhrase('select'),'class'=>'form-control',

			              'ng-model'=>'driver_id',

			              'id'=>'driver_id',

			              'required'=> 'true', 

			              'ng-class'=>'{"has-error": formQuiz.driver_id.$touched && formQuiz.driver_id.$invalid}'

			             ])}}

			              <div class="validation-error" ng-messages="formQuiz.driver_id.$error" >

			                {!! getValidationMessage()!!}

			            </div>

			       </fieldset>
			        <fieldset class="form-group col-md-12">

			             {{ Form::label('track_device', getphrase('select_tracking_device')) }}

			            <span class="text-red">*</span>
			          
			           {{Form::select('track_device', $trackdevice,null, ['placeholder' => getPhrase('select'),'class'=>'form-control',

			              'ng-model'=>'track_device',

			              'id'=>'track_device',

			              'required'=> 'true', 

			              'ng-class'=>'{"has-error": formQuiz.track_device.$touched && formQuiz.track_device.$invalid}'

			             ])}}

			              <div class="validation-error" ng-messages="formQuiz.track_device.$error" >

			                {!! getValidationMessage()!!}

			            </div>

			       </fieldset>
			        <fieldset class="form-group col-md-12">

			             {{ Form::label('track_student', getphrase('select_component')) }}

			            <span class="text-red">*</span>
			          
			           {{Form::select('track_student', $component,null, ['placeholder' => getPhrase('select'),'class'=>'form-control',

			              'ng-model'=>'track_student',

			              'id'=>'track_student',

			              'required'=> 'true', 

			              'ng-class'=>'{"has-error": formQuiz.track_student.$touched && formQuiz.track_student.$invalid}'

			             ])}}

			              <div class="validation-error" ng-messages="formQuiz.track_student.$error" >

			                {!! getValidationMessage()!!}

			            </div>

			       </fieldset>


					{{-- 	 <fieldset class="form-group col-md-12">

						{{ Form::label('driver_name', getphrase('driver_name')) }}

						<span class="text-red">*</span>

						{{ Form::text('driver_name', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('driver_name'),

							'ng-driver_name'=>'driver_name', 


							'required'=> 'true', 

							'ng-class'=>'{"has-error": formQuiz.driver_name.$touched && formQuiz.driver_name.$invalid}',

							'ng-minlength' => '4',

							'ng-maxlength' => '100',

							)) }}

						<div class="validation-error" ng-messages="formQuiz.driver_name.$error" >

	    					{!! getValidationMessage()!!}


	    					{!! getValidationMessage('minlength')!!}

	    					{!! getValidationMessage('maxlength')!!}

						</div>

					</fieldset>	

						 <fieldset class="form-group col-md-12">

						{{ Form::label('driver_license', getphrase('driver_license')) }}

						<span class="text-red">*</span>

						{{ Form::text('driver_license', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('driver_license'),

							'ng-driver_license'=>'driver_license', 


							'required'=> 'true', 

							'ng-class'=>'{"has-error": formQuiz.driver_license.$touched && formQuiz.driver_license.$invalid}',

							'ng-minlength' => '4',

							'ng-maxlength' => '100',

							)) }}

						<div class="validation-error" ng-messages="formQuiz.driver_license.$error" >

	    					{!! getValidationMessage()!!}


	    					{!! getValidationMessage('minlength')!!}

	    					{!! getValidationMessage('maxlength')!!}

						</div>

					</fieldset>	

						 <fieldset class="form-group col-md-12">

						{{ Form::label('driver_contact', getphrase('driver_contact')) }}

						<span class="text-red">*</span>

						{{ Form::text('driver_contact', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('driver_contact'),

							'ng-driver_contact'=>'driver_contact', 


							'required'=> 'true', 

							'ng-class'=>'{"has-error": formQuiz.driver_contact.$touched && formQuiz.driver_contact.$invalid}',

							'ng-minlength' => '4',

							'ng-maxlength' => '100',

							)) }}

						<div class="validation-error" ng-messages="formQuiz.driver_contact.$error" >

	    					{!! getValidationMessage()!!}


	    					{!! getValidationMessage('minlength')!!}

	    					{!! getValidationMessage('maxlength')!!}

						</div>

					</fieldset>	 --}}

						

					

					<fieldset class="form-group col-md-12">
						
						{{ Form::label('description', getphrase('description')) }}
						
						{{ Form::textarea('description', $value = null , $attributes = array('class'=>'form-control', 'rows'=>'5', 'placeholder' => 'Description','id'=>'description')) }}
					</fieldset>	

					</div>


						<div class="buttons text-center">

							<button class="btn btn-lg btn-primary button"

							ng-disabled='!formQuiz.$valid'>{{ $button_name }}</button>

						</div>

		 