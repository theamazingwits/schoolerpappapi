@extends($layout)
 
@section('header_scripts')
<link href="{{CSS}}animate.css" rel="stylesheet">
@stop

@section('custom_div')
<div ng-controller="courseSubjectsController" ng-init="ingAngData({{$items}})">
@stop
<?php
  if($right_bar === TRUE){
    $column = "col-xl-8";
    $column1 = "col-xl-6";
  }else{
    $column = "col-xl-12";
    $column1 = "col-xl-4";
  }
?>
@section('content')

<div id="page-wrapper">
			<section id="main" class="main-wrap bgc-white-darkest" role="main">
      <div class="container-fluid content-wrap">
				<!-- Page Heading -->
				<div class="row">
					<div class="col-lg-12">
						<ol class="breadcrumb">
							<li><a href="{{PREFIX}}"><i class="fa fa-home bc-home"></i></a> </li>
							<li><a href="{{URL_ASSIGN_STUDENT_VEHICLE}}">Select Details</a> </li>
							
							<li>{{ $title }}</li>
						</ol>
					</div>
				</div>
					<?php 
					$subjects_list = [];
					$angular_keys = [];
					?>		

				<!-- /.row -->
				{{-- 	{!! Form::open(array('url' => URL_MASTERSETTINGS_COURSE_SUBJECTS_ADD, 'method' => 'POST', 'name'=>'formQuiz ', 'novalidate'=>'')) !!}
 --}}
				
      <div class="row panel-grid" id="panel-grid">
        <div class="col-sm-12 col-md-12 col-lg-12 {{$column}} panel-wrap panel-grid-item grid-stack-item">
				<div class="panel panel-custom academia_visiblelist_fix" data-spy="affix" data-offset-top="0">
					<div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
						
						<div class="pull-right messages-buttons ">
							  
						{{-- 	<button type="submit" class="btn  btn-success button helper_step3" >{{ getPhrase('update')}}</button> --}}
						
							 
						</div>
						<h2 class="pull-left">{{ $title }}</h2>

            <!--Start panel icons-->
                <div class="panel-icons panel-icon-slide ">
                    <ul>
                        <li><a href=""><i class="fa fa-angle-left"></i></a>
                            <ul>
                                <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                                <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                                <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
                                <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
                                <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li>
                               <!--  <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li> -->
                            </ul>
                        </li>
                    </ul>
                </div>
                    <!--End panel icons-->
					   </div>
    					 
    					<div class="panel-body packages vertical-scroll" id="window_auto_height">
    					
                         <?php $i = 1; ?>
    				@foreach($vehicles as $vehicle)
                      <?php
                        $routes  = App\VehicleAssign::join('vroutes','vroutes.id','=','vechicle_assign.route_id')
                                                      ->select(['name','cost'])
                                                      ->where('vechicle_id',$vehicle->id)
                                                      ->get();

                      ?>
    					<h4 >
    					
                               
                               <span  data-toggle="popover" title="Stops" 
                               data-content="
                               <?php
                               foreach($routes as $vroute){
                               ?>
                               {{ucwords($vroute->name)}} ( {{getCurrencyCode()}} {{ $vroute->cost}} )
                               <?php } ?> 
                               ">
                                


    						<i class="fa fa-bus" aria-hidden="true"></i>{{$i}}. {{ getPhrase('vehicle -').' '. $vehicle->number }}( Seats {{$vehicle->seats}} ) </span>
    					
    				</h4>

    					<div class="sem-parent-container">
    					
    					
    					<?php 
    					
    						$angular_key = $vehicle->id;
    						?>
    						 @include('transport.studentassign.new.sub-list', 
    						array(	
    							'academic_id' 	=> $academic_id,
    							'course_id' 	=> $course_id,
    							'vehiclenum'    => $vehicle->id,
    							'semister' 		=> 0,
    							'angular_key'   => $angular_key,
    							'vehicle_id'    =>$vehicle->id,
    							'vehicle_capacity'    =>$vehicle->seats,
    							))
    							 <?php $angular_keys[] = $angular_key;?>
    					 </div>
    					<?php $i++; ?>
    				@endforeach
    					</div>
    				</div>


    				{{-- {!! Form::close() !!} --}}




              <div id="assignRoute" class="modal fade" role="dialog">
                <div class="modal-dialog modal-md">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                       <h4 class="modal-title" align="center"><b>{{getPhrase('assign_vehicle_routes')}}</b></h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                     
                    </div>
                    <div class="modal-body">

                     
                         <div ng-repeat="ritem in vehicle_routes">

                         	 <input id="@{{ ritem.id }}" value="@{{ ritem.id }}" name="assigned_route_id" type="radio" />
                          
                          <label for="@{{ ritem.id }}">
                              <span class="fa-stack radio-button">
                                  <i class="mdi mdi-check active">
                                  </i>
                              </span>
                           @{{$index+1}}. <b>@{{ritem.name}} - {{getCurrencyCode()}} @{{ritem.cost}}</b>
                          </label><br>

                       
                    </div>
                    <div class="modal-footer">
                      <button type="submit" class="btn btn-primary" style="float: right;" ng-click="routeAssign()">Yes</button>&nbsp;&nbsp;
                      <button type="button" class="btn btn-default pull-right" data-dismiss="modal">No</button>
                    </div>
                    
                  </div>

                </div>
              </div>
              </div>



              <div id="viewUsers" class="modal fade" role="dialog">
                <div class="modal-dialog modal-md">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title" align="center"><b>{{getPhrase('route_assigned_users')}}</b></h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      
                    </div>
                    <div class="modal-body">

                     
                         <div ng-repeat="user in users">
                           
                           @{{$index+1}}. <b>@{{user.name}} - @{{user.route_name}} ( {{getCurrencyCode()}} @{{user.cost}} )</b>

                           	<div class="add-delete-classes ">
                         		
                         	    <i class="fa fa-trash text-danger pull-right" ng-click="removeItem(user, target_items[user.vehicle_id], 'target_items')" title="{{getPhrase('delete')}}"></i>

                       
                         		</div>

                        </div>

                       
                    </div>
                    <div class="modal-footer">
                    
                      <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Ok</button>
                    </div>
                    
                  </div>

                </div>
              </div>
            </div>
            @if(isset($right_bar))
            <section class="col-sm-12 col-md-12 col-lg-12 col-xl-4 panel-wrap panel-grid-item">
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                        <h2 class="pull-left"> {{ $title }} </h2>
                        
                        <!--Start panel icons-->
                        <div class="panel-icons panel-icon-slide bgc-white-dark">
                            <ul>
                                <li><a href=""><i class="fa fa-angle-left"></i></a>
                                    <ul>
                                        <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                                        <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                                        <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
                                        <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
                                        <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li>
                                        <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <!--End panel icons-->
                    </div>
                    <div class="panel-body panel-body-p">
                      <?php $data = '';

                      if(isset($right_bar_data))

                        $data = $right_bar_data;

                      ?>

                      @include($right_bar_path, array('data' => $data))
                    </div>
                </div>
            </section>
            @endif
          </div>
              </div>
    </section>

			</div>
			<!-- /.container-fluid -->





		
@stop
 
@section('footer_scripts')
	@include('transport.studentassign.new.scripts', array('keys'=>$angular_keys))
	@include('common.alertify')

@include('common.affix-window-size-script')

<script type="text/javascript">
$(document).ready(function(){
    $('[data-toggle="popover"]').popover({
        placement : 'top',
        trigger : 'hover'
    });
});
</script>
<style type="text/css">
	.bs-example{
    	margin: 150px 50px;
    }
</style>

<script>
      function assignUser(){
         
           $('#assignRoute').modal('show');
        }

      function viewAssignedUser(){
         
           $('#viewUsers').modal('show');
        }
  </script>  
	
@stop 

