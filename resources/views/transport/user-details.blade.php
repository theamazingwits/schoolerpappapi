@extends('layouts.admin.adminlayout')
@section('header_scripts')
@stop
@section('content')

  <div id="page-wrapper ">
        <!--Begin Content-->
        <section id="main" class="main-wrap bgc-white-darkest" role="main">

                 <div class="container-fluid content-wrap">

                    <!-- Page Heading -->
                        <div class="row">
                          <div class="col-lg-12">
                            <ol class="breadcrumb">

                              <li><a href="{{PREFIX}}"><i class="mdi mdi-home"></i></a> </li>
                               @if(checkRole(getUserGrade(2)))
                              <li><a href="{{URL_USERS_DASHBOARD}}">{{getPhrase('users_dashboard')}}</a></li>
                                              @endif
                              @if(checkRole(getUserGrade(2)))
                              <li><a href="{{URL_USERS."student"}}">{{ getPhrase('student_users') }}</a> </li>
                              <li><a href="{{URL_USER_DETAILS.$user->slug}}">{{ucwords($user->name)}} Details</a> </li>
                              @endif
                              
                              @if(checkRole(getUserGrade(7)))
                              <li><a href="{{URL_PARENT_CHILDREN}}">{{ getPhrase('children') }}</a> </li>
                              <li><a href="{{URL_USER_DETAILS.$user->slug}}">{{ucwords($user->name)}} Details</a> </li>
                              @endif
                              
                              <li>{{ $title }}</li>
                            </ol>
                          </div>
                        </div>


                <div class="row panel-grid" id="panel-grid">
                    <div class="col-sm-12 col-md-12 col-lg-12 panel-wrap panel-grid-item grid-stack-item">
                        <!--Start Panel-->
                        <div class="panel bgc-white-dark">
                            <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                                <h2 class="pull-left">
                                {{$title}}</h2>
                                <!--Start panel icons-->
                                <div class="panel-icons panel-icon-slide ">
                                    <ul>
                                        <li><a href=""><i class="fa fa-angle-left"></i></a>
                                            <ul>
                                                <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                                                <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                                                <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
                                                <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
                                                <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li>
                                                <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                <!--End panel icons-->
                            </div>
                            <div class="panel-body">
                                <div class="page-size-table">
                                  <div > 
            
            @if( count( $transport_details ) > 0 )

                      <table class="table table-striped table-bordered datatable" cellspacing="0" width="100%">

                          <thead>
                            <th><b>{{getPhrase('vehicle')}}</b></th>
                            <th><b>{{getPhrase('route')}}</b></th>
                            <th><b>{{getPhrase('cost')}}</b></th>
                            <th><b>{{getPhrase('joined_on')}}</b></th>
                          </thead>
                          <tbody>
                              @foreach($transport_details as $hst_detail)
                                <tr>
                                  <td>{{ $hst_detail->vehicle_name() }}</td>
                                  <td>{{ $hst_detail->vroute_name() }}</td>
                                  <td>{{ getCurrencyCode() }} {{ $hst_detail->seat_fee() }}</td>
                                  <td>{{ $hst_detail->created_at }}</td>

                                </tr>
                             @endforeach
                          </tbody>
                        
                      </table>

                      @else
                        <h4>No data available</h4>
                      @endif
                           
                         <br>  
                        @if( count( $records ) > 0 )

                        <h4>Fee History</h4>

                       <table class="table panel-table">
                                    <thead>
                                      <th><b>Title</b></th>
                                      <th><b>Month - Year</b></th>
                                      <th><b>{{getPhrase('date')}}</b></th>
                                      <th><b>{{getPhrase('amount_to_pay')}}</b></th>
                                      <th><b>{{getPhrase('discount')}}</b></th>
                                      <th><b>{{getPhrase('paid_amount')}}</b></th>
                                      <th><b>{{getPhrase('balance')}}</b></th>
                                      
                                    </thead>
                                    <tbody>

                                     @foreach($records as $record)    
                                      <tr>

                                        <td>{{$record->title}}</td>
                                        <td>{{$record->month}} - {{$record->year}}</td>
                                        <td>
                                          @if($record->paid_date)

                                          {{$record->paid_date}}

                                          @else
                                          -
                                          @endif
                                        </td>
                                      
                                        <td>{{$currency}} {{$record->amount}}</td>
                                        <td>{{$currency}} {{$record->discount}}</td>
                                        <td>{{$currency}} {{$record->paid_amount}}</td>
                                        <td>{{$currency}} {{$record->balance}}</td>


                                      </tr>
                                    @endforeach  
                                      <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td><b>{{$currency}} {{$total_pay}}</b></td>
                                        <td><b>{{$currency}} {{$total_discount}}</b></td>
                                        <td><b>{{$currency}} {{$total_paid}}</b></td>
                                        <td><b>{{$currency}} {{$total_balance}}</b></td>
                                        
                                      </tr>

                                    

                                    </tbody>
                                   
                       </table>

                               @else
                        <h4>No data available</h4>
                      @endif  

            </div>
                                    <!-- <table id="table-student-exam-scheduledexam" class="card-view-no-edit page-size-table">
                                        
                                    </table> -->
                                </div>
                            </div>
                        </div>
                        <!--End Panel-->
                    </div>
                </div>
            </div>


        </section>
        <!--End Content-->
      
 
    </div>
@endsection
 

@section('footer_scripts')


@stop
