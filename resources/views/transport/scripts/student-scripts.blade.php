@include('common.angular-factory')
<script>

 app.controller('vehicleAssign', function ($scope, $http, httpPreConfig)
  {

   $scope.getVehicles = function(route_id){

            $scope.selected_route_id = route_id;

            route   = '{{ URL_GET_VEHICLE_ROUTES }}';  
            data    = {   
                   _method: 'post', 
                  '_token':httpPreConfig.getToken(), 
                  'route_id': route_id, 
               };

        httpPreConfig.webServiceCallPost(route, data).then(function(result){
          // console.log(result.data);
           $scope.vehicles  = result.data;

       });
   }

});

</script>