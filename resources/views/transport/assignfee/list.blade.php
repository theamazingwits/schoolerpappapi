@extends($layout)

@section('header_scripts')
<link href="{{CSS}}bootstrap-datepicker.css" rel="stylesheet">  

@stop

@section('content')
<div id="page-wrapper" ng-controller="assignHostelFee">
    <section id="main" class="main-wrap bgc-white-darkest" role="main">
        <div class="container-fluid content-wrap">
            <div class="row">
                <div class="">
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{PREFIX}}">
                                <i class="fa fa-home bc-home">
                                </i>
                            </a>
                        </li>
                         
                       
                        <li>{{$title}}</li>
                    </ol>
                </div>
            </div>

                @include('errors.errors')
         <div class="row panel-grid" id="panel-grid">
        <div class="col-sm-12 col-md-12 col-lg-12 panel-wrap panel-grid-item grid-stack-item">

            <div class="panel bgc-white-dark">
                <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm mll">
                    <h2>
                        {{getPhrase('select_details')}}
                    </h2>
                     <!--Start panel icons-->
                    <div class="panel-icons panel-icon-slide ">
                        <ul>
                            <li><a href=""><i class="fa fa-angle-left"></i></a>
                                <ul>
                                    <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                                    <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                                    <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
                                    <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
                                    <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li>
                                   <!--  <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li> -->
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!--End panel icons-->
                </div>
                <div class="panel-body instruction">

                  {!! Form::open(array('url' => URL_STORE_TRANSPORT_FEE_TYPE, 'method' => 'POST', 'name'=>'formQuiz ', 'novalidate'=>'')) !!}

                <div class="row">
                    <fieldset class='col-sm-6'>
                    <label for="exampleInputEmail1">{{ getPhrase('select_fee_type') }}</label>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <input type="radio" checked="checked" id="available" name="fee_type" value="1" ng-model="account_available" ng-init="account_available=1; accountAvailable(1);" ng-click="accountAvailable(1)">
                            <label for="available"> <span class="fa-stack radio-button"> <i class="mdi mdi-check active"></i> </span> {{ getPhrase('monthly')}} </label>
                        </div>
                        <div class="col-md-6">
                            <input type="radio" id="not_available" name="fee_type" value="0" ng-model="account_not_available" ng-click="accountAvailable(0)">
                            <label for="not_available"> <span class="fa-stack radio-button"> <i class="mdi mdi-check active"></i> </span> {{ getPhrase('yearly')}} </label>
                        </div>
                    </div>
                </fieldset>
            </div>

            <div class="row" >

                 <fieldset class="form-group col-md-6">

                                {{ Form::label('route_id', getphrase('route')) }}
                                <span class="text-red">*</span>
                                {{Form::select('route_id', $routes_list, null, ['class'=>'form-control', 'id'=>'route_id',
                                    'placeholder'=>'Select',
                                    'ng-model'=>'route_id',
                                    'required'=> 'true', 
                                    'ng-class'=>'{"has-error": formQuiz.route_id.$touched && formQuiz.route_id.$invalid}'
                                ])}}
                                 <div class="validation-error" ng-messages="formQuiz.route_id.$error" >
                                    {!! getValidationMessage()!!}
                                </div>

                            
              </fieldset>

              <fieldset class="form-group col-md-6">

                            {{ Form::label('title', getphrase('fee_title')) }}

                            <span class="text-red">*</span>

                            {{ Form::text('title', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('title'),

                                'ng-model'=>'title', 
                                
                                 'required'=> 'true', 

                                'ng-class'=>'{"has-error": formQuiz.title.$touched && formQuiz.title.$invalid}',

                           
                                )) }}

                            <div class="validation-error" ng-messages="formQuiz.title.$error" >

                                {!! getValidationMessage()!!}
                                
                             

                            </div>

                        </fieldset> 


                <fieldset class="form-group col-md-6">

                                {{ Form::label('year', getphrase('year')) }}
                                <span class="text-red">*</span>
                                {{Form::select('year', $years, null, ['class'=>'form-control', 'id'=>'year',
                                    'placeholder'=>'Select',
                                    'ng-model'=>'year',
                                    'required'=> 'true', 
                                    'ng-class'=>'{"has-error": formQuiz.year.$touched && formQuiz.year.$invalid}'
                                ])}}
                                 <div class="validation-error" ng-messages="formQuiz.year.$error" >
                                    {!! getValidationMessage()!!}
                                </div>

                            
              </fieldset>

                <fieldset class="form-group col-md-6" ng-if="showMonth">

                                {{ Form::label('month', getphrase('month')) }}
                                <span class="text-red">*</span>
                                {{Form::select('month', $months, null, ['class'=>'form-control', 'id'=>'month',
                                    'placeholder'=>'Select',
                                    'ng-model'=>'month',
                                    'required'=> 'true', 
                                    'ng-class'=>'{"has-error": formQuiz.month.$touched && formQuiz.month.$invalid}'
                                ])}}
                                 <div class="validation-error" ng-messages="formQuiz.month.$error" >
                                    {!! getValidationMessage()!!}
                                </div>

                            
              </fieldset>
                
            </div>


             <div class="row input-daterange" id="dp">
                    <?php 
                    $date_from = date('Y/m/d');
                    $date_to = date('Y/m/d');
                  
                     ?>
                     <fieldset class="form-group col-md-6">
                        {{ Form::label('start_date', getphrase('start_date')) }}
                        {{ Form::text('start_date', $value = $date_from , $attributes = array('class'=>'input-sm form-control', 'placeholder' => '2015/7/17')) }}
                    </fieldset>

                    <fieldset class="form-group col-md-6">
                        {{ Form::label('end_date', getphrase('end_date')) }}
                        {{ Form::text('end_date', $value = $date_to , $attributes = array('class'=>'input-sm form-control', 'placeholder' => '2015/7/17')) }}
                    </fieldset>
                </div>
                     
                     

                        <div class="buttons text-center">

                                <button class="btn btn-lg btn-primary button"

                                ng-disabled='!formQuiz.$valid'>{{ getPhrase('add_fee') }}</button>

                            </div>




                  {!! Form::close() !!}  
                    
               </div>
                                
                           
                        
                </div>
            </div>
        </div>
            </div>
        </div>
    </section>


  

</div>

 

@stop
 
 

@section('footer_scripts')

@include('common.angular-factory')


<script>
 
 app.controller('assignHostelFee', function ($scope, $http, httpPreConfig) {

    $scope.accountAvailable = function (availability)
      {

        
        if(!availability)
        {
          $scope.showYear = true;
          $scope.showMonth = false;
        }
        else {
          $scope.showMonth = true;
          $scope.showYear = false;
        }
      }
 

});

</script>

<script src="{{JS}}datepicker.min.js"></script>
    <script>
     
     $('.input-daterange').datepicker({
        autoclose: true,
        // startDate: "0d",
         format: '{{getDateFormat()}}',
    });

    </script>

  
@stop