@extends($layout)
@section('content')
<div id="page-wrapper" ng-controller="vehicleAssign">
			<div class="container-fluid">
				<!-- Page Heading -->
				<div class="row">
					<div class="col-lg-12">
						<ol class="breadcrumb">
							<li><a href="/"><i class="mdi mdi-home"></i></a> </li>
							<li class="active">{{isset($title) ? $title : ''}}</li>
						</ol>
					</div>
				</div>
					@include('errors.errors')
				<!-- /.row -->
				
				<div class="panel panel-custom col-lg-12">
					<div class="panel-heading">
						
						
					<h1>{{ $title }}  </h1>
					</div>
					<div class="panel-body" >
					<?php 

					 $button_name = getPhrase('add_to_vehicle');
					?>

					
				  {!! Form::open(array('url' => URL_STORE_STAFF_VEHICLE_DETAILS, 'method' => 'POST', 'name'=>'formQuiz ', 'novalidate'=>'')) !!}
					

					 @include('transport.staffassign.form_elements', 
					 array('button_name'=> $button_name),
					 array(
					  
					  'vehicles_list'=>$vehicles_list,
					  'user'=>$user,
					  'details'=>$details,
					  'vehicle_details'=>$vehicle_details

					 ))
					 		
					{!! Form::close() !!}
					</div>

				</div>
			</div>
			<!-- /.container-fluid -->


			  <div id="hostelVacate" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" align="center"><b>{{getPhrase('stop_user')}}</b></h4>
      </div>
      <div class="modal-body">

           {!!Form::open(array('url'=> URL_STAFF_STOP_FROM_VEHICLE,'method'=>'POST','name'=>'userstatus'))!!} 
       
        <h4 style="color: #ffa616;" align="center">{{getPhrase('are_you_sure_stop_this_user_from_vehicle')}}</h4>
        <input type="hidden" name="stop_user_id" id="stop_user_id">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>&nbsp;
        <button type="submit" class="btn btn-primary">Yes</button>
      </div>
      {!!Form::close()!!}
      
    </div>

  </div>
</div>

		</div>
		<!-- /#page-wrapper -->
@stop

@section('footer_scripts')
 {{-- @include('common.validations') --}}

 @include('transport.scripts.student-scripts')

 <script>
      function stopUser(user_slug){
           // console.log(user_slug);
           $('#stop_user_id').val(user_slug);
           $('#hostelVacate').modal('show');
        }
  </script>  
@stop
 
 