<div class="row">
                       
                       @if( $details > 0 )

                       

                    	<table class="table table-striped table-bordered datatable" cellspacing="0" width="100%">

                    		<thead>
                    			<th><b>{{getPhrase('vehicle_name')}}</b></th>
                    			<th><b>{{getPhrase('route_name')}}</b></th>
                    			<th><b>{{getPhrase('cost')}}</b></th>
                    			<th><b>{{getPhrase('joined_on')}}</b></th>
                    			<th></th>
                    		</thead>
                    		<tbody>

                    			  @foreach($vehicle_details as $vehi_detail)
                    			<tr>
                    				<td>{{ $vehi_detail->vehicle_name() }}</td>
                            <td>{{ $vehi_detail->vroute_name() }}</td>
                    				<td>{{ getCurrencyCode() }} {{ $vehi_detail->seat_fee() }}</td>
                    				<td>{{ $vehi_detail->created_at }}</td>

                    				@if( $vehi_detail->is_stoped == 0 && $vehi_detail->is_active == 1 )
                    				
                                    <td><a href="javascript:void(0)" onclick="stopUser({{$user->id}})" class="btn btn-warning btn-sm">{{getPhrase('stop_user')}}</a></td>

                                    @elseif($vehi_detail->is_stoped == 1)

                                      <td><a href="javascript:void(0)" class="btn btn-info btn-sm"><b>{{getPhrase('stoped_from')}} {{$vehi_detail->updated_at}}</b></a></td>

                    				@endif
                    			</tr>
                    	          @endforeach
                    		</tbody>
                    		
                    	</table>


                    	@endif

                    	<input type="hidden" name="user_id" value="{{$user->id}}">

                    <fieldset class="form-group col-md-6">

							{{ Form::label('route_id', getphrase('routes')) }}
							<span class="text-red">*</span>
							{{Form::select('route_id', $vehicles_list, null, ['class'=>'form-control', 'id'=>'route_id',
								'placeholder'=>'Select',
								'ng-model'=>'route_id',
								"ng-change" => "getVehicles(route_id)",
								'required'=> 'true', 
								'ng-class'=>'{"has-error": formQuiz.route_id.$touched && formQuiz.route_id.$invalid}'
							])}}
							 <div class="validation-error" ng-messages="formQuiz.route_id.$error" >
		    					{!! getValidationMessage()!!}
							</div>

						
					</fieldset>

					   <fieldset ng-if="selected_route_id" class="form-group col-md-6">
                          <span class="text-red">*</span>
                           <label for = "vehicle_id">{{getPhrase('vehicles')}}</label>
                          <select 
                          name      = "vehicle_id" 
                          id        = "vehicle_id" 
                          class     = "form-control" 
                          ng-model  = "vehicle_id" 
                          ng-options= "option.id as option.vehicle_number for option in vehicles track by option.id"
                          >
                          <option value="">{{getPhrase('select')}}</option>
                          </select>
                      </fieldset> 



				</div>

					   <div class="buttons text-center">

							<button class="btn btn-lg btn-primary button"

							ng-disabled='!formQuiz.$valid'>{{ $button_name }}</button>

						</div>