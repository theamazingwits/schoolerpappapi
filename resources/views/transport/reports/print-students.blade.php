
<h1 align="center"><b>{{$title}} Fee Reports</b></h1><br/>
<div class="row vertical-scroll" id="printableArea">
  
    <table style="border-collapse: collapse;">

    <thead>
        <th style="border:1px solid #000;">{{getPhrase('sno')}}</th>
        <th style="border:1px solid #000;" >{{getPhrase('name')}}</th>
        <th style="border:1px solid #000;">{{getPhrase('roll_no')}}</th>
        <th style="border:1px solid #000;">{{getPhrase('course')}}</th>
        <th style="border:1px solid #000;">{{getPhrase('vehicle_number')}}</th>
        <th style="border:1px solid #000;">{{getPhrase('route')}}</th>
        <th style="border:1px solid #000;">{{getPhrase('year')}}</th>
        <th style="border:1px solid #000;">{{getPhrase('month')}}</th>
        {{-- <th style="border:1px solid #000;">{{getPhrase('start_date')}}</th> --}}
        {{-- <th style="border:1px solid #000;">{{getPhrase('end_date')}}</th> --}}
        <th style="border:1px solid #000;">{{getPhrase('amount')}}</th>
        <th style="border:1px solid #000;">{{getPhrase('discount')}}</th>
        <th style="border:1px solid #000;">{{getPhrase('paid_amount')}}</th>
        <th style="border:1px solid #000;">{{getPhrase('balance')}}</th>
       
       
        
    </thead>
    <tbody>
    <?php $sno =1;?>
     @foreach($reports as $record)
    <tr>
        {{-- {{dd($record)}} --}}
        <td style="border:1px solid #000;">{{$sno++}}</td>
        <td style="border:1px solid #000;">{{$record['name']}}</td>
        <td style="border:1px solid #000;">{{$record['roll_no']}}</td>
        <td style="border:1px solid #000;">{{$record['course_title']}}</td>
        <td style="border:1px solid #000;">{{$record['number']}}</td>
        <td style="border:1px solid #000;">{{$record['route_name']}}</td>
        <td style="border:1px solid #000;">{{$record['year']}}</td>
        <td style="border:1px solid #000;">{{$record['month']}}</td>
        <td style="border:1px solid #000;">{{ getCurrencyCode() }}{{$record['amount']}}</td>
        <td style="border:1px solid #000;">{{ getCurrencyCode() }}{{$record['discount']}}</td>
        <td style="border:1px solid #000;">{{ getCurrencyCode() }}{{$record['paid_amount']}}</td>
        <td style="border:1px solid #000;">{{ getCurrencyCode() }}{{$record['balance']}}</td>
        
        
       
    </tr> 
    @endforeach
    </tbody>
    </table>
</div>


 </body>
 </html>