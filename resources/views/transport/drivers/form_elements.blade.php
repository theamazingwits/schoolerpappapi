 					@if(checkRole(['owner']) && App\Hostel::isMultiBranch())
					<div class="row">
 						<fieldset class="form-group col-md-12">
							{{ Form::label('branch_id', getphrase('branch')) }}
							<span class="text-red">*</span>
							{{Form::select('branch_id', $branches, null, ['class'=>'form-control', 'id'=>'branch_id',
								'ng-model'=>'branch_id',
								'required'=> 'true', 
								'ng-class'=>'{"has-error": formQuiz.branch_id.$touched && formQuiz.branch_id.$invalid}'
							])}}
							 <div class="validation-error" ng-messages="formQuiz.branch_id.$error" >
		    					{!! getValidationMessage()!!}
							</div>
						</fieldset>
 					</div>
 					@else 
						{{ Form::hidden('branch_id', getUserRecord()->branch_id, array('id'=>'branch_id')) }}
					@endif


					<div class="row form-group">

 					 <fieldset class="col-md-6">

						{{ Form::label('name', getphrase('name')) }}

						<span class="text-red">*</span>

						{{ Form::text('name', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('name'),

							'ng-model'=>'name', 
              
              'required'=> 'true', 

							'ng-class'=>'{"has-error": formQuiz.name.$touched && formQuiz.name.$invalid}',

							'ng-minlength' => '4',

							'ng-maxlength' => '100',

							)) }}

						<div class="validation-error" ng-messages="formQuiz.name.$error" >

	    					{!! getValidationMessage()!!}
                
                {!! getValidationMessage('minlength')!!}

	    					{!! getValidationMessage('maxlength')!!}

						</div>

					</fieldset>	



        <fieldset class="col-md-6">

             {{ Form::label('experience', getphrase('experience')) }}

            <span class="text-red">*</span>
          
           {{Form::select('experience', $experience,null, ['placeholder' => getPhrase('select'),'class'=>'form-control',

              'ng-model'=>'experience',

              'id'=>'experience',

              'required'=> 'true', 

              'ng-class'=>'{"has-error": formQuiz.experience.$touched && formQuiz.experience.$invalid}'

             ])}}

              <div class="validation-error" ng-messages="formQuiz.experience.$error" >

                {!! getValidationMessage()!!}

            </div>

       </fieldset>

     </div>
       
        <div class="row form-group">

          <fieldset class="col-md-6">

            {{ Form::label('phone_number', getphrase('phone_number')) }}

            <span class="text-red">*</span>

            {{ Form::number('phone_number', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => 
            getPhrase('please_enter_10-15_digit_mobile_number'),

              'ng-model'=>'phone_number',

              'required'=> 'true', 
              
              'ng-class'=>'{"has-error": formQuiz.phone_number.$touched && formQuiz.phone_number.$invalid}',


            )) }}

            <div class="validation-error" ng-messages="formQuiz.phone_number.$error" >

                {!! getValidationMessage()!!}

            </div>

          </fieldset>


               <fieldset class="col-md-6">

            {{ Form::label('licence_number', getphrase('licence_number')) }}

            <span class="text-red">*</span>

            {{ Form::text('licence_number', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('licence_number'),

              'ng-model'=>'licence_number', 
              
              'required'=> 'true', 

              'ng-class'=>'{"has-error": formQuiz.licence_number.$touched && formQuiz.licence_number.$invalid}',

              'ng-minlength' => '4',

              'ng-maxlength' => '100',

              )) }}

            <div class="validation-error" ng-messages="formQuiz.licence_number.$error" >

                {!! getValidationMessage()!!}
                
                {!! getValidationMessage('minlength')!!}

                {!! getValidationMessage('maxlength')!!}

            </div>

          </fieldset> 

        </div>

          <div class="row form-group">

          <fieldset class="col-md-6">
            
            {{ Form::label('licence_document', getphrase('licence_document')) }}
             <span class="text-red">*</span>
            <input type="file" name="licence" class="form-control">
            
          </fieldset>

          @if($record)
           @php
              
              $licence_document  = App\VdriverDocuments::where('driver_id',$record->id)
                                                ->where('document_type','licence')
                                                ->first();

           @endphp


           <fieldset class="col-md-6">
           
           @if($licence_document)
            
           <img src="{{IMAGE_DRIVER_DOCUMENT_DEFAULT}}" width="40px" height="40px">
           <p>
             <a href="{{URL_DOWNLOAD_DRIVER_DOCUMENT.$licence_document->id}}">{{ucwords($licence_document->document_name)}}</a>
           </p>

           @endif
            

          </fieldset>
          @endif


 					 <fieldset class="col-md-6">

          {{ Form::label('spouse_name', getphrase('spouse_name')) }}

          <span class="text-red">*</span>

          {{ Form::text('spouse_name', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('spouse_name'),

            'ng-model'=>'spouse_name', 
            
            'required'=> 'true', 

            'ng-class'=>'{"has-error": formQuiz.spouse_name.$touched && formQuiz.spouse_name.$invalid}',

            'ng-minlength' => '4',

            'ng-maxlength' => '100',

            )) }}

          <div class="validation-error" ng-messages="formQuiz.spouse_name.$error" >

              {!! getValidationMessage()!!}
              
              {!! getValidationMessage('minlength')!!}

              {!! getValidationMessage('maxlength')!!}

          </div>

          </fieldset>	

        </div>

         <div class="row form-group">


          <fieldset class="col-md-6">
            
            {{ Form::label('experience_document', getphrase('experience_document')) }}
            
            <input type="file" name="experience" class="form-control">

          </fieldset> 

          @if($record)

           @php
              
              $experience  = App\VdriverDocuments::where('driver_id',$record->id)
                                                ->where('document_type','experience')
                                                ->first();
           @endphp
         
           @if($experience)

           <fieldset class="col-md-6">

            
           <img src="{{IMAGE_DRIVER_DOCUMENT_DEFAULT}}" width="40px" height="40px">
           <p>
             <a href="{{URL_DOWNLOAD_DRIVER_DOCUMENT.$experience->id}}">{{ucwords($experience->document_name)}}</a>
           </p>
            

          </fieldset>


          @endif
          @endif
          <fieldset class="col-md-6">

          {{ Form::label('number_of_children', getphrase('number_of_children')) }}

          <span class="text-red">*</span>

          {{ Form::number('number_of_children', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => 
          getPhrase('number_of_children'),

            'ng-model'=>'number_of_children',

            'required'=> 'true', 
            
            'ng-class'=>'{"has-error": formQuiz.number_of_children.$touched && formQuiz.number_of_children.$invalid}',


          )) }}

          <div class="validation-error" ng-messages="formQuiz.number_of_children.$error" >

              {!! getValidationMessage()!!}

          </div>

          </fieldset>


        </div>



  <div class="row form-group">

            <fieldset class="col-md-6">
            
            {{ Form::label('id_proof', "ID Proof") }}
             <span class="text-red">*</span>
            <input type="file" name="id_proof" class="form-control">
            
          </fieldset> 


          @if($record)
           @php
              
              $id_proof  = App\VdriverDocuments::where('driver_id',$record->id)
                                                ->where('document_type','id_proof')
                                                ->first();
           @endphp
 @if($id_proof)

           <fieldset class="col-md-6">

            
           <img src="{{IMAGE_DRIVER_DOCUMENT_DEFAULT}}" width="40px" height="40px">
           <p>
             <a href="{{URL_DOWNLOAD_DRIVER_DOCUMENT.$id_proof->id}}">{{ucwords($id_proof->document_name)}}</a>
           </p>
            

          </fieldset>
          @endif
          @endif
          <fieldset class="col-md-6">

          {{ Form::label('alternate_mobile_number', getphrase('alternate_mobile_number')) }}

          <span class="text-red">*</span>

          {{ Form::number('alternate_mobile_number', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => 
          getPhrase('please_enter_10-15_digit_mobile_number'),

            'ng-model'=>'alternate_mobile_number',

            'required'=> 'true', 
            
            'ng-class'=>'{"has-error": formQuiz.alternate_mobile_number.$touched && formQuiz.alternate_mobile_number.$invalid}',


          )) }}

          <div class="validation-error" ng-messages="formQuiz.alternate_mobile_number.$error" >

              {!! getValidationMessage()!!}

          </div>

          </fieldset>
     
        </div>

         <div class="row form-group">

             <fieldset class="col-md-6">
            
            {{ Form::label('other_document', getphrase('other_document')) }}
            <input type="file" name="other" class="form-control">
            
          </fieldset> 

          @if($record)
          @php
              
              $other_document  = App\VdriverDocuments::where('driver_id',$record->id)
                                                ->where('document_type','other')
                                                ->first();
           @endphp

 @if($other_document)
           <fieldset class="col-md-6">

            
           <img src="{{IMAGE_DRIVER_DOCUMENT_DEFAULT}}" width="40px" height="40px">
           <p>
             <a href="{{URL_DOWNLOAD_DRIVER_DOCUMENT.$other_document->id}}">{{ucwords($other_document->document_name)}}</a>
           </p>
            

          </fieldset>
          @endif
          @endif

        </div>

				
					 <div class="row form-group">

					<fieldset class="col-md-12">
						
						{{ Form::label('address', getphrase('address')) }}
						
						{{ Form::textarea('address', $value = null , $attributes = array('class'=>'form-control', 'rows'=>'5', 'placeholder' => 'address','id'=>'address')) }}
					</fieldset>

          </div>	


         <input type="hidden" name="added_by" value="{{Auth::user()->id}}"> 




						<div class="buttons text-center">

							<button class="btn btn-lg btn-primary button"

							ng-disabled='!formQuiz.$valid'>{{ $button_name }}</button>

						</div>

		 