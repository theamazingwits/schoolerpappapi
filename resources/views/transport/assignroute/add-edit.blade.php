@extends($layout)

@section('header_scripts')
<link href="{{CSS}}animate.css" rel="stylesheet">
@stop
@section('custom_div')
<div ng-controller="assignVehicleRoute" ng-init="ingAngData({{$items}})">
@stop

@section('content')
<?php
  if($right_bar === TRUE){
    $column = "col-xl-8";
    $column1 = "col-xl-6";
  }else{
    $column = "col-xl-12";
    $column1 = "col-xl-4";
  }
?>
<div id="page-wrapper">
     <section id="main" class="main-wrap bgc-white-darkest" role="main">
      <div class="container-fluid content-wrap">
        <!-- Page Heading -->
        <div class="row">
          <div class="col-lg-12">
            <ol class="breadcrumb">
              <li><a href="{{PREFIX}}"><i class="fa fa-home bc-home"></i></a> </li>
              <li><a href="{{URL_VECHICLES}}">{{ getPhrase('vehicles')}}</a> </li>
              
              <li class="active">{{isset($title) ? $title : ''}}</li>
            </ol>
          </div>
        </div>
          @include('errors.errors')
        <!-- /.row -->
        <div class="row panel-grid" id="panel-grid">
          <div class="col-sm-12 col-md-12 {{$column}} panel-wrap panel-grid-item grid-stack-item">
        <div class="panel panel-custom academia_visiblelist_fix" data-spy="affix" data-offset-top="0" id="app">
          <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">

          <h1>{{ $title }}  </h1>

           <!--Start panel icons-->
                            <div class="panel-icons panel-icon-slide ">
                                <ul>
                                    <li><a href=""><i class="fa fa-angle-left"></i></a>
                                        <ul>
                                            <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                                            <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                                            <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
                                            <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
                                            <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li>
                                           <!--  <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li> -->
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <!--End panel icons-->


          </div>
          <div class="panel-body form-auth-style " >
          <?php $button_name = getPhrase('create'); ?>
          
           <?php $button_name = getPhrase('update'); ?>

            {{ Form::model($record, array('url' => URL_STORE_VEHICLE_ROUTES, 'method'=>'post')) }}
          
         <input type="hidden" name="vehicle_id" value="{{$record->id}}">
           
            
                <div class="row">
                  <div class="col-md-12 col-lg-12">
                      <h2 class="selected-item-title">{{getPhrase('assign_routes')}}</h2>
                       <div class='containerVertical' id="target"  ng-drop="true" ng-drop-success="onDropComplete($data,$event)">
                       <div ng-if="!target_items.length" class="subject-placeholder"> No Item Selected</div>
                      <div ng-repeat="item in target_items" class="items-sub" id="target_items-@{{item.id}}">@{{item.name}}
                      <input type="hidden" name="selected_list[]" data-myname="@{{item.name}}" value="@{{item.id}}">
                      <div class="buttons-right">
                        
                      <i class="fa fa-trash text-danger pull-right" ng-click="removeItem(item, target_items, 'target_items')"></i>

                      
                      </div>


                      </div>

                   </div>
    

           </div>
          
        </div>
     <hr>
    <div class="text-center">
      <button class="btn btn-primary btn-lg">Update</button>
    </div>
 


           
          {!! Form::close() !!}
           



          </div>
        </div>
      </div>
      @if(isset($right_bar))
      <section class="col-sm-12 col-md-12 col-lg-12 col-xl-4 panel-wrap panel-grid-item">
        <!--Start Panel-->
        <div class="panel bgc-white-dark">
          <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
              
            <!--Start panel icons-->
            <!--End panel icons-->
          </div>
          <div class="panel-body panel-body-p">
            <?php $data = '';

            if(isset($right_bar_data))

              $data = $right_bar_data;

            ?>

            @include($right_bar_path, array('data' => $data))
          </div>
        </div>
      </section>
      @endif
      


    </div>
      </div>
      <!-- /.container-fluid -->
    </section>
    </div>
    <!-- /#page-wrapper -->

  
@stop

@section('custom_div_end')
</div>
@stop

@section('footer_scripts')
@include('transport.assignroute.scripts.js-scripts')
@include('common.affix-window-size-script', array('newId'=>'app'))
@include('common.alertify')
{{--  <script src="{{JS}}bootstrap-toggle.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
    $('[data-toggle="popover"]').popover({
        placement : 'bottom',
        trigger : 'hover'
    });
});
</script>
<style type="text/css">
  .bs-example{
      margin: 150px 50px;
    }
</style> --}}

 @stop