          @if(checkRole(['owner']) && App\Hostel::isMultiBranch())
          <div class="row">
            <fieldset class="form-group col-md-12">
              {{ Form::label('branch_id', getphrase('branch')) }}
              <span class="text-red">*</span>
              {{Form::select('branch_id', $branches, null, ['class'=>'form-control', 'id'=>'branch_id',
                'ng-model'=>'branch_id',
                'required'=> 'true', 
                'ng-class'=>'{"has-error": formQuiz.branch_id.$touched && formQuiz.branch_id.$invalid}'
              ])}}
               <div class="validation-error" ng-messages="formQuiz.branch_id.$error" >
                  {!! getValidationMessage()!!}
              </div>
            </fieldset>
          </div>
          @else 
            {{ Form::hidden('branch_id', getUserRecord()->branch_id, array('id'=>'branch_id')) }}
          @endif


          <div class="row">

           <fieldset class="form-group col-md-12">

            {{ Form::label('name', getphrase('destination')) }}

            <span class="text-red">*</span>

            {{ Form::text('name', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('name'),

              'ng-model'=>'name', 


              'required'=> 'true', 

              'ng-class'=>'{"has-error": formQuiz.name.$touched && formQuiz.name.$invalid}',

              'ng-minlength' => '4',

              'ng-maxlength' => '100',

              )) }}

            <div class="validation-error" ng-messages="formQuiz.name.$error" >

                {!! getValidationMessage()!!}


                {!! getValidationMessage('minlength')!!}

                {!! getValidationMessage('maxlength')!!}

            </div>

          </fieldset> 

        
          

          <fieldset class="form-group col-md-12">
            
            {{ Form::label('description', getphrase('description')) }}
            
            {{ Form::textarea('description', $value = null , $attributes = array('class'=>'form-control', 'rows'=>'5', 'placeholder' => 'Description','id'=>'description')) }}
          </fieldset> 

          </div>


    <fieldset class="form-group col-md-12" >
<label for="name">Stops Details:<span class="text-red">*</span></label>

  <table class="table">
    <thead>
      <th><b>Stop Name</b></th>
      <th><b>Cost</b></th>
      <th><b>Sort Order</b></th>
      <th><b>Location</b></th>
      <th><b>Select to remove</b></th>

    </thead>
    <tbody>
      <tr ng-repeat="routeData in routeDetails">

     @if($record)   

      <td>
          <input type="text" class="form-control" name="route_names[@{{routeData.id}}]" ng-model="routeData.name"/>
      </td>
      <td>
         <input type="number" class="form-control" name="route_costs[@{{routeData.id}}]" ng-model="routeData.cost"/>
     </td>

       <td>
         <input type="number" class="form-control" name="sort_order[@{{routeData.id}}]" ng-model="routeData.sort_order"/>
     </td>
      <td>
          <button type="button" class="btn btn-lg btn-primary button" data-toggle="modal" data-target="#map"ng-click= "SetLocation($index)" >Change Location</button>
         <!-- <input  class="form-control"  name="gps_coord[]" id="auto-search@{{$index}}" ng-click= "getLocation($index)" ng-model="routeData.gps_coord"  min="0" /> -->
         <input  type="hidden" class="form-control"  name="gps_lat[@{{routeData.id}}]" id="lat@{{$index}}" value="@{{routeData.gps_lat}}" ng-model="routeData.gps_lat"  min="0" />
         <input  type="hidden" class="form-control"  name="gps_lng[@{{routeData.id}}]" value="@{{routeData.gps_lng}}" id="lng@{{$index}}" ng-model="routeData.gps_lng"  min="0" />
     </td>
        

     @else

       <td>
          <input type="text" class="form-control" name="route_names[]" ng-model="routeData.name"/>
      </td>
      <td>
         <input type="number" class="form-control" name="route_costs[]" ng-model="routeData.cost"  min="0" />
     </td>

       <td>
         <input type="number" class="form-control" name="sort_order[]" ng-model="routeData.sort_order"  min="0" />
     </td>
     <td>
          <button type="button" class="btn btn-lg btn-primary button" data-toggle="modal" data-target="#map"ng-click= "SetLocation($index)" >SET Location</button>
         <!-- <input  class="form-control"  name="gps_coord[]" id="auto-search@{{$index}}" ng-click= "getLocation($index)" ng-model="routeData.gps_coord"  min="0" /> -->
         <input  type="hidden" class="form-control"  name="gps_lat[]" id="lat@{{$index}}" ng-model="routeData.gps_lat"  min="0" />
         <input  type="hidden" class="form-control"  name="gps_lng[]" id="lng@{{$index}}" ng-model="routeData.gps_lng"  min="0" />
     </td>



     @endif
   
      
      <td>
        <input type="checkbox" name="options" id="free" ng-model="routeData.selected" style="display: block;" /></td>
        
      </tr>

      <tr ng-if="routeDetails.length <= 0">
        <td class="text-center"></td>
        <td></td>
        <td><h5>Please Add Route Names</h5></td>
        <td></td>
        <td></td>
      </tr>
    </tbody>



    
  </table>

      <div class="form-group">
         

         <input type="button" class="btn btn-primary addnew pull-right" ng-click="addNewRoute()" value="Add Route">
           <input ng-hide="!routeDetails.length" type="button" class="btn btn-danger pull-right" ng-click="removeRoute()" value="Remove" style="margin-right: 5px;">
     </div>

</fieldset>


            <div class="buttons text-center">

              <button class="btn btn-lg btn-primary button"

              ng-disabled='!formQuiz.$valid'>{{ $button_name }}</button>

            </div>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyBa6mNrfx2rsr-S_7b5Asji6DW4E1aJVd8&libraries=places" async defer></script>
    <!--  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBa6mNrfx2rsr-S_7b5Asji6DW4E1aJVd8&amp;libraries=places"></script>
          -->
<script>
  //google.maps.event.addDomListener(window, 'load', initialize);
    function initialize(id) {
      var input = document.getElementById(id);
      var autocomplete = new google.maps.places.Autocomplete(input);
      autocomplete.addListener('place_changed', function () {
      var place = autocomplete.getPlace();
      // place variable will have all the information you are looking for.
      $('#lat').val(place.geometry['location'].lat());
      $('#long').val(place.geometry['location'].lng());
    });
  }
</script>