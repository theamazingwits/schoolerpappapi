@extends($layout)
<!-- <link href="{{CSS}}bootstrap-datepicker.css" rel="stylesheet"> -->
@section('content')
@if($record)
<div id="page-wrapper" ng-controller ="prepareStudentsData" ng-init="initFunctions('{{$record->id}}')">
@else
<div id="page-wrapper" ng-controller ="prepareStudentsData" >
@endif
	<style>
                /*.sugges{

            display: none;
        }*/

        /*.image--cover {
        width: 80px;
        height: 80px;
        border-radius: 50%;
        

        object-fit: cover;
        object-position: center right;
        }*/


       /* .margin-reduce{
            margin-top: -7px;
        }
          */  
        /*.btn-primary{

        background-color: #425262 !important;
        border: 1px solid #425262 !important;
        }*/
        
 
#description {
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
      }

      #infowindow-content .title {
        font-weight: bold;
      }

      #infowindow-content {
        display: none;
      }

      #map #infowindow-content {
        display: inline;
      }

      .pac-card {
        margin: 10px 10px 0 0;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        background-color: #fff;
        font-family: Roboto;
      }
      .pac-container {
    z-index: 9999999999 !important;
}

      .pac-controls {
        display: inline-block;
        padding: 5px 11px;
      }

      .pac-controls label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 400px;
      }
      #pac-input:focus {
        border-color: #4d90fe;
      }
      #title {
        color: #fff;
        background-color: #4d90fe;
        font-size: 25px;
        font-weight: 500;
        padding: 6px 12px;
      }
      #target {
        width: 345px;
      }

.ui-front {
    z-index: 9999;
}

.pac-container:after {
    /* Disclaimer: not needed to show 'powered by Google' if also a Google Map is shown */

    background-image: none !important;
    height: 0px;
}
/* //==Dimension font-sizes */
.dim_size{
    font-size:14px;
}

.no_pad{
    padding: 0px;
}
            </style>
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
		<div class="container-fluid content-wrap">
				<!-- Page Heading -->
				<div class="row">
					<div class="col-lg-12">
						<ol class="breadcrumb">
							<li><a href="/"><i class="fa fa-home bc-home"></i></a> </li>
							<li><a href="{{URL_VECHICLE_ROUTE}}">{{ getPhrase('vehicle_routes')}}</a></li>
							<li class="active">{{isset($title) ? $title : ''}}</li>
						</ol>
					</div>
				</div>
					@include('errors.errors')
				<!-- /.row -->
			<div class="row panel-grid" id="panel-grid">
				<div class="col-sm-12 col-md-12 col-lg-12 panel-wrap panel-grid-item grid-stack-item">
					<div class="panel panel-custom col-lg-12">
						<div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
							<div class="pull-right messages-buttons mr-6">
								<a href="{{URL_VECHICLE_ROUTE}}" class="btn  btn-primary button" >{{ getPhrase('list')}}</a>
							</div>
							
						<h1>{{ $title }}  </h1>
						 <div class="panel-icons panel-icon-slide ">
			                        <ul>
			                            <li><a href=""><i class="fa fa-angle-left"></i></a>
			                                <ul>
			                                    <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
			                                    <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
			                                    <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
			                                    <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
			                                    <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li>
			                                   <!--  <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li> -->
			                                </ul>
			                            </li>
			                        </ul>
			                    </div>
						</div>
						<div class="panel-body" >
						<?php $button_name = getPhrase('create'); ?>
						@if ($record)
						 <?php $button_name = getPhrase('update'); ?>
							{{ Form::model($record, 
							array('url' => URL_VECHICLE_ROUTE_EDIT.$record->id, 
							'method'=>'patch', 'name'=>'formQuiz ', 'novalidate'=>'')) }}
						@else
							{!! Form::open(array('url' => URL_VECHICLE_ROUTE_ADD, 'method' => 'POST', 'name'=>'formQuiz ', 'novalidate'=>'')) !!}
						@endif
						

						 @include('transport.route.form_elements', 
						 array('button_name'=> $button_name),
						 array('record'=> $record, 
						 'branches'=>$branches
						 ))
						 		
						{!! Form::close() !!}
						</div>

					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="map" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		    <div class="modal-dialog modal-sm" role="document">
		        <div class="modal-content" style="width: 50em;margin-left: -15em;">
		            <div class="modal-header text-center">
		                <h4 class="modal-title" id="myModalLabel">{{getPhrase('chat info')}}</h4>
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		            </div>
		            <div class="modal-body">
		            	<div class="row">
		            		<div class="col-md-12">
		            			<!-- <input  class="form-control"   id="pac-input" ng-model="routeData.gps_coord"  min="0" /> -->
			            		<div id="map_canvas" style="height: 500px;width: 100%;margin: 0.6em;padding-top: 10px;"></div>
							</div>
		            	</div>
		    		</div>
		        </div>
                
            </div>
        </div>
			<!-- /.container-fluid -->
	</section>
</div>
		<!-- /#page-wrapper -->
@stop

@section('footer_scripts')
 @include('common.validations')

 @include('common.angular-factory',array('load_module'=> FALSE))




 <script>

app.controller('prepareStudentsData', function( $scope, $http, httpPreConfig) {

      
        $scope.initFunctions = function(parent_id) 
        {   
       
            $scope.getStudentApplicationDetails(parent_id);
        }
    

 $scope.routeDetails           = [];
 
 $scope.getStudentApplicationDetails = function(vroute_id)
      {
      	console.log("Enter Via Routes")
        route = '{{URL_GET_VIA_ROUTES}}',
        data= {_method: 'post',
                 '_token':httpPreConfig.getToken(),
                   'vroute_id': vroute_id,
                 };

           httpPreConfig.webServiceCallPost(route, data).then(function(result){
              
              // console.log(result.data);
              $scope.routeDetails           = result.data.all_routes;
        	console.log(JSON.stringify(result))
         });

      }

       $scope.getLocation = function(id)
      {
      	console.log(id);
      	 var input = document.getElementById("auto-search"+id);
      	 console.log(input);
      var autocomplete = new google.maps.places.Autocomplete(input);
      autocomplete.addListener('place_changed', function () {
      var place = autocomplete.getPlace();
      // place variable will have all the information you are looking for.
      $('#lat'+id).val(place.geometry['location'].lat());
      $('#lng'+id).val(place.geometry['location'].lng());
    });
  	}
  	$scope.SetLocation = function(id){


    $(function () {
              
        var lat = 13.0827,
            lng = 80.2707,
            latlng = new google.maps.LatLng(lat, lng),
            new_image = '../../images/search_pin.png',
            image = 'http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png';;

            
        //zoomControl: true,
        //zoomControlOptions: google.maps.ZoomControlStyle.LARGE,

        
        var mapOptions = {
            center: new google.maps.LatLng(lat, lng),
            zoom: 15,
            maxZoom: 17,
            //mapTypeId: google.maps.MapTypeId.ROADMAP,
            panControl: false,
            mapTypeControl: false,
            streetViewControl: false,
            panControlOptions: {
                position: google.maps.ControlPosition.TOP_RIGHT
            },
            zoomControl: true,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.LARGE,
                position: google.maps.ControlPosition.TOP_left
            },
            // types: ['geocode'],
            // componentRestrictions: {country: 'om'}
           

        }
         map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions),
            marker = new google.maps.Marker({
                position: latlng,
                map: map,
                icon: image
            });
            // marker.setMap(null);   
              var infowindow = new google.maps.InfoWindow();
              //map.panTo(latlng);
              var pacinput = '<input id="pac-input" class="form-control" type="text" style="border: groove;margin-top: 20px;">';
        $('#map_canvas').append(pacinput);
              // Create the search box and link it to the UI element.
       var input = document.getElementById('pac-input');
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
       var autocomplete = new google.maps.places.Autocomplete(input,{ types: ['geocode'] });
      google.maps.event.addListener(autocomplete,'place_changed', function () {
      var place = autocomplete.getPlace();
      var lat = place.geometry['location'].lat(),
            lng = place.geometry['location'].lng(),
            placeName = "",
            latlng = new google.maps.LatLng(lat, lng);
            // $('#lat'+id).val(results[0].geometry.location.lat());
            // $('#lng'+id).val(results[0].geometry.location.lng());
        moveMarker(placeName, latlng);
      // place variable will have all the information you are looking for.
      $('#lat'+id).val(place.geometry['location'].lat());
      $('#lng'+id).val(place.geometry['location'].lng());
    });
     
      
        google.maps.event.addListener(map, 'click', function (event) {
            
            $('.MapLat').val(event.latLng.lat());
            $('.MapLon').val(event.latLng.lng());
            infowindow.close();
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({
                        "latLng":event.latLng
                    }, function (results, status) {
                        console.log(results, status);
                        if (status == google.maps.GeocoderStatus.OK) {
                            console.log(results);
                            var lat = results[0].geometry.location.lat(),
                                lng = results[0].geometry.location.lng(),
                                placeName = results[0].address_components[0].long_name,
                                latlng = new google.maps.LatLng(lat, lng);
                                $('#lat'+id).val(results[0].geometry.location.lat());
      							$('#lng'+id).val(results[0].geometry.location.lng());
                            moveMarker(placeName, latlng);
                        }
                    });
        });
       
        function moveMarker(placeName, latlng) {
            marker.setIcon(image);
            marker.setPosition(latlng);
            infowindow.setContent(placeName);
             map.setCenter(latlng);
        }
            $("#reset_state").click(function() {
                infowindow.close();
                map.fitBounds(latlng);
              })
            

    });

  	}

  $scope.addNewRoute = function(){

        $scope.routeDetails.push({ 
            'name': "", 
            'cost': "",
            'order': "",
            
        });
    };

$scope.removeRoute = function(){

    var newExpDataList=[];
    $scope.selectedAll = false;
    angular.forEach($scope.routeDetails, function(selected){
        if(!selected.selected){
            newExpDataList.push(selected);
        }
    }); 
    $scope.routeDetails = newExpDataList;
};



});





 
</script>

@stop
 
 