@extends($layout)

@section('header_scripts')
<link href="{{CSS}}animate.css" rel="stylesheet"> 

@stop
@section('custom_div')
<?php $data = null;
    if(isset($allocated_periods))
    {
        $data = $allocated_periods;
    }
?>
<div ng-controller="TimetableController" ng-init="ingAngData({{$data}})">
@stop
@section('content')
<div id="page-wrapper" >
    <section id="main" class="main-wrap bgc-white-darkest" role="main">
        <div class="container-fluid content-wrap">
            <div class="row">
                <div class="col-lg-12">
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{PREFIX}}">
                                <i class="fa fa-home">
                                </i>
                            </a>
                        </li>
                       @if($role!='staff')
                        <li>
                            <a href="{{URL_TIMETABLE_DASHBOARD}}">
                                {{getPhrase('timetable_dashboard')}}
                            </a>
                        </li>
                        @endif
                         @if(checkRole(getUserGrade(2)))
                           <li><a href="{{URL_USERS_DASHBOARD}}">{{ getPhrase('users_dashboard') }}</a> </li>
                           

                        <li><a href="{{URL_USERS."staff"}}">{{ getPhrase('staff_users') }}</a> </li>
                        @endif
                        <li><a href="{{URL_STAFF_DETAILS.$record->slug}}">{{ $record->name }} {{getPhrase('details') }}</a> </li> 
                        <li>
                            
                                {{$title}}
                            
                        </li>
                    </ol>
                </div>
            </div>
            {!! Form::open(array('url' => URL_UPDATE_TIMETABLE, 'method' => 'POST', 'name'=>'idCards ', 'novalidate'=>'')) !!}
            <section class="col-sm-12 col-md-12 col-lg-12 col-xl-9 panel-wrap panel-grid-item">
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                        <h2 class="pull-left"> {{getPhrase('timetable_for_').' '.$user->name}}  
                        </h2>
                      <!--Start panel icons-->
                      <div class="panel-icons panel-icon-slide bgc-white-dark">

                            <ul>
                                <li><a href=""><i class="fa fa-angle-left"></i></a>
                                  <ul>
                                      <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                                      <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                                      <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
                                      <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
                                      <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li>
                                      <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li>
                                  </ul>
                              </li>
                          </ul>
                      </div>
                      <!--End panel icons-->
                    </div>
                    <div class="panel-body panel-body-p">
                        <a  href="javascript:void(0);" 
                            class="pull-right btn btn-link"
                            ng-click="toggleCalender()"
                            ng-show = "showCalender"
                            > 
                        Back </a>
                        <a target="_blank" href="{{URL_TIMETABLE_STAFF_STUDENT_PRINT.$user->slug}}" class="btn btn-primary pull-right" >
                            {{getPhrase('print')}}</a> 
                        @include('timetable.user-timetables.calender-view')
                        <br>
                    </div>
                </div>
                {!! Form::close() !!}                                               
                </hr>
            </section>
            <section class="col-sm-12 col-md-12 col-lg-12 col-xl-3 panel-wrap panel-grid-item">
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                        <h2 class="pull-left"> 
                        </h2>
                      <!--Start panel icons-->
                      <div class="panel-icons panel-icon-slide bgc-white-dark">

                            <ul>
                                <li><a href=""><i class="fa fa-angle-left"></i></a>
                                  <ul>
                                      <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                                      <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                                      <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
                                      <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
                                      <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li>
                                      <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li>
                                  </ul>
                              </li>
                          </ul>
                      </div>
                      <!--End panel icons-->
                    </div>
                    <div class="panel-body panel-body-p">
                        <?php $data = '';
                        if(isset($right_bar_data))
                            $data = $right_bar_data;
                        ?>
                        @include($right_bar_path, array('data' => $data))
                    </div>
                </div>
            </section>
        </div>
    </section>
</div>



@stop

@section('footer_scripts')
  
    @include('timetable.user-timetables.scripts.js-scripts')
    @include('common.alertify')
@stop

@section('custom_div_end')
</div>
@stop