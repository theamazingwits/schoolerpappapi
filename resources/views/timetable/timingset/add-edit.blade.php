@extends('layouts.admin.adminlayout')

<!-- <link href="{{CSS}}plugins/datetimepicker/css/bootstrap-datetimepicker.css" rel="stylesheet">	 -->

@section('custom_div')
<?php 
$data = null;
if($record) {
 
	$data = $timingset;
	
}
?>
<div ng-controller="timigsetController" ng-init="intilizeData({{$data}})">
 
<?php $button_name = getPhrase('create'); ?>
					@if ($record)
					 <?php $button_name = getPhrase('update'); ?>
						{{ Form::model($record, 
						array('url' => URL_TIMINGSET_EDIT.'/'.$record->slug, 
						'method'=>'patch', 'name'=>'formTimingset ', 'novalidate'=>'')) }}
					@else
						{!! Form::open(array('url' => URL_TIMINGSET_ADD, 'method' => 'POST', 'name'=>'formTimingset ', 'novalidate'=>'')) !!}
					@endif
@stop

@section('content')
<div id="page-wrapper">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
        <div class="container-fluid content-wrap">
			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>
						<li><a href="{{URL_ACADEMICOPERATIONS_DASHBOARD}}">  {{ getPhrase('academic_operatons')}}</a></li> 
						<li><a href="{{URL_TIMETABLE_DASHBOARD}}">  {{ getPhrase('timetable_dashboard')}}</a></li> 
						<li><a href="{{URL_TIMINGSET}}">{{ getPhrase('timing_sets')}}</a> </li>
						<li class="active">{{isset($title) ? $title : ''}}</li>
					</ol>
				</div>
			</div>
			@include('errors.errors')	

			<div class="row">
				<section class="col-sm-12 col-md-12 col-lg-12 col-xl-7 panel-wrap panel-grid-item">
                <!--Start Panel-->
	                <div class="panel bgc-white-dark">
	                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
	                        <h2 class="pull-left"> {{ $title }} </h2>
		                        
	                      	<div class="pull-right messages-buttons helper_step1" style ="margin-right: 2em;color: white;">
								<a href="{{URL_TIMINGSET}}" class="btn  btn-primary button panel-header-button" >{{ getPhrase('list')}}</a>
							</div>
	                        <!--End panel icons-->
	                    </div>
	                    <div class="panel-body panel-body-p">


							 @include('timetable.timingset.form_elements', 
							 array('button_name'=> $button_name),
							 array('record' => $record))
						
						</div>
					</div>
				</section>
				<section class="col-sm-12 col-md-12 col-lg-12 col-xl-5 panel-wrap panel-grid-item">
                <!--Start Panel-->
	                <div class="panel bgc-white-dark">
	                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
	                        <h2 class="pull-left"> {{getPhrase('available_timesets')}} </h2>
		                        
	                        <!--End panel icons-->
	                    </div>
	                    <div class="panel-body panel-body-p">


							 @include('timetable.timingset.right-bar')
						</div>
					</div>
				</section>
				
			</div>
			 
		</div>
		<!-- /.container-fluid -->
	</section>
</div>
<!-- /#page-wrapper -->
@stop
@section('footer_scripts')
  <script src="{{JS}}bootstrap-toggle.min.js"></script>   
  @include('common.alertify')
  @include('timetable.timingset.scripts.js-scripts');
  @include('common.validations', array('isLoaded'=>1));
  <script src="{{JS}}moment.min.js"></script>
  <!-- <script src="{{JS}}plugins/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
 <script>
 
      $(function () {
        $('.datetimepicker').datetimepicker({
        	  format: 'HH:mm',
        	  stepping: 10,
        });
        
    });
 </script> -->

@stop
 
@section('custom_div_end')
{!! Form::close() !!}
</div>
@stop