 	<div class="row">
 			<fieldset class="form-group col-md-4">
				
				{{ Form::label('salary_grades', getphrase('salary_grades')) }}
				<span class="text-red">*</span>
				
				{{ Form::text('salary_grades', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('salary_grades'),

					         'ng-model'=>'salary_grades', 

							'required'=> 'true', 

							'ng-class'=>'{"has-error": formSt.salary_grades.$touched && formSt.salary_grades.$invalid}'

							)) }}

						<div class="validation-error" ng-messages="formSt.salary_grades.$error" >

	    					{!! getValidationMessage()!!}
           
						</div>
			</fieldset>

			<fieldset class="form-group col-md-4">
				
					{{ Form::label('basic_salary', getphrase('basic_salary')) }}

						<span class="text-red">*</span>
				 
                   {{ Form::number('basic_salary', null , $attributes = array('class'=>'form-control amt', 'placeholder' => getPhrase('basic_salary'),
                              'id' => 'basic_salary',
                              'string-to-number',
                   	          'ng-model'=>'basic_salary', 
                   	          'required'=> 'true', 

							'ng-class'=>'{"has-error": formSt.basic_salary.$touched && formSt.basic_salary.$invalid}',

							)) }} 

					<div class="validation-error" ng-messages="formSt.basic_salary.$error" >

	    					{!! getValidationMessage()!!}
                {!! getValidationMessage('number')!!}
           
						</div>

			</fieldset>

			<fieldset class="form-group col-md-4">

					{{ Form::label('overtime_rate', getphrase('overtime_rate').' ('.getPhrase('per_hour').')') }}

						<span class="text-red">*</span>
				 
                   {{ Form::number('overtime_rate', null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('overtime_rate'),
                              'id'=>'overtime_rate',
                              'string-to-number',
                   	          'ng-model'=>'overtime_rate', 
                   	          'required'=> 'true', 
                              'min'=>0,

							'ng-class'=>'{"has-error": formSt.overtime_rate.$touched && formSt.overtime_rate.$invalid}',

							)) }}

					<div class="validation-error" ng-messages="formSt.overtime_rate.$error" >

	    					{!! getValidationMessage()!!}
                {!! getValidationMessage('number')!!}
           
						</div>

			</fieldset>

    </div>

    <div class="row">
			<div class="col-md-6">
            <h4>{{ getPhrase('allowances') }}</h4>
            <table class="table" id="tbl_allowances">
                <tbody>
                    <?php
                            if(!empty($allowances)) {

                                foreach ($allowances as $key => $value) {

                                    if(++$key == 1) {
                                        $action_btn = '<span class="input-group-btn"><button tabindex="-1" class="btn btn-success" type="button" id="btn_add_row" onclick="addRow(\'tbl_allowances\');" > <i class="fa fa-plus" aria-hidden="true"></i></button> </span>';
                                    } else {
                                        $action_btn = '<span class="input-group-btn"><button tabindex="-1" class="btn btn-danger" type="button" id="btn_remove_row" onclick="deleteRow(this);" > <i class="fa fa-minus" aria-hidden="true"></i></button> </span>';
                                    }

                     ?>
                    <tr class="allowances-list" id="<?php echo $key; ?>">
                        <td>
                            <input class="form-control" placeholder="{{getphrase('allowance_name')}}" name="allowance_name[]" id="allowance_name_<?php echo $key; ?>" type="text" value="<?php echo (!empty($value->allowance_name)) ? $value->allowance_name: ''; ?>" />
                          </td>
                          <td>
                            <input class="form-control amt" placeholder="{{getphrase('allowance_value')}}" name="allowance_value[]" id="allowance_value_<?php echo $key; ?>" type="text" value="<?php echo (!empty($value->allowance_value)) ? $value->allowance_value: ''; ?>" />
                          </td>
                        <td> <?php echo $action_btn; ?> </td>
                    </tr>
                    <?php } } else { ?>
                    <tr class="allowances-list" id="1">
                        <td>
                            <input class="form-control" placeholder="{{getphrase('allowance_name')}}" name="allowance_name[]" id="allowance_name_1" type="text" value="{{getphrase('house_rent')}}" />
                          </td>
                        <td>
                            <input class="form-control amt" placeholder="{{getphrase('allowance_value')}}" name="allowance_value[]" id="allowance_value_1" type="text" />
                        </td>
                        <td> <span class="input-group-btn">
                          <button tabindex="-1" class="btn btn-success" type="button" id="btn_add_row" onclick="addRow('tbl_allowances');" >
                                    <i class="fa fa-plus" aria-hidden="true"></i></button>
                                </span> </td>
                    </tr>
                    <?php } ?>


                    <input type="hidden" name="cust_row_cnt" id="cust_row_cnt" value="<?php if(!empty($allowances)) echo count($allowances); else echo '1'; ?>" />
                </tbody>
            </table>
          </div>


          <div class="col-md-6">
            <h4>{{ getPhrase('deduction') }}</h4>
            <table class="table" id="tbl_deductions">
                <tbody>
                    <?php
                            if(!empty($deductions)) {

                                foreach ($deductions as $key => $value) {

                                    if(++$key == 1) {
                                        $action_btn = '<span class="input-group-btn"><button tabindex="-1" class="btn btn-success" type="button" id="btn_add_row" title="getphrase("add_row")" onclick="addRow(\'tbl_deductions\');" > <i class="fa fa-plus" aria-hidden="true"></i></button> </span>';
                                    } else {
                                        $action_btn = '<span class="input-group-btn"><button tabindex="-1" class="btn btn-danger" type="button" id="btn_remove_row" onclick="deleteRow(this);" > <i class="fa fa-minus" aria-hidden="true"></i></button> </span>';
                                    }

                     ?>
                    <tr class="deductions-list" id="<?php echo $key; ?>">
                        <td>
                            <input class="form-control" placeholder="{{getphrase('deduction_name')}}" name="deduction_name[]" id="deduction_name_<?php echo $key; ?>" type="text" value="<?php echo (!empty($value->deduction_name)) ? $value->deduction_name: ''; ?>" />
                          </td>
                          <td>
                            <input class="form-control amt" placeholder="{{getphrase('deduction_value')}}" name="deduction_value[]" id="deduction_value_<?php echo $key; ?>" type="text" value="<?php echo (!empty($value->deduction_value)) ? $value->deduction_value: ''; ?>" />
                          </td>
                        <td> <?php echo $action_btn; ?> </td>
                    </tr>
                    <?php } } else { ?>
                    <tr class="deductions-list" id="1">
                        <td>
                            <input class="form-control" placeholder="{{getphrase('deduction_name')}}" name="deduction_name[]" id="deduction_name_1" type="text" value="{{getphrase('provident_fund')}}" />
                          </td>
                        <td>
                            <input class="form-control amt" placeholder="{{getphrase('deduction_value')}}" name="deduction_value[]" id="deduction_value_1" type="text" />
                        </td>
                        <td> <span class="input-group-btn">
                          <button tabindex="-1" class="btn btn-success" type="button" id="btn_add_row" title="{{getphrase('add_row')}}" onclick="addRow('tbl_deductions');" >
                                    <i class="fa fa-plus" aria-hidden="true"></i></button>
                                </span> </td>
                    </tr>
                    <?php } ?>


                    <input type="hidden" name="cust_row_cnt_ded" id="cust_row_cnt_ded" value="<?php if(!empty($deductions)) echo count($deductions); else echo '1'; ?>" />
                </tbody>
            </table>
          </div>
        </div>


        <div class="row">
          <div class="col-md-8 col-md-offset-4">
          <br />
          <h5>{{ getPhrase('total_salary_details') }}</h5>
          	<table class="table table-bordered">
          		<tbody>
          			<tr>
          				<td valign="middle">
          					<label>{{getPhrase('gross_salary')}}</label>
          				</td>
          				<td>
                    {{ Form::text('gross_salary', $value = null , $attributes = array('class'=>'form-control', 
                          'readonly'=>'readonly',
                           'id'=>'gross_salary',

                      )) }}
          				</td>
          			</tr>
          			<tr>
          				<td valign="middle">
          					<label>{{getPhrase('total_deduction')}}</label>
          				</td>
          				<td>
                    {{ Form::text('total_deduction', $value = null , $attributes = array('class'=>'form-control', 
                          'readonly'=>'readonly',
                           'id'=>'total_deduction',

                      )) }}
          				</td>
          			</tr>
          			<tr>
          				<td valign="middle">
          					<label>{{getPhrase('net_salary')}}</label>
          				</td>
          				<td>
                    {{ Form::text('net_salary', $value = null , $attributes = array('class'=>'form-control', 
                          'readonly'=>'readonly',
                           'id'=>'net_salary',

                      )) }}
          				</td>
          			</tr>
          		</tbody>
          	</table>
          </div>
		    </div>
			 
		<div class="buttons text-center">

			<button class="btn btn-lg btn-primary button"

			ng-disabled='!formSt.$valid'>{{ $button_name }}</button>

		</div>



<style>
  .error {
    background: #fff4f2;
    color: #e4391f;
    padding: 3px 10px;
    font-size: 13px;
  }
</style>

<script src="{{JS}}jquery-1.12.1.min.js"></script>	 
<script>
function addRow(tableID)
{

      var table = document.getElementById(tableID);

      var rowCount = table.rows.length;
      var row = table.insertRow(rowCount);

      var cell0   = row.insertCell(0);
      var cell1   = row.insertCell(1);
      var cell2   = row.insertCell(2);


      if(tableID == "tbl_allowances") {
      	var custRowId = 'cust_row_cnt';
      	row.setAttribute('class', 'allowances-list');
      }
      else if(tableID == "tbl_deductions") {
      	var custRowId = 'cust_row_cnt_ded';
      	row.setAttribute('class', 'deductions-list');
      }

      var custRowCnt = parseInt($('#'+custRowId).val()) + 1;
      $('#'+custRowId).val(custRowCnt);

      row.setAttribute('id', custRowCnt);


      if(tableID == "tbl_allowances") {

	      cell0.innerHTML = cell0.innerHTML + ' <input class="form-control" placeholder="{{getphrase("allowance_name")}}" name="allowance_name[]" id="allowance_name_'+custRowCnt+'" type="text" /> ';

	      cell1.innerHTML = cell1.innerHTML +' <input class="form-control amt" placeholder="{{getphrase("allowance_value")}}" name="allowance_value[]" id="allowance_value_'+custRowCnt+'" type="text" /> ';
  	  }
  	  else if(tableID == "tbl_deductions") {

  	      cell0.innerHTML = cell0.innerHTML + ' <input class="form-control" placeholder="{{getphrase("deduction_name")}}" name="deduction_name[]" id="deduction_name_'+custRowCnt+'" type="text" /> ';

  	      cell1.innerHTML = cell1.innerHTML +' <input class="form-control amt" placeholder="{{getphrase("deduction_value")}}" name="deduction_value[]" id="deduction_value_'+custRowCnt+'" type="text" /> ';
  	  }

      cell2.innerHTML = cell2.innerHTML +' <span class="input-group-btn"><button tabindex="-1" class="btn btn-danger" type="button" id="btn_remove_row" onclick="deleteRow(this);" > <i class="fa fa-minus" aria-hidden="true"></i></button> </span> ';

}

function deleteRow(el)
{

      // while there are parents, keep going until reach TR 
      while (el.parentNode && el.tagName.toLowerCase() != 'tr') {
          el = el.parentNode;
      }

      // If el has a parentNode it must be a TR, so delete it
      if (el.parentNode) {
          el.parentNode.removeChild(el);
      }
      calc_total();
}

$(document).on('keyup', '.amt', function() {

    id = $(this).attr('id');

    if(!($(this).val() >= 0)) {
      $('#validatn_msg'+id).remove();
      $(this).after('<div class="error" id="validatn_msg'+id+'">Please Enter Valid Number</div>');
      $('form').addClass('ng-invalid ng-invalid-number');
      $('button.button').attr('disabled', 'disabled');
    } else {
      $('#validatn_msg'+id).remove();
      $('form').removeClass('ng-invalid ng-invalid-number').addClass('ng-valid ng-valid-number');
      $('button.button').removeAttr('disabled');
    }

    calc_total();

});


function calc_total()
{
    allowances_total  = 0;
    deduction_total   = 0;
    gross_salary      = 0;
    net_salary        = 0;

    basic_salary = ($('#basic_salary').val() > 0) ? parseFloat($('#basic_salary').val()) : 0;

    //Allowances Total
    $('input[name="allowance_value[]').each(function() {
      if($(this).val() > 0)
        allowances_total += parseFloat($(this).val());
    });

    //Deduction Total
    $('input[name="deduction_value[]').each(function() {
      if($(this).val() > 0)
        deduction_total += parseFloat($(this).val());
    });

    //Calculate Gross Salary
    gross_salary += (basic_salary + allowances_total);

    //Calculate Net Salary
    net_salary += (gross_salary - deduction_total);

    //Assign Values
    $('#gross_salary').val(gross_salary);
    $('#total_deduction').val(deduction_total);
    $('#net_salary').val(net_salary);


}


</script>