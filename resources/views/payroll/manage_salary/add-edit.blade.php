@extends($layout)

@section('header_scripts')
<link href="{{CSS}}ajax-datatables.css" rel="stylesheet">
<link href="{{CSS}}plugins/datetimepicker/css/bootstrap-datetimepicker.css" rel="stylesheet">  
@stop

@section('content')
<div id="page-wrapper">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
	    <div class="container-fluid content-wrap">
			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>
						<li><a href="{{URL_PAYROLL_MANAGE_SALARY}}">{{ getPhrase('manage_salary')}}</a> </li>
						<li class="active">{{isset($title) ? $title : ''}}</li>
					</ol>
				</div>
			</div>
				@include('errors.errors')
			<!-- /.row -->
			
			<section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item " >
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                        <h2 class="pull-left"> {{ $title }} </h2>
                        
                       	<div class="pull-right messages-buttons helper_step2">
							<a href="{{URL_PAYROLL_MANAGE_SALARY}}" class="btn  btn-primary button panel-header-button" >{{ getPhrase('list')}}</a>
						</div>
                        <!--End panel icons-->
                    </div>
                    <div class="panel-body panel-body-p packages">
						<?php $button_name = getPhrase('create'); ?>
						@if ($record)
						 <?php $button_name = getPhrase('update'); ?>
							{{ Form::model($record, 
							array('url' => URL_PAYROLL_MANAGE_SALARY_EDIT.$record->id, 
							'method'=>'patch','name'=>'formMs ', 'novalidate'=>'')) }}
						@else
							{!! Form::open(array('url' => URL_PAYROLL_MANAGE_SALARY_ADD, 'method' => 'POST','name'=>'formMs ', 'id'=>'formMs', 'novalidate'=>'')) !!}
						@endif

						 @include('payroll.manage_salary.form_elements', array('button_name'=> $button_name, 'record'=>$record,  'roles'=>$roles, 'salary_types'=>$salary_types))


						 
						{!! Form::close() !!}
				 

					</div>
				</div>
			</section>
		</div>
		<!-- /.container-fluid -->
	</section>
</div>
<!-- /#page-wrapper -->
@stop

 
@section('footer_scripts')
 @include('common.validations')
 

 @stop