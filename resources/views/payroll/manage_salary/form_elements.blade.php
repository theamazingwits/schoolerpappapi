 	<div class="row">
 		<div class="col-md-6">
 			
			<fieldset class="form-group">
				
				{{ Form::label('role', getphrase('role')) }}
				<span class="text-red">*</span>
				<?php 
                		$selected = "";
                		if(!empty($record->role_id))
                			$selected = $record->role_id;
                 ?>
				{{ Form::select('role_id', $roles, $selected, 
	                        [   'class'     => 'form-control', 
	                            "id"        => "role_id",
	                            "ng-model"  => "role_id",
	                            "onchange" => "getUsers()",
	                            'required'=> 'true', 
	                            'ng-class'=>'{"has-error": formMs.role_id.$touched && formMs.role_id.$invalid}',
	                        ])}}

						<div class="validation-error" ng-messages="formMs.role_id.$error" >

	    					{!! getValidationMessage()!!}
           
						</div>
			</fieldset>
			<fieldset class="form-group">

				{{ Form::label('user', getphrase('user')) }}
				<span class="text-red">*</span>

				{{ Form::select('user_id', array(''=>getPhrase('select')) , '', 
	                        [   'class'     => 'form-control', 
	                            "id"        => "user_id",
	                            "onchange" => "getUserDetails()",
	                            'required'=> 'true', 
	                        ])}}

			</fieldset>
			<fieldset class="form-group">
				
				{{ Form::label('salary_type', getphrase('salary_type')) }}
				<span class="text-red">*</span>
				<?php 
                		$selected = "";
                		if(!empty($record->salary_type))
                			$selected = $record->salary_type;
                 ?>
				{{ Form::select('salary_type', $salary_types, $selected, 
	                        [   'class'     => 'form-control', 
	                            "id"        => "salary_type",
	                            "ng-model"  => "salary_type",
	                            "onchange" => "getTemplates()",
	                            'required'=> 'true', 
	                            'ng-class'=>'{"has-error": formMs.salary_type.$touched && formMs.salary_type.$invalid}',
	                        ])}}

						<div class="validation-error" ng-messages="formMs.salary_type.$error" >

	    					{!! getValidationMessage()!!}
           
						</div>
			</fieldset>
			<fieldset class="form-group">

				{{ Form::label('template', getphrase('template')) }}
				<span class="text-red">*</span>

				{{ Form::select('template_id', array(''=>getPhrase('select_salary_type')) , '', 
	                        [   'class'     => 'form-control', 
	                            "id"        => "template_id",
	                            "onchange"  => "setTemplateName()",
	                            'required'=> 'true', 
	                        ])}}

				<input type="hidden" name="template_name" id="template_name" />
			</fieldset>
		</div>

		<div class="col-md-6" id="div_user_det" style="display: none;">
		</div>


    </div>

			 
		<div class="buttons text-center">

			<button class="btn btn-lg btn-primary button"

			ng-disabled='!formMs.$valid'>{{ $button_name }}</button>

		</div>

<script src="{{JS}}jquery-1.12.1.min.js"></script>
<script>

$(document).ready(function() {
	<?php if(!empty($record->branch_id) && !empty($record->role_id)) { ?>
		getUsers();
	<?php } ?>

	<?php if(!empty($record->salary_type)) { ?>
		getTemplates();
	<?php } ?>

	<?php if(!empty($record->template_id)) { ?>
		setTemplateName();
	<?php } ?>
});


	function getUsers()
	{
		// branch_id 	= $('#branch_id option:selected').val();
		role_id 	= $('#role_id option:selected').val();

		if(!role_id) {
			$('#user_id').empty();
			$('#user_id').append('<option value="">{{ getPhrase("select") }}</option>');
			return;
		}

		$.ajax({
				type: "get",
				url: "{{ URL_PAYROLL_MANAGE_SALARY_AJAXGETUSERS }}",
				data: {  role_id: role_id },
				success: function(resp) {
					$('#user_id').empty();
					$('#user_id').append(resp);
					<?php if(!empty($record->user_id)) { ?>
						$('#user_id').val('<?php echo $record->user_id; ?>');
					<?php } ?>
				}
		});
	}

	function getTemplates()
	{
		salary_type = $('#salary_type option:selected').val();

		if(!salary_type) {
			$('#template_id').empty();
			$('#template_id').append('<option value="">{{ getPhrase("select_salary_type") }}</option>');
			return;
		}

		$.ajax({
				type: "get",
				url: "{{ URL_PAYROLL_MANAGE_SALARY_AJAXGETTEMPLATES }}",
				data: { salary_type: salary_type },
				success: function(resp) {
					$('#template_id').empty();
					$('#template_id').append(resp);
					<?php if(!empty($record->template_id)) { ?>
						$('#template_id').val('<?php echo $record->template_id; ?>');
					<?php } ?>
				}
		});
	}

	function setTemplateName()
	{
		$('#template_name').val($('#template_id option:selected').text());
	}


	function getUserDetails()
	{
		user_id = $('#user_id option:selected').val();

		if(!user_id) {
			$('#div_user_det').html('');
			return;
		}

		$.ajax({
				type: "get",
				url: "{{ URL_PAYROLL_MANAGE_SALARY_AJAXGETUSERDETAILS }}",
				data: { user_id: user_id },
				success: function(resp) {
					$('#div_user_det').html(resp).slideDown();
				}
		});
	}


</script>