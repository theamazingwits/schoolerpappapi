 	<div class="row">
 			<fieldset class="form-group col-md-4">
				
				{{ Form::label('hourly_grades', getphrase('hourly_grades')) }}
				<span class="text-red">*</span>
				
				{{ Form::text('hourly_grades', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('hourly_grades'),

					         'ng-model'=>'hourly_grades', 

							'required'=> 'true', 

							'ng-class'=>'{"has-error": formHt.hourly_grades.$touched && formHt.hourly_grades.$invalid}'

							)) }}

						<div class="validation-error" ng-messages="formHt.hourly_grades.$error" >

	    					{!! getValidationMessage()!!}
           
						</div>
			</fieldset>


			<fieldset class="form-group col-md-4">

					{{ Form::label('hourly_rate', getphrase('hourly_rate')) }}

						<span class="text-red">*</span>
				 
                   {{ Form::number('hourly_rate', null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('hourly_rate'),
                              'id'=>'hourly_rate',
                              'string-to-number',
                   	          'ng-model'=>'hourly_rate', 
                   	          'required'=> 'true', 

							'ng-class'=>'{"has-error": formHt.hourly_rate.$touched && formHt.hourly_rate.$invalid}',

							)) }}

					<div class="validation-error" ng-messages="formHt.hourly_rate.$error" >

	    					{!! getValidationMessage()!!}
                {!! getValidationMessage('number')!!}
           
						</div>

			</fieldset>

    </div>

			 
		<div class="buttons text-center">

			<button class="btn btn-lg btn-primary button"

			ng-disabled='!formHt.$valid'>{{ $button_name }}</button>

		</div>