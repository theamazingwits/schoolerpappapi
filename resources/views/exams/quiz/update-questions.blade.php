@extends('layouts.admin.adminlayout')

 @section('custom_div')

 <div ng-controller="prepareQuestions">

 @stop
<?php
	if($right_bar === TRUE){
		$column = "col-xl-8";
		$column1 = "col-xl-6";
	}else{
		$column = "col-xl-12";
		$column1 = "col-xl-4";
	}
?>
@section('content')

<div id="page-wrapper">

	<section id="main" class="main-wrap bgc-white-darkest" role="main">
	    <div class="container-fluid content-wrap">

			<!-- Page Heading -->

			<div class="row">

				<div class="col-lg-12">

					<ol class="breadcrumb">

						<li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>
						<li><a  href="{{URL_EXAMS_DASHBOARD}}">{{ getPhrase('exams_dashboard')}}</a></li>


						<li><a href="{{URL_QUIZZES}}">{{ getPhrase('quizzes')}}</a></li>

						<li class="active">{{isset($title) ? $title : ''}}</li>

					</ol>

				</div>

			</div>

				@include('errors.errors')

			<?php $settings = ($record) ? $settings : ''; ?>

			<section class="col-sm-12 col-md-12 col-lg-12 {{$column}} panel-wrap panel-grid-item" ng-init="initAngData({{$settings}});">
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                        <h2 class="pull-left"> {{ $title }} </h2>
                        <div class="pull-right messages-buttons">

							<a href="{{URL_QUIZZES}}" class="btn  btn-primary button helper_step1 panel-header-button" >{{ getPhrase('list')}}</a>

						</div>
                        <!--End panel icons-->
                    </div>
                    <div class="panel-body panel-body-p">

						<?php $button_name = getPhrase('create'); ?>

				 		<div class="row">

							<fieldset class="form-group col-md-6">

								{{ Form::label('subject', getphrase('subjects')) }}

								<span class="text-red">*</span>

								{{Form::select('subject', $subjects, null, ['class'=>'form-control', 'ng-model' => 'subject_id', 

								'placeholder' => 'Select', 'ng-change'=>'subjectChanged(subject_id)','id'=>'helper_step2' ])}}

							</fieldset>


							<fieldset class="form-group col-md-6 helper_step3">

							{{ Form::label('difficulty', getphrase('difficulty')) }}
		

							<select ng-model="difficulty" class="form-control" >

							<option value="">{{getPhrase('select')}}</option>	

							<option value="easy">{{getPhrase('easy')}}</option>	

							<option value="medium">{{getPhrase('medium')}}</option>	

							<option value="hard">{{getPhrase('hard')}}</option>	

							</select>

							</fieldset>



							<fieldset class="form-group col-md-6 helper_step4">

							{{ Form::label('question_type', getphrase('question_type')) }}

								<select ng-model="question_type" class="form-control" >

								<option selected="selected" value="">{{getPhrase('select')}}</option>

								<option value="radio">{{getPhrase('single_answer')}}</option>

								<option value="checkbox">{{getPhrase('multi_answer')}}</option>

								{{-- <option value="descriptive">Discriptive</option> --}}

								<option value="blanks">{{getPhrase('fill_in_the_blanks')}}</option>

								<option value="match">{{getPhrase('match_the_following')}}</option>

								<option value="para">{{getPhrase('paragraph')}}</option>

								<option value="video">{{getPhrase('video')}}</option>
								<option value="audio">{{getPhrase('audio')}}</option>

								</select>

							</fieldset>



							<fieldset class="form-group col-md-6">

							{{ Form::label('searchTerm', getphrase('search_term')) }}

							{{ Form::text('searchTerm', $value = null , $attributes = array('class'=>'form-control', 

							'placeholder' => getPhrase('enter_search_term'),

							'ng-model'=>'searchTerm',
							'id'=>'helper_step5')) }}

							</fieldset>


							<div class="col-md-12" ng-show="contentAvailable">

							<div ng-if="subjectQuestions!=''" class="vertical-scroll" >

							<h4 class="text-success">Questions @{{ subjectQuestions.length }} </h4>
							<table  

							  class="table table-hover">

									 

								<th >{{getPhrase('subject')}}</th>

								<th>{{getPhrase('question')}}</th>

								<th>{{getPhrase('difficulty')}}</th>

								<th>{{getPhrase('type')}}</th>

								<th>{{getPhrase('marks')}}</th>	

								<th>{{getPhrase('action')}}</th>	


								<tr ng-repeat="question in subjectQuestions | filter: { difficulty_level:difficulty, question_type:question_type} | filter: searchTerm track by $index ">
									 

									<td>@{{subject.subject_title}}</td>

									<td 

									title="@{{subjectQuestions[$index].question}}" >

									@{{question.question}}

									</td>

									
									<td>@{{question.difficulty_level | uppercase}}</td>

									<td>@{{question.question_type | uppercase}}</td>

									<td>@{{question.marks}}</td>

									<td><a 
									 

									ng-click="addQuestion(question, subject);" class="btn btn-primary panel-header-button" >{{getPhrase('add')}}</a>

								  		

									  </td>

									

								</tr>

							</table>

							</div>	


				 			</div>


				 		</div>

					</div>


				</div>
			</section>
			@if(isset($right_bar))
			<section class="col-sm-12 col-md-12 col-lg-12 col-xl-4 panel-wrap panel-grid-item">
                <!--Start Panel-->
                    	<?php $data = '';

						if(isset($right_bar_data))

							$data = $right_bar_data;

						?>

						@include($right_bar_path, array('data' => $data))
            </section>
            @endif

		</div>

		<!-- /.container-fluid -->
	</section>

</div>

<!-- /#page-wrapper -->

@stop

@section('footer_scripts')

@include('exams.quiz.scripts.js-scripts')

@stop


@section('custom_div_end')

 </div>

@stop