@extends($layout)
@section('content')


				<section id="main" class="main-wrap bgc-white-darkest" role="main">
			<div class="container-fluid content-wrap">
				<div class="row panel-grid grid-stack">

					<section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-body pt-2 grid-stack-handle">
                                <div class="h-lg pos-r panel-body-p py-0">
									<h3 class=""><span class="fs-3 fw-light">{{ getPhrase('categories')}}</span> <i class="pull-right fa fa-random fs-1 m-2"></i></h3>
									<br><br><br><br>
                                    <a href="{{URL_QUIZ_CATEGORIES}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
								
                                </div>
                                    <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80" sparkLineColor="#fafafa"  sparkFillColor="#97d881" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>
					


					<section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-body pt-2 grid-stack-handle">
                                <div class="h-lg pos-r panel-body-p py-0">
									<h3 class=""><span class="fs-3 fw-light">{{ getPhrase('question_bank')}}</span> <i class="pull-right fa fa-question fs-1 m-2"></i></h3>
									<br><br><br><br>
                                    <a href="{{URL_QUIZ_QUESTIONBANK}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
									
                                </div>
                                <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80" sparkLineColor="#fafafa"  sparkFillColor="#f74948" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>
					


					<section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-body pt-2 grid-stack-handle">
                                <div class="h-lg pos-r panel-body-p py-0">
									<h3 class=""><span class="fs-3 fw-light">{{ getPhrase('exams')}}</span> <i class="pull-right fa fa-clock-o fs-1 m-2"></i></h3>
									<br><br><br><br>
                                    <a href="{{URL_QUIZZES}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
									
                                </div>
                                <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80" sparkLineColor="#fafafa"  sparkFillColor="#f3d74c" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>
					


					<section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-body pt-2 grid-stack-handle">
                                <div class="h-lg pos-r panel-body-p py-0">
									<h3 class=""><span class="fs-3 fw-light">{{ getPhrase('offline_exam_categories')}}</span> <i class="pull-right fa fa-sort-amount-asc fs-1 m-2"></i></h3>
									<br><br><br><br>
                                    <a href="{{URL_OFFLINEEXMAS_QUIZ_CATEGORIES}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
									
                                </div>
                                <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80" sparkLineColor="#fafafa"  sparkFillColor="#353f4d" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>
					

					<section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-body pt-2 grid-stack-handle">
                                <div class="h-lg pos-r panel-body-p py-0">
									<h3 class=""><span class="fs-3 fw-light">{{ getPhrase('offline_exams')}}</span> <i class="pull-right fa fa-external-link fs-1 m-2"></i></h3>
									<br><br><br><br>
                                    <a href="{{URL_OFFLINE_EXAMS}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
									
                                </div>
                                <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80" sparkLineColor="#fafafa"  sparkFillColor="#d2ae35" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>
					

					<section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-body pt-2 grid-stack-handle">
                                <div class="h-lg pos-r panel-body-p py-0">
									<h3 class=""><span class="fs-3 fw-light">{{ getPhrase('exam-series')}}</span> <i class="pull-right fa fa-list-ol fs-1 m-2"></i></h3>
									<br><br><br><br>
                                    <a href="{{URL_EXAM_SERIES}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
									
                                </div>
                                <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80" sparkLineColor="#fafafa"  sparkFillColor="#97d881" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>
					

					<section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-body pt-2 grid-stack-handle">
                                <div class="h-lg pos-r panel-body-p py-0">
									<h3 class=""><span class="fs-3 fw-light">{{ getPhrase('instructions')}}</span> <i class="pull-right fa fa-hand-o-right fs-1 m-2"></i></h3>
									<br><br><br><br>
                                    <a href="{{URL_INSTRUCTIONS}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
								
                                </div>
                                    <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80" sparkLineColor="#fafafa"  sparkFillColor="#aaaaaa" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>

		</div>
		</div>
	</section>


		<!-- /#page-wrapper -->

@stop

@section('footer_scripts')
 
 

@stop
