
@include('common.angular-factory')
<script>

 
     app.controller('staffAttedance', function ($scope, $http, httpPreConfig) {


     	$scope.viewCourse  = 0;

       $scope.getParentCourses  = function(branch_id){
       
           route = '{{ URL_GET_BRANCH_PARENT_COURSES }}',

           data  = {
           	         _method: 'post',
                     '_token':httpPreConfig.getToken(),
                     'branch_id': branch_id

                   };

          $scope.parent_courses =[];

          httpPreConfig.webServiceCallPost(route, data).then(function(result){

          $scope.parent_courses    = result.data;
         
        });


    }


    $scope.getStaffParentCourses = function(role_id){
        
        if(role_id  == 3){
          
          $scope.viewCourse  = 1;
        }else{

        	 $scope.viewCourse  = 0;
        }
    }
 
 
});
  
</script>