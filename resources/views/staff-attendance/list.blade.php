@extends($layout)
@section('header_scripts')
<link href="{{CSS}}ajax-datatables.css" rel="stylesheet">
@stop
@section('content')


<div id="page-wrapper" ng-controller="attendanceController" ng-init="initAngData('{{count($staff)}}');">
			<div class="container-fluid">
				<!-- Page Heading -->
				<div class="row">
					<div class="col-lg-12">
						<ol class="breadcrumb">
							<li><a href="{{PREFIX}}"><i class="mdi mdi-home"></i></a> </li>
                        
                         <li><a href="{{ URL_STAFF_ATTENDENCE_SELECTS }}">{{getphrase('select_branch')}}</a></li>
                        
							<li>{{ $title }}</li>
						</ol>
					</div>
				</div>
								
				<!-- /.row -->
				<div class="panel panel-custom">
					<div class="panel-heading">
					<div class="row">
						<div class="col-sm-8">
						
						<h1>{{ getPhrase('attendance_for').' '.$role_name.' - '.$title }}</h1>
						<p><strong>{{ getPhrase('date').' '.$submitted_data->attendance_date }}</strong></p>

						</div>
						 
						<div class="col-sm-4 text-right">
						<ul class="list-unstyled attendance_summary">
							<li class="clearfix">
								<p class="pull-left"><strong>Total:</strong> @{{total}}</p>
								<p class="pull-right"><strong>Present:</strong> @{{present}}</p>
							</li>
							<li class="clearfix">
								<p class="pull-left text-danger"><strong>Absent:</strong> @{{absent}}</p>
								<p class="pull-right"><strong>Leave:</strong> @{{leave}}</p>
							</li>
						</ul>
							<span >
							
						</span>		
						</div>
					</div>
						
						
						
						
					</div>
					<?php 
					?>
					{!! Form::open(array('url' => URL_STAFF_ATTENDENCE_UPDATE, 'method' => 'POST')) !!}

					<input type="hidden" name="branch_id" value="{{$submitted_data->branch_id}}">
					<input type="hidden" name="role_id" value="{{$submitted_data->role_id}}">
					<input type="hidden" name="course_parent_id" value="{{$submitted_data->course_parent_id}}">
					<input type="hidden" name="attendance_date" value="{{$submitted_data->attendance_date}}">
					<input type="hidden" name="attendance_taken" value="{{$attendance_taken}}">

					<div class="panel-body packages" id="myForm">
						<div class="table-responsive vertical-scroll"> 
						<table class="table table-striped table-bordered student-attendance-table datatable" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>{{ getPhrase('sno')}}</th>
									<th>{{ getPhrase('employee_id')}}</th>
									<th>{{ getPhrase('photo')}}</th>
									<th>{{ getPhrase('name')}}</th>
									<th>{{ getPhrase('attendance')}}</th>
									<th>{{ getPhrase('remarks')}}</th>
									<th>{{ getPhrase('notes')}}</th>
								</tr>
							</thead>
							<?php $sno = 1; ?>
							@foreach($staff as $record)
							<tr>
								<td>{{ $sno++ }}</td>
								<td>{{ $record->employee_id }}</td>
								<td><img src="{{getProfilePath($record->image)}}"> </td>
								<td>{{ ucwords( $record->name ) }}</td>
								<td>
									<div class="col-md-4">

									<?php 
									$present = true;
									$absent = false;
									$leave = false;
									$remarks = '';
									$notes = '';
									if($attendance_taken) { 
										foreach($attendance_records as $atr)
										{	


											if($record->id == $atr->user_id)
											{
												$present 	= false;
												$absent 	= false;
												$leave 		= false;
												$notes = $atr->notes;
												$remarks = $atr->remarks;
												switch ($atr->attendance_code) {
													case 'P':
															$present = true;
														break;
													case 'A':
															$absent = true;
														break;
													case 'L':
															$leave = true;
														break;
													
													default:
														$present = true;
														break;
												}
												break;
											}
										}
									 } ?>

							{{ Form::radio('attendance_code', 'P', $present, array(
								'id'=>'present'.$record->id,
								'name'=>'attendance_code['.$record->id.']',
								'ng-click' => 'updateCount()',
								'class' => 'attendance_code'
							)) }}
								
								<label for="present{{$record->id}}"> <span class="fa-stack radio-button"> <i class="mdi mdi-check active"></i> </span> {{getPhrase('present')}}</label> 
							</div>
							<div class="col-md-4">
							{{ Form::radio('attendance_code', 'A', $absent, array('id'=>'absent'.$record->id, 'name'=>'attendance_code['.$record->id.']',
								'ng-click' => 'updateCount()'
							)) }}
								<label for="absent{{$record->id}}"> <span class="fa-stack radio-button"> <i class="mdi mdi-check active"></i> </span> {{getPhrase('absent')}} </label>
							</div>
							<div class="col-md-4">
							{{ Form::radio('attendance_code', 'L', $leave, array('id'=>'leave'.$record->id, 'name'=>'attendance_code['.$record->id.']',
							'ng-click' => 'updateCount()'
							)) }}
								<label for="leave{{$record->id}}"> <span class="fa-stack radio-button"> <i class="mdi mdi-check active"></i> </span> {{getPhrase('leave')}} </label>
							</div>
								</td>
								<td>
									<fieldset class="form-group">
									 {{ Form::textarea('remarks', $remarks , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('remarks'), 'rows'=>1, 'cols'=>15, 'name'=>'remarks['.$record->id.']')) }}										 
									</fieldset>
								</td>
								<td>
									<fieldset class="form-group">
									 {{ Form::textarea('notes', $notes , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('notes'), 'rows'=>1, 'cols'=>15, 'name'=>'notes['.$record->id.']')) }}
										 
									</fieldset>
								</td>
							</tr>

							@endforeach
						</table>
						</div>
						<div class="buttons text-center">
							<button class="btn btn-lg btn-primary button">{{ getPhrase('update') }}</button>
						</div>
					</div>
					</form>
				</div>

			</div>
			<!-- /.container-fluid -->
		</div>
@endsection
 

@section('footer_scripts')
  
@include('staff-attendance.scripts.attendance-script') 

@stop
