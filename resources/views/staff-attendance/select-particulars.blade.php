@extends($layout)
@section('header_scripts')
<link href="{{CSS}}plugins/datetimepicker/css/bootstrap-datetimepicker.css" rel="stylesheet">   

@stop

@section('content')
<div id="page-wrapper" ng-controller="staffAttedance">
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <ol class="breadcrumb">
                    <li>
                        <a href="{{PREFIX}}">
                            <i class="mdi mdi-home">
                            </i>
                        </a>
                    </li>
                 
                    <li>{{getphrase('select_branch')}}</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
        <div class="panel panel-custom">
            <div class="panel-heading">
                <h1>
                    {{getPhrase('select_details')}}
              
                </h1>
            </div>
            <div class="panel-body instruction">
              


                <div class="row">
                <div class="col-md-6">
                <h3>{{ getPhrase('general_instructions') }}</h3>
                        <ul class="guide">
                            <li>
                                <span class="answer">
                                    <i class="mdi mdi-check">
                                    </i>
                                </span>
                                {{getPhrase('present')}}
                            </li>
                            <li>
                                <span class="notanswer">
                                    <i class="mdi mdi-close">
                                    </i>
                                </span>
                                {{getPhrase('absent')}}
                            </li>
                            <li>
                                <span class="marked">
                                    <i class="mdi mdi-eye">
                                    </i>
                                </span>
                                {{getPhrase('leave')}}
                            </li>
                            
                        </ul>
                    </div>
                     {!! Form::open(array('url' => URL_GET_BRANCH_STAFF, 'method' => 'POST')) !!}


                 <div class ="col-md-6">

                         <fieldset class="form-group col-md-12">

                        {{ Form::label ('branch_id', getphrase('branch')) }}
                        <span class="text-red">*</span>
                        {{ Form::select('branch_id', $branches, '', 
                        [   'class'     => 'form-control', 
                            "id"        => "select_branch",
                            "ng-model"  => "branch_id",
                             "ng-change" => "getParentCourses(branch_id)"
                        ])}}
                    </fieldset>


                     <fieldset ng-if="branch_id" class="form-group col-md-12">

                        {{ Form::label ('role_id', getphrase('staff_type')) }}
                        <span class="text-red">*</span>
                        {{ Form::select('role_id', $roles, '', 
                        [   
                            'class'     => 'form-control', 
                            "id"        => "select_branch",
                            "ng-model"  => "role_id",
                            'placeholder'=>getPhrase('select'),
                           "ng-change" => "getStaffParentCourses(role_id)"

                        ])}}
                    </fieldset>


                      <fieldset ng-if = "viewCourse == 1 " class="form-group col-md-12">

                         <label for = "course_parent_id">{{getPhrase('parent_course')}}</label>
                         
                        <select 
                        name      = "course_parent_id" 
                        id        = "course_parent_id" 
                        class     = "form-control" 
                        ng-model  = "course_parent_id" 
                        ng-options= "option.id as option.course_title for option in parent_courses track by option.id">
                        <option value="">{{getPhrase('select')}}</option>
                        </select>
                    </fieldset>

                       




                        <fieldset class="form-group col-md-12">
                                     
                        {{ Form::label('attendance_date', getphrase('attendance_date')) }}
                                                <span class="text-red">*</span>
                        <div class="input-group date" data-date="{{date('Y/m/d')}}" data-provide="datepicker" data-date-format="{{getDateFormat()}}">
                        {{ Form::text('attendance_date', $value = date('Y/m/d') , $attributes = array('class'=>'form-control', 'placeholder' => '2015/7/17', 'id'=>'dp')) }}
                            <div class="input-group-addon">
                                <span class="mdi mdi-calendar"></span>
                            </div>
                        </div>
                        </fieldset>
            
                </div>


                <hr>
                       
                            
                                <div class="text-center">
                                    <button type="submit" class="btn button btn-lg btn-primary">
                                        {{getPhrase('get_details')}}
                                    </button>
                                </div>
                            
                        
                        {!! Form::close() !!}
                    
                </hr>
            </div>
        </div>
    </div>
</div>
@stop
 
 

@section('footer_scripts')
 
 <script src="{{JS}}moment.min.js"></script>

  <script src="{{JS}}plugins/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
 <script>
    
     $(function () {
        $('#dp').datetimepicker({
               format: 'YYYY-MM-DD',
                maxDate: 'now'
            
            });
    });
 </script>
    @include('staff-attendance.scripts.js-scripts')
    
@stop