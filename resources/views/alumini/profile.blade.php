@extends('layouts.alumini.layout')

@section('header_scripts')


@stop


@section('breadcrumb')

<div class="container">
    <ol class="breadcrumb">
       
        @if($user->user_id  != Auth::user()->id )   
          
          <li><a href="{{URL_SEARCH_ALUMNI_USERS}}">Alumni</a></li>

        @endif
        <li class="active">Alumnus Details</li>
    </ol>
</div>



@endsection



@section('content')
{{-- {{dd($user)}} --}}
     <!--MAIN Content-->
            <div class="col-md-8">
                <div id="page-main">
                    <section id="members">
                        <header><h1>Alumnus Details</h1></header>
                  <div class="author-block member-detail">  
                         <figure class="author-picture">
							  <img src={{getProfilePath($user->image,'profile')}} alt="">
                            
		                    </figure>
                            <article class="paragraph-wrapper">
                                <div class="inner">
                                    <header>
                                        <h2>{{ucwords($user->name)}}</h2>

                                     @if($user->user_id  == Auth::user()->id )   

                                       <a href="{{URL_EDIT_ALUMNI_PROFILE.$user->user_id}}" class="btn btn-framed pull-right">
                                            <i class="fa fa-pencil"> Edit Profile</i>
                                        </a>

                                     @endif   
                                                                        </header>
                                    <hr>
                                    <figure style="color:#262626">
                                        {{ucwords($user->alumni_profession)}}                                    </figure>
                                    <hr>
                                    <p class="quote">
                                        @if($user->quote)
                                         {{$user->quote}}
                                        @else
                                        -
                                        @endif
                                                                     
                                          </p>
                                    <hr>
                                    <div class="contact">
                                        <strong>Social Links</strong>
                                        <div class="icons">
										  <a href="{{$user->alumni_social}}" target="_blank">
                                                <i class="fa fa-linkedin" style="color: #007bb5"></i>
                                            </a>
                                                                                </div><!-- /.icons -->
                                    </div>
                                    <h3>Details</h3>
                                    <div class="table-responsive">
                                        <table class="table course-list-table">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr style="color:#262626">
                                                    <td class="course-title">Passed Out Year</td>
                                                    <td>
                                                        {{$user->alumni_class}}                                                    </td>
                                                </tr>
                                                <tr style="color:#262626">
                                                    <td class="course-title">Email</td>
                                                    <td>
                                                        {{$user->email}}                                                   </td>
                                                </tr>
                                                <tr style="color:#262626">
                                                    <td class="course-title">Mobile</td>
                                                    <td>
                                                        {{$user->phone}}                                            </td>
                                                </tr>
                                                <tr style="color:#262626">
                                                    <td class="course-title">Website</td>
                                                    <td>
                                                         <a href="{{$user->alumni_social}}" target="_blank">
                                                            {{$user->alumni_social}}                                                       </a>
                                                                                                        </td>
                                                </tr>
                                                <tr style="color:#262626">
                                                    <td class="course-title">Location</td>
                                                    <td>
                                                        @if($user->location)
                                                       {{$user->location}}                                                   
                                                       @else
                                                       -
                                                       @endif
                                                   </td>
                                                </tr>
                                                <tr style="color:#262626">
                                                    <td class="course-title">Profession</td>
                                                    <td>
                                                        {{$user->alumni_profession}}                                                    </td>
                                                </tr>
                                                <tr style="color:#262626">
                                                    <td class="course-title">Blood Group</td>
                                                    <td>   @if($user->location)
                                                       {{$user->blood_group}}                                                   
                                                       @else
                                                       -
                                                       @endif</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </article>
                                                </div><!-- /.author -->
                    </section>
                </div><!-- /#page-main -->
            </div><!-- /.col-md-8 -->

@endsection

@section('footer_scripts')


@stop

