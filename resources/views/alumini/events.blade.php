@extends('layouts.alumini.layout')

@section('header_scripts')


@stop


@section('breadcrumb')

<div class="container">

    <ol class="breadcrumb">
       
        <li class="active">Events</li>
    </ol>
    
</div>



@endsection



@section('content')


            <div class="col-md-8">
                <div id="page-main">
                    <section class="events images" id="events">
                        <header><h1>Events</h1></header>
                        <div class="section-content">

                            @foreach($events as $event)

                            <article class="event">
                               <div class="col-md-3 col-sm-3">
                                <figure class="event-image">
                                    <div class="image-wrapper">
                                        <img src="{{URL_ALUMNI_EVENTS_IMAGE.$event->image}}">
                                    </div>
                                </figure>
                            </div>
                                <aside>
                                    <header>
                                        <a href="{{URL_EVENT_INFO.$event->id}}">
                                            {{ucwords($event->title)}}                                        </a>
                                    </header>
                                    <div class="additional-info">
                                        <span class="fa fa-map-marker"></span>  {{$event->eaddress}} <b>{{$event->date}}</b> </div>
                                    <div class="description">
                                        <p>{!!$event->short_description!!}</p>
                                    </div>
                                    <a href="{{URL_EVENT_INFO.$event->id}}" class="btn btn-framed btn-color-grey btn-small">View Details</a>
                                </aside>
                            </article>


                            @endforeach()

                       
                    </div><!-- /.section-content -->
                    </section><!-- /.events-images -->
                    <div class="row text-center">
                        <div class="col-sm-12">
                            <ul>
                                {{ $events->links() }}
                            </ul>
                        </div>
                    </div>
                </div><!-- /#page-main -->
            </div><!-- /.col-md-8 -->

     

    

@endsection

@section('footer_scripts')


@stop

