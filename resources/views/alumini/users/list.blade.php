@extends($layout)
@section('header_scripts')
<link href="{{CSS}}ajax-datatables.css" rel="stylesheet">
@stop
@section('content')


<div id="page-wrapper">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
	    <div class="container-fluid content-wrap">
			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>
						<li>{{ $title }}</li>
					</ol>
				</div>
			</div>
							
			<!-- /.row -->
			<section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item " >
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                        <h2 class="pull-left"> {{ $title }} </h2>   
                        <div class="pull-right messages-buttons">
							<a href="{{URL_ALUMNI_USERS_ADD}}" class="btn  btn-primary button panel-header-button" >{{ getPhrase('create')}}</a>
						</div>                  
                        <!--End panel icons-->
                    </div>
                    <div class="panel-body panel-body-p packages">
						<div> 
						<table class="table table-striped table-bordered datatable" cellspacing="0" width="100%">
							<thead>
								<tr>
									
									<th>{{ getPhrase('image')}}</th>
									<th>{{ getPhrase('name')}}</th>
									<th>{{ getPhrase('passed_out_year')}}</th>
									<th>{{ getPhrase('phone')}}</th>
									<th>{{ getPhrase('email')}}</th>
									<th>{{ getPhrase('status')}}</th>
								    <th>{{ getPhrase('action')}}</th>
								  
								</tr>
							</thead>
							 
						</table>
						</div>

					</div>
				</div>
			</section>
		</div>
		<!-- /.container-fluid -->
	</section>


			<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
      	 <h4 class="modal-title">{{getPhrase('alumni_status')}}</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
       
      </div>
      <div class="modal-body">
      {!!Form::open(array('url'=> URL_ALUMNI_STATUS_CHANGE,'method'=>'POST','name'=>'userstatus'))!!} 

      <span id="message"></span>

        <input type="hidden" name="current_status" id="current_status" >
        <input type="hidden" name="user_id" id="user_id" >

        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        <button type="submit" class="btn btn-primary" >Yes</button>
      </div>
      {!!Form::close()!!}
    </div>

  </div>
</div>

		</div>
@endsection
 

@section('footer_scripts')
  
 @include('common.datatables', array('route'=>URL_ALUMNI_USERS_GETLIST, 'route_as_url' => TRUE ))
 @include('common.deletescript', array('route'=>URL_ALUMNI_USERS_DELETE ))


  <script >
 	 
 		function changeStatus(user_id, status)
 		{
 			$('#current_status').val(status);
 			$('#user_id').val(user_id);
 			message = '{{ getPhrase('are_you_sure_to_make_alumni_active')}}?'; 
 			if(status==1)
 			message = '{{ getPhrase('are_you_sure_to_make_alumni_inactive')}}?'; 
 			$('#message').html(message);

 			$('#myModal').modal('show');
 		}

 		function showMessage(){
           $('#myUserModal').modal('show');
 		}
  
 </script>

@stop
