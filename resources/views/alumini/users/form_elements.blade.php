 				
     <div class="row">
               
          <fieldset class="form-group col-md-6">

            {{ Form::label('name', getphrase('name')) }}

            <span class="text-red">*</span>

            {{ Form::text('name', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('name'),

              'ng-model'=>'name', 


              'required'=> 'true', 

              'ng-class'=>'{"has-error": formLocations.name.$touched && formLocations.name.$invalid}',

              )) }}

            <div class="validation-error" ng-messages="formLocations.name.$error" >

                {!! getValidationMessage()!!}


            </div>

          </fieldset> 

            <fieldset class="form-group col-md-6">



            {{ Form::label('class', getphrase('passed_out_year')) }}

            <span class="text-red">*</span>

            {{Form::select('alumni_class', $years, null, ['placeholder' => getPhrase('select_year'),'class'=>'form-control', 

              'ng-model'=>'alumni_class',
              'id'=>'alumni_class',

              'required'=> 'true', 

              'ng-class'=>'{"has-error": formLocations.alumni_class.$touched && formLocations.alumni_class.$invalid}'

             ])}}

              <div class="validation-error" ng-messages="formLocations.alumni_class.$error" >

                {!! getValidationMessage()!!}

                 

            </div>

              

          </fieldset>


           <fieldset class="form-group col-md-6">


            {{ Form::label('email', getphrase('email')) }}

            <span class="text-red">*</span>

            {{ Form::email('email', $value = null, $attributes = array('class'=>'form-control', 'placeholder' => 'jack@jarvis.com',

              'ng-model'=>'email',

              'required'=> 'true', 

              'ng-class'=>'{"has-error": formLocations.email.$touched && formLocations.email.$invalid}',

             )) }}

             <div class="validation-error" ng-messages="formLocations.email.$error" >

                {!! getValidationMessage()!!}

                {!! getValidationMessage('email')!!}

            </div>

          </fieldset>



          <fieldset class="form-group col-md-6">

            {{ Form::label('password', getphrase('password')) }}
              
              @if(!$record)
             <span class="text-red">*</span>
             @endif

           <input type="password" name="password" class="form-control">

          </fieldset>

          <fieldset class="form-group col-md-6">

            {{ Form::label('phone', getphrase('phone')) }}

            <span class="text-red">*</span>

            {{ Form::number('phone', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => 
            getPhrase('please_enter_10-15_digit_mobile_number'),

              'ng-model'=>'phone',

              'required'=> 'true', 
              

              'ng-class'=>'{"has-error": formLocations.phone.$touched && formLocations.phone.$invalid}',


            )) }}

             

            <div class="validation-error" ng-messages="formLocations.phone.$error" >

                {!! getValidationMessage()!!}

             

            </div>

          </fieldset>

            <fieldset class="form-group col-md-6">



            {{ Form::label('profession', getphrase('profession')) }}

            <span class="text-red">*</span>

            {{Form::select('alumni_profession', $profession, null, ['placeholder' => getPhrase('select_role'),'class'=>'form-control', 

              'ng-model'=>'alumni_profession',
              'id'=>'alumni_profession',

              'required'=> 'true', 

              'ng-class'=>'{"has-error": formLocations.alumni_profession.$touched && formLocations.alumni_profession.$invalid}'

             ])}}

              <div class="validation-error" ng-messages="formLocations.alumni_profession.$error" >

                {!! getValidationMessage()!!}

                 

            </div>

              

          </fieldset>


            <fieldset class="form-group col-md-6">

            {{ Form::label('alumni_social', getphrase('social_link')) }}

            <span class="text-red">*</span>

            {{ Form::text('alumni_social', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('alumni_social'),

              'ng-model'=>'alumni_social', 


              'required'=> 'true', 

              'ng-class'=>'{"has-error": formLocations.alumni_social.$touched && formLocations.alumni_social.$invalid}',

              )) }}

            <div class="validation-error" ng-messages="formLocations.alumni_social.$error" >

                {!! getValidationMessage()!!}


            </div>

          </fieldset> 

        
        <fieldset class="form-group col-md-3" >
           {{ Form::label('image', getphrase('image')) }}
                 <input type="file" class="form-control" name="image" 
                 accept=".png,.jpg,.jpeg" id="image_input">
                  
                  
            </fieldset>

          
            @if($record && $record->image)

              <fieldset class="form-group col-md-3">
                
                  <img src="{{getProfilePath($record->image,'profile')}}" class="img img-responsive" height="100px" width="100px"><p></p>
               

          

              @elseif( $record && !$record->image )

               <img src="{{IMAGE_PATH_UPLOAD_ASSETS_DEFAULT}}" height="80px" width="80px"><p></p>

                  </fieldset>
              @endif


                  
                 

					</div>

					<input type="hidden" name="added_by" value="{{ Auth::user()->id }}">


						<div class="buttons text-center">

							<button class="btn btn-lg btn-primary button"

							ng-disabled='!formLocations.$valid'>{{ $button_name }}</button>

						</div>

		 