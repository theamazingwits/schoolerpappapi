@extends('layouts.alumini.layout')

@section('header_scripts')


@stop


@section('breadcrumb')

<div class="container">
    <ol class="breadcrumb">
       
        <li class="active">Edit Alumnus Details</li>
    </ol>
</div>

@endsection


@section('content')
@include('errors.errors')   
<div id="page-content">
    <div class="container">
        <div class="row">

 <!--MAIN Content-->
            <div id="page-main">
                <div class="col-md-10 col-sm-10 col-sm-offset-1 col-md-offset-1">
                    <div class="row">
                                            <div class="col-md-12">
                            <section class="account-block">
                                <header><h2>Update Your Alumni Account</h2></header>

                             {!! Form::open(array('url' => URL_UPDATE_ALUMNI_PROFILE, 'method' => 'POST', 'files' => true, 'novalidate'=>'','name'=>'formCategories')) !!}

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input  name="name" type="text" class="form-control" placeholder="Enter full name" data-parsley-required="true" value="{{$user->name}}">
                                        </div>
                                        <div class="form-group">
                                            <label>Passed Out Year</label>
                                             {{ Form::select('class', $years, $user->alumni_class , ['placeholder' => getPhrase('select_year'),'class'=>'selectize-control single selectize-input', 
                            'id'=>'class',

                            'required'=> 'true', 

                         

                         ]) }}
                                        </div>
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input  value="{{$user->email}}" name="email" type="email" class="form-control" placeholder="Enter email" data-parsley-required="true">
                                        </div>
                                        <div class="form-group">
                                            <label>Mobile Number</label>
                                            <input value="{{$user->phone}}" name="mobile_number" type="text" class="form-control" placeholder="Enter mobile number" data-parsley-required="true">
                                        </div>
                                        <div class="form-group">
                                            <label>Location</label>
                             {{ Form::select('location', $countries, $user->location , ['placeholder' => getPhrase('select_location'),'class'=>'selectize-control single selectize-input', 
                            'id'=>'location',

                            'required'=> 'true', 

                         

                         ]) }}
                                        </div>
                                        <div class="form-group">
                                            <label>Password</label>
                                            <input value="" name="password" type="password" class="form-control" placeholder="Enter password" data-parsley-required="true">
                                        </div>
                                        <div class="form-group">
                                            <label>Website</label>
                                            <input value="{{$user->alumni_social}}" name="website" type="text" class="form-control" placeholder="Enter personal website url">
                                        </div>
                                        <div class="form-group">
                                            <label>Short Biography</label>
                                            <textarea placeholder="Enter short biography" name="biography" maxlength="181" rows="3" style="resize:none" data-parsley-required="true">{{$user->quote}}</textarea>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Username</label>
                                            <input value="{{$user->username}}"  name="username" type="text" class="form-control" placeholder="Enter username" data-parsley-required="true">
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>Profession</label>
                                                {{ Form::select('alumni_profession', $profession, $user->alumni_profession , ['placeholder' => getPhrase('select_profession'),'class'=>'selectize-control single selectize-input', 
                            'id'=>'alumni_profession',

                            'required'=> 'true', 

                         

                         ]) }}
                                        </div>

                                        <div class="form-group">
                                            <label>Date of Birth</label>
                                            <div class="input-group">
                                                <input type="text" name="date_of_birth" class="form-control" value="{{$user->date_of_birth}}" placeholder="yyyy-mm-dd">
                                                <div class="input-group-addon">
                                                    <span class="glyphicon glyphicon-th"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Position</label>
                                            <input value="{{$user->alumni_profession}}" name="alumni_profession" type="text" class="form-control" placeholder="Enter current position" data-parsley-required="true">
                                        </div>
                                        <div class="form-group">
                                            <label>Social Profile Link</label>
                                            <input value="{{$user->alumni_social}}" name="alumni_social" type="text" class="form-control" placeholder="Enter profile url" data-parsley-required="true">
                                        </div>
                                        <div class="form-group">
                                            <label>Blood Group</label>
                                           {{ Form::select('blood_group', $groups, $user->blood_group , ['placeholder' => getPhrase('select_blood_group'),'class'=>'selectize-control single selectize-input', 
                            'id'=>'blood_group',

                            'required'=> 'true', 

                         

                         ]) }}
                                        </div>

                                         <div class="form-group">

                                             <label>Image</label>

                                             {!! Form::file('image', array('id'=>'image_input', 'accept'=>'.png,.jpg,.jpeg')) !!}


                                         </div>
                                     
                                    </div>

                                    <input type="hidden" name="user_id" value="{{$user->user_id}}">
                                    
                                    <div align="right">
                                        <button type="submit" class="btn">Update</button>
                                    </div>
                                {!! Form::close() !!}     
                                                     </section>
                        </div><!-- /.col-md-6 -->
                                        </div><!-- /.row -->
                </div><!-- /.col-md-10 -->
            </div><!-- /#page-main -->
            <!-- end SIDEBAR Content-->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div>
<!-- end Page Content -->

   

@endsection

@section('footer_scripts')




@stop

