@extends('layouts.alumini.layout')

@section('header_scripts')


@stop


@section('breadcrumb')

<div class="container">
    <ol class="breadcrumb">
       
        <li><a href={{URL_ALUMNI_USER_STORIES}}>Stories</a></li>
        <li class="active">{{ucwords($story->title)}}</li>
    </ol>
</div>

@include('errors.errors')



@endsection



@section('content')



<!--MAIN Content-->
            <div class="col-md-8">
                <div id="page-main" style="margin-bottom: 100px">
                    <section id="blog-detail">
                                            <header><h1>Story</h1></header>
                        <article class="blog-detail">
                            <header class="blog-detail-header">
                                <img src="{{URL_ALUMNI_STORIES_IMAGE.$story->image}}">
                                <h2>{{ucwords($story->title)}}</h2>
                                <div class="blog-detail-meta">
                                    <span class="date">
                                        <span class="fa fa-file-o"></span>
                                        {{$story->date}}                       
                                                     </span>
                                    <span class="author">
                                        <span class="fa fa-user"></span>
                                        {{ucwords($user->name)}}                                    </span>
                                    <span class="comments">
                                        <span class="fa fa-comment-o"></span>
                                        {{count($comments)}} comments
                                    </span>
                                </div>
                            </header>
                            <hr>
                            <p>{!! $story->description  !!}</p>
                        </article>
                                        </section><!-- /.blog-detail -->
                    <hr>

                      @if(count($comments) > 0 )
                    <section id="discussion">
                        <header><h2>Comments</h2></header>
                        <ul class="discussion-list">

                    
                        
                        @foreach($comments as $comment)

                          <?php
                            $cuser  = getUserRecord($comment->user_id);
                          ?>

                        <li class="author-block">
                                <figure class="author-picture">
                            <img src="{{getProfilePath($cuser->image,'profile')}}">
                        </figure>
                                <article class="paragraph-wrapper">
                                    <div class="inner">
                                        <header>
                                            <h5>{{ucwords($cuser->name)}}</h5>
                                        </header>
                                        <p>{{$comment->comment}}</p>
                                    </div>
                                    <div class="comment-controls">
                                        <span>{{$comment->updated_at}}</span>
                                    </div>
                                </article>
                            </li>

                            @endforeach

                            
                        </ul><!-- /.discussion-list -->
                    </section><!-- /.discussion -->
                        @endif

        <section id="leave-reply">
                        <header><h2>Leave a comment</h2></header>

            {!! Form::open(array('url' => URL_STORY_COMMENT_SEND, 'method' => 'POST', 'name'=>'formLocations ', 'novalidate'=>'','class'=>'reply-form' )) !!}

            <input type="hidden" name="story_id" value="{{$story->id}}">
            <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
    
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <div class="controls">
                                            <label for="comment">Your comment</label>
                                            <textarea maxlength="333" rows="4" style="resize: none" name="comment" id="comment" data-parsley-required="true"></textarea>
                                        </div><!-- /.controls -->
                                    </div><!-- /.control-group -->
                                </div><!-- /.col-md-4 -->
                            </div><!-- /.row -->
                            <div class="form-actions pull-right">
                                <input type="submit" class="btn btn-color-primary" value="Submit">
                            </div><!-- /.form-actions -->
                       {!! Form::close() !!}         
                    </section>
                    
                                    </div><!-- /#page-main -->
            </div><!-- /.col-md-8 -->

           

     

    

@endsection

@section('footer_scripts')


@stop

