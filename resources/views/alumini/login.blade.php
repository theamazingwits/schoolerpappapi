@extends('layouts.alumini.layout')

@section('header_scripts')


@stop


@section('breadcrumb')

<div class="container">
    <ol class="breadcrumb">
        <li class="active">Login or Register</li>
    </ol>
</div>


@endsection

@section('content')
  


  <!-- Page Content -->
<div id="page-content">
@include('errors.errors')   
    <div class="container">
        <div class="row">
            <!--MAIN Content-->
            <div id="page-main">
                <div class="col-md-10 col-sm-10 col-sm-offset-1 col-md-offset-1">
                    <div class="row">
                        <div class="col-md-6">
                            <section id="account-sign-in" class="account-block">
                                <header><h2>Have an Account?</h2></header>
                                                                                                                                
                                {!! Form::open(array('url' => URL_USERS_LOGIN, 'method' => 'POST', 'name'=>'formLanguage ', 'novalidate'=>'', 'class'=>"loginform", 'name'=>"loginForm")) !!}

<input type="hidden" name="csrf_test_name" value="5a73b59767b273b72560ca8d6d80e00a" />
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input  name="email" type="email" class="form-control" id="email" placeholder="Enter email" data-parsley-required="true">
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input name="password" type="password" class="form-control" id="password" placeholder="Enter password" data-parsley-required="true">
                                    </div>
                                  
                                    <button type="submit" class="btn pull-right">Sign In</button>

                               {!! Form::close() !!}
                                    {{-- <button type="submit" class="btn pull-left">Forgot Password</button> --}}

                                <button class="btn pull-left" data-toggle="modal" data-target="#myModal" > {{getPhrase('forgot_password')}}</button>



                         </section><!-- /#account-block -->
                        </div><!-- /.col-md-6 -->
                        <div class="col-md-6">
                            <section class="account-block">
                                <header><h2>Create New Alumni Account</h2></header>

               {!! Form::open(array('url' => URL_ALUMNI_REGISTER, 'method' => 'POST',  'novalidate'=>'','name'=>'formAlumniResgister')) !!}

<input type="hidden" name="csrf_test_name" value="5a73b59767b273b72560ca8d6d80e00a" />
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input name="name" type="text" class="form-control" placeholder="Enter full name" data-parsley-required="true">
                                    </div>
                                     <div class="form-group">
                                            <label>Passed Out Year</label>
                                      {{ Form::select('class', $years, null , ['placeholder' => getPhrase('select_year'),'class'=>'selectize-control single selectize-input ',  
                                            'id'=>'class',

                                            'required'=> 'true', 

                                         ]) }}

                                        </div>
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input name="email" type="email" class="form-control" placeholder="Enter email" data-parsley-required="true">
                                    </div>

                                    <div class="form-group">
                                        <label>Password</label>
                                        <input name="password" type="password" class="form-control" placeholder="Enter password" data-parsley-required="true">
                                    </div>

                                    <div class="form-group">
                                        <label>Mobile Number</label>
                                        <input name="mobile_number" type="text" class="form-control" placeholder="Enter mobile number" data-parsley-required="true">
                                    </div>
                                    <div class="form-group">
                                            <label>Profession</label>

                                      {{ Form::select('profession', $profession, null , ['placeholder' => getPhrase('select_profession'),'class'=>'selectize-control single selectize-input', 
                                            'id'=>'profession',

                                            'required'=> 'true', 

                                         ]) }}

                                        </div>
                                    <div class="form-group">
                                        <label>Social Profile Link</label>
                                        <input name="social_link" type="text" class="form-control" placeholder="Enter linkedin profile url" data-parsley-required="true">
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="terms" data-parsley-required="true">I understand terms &amp; Conditions                                        </label>
                                    </div>
                                    <button style="margin: 6% 0 10% 0" type="submit" class="btn pull-right">Create New Account</button>
                                    {!! Form::close() !!}
                                                            </section><!-- /#account-block -->
                        </div><!-- /.col-md-6 -->
                    </div><!-- /.row -->
                </div><!-- /.col-md-10 -->
            </div><!-- /#page-main -->
            <!-- end SIDEBAR Content-->
        </div><!-- /.row -->
    </div><!-- /.container -->



        <!-- Modal -->

<div id="myModal" class="modal fade" role="dialog">

  <div class="modal-dialog">

    {!! Form::open(array('url' => URL_FORGOT_PASSWORD, 'method' => 'POST', 'name'=>'formLanguage ', 'novalidate'=>'', 'class'=>"loginform", 'name'=>"passwordForm")) !!}

    <!-- Modal content-->

    <div class="modal-content">

      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal">&times;</button>

        <h4 class="modal-title">{{getPhrase('forgot_password')}}</h4>

      </div>

      <div class="modal-body">

        <div class="input-group">

                <span class="input-group-addon" id="basic-addon1"><i class="icon icon-email-resend"></i></span>



                {{ Form::email('email', $value = null , $attributes = array('class'=>'form-control',

         

            'required'=> 'true',

            'placeholder' => getPhrase('email'),

         

        )) }}



            </div>

      </div>

      <div class="modal-footer">

      <div class="pull-right">

        <button type="button" class="btn btn-default" data-dismiss="modal">{{getPhrase('close')}}</button>

        <button type="submit" class="btn btn-primary" ng-disabled='!passwordForm.$valid'>{{getPhrase('submit')}}</button>

        </div>

      </div>

    </div>

    {!! Form::close() !!}

  </div>

</div>

</div>
<!-- end Page Content -->


@endsection

@section('footer_scripts')


@stop

