@extends('layouts.alumini.layout')

@section('header_scripts')


@stop


@section('breadcrumb')

<div class="container">

    <ol class="breadcrumb">
       
        <li class="active">Donations</li>
    </ol>
    
</div>



@endsection



@section('content')


            <div class="col-md-8">
                <div id="page-main">
                    <section class="events images" id="events">
                        <header><h1>Donations</h1></header>
                        <div class="section-content">

                            @foreach($donations as $donation)

                            <article class="event">
                               <div class="col-md-3 col-sm-3">
                                <figure class="event-image">
                                    <div class="image-wrapper">
                                        <img src="{{URL_ALUMNI_DONATIONS_IMAGE.$donation->image}}">
                                    </div>
                                </figure>
                            </div>
                                <aside>
                                    <header>
                                        <a href="javascript:void(0)">
                                            {{ucwords($donation->title)}} -  {{ getCurrencyCode() }} {{$donation->amount}}                                       </a>
                                    </header>
                                    <div class="additional-info">
                                    <span class="fa fa-map-marker"></span>   <b>{{$donation->date}}</b> &nbsp;&nbsp;&nbsp;
                                    {{-- <span class="fa fa-money"></span> <b>Donation Amount</b> - <b>{{ getCurrencyCode() }} {{$donation->amount}}</b>  --}}
                                    </div>

                                    <div class="description">
                                        <p>{!!$donation->description!!}</p>
                                    </div>

                                    @if(Auth::check())

                                    <a href="{{URL_DONATE_AMOUNT.$donation->id}}" class="btn btn-framed btn-color-grey btn-small">Click to Donate {{ getCurrencyCode() }} {{$donation->amount}}</a>

                                    @else
                                    <a href="{{URL_ALUMINI_LOGIN}}" class="btn btn-framed btn-color-grey btn-small">Click to Donate {{ getCurrencyCode() }} {{$donation->amount}}</a>

                                    @endif

                                </aside>
                            </article>


                            @endforeach()

                       
                    </div><!-- /.section-content -->
                    </section><!-- /.events-images -->
                    <div class="row text-center">
                        <div class="col-sm-12">
                            <ul>
                                {{ $donations->links() }}
                            </ul>
                        </div>
                    </div>
                </div><!-- /#page-main -->
            </div><!-- /.col-md-8 -->

     

    

@endsection

@section('footer_scripts')


@stop

