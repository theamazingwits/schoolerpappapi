@extends($layout)
@section('header_scripts')
<link href="{{CSS}}ajax-datatables.css" rel="stylesheet">
@stop
@section('content')


<div id="page-wrapper">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
	    <div class="container-fluid content-wrap">
			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>
						<li><a href="{{URL_ALUMNI_GALLERY}}">{{ getPhrase('gallery')}}</a></li>
						<li>{{ $title }}</li>
					</ol>
				</div>
			</div>
							
			<!-- /.row -->
			<section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item " >
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                        <h2 class="pull-left"> {{ $title }} </h2>   
                        <!--End panel icons-->
                    </div>
                    <div class="panel-body panel-body-p packages">
						<div> 
						<table class="table table-striped table-bordered datatable" cellspacing="0" width="100%">
							<thead>
								<tr>
									
									<th>{{ getPhrase('image')}}</th>
								    <th>{{ getPhrase('action')}}</th>
								  
								</tr>
							</thead>
							<tbody>
								@if(count($sub_records) > 0 )

								 @foreach($sub_records as $myrecord)

								<tr>
									@if($myrecord->image)
									<td><img src ="{{URL_ALUMNI_GALLERY_IMAGE.$myrecord->image}}" height="100px" width="100px"></td>
									@else
									 <td><img src="{{IMAGE_PATH_UPLOAD_ASSETS_DEFAULT}}" height="80px" width="80px"></td>
									@endif
									<td><a href="{{URL_DELETE_GALLERY_IMAGE.$myrecord->id}}" class="btn btn-primary btn-sm">Delete</a></td>
								</tr>

								 @endforeach

								@else

								<tr>
									<td></td>
									<td>No Images are avaialble <a href="{{URL_ALUMNI_GALLERY_EDIT.$record->id}}" class="btn btn-primary btn-sm">Click to add images</a></td>

								</tr>

								@endif

							</tbody>
							 
						</table>
						</div>

					</div>
				</div>
			</section>
		</div>
		<!-- /.container-fluid -->
	</section>
</div>
@endsection
 

@section('footer_scripts')
  
 
@stop
