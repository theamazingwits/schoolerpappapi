 				
                 <div class="row">
               
          <fieldset class="form-group col-md-12">

            {{ Form::label('title', getphrase('title')) }}

            <span class="text-red">*</span>

            {{ Form::text('title', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('title'),

              'ng-model'=>'title', 


              'required'=> 'true', 

              'ng-class'=>'{"has-error": formLocations.title.$touched && formLocations.title.$invalid}',

              )) }}

            <div class="validation-error" ng-messages="formLocations.title.$error" >

                {!! getValidationMessage()!!}


            </div>

          </fieldset> 


                 




                      <fieldset class="form-group col-md-12">
                                     
                        {{ Form::label('date', getphrase('date')) }}
                        <span class="text-red">*</span>

                        <div class="input-group date" data-date="{{date('Y-m-d')}}" data-provide="datepicker" data-date-format="yyyy-mm-dd">

                        {{ Form::text('date', $value = null , $attributes = array(

                          'class'       =>'form-control',
                          'placeholder' => '2015-07-17', 
                          'id'          =>'dp',
                      
                         

                           )) }}

                      
                            <div class="input-group-addon">

                                <span class="mdi mdi-calendar"></span>

                            </div>

                        </div>

              </fieldset>


              

        <fieldset class="form-group col-md-12" >

  <table class="table">
    <thead>
      <th><b>Image</b> (jpg, jpeg, png)</th>
      <th><b>Select to remove</b></th>

    </thead>
    <tbody>
      <tr ng-repeat="routeData in routeDetails">

  
       <td>
         <input type="file" class="form-control" name="certificate_files[]" ng-model="routeData.cost" accept=".png,.jpg,.jpeg,.PNG,.JPG,.JPEG" />
     </td>



   
      
      <td>
        <input type="checkbox" name="options" id="free" ng-model="routeData.selected" style="display: block;" /></td>
        
      </tr>

      <tr ng-if="routeDetails.length <= 0">
        <td><h5>Please upload images</h5></td>
       
      </tr>
    </tbody>



    
  </table>

      <div class="form-group">
         

         <input type="button" class="btn btn-primary addnew pull-right" ng-click="addNewRoute()" value="Add Image">
           <input ng-hide="!routeDetails.length" type="button" class="btn btn-danger pull-right" ng-click="removeRoute()" value="Remove" style="margin-right: 5px;">
     </div>

</fieldset>

         
                  
                 

					</div>

					<input type="hidden" name="added_by" value="{{ Auth::user()->id }}">


						<div class="buttons text-center">

							<button class="btn btn-lg btn-primary button"

							ng-disabled='!formLocations.$valid'>{{ $button_name }}</button>

						</div>

		 