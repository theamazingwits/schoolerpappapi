@extends('layouts.alumini.layout')

@section('header_scripts')


@stop


@section('breadcrumb')

<div class="container">
    <ol class="breadcrumb">
       
        <li class="active">Stories</li>
    </ol>
</div>



@endsection



@section('content')


<!--MAIN Content-->
            <div class="col-md-8">
                <div id="page-main">
                    <section class="blog-listing" id="blog-listing">
                        <header><h1>Stories</h1></header>
                        <div class="row">

                            @if(count($stories) > 0)

                              @foreach($stories as $story)

                            <div class="col-md-6 col-sm-6">
                                <article class="blog-listing-post">
                                    <figure class="blog-thumbnail">
                                        <figure class="blog-meta">
                                            <span class="fa fa-clock-o"></span>
                                            {{$story->date}}                                      
                                             </figure>
                                        <div class="image-wrapper">

                                            @if(Auth::check())
                                            <a href="{{URL_STORY_INFO.$story->id}}">
                                            @else
                                            <a href="{{URL_ALUMINI_LOGIN}}">
                                            @endif 
                                                <img src="{{URL_ALUMNI_STORIES_IMAGE.$story->image}}">
                                            </a>
                                        </div>
                                    </figure>
                                    <aside>
                                        <header>
                                          @if(Auth::check())
                                            <a href="{{URL_STORY_INFO.$story->id}}">
                                            @else
                                            <a href="{{URL_ALUMINI_LOGIN}}">
                                            @endif 
                                        </header>
                                        <div class="description">
                                            <p>{!! $story->description  !!}</p>
                                        </div>

                                        @if(Auth::check())
                                             <a href="{{URL_STORY_INFO.$story->id}}" class="read-more stick-to-bottom">Read More</a>
                                            @else
                                            <a href="{{URL_ALUMINI_LOGIN}}">
                                            @endif 
                                       
                                    </aside>
                                </article><!-- /article -->
                            </div><!-- /.col-md-6 -->

                            @endforeach

                          @endif  

                          
                        </div><!-- /.row -->
                    </section><!-- /.blog-listing -->
                  <div class="row text-center">
                        <div class="col-sm-12">
                            <ul>
                                {{ $stories->links() }}
                            </ul>
                        </div>
                    </div>
                </div><!-- /#page-main -->
            </div><!-- /.col-md-8 -->


           

     

    

@endsection

@section('footer_scripts')


@stop

