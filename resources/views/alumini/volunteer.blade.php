@extends('layouts.alumini.layout')

@section('header_scripts')


@stop


@section('breadcrumb')

<div class="container">
    <ol class="breadcrumb">
       
        <li class="active">Volunteers</li>
    </ol>
</div>



@endsection



@section('content')

    <!--MAIN Content-->
            <div class="col-md-12">
                <div id="page-main">
                    <section class="course-listing" id="courses">
                        <header><h1>Volunteers</h1></header>
                                                <section id="course-list">
                            <div class="table-responsive">
                                <table class="table table-hover course-list-table">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Batch</th>
                                            <th>Profession</th>
                                            <th>Event</th>
                                            <th>Date</th>
                                            <th>Address</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                      @if(count($volunteers)  > 0)
                                        
                                         @foreach($volunteers as $volunteer)  
                                        <tr>
                                            <th>{{ucwords($volunteer->name)}} </th>
                                            <th>{{$volunteer->email}} </th>
                                            <th>{{$volunteer->alumni_class}} </th>
                                            <th>{{$volunteer->alumni_profession}} </th>
                                            <th><a href="{{URL_EVENT_INFO.$volunteer->event_id}}">{{ucwords($volunteer->title)}} </a></th>
                                            <th>{{$volunteer->date}} </th>
                                            <th>{{$volunteer->eaddress}} </th>
                                            
                                        </tr>

                                         @endforeach

                                       @endif  
                                                  
                                    </tbody>
                                </table>
                            </div>
                        </section>
                    </section><!-- /.course-listing -->
                     <div class="row text-center">
                        <div class="col-sm-12">
                            <ul>
                                {{ $volunteers->links() }}
                            </ul>
                        </div>
                    </div>
                </div><!-- /#page-main -->
            </div><!-- /.col-md-8 -->



@endsection

@section('footer_scripts')


@stop

