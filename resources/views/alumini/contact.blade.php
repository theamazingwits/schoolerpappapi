@extends('layouts.alumini.layout')

@section('header_scripts')


@stop


@section('breadcrumb')

<div class="container">
    <ol class="breadcrumb">
       
        <li class="active">Contact Us</li>
    </ol>
</div>



@endsection



@section('content')

@include('errors.errors')

  <!--MAIN Content-->
            <div class="col-md-8">
                <div id="page-main">
                    <section id="contact">
                                            <header><h1>Contact Us</h1></header>
                        <div class="row">
                            <div class="col-md-6">
                                <address>
                                    <h3>{{ getSetting('site_title', 'site_settings') }}</h3>
                                    <br>
                                    <span>{{ getSetting('site_address', 'site_settings') }}</span>
                                    <br>
                                   
                                    <abbr title="Telephone">Telephone:</abbr> {{ getSetting('site_phone', 'site_settings') }} <br>
                                    <abbr title="Email">Email:</abbr> <a href="mailto:{{$user->email}}">{{$user->email}}</a>
                                </address>
                              
                                <hr>
                                <p>As for any undergraduate, the first year passed in a blur of textbooks, lectures and nights out. The atmosphere at Cambridge was like nothing I’d ever experienced before: everyone seemed 
                            </div>
                          
                        </div>
                                        </section>
                    <section id="contact-form" class="clearfix">
                        <header><h2>Send Us a Message</h2></header>
    
       {!! Form::open(array('url'=>URL_SEND_CONTACTUS, 'name'=>'user-contact' ,'id'=>'user-contact','class'=>'contact-form'))  !!}


                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="controls">
                                            <label for="name">Your Name</label>
                                            <input type="text" name="name" id="name" data-parsley-required="true">
                                        </div><!-- /.controls -->
                                    </div><!-- /.control-group -->
                                </div><!-- /.col-md-4 -->
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="controls">
                                            <label for="email">Your Email</label>
                                            <input type="email" name="email" id="email" data-parsley-required="true">
                                        </div><!-- /.controls -->
                                    </div><!-- /.control-group -->
                                </div><!-- /.col-md-4 -->
                            </div><!-- /.row -->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <div class="controls">
                                            <label for="message">Your Message</label>
                                            <textarea name="message" id="message" style="resize: none" maxlength="666" data-parsley-required="true"></textarea>
                                        </div><!-- /.controls -->
                                    </div><!-- /.control-group -->
                                </div><!-- /.col-md-4 -->
                            </div><!-- /.row -->
                            <div class="pull-right">
                                <input type="submit" class="btn btn-color-primary" id="submit" value="Send Us a Message">
                            </div><!-- /.form-actions -->
                            <br>
                            <br>
                    {!! Form::close() !!}
                         </section>
                </div><!-- /#page-main -->
            </div><!-- /.col-md-8 -->


@endsection

@section('footer_scripts')


@stop

