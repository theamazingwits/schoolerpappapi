@extends('layouts.alumini.layout')

@section('header_scripts')


@stop


@section('breadcrumb')

<div class="container">
    <ol class="breadcrumb">
       
        <li><a href="{{URL_ALUMNI_USER_EVENTS}}">Events</a></li>
        <li class="active">{{ucwords($event_data->title)}}</li>
    </ol>
</div>



@endsection



@section('content')


          
        <header><h1>{{ucwords($event_data->title)}}</h1></header>
        <div class="row">
            <!-- Course Image -->
            <div class="col-md-2 col-sm-3">
                <figure class="event-image">
                    <div class="image-wrapper">
                        <img src="{{URL_ALUMNI_EVENTS_IMAGE.$event_data->image}}">
                    </div>
                </figure>
            </div><!-- end Course Image -->
            <!--MAIN Content-->
            <div class="col-md-6 col-sm-9">
                <div id="page-main">
                    <section id="event-detail">
                        <article class="event-detail">
                            <section id="event-header">
                                <header>
                                    <h2 class="event-date">{{$day}} {{$month}}, {{$year}}</h2>
                                </header>
                                <hr>
                                <div class="course-count-down" id="course-count-down">
                                    <figure class="course-start"></figure>
                                    <!-- /.course-start-->

                                    <div class="count-down-wrapper">
                                        <script type="text/javascript">
                                            var _date = "{{$day}} {{$month}}, {{$year}}";
                                        </script>
                                    </div><!-- /.count-down-wrapper -->

                                </div><!-- /.course-count-down -->
                                <hr>
                                <figure>
                                    <span class="course-summary"><i class="fa fa-map-marker"> {{$event_data->eaddress}} </i> </span>
                                    <span class="course-summary"><i class="fa fa-hashtag"> reunion-{{$month}}-{{$year}}</i></span>
                                    {{-- <span class="course-summary"><i class="fa fa-clock-o"></i>08:00 AM</span> --}}
                                </figure>
                            </section><!-- /#course-header -->
                            
                                                        
                            <section id="course-info">
                                <header><h2>Event Details</h2></header>
                                <p>{!!$event_data->long_description!!}</p>
                            </section>
                            
                                                        
                          
                            
               <section id="course-list">
                                <header><h2>Event Volunteers</h2></header>
                                <div class="table-responsive">
                                    <table class="table table-hover course-list-table">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Batch</th>
                                                <th>Profession</th>
                                                {{-- <th>Link</th> --}}
                                            </tr>
                                        </thead>
                                        <tbody>
                                           <tr>
                                                <th>{{ucwords($user->name)}}</th>
                                                <th>{{$user->email}}</th>
                                                <th>{{$user->alumni_class}}</th>
                                                <th>{{$user->alumni_profession}}</th>
                                                {{-- <th>{{$user->alumni_social}}</th> --}}
                                            </tr>
                                          
                                     </tbody>

                                    </table>
                                </div>
                            </section>
                        </article><!-- /.course-detail -->
                    </section><!-- /.course-detail -->
                </div><!-- /#page-main -->
            </div><!-- /.col-md-8 -->


 @php

             $recent_stories  = App\AlumniStory::orderby('created_at','desc')->limit(3)->get();
             // dd($recent_stories);

          @endphp
              <!--SIDEBAR Content-->
            <div class="col-md-4">
                <div id="page-sidebar" class="sidebar">
                    <aside class="news-small" id="news-small">
                        <header><h2>Most Read Stories</h2></header>
                        <div class="section-content">
                           
                           @if(count($recent_stories) > 0)

                             @foreach($recent_stories as $mystory)

                            <article>
                                <figure class="date">
                                    <i class="fa fa-calendar"></i>
                                    {{$mystory->date}}                               
                                     </figure>
                                <header>
                                   @if(Auth::check())   
                                    <a href="{{URL_STORY_INFO.$mystory->id}}">
                                        {{ucwords($mystory->title)}}                                
                                   </a>
                                 @else

                                   <a href="{{URL_ALUMINI_LOGIN}}">
                                        {{ucwords($mystory->title)}}                                
                                   </a>
                                 
                                 @endif  
                                </header>
                            </article><!-- /article -->

                            @endforeach

                          @endif 
                                                 
                        </div>
                    </aside><!-- /.news-small -->
                </div><!-- /#sidebar -->
            </div><!-- /.col-md-4 -->


        </div><!-- /.row -->

    

@endsection

@section('footer_scripts')


@stop

