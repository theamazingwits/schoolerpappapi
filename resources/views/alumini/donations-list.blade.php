@extends($layout)
@section('header_scripts')
<link href="{{CSS}}ajax-datatables.css" rel="stylesheet">
@stop
@section('content')


<div id="page-wrapper">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
			<div class="container-fluid content-wrap">
				<!-- Page Heading -->
				<div class="row">
					<div class="col-lg-12">
						<ol class="breadcrumb">
							<li><a href="{{PREFIX}}"><i class="bc-home fa fa-home"></i></a> </li>
							<li>{{ $title }}</li>
						</ol>
					</div>
				</div>
								
				<!-- /.row -->
				<section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item">
					<div class="panel bgc-white-dark">
						<div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
							<h2 class="pull-left"> {{ $title }} </h2>
							<div class="pull-right messages-buttons mr-3">
								<a href="{{URL_ALUMNI_DONATIONS}}" class="btn  btn-primary button" >{{ getPhrase('list')}}</a>
							</div>
						 
						</div>
						<div class="panel-body packages">
							<div> 
							<table class="table table-striped table-bordered datatable" cellspacing="0" width="100%">
								<thead>
									<tr>
										
										<th>{{ getPhrase('donation')}}</th>
										<th>{{ getPhrase('user')}} - {{ getPhrase('passed_out_year')}}</th>
									    <th>{{ getPhrase('transaction_id')}}</th>
									    <th>{{ getPhrase('payer_email')}}</th>
										<th>{{ getPhrase('amount')}}</th>
										<th>{{ getPhrase('status')}}</th>
									  
									</tr>
								</thead>
								 
							</table>
							</div>

						</div>
					</div>
				</section>
			</div>
			<!-- /.container-fluid -->
		</section>
		</div>
@endsection
 

@section('footer_scripts')
  
 @include('common.datatables', array('route'=>URL_ALUMNI_USERS_DONATIONS_GETLIST, 'route_as_url' => TRUE ))

@stop
