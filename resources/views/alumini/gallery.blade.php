@extends('layouts.alumini.layout')

@section('header_scripts')


@stop


@section('breadcrumb')

<div class="container">
    <ol class="breadcrumb">
       
        <li class="active">Gallery</li>
    </ol>
</div>



@endsection



@section('content')

<!--MAIN Content-->
            <div class="col-md-12">
                <div id="page-main">
                    <section class="blog-listing">
                        <header><h1>Gallery</h1></header>
                        <div class="row">

                          @if(count($gallery) > 0)  

                            @foreach($gallery as $mygallery)
                                
                            <div class="col-md-12 col-sm-12">
                                <section id="gallery">
                                    <header>
                                        <h2>{{$mygallery->title}}</h2>
                                    </header>
                                    <div class="section-content">
                                        <ul class="gallery-list">

                                         @php
                                           $sub_gallery  = App\AlumniGallery::where('gallery_id',$mygallery->id)->get();
                                         @endphp 

                                        @if(count($sub_gallery) > 0)  

                                             @foreach($sub_gallery as $my_subgallery)

                                           <li>
                                            <a href="{{URL_ALUMNI_GALLERY_IMAGE.$my_subgallery->image}}" class="image-popup" target ="_blank">
                                                    <img src="{{URL_ALUMNI_GALLERY_IMAGE.$my_subgallery->image}}" alt="">
                                                </a>
                                            </li>

                                             @endforeach

                                        @endif  
                                                             
                                                                                    
                                        </ul>
                                    </div>
                                </section>
                            </div>

                            @endforeach

                          @endif  

                        </div><!-- /.row -->

                    </section><!-- /.blog-listing -->
                     <div class="row text-center">
                        <div class="col-sm-12">
                            <ul>
                                {{ $gallery->links() }}
                            </ul>
                        </div>
                    </div>
                </div><!-- /#page-main -->
            </div><!-- /.col-md-8 -->
    

@endsection

@section('footer_scripts')


@stop

