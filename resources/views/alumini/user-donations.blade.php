@extends('layouts.alumini.layout')

@section('header_scripts')


@stop


@section('breadcrumb')

<div class="container">
    <ol class="breadcrumb">
       
        <li class="active">User Donations</li>
    </ol>
</div>



@endsection



@section('content')

    <!--MAIN Content-->
            <div class="col-md-8">
                <div id="page-main">
                    <section class="course-listing" id="courses">
                        <header><h1>User Donations</h1></header>
                                                <section id="course-list">
                            <div class="table-responsive">
                                <table class="table table-hover course-list-table">
                                    <thead>
                                        <tr>
                                            <th>Donate To</th>
                                            <th>Amount</th>
                                            <th>Date</th>
                                            {{-- <th>Profession</th> --}}
                                        </tr>
                                    </thead>
                                    <tbody>

                                      @if(count($user_donations)  > 0)
                                        
                                         @foreach($user_donations as $donation)  
                                        <tr>
                                            <th>{{ucwords($donation->title)}} </th>
                                            <th>{{getCurrencyCode()}} {{$donation->amount}} </th>
                                            <th>{{$donation->created_at}} </th>
                                            {{-- <th>{{$donation->alumni_profession}} </th> --}}
                                            
                                        </tr>

                                         @endforeach

                                         @else

                                         <h4>No donations are available</h4>

                                       @endif  
                                                  
                                    </tbody>
                                </table>
                            </div>
                        </section>
                    </section><!-- /.course-listing -->
                     <div class="row text-center">
                        <div class="col-sm-12">
                            <ul>
                                {{ $user_donations->links() }}
                            </ul>
                        </div>
                    </div>
                </div><!-- /#page-main -->
            </div><!-- /.col-md-8 -->



@endsection

@section('footer_scripts')


@stop

