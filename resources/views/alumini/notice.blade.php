@extends('layouts.alumini.layout')

@section('header_scripts')


@stop


@section('breadcrumb')

<div class="container">
    <ol class="breadcrumb">
       
        <li class="active">Notice Board</li>
    </ol>
</div>



@endsection



@section('content')

   <!--MAIN Content-->
            <div class="col-md-12">
                <div id="page-main">
                    <section class="blog-listing">
                        <header><h1>Notice Board</h1></header>
                        <div class="row">

                            @if(count($notices) > 0)

                               @foreach($notices as $notice)
                            
                             <div class="col-md-6 col-sm-12">
                                <div class="author">
                                    <blockquote>
                                        <article class="paragraph-wrapper">
                                            <div class="inner">
                                                <h3>{{ucwords($notice->title)}}</h3>
                                                <header>{!! $notice->description !!}</header>
                                                <footer>{{$notice->date}}</footer>
                                            </div>
                                        </article>
                                    </blockquote>
                                </div>
                            </div>

                              @endforeach

                            @endif  
                            

                                              
                         </div><!-- /.row -->
                    </section><!-- /.blog-listing -->
                     <div class="row text-center">
                        <div class="col-sm-12">
                            <ul>
                                {{ $notices->links() }}
                            </ul>
                        </div>
                    </div>
                </div><!-- /#page-main -->
            </div><!-- /.col-md-8 -->


@endsection

@section('footer_scripts')


@stop

