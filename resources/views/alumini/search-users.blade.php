@extends('layouts.alumini.layout')

@section('header_scripts')


@stop


@section('breadcrumb')

<div class="container">
    <ol class="breadcrumb">
       
        <li class="active">Search Alumni</li>
    </ol>
</div>



@endsection



@section('content')


@include('errors.errors')
<div class="col-md-8">
                <div id="page-main">
                    <section id="members">
                        <header><h1>Alumni</h1></header>
                        <section id="event-search">
                            <div class="search-box">

                                  {!! Form::open(array('url' => URL_GET_SEARCH_ALUMNI, 'method' => 'POST',  'novalidate'=>'','name'=>'formCategories','class'=>'form-inline','id'=>"event-search-form")) !!}

                               
                                    <div class="from-row">
                                        <div class="form-group">
                                            <label>Passed Out Year</label>
                                          

                                       {{Form::select('batch', $years, null, ['placeholder' => getPhrase('select_year'),'class'=>'selectize-control single selectize-input', 

                                            
                                             ])}}

                                        </div>
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input name="name"placeholder="Enter name" type="text">
                                        </div>
                                    </div>
                                    <button type="submit" class="btn pull-right">Search</button>
                              {!! Form::close() !!}                        
                                 </div>
                        </section>
                        <section id="our-speakers">
                            <div class="row">
                                <div class="col-md-12">
                                
                                @if(count($users) > 0 )

                                  @foreach( $users as $user )
 
                                     <div class="col-md-6">
                                        <div class="author-block course-speaker">
                                            <a href="https://venus.t1m9m.com/alumnus/dokoh">
                                                <figure class="author-picture">
                             <img src="{{ getProfilePath($user->image,'profile') }}" alt="">
                                         </figure>
                                            </a>
                                            <article class="paragraph-wrapper">
                                                <div class="inner">
                                                    <header>
                                                <a href="{{URL_VIEW_ALUMNI_DETAILS.$user->user_id}}">{{ucwords($user->name)}} </a>
                                                    </header>
                                                    <figure style="color: #262626;">
                                                        {{ucwords($user->alumni_profession)}}                                                
                                                           </figure>
                                                    <p>{{$user->quote}}</p>
                                                    <a href="{{URL_VIEW_ALUMNI_DETAILS.$user->user_id}}" class="btn btn-framed btn-small btn-color-grey">Full Bio</a>
                                                </div>
                                            </article>
                                        </div>
                                    </div>

                                    @endforeach

                                   @else
                                   
                                   <h4>No data available</h4> 

                                 @endif                        

                                  </div>
                            </div>
                        </section>
                        <div class="row text-center">
                        <div class="col-sm-12">
                            <ul>
                                {{ $users->links() }}
                            </ul>
                        </div>
                    </div>
                    </section>
                </div>
            </div>


 
    

@endsection

@section('footer_scripts')


@stop

