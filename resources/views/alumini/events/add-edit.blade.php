@extends($layout)
@section('header_scripts')
<link href="{{CSS}}bootstrap-datepicker.css" rel="stylesheet">
<link href="{{CSS}}plugins/datetimepicker/css/bootstrap-datetimepicker.css" rel="stylesheet">   
@stop
@section('content')
<div id="page-wrapper" ng-controller="visitorManage">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
	    <div class="container-fluid content-wrap">
			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="/"><i class="fa fa-home"></i></a> </li>
						<li><a href="{{URL_ALUMNI_EVENTS}}">{{ getPhrase('events')}}</a></li>
						<li class="active">{{isset($title) ? $title : ''}}</li>
					</ol>
				</div>
			</div>
				@include('errors.errors')
			<!-- /.row -->
			
			<section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item " >
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                        <h2 class="pull-left"> {{ $title }} </h2>   
                        <div class="pull-right messages-buttons">
							<a href="{{URL_ALUMNI_EVENTS}}" class="btn  btn-primary button panel-header-button" >{{ getPhrase('list')}}</a>
						</div>                 
                        <!--End panel icons-->
                    </div>
                    <div class="panel-body panel-body-p packages">
						<?php $button_name = getPhrase('create'); ?>
						@if ($record)
						 <?php $button_name = getPhrase('update'); ?>
							{{ Form::model($record, 
							array('url' => URL_ALUMNI_EVENTS_EDIT.$record->id, 
							'method'=>'patch', 'name'=>'formLocations ', 'novalidate'=>'','files'=>TRUE )) }}
						@else
							{!! Form::open(array('url' => URL_ALUMNI_EVENTS_ADD, 'method' => 'POST', 'name'=>'formLocations ', 'novalidate'=>'','files'=>TRUE )) !!}
						@endif
						

						 @include('alumini.events.form_elements', 
						 array('button_name'=> $button_name),
						 array(
						 	'record'=> $record, 
						 	'volunteers'=> $volunteers, 
						 
						 ))
						 		
						{!! Form::close() !!}
					</div>

				</div>
			</section>
		</div>
		<!-- /.container-fluid -->
	</section>
</div>
<!-- /#page-wrapper -->
@stop

@section('footer_scripts')
 @include('common.validations')
  @include('common.editor')


 <script src="{{JS}}moment.min.js"></script>

  <script src="{{JS}}plugins/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
 <script>
    
     $(function () {
        $('#dp').datetimepicker({
               format: 'YYYY-MM-DD',
            
            });

    });
 </script>


 
@stop
 
 