 				
                 <div class="row">
               
          <fieldset class="form-group col-md-6">

            {{ Form::label('title', getphrase('title')) }}

            <span class="text-red">*</span>

            {{ Form::text('title', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('title'),

              'ng-model'=>'title', 


              'required'=> 'true', 

              'ng-class'=>'{"has-error": formLocations.title.$touched && formLocations.title.$invalid}',

              )) }}

            <div class="validation-error" ng-messages="formLocations.title.$error" >

                {!! getValidationMessage()!!}


            </div>

          </fieldset> 


                 




                      <fieldset class="form-group col-md-6">
                                     
                        {{ Form::label('date', getphrase('date')) }}
                        <span class="text-red">*</span>

                        <div class="input-group date" data-date="{{date('Y-m-d')}}" data-provide="datepicker" data-date-format="yyyy-mm-dd">

                        {{ Form::text('date', $value = null , $attributes = array(

                          'class'       =>'form-control',
                          'placeholder' => '2015-07-17', 
                          'id'          =>'dp',
                      
                         

                           )) }}

                      
                            <div class="input-group-addon">

                                <span class="fa fa-calendar"></span>

                            </div>

                        </div>

              </fieldset>

               <fieldset class="form-group  col-md-6">
            
            {{ Form::label('eaddress', getphrase('address')) }}
             <span class="text-red">*</span>
            
            {{ Form::textarea('eaddress', $value = null , $attributes = array('class'=>'form-control', 'rows'=>'5', 'placeholder' => getPhrase('eaddress'))) }}
          </fieldset>



                  <fieldset class="form-group col-md-6">

                            {{ Form::label('volunteer_id', getphrase('volunteer')) }}
                            <span class="text-red">*</span>
                            {{Form::select('volunteer_id', $volunteers, null, ['class'=>'form-control', 'id'=>'volunteer_id',
                                'placeholder'=>'Select',
                                'ng-model'=>'volunteer_id',
                                'required'=> 'true', 
                                'ng-class'=>'{"has-error": formLocations.volunteer_id.$touched && formLocations.volunteer_id.$invalid}'
                            ])}}
                             <div class="validation-error" ng-messages="formLocations.volunteer_id.$error" >
                                {!! getValidationMessage()!!}
                            </div>

                        
                  </fieldset>

                

           <fieldset class="form-group  col-md-6">
            
            {{ Form::label('short_description', getphrase('short_description')) }}
             <span class="text-red">*</span>
            
            {{ Form::textarea('short_description', $value = null , $attributes = array('class'=>'form-control ckeditor', 'rows'=>'5', 'placeholder' => getPhrase('short_description'))) }}
          </fieldset>

          <fieldset class="form-group  col-md-6">
            
            {{ Form::label('long_description', getphrase('long_description')) }}
            
            {{ Form::textarea('long_description', $value = null , $attributes = array('class'=>'form-control ckeditor', 'rows'=>'5', 'placeholder' => getPhrase('long_description'))) }}
          </fieldset>



            <fieldset class="form-group col-md-6" >
           {{ Form::label('image', getphrase('image')) }}
                 <input type="file" class="form-control" name="catimage" 
                 accept=".png,.jpg,.jpeg" id="image_input">
                  
                  
            </fieldset>

          
            @if($record && $record->image)

              <fieldset class="form-group col-md-6">
                
                  <img src="{{URL_ALUMNI_EVENTS_IMAGE.$record->image}}" class="img img-responsive" height="100px" width="100px"><p></p>
               

          

              @elseif( $record && !$record->image )

               <img src="{{IMAGE_PATH_UPLOAD_ASSETS_DEFAULT}}" height="80px" width="80px"><p></p>

                  </fieldset>
              @endif


                  
                 

					</div>

					<input type="hidden" name="added_by" value="{{ Auth::user()->id }}">


						<div class="buttons text-center">

							<button class="btn btn-lg btn-primary button"

							ng-disabled='!formLocations.$valid'>{{ $button_name }}</button>

						</div>

		 