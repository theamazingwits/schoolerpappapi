@extends($layout)

@section('header_scripts')

<link href="{{CSS}}ajax-datatables.css" rel="stylesheet">

@stop

@section('content')

  <div id="page-wrapper ">
        <!--Begin Content-->
        <section id="main" class="main-wrap bgc-white-darkest" role="main">

    
            <div class="container-fluid content-wrap">

                    <!-- Page Heading -->
                        <div class="row">

                            <div class="col-lg-12">

                                <ol class="breadcrumb">

                                    <li><a href="{{PREFIX}}"><i class="menu-icon1 fa fa-home"></i></a> </li>
                                    
                                    @if(checkRole(getUserGrade(2)))
                            <li><a href="{{URL_USERS_DASHBOARD}}">{{ getPhrase('users_dashboard') }}</a> </li>
                            @endif

                            @if(checkRole(getUserGrade(2)))
                            <li><a href="{{URL_USERS."student"}}">{{ getPhrase('student_users') }}</a> </li>
                            @endif

                            @if(checkRole(getUserGrade(7)))
                            <li><a href="{{URL_PARENT_CHILDREN}}">{{ getPhrase('children') }}</a> </li>
                            @endif

                            <li>{{ $title }}</li>

                                </ol>

                            </div>

                            </div>


                <div class="row panel-grid" id="panel-grid">
                    <div class="col-sm-12 col-md-12 col-lg-12 panel-wrap panel-grid-item grid-stack-item">
                        <!--Start Panel-->
                        <div class="panel bgc-white-dark">
                            <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                                <h2 class="pull-left">
                                {{ $title }}</h2>
                                <!--Start panel icons-->
                                <div class="panel-icons panel-icon-slide ">
                                    <ul>
                                        <li><a href=""><i class="fa fa-angle-left"></i></a>
                                            <ul>
                                                <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                                                <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                                                <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
                                                <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
                                                <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li>
                                                <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                <!--End panel icons-->
                            </div>
                            <div class="panel-body packages">
						<div> 
							<h4>{{$record->title}} :  

									 @if($status_record)

								             @if($status_record->is_approved == 1)

								             	 <span class="label label-success">Approved</span>
								             
								             @elseif ($status_record->is_resubmitted == 1) 

								                <span class="label label-warning">Rejected</span>
								             
								             @else
								             	
								                <span class="label label-info">Submitted</span>
								             
	                                         @endif
							           
							           @else

							           	    <span class="label label-primary">Not Submitted</span>
							           

							           @endif
                               </h4>

								<div class="alert alert-info">

						  <strong>Note:</strong> {{$record->description}}

						</div>

						<table class="table table-striped table-bordered datatable" cellspacing="0" width="100%">
							<thead>
								<tr>
									
									<th><b>{{ getPhrase('file_name')}}</b></th>
									<th><b>{{ getPhrase('file')}}</b></th>
								  
								</tr>
							</thead>
							<tbody>
								@foreach($uploads as $upload)
								 <tr>
								 	@if($upload->name)
								 	  <td>{{$upload->name}}</td>
								 	@else
								 	  <td>-</td>
								 	@endif
								 	 <td>
								 	 	<img src="{{IMAGE_PATH_UPLOAD_CERTIFICATES_DEFAULT}}" height="50px" width="50px">
								 	 	<p><a href="{{URL_DOWNLOAD_USER_CERTIFICATE.$upload->image}}"><i class="fa fa-cloud-download"></i> {{$upload->name}}</a></p>
								 	 </td>  
								 </tr>

								@endforeach
							</tbody>
							 
						</table>
						</div>

						@if(checkRole(getUserGrade(1)) && count($uploads) > 0 )

                       <div style="float: right;">

                       	    @if($any_approved == 0 )
							<a href="javascript:void(0)" onclick="adminApprove('approve')" class="btn btn-primary btn-sm">Approve</a>
							@endif

							@if($any_rejected == 0)
							<a href="javascript:void(0)" onclick="adminApprove('reject')" class="btn btn-warning btn-sm">Reject</a>

							@endif
					  </div>

						@endif

					</div>
                        </div>
                        <!--End Panel-->
                    </div>
                </div>
            </div> 
            </section>
             
             <div id="approve" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" align="center"><b id="main_title"></b></h4>
      </div>
      <div class="modal-body">
        

	{!! Form::open(array('url' => URL_APPROVE_USER_CERTIFICATE, 'method' => 'POST', 'name'=>'formQuiz ', 'novalidate'=>'','id'=>'formQuiz' )) !!}
       
        <h4 style="color: #ffa616;" align="center" id="sub_title"></h4>

        <input type="hidden" name="admin_status" id="admin_status">
        <input type="hidden" name="notification_id" value="{{$record->id}}">
        <input type="hidden" name="user_id" value="{{$user->id}}">
      

      </div>



      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Yes</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>&nbsp;
      </div>
      {!! Form::close() !!}
      
    </div>

  </div>
</div>


		</div>
@endsection

@section('footer_scripts')

<script>

      function adminApprove(type)
      {    
      	   if(type == "approve"){
              
              $('#main_title').html('Approve Certificates');
              $('#sub_title').html('Are you sure to approve certificates');

      	   }
      	   else{
                
                 $('#main_title').html('Reject Certificates');
                 $('#sub_title').html('Are you sure to reject certificates');
      	   }
           
           $('#admin_status').val(type);
           $('#approve').modal('show');
      }

 </script>  


@stop
