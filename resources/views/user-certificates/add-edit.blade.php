@extends($layout)
<!-- <link href="{{CSS}}bootstrap-datepicker.css" rel="stylesheet"> -->
@section('content')

<div id="page-wrapper" ng-controller ="prepareStudentsData" >
	<section id="main" class="main-wrap bgc-white-darkest" role="main">

			<div class="container-fluid content-wrap">
				<!-- Page Heading -->
				<div class="row">
					<div class="col-lg-12">
						<ol class="breadcrumb">
							<li><a href="/"><i class="fa fa-home bc-home"></i></a> </li>
							<li class="active">{{isset($title) ? $title : ''}}</li>
						</ol>
					</div>
				</div>
					@include('errors.errors')
				<!-- /.row -->

				 <div class="row panel-grid" id="panel-grid">
                    <div class="col-sm-12 col-md-12 col-lg-12 panel-wrap panel-grid-item grid-stack-item">
                        <!--Start Panel-->
                        <div class="panel bgc-white-dark">
                            <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                                <h2 class="pull-left">
                                {{$title}}</h2>
                                <!--Start panel icons-->
                                <div class="panel-icons panel-icon-slide ">
                                    <ul>
                                        <li><a href=""><i class="fa fa-angle-left"></i></a>
                                            <ul>
                                                <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                                                <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                                                <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
                                                <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
                                                <!-- <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li> -->
                                                <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                <!--End panel icons-->
                            </div>
                            <div class="panel-body" >
					
					
						{!! Form::open(array('url' => URL_USER_CERTIFICATES_STORE, 'method' => 'POST', 'name'=>'formQuiz ', 'novalidate'=>'','files'=>TRUE )) !!}
					
					    <div class="alert alert-info">

						  <strong>Note:</strong> {{$record->description}}

						</div>

						<input type="hidden" name="user_id" value="{{$user_id}}">
						<input type="hidden" name="notification_id" value="{{$record->id}}">

					 @include('user-certificates.form_elements', 
					
					 array('record'=> $record, 
					 ))
					 		
					{!! Form::close() !!}
					</div>
                        </div>
                        <!--End Panel-->
                    </div>
                </div>





			</div>
			<!-- /.container-fluid -->
		</section>
</div>
		<!-- /#page-wrapper -->
@stop

@section('footer_scripts')
 @include('common.validations')

 @include('common.angular-factory',array('load_module'=> FALSE))



 <script>
app.controller('prepareStudentsData', function( $scope, $http, httpPreConfig) {

      
        $scope.initFunctions = function(parent_id) 
        {   
       
            $scope.getStudentApplicationDetails(parent_id);
        }
    

 $scope.routeDetails           = [];
 
 $scope.getStudentApplicationDetails = function(vroute_id)
      {

        route = '{{URL_GET_VIA_ROUTES}}',
        data= {_method: 'post',
                 '_token':httpPreConfig.getToken(),
                   'vroute_id': vroute_id,
                 };

           httpPreConfig.webServiceCallPost(route, data).then(function(result){
              
              // console.log(result.data);
              $scope.routeDetails           = result.data.all_routes;
        
         });

      }

  $scope.addNewRoute = function(){

        $scope.routeDetails.push({ 
            'name': "", 
            'cost': "",
            
        });
    };

$scope.removeRoute = function(){

    var newExpDataList=[];
    $scope.selectedAll = false;
    angular.forEach($scope.routeDetails, function(selected){
        if(!selected.selected){
            newExpDataList.push(selected);
        }
    }); 
    $scope.routeDetails = newExpDataList;
};



});





 
</script>
 

@stop
 