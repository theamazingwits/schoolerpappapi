 				
                 <div class="row">

 					 <fieldset class="form-group col-md-12">

						{{ Form::label('serial_no', getphrase('serial_no')) }}

						<span class="text-red">*</span>

						{{ Form::text('serial_no', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('serial_no'),

							'ng-model'=>'serial_no', 


							'required'=> 'true', 

							'ng-class'=>'{"has-error": formLocations.serial_no.$touched && formLocations.serial_no.$invalid}',


							'ng-maxlength' => '100',

							)) }}

						<div class="validation-error" ng-messages="formLocations.serial_no.$error" >

	    					{!! getValidationMessage()!!}
                            
                            {!! getValidationMessage('maxlength')!!}

						</div>

					</fieldset>	



 					 <fieldset class="form-group col-md-12">

						{{ Form::label('title', getphrase('asset_name')) }}

						<span class="text-red">*</span>

						{{ Form::text('title', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('title'),

							'ng-model'=>'title', 


							'required'=> 'true', 

							'ng-class'=>'{"has-error": formLocations.title.$touched && formLocations.title.$invalid}',

							'ng-minlength' => '4',

							'ng-maxlength' => '100',

							)) }}

						<div class="validation-error" ng-messages="formLocations.title.$error" >

	    					{!! getValidationMessage()!!}


	    					{!! getValidationMessage('minlength')!!}

	    					{!! getValidationMessage('maxlength')!!}

						</div>

					</fieldset>	


					 <fieldset class="form-group col-md-12">

                            {{ Form::label('status', getphrase('status')) }}
                            <span class="text-red">*</span>
                            {{Form::select('status', $status, null, ['class'=>'form-control', 'id'=>'status',
                                'placeholder'=>'Select',
                                'ng-model'=>'status',
                                'required'=> 'true', 
                                'ng-class'=>'{"has-error": formLocations.status.$touched && formLocations.status.$invalid}'
                            ])}}
                             <div class="validation-error" ng-messages="formLocations.status.$error" >
                                {!! getValidationMessage()!!}
                            </div>

                        
                  </fieldset>


                  	 <fieldset class="form-group col-md-12">

                            {{ Form::label('asset_condition', getphrase('condition')) }}
                            <span class="text-red">*</span>
                            {{Form::select('asset_condition', $condition, null, ['class'=>'form-control', 'id'=>'asset_condition',
                                'placeholder'=>'Select',
                                'ng-model'=>'asset_condition',
                                'required'=> 'true', 
                                'ng-class'=>'{"has-error": formLocations.asset_condition.$touched && formLocations.asset_condition.$invalid}'
                            ])}}
                             <div class="validation-error" ng-messages="formLocations.asset_condition.$error" >
                                {!! getValidationMessage()!!}
                            </div>

                        
                  </fieldset>


                   <fieldset class="form-group col-md-12">

                            {{ Form::label('category_id', getphrase('category')) }}
                            <span class="text-red">*</span>
                            {{Form::select('category_id', $categories, null, ['class'=>'form-control', 'id'=>'category_id',
                                'placeholder'=>'Select',
                                'ng-model'=>'category_id',
                                'required'=> 'true', 
                                'ng-class'=>'{"has-error": formLocations.category_id.$touched && formLocations.category_id.$invalid}'
                            ])}}
                             <div class="validation-error" ng-messages="formLocations.category_id.$error" >
                                {!! getValidationMessage()!!}
                            </div>

                        
                  </fieldset>

                    <fieldset class="form-group col-md-12">

                            {{ Form::label('location_id', getphrase('location')) }}
                            <span class="text-red">*</span>
                            {{Form::select('location_id', $locations, null, ['class'=>'form-control', 'id'=>'location_id',
                                'placeholder'=>'Select',
                                'ng-model'=>'location_id',
                                'required'=> 'true', 
                                'ng-class'=>'{"has-error": formLocations.location_id.$touched && formLocations.location_id.$invalid}'
                            ])}}
                             <div class="validation-error" ng-messages="formLocations.location_id.$error" >
                                {!! getValidationMessage()!!}
                            </div>

                        
                  </fieldset>



                   <fieldset class="form-group col-md-6" >
				        {{ Form::label('image', getphrase('file')) }}
				         <input type="file" class="form-control" name="image">


				      </fieldset>
                     
                      @if($record && $record->image)

				      <fieldset class="form-group col-md-6">
				      	
				          <img src="{{IMAGE_PATH_UPLOAD_ASSETS.$record->image}}" class="img img-responsive" height="100px" width="100px"><p></p>
				       

				      </fieldset>

				      @elseif( $record && !$record->image )

				       <img src="{{IMAGE_PATH_UPLOAD_ASSETS_DEFAULT}}" height="80px" width="80px"><p></p>

				      @endif



					</div>

					<input type="hidden" name="added_by" value="{{ Auth::user()->id }}">


						<div class="buttons text-center">

							<button class="btn btn-lg btn-primary button"

							ng-disabled='!formLocations.$valid'>{{ $button_name }}</button>

						</div>

		 