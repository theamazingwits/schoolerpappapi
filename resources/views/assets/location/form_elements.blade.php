 				
                 <div class="row">

 					 <fieldset class="form-group col-md-12">

						{{ Form::label('location', getphrase('location')) }}

						<span class="text-red">*</span>

						{{ Form::text('location', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('location'),

							'ng-model'=>'location', 


							'required'=> 'true', 

							'ng-class'=>'{"has-error": formLocations.location.$touched && formLocations.location.$invalid}',

							'ng-minlength' => '4',

							'ng-maxlength' => '100',

							)) }}

						<div class="validation-error" ng-messages="formLocations.location.$error" >

	    					{!! getValidationMessage()!!}


	    					{!! getValidationMessage('minlength')!!}

	    					{!! getValidationMessage('maxlength')!!}

						</div>

					</fieldset>	

			
					<fieldset class="form-group col-md-12">
						
						{{ Form::label('description', getphrase('description')) }}
						
						{{ Form::textarea('description', $value = null , $attributes = array('class'=>'form-control', 'rows'=>'5', 'placeholder' => 'Description','id'=>'description')) }}
					</fieldset>	

					</div>

					<input type="hidden" name="added_by" value="{{ Auth::user()->id }}">


						<div class="buttons text-center">

							<button class="btn btn-lg btn-primary button"

							ng-disabled='!formLocations.$valid'>{{ $button_name }}</button>

						</div>

		 