 				
                 <div class="row">

 					 <fieldset class="form-group col-md-12">

						{{ Form::label('name', getphrase('vendor_name')) }}

						<span class="text-red">*</span>

						{{ Form::text('name', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('name'),

							'ng-model'=>'name', 


							'required'=> 'true', 

							'ng-class'=>'{"has-error": formLocations.name.$touched && formLocations.name.$invalid}',

							'ng-minlength' => '4',

							'ng-maxlength' => '100',

							)) }}

						<div class="validation-error" ng-messages="formLocations.name.$error" >

	    					{!! getValidationMessage()!!}


	    					{!! getValidationMessage('minlength')!!}

	    					{!! getValidationMessage('maxlength')!!}

						</div>

					</fieldset>	


					 <fieldset class="form-group col-md-12">

					
						{{ Form::label('email', getphrase('email')) }}

						<span class="text-red">*</span>

						{{ Form::email('email', $value = null, $attributes = array('class'=>'form-control', 'placeholder' => 'jack@jarvis.com',

							'ng-model'=>'email',

							'required'=> 'true', 

							'ng-class'=>'{"has-error": formLocations.email.$touched && formLocations.email.$invalid}')) }}

						 <div class="validation-error" ng-messages="formLocations.email.$error" >

	    					{!! getValidationMessage()!!}

	    					{!! getValidationMessage('email')!!}

						</div>

					</fieldset>


						 <fieldset class="form-group col-md-12">

						{{ Form::label('phone', getphrase('phone')) }}

						<span class="text-red">*</span>

						{{ Form::number('phone', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('phone'),

							'ng-model'=>'phone', 


							'required'=> 'true', 

							'ng-class'=>'{"has-error": formLocations.phone.$touched && formLocations.phone.$invalid}',

					
							)) }}

						<div class="validation-error" ng-messages="formLocations.phone.$error" >

	    					{!! getValidationMessage()!!}
                       </div>

					</fieldset>	

			
					<fieldset class="form-group col-md-12">
						
						{{ Form::label('supply', getphrase('supplied_items')) }}

						<span class="text-red">*</span>
						
						{{ Form::textarea('supply', $value = null , $attributes = array('class'=>'form-control', 'rows'=>'5', 'placeholder' => 'supply','id'=>'supply')) }}
					</fieldset>	

					<fieldset class="form-group col-md-12">
						
						{{ Form::label('address', getphrase('address')) }}
						
						{{ Form::textarea('address', $value = null , $attributes = array('class'=>'form-control', 'rows'=>'5', 'placeholder' => 'address','id'=>'address')) }}
					</fieldset>	

					</div>

					<input type="hidden" name="added_by" value="{{ Auth::user()->id }}">


						<div class="buttons text-center">

							<button class="btn btn-lg btn-primary button"

							ng-disabled='!formLocations.$valid'>{{ $button_name }}</button>

						</div>

		 