 				
                 <div class="row">
               
                <fieldset class="form-group col-md-6">

                            {{ Form::label('asset_id', getphrase('asset')) }}
                            <span class="text-red">*</span>
                            {{Form::select('asset_id', $assets, null, ['class'=>'form-control', 'id'=>'asset_id',
                                'placeholder'=>'Select',
                                'ng-model'=>'asset_id',
                                'required'=> 'true', 
                                'ng-class'=>'{"has-error": formLocations.asset_id.$touched && formLocations.asset_id.$invalid}'
                            ])}}
                             <div class="validation-error" ng-messages="formLocations.asset_id.$error" >
                                {!! getValidationMessage()!!}
                            </div>

                        
                  </fieldset>



            <fieldset class="form-group col-md-6 ">
                
                {{ Form::label('role', getphrase('role')) }}
               
               <span class="text-red">*</span>
          
               {{Form::select('role_id', $roles, null, [

              'placeholder' => getPhrase('select_role'),

              'class'=>'form-control',

              'ng-model'=>'role_id',

              'id'=>'role_id',

              'required'=> 'true', 

              'ng-change'=>'getUsers(role_id)',

              'ng-class'=>'{"has-error": formQuiz.role_id.$touched && formQuiz.role_id.$invalid}'

             ])}}

              <div class="validation-error" ng-messages="formQuiz.role_id.$error" >

                {!! getValidationMessage()!!}

            </div>

        </fieldset>


         <fieldset ng-if = "selected_role_id" class="form-group col-md-6">
             <label for = "user_id">{{getPhrase('users')}}</label>

                        <select 

                        name      = "user_id" 
                        id        = "user_id" 
                        class     = "form-control" 
                        ng-model  = "user_id" 
                        ng-options= "option.id as option.name for option in users track by option.id">
                        <option value="">{{getPhrase('select')}}</option>
                    
                        </select>

            </fieldset>



                      <fieldset class="form-group col-md-6">

            {{ Form::label('quantity', getphrase('quantity')) }}

            <span class="text-red">*</span>

            {{ Form::number('quantity', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('quantity'),

              'ng-model'=>'quantity', 


              'required'=> 'true', 

              'ng-class'=>'{"has-error": formLocations.quantity.$touched && formLocations.quantity.$invalid}',

              )) }}

            <div class="validation-error" ng-messages="formLocations.quantity.$error" >

                {!! getValidationMessage()!!}


            </div>

          </fieldset> 


                 




                      <fieldset class="form-group col-md-6">
                                     
                        {{ Form::label('due_date', getphrase('due_date')) }}
                        <span class="text-red">*</span>

                        <div class="input-group date" data-date="{{date('Y-m-d')}}" data-provide="datepicker" data-date-format="yyyy-mm-dd">

                        {{ Form::text('due_date', $value = null , $attributes = array(

                          'class'       =>'form-control',
                          'placeholder' => '2015-7-17', 
                          'id'          =>'dp',
                      
                         

                           )) }}

                      
                            <div class="input-group-addon">

                                <span class="mdi mdi-calendar"></span>

                            </div>

                        </div>

              </fieldset>


               <fieldset class="form-group col-md-6">
                                     
                        {{ Form::label('checkout_date', getphrase('checkout_date')) }}
                        <span class="text-red">*</span>

                        <div class="input-group date" data-date="{{date('Y-m-d')}}" data-provide="datepicker" data-date-format="yyyy-mm-dd">

                        {{ Form::text('checkout_date', $value = null , $attributes = array(

                          'class'       =>'form-control',
                          'placeholder' => '2015-7-17', 
                          'id'          =>'dp1',
                         

                           )) }}

                   
                            <div class="input-group-addon">

                                <span class="mdi mdi-calendar"></span>

                            </div>

                        </div>

              </fieldset>


               <fieldset class="form-group col-md-6">
                                     
                        {{ Form::label('checkin_date', getphrase('checkin_date')) }}
                        <span class="text-red">*</span>

                        <div class="input-group date" data-date="{{date('Y-m-d')}}" data-provide="datepicker" data-date-format="yyyy-mm-dd">

                        {{ Form::text('checkin_date', $value = null , $attributes = array(

                          'class'       =>'form-control',
                          'placeholder' => '2015-7-17', 
                          'id'          =>'dp2',
                        

                           )) }}

                      
                            <div class="input-group-addon">

                                <span class="mdi mdi-calendar"></span>

                            </div>

                        </div>

              </fieldset>

              <fieldset class="form-group col-md-6">

                            {{ Form::label('location_id', getphrase('location')) }}
                            <span class="text-red">*</span>
                            {{Form::select('location_id', $locations, null, ['class'=>'form-control', 'id'=>'location_id',
                                'placeholder'=>'Select',
                                'ng-model'=>'location_id',
                                'required'=> 'true', 
                                'ng-class'=>'{"has-error": formLocations.location_id.$touched && formLocations.location_id.$invalid}'
                            ])}}
                             <div class="validation-error" ng-messages="formLocations.location_id.$error" >
                                {!! getValidationMessage()!!}
                            </div>

                        
                  </fieldset>

                  <fieldset class="form-group col-md-6">

                            {{ Form::label('status', getphrase('status')) }}
                            <span class="text-red">*</span>
                            {{Form::select('status', $status, null, ['class'=>'form-control', 'id'=>'status',
                                'placeholder'=>'Select',
                                'ng-model'=>'status',
                                'required'=> 'true', 
                                'ng-class'=>'{"has-error": formLocations.status.$touched && formLocations.status.$invalid}'
                            ])}}
                             <div class="validation-error" ng-messages="formLocations.status.$error" >
                                {!! getValidationMessage()!!}
                            </div>

                        
                  </fieldset>

                  <fieldset class="form-group col-md-12">
            
            {{ Form::label('notes', getphrase('notes')) }}
            
            {{ Form::textarea('notes', $value = null , $attributes = array('class'=>'form-control', 'rows'=>'5', 'placeholder' => 'Notes','id'=>'notes')) }}
          </fieldset> 




                  
                 

					</div>

					<input type="hidden" name="added_by" value="{{ Auth::user()->id }}">


						<div class="buttons text-center">

							<button class="btn btn-lg btn-primary button"

							ng-disabled='!formLocations.$valid'>{{ $button_name }}</button>

						</div>

		 