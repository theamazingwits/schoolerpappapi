 				
                 <div class="row">

 					


 				


					 <fieldset class="form-group col-md-12">

                            {{ Form::label('asset_id', getphrase('asset')) }}
                            <span class="text-red">*</span>
                            {{Form::select('asset_id', $assets, null, ['class'=>'form-control', 'id'=>'asset_id',
                                'placeholder'=>'Select',
                                'ng-model'=>'asset_id',
                                'required'=> 'true', 
                                'ng-class'=>'{"has-error": formLocations.asset_id.$touched && formLocations.asset_id.$invalid}'
                            ])}}
                             <div class="validation-error" ng-messages="formLocations.asset_id.$error" >
                                {!! getValidationMessage()!!}
                            </div>

                        
                  </fieldset>


                  	 <fieldset class="form-group col-md-12">

                            {{ Form::label('vendor_id', getphrase('vendor')) }}
                            <span class="text-red">*</span>
                            {{Form::select('vendor_id', $vendors, null, ['class'=>'form-control', 'id'=>'vendor_id',
                                'placeholder'=>'Select',
                                'ng-model'=>'vendor_id',
                                'required'=> 'true', 
                                'ng-class'=>'{"has-error": formLocations.vendor_id.$touched && formLocations.vendor_id.$invalid}'
                            ])}}
                             <div class="validation-error" ng-messages="formLocations.vendor_id.$error" >
                                {!! getValidationMessage()!!}
                            </div>

                        
                  </fieldset>


                     <fieldset class="form-group col-md-12">

            {{ Form::label('quantity', getphrase('quantity')) }}

            <span class="text-red">*</span>

            {{ Form::number('quantity', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('quantity'),

              'ng-model'=>'quantity', 
              'min'=>1,

              'required'=> 'true', 

              'ng-class'=>'{"has-error": formLocations.quantity.$touched && formLocations.quantity.$invalid}',

              )) }}

            <div class="validation-error" ng-messages="formLocations.quantity.$error" >

                {!! getValidationMessage()!!}


            </div>

          </fieldset> 


                   <fieldset class="form-group col-md-12">

                            {{ Form::label('unit', getphrase('unit')) }}
                            <span class="text-red">*</span>
                            {{Form::select('unit', $units, null, ['class'=>'form-control', 'id'=>'unit',
                                'placeholder'=>'Select',
                                'ng-model'=>'unit',
                                'required'=> 'true', 
                                'ng-class'=>'{"has-error": formLocations.unit.$touched && formLocations.unit.$invalid}'
                            ])}}
                             <div class="validation-error" ng-messages="formLocations.unit.$error" >
                                {!! getValidationMessage()!!}
                            </div>

                        
                  </fieldset>

                     <fieldset class="form-group col-md-12">

            {{ Form::label('price', getphrase('price')) }}

            <span class="text-red">*</span>

            {{ Form::number('price', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('price'),

              'ng-model'=>'price',

              'min'=>1, 


              'required'=> 'true', 

              'ng-class'=>'{"has-error": formLocations.price.$touched && formLocations.price.$invalid}',

              )) }}

            <div class="validation-error" ng-messages="formLocations.price.$error" >

                {!! getValidationMessage()!!}


            </div>

          </fieldset> 

                      <fieldset class="form-group col-md-12">
                                     
                        {{ Form::label('purchase_date', getphrase('purchase_date')) }}
                        <span class="text-red">*</span>

                        <div class="input-group date" data-date="{{date('Y-m-d')}}" data-provide="datepicker" data-date-format="yyyy-mm-dd">

                        {{ Form::text('purchase_date', $value = null , $attributes = array(

                          'class'       =>'form-control',
                          'placeholder' => '2015-7-17', 
                          'id'          =>'dp',
                         
                           )) }}

                      

                            <div class="input-group-addon">

                                <span class="mdi mdi-calendar"></span>

                            </div>

                        </div>

              </fieldset>


               <fieldset class="form-group col-md-12">
                                     
                        {{ Form::label('service_date', getphrase('service_date')) }}
                        <span class="text-red">*</span>

                        <div class="input-group date" data-date="{{date('Y-m-d')}}" data-provide="datepicker" data-date-format="yyyy-mm-dd">

                        {{ Form::text('service_date', $value = null , $attributes = array(

                          'class'       =>'form-control',
                          'placeholder' => '2015-7-17', 
                          'id'          =>'dp1',
                        
                           )) }}

                     
                            <div class="input-group-addon">

                                <span class="mdi mdi-calendar"></span>

                            </div>

                        </div>

              </fieldset>


               <fieldset class="form-group col-md-12">
                                     
                        {{ Form::label('expire_date', getphrase('expire_date')) }}
                        <span class="text-red">*</span>

                        <div class="input-group date" data-date="{{date('Y-m-d')}}" data-provide="datepicker" data-date-format="yyyy-mm-dd">

                        {{ Form::text('expire_date', $value = null , $attributes = array(

                          'class'       =>'form-control',
                          'placeholder' => '2015-7-17', 
                          'id'          =>'dp2',
                         

                           )) }}

                      

                            <div class="input-group-addon">

                                <span class="mdi mdi-calendar"></span>

                            </div>

                        </div>

              </fieldset>



                  
                 

					</div>

					<input type="hidden" name="added_by" value="{{ Auth::user()->id }}">


						<div class="buttons text-center">

							<button class="btn btn-lg btn-primary button"

							ng-disabled='!formLocations.$valid'>{{ $button_name }}</button>

						</div>

		 