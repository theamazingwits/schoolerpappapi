@extends($layout)
@section('header_scripts')
 {!! Charts::assets() !!}
@stop

@section('content')

{{-- User Certificates --}}
<?php
    
   $crecords        = App\UserCertificates::DocumetsSubmitted();
   $user_submites   = App\UserCertificates::where('user_id',Auth::user()->id)
                                           ->where('is_submitted',1)
                                           ->orWhere('is_approved',1)
                                           ->groupby('notification_id')
                                           ->pluck('notification_id')
                                           ->toArray();

?>  

@if($crecords)                                         

   @if(count($user_submites) > 0 )

   	  @foreach ($crecords as $key => $value)
   	     
         @if(in_array($value['id'], $user_submites))

         @else

           <div class="alert alert-danger">
			  <strong>Note:</strong> {{$value['description']}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="{{URL_UPLOAD_USER_CERTIFICATES.$value['id']}}" class="btn btn-primary btn-sm">Click here to upload</a>
			</div>

         @endif

   	   
      @endforeach

   @else
     
       @foreach ($crecords as $crecord)
   	      
          <div class="alert alert-danger">
			  <strong>Note:</strong> {{$crecord['description']}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="{{URL_UPLOAD_USER_CERTIFICATES.$crecord['id']}}" class="btn btn-primary btn-sm">Click here to upload</a>
			</div>

      @endforeach

  @endif 

@endif 







<div id="page-wrapper">

	<section id="main" class="main-wrap bgc-white-darkest" role="main">
             <div class="container-fluid content-wrap">

					<div class="row">
					<div class="col-lg-12">
						<ol class="breadcrumb">
							 
							<li><i class="fa fa-home bc-home"></i> {{ $title}}</li>
						</ol>
					</div>
				</div>


                <div class="row panel-grid grid-stack" id="panel-grid">

                	           <!--**  hostels **-->
                	<section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
	                        <!--Start Panel-->
	                        <div class="panel pb-0 bgc-white-dark">
	                            <div class="panel-body pt-2 grid-stack-handle">
	                                <div class="h-lg pos-r panel-body-p py-0">
	                                    <h3 class=""><span class="fs-2 fw-light">{{ getPhrase('hostels')}}</span> <i class="pull-right fa fa-calculator fs-1 m-2"></i></h3>
	                                    <h6 class="c-primary lh-5"></h6>
	                                    <br><br>
	                                    <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{ URL_HOSTEL }}">
	                                        <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
	                                            <i class="icon-directions fs-4"></i>
	                                        </span>
	                                        <span class="d-inline-block align-top lh-7">
	                                            <span class="fs-6 d-block">{{ getPhrase('view_all')}}</span>
	                                            <span class="c-gray fs-7">Click to View more details</span>
	                                        </span>
	                                        <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
	                                    </a>
	                                </div>
	                            </div>
	                        </div>
	                        <!--End Panel-->
                    </section>

                    			<!--** hostel_rooms **-->
                    <section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
	                        <!--Start Panel-->
	                        <div class="panel pb-0 bgc-white-dark">
	                            <div class="panel-body pt-2 grid-stack-handle">
	                                <div class="h-lg pos-r panel-body-p py-0">
	                                    <h3 class=""><span class="fs-2 fw-light">{{ getPhrase('hostel_rooms')}}</span> <i class="pull-right fa fa-list-ol fs-1 m-2"></i></h3>
	                                    <h6 class="c-primary lh-5"></h6>
	                                    <br><br>
	                                    <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{ URL_HOSTEL_ROOMS }}">
	                                        <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
	                                            <i class="icon-directions fs-4"></i>
	                                        </span>
	                                        <span class="d-inline-block align-top lh-7">
	                                            <span class="fs-6 d-block">{{ getPhrase('view_all')}}</span>
	                                            <span class="c-gray fs-7">Click to View more details</span>
	                                        </span>
	                                        <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
	                                    </a>
	                                </div>
	                            </div>
	                        </div>
	                        <!--End Panel-->
                    </section>

                    		<!--** pay_fee **-->


                    <section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
	                        <!--Start Panel-->
	                        <div class="panel pb-0 bgc-white-dark">
	                            <div class="panel-body pt-2 grid-stack-handle">
	                                <div class="h-lg pos-r panel-body-p py-0">
	                                    <h3 class=""><span class="fs-2 fw-light">{{ getPhrase('pay_fee')}}</span> <i class="pull-right fa fa-money fs-1 m-2"></i></h3>
	                                    <h6 class="c-primary lh-5"></h6>
	                                    <br><br>
	                                    <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{ URL_PAY_HOSTEL_FEE }}">
	                                        <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
	                                            <i class="icon-directions fs-4"></i>
	                                        </span>
	                                        <span class="d-inline-block align-top lh-7">
	                                            <span class="fs-6 d-block">{{ getPhrase('view_all')}}</span>
	                                            <span class="c-gray fs-7">Click to View more details</span>
	                                        </span>
	                                        <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
	                                    </a>
	                                </div>
	                            </div>
	                        </div>
	                        <!--End Panel-->
                    </section>


                    		<!--** feed_back **-->


                    <section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
	                        <!--Start Panel-->
	                        <div class="panel pb-0 bgc-white-dark">
	                            <div class="panel-body pt-2 grid-stack-handle">
	                                <div class="h-lg pos-r panel-body-p py-0">
	                                    <h3 class=""><span class="fs-2 fw-light">{{ getPhrase('feed_back')}}</span> <i class="pull-right fa fa-pencil fs-1 m-2"></i></h3>
	                                    <h6 class="c-primary lh-5"></h6>
	                                    <br><br>
	                                    <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{ URL_FEEDBACK_SEND }}">
	                                        <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
	                                            <i class="icon-directions fs-4"></i>
	                                        </span>
	                                        <span class="d-inline-block align-top lh-7">
	                                            <span class="fs-6 d-block">{{ getPhrase('view_all')}}</span>
	                                            <span class="c-gray fs-7">Click to View more details</span>
	                                        </span>
	                                        <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
	                                    </a>
	                                </div>
	                            </div>
	                        </div>
	                        <!--End Panel-->
                    </section>




                </div>
            </div>
           
        </section>
    </div>


@stop

@section('footer_scripts')
  
 
@stop
