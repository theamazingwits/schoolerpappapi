@extends($layout)
@section('header_scripts')
{!! Charts::assets() !!}
<link href="{{CHAT}}/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
<link href="{{CHAT}}/flaticon/flaticon.css" rel="stylesheet" type="text/css" />
<link href="{{CHAT}}/flaticon2/flaticon.css" rel="stylesheet" type="text/css" />
<link href="{{CHAT}}/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<!--end:: Vendor Plugins -->
<link href="{{CHAT}}/style.bundle.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">
<script src="{{CHAT}}/socket.io.js" type="text/javascript"></script>
@stop
<!-- $leaveapplystaff = []; -->
@section('content')
	<style>
		.customscroll1{
			max-height: 36em;
			min-height: 36em;
			overflow-y: hidden;
		}
		.customscroll1:hover{
			overflow-y: scroll;
		}
		.customscroll1::-webkit-scrollbar {
		    width: 0.5em;
		}
		 
		.customscroll1::-webkit-scrollbar-track {
		    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
		}
		 
		.customscroll1::-webkit-scrollbar-thumb {
		  background-color: darkgrey;
		  outline: 1px solid slategrey;
		}
		.customscroll{
			max-height: 35em;
			min-height: 35em;
			overflow-y: hidden;
		}
		.customscroll:hover{
			overflow-y: scroll;
		}
		.customscroll::-webkit-scrollbar {
		    width: 0.5em;
		}
		 
		.customscroll::-webkit-scrollbar-track {
		    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
		}
		 
		.customscroll::-webkit-scrollbar-thumb {
		  background-color: darkgrey;
		  outline: 1px solid slategrey;
		}
		
	</style>
	
	<div class="kt-grid kt-grid--hor kt-grid--root chatmargin" >
		 <div style = "margin-top: 5em;margin-left: 40em;">{!! $qrcode !!}</div>

		<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
			<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

				<!-- begin:: Content -->
				<div class="kt-container  kt-grid__item kt-grid__item--fluid">

					<!--Begin::App-->
					<div class="kt-grid kt-grid--desktop kt-grid--ver kt-grid--ver-desktop kt-app">

						<!--Begin:: App Aside Mobile Toggle-->
						<button class="kt-app__aside-close" id="kt_chat_aside_close">
							<i class="la la-close"></i>
						</button>

						<!--End:: App Aside Mobile Toggle-->

						<!--Begin:: App Aside-->

						<div class="kt-grid__item kt-app__toggle kt-app__aside kt-app__aside--lg kt-app__aside--fit" id="kt_chat_aside">
							<div class="kt-chat">
								<div class="kt-portlet kt-portlet--head-lg kt-portlet--last">
									<div class="kt-portlet__head">
										<div class="kt-chat__head ">
											<div class="kt-chat__left">
												<img src="{{CHAT}}/assets/media/users/300_12.jpg" style="width:5em;height: 5em;border-radius: 5em;"> 
											</div>
											<div class="kt-chat__center">
												<div class="kt-chat__label">
													<a href="#" class="kt-chat__title">{{$user->name}}</a>
													<!-- <p id="groupid" style="display: none;"></p> -->
													<span class="kt-chat__status">
														<span class="kt-badge kt-badge--dot kt-badge--success"></span> Active
													</span>
												</div>
												<div class="kt-chat__pic kt-hidden">
													<span class="kt-media kt-media--sm kt-media--circle" data-toggle="kt-tooltip" data-placement="right" title="Jason Muller" data-original-title="Tooltip title">
														<img src="{{CHAT}}/assets/media/users/300_12.jpg" alt="image">
													</span>
													<span class="kt-media kt-media--sm kt-media--circle" data-toggle="kt-tooltip" data-placement="right" title="Nick Bold" data-original-title="Tooltip title">
														<img src="{{CHAT}}/assets/media/users/300_11.jpg" alt="image">
													</span>
													<span class="kt-media kt-media--sm kt-media--circle" data-toggle="kt-tooltip" data-placement="right" title="Milano Esco" data-original-title="Tooltip title">
														<img src="{{CHAT}}/assets/media/users/100_14.jpg" alt="image">
													</span>
													<span class="kt-media kt-media--sm kt-media--circle" data-toggle="kt-tooltip" data-placement="right" title="Teresa Fox" data-original-title="Tooltip title">
														<img src="{{CHAT}}/assets/media/users/100_4.jpg" alt="image">
													</span>
												</div>
											</div>
											<div class="kt-chat__right">
												<div class="dropdown dropdown-inline">
													<button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														<i class="flaticon2-add-1"></i>
													</button>
													<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-md">

														<!--begin::Nav-->
														<ul class="kt-nav">
															<li class="kt-nav__head">
																Messaging
																<i class="flaticon2-information" data-toggle="kt-tooltip" data-placement="right" title="Click to learn more..."></i>
															</li>
															<li class="kt-nav__separator"></li>
															<li class="kt-nav__item">
																<a href="javascript:void(0);" class="kt-nav__link" data-toggle="modal" data-target="#groupchatmodal">
																	<i class="kt-nav__link-icon flaticon2-group"></i>
																	<span class="kt-nav__link-text">New Group</span>
																</a>
															</li>
															<li class="kt-nav__item">
																<a href="javascript:void(0);" class="kt-nav__link"data-toggle="modal" data-target="#individualchatmodal">
																	<i class="kt-nav__link-icon flaticon2-open-text-book"></i>
																	<span class="kt-nav__link-text">New Chat</span>
																</a>
															</li>
															<!-- <li class="kt-nav__item">
																<a href="#" class="kt-nav__link">
																	<i class="kt-nav__link-icon flaticon2-bell-2"></i>
																	<span class="kt-nav__link-text">Calls</span>
																</a>
															</li>
															<li class="kt-nav__item">
																<a href="#" class="kt-nav__link">
																	<i class="kt-nav__link-icon flaticon2-dashboard"></i>
																	<span class="kt-nav__link-text">Settings</span>
																</a>
															</li>
															<li class="kt-nav__item">
																<a href="#" class="kt-nav__link">
																	<i class="kt-nav__link-icon flaticon2-protected"></i>
																	<span class="kt-nav__link-text">Help</span>
																</a>
															</li>
															<li class="kt-nav__separator"></li>
															<li class="kt-nav__foot">
																<a class="btn btn-label-brand btn-bold btn-sm" href="#">Upgrade plan</a>
																<a class="btn btn-clean btn-bold btn-sm" href="#" data-toggle="kt-tooltip" data-placement="right" title="Click to learn more...">Learn more</a>
															</li> -->
														</ul>

														<!--end::Nav-->
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--begin::Portlet-->
							<div class="kt-portlet kt-portlet--last">
								<div class="kt-portlet__body">
									<div class="kt-searchbar">
										<div class="input-group">
											<div class="input-group-prepend"><!-- <span class="input-group-text" id="basic-addon1"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
												<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<rect x="0" y="0" width="24" height="24" />
													<path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
													<path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero" />
												</g>
											</svg></span> -->
												
											</div>
											<!-- <input type="text" class="form-control" placeholder="Search" aria-describedby="basic-addon1"> -->
											<a href="#" style="margin-left: 8em;" id="leaveapplybutton" data-toggle="modal" data-target="#leaveapplicationmodal"><h1 style="font-weight: bold;font-size: 1.5em;"><i class="flaticon2-fast-back"></i> Apply for Leave <i class="flaticon2-fast-next"></i></h1></a>
										</div>
									</div>
									<div class="kt-widget kt-widget--users kt-mt-20">
										<p id="user" style="display: none;">{{$user}}</p>
										<div class="kt-scroll kt-scroll--pull">
											<div class="kt-widget__item"> 
												<span class="kt-media kt-media--circle"> 
													<img src="{{CHAT}}/assets/media/users/300_9.jpg" alt="image"> 
												</span>
												<div class="kt-widget__info">
													<div class="kt-widget__section"> 
														<a href="javascript:void(0);" class="kt-widget__username" onclick="viewchat()">Leave Aplications</a> 
														<span class="kt-badge kt-badge--success kt-badge--dot"></span>
													</div> 
													<span class="kt-widget__desc"> </span>
												</div>
												<div class="kt-widget__action"> <span class="kt-widget__date">36 Mines</span></div>
											</div>
										</div>
										<div class="kt-scroll kt-scroll--pull customscroll1" id="chatlist">
										</div>
									</div>
								</div>
							</div>

							<!--end::Portlet-->
						</div>

						<!--End:: App Aside-->
						<!--Begin:: App Content-->
						<div class="kt-grid__item kt-grid__item--fluid kt-app__content" id="kt_chat_content1">
							<div class="kt-chat">
								<div class="kt-portlet kt-portlet--head-lg kt-portlet--last">
									<div class="kt-portlet__head">
										<div class="kt-chat__head ">
											<div class="kt-chat__left">
											</div>
										</div>
									</div>
									<div class="kt-portlet__body">
										<div class="kt-scroll kt-scroll--pull" data-mobile-height="300" style="min-height: 46.3em;max-height: 46.3em;">
											<div class="row">
												<div class="col-md-12" style="margin-left:12em;margin-top: -5em;">
													<img src="{{CHAT}}/icon.png" style="font-size: 2em;color: black;">
												</div>
												<div class="col-md-12" style="margin-left: 12em;margin-top: -11em;">
													<p style="font-size: 2em;color: black;">Please Select a chat to start messaging</p>
												</div>

											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--Begin:: App Content-->
						<div class="kt-grid__item kt-grid__item--fluid kt-app__content" id="kt_chat_content">
							<div class="kt-chat">
								<div class="kt-portlet kt-portlet--head-lg kt-portlet--last">
									<div class="kt-portlet__head">
										<div class="kt-chat__head ">
											<div class="kt-chat__left">

											</div>
											<div class="kt-chat__center">
												<div class="kt-chat__label">
													<a href="javascript:void(0);" class="kt-chat__title" id="chattitle"></a>
													<p id="groupid" style="display: none;"></p>
													<p id="type" style="display: none;"></p>
													<span class="kt-chat__status">
														<span class="kt-badge kt-badge--dot kt-badge--success"></span> Active
													</span>
												</div>
												<div class="kt-chat__pic kt-hidden">
													<span class="kt-media kt-media--sm kt-media--circle" data-toggle="kt-tooltip" data-placement="right" title="Jason Muller" data-original-title="Tooltip title">
														<img src="{{CHAT}}/assets/media/users/300_12.jpg" alt="image">
													</span>
													<span class="kt-media kt-media--sm kt-media--circle" data-toggle="kt-tooltip" data-placement="right" title="Nick Bold" data-original-title="Tooltip title">
														<img src="{{CHAT}}/assets/media/users/300_11.jpg" alt="image">
													</span>
													<span class="kt-media kt-media--sm kt-media--circle" data-toggle="kt-tooltip" data-placement="right" title="Milano Esco" data-original-title="Tooltip title">
														<img src="{{CHAT}}/assets/media/users/100_14.jpg" alt="image">
													</span>
													<span class="kt-media kt-media--sm kt-media--circle" data-toggle="kt-tooltip" data-placement="right" title="Teresa Fox" data-original-title="Tooltip title">
														<img src="{{CHAT}}/assets/media/users/100_4.jpg" alt="image">
													</span>
												</div>
											</div>
											<div class="kt-chat__right">
												<div class="dropdown dropdown-inline" >
													<button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														<i class="flaticon-more"></i>
													</button>
													<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-md" style="margin-left: -15em;">

														<!--begin::Nav-->
														<ul class="kt-nav">
															<!-- <li class="kt-nav__head">
																Chat Options
																<i class="flaticon2-information" data-toggle="kt-tooltip" data-placement="right" title="Click to learn more..."></i>
															</li> -->
															<li class="kt-nav__item">
																<a href="javascript:void(0);" class="kt-nav__link" onclick="viewgroup();">
																	<i class="kt-nav__link-icon flaticon-information"></i>
																	<span class="kt-nav__link-text">Chat Info</span>
																</a>
															</li>
															<li class="kt-nav__item" id="eventoptionmenu">
																<a href="javascript:void(0);" class="kt-nav__link"data-toggle="modal" data-target="#eventmodal">
																	<i class="kt-nav__link-icon flaticon2-calendar-7"></i>
																	<span class="kt-nav__link-text">Create Event</span>
																</a>
															</li>
															<!-- <li class="kt-nav__item">
																<a href="#" class="kt-nav__link">
																	<i class="kt-nav__link-icon flaticon2-bell-2"></i>
																	<span class="kt-nav__link-text">Calls</span>
																</a>
															</li>
															<li class="kt-nav__item">
																<a href="#" class="kt-nav__link">
																	<i class="kt-nav__link-icon flaticon2-dashboard"></i>
																	<span class="kt-nav__link-text">Settings</span>
																</a>
															</li>
															<li class="kt-nav__item">
																<a href="#" class="kt-nav__link">
																	<i class="kt-nav__link-icon flaticon2-protected"></i>
																	<span class="kt-nav__link-text">Help</span>
																</a>
															</li>
															<li class="kt-nav__separator"></li>
															<li class="kt-nav__foot">
																<a class="btn btn-label-brand btn-bold btn-sm" href="#">Upgrade plan</a>
																<a class="btn btn-clean btn-bold btn-sm" href="#" data-toggle="kt-tooltip" data-placement="right" title="Click to learn more...">Learn more</a>
															</li> -->
														</ul>

														<!--end::Nav-->
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="kt-portlet__body">
										<div class="kt-scroll kt-scroll--pull customscroll" data-mobile-height="300">
											<div class="kt-chat__messages" id="chatmessages">
											</div>
										</div>
									</div>
									<div class="kt-portlet__foot" id="textfoot">
										<div class="kt-chat__input">
											<div class="kt-chat__editor">
												<textarea style="height: 50px;color:black;font-size: 1em;" placeholder="Type here..." id="message"></textarea>
											</div>
											<div class="kt-chat__toolbar">
												<div class="kt_chat__tools">
													<a href="javascript:void(0);" onclick="trigger()"><i class="flaticon2-photograph"></i></a>
													<a href="#"><i class="flaticon2-photo-camera"></i></a>
													<input type="file"  style="display: none;" id="chatfile" onchange="fileget(event)" accept="image/png, image/jpeg" />
												</div>
												<div class="kt_chat__actions">
													<button type="button" class="btn btn-brand btn-md btn-upper btn-bold kt-chat__reply" onclick="newmessage();">reply</button>
												</div>
											</div>
										</div>
									</div>
									<div class="kt-portlet__foot" id="imgfoot">
										<div class="kt-chat__input">
											<!-- <div class="kt-chat__editor">
												<textarea style="height: 50px;color:black;font-size: 1em;" placeholder="Type here..." id="message"></textarea>
											</div> -->
											<div class="kt-chat__toolbar">
												<div class="kt_chat__tools">
													<img src="{{CHAT}}/assets/media/users/100_14.jpg"/ style="height: 6.5em;margin-left: 25em;width: auto;" id="chatfileview">
												</div>
												<div class="kt_chat__actions">
													<button type="button" class="btn btn-brand btn-md btn-upper btn-bold kt-chat__reply" onclick="newmediamessage();">upload</button>
													<button type="button" class="btn btn-brand btn-md btn-upper btn-bold kt-chat__reply" onclick="cancelfile();">Cancel</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<!--End:: App Content-->
					</div>

					<!--End::App-->
				</div>

				<!-- end:: Content -->
			</div>
		</div>
		<div class="modal fade" id="groupchatmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		    <div class="modal-dialog modal-sm" role="document">
		        <div class="modal-content" style="width: 50em;margin-left: -15em;">
		            <div class="modal-header text-center">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		                <h4 class="modal-title" id="myModalLabel">{{getPhrase('create group')}}</h4>
		            </div>
		            <div class="modal-body">
		            	<div class="row">
		            		<p id="members" style="display: none;"></p>
		            		<div class= "col-md-6">
		            			<input type="text" class="form-control" id="groupname" placeholder="Group Name">
		            		</div>
		            		<div class="col-md-6">
		            			<select class = "form-control" id="classlist" onChange="getStudents()" >
		            				<option value="">Select Class</option>
		            				@foreach($classes as $classlist)
		            				<option value="{{$classlist->id}}">{{$classlist->class}}</option>
		            				@endforeach
		            			</select>
		            		</div>
		            	</div>
		    		</div>
		        	<div class="modal-footer text-center">
                    <button type="button" class="btn btn-success button btn-lg"  onclick="createGroup();">{{getPhrase('create')}}</button>
                	</div>        
		        </div>
                
            </div>
        </div>
        <div class="modal fade" id="leaveapplicationmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		    <div class="modal-dialog modal-sm" role="document">
		        <div class="modal-content" style="width: 50em;margin-left: -15em;">
		            <div class="modal-header text-center">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		                <h4 class="modal-title" id="eventmodal">{{getPhrase('create event')}}</h4>
		            </div>
		            <div class="modal-body">
		            	<div class="row">
		            		<div class="col-md-6">
		            			<select class = "form-control" id="leavestudent" onChange="getStaffs()" >
		            				<option value="" selected hidden>Select Children</option>
		            				@foreach($students as $studentlist)
		            				<option value="{{$studentlist->id}}">{{$studentlist->name}}</option>
		            				@endforeach
		            			</select>
		            		</div>
		            		<div class="col-md-6">
		            			<select class = "form-control" id="leaveapplystaff" onChange="getStaffs()" >
		            				<option value="" selected hidden>Select Children</option>
		            			</select>
		            		</div>
		            		<!-- <div class= "col-md-12">
		            			<input type="text" class="form-control" id="eventname" placeholder="Event Name">
		            		</div> -->
		            	</div><br>
		            	<div class="row">
		            		<div class="col-md-6">
		            			<div class="input-group date">
									<input type="text" class="form-control" readonly placeholder="Select event start date & time" id="eventstartdate" />
									<div class="input-group-append">
										<span class="input-group-text">
											<i class="la la-calendar-check-o"></i>
										</span>
									</div>
								</div>
		            		</div>
		            		<div class="col-md-6">
		            			<div class="input-group date">
									<input type="text" class="form-control" readonly placeholder="Select event end date & time" id="eventenddate" />
									<div class="input-group-append">
										<span class="input-group-text">
											<i class="la la-calendar-check-o"></i>
										</span>
									</div>
								</div>
		            		</div>
		            	</div><br>
		            	<div class="row">
		            		<div class= "col-md-12">
		            			<input type="text" class="form-control" id="eventdes" placeholder="Event Description">
		            		</div>
		            	</div><br>
		    		</div>
		        	<div class="modal-footer text-center">
                    <button type="button" class="btn btn-success button btn-lg"  onclick="createevent();">{{getPhrase('create')}}</button>
                	</div>        
		        </div>
                
            </div>
        </div>
        <div class="modal fade" id="eventmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		    <div class="modal-dialog modal-sm" role="document">
		        <div class="modal-content" style="width: 50em;margin-left: -15em;">
		            <div class="modal-header text-center">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		                <h4 class="modal-title" id="eventmodal">{{getPhrase('create event')}}</h4>
		            </div>
		            <div class="modal-body">
		            	<div class="row">
		            		<div class= "col-md-12">
		            			<input type="text" class="form-control" id="eventname" placeholder="Event Name">
		            		</div>
		            	</div><br>
		            	<div class="row">
		            		<div class="col-md-6">
		            			<div class="input-group date">
									<input type="text" class="form-control" readonly placeholder="Select event start date & time" id="eventstartdate" />
									<div class="input-group-append">
										<span class="input-group-text">
											<i class="la la-calendar-check-o"></i>
										</span>
									</div>
								</div>
		            		</div>
		            		<div class="col-md-6">
		            			<div class="input-group date">
									<input type="text" class="form-control" readonly placeholder="Select event end date & time" id="eventenddate" />
									<div class="input-group-append">
										<span class="input-group-text">
											<i class="la la-calendar-check-o"></i>
										</span>
									</div>
								</div>
		            		</div>
		            	</div><br>
		            	<div class="row">
		            		<div class= "col-md-12">
		            			<input type="text" class="form-control" id="eventdes" placeholder="Event Description">
		            		</div>
		            	</div><br>
		    		</div>
		        	<div class="modal-footer text-center">
                    <button type="button" class="btn btn-success button btn-lg"  onclick="createevent();">{{getPhrase('create')}}</button>
                	</div>        
		        </div>
                
            </div>
        </div>
        <div class="modal fade" id="individualchatmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		    <div class="modal-dialog modal-sm" role="document">
		        <div class="modal-content" style="width: 50em;margin-left: -15em;">
		            <div class="modal-header text-center">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		                <h4 class="modal-title" id="myModalLabel">{{getPhrase('create chat')}}</h4>
		            </div>
		            <div class="modal-body">
		            	<div class="row">
		            		<p id="members1" style="display: none;"></p>
		            		<div class="col-md-12">
		            			<select class = "form-control" id="classlist1" onChange="getStudents1()" >
		            				<option value="">Select Class</option>
		            				@foreach($classes as $classlist)
		            				<option value="{{$classlist->id}}">{{$classlist->class}}</option>
		            				@endforeach
		            			</select>
		            		</div>
		            	</div><br>
		            	<div class="row">
		            		<div class="col-md-6">
			            		<div class="kt-portlet">
									<div class="kt-portlet__head">
										<div class="kt-portlet__head-label">
											<h3 class="kt-portlet__head-title">
												Student Lists
											</h3>
										</div>
									</div>
									<div class="kt-portlet__body">
										<div class="kt-notification-v2" id="chatstudentlist">
										</div>
										<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
										<!--end::Dropdown-->
									</div>
								</div>
							</div>
							<div class="col-md-6">
			            		<div class="kt-portlet">
									<div class="kt-portlet__head">
										<div class="kt-portlet__head-label">
											<h3 class="kt-portlet__head-title">
												Parent Lists
											</h3>
										</div>
									</div>
									<div class="kt-portlet__body">
										<div class="kt-notification-v2" id="chatparentlist">
										</div>
										<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
										<!--end::Dropdown-->
									</div>
								</div>
							</div>
		            	</div>
		    		</div>
		        	<div class="modal-footer text-center">
                    <button type="button" class="btn btn-success button btn-lg"  onclick="createGroup();">{{getPhrase('create')}}</button>
                	</div>        
		        </div>
                
            </div>
        </div>

        <div class="modal fade" id="chatinfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		    <div class="modal-dialog modal-sm" role="document">
		        <div class="modal-content" style="width: 50em;margin-left: -15em;">
		            <div class="modal-header text-center">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		                <h4 class="modal-title" id="myModalLabel">{{getPhrase('chat info')}}</h4>
		            </div>
		            <div class="modal-body">
		            	<div class="row">
		            		<div class="col-md-12">
			            		<div class="kt-portlet kt-portlet--bordered-semi kt-portlet--height-fluid">
									<div class="kt-portlet__head">
										<div class="kt-portlet__head-label">
											<h3 class="kt-portlet__head-title">
												Group Members
											</h3>
										</div>
									</div>
									<div class="kt-portlet__body">
										<div class="kt-widget4" id="chatmembers">
										</div>
									</div>
								</div>
							</div>
		            	</div>
		    		</div>
		        </div>
                
            </div>
        </div>
    </div>

		<!-- end:: Footer -->
	</div>
</div>
</div>

@stop
@section('footer_scripts')
 @include('chat.chat')
@stop