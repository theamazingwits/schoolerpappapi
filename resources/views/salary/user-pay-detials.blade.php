@extends($layout)
@section('header_scripts')
<link href="{{CSS}}ajax-datatables.css" rel="stylesheet">

@stop
@section('content')

<div id="page-wrapper">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
	    <div class="container-fluid content-wrap">
			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>
	                      
	                    @if(checkRole(getUserGrade(1)))

						<li><a href="{{URL_PAY_SALARY}}">{{getPhrase('select_staff')}}</a> </li>
						<li><a href="{{URL_VIEW_USER_PAYS.$user->slug}}">{{ucwords($user->name)}} {{getPhrase('payments')}}</a> </li>

						@elseif(checkRole(getUserGrade(11)))

						<li><a href="{{URL_STAFF_DETAILS.$user->slug}}">{{ucwords($record->name)}} Details</a> </li>

						@endif

						<li>{{ ucwords(getRole($user->id)).' - '.$record->month.' Payment Details' }}</li>
					</ol>
				</div>
			</div>
			<!-- /.row -->
			<section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item">
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                        <h2 class="pull-left"> {{ $title }}  
                        </h2>
                      	<!--Start panel icons-->
                  		<div class="panel-icons panel-icon-slide bgc-white-dark">

                            <ul>
                                <li><a href=""><i class="fa fa-angle-left"></i></a>
                                  <ul>
                                      <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                                      <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                                      <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
                                      <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
                                      <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li>
                                      <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li>
                                  </ul>
                              </li>
                          </ul>
                      	</div>
                      <!--End panel icons-->
                    </div>
                    <div class="panel-body panel-body-p">
	                  	<div class="profile-details text-center">
							<div class="profile-img"><img src="{{ getProfilePath($user->image,'profile')}}" alt=""></div>

							<div class="aouther-school">

								<h2>{{ $user->name}}</h2>
								<p><span>{{$user->email}}</span></p>
								
							</div>
					    </div>
					    <hr>
						<div class="row">
							<div class="col-md-6">
		                      	<h3>{{getPhrase('salary_grade_details')}}</h3>
						   		<table class="table table-striped table-bordered datatable" cellspacing="0" width="100%">
									<tbody>
										<tr>
											<td><b>{{getPhrase('salary_grade')}}</b></td>
											<td>{{ ucwords($template->salary_grades) }}</td>
											
										  
										</tr>

										<tr>
											<td><b>{{getPhrase('basic_salary')}}</b></td>
											<td>{{getCurrencyCode()}} {{ $template->basic_salary }}</td>
										</tr>

										<tr>
											<td><b>{{getPhrase('total_allowances')}}</b></td>
											<td>{{getCurrencyCode()}} {{ $total_allowance }}</td>
										</tr>

											<tr>
											<td><b>{{getPhrase('total_deductions')}}</b></td>
											<td>{{getCurrencyCode()}} {{ $total_deductions }}</td>
										</tr>

										<tr>
											<td><b>{{getPhrase('over_time_rate')}}</b></td>
											<td>{{getCurrencyCode()}} {{ $template->overtime_rate }}</td>
										</tr>

									     <tr>

											<td><b>{{getPhrase('date')}}</b></td>
											<td>{{ $record->month }}</td>

										 </tr>

										  <tr>

											<td><b>{{getPhrase('payment_method')}}</b></td>
											<td>{{ ucwords($record->payment_method) }}</td>

										 </tr>

										 <tr>

											<td><b>{{getPhrase('comments')}}</td>
											<td>{{ ucwords($record->comments) }}</td>

										 </tr>

									</tbody>
							   	</table>
						 	</div>
						  	<div class="col-md-6">
		                       	<h3>{{getPhrase('salary_details')}}</h3>
							  	<table class="table table-striped table-bordered datatable" cellspacing="0" width="100%">
									
									<tbody>
										<tr>
											<td><b>{{getPhrase('gross_salary')}}</b></td>
											<td>{{getCurrencyCode()}} {{ ucwords($template->gross_salary) }}</td>
											
										  
										</tr>

										<tr>
											<td><b>{{getPhrase('total_deduction')}}</b></td>
											<td>{{getCurrencyCode()}} {{ $template->total_deduction }}</td>
										</tr>

										<tr>
											<td><b>{{getPhrase('net_salary')}}</b></td>
											<td>{{getCurrencyCode()}} {{ $template->net_salary }}</td>
										</tr>

									     <tr>

											<td><b>{{getPhrase('other_deductions')}}</b></td>
											<td>{{getCurrencyCode()}} {{ $record->net_salary - $record->paid_amount }}</td>

										 </tr>

										  <tr>

											<td><b>{{getPhrase('total_paid')}}</b></td>
											<td>{{getCurrencyCode()}} {{ ucwords($record->paid_amount) }}</td>

										 </tr>
									</tbody>
							   	</table>
						 	</div>
		               	</div>
					</div>
				</div>
			</section>
		</div>
		<!-- /.container-fluid -->
	</section>
</div>
@stop
 


