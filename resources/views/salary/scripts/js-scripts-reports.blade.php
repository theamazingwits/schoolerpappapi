
@include('common.angular-factory')
<script>

 
     app.controller('salaryReports', function ($scope, $http, httpPreConfig) {

      $scope.selected_branch_id  = 0;
      $scope.selected_role_id    = 0;
      $scope.selected_date       = 0;
      mydate  = 0;

       $scope.branchChanged  = function(branch_id){
           
           $scope.selected_branch_id   = branch_id;
           
           if( $scope.selected_role_id && mydate )
               $scope.doCall(mydate);
    }

     $scope.staffChanged  = function(role_id){
           
           $scope.selected_role_id   = role_id;

              if(  mydate )
               $scope.doCall(mydate);
           
    }

    $("#txtDate").datepicker({

        dateFormat: 'yy-mm-dd',
        maxDate: '0',
        onSelect: function (selectedDate) {

            mydate   = selectedDate;
          
           if( $scope.selected_role_id  )
               $scope.doCall(mydate);

            $scope.$digest();
        }

    });


    $scope.doCall  = function(mydate){

              route = '{{ URL_GET_SALARY_REPORTS_OF_MONTH }}',

             data  = {
                     _method: 'post',
                     '_token':httpPreConfig.getToken(),
                     'role_id': $scope.selected_role_id,
                     'mydate': mydate

                   };


               httpPreConfig.webServiceCallPost(route, data).then(function(result){

               users = [];
              
               angular.forEach(result.data, function(value, key) {
                 
                 users.push(value);
              })

               $scope.result_data = users;
               // console.log($scope.result_data);
          });

    }


 $scope.printIt = function(){
   
   $('#pay_slip').val('no');   
   $('#salary_payid').val(0);  
   $('#htmlform').submit();
}

$scope.printPaySlip = function(sal_payid){
   
   $('#pay_slip').val('yes');  
   $('#salary_payid').val(sal_payid);  
   $('#htmlform').submit();
}

    
 
});


  app.filter('capitalizeWord', function() {
    return function(text) {
      return (!!text) ? text.charAt(0).toUpperCase() + text.substr(1).toLowerCase() : '';
    }
});
  
</script>