@extends($layout)


@section('content')
<div id="page-wrapper" ng-controller="paySalary">
    <section id="main" class="main-wrap bgc-white-darkest" role="main">
        <div class="container-fluid content-wrap">
            <div class="row">
                <div class="col-lg-12">
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{PREFIX}}">
                                <i class="fa fa-home">
                                </i>
                            </a>
                        </li>
                         
                       
                        <li>
                            
                                {{$title}}
                            
                        </li>
                    </ol>
                </div>
            </div>
        

            
            <section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item " >
                    <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                        <h2 class="pull-left"> {{ $title }} </h2>
                        <!--End panel icons-->
                    </div>
                    <div class="panel-body panel-body-p packages">


                        {!! Form::open(array('url' => URL_PAYSALAY_GET_STAFF, 'method' => 'POST')) !!}

                        <fieldset class="form-group col-md-6 col-md-offset-3">

                            {{ Form::label ('role_id', getphrase('user_type')) }}
                            <span class="text-red">*</span>
                            {{ Form::select('role_id', $roles, '', 
                            [   
                                'class'     => 'form-control', 
                                "id"        => "select_branch",
                                "ng-model"  => "role_id",
                                'placeholder'=>getPhrase('select'),
                               "ng-change" => "getStaffParentCourses(role_id)"

                            ])}}
                        </fieldset>


                        <fieldset ng-if = "viewCourse == 1 " class="form-group col-md-6 col-md-offset-3">

                             <label for = "course_parent_id">{{getPhrase('parent_course')}}</label>
                             
                            <select 
                            name      = "course_parent_id" 
                            id        = "course_parent_id" 
                            class     = "form-control" 
                            ng-model  = "course_parent_id" 
                            ng-options= "option.id as option.course_title for option in parent_courses track by option.id">
                            <option value="">{{getPhrase('select')}}</option>
                            </select>
                        </fieldset>
                    </div>
                    <br>
                    <div class="text-center">
                        <button type="submit" class="btn button btn-lg btn-primary">
                            {{getPhrase('get_details')}}
                        </button>
                    </div>
                </div>
            </section>
        </div>
    </section>
</div>
   
{!! Form::close() !!}
@stop
 
 

@section('footer_scripts')
 
 <script src="{{JS}}moment.min.js"></script>

    @include('salary.scripts.js-scripts')
    
@stop