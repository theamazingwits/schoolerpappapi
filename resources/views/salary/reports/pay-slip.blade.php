<!DOCTYPE html>

<html>
<head>
 <title>PAYSLIP</title>
</head>

<body>
<div style="border:1px solid #111;margin:5px;border-radius:5px;margin-bottom:50px;padding-bottom: 80px;">
<table width="100%" cellspacing="0px" cellpadding="0px" style=" padding-top: 34px;">
  <tbody>
  
   <tr>
     <td rowspan="3"><img src="{{IMAGE_PATH_SETTINGS.getSetting('site_logo', 'site_settings')}}" alt="logo" style="width:180px;display:block;margin:0px auto;padding:0px 0px 50px;"></td>
     <td colspan="3" style="color:#1c66b1;text-align:left;font-size:15px;"><strong>{{getSetting('site_title', 'site_settings')}}</strong>
     </td>
   </tr>
   
   <tr>
     <td colspan="3" style="color:#bcbdb2;text-align:left;font-size:15px;"><strong>{!!getSetting('site_address', 'site_settings')!!}</strong></td>
   </tr>
   
   <tr>
     <td colspan="3" style="text-align:left;font-size:15px;padding:0px 0px 50px;"><strong>Phone NO:{{getSetting('site_phone', 'site_settings')}}</strong></td>
   </tr>
  </tbody>
   
</table>
<table style="width: 92%;
    /* margin: 33px; */
    margin: 0 auto;margin-bottom: 32px;
    border: 1px solid grey;
    border-radius: 5px;">


  
    <tbody>
     
     <h2 style="text-align:center;margin:10px;margin-left:30px !important;">Payslip for {{$title}}</h2>
       <tr>
        <td style="border-bottom: 1px solid grey;border-right: 1px solid grey;    padding: 5px;"><b>Employee Name</b></td>
        <td style="border-bottom: 1px solid grey;border-right: 1px solid grey;    padding: 5px;">{{ucwords($user->name)}}</td>
        <td style="border-bottom: 1px solid grey;border-right: 1px solid grey;    padding: 5px;"><b>Employee ID</b></td>
        <td style="border-bottom: 1px solid grey;padding: 5px;">{{$user->employee_id}}</td>
      </tr>
      <tr>
        <td style="border-bottom: 1px solid grey;border-right: 1px solid grey;    padding: 5px;"><b>Designation</b></td>
        <td style="border-bottom: 1px solid grey;border-right: 1px solid grey;    padding: 5px;">{{$role_name}}</td>
        <td style="border-bottom: 1px solid grey;border-right: 1px solid grey;    padding: 5px;"><b>Payable Days</b></td>
        <td style="border-bottom: 1px solid grey;padding: 5px;">{{$total_days}}</td>
      </tr>
       <tr>
        <td style="padding: 5px;"></td>
         <td style="padding: 5px;"></td>
     <td style="padding: 5px;"></td>
        <td style="padding: 5px;"></td>
      </tr>
        <tr>
        <td style="padding: 5px;"></td>
         <td style="padding: 5px;"></td>
     <td style="padding: 5px;"></td>
        <td style="padding: 5px;"></td>
      </tr>

        <tr>
        <td style="border-bottom: 1px solid grey; padding: 5px;"></td>
        <td style="border-bottom: 1px solid grey;padding: 5px;"></td>
        <td style="border-bottom: 1px solid grey;padding: 5px;"></td>
        <td style="border-bottom: 1px solid grey;padding: 5px;"></td>
      </tr>
       <tr>
        <td style="border-bottom:1px solid grey;text-align:center;padding: 5px;font-weight: 700 !important; ">Allowances</td>
        <td style="border-right:1px solid grey;border-bottom:1px solid grey;    padding: 5px;"></td>
        <td style="border-bottom:1px solid grey;text-align:center;font-weight: 700 !important;    padding: 5px;">Deductions</td>
        <td style="border-bottom:1px solid grey;    padding: 5px;"></td>
      </tr>


 <tr>

    <!-- Start of allowances -->
   <td style="padding: 0;margin:  0;border-bottom: 1px solid grey;">
      <table style="width: 143%;">
        <tbody>

           <tr>
                <td style="width:70%;"><b>Basic Salary</b></td>
                <td style="width: 29%;">{{$template->basic_salary}}</td>
            </tr>
  
           @foreach($allowances as $data1)   
                 
            <tr>
                <td style="width:70%;"><b>{{ucwords($data1->allowance_name)}}</b></td>
                <td style="width: 29%;">{{$data1->allowance_value}}</td>
            </tr>

            @endforeach

             


        
        </tbody>
        </table>
    </td>
     
      <td style="border-right: 1px solid grey;border-bottom: 1px solid grey;   padding: 5px 0;">
      </td>

      <!-- End of allowances -->
         
         <!-- Start of desuctions -->

   <td style="padding: 0;margin:  0;border-bottom: 1px solid grey;">
      <table style="width: 138%;">
        <tbody>

             @foreach($deductions as $data1)  
             <tr>
                <td style="width:73%;"><b>{{ucwords($data1->deduction_name)}}</b></td>
                <td style="width: 33%;">{{$data1->deduction_value}}</td>
            </tr>

             @endforeach
          
            @if(isset($other))
             <tr>
                <td><b>Unpaid Leaves</b></td>
                <td style="width: 33%;">{{$other}}</td>
            </tr>
            @endif
 
  
           
        </tbody>
        </table>
    </td>
     
      <td style="border-bottom: 1px solid grey;   padding: 5px 0;">
      </td>
      <!-- End of deductions -->

      </tr>

     
       <tr>
        <td style="border-bottom: 1px solid grey;border-right: 1px solid grey;  padding: 5px;"><b>Total </b></td>
        <td style="border-bottom: 1px solid grey;border-right: 1px solid grey;    padding: 5px;">{{ $total_allowance + $template->basic_salary}}</td>
        <td style="border-bottom: 1px solid grey;border-right: 1px solid grey;   padding: 5px;"><b>Total </b></td>
        @if(isset($other))
        <td style="border-bottom: 1px solid grey;padding: 5px;">{{$total_deductions + $other}}</td>
        @else
        <td style="border-bottom: 1px solid grey;padding: 5px;">{{$total_deductions}}</td>
        @endif
      </tr>
      
       <tr>
        <td style="padding: 5px;"><b>Net Pay</b></td>
        <td style=" padding: 5px;"></td>
        <td style=" padding: 5px;"></td>
        <td style=" padding: 5px;"><b>{{getCurrencyCode()}} {{$record->paid_amount}}<b></td>
      </tr>
    </tbody>
  </table>
<h3 style="text-align:right;margin-right:40px;margin-top:80px;">Signature</h3>
</div>
 
</body>
</html>