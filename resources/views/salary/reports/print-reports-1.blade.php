<!DOCTYPE html>

<html>
 <head>
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
</head>
 
 <body onload="printMe()"  id="printableArea">


<h3><b>{{$title}}</b></h3><br/>
<div class="row vertical-scroll">
  
    <table style="border-collapse: collapse;">

    <thead>
        <th style="border:1px solid #000;">{{getPhrase('sno')}}</th>
        <th style="border:1px solid #000;" >{{getPhrase('name')}}</th>
        <th style="border:1px solid #000;">{{getPhrase('employee_id')}}</th>
        <th style="border:1px solid #000;">{{getPhrase('net_salary')}}</th>
        <th style="border:1px solid #000;">{{getPhrase('paid_amount')}}</th>
        <th style="border:1px solid #000;">{{getPhrase('payment_method')}}</th>
        <th style="border:1px solid #000;">{{getPhrase('paid_date')}}</th>
      
       
        
    </thead>
    <tbody>
    <?php $sno =1;?>
     @foreach($records as $record)
    <tr>
        
        <td style="border:1px solid #000;">{{$sno++}}</td>
        <td style="border:1px solid #000;">{{ ucfirst($record['name']) }}</td>
        <td style="border:1px solid #000;">{{$record['employee_id']}}</td>
        <td style="border:1px solid #000;">{{getCurrencyCode()}}{{$record['net_salary']}}</td>
        <td style="border:1px solid #000;">{{getCurrencyCode()}}{{$record['paid_amount']}}</td>
        <td style="border:1px solid #000;">{{ ucfirst($record['payment_method']) }}</td>
        <td style="border:1px solid #000;">{{$record['month']}}</td>
       
        
       
    </tr> 
    @endforeach
    </tbody>
    </table>
</div>


<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script>
    function printMe(){
        var printContents = document.getElementById('printableArea').innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = 
              "<html><head><title></title></head><body>" + 
              printContents + "</body>";;

     window.print();

     document.body.innerHTML = originalContents;
    };
</script> 


 </body>
</html>