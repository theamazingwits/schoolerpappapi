@extends($layout)

@section('header_scripts')
 <link href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" rel="Stylesheet"
        type="text/css" />   

@stop

@section('content')
<div id="page-wrapper" >
  <section id="main" class="main-wrap bgc-white-darkest" role="main">
    <div class="container-fluid content-wrap">
      <div class="row">
        <div class="col-lg-12">
          <ol class="breadcrumb">
            <li>
              <a href="{{PREFIX}}">
                <i class="fa fa-home">
                </i>
              </a>
            </li>


            <li>

              {{$title}}

            </li>
          </ol>
        </div>
      </div>
      
      {!! Form::open(array('url' => URL_PRINT_SALARY_REPORTS, 'method' => 'POST', 'name'=>'htmlform ','target'=>'_blank', 'id'=>'htmlform', 'novalidate'=>'')) !!}

      <section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item " ng-controller="salaryReports">
        <!--Start Panel-->
        <div class="panel bgc-white-dark" >
          <div class="panel-body panel-body-p packages">

             <div class="row">



              <fieldset  class="form-group col-md-4">

                {{ Form::label ('role_id', getphrase('staff_type')) }}
                <span class="text-red">*</span>
                {{ Form::select('role_id', $roles, '', 
                [   
                'class'     => 'form-control', 
                "id"        => "select_branch",
                "ng-model"  => "role_id",
                'placeholder'=>getPhrase('select'),
                'ng-change' => 'staffChanged(role_id)'

                ])}}
              </fieldset>

              <fieldset class="form-group col-md-4">


                {{ Form::label('pay_month', getphrase('pay_month')) }}

                <span class="text-red">*</span>

                <input type="text" name="pay_month" id="txtDate" class="form-control " placeholder="2018-05-12">


              </fieldset>


            </div>

            <div ng-show="result_data.length>0" class="row">

              <div class="col-sm-4 col-sm-offset-8">
                <div class="input-group">
                  <input type="text" ng-model="search" class="form-control input-lg" placeholder="{{getPhrase('search')}}" name="search" />
                  <!-- <span class="input-group-btn">
                    <button class="btn btn-primary btn-lg" type="button">
                      <i class="glyphicon glyphicon-search"></i>
                    </button>
                  </span> -->
                </div>
              </div>
            </div>

            <br>

            <div ng-if="result_data.length>0">

              <div class="row vertical-scroll">


                <table class="table table-bordered" style="border-collapse: collapse;">
                  <thead>
                    <th style="border:1px solid #000;text-align: center;"><b>{{getPhrase('sno')}}</b></th>
                    <th style="border:1px solid #000;text-align: center;"><b>{{getPhrase('name')}}</b></th>

                    <th style="border:1px solid #000;text-align: center;"><b>{{getPhrase('employee_id')}}</b></th>
                    <th style="border:1px solid #000;text-align: center;"><b>{{getPhrase('net_salary')}}</b></th>
                    <th style="border:1px solid #000;text-align: center;"><b>{{getPhrase('paid_amount')}}</b></th>
                    <th style="border:1px solid #000;text-align: center;"><b>{{getPhrase('payment_method')}}</b></th>
                    <th style="border:1px solid #000;text-align: center;"><b>{{getPhrase('paid_date')}}</b></th>
                    <th style="border:1px solid #000;text-align: center;"><b>{{getPhrase('pay_slip')}}</b></th>


                  </thead>
                  <tbody>

                    <tr ng-repeat="user in result_data | filter:search track by $index">


                     <td style="border:1px solid #000;text-align: center;" >@{{$index+1}}</td>
                     <td style="border:1px solid #000;text-align: center;">@{{user.name | capitalizeWord}}</td>

                     <td style="border:1px solid #000;text-align: center;">@{{user.employee_id}}</td>
                     <td style="border:1px solid #000;text-align: center;">{{getCurrencyCode()}} @{{user.net_salary}}</td>
                     <td style="border:1px solid #000;text-align: center;">{{getCurrencyCode()}} @{{user.paid_amount}}</td>
                     <td style="border:1px solid #000;text-align: center;">@{{user.payment_method | capitalizeWord }}</td>
                     <td style="border:1px solid #000;text-align: center;">@{{user.month}}</td>
                     <td style="border:1px solid #000;text-align: center;">
                      <a class="btn btn-info btn-sm panel-header-button" ng-click="printPaySlip(user.pay_id)">{{getPhrase('print_payslip')}}</a>
                    </td>

                  </tr> 

                </tbody>
              </table>
            </div>

            <input type="hidden" name="pay_slip" id="pay_slip">
            <input type="hidden" name="salary_payid" id="salary_payid">

            <br>
            <a ng-if="result_data.length!=0" class="btn btn-primary panel-header-button" ng-click="printIt()">Print</a>
          </div>

          <div ng-if="result_data.length==0" class="text-center" >{{getPhrase('no_data_available')}}</div> 


        </div>

      </div>


      {!! Form::close() !!}
    </section>


  </div>
</section>
</div>
   
@stop
 
 

@section('footer_scripts')
     
       <script src="{{JS}}moment.min.js"></script>

<script type="text/javascript" src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

    @include('salary.scripts.js-scripts-reports')


  
     <script>
        
     </script>
    
@stop