

<h3><b>{{$title}}</b></h3><br/>
<div class="row vertical-scroll">
  
    <table style="border-collapse: collapse;">

    <thead>
        <th style="border:1px solid #000;">{{getPhrase('sno')}}</th>
        <th style="border:1px solid #000;" >{{getPhrase('name')}}</th>
        <th style="border:1px solid #000;">{{getPhrase('employee_id')}}</th>
        <th style="border:1px solid #000;">{{getPhrase('net_salary')}}</th>
        <th style="border:1px solid #000;">{{getPhrase('paid_amount')}}</th>
        <th style="border:1px solid #000;">{{getPhrase('payment_method')}}</th>
        <th style="border:1px solid #000;">{{getPhrase('paid_date')}}</th>
      
       
        
    </thead>
    <tbody>
    <?php $sno =1;?>
     @foreach($records as $record)
    <tr>
        
        <td style="border:1px solid #000;">{{$sno++}}</td>
        <td style="border:1px solid #000;">{{ ucfirst($record['name']) }}</td>
        <td style="border:1px solid #000;">{{$record['employee_id']}}</td>
        <td style="border:1px solid #000;">{{getCurrencyCode()}}{{$record['net_salary']}}</td>
        <td style="border:1px solid #000;">{{getCurrencyCode()}}{{$record['paid_amount']}}</td>
        <td style="border:1px solid #000;">{{ ucfirst($record['payment_method']) }}</td>
        <td style="border:1px solid #000;">{{$record['month']}}</td>
       
        
       
    </tr> 
    @endforeach
    </tbody>
    </table>
</div>