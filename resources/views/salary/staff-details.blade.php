@extends($layout)
@section('header_scripts')
<link href="{{CSS}}ajax-datatables.css" rel="stylesheet">
@stop
@section('content')

<div id="page-wrapper">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
	    <div class="container-fluid content-wrap">
			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>
						<li><a href="{{URL_STAFF_DETAILS.$record->slug}}">{{ucwords($record->name)}} Details</a> </li>
						
						<li>{{ $title }}</li>
					</ol>
				</div>
			</div>
							
			<section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item">
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                        <h2 class="pull-left"> {{ $title }}  
                        </h2>
                      	<!--Start panel icons-->
                  		<div class="panel-icons panel-icon-slide bgc-white-dark">

                            <ul>
                                <li><a href=""><i class="fa fa-angle-left"></i></a>
                                  <ul>
                                      <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                                      <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                                      <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
                                      <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
                                      <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li>
                                      <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li>
                                  </ul>
                              </li>
                          </ul>
                      	</div>
                      <!--End panel icons-->
                    </div>
                    <div class="panel-body panel-body-p">
                    	
						<div > 
						<table class="table table-striped table-bordered datatable" cellspacing="0" width="100%">
							<thead>
								<tr>
									 
								   <th>{{getPhrase('date')}}</th>
									<th>{{getPhrase('net_salary')}}</th>
									<th>{{getPhrase('paid_amount')}}</th>
									<th>{{getPhrase('action')}}</th>
								  
								</tr>
							</thead>
							 
						</table>
						</div>

					</div>
				</div>
			</section>
		</div>
		<!-- /.container-fluid -->
	</section>
</div>
@endsection
 

@section('footer_scripts')
  
 @include('common.datatables', array('route'=>URL_STAFF_SALARY_HISTORY.$record->id, 'route_as_url' => TRUE))

@stop
