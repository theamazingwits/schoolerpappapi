@extends($layout)
@section('header_scripts')
<link href="{{CSS}}ajax-datatables.css" rel="stylesheet">
<link href="{{CSS}}plugins/datetimepicker/css/bootstrap-datetimepicker.css" rel="stylesheet">   

@stop
@section('content')

<div id="page-wrapper">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
	    <div class="container-fluid content-wrap">
			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>
						<li><a href="{{URL_PAY_SALARY}}">{{getPhrase('select_staff')}}</a> </li>
						<li>{{ $title }}</li>
					</ol>
				</div>
			</div>

			<!-- /.row -->
			<section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item " >
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                        <h2 class="pull-left"> {{getPhrase('add_payment_to')}} {{$role_name}} - {{ $title }} </h2>                        
                        <!--End panel icons-->
                    </div>
                    <div class="panel-body panel-body-p packages">

						<div class="row"> 


							
							<div class="col-md-4">

								{!! Form::open(array('url' => URL_ADD_SALARY_AND_EXPENSE, 'method' => 'POST', 'novalidate'=>'','name'=>'userPay ',)) !!}

								<div class="profile-details text-center">
									<div class="profile-img"><img src="{{ getProfilePath($record->image,'profile')}}" alt=""></div>

									<div class="aouther-school">

										<h2>{{ $record->name}}</h2>
										<p><span>{{$record->email}}</span></p>

									</div>

								</div>

								@if($template)

								<fieldset class="form-group col-md-12">

									{{ Form::label('gross_salary', getphrase('gross_salary')) }}

									{{ Form::text('gross_salary', $value = $template->gross_salary , $attributes = array(
									'class'=>'form-control',
									'readonly' =>TRUE
									)) }}

								</fieldset>

								<fieldset class="form-group col-md-12">

									{{ Form::label('total_deduction', getphrase('total_deduction')) }}

									{{ Form::text('total_deduction', $value = $template->total_deduction , $attributes = array(
									'class'=>'form-control',
									'readonly' =>TRUE
									)) }}

								</fieldset>


								<fieldset class="form-group col-md-12">

									{{ Form::label('net_salary', getphrase('net_salary')) }}

									{{ Form::text('net_salary', $value = $template->net_salary , $attributes = array(
									'class'=>'form-control',
									'readonly' =>TRUE
									)) }}

								</fieldset>

								@endif  


								<fieldset class="form-group col-md-12">


									{{ Form::label('month', getphrase('month')) }}

									<span class="text-red">*</span>

									{{ Form::text('month', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => 'Select month',

									'id' =>'dp',

									)) }}


								</fieldset>


								<fieldset class="form-group col-md-12">


									{{ Form::label('paid_amount', getphrase('pay_amount')) }}

									<span class="text-red">*</span>

									{{ Form::number('paid_amount', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => '25000',

									'ng-model'=>'paid_amount',

									'required'=> 'true',

									'min'=>0,


									'ng-class'=>'{"has-error": userPay.paid_amount.$touched && userPay.paid_amount.$invalid}',

									)) }}

									<div class="validation-error" ng-messages="userPay.paid_amount.$error" >

										{!! getValidationMessage()!!}


									</div>

								</fieldset>


								<fieldset class="form-group col-md-12">


									{{ Form::label('payment_method', getphrase('payment_method')) }}

									<span class="text-red">*</span> 

									{{Form::select('payment_method', $payment_methods,null, [

									'placeholder' => getPhrase('select_payment_method'),

									'class'=>'form-control',

									'ng-model'=>'payment_method',

									'id'=>'payment_method',

									'required'=> 'true', 

									'ng-class'=>'{"has-error": userPay.payment_method.$touched && userPay.payment_method.$invalid}'

									])}}

									<div class="validation-error" ng-messages="userPay.payment_method.$error" >

										{!! getValidationMessage()!!}

									</div>

								</fieldset>

								<fieldset class="form-group col-md-12">
									{{ Form::label('expense_category_id', getphrase('expense_category')) }}
									<span class="text-red">*</span>
									{{Form::select('expense_category_id', $expense_categories, null, ['class'=>'form-control', 'id'=>'expense_category_id', 'placeholder'=>getPhrase('select'),
									'ng-model'=>'expense_category_id',
									'required'=> 'true', 
									'ng-class'=>'{"has-error": userPay.expense_category_id.$touched && userPay.expense_category_id.$invalid}'
									])}}

									<div class="validation-error" ng-messages="userPay.expense_category_id.$error" >
										{!! getValidationMessage()!!}
									</div>
								</fieldset>

								<fieldset class="form-group col-md-12">

									{{ Form::label('comments', getphrase('comments')) }}

									{{ Form::textarea('comments', $value = null , $attributes = array('class'=>'form-control','rows'=>3, 'cols'=>'15', 'placeholder' => getPhrase('please_enter_your_comments'),


									)) }}

								</fieldset>
								<input type="hidden" name="user_id" value="{{ $record->id }}">
								{{-- <input type="hidden" name="branch_id" value="{{ $record->branch_id }}"> --}}
								<input type="hidden" name="role_id" value="{{ $record->role_id }}">
								<input type="hidden" name="employee_id" value="{{ $record->employee_id }}">
								<input type="hidden" name="template_id" value="{{ $template->id }}">

								<div class="buttons text-center">

									<button class="btn btn-lg btn-primary button" 

									ng-disabled='!userPay.$valid'>{{ getPhrase('add_payment')}}</button>

								</div>

								{!! Form::close() !!}

							</div>

							<div class="col-md-8">

								<table class="table table-striped table-bordered datatable" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th>{{getPhrase('date')}}</th>
											<th>{{getPhrase('net_salary')}}</th>
											<th>{{getPhrase('paid_amount')}}</th>
											<th>{{getPhrase('action')}}</th>

										</tr>
									</thead>

								</table>

							</div>



						</div>

					</div>
				</div>
			</section>
		</div>
		<!-- /.container-fluid -->
	</section>
</div>
@stop
 

@section('footer_scripts')

 @include('common.datatables', array('route'=>URL_GET_STAFF_SALARY_HISTORY.$record->id, 'route_as_url' => TRUE))
  @include('common.deletescript', array('route'=>URL_STAFF_SALARY_RECORD ))

  
@include('common.validations')

<script src="{{JS}}moment.min.js"></script>

  <script src="{{JS}}plugins/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
 <script>
    
     $(function () {
        $('#dp').datetimepicker({

               format: 'YYYY-MM-DD',
                maxDate: 'now'
            
            });
    });
 </script>


@stop
