@extends($layout)

@section('header_scripts')

<link href="{{CSS}}bootstrap-datepicker.css" rel="stylesheet">
@stop

@section('content')
<div id="page-wrapper" ng-controller="academicAttendance">
    <section id="main" class="main-wrap bgc-white-darkest" role="main">
        <div class="container-fluid content-wrap">
            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{PREFIX}}">
                                <i class="fa fa-home">
                                </i>
                            </a>
                        </li>
                         <li>
                            <a href="{{URL_ACADEMICOPERATIONS_DASHBOARD}}">
                                {{getPhrase('academic_operations')}}
                            </a>
                        </li>
                        <li>
                            <a href="{{URL_OFFLINE_EXAMS}}">
                                {{getPhrase('offline_exams')}}
                            </a>
                        </li>
                        <li>
                            {{getPhrase('selection_details')}} 
                        </li>
                    </ol>
                </div>
            </div>
            <!-- /.row -->
           <section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item">
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                        <h2 class="pull-left"> {{ $examtitle }} </h2>
                        <!--End panel icons-->
                    </div>
                    <div class="panel-body panel-body-p instruction">
                        <div class="row">
                        
                             {!! Form::open(array('url' => URL_OFFLINE_EXAMS_ADD, 'method' => 'POST')) !!}
                             
                           
                          <div class ="col-md-12">

                         <fieldset class="form-group col-md-12">
                                {{ Form::label('select_subject', getphrase('select_course')) }}
                                
                                <span class="text-red">*</span>

                                {{Form::select('quiz_applicability_id',$quizzes,  null, 
                                [   'class'     => 'form-control',
                                    
                                    'id'        =>'select_academic_year'
                                   
                                ])}}
                                </fieldset>
                                </div>
                        </div>
                        
                        <div class="text-center">
                            <button type="submit" class="btn button btn-lg btn-success">
                                {{getPhrase('get_details')}}
                            </button>
                        </div>
                                    
                                
                        {!! Form::close() !!}
                            
                        </hr>
                    </div>
                </div>
            </section>
        </div>
    </section>
</div>
@stop
 
 

@section('footer_scripts')
<script src="{{JS}}datepicker.min.js"></script>
 <script src="{{JS}}bootstrap-toggle.min.js"></script>   
 <script>
      $('.input-daterange').datepicker({
        autoclose: true,
        startDate: "0d",
         format: '{{getDateFormat()}}',
     });
 </script>
    
@stop