@extends($layout)

@section('header_scripts')

@stop

@section('content')
<div id="page-wrapper" ng-controller="TabController">
    <section id="main" class="main-wrap bgc-white-darkest" role="main">
        <div class="container-fluid content-wrap">
            <div class="">
                <div class="col-lg-12">
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{PREFIX}}">
                                <i class="fa fa-home">
                                </i>
                            </a>
                        </li>
                          <li>
                            <a href="{{URL_ACADEMICOPERATIONS_DASHBOARD}}">
                                {{getPhrase('academic_operations')}}
                            </a>
                        </li>
                       
                        <li>
                            
                                {{$title}}
                            
                        </li>
                    </ol>
                </div>
            </div>
             {!! Form::open(array('url' => URL_PRINT_CLASS_OFFLINE_MARKS_REPORT, 'method' => 'POST', 'name'=>'htmlform ','target'=>'_blank', 'id'=>'htmlform', 'novalidate'=>'')) !!}
             <div class=" panel-grid" id="panel-grid">

                 <section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item">
                    <!--Start Panel-->
                    <div class="panel bgc-white-dark">
                        <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                            <h2 class="pull-left"> {{ getPhrase('select_details') }} </h2>
                            <!--End panel icons-->

                             <!--Start panel icons-->
                  <div class="panel-icons panel-icon-slide bgc-white-dark">
                      <ul>
                          <li><a href=""><i class="fa fa-angle-left"></i></a>
                              <ul>
                                  <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                                  <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                                  <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
                                  <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
                                  <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li>
                                  <!-- <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li> -->
                              </ul>
                          </li>
                      </ul>
                  </div>
                  <!--End panel icons-->
                        </div>
                        <div class="panel-body panel-body-p ml-1">

                            <?php 
                            $user = Auth::user();

                            $role_name = getRoleData($user->role_id);
                            
                            $param = array('class'=>'custom-row-6');
                            if($role_name=='student') {

                                $param = array('user_slug'=>$user->slug, 
                                                'class'=>'custom-row-6');
                            }
                            ?>
                            @include('common.year-selection-view', $param)

                            <fieldset  class="form-group">
                                 <label for = "offline_quiz_category_id">{{getPhrase('category')}}</label>
                                <select 
                                name      = "offline_quiz_category_id" 
                                id        = "offline_quiz_category_id" 
                                class     = "form-control" 
                                ng-model  = "offline_quiz_category_id" 
                                ng-change = "getStudentMarks112()"
                                ng-options= "option.id as option.title for option in quiz_categories track by option.id">
                                <option value="">{{getPhrase('select')}}</option>
                                </select>
                            </fieldset>

                            <hr>
                                   
                            <div ng-show="result_data.length>0" class="row">

                                <div class="col-sm-4 col-sm-offset-8">
                                    <div class="input-group">
                                            <input type="text" ng-model="search" class="form-control input-lg" placeholder="{{getPhrase('search')}}" name="search" />
                                            <span class="input-group-btn">
                                                <button class="btn btn-info btn-lg" type="button">
                                                    <i class="glyphicon glyphicon-search"></i>
                                                </button>
                                            </span>
                                        </div>
                                </div>
                            </div>
                            <br>
                            <div class="row vertical-scroll">
                                <h4 ng-if="result_data.students.length>0" >@{{course_title}}</h4>
                                <table ng-if="result_data.students.length>0" class="table table-bordered" style="border-collapse: collapse;">
                                <thead>
                                    <th style="border:1px solid #000;">{{getPhrase('name')}}</th>
                                    
                                    <th style="border:1px solid #000;">{{getPhrase('roll_no')}}</th>
                                    <th style="border:1px solid #000;" ng-repeat="subject in subjects">@{{subject.subject_code}} (@{{subject.total_marks}})</th>
                                    <th style="border:1px solid #000;">AVG. %</th>
                                    
                                </thead>
                                <tbody>
                                <tr ng-repeat="student in students | filter:search track by $index">
                                    <td style="border:1px solid #000;"><a href="{{URL_USER_DETAILS}}@{{student.slug}}">@{{student.name}}</a></td>
                                    
                                    <td style="border:1px solid #000;">@{{student.roll_no}}</td>
                                    
                                     <td style="border:1px solid #000; text-align: center;" ng-repeat="marks_record in student.marks">@{{marks_record.score.marks_obtained}}</td>
                                    <td style="border:1px solid #000;">
                                   
                                    <div class="progress">
                                      <div  ng-class="{'progress-bar progress-bar-success':student.average>=75, 'progress-bar progress-bar-warning':student.average<75 && student.average>=50, 'progress-bar progress-bar-danger':student.average<50 && student.average>=0}" role="progressbar" aria-valuenow="@{{student.average}}"
                                      aria-valuemin="0" aria-valuemax="100" style="width:@{{student.average}}%">
                                        @{{student.average}}%
                                      </div>
                                    </div>

                                    </td>
                                </tr> 
                                </tbody>
                                </table>
                            </div>
                            <div ng-if="result_data.students.length<=0"  class="text-center" >{{getPhrase('no_data_available')}}</div> 
                            <br>
                            <a ng-if="result_data.students.length>0"  class="btn btn-primary btn-lg panel-header-button" ng-click="printIt()">Print</a>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</div>

{!! Form::close() !!}

@stop
 
@section('footer_scripts')

  @include('offlineexams.class-report.scripts.js-scripts')
    
@stop