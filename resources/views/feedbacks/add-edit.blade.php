@extends($layout)

@section('content')
<div id="page-wrapper">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
	 	<div class="container-fluid content-wrap">
			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>
					 
						<li class="active">{{getPhrase('feedback_form')}}</li>
					</ol>
				</div>
			</div>
				@include('errors.errors')
			<!-- /.row -->
			
			<section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item">
      			<!--Start Panel-->
		        <div class="panel bgc-white-dark">
		          	<div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
		              	<h2 class="pull-left"> {{ $title }} </h2>
		              	<!--Start panel icons-->
		              	<div class="panel-icons panel-icon-slide bgc-white-dark">
		                  <ul>
		                      <li><a href=""><i class="fa fa-angle-left"></i></a>
		                          <ul>
		                              <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
		                              <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
		                              <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
		                              <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
		                              <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li>
		                              <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li>
		                          </ul>
		                      </li>
		                  </ul>
		              	</div>
		              <!--End panel icons-->
		          	</div>
		          	<div class="panel-body panel-body-p">
						<?php $button_name = getPhrase('send'); ?>
						 
						{!! Form::open(array('url' => URL_FEEDBACK_SEND, 'method' => 'POST', 'name'=>'formQuiz ', 'novalidate'=>'')) !!}
						<div class="row">
							 	<fieldset class="form-group col-md-12">
								{{ Form::label('title', getphrase('title')) }}
								<span class="text-red">*</span>
								{{ Form::text('title', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('title'),
									'ng-model'=>'title', 
									'ng-pattern'=>getRegexPattern('name'), 
									'required'=> 'true', 
									'ng-class'=>'{"has-error": formQuiz.title.$touched && formQuiz.title.$invalid}',
									'ng-minlength' => '4',
									'ng-maxlength' => '45',
									)) }}
								<div class="validation-error" ng-messages="formQuiz.title.$error" >
			    					{!! getValidationMessage()!!}
			    					{!! getValidationMessage('pattern')!!}
			    					{!! getValidationMessage('minlength')!!}
			    					{!! getValidationMessage('maxlength')!!}
								</div>
							</fieldset>
						</div>
						<div class="row">
							<fieldset class="form-group col-md-12">
								
								{{ Form::label('subject', getphrase('subject')) }}
								<span class="text-red">*</span>
								{{ Form::text('subject', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('subject'),
									'ng-model'=>'subject', 
									'required'=> 'true', 
									'ng-class'=>'{"has-error": formQuiz.subject.$touched && formQuiz.subject.$invalid}',
									'ng-minlength' => '2',
									'ng-maxlength' => '40',
									)) }}
								<div class="validation-error" ng-messages="formQuiz.subject.$error" >
			    					{!! getValidationMessage()!!}
			    					{!! getValidationMessage('pattern')!!}
			    					{!! getValidationMessage('minlength')!!}
			    					{!! getValidationMessage('maxlength')!!}
								</div>
							</fieldset>
					 	</div> 
						<div class="row">
						 	<fieldset class="form-group col-md-12">
							 {{ Form::label('description', getphrase('description')) }}
								<span class="text-red">*</span>
									 <textarea name="description" ng-model="description"
									 required="true" class='form-control' rows="5"></textarea>
								<div class="validation-error ckeditor" ng-messages="formQuiz.description.$error"  >
			    					{!! getValidationMessage()!!}
			    					{!! getValidationMessage('number')!!}
								</div>
							</fieldset>
						</div>
						<div class="buttons text-center">
							<button class="btn btn-lg btn-primary button"
							ng-disabled='!formQuiz.$valid'>{{ $button_name }}</button>
						</div>
						{!! Form::close() !!}
					</div>
				</div>
			</section>
		</div>
		<!-- /.container-fluid -->
	</section>
</div>
<!-- /#page-wrapper -->
@stop

@section('footer_scripts')
 @include('common.validations');
 
    
@stop
 
 