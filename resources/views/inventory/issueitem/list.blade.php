@extends($layout)

@section('header_scripts')

<link href="{{CSS}}ajax-datatables.css" rel="stylesheet">

@stop

@section('content')




<div id="page-wrapper">

	<section id="main" class="main-wrap bgc-white-darkest" role="main">
	    <div class="container-fluid content-wrap">

			<!-- Page Heading -->

			<div class="row">

				<div class="col-lg-12">

					<ol class="breadcrumb">

						<li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>

						<li>{{ $title }}</li>

					</ol>

				</div>

			</div>



			<!-- /.row -->

			<section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item " >
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                        <h2 class="pull-left"> {{ $title }} </h2>   
                        <div class="pull-right messages-buttons helper_step1">
							<a href="{{URL_INVENTORY_ISSUE_ITEM_ADD}}" class="btn  btn-primary button panel-header-button" >{{ getPhrase('create')}}</a>
						</div>                   
                        <!--End panel icons-->
                    </div>
                    <div class="panel-body panel-body-p packages">

						<div> 

							<table class="table table-striped table-bordered datatable" cellspacing="0" width="100%">

								<thead>

									<tr>

										<th>{{ getPhrase('item')}}</th>

										<th>{{ getPhrase('category')}}</th>

										<th>Issue - Return</th>

										<th>{{ getPhrase('issue_to')}}</th>

										<th>{{ getPhrase('issue_by')}}</th>

										<th>{{ getPhrase('quantity')}}</th>

										<th>{{ getPhrase('status')}}</th>

										<th>{{ getPhrase('action')}}</th>

									</tr>

								</thead>



							</table>

						</div>



					</div>

				</div>
			</section>

		</div>
		<!-- /.container-fluid -->
	</section>

	<div id="approve" class="modal fade" role="dialog">
		<div class="modal-dialog modal-md">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" align="center"><b>{{getPhrase('return_inventory_item')}}</b></h4>
				</div>
				<div class="modal-body">


					{!! Form::open(array('url' => URL_INVENTORY_ISSUE_ITEM_RETURN, 'method' => 'POST', 'name'=>'formQuiz ', 'novalidate'=>'','files'=>TRUE,'id'=>'formQuiz' )) !!}

					<h4 style="color: #ffa616;" align="center">{{getPhrase('are_you_sure_to_return_item')}}</h4>

					<fieldset class="form-group helper_step1">

						{{ Form::label('notes', getphrase('notes')) }}

						{{ Form::textarea('notes', $value = null , $attributes = array('class'=>'form-control editor1', 'id'=>'editor1', 'rows'=>'5', 'placeholder' => getPhrase('notes'))) }}

					</fieldset>

					<input type="hidden" name="return_id" id="return_id">


				</div>



				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Yes</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">No</button>&nbsp;
				</div>
				{!! Form::close() !!}

			</div>

		</div>
	</div>



</div>

@endsection

 



@section('footer_scripts')

  

 @include('common.datatables', array('route'=>URL_INVENTORY_ISSUE_ITEM_GETLIST, 'route_as_url' => TRUE))

 @include('common.deletescript', array('route'=>URL_INVENTORY_ISSUE_ITEM_DELETE))

<script>
	 function returnRecord(id)
      {    
      	  $('#return_id').val(id);
          $('#approve').modal('show');
      }
</script>


@stop

