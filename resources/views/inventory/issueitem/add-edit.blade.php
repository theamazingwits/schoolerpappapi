@extends($layout)
 
@section('content')
@if($record)
<div id="page-wrapper" ng-controller="inventoryAssign" ng-init="PreData('{{$record->category_id}}','{{$record->item_id}}','{{$record->role_id}}','{{$record->user_id}}')">
@else	
<div id="page-wrapper" ng-controller="inventoryAssign">
@endif	
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
    <div class="container-fluid content-wrap">
  		<!-- Page Heading -->
  		    <div class="row">
  			<div class="col-lg-12">
  				<ol class="breadcrumb">
  					<li><a href="/"><i class="fa fa-home"></i></a> </li>
  					<li><a href="{{URL_INVENTORY_ISSUE_ITEM}}">{{getPhrase('inventory_issues')}}</a></li>
  					<li class="active">{{isset($title) ? $title : ''}}</li>
  				</ol>
  			</div>
  		</div>
  			
  			@include('errors.errors')
  		<!-- /.row -->
  		
  		<section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item " >
                <!--Start Panel-->
        <div class="panel bgc-white-dark">
            <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                <h2 class="pull-left"> {{ $title }} </h2>   
                <div class="pull-right messages-buttons">
                  <a href="{{URL_INVENTORY_ISSUE_ITEM}}" class="btn  btn-primary button panel-header-button" >{{ getPhrase('list')}}</a>
                </div>                   
                <!--End panel icons-->
            </div>
            <div class="panel-body panel-body-p packages">
        			<?php $button_name = getPhrase('create'); ?>
        			@if ($record)
        			 <?php $button_name = getPhrase('update'); ?>
        				{{ Form::model($record, 
        				array('url' => URL_INVENTORY_ISSUE_ITEM_EDIT.$record->id, 
        				'method'=>'patch', 'files' => true, 'name'=>'formInventory ', 'novalidate'=>'')) }}
        			@else
        		{!! Form::open(array('url' => URL_INVENTORY_ISSUE_ITEM_ADD, 'method' => 'POST', 'files' => true, 'name'=>'formInventory ', 'novalidate'=>'')) !!}
        			@endif
        			
        			 @include('inventory.issueitem.form_elements', 
        			 array('button_name'=> $button_name,'categories'=>$categories,'roles'=>$roles,'users'=>$users))
        			 		
        			{!! Form::close() !!}
  			   </div>

  		  </div>
      </section>
  	</div>
  	<!-- /.container-fluid -->
  </section>
</div>
		<!-- /#page-wrapper -->
@stop

@section('footer_scripts')
 @include('common.validations')
 
 <script>

 	app.controller('inventoryAssign', function( $scope, $http ){

     $scope.PreData  = function(category_id, item_id, role_id, user_id){

 		 	    $scope.selected_category_id  = category_id;
          
                route   = '{{ URL_GET_INVENTORY_CATEGORY_ITEMS }}';
    
                data    = {   
                   _method: 'post', 
                  '_token':$scope.getToken(), 
                  'category_id': category_id, 
               };

              $http.post(route,data).then(function(response){
                 
               $scope.cat_items  = response.data;
              
               $scope.my_item_index  =  $scope.findMyIndex($scope.cat_items, 'id', item_id);
               $scope.item_id        = $scope.cat_items[$scope.my_item_index];    


              });  


               $scope.selected_role_id  = role_id;

                route = '{{ URL_GET_INVENTORY_ISSUE_ITEM }}';
                
                data    = {   
                   _method: 'post', 
                  '_token':$scope.getToken(), 
                  'role_id': role_id, 
               };

              $http.post(route,data).then(function(response){
   
                  $scope.rol_users  = response.data;


                   $scope.my_user_index  =  $scope.findMyIndex($scope.rol_users, 'id', user_id);
                   
                   $scope.user_id        = $scope.rol_users[$scope.my_user_index];

              });  

 		 }


 		 $scope.findMyIndex  = function( Array, property, action ){
       
            var result           = -1;
          angular.forEach(Array, function(value, index) {
          	
             if(value[property]== action){
                result   = index;
             }
          });
          return result;
 		 } 
        
         $scope.getItems = function(category_id){

                $scope.selected_category_id  = category_id;
                route = '{{ URL_GET_INVENTORY_CATEGORY_ITEMS }}';
    
                data    = {   
                   _method: 'post', 
                  '_token':$scope.getToken(), 
                  'category_id': category_id, 
               };

              $http.post(route,data).then(function(response){
                 
               $scope.cat_items  = response.data;
              
              });  

         }


          $scope.getUsers = function(role_id){
             
                $scope.selected_role_id  = role_id;

                route = '{{ URL_GET_INVENTORY_ISSUE_ITEM }}';
                
                data    = {   
                   _method: 'post', 
                  '_token':$scope.getToken(), 
                  'role_id': role_id, 
               };

              $http.post(route,data).then(function(response){
   
                  $scope.rol_users  = response.data;
              });  

         }




         

    $scope.getToken = function(){
      return  $('[name="_token"]').val();
    }

}); 

 	
 	
 	
 </script>
@stop
 
 