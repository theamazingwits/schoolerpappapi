

					<div class="row">

 					<fieldset class="form-group col-md-6">
				{{ Form::label('category_id', getphrase('inventory_category')) }}
						<span class="text-red">*</span>
						{{Form::select('category_id', $categories, null,
						 [
							'class'=>'form-control',
							'ng-change'=>'getItems(category_id)', 
							'id'=>'category_id',
						    'ng-model'=>'category_id',
						    'placeholder'=>'Select',
						    'required'=> 'true', 
	     'ng-class'=>'{"has-error": formInventory.category_id.$touched && formInventory.category_id.$invalid}',
						])}}
						 <div class="validation-error" ng-messages="formInventory.category_id.$error" >
	    					{!! getValidationMessage()!!}
						</div>
					</fieldset>


					<fieldset ng-if="selected_category_id"  class="form-group col-md-6">
                           <label for = "item_id">{{getPhrase('items')}}</label>
                          <span class="text-red">*</span>
                          <select 
                          name      = "item_id" 
                          id        = "item_id" 
                          class     = "form-control" 
                          ng-model  = "item_id" 
                          ng-options= "option.id as option.name for option in cat_items track by option.id">
                          <option value="">{{getPhrase('select')}}</option>
                          </select>
                      </fieldset> 


 					<fieldset class="form-group col-md-6">
						{{ Form::label('role_id', getphrase('user_type')) }}
						<span class="text-red">*</span>
						{{Form::select('role_id', $roles, null,
						 [
							'class'=>'form-control',
							'ng-change'=>'getUsers(role_id)', 
							'id'=>'role_id',
						     'ng-model'=>'role_id',
						     'placeholder'=>'Select',
						     'required'=> 'true', 
						     'ng-class'=>'{"has-error": formInventory.role_id.$touched && formInventory.role_id.$invalid}',
						])}}
						 <div class="validation-error" ng-messages="formInventory.role_id.$error" >
	    					{!! getValidationMessage()!!}
						</div>
					</fieldset>

					

			         
			          <fieldset ng-if="selected_role_id"  class="form-group col-md-6">
                         <label for = "user_id">{{getPhrase('users')}}</label>
                          <span class="text-red">*</span>
                          <select 
                          name      = "user_id" 
                          id        = "user_id" 
                          class     = "form-control" 
                          ng-model  = "user_id" 
                     ng-options= "option.id as option.name for option in rol_users track by option.id">
                          <option value="">{{getPhrase('select')}}</option>
                          </select>
                   
                      </fieldset> 

                    
 				

 					 <fieldset class="form-group col-md-6">

						{{ Form::label('quantity', getphrase('quantity')) }}

						<span class="text-red">*</span>

						{{ Form::number('quantity', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('enter quantity'),

							'ng-model'=>'quantity', 

							'id'=>'quantity',

							'required'=> 'true', 

							'ng-class'=>'{"has-error": formInventory.quantity.$touched && formInventory.quantity.$invalid}',


							)) }}

						<div class="validation-error" ng-messages="formInventory.quantity.$error" >

	    					{!! getValidationMessage()!!}

						</div>

					</fieldset>



 		
				   
					<fieldset class="form-group helper_step1 col-md-12">

						{{ Form::label('description', getphrase('description')) }}

						{{ Form::textarea('description', $value = null , $attributes = array('class'=>'form-control editor1', 'id'=>'editor1', 'rows'=>'5', 'placeholder' => getPhrase('description'))) }}

					</fieldset>

				</div>


						<div class="buttons text-center">

							<button class="btn btn-lg btn-primary button"

							ng-disabled='!formInventory.$valid'>{{ $button_name }}</button>

						</div>

		 