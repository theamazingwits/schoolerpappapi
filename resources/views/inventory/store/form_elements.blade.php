 					

 				

					<div class="row">

 					 <fieldset class="form-group col-md-6">

						

						{{ Form::label('store_name', getphrase('store_name')) }}

						<span class="text-red">*</span>

						{{ Form::text('store_name', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('enter_your_storename'),

							'ng-model'=>'store_name', 

							'ng-pattern'=>getRegexPattern('name'), 
							'id'=>'store_name',

							'required'=> 'true', 

							'ng-class'=>'{"has-error": formInventory.store_name.$touched && formInventory.store_name.$invalid}',

							'ng-minlength' => '4',

							'ng-maxlength' => '40',

							)) }}

						<div class="validation-error" ng-messages="formInventory.store_name.$error" >

	    					{!! getValidationMessage()!!}

	    					{!! getValidationMessage('pattern')!!}

	    					{!! getValidationMessage('minlength')!!}

	    					{!! getValidationMessage('maxlength')!!}

						</div>

					</fieldset>

 					 <fieldset class="form-group col-md-6">

						

						{{ Form::label('stock_code', getphrase('stock_code')) }}

						<span class="text-red">*</span>

						{{ Form::text('stock_code', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('enter_your_stockcode'),

							'ng-model'=>'stock_code', 

							'ng-pattern'=>getRegexPattern('name'), 
							'id'=>'stock_code',

							'required'=> 'true', 

							'ng-class'=>'{"has-error": formInventory.stock_code.$touched && formInventory.stock_code.$invalid}',

							'ng-minlength' => '1',

							'ng-maxlength' => '40',

							)) }}

						<div class="validation-error" ng-messages="formInventory.stock_code.$error" >

	    					{!! getValidationMessage()!!}

	    					{!! getValidationMessage('pattern')!!}

	    					{!! getValidationMessage('minlength')!!}

	    					{!! getValidationMessage('maxlength')!!}

						</div>

					</fieldset>



				   





					<fieldset class="form-group helper_step1 col-md-12">

						{{ Form::label('description', getphrase('description')) }}

						

						{{ Form::textarea('description', $value = null , $attributes = array('class'=>'form-control editor1', 'id'=>'editor1', 'rows'=>'5', 'placeholder' => getPhrase('description'))) }}

					</fieldset>

					 </div>





						<div class="buttons text-center">

							<button class="btn btn-lg btn-primary button"

							ng-disabled='!formInventory.$valid'>{{ $button_name }}</button>

						</div>

		 