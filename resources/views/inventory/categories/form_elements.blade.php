 					

 				

					<div class="row">

 					 <fieldset class="form-group col-md-12">

						{{ Form::label('name', getphrase('name')) }}

						<span class="text-red">*</span>

						{{ Form::text('name', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('enter_category_name'),

							'ng-model'=>'name', 

							
							'id'=>'name',

							'required'=> 'true', 

						

							)) }}


					</fieldset>

				   
					<fieldset class="form-group col-md-12">

						{{ Form::label('description', getphrase('description')) }}

						{{ Form::textarea('description', $value = null , $attributes = array('class'=>'form-control editor1', 'id'=>'editor1', 'rows'=>'5', 'placeholder' => getPhrase('description'))) }}

					</fieldset>

				</div>



						<div class="buttons text-center">

							<button class="btn btn-lg btn-primary button"

							>{{ $button_name }}</button>

						</div>

		 