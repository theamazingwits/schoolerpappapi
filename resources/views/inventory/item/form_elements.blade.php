 					

 				

					<div class="row">

 					 <fieldset class="form-group col-md-6">

						

						{{ Form::label('name', getphrase('name')) }}

						<span class="text-red">*</span>

						{{ Form::text('name', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('enter_your_name'),

							'ng-model'=>'name', 

							'ng-pattern'=>getRegexPattern('name'), 
							'id'=>'name',

							'required'=> 'true', 

							'ng-class'=>'{"has-error": formInventory.name.$touched && formInventory.name.$invalid}',

							'ng-minlength' => '4',

							'ng-maxlength' => '40',

							)) }}

						<div class="validation-error" ng-messages="formInventory.name.$error" >

	    					{!! getValidationMessage()!!}

	    					{!! getValidationMessage('pattern')!!}

	    					{!! getValidationMessage('minlength')!!}

	    					{!! getValidationMessage('maxlength')!!}

						</div>

					</fieldset>


 					<fieldset class="form-group col-md-6">
						{{ Form::label('category_id', getphrase('inventory_category')) }}
						<span class="text-red">*</span>
						{{Form::select('category_id', $categories, null, ['class'=>'form-control','onChange'=>'getSubjectParents()', 'id'=>'subject',
							'ng-model'=>'category_id',
							'placeholder'=>'Select',
							'required'=> 'true', 
							'ng-class'=>'{"has-error": formInventory.category_id.$touched && formInventory.category_id.$invalid}'
						])}}
						 <div class="validation-error" ng-messages="formInventory.category_id.$error" >
	    					{!! getValidationMessage()!!}
						</div>
					</fieldset>

					<fieldset class="form-group helper_step1 col-md-12">

						{{ Form::label('description', getphrase('description')) }}

						

						{{ Form::textarea('description', $value = null , $attributes = array('class'=>'form-control editor1', 'id'=>'editor1', 'rows'=>'5', 'placeholder' => getPhrase('description'))) }}

					</fieldset>

				</div>




						<div class="buttons text-center">

							<button class="btn btn-lg btn-primary button"

							ng-disabled='!formInventory.$valid'>{{ $button_name }}</button>

						</div>

		 