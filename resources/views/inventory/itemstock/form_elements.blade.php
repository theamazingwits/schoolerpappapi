 					

 				

					<div class="row">

 					<fieldset class="form-group col-md-6">
						{{ Form::label('category_id', getphrase('inventory_category')) }}
						<span class="text-red">*</span>
						{{Form::select('category_id', $categories, null, ['class'=>'form-control','onChange'=>'getSubjectParents()', 'id'=>'subject',
							'ng-model'=>'category_id',
							'placeholder'=>'Select',
							'required'=> 'true', 
							'ng-class'=>'{"has-error": formInventory.category_id.$touched && formInventory.category_id.$invalid}',
						])}}
						 <div class="validation-error" ng-messages="formInventory.category_id.$error" >
	    					{!! getValidationMessage()!!}
						</div>
					</fieldset>

 					<fieldset class="form-group col-md-6">
						{{ Form::label('item_id', getphrase('inventory_item')) }}
						<span class="text-red">*</span>
						{{Form::select('item_id', $items, null, ['class'=>'form-control','onChange'=>'getSubjectParents()', 'id'=>'subject',
							'ng-model'=>'item_id',
							'placeholder'=>'Select',
							'required'=> 'true', 
							'ng-class'=>'{"has-error": formInventory.item_id.$touched && formInventory.item_id.$invalid}',
						])}}
						 <div class="validation-error" ng-messages="formInventory.item_id.$error" >
	    					{!! getValidationMessage()!!}
						</div>
					</fieldset>

 					<fieldset class="form-group col-md-6">
						{{ Form::label('supplier_id', getphrase('inventory_supplier')) }}
						<span class="text-red">*</span>
						{{Form::select('supplier_id', $suppliers, null, ['class'=>'form-control','onChange'=>'getSubjectParents()', 'id'=>'subject',
							'ng-model'=>'supplier_id',
							'placeholder'=>'Select',
							'required'=> 'true', 
							'ng-class'=>'{"has-error": formInventory.supplier_id.$touched && formInventory.supplier_id.$invalid}',
						])}}
						 <div class="validation-error" ng-messages="formInventory.supplier_id.$error" >
	    					{!! getValidationMessage()!!}
						</div>
					</fieldset>

 					<fieldset class="form-group col-md-6">
						{{ Form::label('store_id', getphrase('inventory_store')) }}
						<span class="text-red">*</span>
						{{Form::select('store_id', $stores, null, ['class'=>'form-control','onChange'=>'getSubjectParents()', 'id'=>'subject',
							'ng-model'=>'store_id',
							'placeholder'=>'Select',
							'required'=> 'true', 
							'ng-class'=>'{"has-error": formInventory.store_id.$touched && formInventory.store_id.$invalid}',
						])}}
						 <div class="validation-error" ng-messages="formInventory.store_id.$error" >
	    					{!! getValidationMessage()!!}
						</div>
					</fieldset>

 					 <fieldset class="form-group col-md-6">

						{{ Form::label('quantity', getphrase('quantity')) }}

						<span class="text-red">*</span>

						{{ Form::text('quantity', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('enter quantity'),

							'ng-model'=>'quantity', 

							'id'=>'quantity',

							'required'=> 'true', 

							'ng-class'=>'{"has-error": formInventory.quantity.$touched && formInventory.quantity.$invalid}',


							)) }}

						<div class="validation-error" ng-messages="formInventory.quantity.$error" >

	    					{!! getValidationMessage()!!}

						</div>

					</fieldset>

				    <fieldset class="form-group col-md-6">
                                     
                        {{ Form::label('date', getphrase('date')) }}
                        <span class="text-red">*</span>

                        <div class="input-group date" data-date="{{date('Y/m/d')}}" data-provide="datepicker" data-date-format="{{getDateFormat()}}">

                        {{ Form::text('date', $value = date('Y/m/d') , $attributes = array(

                        	'class'       =>'form-control',
                        	'placeholder' => '2015/7/17', 
                        	'id'          =>'dp',
                        	'ng-model'=>'date',
							'required'=> 'true',
							'ng-class'=>'{"has-error": formQuiz.date.$touched && formQuiz.date.$invalid}'

                        	 )) }}

                        <div class="validation-error" ng-messages="formQuiz.date.$error" >
	    					{!! getValidationMessage()!!}
						</div>

                            <div class="input-group-addon">

                                <span class="mdi mdi-calendar"></span>

                            </div>

                        </div>

                        </fieldset>

 					 <fieldset class="form-group col-md-6">

						

						{{ Form::label('item_file', getphrase('attachment')) }}


						{{ Form::file('item_file', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('enter item_file'), 	)) }}

							<fieldset>
							@if($record)
								@if($record->upload_file)

								<img src="{{IMAGE_PATH_UPLOAD_INVENTORY_ITEM_STOCK.$record->upload_file}}" height="50px" width="50px">
								@endif
								@endif

							</fieldset>
						
							
						</fieldset>
					<fieldset class="form-group helper_step1 col-md-12">

						{{ Form::label('description', getphrase('description')) }}

						

						{{ Form::textarea('description', $value = null , $attributes = array('class'=>'form-control editor1', 'id'=>'editor1', 'rows'=>'5', 'placeholder' => getPhrase('description'))) }}

					</fieldset>

				</div>


						<div class="buttons text-center">

							<button class="btn btn-lg btn-primary button"

							ng-disabled='!formInventory.$valid'>{{ $button_name }}</button>

						</div>

		 