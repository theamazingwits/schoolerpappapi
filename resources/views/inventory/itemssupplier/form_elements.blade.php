 					

 				

					<div class="row">

 					 <fieldset class="form-group col-md-6">

						

						{{ Form::label('name', getphrase('name')) }}

						<span class="text-red">*</span>

						{{ Form::text('name', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('enter_your_name'),

							'ng-model'=>'name', 

							'ng-pattern'=>getRegexPattern('name'), 
							
							'id'=>'name',

							'required'=> 'true', 

							'ng-class'=>'{"has-error": formInventory.name.$touched && formInventory.name.$invalid}',

							'ng-minlength' => '4',

							'ng-maxlength' => '40',

							)) }}

						<div class="validation-error" ng-messages="formInventory.name.$error" >

	    					{!! getValidationMessage()!!}

	    					{!! getValidationMessage('pattern')!!}

	    					{!! getValidationMessage('minlength')!!}

	    					{!! getValidationMessage('maxlength')!!}

						</div>

					</fieldset>

 					 <fieldset class="form-group col-md-6">

						

						{{ Form::label('phone', getphrase('phone_no')) }}

						<span class="text-red">*</span>

						{{ Form::number('phone', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('enter_phone_number'),

							'ng-model'=>'phone', 	
						
							'id'=>'phone',

							'required'=> 'true', 

							'ng-class'=>'{"has-error": formInventory.phone.$touched && formInventory.phone.$invalid}',


							)) }}

						<div class="validation-error" ng-messages="formInventory.phone.$error" >

	    					{!! getValidationMessage()!!}

	 						{!! getValidationMessage('pattern')!!}


						</div>

					</fieldset>
 					 <fieldset class="form-group col-md-6">

						

						{{ Form::label('email', getphrase('email')) }}

						<span class="text-red">*</span>

						{{ Form::text('email', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('enter_your_email_id'),

							'ng-model'=>'email', 

							'ng-pattern'=>getRegexPattern('email'), 
							
							'id'=>'email',

							'required'=> 'true', 

							'ng-class'=>'{"has-error": formInventory.email.$touched && formInventory.email.$invalid}',

							'ng-minlength' => '4',

							'ng-maxlength' => '50',

							)) }}

						<div class="validation-error" ng-messages="formInventory.email.$error" >

	    					{!! getValidationMessage()!!}

	    					{!! getValidationMessage('pattern')!!}

	    					{!! getValidationMessage('minlength')!!}

	    					{!! getValidationMessage('maxlength')!!}

						</div>

					</fieldset>

 					 <fieldset class="form-group col-md-6">

						

						{{ Form::label('address', getphrase('address')) }}

						<span class="text-red">*</span>

						{{ Form::text('address', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('enter_your_address'),

							'ng-model'=>'address', 

							
							'id'=>'address',

							'required'=> 'true', 

							'ng-class'=>'{"has-error": formInventory.address.$touched && formInventory.address.$invalid}',

							'ng-minlength' => '4',

							'ng-maxlength' => '100',

							)) }}

						<div class="validation-error" ng-messages="formInventory.address.$error" >

	    					{!! getValidationMessage()!!}

	    					{!! getValidationMessage('pattern')!!}

	    					{!! getValidationMessage('minlength')!!}

	    					{!! getValidationMessage('maxlength')!!}

						</div>

					</fieldset>
 					 <fieldset class="form-group col-md-4">

						

						{{ Form::label('contact_person_name', getphrase('contact_person_name')) }}

						

						{{ Form::text('contact_person_name', $value = null , $attributes = array('class'=>'form-control', 'id'=>'contact_person_email','placeholder' => getPhrase('enter_contact_person_name'))) }}



						

						

					</fieldset>

 					 <fieldset class="form-group col-md-4">

						

						{{ Form::label('contact_person_number', getphrase('contact_person_number')) }}


						{{ Form::number('contact_person_number', $value = null , $attributes = array('class'=>'form-control contact_person_number','id'=>'contact_person_email' ,'placeholder' => getPhrase('enter_contact_person_number'))) }}

						

						


					</fieldset>
 					 <fieldset class="form-group col-md-4">	

						{{ Form::label('contact_person_email', getphrase('contact_person_email')) }}
						
						{{ Form::email('contact_person_email', $value = null , $attributes = array('class'=>'form-control','id'=>'contact_person_email' ,'placeholder' => getPhrase('enter_contact_person_email'))) }}


							

						

						

					</fieldset>
					<fieldset class="form-group helper_step1 col-md-12">

						{{ Form::label('description', getphrase('description')) }}

						

						{{ Form::textarea('description', $value = null , $attributes = array('class'=>'form-control editor1', 'id'=>'editor1', 'rows'=>'5', 'placeholder' => getPhrase('description'))) }}

					</fieldset>

				</div>





						<div class="buttons text-center">

							<button class="btn btn-lg btn-primary button"

							ng-disabled='!formInventory.$valid'>{{ $button_name }}</button>

						</div>

		 