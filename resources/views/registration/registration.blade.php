<!DOCTYPE html>

<html lang="en" dir="{{ (App\Language::isDefaultLanuageRtl()) ? 'rtl' : 'ltr' }}">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="{{getSetting('meta_description', 'seo_settings')}}">
	<meta name="keywords" content="{{getSetting('meta_keywords', 'seo_settings')}}">
	 
	<link rel="icon" href="{{IMAGE_PATH_SETTINGS.getSetting('site_favicon', 'site_settings')}}" type="image/x-icon" />


    <link href="{{CSS}}{{getSetting('current_theme', 'site_settings')}}-theme.css" rel="stylesheet">

    <!-- Morris Charts CSS -->

    <link href="{{CSS}}plugins/morris.css" rel="stylesheet">

    <!-- Proxima Nova Fonts CSS -->

    <link href="{{CSS}}proximanova.css" rel="stylesheet">

    <link href="{{CSS}}sweetalert.css" rel="stylesheet" type="text/css">
	<!-- Custom Fonts -->
	<!-- <link href="{{CSS}}custom-fonts.css" rel="stylesheet" type="text/css"> -->
	<link href="{{PREFIX1}}assets/css/style.css" rel="stylesheet" type="text/css">
	<script src="{{PREFIX1}}assets/js/system.js"></script>    
    <style>

        .registerbtn{
            margin-left: 15em !important;
        }

        @media all and (device-width: 360px) and (device-height: 640px) {
            .registerbtn{
            margin-left: 4em !important;
            }
        }
        @media all and (device-width: 600px) and (device-height: 960px) {
            .registerbtn{
            margin-left: 9em !important;
            }
        }
        @media all and (device-width: 411px) and (device-height: 731px) {
            .registerbtn{
            margin-left: 5em !important;
            }
        }
        @media all and (device-width: 411px) and (device-height: 823px) {
            .registerbtn{
            margin-left: 5em !important;
            }
        }
        @media all and (device-width: 320px) and (device-height: 568px) {
            .registerbtn{
            margin-left: 3em !important;
            }
        }
        @media all and (device-width: 375px) and (device-height: 667px) {
            .registerbtn{
            margin-left: 5em !important;
            }
        }
        @media all and (device-width: 414px) and (device-height: 736px) {
            .registerbtn{
            margin-left: 5em !important;
            }
        }
        @media all and (device-width: 375px) and (device-height: 812px) {
            .registerbtn{
            margin-left: 5em !important;
            }
        }
        @media all and (device-width: 768px) and (device-height: 1024px) {
            .registerbtn{
            margin-left: 12em !important;
            }
        }
        .fl-fl { background: #3a8486; text-transform: uppercase; letter-spacing: 3px; padding: 4px; width: 190px; position: fixed; right: -140px; z-index: 1000; font: normal normal 10px Arial; -webkit-transition: all .25s ease; -moz-transition: all .25s ease; -ms-transition: all .25s ease; -o-transition: all .25s ease; transition: all .25s ease; } .fa-design { font-size: 30px; color: #fff; padding: 10px 0; width: 50px; margin-left: 8px; } .fl-fl:hover { right: 0; } .fl-fl a { color: #fff !important; text-decoration: none; text-align: center; line-height: 43px!important;  font-size:18px } .float-fb { top: 50px; } .float-tw { top: 215px; } .float-gp { top: 270px; } .float-rs { top: 325px; } .float-ig { top: 380px; } .float-pn { top: 435px; }

    </style>
</head>

<body>
    <div id="app" class="empty-page reactive-app">
        <!-- inject-body-start:js -->
        <script src="assets/js/settings.js?1568089386714"></script>
        <!-- endinject -->
        <div id="main" class="main">
            <!--Begin Content-->
            <section id="content" class="content-wrap bgc-white-darkest" role="main">
                <section class="central-block">
                    <div class="empty-page-content d-inline-block col-xl-4 col-lg-6 col-md-8 col-sm-10" style="max-width: 60em !important;">
                        <h1 class="fw-semibold c-info text-uppercase">NOW</h1>
                        <h5 class="mt-1">School Management System</h5>
                        <div class="transition fade right in">
                            <div class="px-sm-5 px-3 pt-4 bgc-white-darker mt-5 text-left">
                                <form id="register_form">
                                <div class="row">
                                    <fieldset class="form-group col-sm-6">
                                        {{ Form::label('schoolname', getphrase('school_name')) }}
                                        <span class="text-red">*</span>
                                        <div class="input-group  mb-2 mr-2 mb-0">
                                            <input type="text" class="form-control form-control-lg" id="schoolname" placeholder="School Name" onkeypress="return onlyalphabetValidation(event)">
                                        </div>
                                    </fieldset>
                                    <fieldset class="form-group col-sm-6">
                                        {{ Form::label('board', getphrase('board')) }}
                                        <span class="text-red">*</span>
                                        <div class="input-group mb-2 mr-2 mb-0">
                                            <select class="form-control form-control-lg" id="schoolboard">
                                                <option value="" selected disabled>Select Board</option>
                                                <option value="CBSE">CBSE</option>
                                                <option value="ICSEBoard">ICSE Board</option>
                                                <option value="IB">IB</option>
                                                <option value="StateBoard">State Board</option>
                                                <option value="IGCSE">IGCSE</option>
                                                <option value="CIE">CIE</option>
                                                <option value="NIOS">NIOS</option>
                                            </select>
                                        </div>
                                    </fieldset>
                                    <fieldset class="form-group col-sm-6">
                                        {{ Form::label('country', getphrase('country')) }}
                                        <span class="text-red">*</span>
                                        <div class="input-group mb-2 mr-2 mb-0">
                                            <select id="country" name ="country"  class="form-control form-control-lg" onchange="populateStates()"></select>
                                        </div>
                                    </fieldset>
                                    <fieldset class="form-group col-sm-6">
                                        {{ Form::label('state', getphrase('state')) }}
                                        <span class="text-red">*</span>
                                        <div class="input-group mb-2 mr-2 mb-0">
                                            <select id ="state" name ="state" class="form-control form-control-lg"></select>
                                        </div>
                                    </fieldset>
                                    <fieldset class="form-group col-sm-6">
                                        {{ Form::label('addr1', getphrase('address_line_1')) }}
                                        <span class="text-red">*</span>
                                        <div class="input-group mb-2 mr-2 mb-0">
                                            <input type="text" class="form-control form-control-lg" id="addr1" placeholder="Address Line1" onkeypress="return addressValidation(event)">
                                        </div>
                                    </fieldset>
                                    <fieldset class="form-group col-sm-6">
                                        {{ Form::label('addr2', getphrase('address_line_2')) }}
                                        <span class="text-red">*</span>
                                        <div class="input-group mb-2 mr-2 mb-0">
                                            <input type="text" class="form-control form-control-lg" id="addr2" placeholder="Address Line2" onkeypress="return addressValidation(event)">
                                        </div>
                                    </fieldset>
                                    <fieldset class="form-group col-sm-6">
                                        {{ Form::label('city', getphrase('city')) }}
                                        <span class="text-red">*</span>
                                        <div class="input-group mb-2 mr-2 mb-0">
                                            <input type="text" class="form-control form-control-lg" id="city" placeholder="City" onkeypress="return onlyalphabetValidation(event)">
                                        </div>
                                    </fieldset>
                                    <fieldset class="form-group col-sm-6">
                                        {{ Form::label('pincode', getphrase('postal_code')) }}
                                        <span class="text-red">*</span>
                                        <div class="input-group mb-2 mr-2 mb-0">
                                            <input type="text" class="form-control form-control-lg" id="pincode" placeholder="Postalcode" onkeypress="return onlynumberValidation(event)">
                                        </div>
                                    </fieldset>
                                    <fieldset class="form-group col-sm-6">
                                        {{ Form::label('contactpersonname', getphrase('contact_person_name')) }}
                                        <span class="text-red">*</span>
                                        <div class="input-group mb-2 mr-2 mb-0">
                                            <input type="text" class="form-control form-control-lg" id="contactpersonname" placeholder="Contact Person Name" onkeypress="return onlyalphabetValidation(event)">
                                        </div>
                                    </fieldset>
                                    <fieldset class="form-group col-sm-6">
                                        {{ Form::label('emailid', getphrase('emailid')) }}
                                        <span class="text-red">*</span>
                                        <div class="input-group mb-2 mr-2 mb-0">
                                            <input type="text" class="form-control form-control-lg" id="emailid" placeholder="Contact Person EmailId">
                                        </div>
                                    </fieldset>
                                    <fieldset class="form-group col-sm-6">
                                        {{ Form::label('mobileno', getphrase('mobile_number')) }}
                                        <span class="text-red">*</span>
                                        <div class="input-group mb-2 mr-2 mb-0">
                                            <input type="text" class="form-control form-control-lg" id="mobileno" placeholder="Contact Person Mobile Number" onkeypress="return onlynumberValidation(event)">
                                        </div>
                                    </fieldset>
                                    <fieldset class="form-group col-sm-6">
                                        {{ Form::label('landline', getphrase('landline_number')) }}
                                        <div class="input-group mb-2 mr-2 mb-0">
                                            <input type="text" class="form-control form-control-lg" id="landlineno" placeholder="Landline Number" onkeypress="return onlynumberValidation(event)">
                                        </div>
                                    </fieldset>
                                </div>
                                <div class="row">
                                    <div class="buttons text-center">
                                        <button type="button" class="btn btn-primary btn-lg w-100 registerbtn" onclick="register();">{{ getPhrase('register') }}</button>
                                    </div>
                                </div>
                            </form>
                                <!-- <div class="pt-3">
                                    <a href="" class="btn btn-primary btn-lg w-100">Register</a>
                                </div>   -->                              
                            </div>
                        </div>
                    </div>
                </section>
            </section>

                        <div class="text">

                                <!-- Floating Social Media bar Starts -->
                                <div class="float-sm">
                                  <div class="fl-fl float-fb">
                                    <i class="fa fa-home fa-design"></i>
                                    <a href="/login" > Home</a>
                                  </div>
                                </div>                          
                        <!-- end wrapper -->
                    </div>
            <!--End Content-->
        </div>
    </div>

	<!-- Bootstrap Core JavaScript -->
    <script src="{{JS}}jquery-1.12.1.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="{{JS}}main.js"></script>
    <script src="{{JS}}countries.js"></script>
    <script src="{{JS}}sweetalert-dev.js"></script>
	<script src="{{JS}}bootstrap.min.js"></script>
	<script src="{{PREFIX1}}assets/js/shim.js"></script>
    <script src="{{PREFIX1}}assets/js/settings.js"></script>
     @include('jspm.jspm')
     @include('common.validations')
     <script>
        populateCountries();
        function register(){
            var schoolname = $('#schoolname').val();
            var schoolboard = $('#schoolboard').val();
            var country = $('#country').val();
            var state = $('#state').val();
            var addr1 = $('#addr1').val();
            var addr2 = $('#addr2').val();
            var city = $('#city').val();
            var pincode = $('#pincode').val();
            var contactpersonname = $('#contactpersonname').val();
            var emailid = $('#emailid').val();
            var mobileno = $('#mobileno').val();
            var landlineno = $('#landlineno').val();
            var mailformat = emailValidation(emailid);
            if(schoolname == "" || schoolboard == "" || country == 0 || state == 0
                || addr1 == "" || addr2 == "" || city == "" || pincode == "" || contactpersonname == "" || mobileno == ""){
                swal("Please fill all manditory fields","","error");
                return false;
            }else if (mailformat == 0) {
                swal("Please Enter Email Address","","error");
                return false;
            }
            else{
                if(mailformat == -1){
                    swal("Please Enter Valid Email Address", "example@example.com","error");
                    return false;
                }
            }
            var data = {
                "school_name": schoolname,
                "addressline1": addr1, 
                "addressline2": addr2,
                "city": city,
                "pincode":pincode,
                "state" : state,
                "country" : country,
                "board":schoolboard,
                "contactperson":contactpersonname,
                "mobilenumber":mobileno,
                "landline" : landlineno,
                "emailid" : emailid,
            }
            $.ajax({
              url: "{{SASS_URL}}/register",
              headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
              },
              type: "POST",
              data: data,
              success: function(success) {
                var status = success.status;
                    swal(success.msg,"","success");
                    $("#register_form").trigger("reset");
              },
              dataType: "json",
              timeout: 2000
        })
        }
        function emailValidation(email) {
            //var mailformat =/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            var mailformat =/\b[a-zA-Z0-9\u00C0-\u017F._%+-]+@[a-zA-Z0-9\u00C0-\u017F.-]+\.[a-zA-Z]{2,}\b/;
            //var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            
            if (email == '') {
                return 0;
            } else {
                if(email.match(mailformat)){
                    return 1;
                }
                else{
                    return -1;
                }
            }
            
        }
        function onlyalphabetValidation(e) {

            var regex = new RegExp("^[a-zA-Z \b]+$");
            var key = String.fromCharCode(!e.charCode ? e.which : e.charCode);   
            var keystobepassedout="ArrowLeftArrowRightDeleteBackspaceTab";
            if(keystobepassedout.indexOf(e.key)!=-1)
            {
              return true;
            }
            if (!regex.test(key)) {
                e.preventDefault();
                return false;
            }
        }
         
        function addressValidation(e) {

            var regex = new RegExp("^[a-zA-Z0-9\s\,\''\-\/ ]*$");
            var key = String.fromCharCode(!e.charCode ? e.which : e.charCode);   
            var keystobepassedout="ArrowLeftArrowRightDeleteBackspaceTab";
            if(keystobepassedout.indexOf(e.key)!=-1)
            {
              return true;
            }
            if (!regex.test(key)) {
                e.preventDefault();
                return false;
            }
        }
        function onlyalphaNumaricValidation(e) {

            var regex = new RegExp("^[a-zA-Z0-9 \b]+$");
            var key = String.fromCharCode(!e.charCode ? e.which : e.charCode);   
            var keystobepassedout="ArrowLeftArrowRightDeleteBackspaceTab";
            if(keystobepassedout.indexOf(e.key)!=-1)
            {
              return true;
            }
            if (!regex.test(key)) {
                e.preventDefault();
                return false;
            }
        }
        function onlynumberValidation(evt) {

                 var charCode = (evt.which) ? evt.which : event.keyCode;

                 if (charCode > 31 && (charCode < 48 || charCode > 57)){
                    return false;
                 }
                    

                 return true;
        }
     </script>
 
</body>

</html>