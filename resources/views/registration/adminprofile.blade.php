 @extends($layout)
 @section('header_scripts')
<style>
	.planhead{
		font-size: 20px;
		font-weight: bold;
	}
	.planscroll {
    	min-height: 40em;
		max-height: 40em;
		overflow-y: scroll;
	}
	.planwarning{
		color: red;
	}
	.planactive{
		color: green;
	}
</style>
@stop
@section('content')
<div id="page-wrapper">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
    	<div class="container-fluid content-wrap">
	     	<section class="col-sm-12 col-md-12 col-lg-12 col-xl-7 panel-wrap panel-grid-item">
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                        <h2 class="pull-left"> {{ $title }} </h2>
                        
                        <!--Start panel icons-->
                        <div class="panel-icons panel-icon-slide bgc-white-dark">
                            <ul>
                                <li><a href=""><i class="fa fa-angle-left"></i></a>
                                    <ul>
                                        <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                                        <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                                        <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
                                        <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
                                        <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li>
                                        <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <!--End panel icons-->
                    </div>
                    <div class="panel-body panel-body-p">
                    	<div class="row">
                    		<!-- <div class="col-md-6">
								<div class="profile-details text-center">
									<div class="profile-img"><img src="{{ getProfilePath($user->image,'profile')}}" alt=""></div>
									<div class="aouther-school">
										<h2>{{ $user->name}}</h2>
										<p><span>{{$user->email}}</span></p>
									</div>
								</div>
							</div> -->
							<div class="col-md-12">
								<div class="profile-details">
								<div class="row">
									<div class="col-md-5">
										<p class="planhead">Plan Name</p>
									</div>
									<div class="col-md-2">
										<p class="planhead">:</p>
									</div>
									<div class="col-md-5">
										<p class="planhead" id="plannameprofile"></p>
									</div>
								</div>
								<div class="row">
									<div class="col-md-5">
										<p class="planhead">Amount($)</p>
									</div>
									<div class="col-md-2">
										<p class="planhead">:</p>
									</div>
									<div class="col-md-5" >
										<p class="planhead" id="planamountprofile"></p>
									</div>
								</div>
								<div class="row">
									<div class="col-md-5">
										<p class="planhead">Plan Expired</p>
									</div>
									<div class="col-md-2">
										<p class="planhead">:</p>
									</div>
									<div class="col-md-5">
										<p class="planhead" id="planexpiredprofile"></p>
									</div>
								</div>
								<div class="row">
									<div class="col-md-5">
										<p class="planhead">Status</p>
									</div>
									<div class="col-md-2">
										<p class="planhead">:</p>
									</div>
									<div class="col-md-5">
										<p class="planhead" id="planstatusprofile"></p>
									</div>
								</div>
								<div class="row">
									<div class="col-md-5">
										<p class="planhead">Plan Description:</p>
									</div>
									<div class="col-md-2">
										<p class="planhead">:</p>
									</div>
									<div class="col-md-5">
										<p class="planhead" id="plandesprofile"></p>
									</div>
								</div>	
								</div>							
							</div>
						</div>
						<hr>
						<h3 class="profile-details-title">{{ getPhrase('plan_history')}}</h3>
						<div class="list celled " id="plandetails1">
                            
                        </div>
					</div>
				</div>
			</section>
			<section class="col-sm-12 col-md-12 col-lg-12 col-xl-5 panel-wrap panel-grid-item">
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                        <h2 class="pull-left"> {{ $title }} </h2>
                        
                        <!--Start panel icons-->
                        <div class="panel-icons panel-icon-slide bgc-white-dark">
                            <ul>
                                <li><a href=""><i class="fa fa-angle-left"></i></a>
                                    <ul>
                                        <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                                        <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                                        <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
                                        <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
                                        <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li>
                                        <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <!--End panel icons-->
                    </div>
                    <div class="panel-body panel-body-p">
                    	<div class="list celled planscroll" id="plandetails">
                            
                        </div>
                    </div>
                </div>
            </section>
		</div>
	</section>
</div>
<div class="modal" id="planmanagementmodal">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Plan Management</h5>
                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form-validation" id="plan_management_form">
                    <input type="hidden" id="planname">
                    <input type="hidden" id="amount">
                    <input type="hidden" id="description">
                    <input type="hidden" id="validity">
                   	<div class="row">
						<div class="col-md-6">
							<p class="planhead">Plan Name</p>
						</div>
						<div class="col-md-6">
							<p class="planhead" id="plannametext"></p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<p class="planhead">Validity(DAYS)</p>
						</div>
						<div class="col-md-6">
							<p class="planhead" id="validitytext"></p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<p class="planhead">Amount($)</p>
						</div>
						<div class="col-md-6">
							<p class="planhead" id="amounttext"></p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<p class="planhead">Description</p>
						</div>
						<div class="col-md-6">
							<p class="planhead" id="descriptiontext"></p>
						</div>
					</div>
					<hr>
                	<div class="row">
	                    <div class="col-md-6">
	                        <div class="form-group mb-4">
	                            <label for="planname">Payment Mode <span style="color: red"> *</span></label>
	                            <select class="form-control" id="paymentmode">
	                            	<option value="" selected disabled>Select</option>
	                            	<option value="1">Online</option>
	                            	<option value="2">Ofline</option>
	                            </select>
	                        </div>
	                    </div>
	                    <div class="col-md-6">
	                        <div class="form-group mb-4">
	                            <label for="planname">Remarks<span style="color: red"> *</span></label>
	                            <input type="text" class="form-control" id="remarks" placeholder="Remarks">
	                        </div>
	                    </div>
                	</div>
            	</form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="paynow();">Submit</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
@endsection
 

@section('footer_scripts')
<script>
	var appname;
	$(document).ready(function(){
        appname = '<?php echo $app_name; ?>';
        getPlans();
        getUserDetails();
        getPlansHistory();
    });
    function getPlans(){
    	var data = {
    		"app_name" : appname
    	}
    	$.ajax({
              url: "{{SASS_URL}}/getUserPlan",
              headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
              },
              type: "POST",
              data: data,
              success: function(success) {
              	//$('#members').html(JSON.stringify(success));
              	//console.clear();
              	
              	$('#plandetails').empty();
              	var data = success.allProducts;
              	var length = data.length;
              	var i;
              	for(i=0;i<length;i++){
              		$('#plandetails').append('<div class="item py-2"><img class="rounded-circle bgc-white-darkest image" alt="" src="{{PREFIX1}}/dollor.png"><div class="content"><a href="" class="header">'+data[i].plan_name+'</a><div class="description">'+data[i].description+'</div></div><div class="pull-right content"><div class="btn btn-primary" onclick="updateplan(\''+data[i].plan_name+'\',\''+data[i].amount+'\',\''+data[i].description+'\',\''+data[i].validity+'\')" data-toggle="modal" data-target="#planmanagementmodal">$ '+data[i].amount+'</div></div></div>');
              	}
              },
              dataType: "json",
              timeout: 2000
        })
    }
    function updateplan(planname,amount,description,validity){
    	$('#planname').val(planname);
    	$('#plannametext').html(planname);
    	$('#amount').val(amount);
    	$('#amounttext').html(amount);
    	$('#description').val(description);
    	$('#descriptiontext').html(description);
    	$('#validity').val(validity);
    	$('#validitytext').html(validity);
    }

    function paynow(){
    	var paymentmode  = $('#paymentmode').val();
    	var remarks = $('#remarks').val();
    	if(paymentmode == "" || remarks == ""){
    		swal("Please fill all fields!","","error");
    		return false;
    	}
    	var data = {
    		"plan_name" : $('#planname').val(),
    		"validity" : $('#validity').val(),
    		"description" : $('#description').val(),
    		"amount" : $('#amount').val(),
    		"app_name" : appname,
    		"paymentmode" : $('#paymentmode').val(),
    		"remarks" : $('#remarks').val(),
    		"paymentdate" : moment().format('DD-MM-YYYY')
    	}
    	$.ajax({
              url: "{{SASS_URL}}/planPayment",
              headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
              },
              type: "POST",
              data: data,
              success: function(success) {
              	//$('#members').html(JSON.stringify(success));
              	//console.clear();
              	swal(success.msg,"","success");
              	$('#planmanagementmodal').modal('hide');
                $('.modal-backdrop').remove();
                $("#plan_management_form").trigger("reset");
               	getPlans();
		        getUserDetails();
		        getPlansHistory();
              },
              dataType: "json",
              timeout: 2000
        })
    }
    function getUserDetails(){
    	var data = {
    		"app_name" : appname
    	}
    	$.ajax({
              url: "{{SASS_URL}}/getSchoolUserDetails",
              headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
              },
              type: "POST",
              data: data,
              success: function(success) {
              	//$('#members').html(JSON.stringify(success));
              	//console.clear();
              	console.log(JSON.stringify(success));
              	var data = success.allProducts;
              	$('#plannameprofile').html(data[0].planname);
              	$('#planamountprofile').html(data[0].amount);
              	$('#planexpiredprofile').html(data[0].expired);
              	if(data[0].planstatus == "0"){
              		$('#planstatusprofile').html("Waiting for Confirmation");
              		$('#planstatusprofile').addClass("planwarning");
              	}else if(data[0].planstatus == "1"){
          			$('#planstatusprofile').html("Active");
          			$('#planstatusprofile').addClass("planactive");
              	}
              	
              	$('#plandesprofile').html(data[0].plandescription);
              },
              dataType: "json",
              timeout: 2000
        })
    }
    function getPlansHistory(){
    	var data = {
    		"app_name" : appname
    	}
    	$.ajax({
              url: "{{SASS_URL}}/getUserPlanHistory",
              headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
              },
              type: "POST",
              data: data,
              success: function(success) {
              	//$('#members').html(JSON.stringify(success));
              	//console.clear();
              	console.log(JSON.stringify(success));
              	$('#plandetails1').empty();
              	var data = success.allProducts;
              	var length = data.length;
              	var i;
              	for(i=0;i<length;i++){
              		$('#plandetails1').append('<div class="item py-2"><img class="rounded-circle bgc-white-darkest image" alt="" src="{{PREFIX1}}/dollor.png"><div class="content"><div class="header" style="font-weight: bold;">Plan Name :'+data[i].plan_name+'</div><div class="header" style="font-weight: bold;">Validity(Days) :'+data[i].validity+'</div></div><div class="pull-right content"><div class="" style="font-weight: bold;">Payment Date :'+data[i].paymentdate+'</div><div class="" style="font-weight: bold;">Amount :'+data[i].amount+'</div></div></div>');
              	}
              },
              dataType: "json",
              timeout: 2000
        })
    }
</script> 
 

@stop
