 @extends($layout)
 @section('header_scripts')
 <script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyBa6mNrfx2rsr-S_7b5Asji6DW4E1aJVd8&libraries=places" async defer></script>
<style>
	.planhead{
		font-size: 20px;
		font-weight: bold;
	}
	.planscroll {
    	min-height: 40em;
		max-height: 40em;
		overflow-y: scroll;
	}
	.planwarning{
		color: red;
	}
	.planactive{
		color: green;
	}
  #description {
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
      }

      #infowindow-content .title {
        font-weight: bold;
      }

      #infowindow-content {
        display: none;
      }

      #map #infowindow-content {
        display: inline;
      }

      .pac-card {
        margin: 10px 10px 0 0;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        background-color: #fff;
        font-family: Roboto;
      }
      .pac-container {
    z-index: 9999999999 !important;
}

      .pac-controls {
        display: inline-block;
        padding: 5px 11px;
      }

      .pac-controls label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 400px;
      }
      #pac-input:focus {
        border-color: #4d90fe;
      }
      #title {
        color: #fff;
        background-color: #4d90fe;
        font-size: 25px;
        font-weight: 500;
        padding: 6px 12px;
      }
      #target {
        width: 345px;
      }

.ui-front {
    z-index: 9999;
}

.pac-container:after {
    /* Disclaimer: not needed to show 'powered by Google' if also a Google Map is shown */

    background-image: none !important;
    height: 0px;
}
/* //==Dimension font-sizes */
.dim_size{
    font-size:14px;
}

.no_pad{
    padding: 0px;
}
</style>
@stop
@section('content')
<div id="page-wrapper">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
    	<div class="container-fluid content-wrap">
	       <section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item">
      <!--Start Panel-->
        <div class="panel bgc-white-dark">
          <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
              <h2 class="pull-left"> {{ $title }} </h2>
              
              <!--Start panel icons-->
              <div class="panel-icons panel-icon-slide bgc-white-dark">
                  <ul>
                      <li><a href=""><i class="fa fa-angle-left"></i></a>
                          <ul>
                              <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                              <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                              <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
                              <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
                              <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li>
                              <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li>
                          </ul>
                      </li>
                  </ul>
              </div>
              <!--End panel icons-->
          </div>
          <div class="panel-body panel-body-p">
            <form id="register_form">
              <!-- <div class="row"> -->
                  <fieldset class="form-group col-sm-6">
                      {{ Form::label('schoolname', getphrase('school_name')) }}
                      <span class="text-red">*</span>
                      <div class="input-group  mb-2 mr-2 mb-0">
                          <input type="text" class="form-control form-control-lg" id="schoolname" placeholder="School Name" onkeypress="return onlyalphabetValidation(event)">
                      </div>
                  </fieldset>
                  <fieldset class="form-group col-sm-6">
                      {{ Form::label('board', getphrase('board')) }}
                      <span class="text-red">*</span>
                      <div class="input-group mb-2 mr-2 mb-0">
                          <select class="form-control form-control-lg" id="schoolboard">
                              <option value="" selected disabled>Select Board</option>
                              <option value="CBSE">CBSE</option>
                              <option value="ICSEBoard">ICSE Board</option>
                              <option value="IB">IB</option>
                              <option value="StateBoard">State Board</option>
                              <option value="IGCSE">IGCSE</option>
                              <option value="CIE">CIE</option>
                              <option value="NIOS">NIOS</option>
                          </select>
                      </div>
                  </fieldset>
                  <fieldset class="form-group col-sm-6">
                      {{ Form::label('country', getphrase('country')) }}
                      <span class="text-red">*</span>
                      <div class="input-group mb-2 mr-2 mb-0">
                          <select id="country" name ="country"  class="form-control form-control-lg" onchange="populateStates()"></select>
                      </div>
                  </fieldset>
                  <fieldset class="form-group col-sm-6">
                      {{ Form::label('state', getphrase('state')) }}
                      <span class="text-red">*</span>
                      <div class="input-group mb-2 mr-2 mb-0">
                          <select id ="state" name ="state" class="form-control form-control-lg"></select>
                      </div>
                  </fieldset>
                  <fieldset class="form-group col-sm-6">
                      {{ Form::label('addr1', getphrase('address_line_1')) }}
                      <span class="text-red">*</span>
                      <div class="input-group mb-2 mr-2 mb-0">
                          <input type="text" class="form-control form-control-lg" id="addr1" placeholder="Address Line1" onkeypress="return addressValidation(event)">
                      </div>
                  </fieldset>
                  <fieldset class="form-group col-sm-6">
                      {{ Form::label('addr2', getphrase('address_line_2')) }}
                      <span class="text-red">*</span>
                      <div class="input-group mb-2 mr-2 mb-0">
                          <input type="text" class="form-control form-control-lg" id="addr2" placeholder="Address Line2" onkeypress="return addressValidation(event)">
                      </div>
                  </fieldset>
                  <fieldset class="form-group col-sm-6">
                      {{ Form::label('city', getphrase('city')) }}
                      <span class="text-red">*</span>
                      <div class="input-group mb-2 mr-2 mb-0">
                          <input type="text" class="form-control form-control-lg" id="city" placeholder="City" onkeypress="return onlyalphabetValidation(event)">
                      </div>
                  </fieldset>
                  <fieldset class="form-group col-sm-6">
                      {{ Form::label('pincode', getphrase('postal_code')) }}
                      <span class="text-red">*</span>
                      <div class="input-group mb-2 mr-2 mb-0">
                          <input type="text" class="form-control form-control-lg" id="pincode" placeholder="Postalcode" onkeypress="return onlynumberValidation(event)">
                      </div>
                  </fieldset>
                  <fieldset class="form-group col-sm-6">
                      {{ Form::label('contactpersonname', getphrase('contact_person_name')) }}
                      <span class="text-red">*</span>
                      <div class="input-group mb-2 mr-2 mb-0">
                          <input type="text" class="form-control form-control-lg" id="contactpersonname" placeholder="Contact Person Name" onkeypress="return onlyalphabetValidation(event)">
                      </div>
                  </fieldset>
                  <fieldset class="form-group col-sm-6">
                      {{ Form::label('emailid', getphrase('emailid')) }}
                      <span class="text-red">*</span>
                      <div class="input-group mb-2 mr-2 mb-0">
                          <input type="text" class="form-control form-control-lg" id="emailid" placeholder="Contact Person EmailId">
                      </div>
                  </fieldset>
                  <fieldset class="form-group col-sm-6">
                      {{ Form::label('mobileno', getphrase('mobile_number')) }}
                      <span class="text-red">*</span>
                      <div class="input-group mb-2 mr-2 mb-0">
                          <input type="text" class="form-control form-control-lg" id="mobileno" placeholder="Contact Person Mobile Number" onkeypress="return onlynumberValidation(event)">
                      </div>
                  </fieldset>
                  <fieldset class="form-group col-sm-6">
                      {{ Form::label('landline', getphrase('landline_number')) }}
                      <div class="input-group mb-2 mr-2 mb-0">
                          <input type="text" class="form-control form-control-lg" id="landlineno" placeholder="Landline Number" onkeypress="return onlynumberValidation(event)">
                      </div>
                  </fieldset>
                  <fieldset class="form-group col-sm-12">
                      {{ Form::label('landline', getphrase('school_geo_location')) }}
                      <div class="input-group mb-2 mr-2 mb-0">
                          <input type="text" class="form-control form-control-lg" id="geoaddress" placeholder="School Geo Location" data-toggle="modal" data-target="#map" onclick="setLocation()">
                          <input type="hidden" id="lat">
                          <input type="hidden" id="lng">
                      </div>
                  </fieldset>
                  <div class="buttons text-center">
                      <button type="button" class="btn btn-lg btn-primary button" onclick="updateDetails();">{{ getPhrase('update') }}</button>
                  </div>
          </form>
          </div>
        </div>
      </section> 	
		  </div>
	</section>
</div>
<div class="modal fade" id="map" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content" style="width: 50em;margin-left: -15em;">
                <div class="modal-header text-center">
                    <h4 class="modal-title" id="myModalLabel">{{getPhrase('get_location')}}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                  <div class="row">
                    <div class="col-md-12">
                      <!-- <input  class="form-control"   id="pac-input" ng-model="routeData.gps_coord"  min="0" /> -->
                      <div id="map_canvas" style="height: 500px;width: 100%;margin: 0.6em;padding-top: 10px;"></div>
              </div>
                  </div>
            </div>
            </div>
                
            </div>
        </div>
@endsection
 

@section('footer_scripts')
 <script src="{{JS}}countries.js"></script>

<script>
        populateCountries();
        var appname;
  $(document).ready(function(){
        appname = '<?php echo $app_name; ?>';
        getUserDetails();        
    });
  function initialize(id) {
      var input = document.getElementById(id);
      var autocomplete = new google.maps.places.Autocomplete(input);
      autocomplete.addListener('place_changed', function () {
      var place = autocomplete.getPlace();
      // place variable will have all the information you are looking for.
      $('#lat').val(place.geometry['location'].lat());
      $('#lng').val(place.geometry['location'].lng());
    });
  }

  function setLocation(){
    $(function () {
              
        var lat = 13.0827,
            lng = 80.2707,
            latlng = new google.maps.LatLng(lat, lng),
            new_image = '../../images/search_pin.png',
            image = 'http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png';;

            
        //zoomControl: true,
        //zoomControlOptions: google.maps.ZoomControlStyle.LARGE,

        
        var mapOptions = {
            center: new google.maps.LatLng(lat, lng),
            zoom: 15,
            maxZoom: 17,
            //mapTypeId: google.maps.MapTypeId.ROADMAP,
            panControl: false,
            mapTypeControl: false,
            streetViewControl: false,
            panControlOptions: {
                position: google.maps.ControlPosition.TOP_RIGHT
            },
            zoomControl: true,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.LARGE,
                position: google.maps.ControlPosition.TOP_left
            },
            // types: ['geocode'],
            // componentRestrictions: {country: 'om'}
           

        }
         map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions),
            marker = new google.maps.Marker({
                position: latlng,
                map: map,
                icon: image
            });
            // marker.setMap(null);   
              var infowindow = new google.maps.InfoWindow();
              //map.panTo(latlng);
              var pacinput = '<input id="pac-input" class="form-control" type="text" style="border: groove;margin-top: 20px;">';
        $('#map_canvas').append(pacinput);
              // Create the search box and link it to the UI element.
       var input = document.getElementById('pac-input');
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
       var autocomplete = new google.maps.places.Autocomplete(input,{ types: ['geocode'] });
      google.maps.event.addListener(autocomplete,'place_changed', function () {
      var place = autocomplete.getPlace();
      var lat = place.geometry['location'].lat(),
            lng = place.geometry['location'].lng(),
            placeName = "",
            latlng = new google.maps.LatLng(lat, lng);

            // $('#lat'+id).val(results[0].geometry.location.lat());
            // $('#lng'+id).val(results[0].geometry.location.lng());
        moveMarker(placeName, latlng);
      // place variable will have all the information you are looking for.
      $('#lat').val(place.geometry['location'].lat());
      $('#lng').val(place.geometry['location'].lng());
      var geocoder = new google.maps.Geocoder();
                geocoder.geocode({
                    "latLng":place.geometry.location
                }, function (results, status) {
                    console.log(results, status);
                    if (status == google.maps.GeocoderStatus.OK) {
                        $("#geoaddress").val(results[0].formatted_address);
                        //addwarehouse();
                    }
                });
    });
     
      
        google.maps.event.addListener(map, 'click', function (event) {
            
            $('.MapLat').val(event.latLng.lat());
            $('.MapLon').val(event.latLng.lng());
            infowindow.close();
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({
                        "latLng":event.latLng
                    }, function (results, status) {
                        console.log(results, status);
                        if (status == google.maps.GeocoderStatus.OK) {
                            console.log(results);
                            var lat = results[0].geometry.location.lat(),
                                lng = results[0].geometry.location.lng(),
                                placeName = results[0].address_components[0].long_name,
                                latlng = new google.maps.LatLng(lat, lng);
                                $('#lat').val(results[0].geometry.location.lat());
                    $('#lng').val(results[0].geometry.location.lng());
                    $('#geoaddress').val(results[0].formatted_address);
                            moveMarker(placeName, latlng);
                        }
                    });
        });
       
        function moveMarker(placeName, latlng) {
            marker.setIcon(image);
            marker.setPosition(latlng);
            infowindow.setContent(placeName);
             map.setCenter(latlng);
        }
            $("#reset_state").click(function() {
                infowindow.close();
                map.fitBounds(latlng);
              })
            

    });
}
    
    function getUserDetails(){
      
      var data = {
        "app_name" : appname
      }
      $.ajax({
              url: "{{SASS_URL}}/getSchoolUserDetails",
              headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
              },
              type: "POST",
              data: data,
              success: function(success) {
                //$('#members').html(JSON.stringify(success));
                //console.clear();
                console.log(JSON.stringify(success));
                var data = success.allProducts;
                $('#schoolname').val(data[0].school_name);
                $('#schoolboard').val(data[0].board).change();
                $("#country").val(data[0].country).change();
                $("#state").val(data[0].state).change();
                $('#addr1').val(data[0].addressline1);
                $('#addr2').val(data[0].addressline2);
                $('#city').val(data[0].city);
                $('#pincode').val(data[0].pincode);
                $('#contactpersonname').val(data[0].contactperson);
                $('#emailid').val(data[0].emailid);
                $('#mobileno').val(data[0].mobilenumber);
                $('#landlineno').val(data[0].landline);
                $('#geoaddress').val(data[0].geo_address);
                $('#lat').val(data[0].geo_lat);
                $('#lng').val(data[0].geo_lng);
              },
              dataType: "json",
              timeout: 2000
        })
    }

        function emailValidation(email) {
            //var mailformat =/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            var mailformat =/\b[a-zA-Z0-9\u00C0-\u017F._%+-]+@[a-zA-Z0-9\u00C0-\u017F.-]+\.[a-zA-Z]{2,}\b/;
            //var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            
            if (email == '') {
                return 0;
            } else {
                if(email.match(mailformat)){
                    return 1;
                }
                else{
                    return -1;
                }
            }
            
        }
        function onlyalphabetValidation(e) {

            var regex = new RegExp("^[a-zA-Z \b]+$");
            var key = String.fromCharCode(!e.charCode ? e.which : e.charCode);   
            var keystobepassedout="ArrowLeftArrowRightDeleteBackspaceTab";
            if(keystobepassedout.indexOf(e.key)!=-1)
            {
              return true;
            }
            if (!regex.test(key)) {
                e.preventDefault();
                return false;
            }
        }
         
        function addressValidation(e) {

            var regex = new RegExp("^[a-zA-Z0-9\s\,\''\-\/ ]*$");
            var key = String.fromCharCode(!e.charCode ? e.which : e.charCode);   
            var keystobepassedout="ArrowLeftArrowRightDeleteBackspaceTab";
            if(keystobepassedout.indexOf(e.key)!=-1)
            {
              return true;
            }
            if (!regex.test(key)) {
                e.preventDefault();
                return false;
            }
        }
        function onlyalphaNumaricValidation(e) {

            var regex = new RegExp("^[a-zA-Z0-9 \b]+$");
            var key = String.fromCharCode(!e.charCode ? e.which : e.charCode);   
            var keystobepassedout="ArrowLeftArrowRightDeleteBackspaceTab";
            if(keystobepassedout.indexOf(e.key)!=-1)
            {
              return true;
            }
            if (!regex.test(key)) {
                e.preventDefault();
                return false;
            }
        }
        function onlynumberValidation(evt) {

                 var charCode = (evt.which) ? evt.which : event.keyCode;

                 if (charCode > 31 && (charCode < 48 || charCode > 57)){
                    return false;
                 }
                    

                 return true;
        }

        function updateDetails(){
            var schoolname = $('#schoolname').val();
            var schoolboard = $('#schoolboard').val();
            var country = $('#country').val();
            var state = $('#state').val();
            var addr1 = $('#addr1').val();
            var addr2 = $('#addr2').val();
            var city = $('#city').val();
            var pincode = $('#pincode').val();
            var contactpersonname = $('#contactpersonname').val();
            var emailid = $('#emailid').val();
            var mobileno = $('#mobileno').val();
            var landlineno = $('#landlineno').val();
            var geoaddress = $('#geoaddress').val();
            var geolat = $('#lat').val();
            var geolng = $('#lng').val();
            var mailformat = emailValidation(emailid);
            if(schoolname == "" || schoolboard == "" || country == 0 || state == 0
                || addr1 == "" || addr2 == "" || city == "" || pincode == "" || contactpersonname == "" || mobileno == ""){
                swal("Please fill all manditory fields","","error");
                return false;
            }else if (mailformat == 0) {
                swal("Please Enter Email Address","","error");
                return false;
            }else if(geoaddress == "" || geolat == "" || geolng == ""){
              swal("Please select school geo location");
              return false;
            }
            else{
                if(mailformat == -1){
                    swal("Please Enter Valid Email Address", "example@example.com","error");
                    return false;
                }
            }
            var data = {
                "school_name": schoolname,
                "addressline1": addr1, 
                "addressline2": addr2,
                "city": city,
                "pincode":pincode,
                "state" : state,
                "country" : country,
                "board":schoolboard,
                "contactperson":contactpersonname,
                "mobilenumber":mobileno,
                "landline" : landlineno,
                "emailid" : emailid,
                "geo_address" : geoaddress,
                "geo_lat" : geolat,
                "geo_lng" : geolng,
                "app_name" :appname
            }
            $.ajax({
              url: "{{SASS_URL}}/updateSchoolDetails",
              headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
              },
              type: "POST",
              data: data,
              success: function(success) {
                var status = success.status;
                  swal(success.msg,"","success");
                   $("#register_form").trigger("reset");
                   getUserDetails();

                   $.ajax({
                      url: "{{TRACK_URL}}/setSchoolLocation",
                      headers: {
                      'Content-Type': 'application/x-www-form-urlencoded'
                      },
                      type: "POST",
                      data: {"app_name" : appname, "lat" : geolat,"lan" : geolng ,"addr" :geoaddress},
                      success: function(success) {
                        var status = success.status;
                      },
                      dataType: "json",
                      timeout: 2000
                })
              },
              dataType: "json",
              timeout: 2000
        })
        }

     </script>
<script>
	
</script> 
 

@stop
