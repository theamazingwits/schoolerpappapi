@extends($layout)
@section('header_scripts')
<link href="{{CSS}}ajax-datatables.css" rel="stylesheet">
@stop
@section('content')

<div id="page-wrapper">
           <section id="main" class="main-wrap bgc-white-darkest" role="main">
            <div class="container-fluid content-wrap">
                <!-- Page Heading -->
                <div class="">
                    <div class="col-lg-12">
                        <ol class="breadcrumb">
                            <li><a href="{{PREFIX}}"><i class="bc-home fa fa-home"></i></a> </li>
                            <li>{{$title}}</li>
                        </ol>
                    </div>
                </div>
                <div class=" panel-grid" id="panel-grid">   
                <section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item">
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                        <h2 class="pull-left"> {{ $title }} </h2>
                        
                        <!--Start panel icons-->
                        <div class="panel-icons panel-icon-slide bgc-white-dark">
                            <ul>
                                <li><a href=""><i class="fa fa-angle-left"></i></a>
                                    <ul>
                                        <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                                        <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                                        <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
                                        <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
                                        <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li>
                                        <!-- <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li> -->
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <!--End panel icons-->
                    </div>
                    <div class="panel-body panel-body-p">
                        <ul class="list-unstyled notification-list">
                            @foreach($notifications as $notification)
                           
                            <li>
                                <a href="{{URL_NOTIFICATIONS_VIEW.$notification->slug}}">
                                    <h4>{{$notification->title}}</h4>
                                    <p>{{$notification->short_description}}</p> <span class="posted-time">{{getPhrase('posted_on')}} : <i class="fa fa-calendar"></i> {{ $notification->updated_at}}</span> </a>
                            </li>
                            @endforeach
                            
                        </ul>
                            {!! $notifications->links() !!}
                        
                    </div>
                </div>
            </section>
                <!-- /.row -->
            </div>
            </div>
        </section>
            <!-- /.container-fluid -->
        </div>
@endsection
 
@section('footer_scripts')
  
 

@stop