 @extends($layout)
@section('content')


{{-- User Certificates --}}
<?php
    
   $crecords        = App\UserCertificates::DocumetsSubmitted();
   $user_submites   = App\UserCertificates::where('user_id',Auth::user()->id)
                                           ->where('is_submitted',1)
                                           ->orWhere('is_approved',1)
                                           ->groupby('notification_id')
                                           ->pluck('notification_id')
                                           ->toArray();

?>  

@if($crecords)                                         

   @if(count($user_submites) > 0 )

   	  @foreach ($crecords as $key => $value)
   	     
         @if(in_array($value['id'], $user_submites))

         @else

           <div class="alert alert-danger">
			  <strong>Note:</strong> {{$value['description']}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="{{URL_UPLOAD_USER_CERTIFICATES.$value['id']}}" class="btn btn-primary btn-sm">Click here to upload</a>
			</div>

         @endif

   	   
      @endforeach

   @else
     
       @foreach ($crecords as $crecord)
   	      
          <div class="alert alert-danger">
			  <strong>Note:</strong> {{$crecord['description']}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="{{URL_UPLOAD_USER_CERTIFICATES.$crecord['id']}}" class="btn btn-primary btn-sm">Click here to upload</a>
			</div>

      @endforeach

  @endif 

@endif 
<section id="main" class="main-wrap bgc-white-darkest" role="main">
 	<div class="container-fluid content-wrap">
    	<div class="row panel-grid grid-stack" id="panel-grid">
    		<section data-gs-min-width="4" data-gs-min-height="20" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                <!--Start Panel-->
                <div class="panel pb-0 bgc-white-dark">
                    <div class="panel-header grid-stack-handle bgc-white-dark panel-header-p panel-header-sm">
                        <h3 class="pull-left fs-2 fw-light">  {{ getPhrase('quiz_categories')}}</h3>
                    </div>
                    <div class="panel-body pt-2">
                        <div class="h-md pos-r panel-body-p py-0">
                            <div class="row">
                        		<div class="col-md-8">
                        			<h3 class="lh-0 fs-1 fw-light">{{ App\QuizCategory::get()->count()}}</h3>
                        		</div>
                        		<div class="col-md-4">
									<i class="pull-right fw-light fa fa-random fs-1"></i>
                        		</div>
                        	</div>
                            <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{URL_STUDENT_EXAM_CATEGORIES}}">
                                <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
                                    <i class="icon-directions fs-4"></i>
                                </span>
                                <span class="d-inline-block align-top lh-7">
                                    <span class="fs-6 d-block">{{ getPhrase('view_all')}}</span>
                                    <span class="c-gray fs-7">Click to View more details</span>
                                </span>
                                <span class="pull-right c-gray mt-1 lh-5"></span>
                            </a>
                        </div>
                    </div>
                </div>
                <!--End Panel-->
            </section>
            <section data-gs-min-width="4" data-gs-min-height="20" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                <!--Start Panel-->
                <div class="panel pb-0 bgc-white-dark">
                    <div class="panel-header grid-stack-handle bgc-white-dark panel-header-p panel-header-sm">
                        <h3 class="pull-left fs-2 fw-light">{{ getPhrase('quizzes')}}</h3>
                    </div>
                    <div class="panel-body pt-2">
                        <div class="h-md pos-r panel-body-p py-0">
                        	<div class="row">
                        		<div class="col-md-8">
                        			<h3 class="lh-0 fs-1 fw-light"> {{App\Quiz::get()->count()}}</h3>
                        		</div>
                        		<div class="col-md-4">
									<i class="pull-right fw-light fa fa-random fs-1"></i>
                        		</div>
                        	</div>
                            
                            <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{URL_STUDENT_EXAM_ALL}}">
                                <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
                                    <i class="icon-directions fs-4"></i>
                                </span>
                                <span class="d-inline-block align-top lh-7">
                                    <span class="fs-6 d-block">{{ getPhrase('view_all')}}</span>
                                    <span class="c-gray fs-7">Click to View more details</span>
                                </span>
                                <span class="pull-right c-gray mt-1 lh-5"></span>
                            </a>
                        </div>
                    </div>
                </div>
                <!--End Panel-->
            </section>
            <section data-gs-min-width="4" data-gs-min-height="20" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                <!--Start Panel-->
                <div class="panel pb-0 bgc-white-dark">
                    <div class="panel-header grid-stack-handle bgc-white-dark panel-header-p panel-header-sm">
                        <h3 class="pull-left fs-2 fw-light"> {{ getPhrase('children')}}</h3>
                    </div>
                    <div class="panel-body pt-2">
                        <div class="h-md pos-r panel-body-p py-0">
                        	<div class="row">
                        		<div class="col-md-8">
                        			<h3 class="lh-0 fs-1 fw-light"> {{ App\User::where('parent_id', '=', $user->id)->get()->count()}}</h3>
                        		</div>
                        		<div class="col-md-4">
									<i class="pull-right fw-light fa fa-random fs-1"></i>
                        		</div>
                        	</div>
                            
                            <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{URL_PARENT_CHILDREN}}">
                                <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
                                    <i class="icon-directions fs-4"></i>
                                </span>
                                <span class="d-inline-block align-top lh-7">
                                    <span class="fs-6 d-block">{{ getPhrase('view_all')}}</span>
                                    <span class="c-gray fs-7">Click to View more details</span>
                                </span>
                                <span class="pull-right c-gray mt-1 lh-5"></span>
                            </a>
                        </div>
                    </div>
                </div>
                <!--End Panel-->
            </section>
        </div>
        <div class="row panel-grid grid-stack" id="panel-grid">
            <section class="col-sm-12 col-md-12 col-xl-6 panel-wrap panel-grid-item grid-stack-item">
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix grid-stack-handle panel-header-p bgc-white-dark panel-header-sm">
                        <h3 class="pull-left fs-2 fw-light">
                        	<!-- <i class="menu-icon fa fa-book"></i> -->
                        {{getPhrase('latest_quizzes')}} </h3>
                        <!--Start panel icons-->
                        <div class="panel-icons panel-icon-slide ">
                            <ul>
                                <li><a href=""><i class="fa fa-angle-left"></i></a>
                                    <ul>
                                        <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                                        <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <!--End panel icons-->
                    </div>
                    <div class="panel-body panel-body-p">
                       @if(!count($latest_quizzes))
					      <br>
					 		 <p> &nbsp;&nbsp;&nbsp;{{getPhrase('no_quizzes_available')}}</p>
					 		 <p> &nbsp;&nbsp;&nbsp; <a href="{{URL_USERS_SETTINGS.Auth::user()->slug}}">{{getPhrase('click_here')}}</a> {{getPhrase('to_change_your_settings')}}</p>
					 	@else
				    	<table class="table ">
				    		<thead>
				    			<tr>
				    				<th><strong>{{getPhrase('title')}}</strong></th>
					    			<th><strong>{{getPhrase('type')}}</strong></th>
					    			<th><strong>{{getPhrase('Action')}}</strong></th>
				    			</tr>

				    		</thead>
				    		<tbody class="referral-list">
					    		@foreach($latest_quizzes as $quiz)
					 			<tr>
					 				<td>{{$quiz->title}}</td>
					 				<td>
					 				@if($quiz->is_paid)
					 					<span class="label label-danger">{{getPhrase('paid')}}
					 					</span>
				 					@else
				 					<span class="label label-success">{{getPhrase('free')}}
					 					</span>
				 					@endif
					 				</td>
					 				<td>
					 				@if($quiz->is_paid)
					 					<a href="{{URL_PAYMENTS_CHECKOUT.'exam/'.$quiz->slug}}">{{getPhrase('buy_now')}}</a> 
				 					@else
				 					-
				 					@endif
					 				</td>
					 			</tr>
					 			@endforeach
			    			</tbody>
		    			</table>
		    			@endif
		    		</div>
    				<!--End Panel-->
    			</div>
			</section>
			<section class="col-sm-12 col-md-12 col-xl-6 panel-wrap panel-grid-item grid-stack-item">
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix grid-stack-handle panel-header-p bgc-white-dark panel-header-sm">
                        <h3 class="pull-left fs-2 fw-light">
                        	<!-- <i class="menu-icon fa fa-book"></i> -->
                        {{getPhrase('latest')}} LMS {{getPhrase('series')}} </h3>
                        <!--Start panel icons-->
                        <div class="panel-icons panel-icon-slide ">
                            <ul>
                                <li><a href=""><i class="fa fa-angle-left"></i></a>
                                    <ul>
                                        <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                                        <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <!--End panel icons-->
                    </div>
                    <div class="panel-body panel-body-p">
                       @if(!count($latest_series))
					      <br>
					 		 <p> &nbsp;&nbsp;&nbsp;{{getPhrase('no_series_available')}}</p>
					 		 <p> &nbsp;&nbsp;&nbsp; <a href="{{URL_USERS_SETTINGS.Auth::user()->slug}}">{{getPhrase('click_here')}}</a> {{getPhrase('to_change_your_settings')}}</p>
					 	@else
				    	<table class="table ">
				    		<thead>
				    			<tr>
				    				<th><strong>{{getPhrase('title')}}</strong></th>
					    			<th><strong>{{getPhrase('type')}}</strong></th>
					    			<th><strong>{{getPhrase('Action')}}</strong></th>
				    			</tr>

				    		</thead>
				    		<tbody class="referral-list">
					    		@foreach($latest_series as $series)
					 			<tr>
					 				<td>{{$series->title}}</td>
					 				<td>
					 				@if($series->is_paid)
					 					<span class="label label-danger">{{getPhrase('paid')}}
					 					</span>
				 					@else
				 					<span class="label label-success">{{getPhrase('free')}}
					 					</span>
				 					@endif
					 				</td>
					 				<td>
					 				@if($series->is_paid)
					 					<a href="{{URL_PAYMENTS_CHECKOUT.'lms/'.$series->slug}}">{{getPhrase('buy_now')}}</a> 
				 					@else
				 					-
				 					@endif
					 				</td>
					 			</tr>
					 			@endforeach
			    			</tbody>
		    			</table>
		    			@endif
		    		</div>
    				<!--End Panel-->
    			</div>
			</section>
    	</div>
    </div>
</section>


@stop

@section('footer_scripts')
  
@stop