@extends($layout)
@section('header_scripts')
<link href="{{CSS}}ajax-datatables.css" rel="stylesheet">
@stop
@section('content')

<section id="main" class="main-wrap bgc-white-darkest" role="main">

               
            <div class="container-fluid content-wrap">


                <div class="row panel-grid" id="panel-grid">
                    <div class="col-sm-12 col-md-12 col-lg-12 panel-wrap panel-grid-item grid-stack-item">
                        <!--Start Panel-->
                        <div class="panel bgc-white-dark">
                            <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                                <h2 class="pull-left">
                                {{$title}}</h2>
                         
                            </div>
       
                            <div class="panel-body pan">
                                <div class="page-size-table">
                                  <div>
                        <table class="table table-striped table-bordered datatable" cellspacing="0" width="100%">
                            <thead>
                               <tr> 
                                    <th>{{ getPhrase('date')}}</th>
                                    <th>{{ getPhrase('name')}}</th>
									<th>{{ getPhrase('image')}}</th>
                                    <th>{{ getPhrase('morning')}}</th>
                                    <th>{{ getPhrase('night')}}</th>
                                  
                             	</tr>
                            </thead>
                             
                        </table>
                        </div>
                                    <!-- <table id="table-student-exam-scheduledexam" class="card-view-no-edit page-size-table">
                                        
                                    </table> -->
                                </div>
                            </div>
                        </div>
                        <!--End Panel-->
                    </div>
                </div>
            </div>
        </section>

@endsection
 
 <?php $url = URL_PARENT_CHILDREN_GETHOSTALATTENDANCELIST;
 
  ?>
@section('footer_scripts')
  @include('common.datatables', array('route'=>$url, 'route_as_url' => TRUE))
 
@stop
