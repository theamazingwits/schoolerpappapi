           <div class="row">
			   <fieldset  class="form-group col-md-6">		

                  {{ Form::label('student_name', getphrase('Choose Cildren')) }}
                  <span class="text-red">*</span>
                 {{ Form::select('student_id' ,array_pluck($students, 'name', 'id'),$student_id, $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('select_children'),
                
                 'ng-model'=>'student_id', 

                 'required'=> 'true', 

                 'ng-class'=>'{"has-error": formleave.student_id.$touched && formleave.student_id.$invalid}',

                 )) }}
                <div class="validation-error" ng-messages="formleave.title.$error" >

                {!! getValidationMessage()!!}
 
               </div>
              </fieldset>

           
                </div>

          <div class="row">
                 <?php 
					$options = array('0'=> getPhrase('Sick Leave'), '1'=> getPhrase('Personal Leave')); 
						?>
              <fieldset class="form-group col-md-6">		

                {{ Form::label('leave_type', getphrase('Choose Leave Type')) }}
                <span class="text-red">*</span>
                {{ Form::select('leave_type' ,$options,null, $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('select_type of leave'),

                'ng-model'=>'leave_type', 

                'required'=> 'true', 

                'ng-class'=>'{"has-error": formleave.leave_type.$touched && formleave.leave_type.$invalid}',

                )) }}
                <div class="validation-error" ng-messages="formleave.title.$error" >

                {!! getValidationMessage()!!}

                </div>
                </fieldset>

               <fieldset class="form-group col-md-6">		

               <?php 
					$options = array('0'=> getPhrase('Half Day'), '1'=> getPhrase('Full Day')); 
						?>

                {{ Form::label('leave_time', getphrase('Choose Time')) }}
                <span class="text-red">*</span>
                {{ Form::select('leave_time',$options,null, $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('select_time'),

                'ng-model'=>'leave_time', 

                'required'=> 'true', 

                'ng-class'=>'{"has-error": formleave.leave_time.$touched && formleave.leave_time.$invalid}',

                )) }}
                <div class="validation-error" ng-messages="formleave.title.$error" >

                {!! getValidationMessage()!!}

                </div>
                </fieldset>
                </div>


                <div class="row input-daterange" id="dp">
                    <?php 
                    $date_from = date('Y/m/d');
                    $date_to = date('Y/m/d');
                    if($records)
                    {
                        
                        $date_from = $records->start_date;
                        $date_to = $records->end_date;
                    }
                    ?>
                    <fieldset class="form-group col-md-6">
                        {{ Form::label('start_date', getphrase('Leave start_date')) }}
                        {{ Form::text('start_date', $value = $date_from , $attributes = array('class'=>'input-sm form-control', 'placeholder' => '2015/7/17')) }}
                    </fieldset>

                    <fieldset class="form-group col-md-6">
                        {{ Form::label('end_date', getphrase('Leave end_date')) }}
                        {{ Form::text('end_date', $value = $date_to , $attributes = array('class'=>'input-sm form-control', 'placeholder' => '2015/7/17')) }}
                    </fieldset>
                </div>
                
                <fieldset class="form-group">

                                
                {{ Form::label('leave_reason', getphrase('if any type here')) }}

                {{ Form::textarea('leave_reason', $value = null , $attributes = array('class'=>'form-control', 'rows'=>'5','placeholder' => '(  it is optional)',
                    'ng-model'=>'leave_reason', 

                    'ng-pattern'=>getRegexPattern('name'), 

                    'ng-class'=>'{"has-error": formleave.leave_reason.$touched && formleave.leave_reason.$invalid}',

                    'ng-minlength' => '2',
                    
                    'ng-maxlength' => '40',

                    )) }}

                <div class="validation-error" ng-messages="formleave.leave_reason.$error" >

                    {!! getValidationMessage()!!}

                    {!! getValidationMessage('pattern')!!}

                    {!! getValidationMessage('minlength')!!}

                    {!! getValidationMessage('maxlength')!!}

                </div>
                <fieldset>
                <div class="buttons text-center">

                    <button class="btn btn-lg btn-primary button"

                    ng-disabled='!formleave.$valid'>{{ $button_name }}</button>

                </div>

