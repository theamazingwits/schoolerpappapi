		
					<fieldset class="form-group">		

                       {{ Form::label('student_name', getphrase('Choose Cildren')) }}
                       <span class="text-red">*</span>
                       {{ Form::select('student_name' ,array_pluck($students, 'name', 'id'),$student_id, $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('select_children'),
	                      'ng-model'=>'student_name', 
	 
                          'required'=> 'true', 
	              
				          'ng-class'=>'{"has-error": formAllergyfood.student_name.$touched && formAllergyfood.student_name.$invalid}',
	
	                 )) }}
                      <div class="validation-error" ng-messages="formAllergyfood.title.$error" >
	                  
					   {!! getValidationMessage()!!}
                        
						</div>
                     </fieldset>

				 <fieldset class="form-group">
		
						
						{{ Form::label('allergy_food', getphrase('allergy food')) }}
						
						{{ Form::textarea('allergy_food', $value = null , $attributes = array('class'=>'form-control', 'rows'=>'5','placeholder' => 'allergy food',
							'ng-model'=>'allergy_food', 

							'ng-pattern'=>getRegexPattern('name'), 

							'required'=> 'true', 

							'ng-class'=>'{"has-error": formAllergyfood.allergy_food.$touched && formAllergyfood.allergy_food.$invalid}',

							'ng-minlength' => '2',
                            
                            'ng-maxlength' => '40',

							)) }}

						<div class="validation-error" ng-messages="formAllergyfood.allergy_food.$error" >

	    					{!! getValidationMessage()!!}

	    					{!! getValidationMessage('pattern')!!}

	    					{!! getValidationMessage('minlength')!!}

	    					{!! getValidationMessage('maxlength')!!}
           
						</div>
 					 
						<div class="buttons text-center">

							<button class="btn btn-lg btn-primary button"

							ng-disabled='!formAllergyfood.$valid'>{{ $button_name }}</button>

						</div>

		 