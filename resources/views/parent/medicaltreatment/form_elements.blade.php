          
					<fieldset class="form-group">
					
                      {{ Form::label('student_name', getphrase('Choose Cildren')) }}
                       <span class="text-red">*</span>
                       {{ Form::select('student_name' ,array_pluck($students, 'name', 'id'),$student_id, $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('select_children'),
                         'ng-model'=>'student_name', 

                         'required'=> 'true', 

                         'ng-class'=>'{"has-error": formMedtreat.student_name.$touched && formMedtreat.student_name.$invalid}',

                       )) }}
                      <div class="validation-error" ng-messages="formMedtreat.title.$error" >

                     {!! getValidationMessage()!!}
 
                      </div>
                    </fieldset>

 					 <fieldset class="form-group" >
						
						{{ Form::label('treatment_name', getphrase('Treatment Name')) }}
						<span class="text-red">*</span>
						{{ Form::text('treatment_name', $value = null , $attributes = array('class'=>'form-control','name'=>'treatment_name', 
							'placeholder' => getPhrase('treatment_name'), 
							'ng-model'=>'treatment_name', 
							'required'=> 'true', 
							'id'=>'treatment_name',
							'ng-class'=>'{"has-error": formMedtreat.treatment_name.$touched && formMedtreat.treatment_name.$invalid}',
							'ng-minlength' => '4',
							'ng-maxlength' => '40',
							)) }}
						<div class="validation-error" ng-messages="formMedtreat.treatment_name.$error" >
	    					{!! getValidationMessage()!!}
	    					{!! getValidationMessage('minlength')!!}
	    					{!! getValidationMessage('maxlength')!!}
						</div>
					</fieldset>


					<fieldset class="form-group" >
						{{ Form::label('medicine_name', getphrase('Medicine Name')) }}
						<span class="text-red">*</span>
						{{ Form::text('medicine_name', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('medicine_name'),
							'name'=>'medicine_name',
							'ng-model'=>'medicine_name',
							'id'=>'medicine_name', 
							'required'=> 'true', 
							'ng-minlength' => '2',
							'ng-maxlength' => '20',
							'ng-class'=>'{"has-error": formMedtreat.medicine_name.$touched && formMedtreat.medicine_name.$invalid}',
						 		
						)) }}
						
						<div class="validation-error" ng-messages="formMedtreat.medicine_name.$error" >
	    					{!! getValidationMessage()!!}
	    					{!! getValidationMessage('minlength')!!}
	    					{!! getValidationMessage('maxlength')!!}
						</div>


					
					</fieldset>
					  
			

					
						<div class="buttons text-center" >
							<button class="btn btn-lg btn-primary button" 
							ng-disabled='!formMedtreat.$valid'>{{ $button_name }}</button>
						</div>
		 