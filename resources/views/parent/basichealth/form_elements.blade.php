                        
                <fieldset class="form-group">		

                {{ Form::label('student_name', getphrase('Choose Cildren')) }}
                <span class="text-red">*</span>
                {{ Form::select('student_name' ,array_pluck($students, 'name', 'id'),$student_id, $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('select_children'),
                'ng-model'=>'student_name', 

                'required'=> 'true', 

                'ng-class'=>'{"has-error": formbasichealth.student_name.$touched && formbasichealth.student_name.$invalid}',

                )) }}
                <div class="validation-error" ng-messages="formbasichealth.title.$error" >

                {!! getValidationMessage()!!}
                
                </div>
                </fieldset>

                <fieldset class="form-group">

                
                {{ Form::label('height', getphrase('height')) }}
                
                {{ Form::text('height', $value = null , $attributes = array('class'=>'form-control', 'rows'=>'2','placeholder' => '(e.g) 172 cm',
                    'ng-model'=>'height', 

                    'ng-pattern'=>getRegexPattern('name'), 

                    'required'=> 'true', 

                    'ng-class'=>'{"has-error": formbasichealth.height.$touched && formbasichealth.height.$invalid}',

                    'ng-minlength' => '2',
                    
                    'ng-maxlength' => '10',

                    )) }}

                <div class="validation-error" ng-messages="formbasichealth.height.$error" >

                    {!! getValidationMessage()!!}

                    {!! getValidationMessage('pattern')!!}

                    {!! getValidationMessage('minlength')!!}

                    {!! getValidationMessage('maxlength')!!}

                </div>
                </fieldset>
                <fieldset class="form-group" >
						
						{{ Form::label('weight', getphrase('weight')) }}
						<span class="text-red">*</span>
						{{ Form::text('weight', $value = null , $attributes = array('class'=>'form-control','name'=>'weight', 
							'placeholder' => getPhrase('(e.g) 70kg'), 
							'ng-model'=>'weight', 
							'required'=> 'true', 
							'id'=>'weight',
							'ng-class'=>'{"has-error": formbasichealth.weight.$touched && formbasichealth.weight.$invalid}',
							'ng-minlength' => '2',
							'ng-maxlength' => '10',
							)) }}
						<div class="validation-error" ng-messages="formbasichealth.weight.$error" >
	    					{!! getValidationMessage()!!}
	    					{!! getValidationMessage('minlength')!!}
	    					{!! getValidationMessage('maxlength')!!}
						</div>
					</fieldset>


					<fieldset class="form-group" >
						{{ Form::label('blood_group', getphrase('Blood Group')) }}
						<span class="text-red">*</span>
						{{ Form::text('blood_group', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('(e.g) B+'),
							'name'=>'blood_group',
							'ng-model'=>'blood_group',
							'id'=>'blood_group', 
							'required'=> 'true', 
							'ng-minlength' => '2',
							'ng-maxlength' => '10',
							'ng-class'=>'{"has-error": formbasichealth.blood_group.$touched && formbasichealth.blood_group.$invalid}',
						 		
						)) }}
						
						<div class="validation-error" ng-messages="formbasichealth.blood_group.$error" >
	    					{!! getValidationMessage()!!}
	    					{!! getValidationMessage('minlength')!!}
	    					{!! getValidationMessage('maxlength')!!}
						</div>


					
					</fieldset>

                <div class="buttons text-center">

                    <button class="btn btn-lg btn-primary button"

                    ng-disabled='!formbasichealth.$valid'>{{ $button_name }}</button>

                </div>

