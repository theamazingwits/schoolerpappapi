<footer id="page-footer">
    <section id="footer-content">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-12">
                    <aside class="logo">
                         <img src="{{IMAGE_PATH_SETTINGS.getSetting('site_logo', 'site_settings')}}" alt="Menorah" class="vertical-center" height="50px" width="200px">
                        
                    </aside>
                </div><!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-4">
                    <aside>
					                        <header><h4>Contact Us</h4></header>
                        <address>
                            <strong>{{ getSetting('site_title', 'site_settings') }}</strong>
                            <br><br>
                            <span>{{ getSetting('site_address', 'site_settings') }}</span>
                            <br>
                           
                            <abbr title="Telephone Number">Telephone:</abbr> {{ getSetting('site_phone', 'site_settings') }}                            <br>
                           
                        </address>
                                        </aside>
                </div><!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-4">
                    <aside>
                        <header><h4>Important Links</h4></header>
                        <ul class="list-links">
							<li><a href="{{URL_ALUMNI_USER_CONTACT}}">Contact Us</a></li>
                              @if(Auth::check())
                            <li><a href="{{URL_SEARCH_ALUMNI_USERS}}">Alumni</a></li>
                            @endif
                            <li><a href="{{URL_ALUMNI_USER_NOTICES}}">Notice Board</a></li>
                            <li><a href="{{URL_ALUMNI_USER_EVENTS}}">Events</a></li>
                        </ul>
                    </aside>
                </div><!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-4">
                    <aside>
                        <header><h4>About Menorah Academy</h4></header>
                        <p>As for any undergraduate, the first year passed in a blur of textbooks, lectures and nights out. The atmosphere at Cambridge was like nothing I’d ever experienced before: everyone seemed to share a common desire to debate.</p>
                    </aside>
                </div><!-- /.col-md-3 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
        <div class="background"><img src="{{URL_ALUMNI_BACKGROUND}}"  alt="background"></div>
    </section><!-- /#footer-content -->

    <section id="footer-bottom">
        <div class="container">
            <div class="footer-inner">
                <div class="copyright" align="center">
                    &copy;
                    <a>{{ getSetting('site_title', 'site_settings') }}</a>
                    2018                    
                </div>
            </div>
        </div>
    </section>
</footer>