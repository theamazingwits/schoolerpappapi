<!DOCTYPE html>

<html lang="en" dir="{{ (App\Language::isDefaultLanuageRtl()) ? 'rtl' : 'ltr' }}">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="{{getSetting('meta_description', 'seo_settings')}}">
	<meta name="keywords" content="{{getSetting('meta_keywords', 'seo_settings')}}">
	 
	<link rel="icon" href="{{IMAGE_PATH_SETTINGS.getSetting('site_favicon', 'site_settings')}}" type="image/x-icon" />
	<title>@yield('title') {{ isset($title) ? $title : getSetting('site_title','site_settings') }}</title>
	<!-- Bootstrap Core CSS -->
	 @yield('header_scripts')
	     <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet"> 

	<link href="{{CSS}}bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="{{CSS}}bootstrap-datepicker.min.css">
	<link href="{{CSS}}sweetalert.css" rel="stylesheet" type="text/css">
	
	<!-- Morris Charts CSS -->
	<link href="{{CSS}}plugins/morris.css" rel="stylesheet">
	<!-- Proxima Nova Fonts CSS -->
	<link href="{{CSS}}proximanova.css" rel="stylesheet">
	<!-- Custom CSS -->

	<!-- Custom Fonts -->
	<link href="{{CSS}}custom-fonts.css" rel="stylesheet" type="text/css">
	<link href="{{CSS}}materialdesignicons.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />

	<link href="{{CSS}}alumini/selectize.css" rel="stylesheet" type="text/css">
	<link href="{{CSS}}alumini/owl.carousel.css" rel="stylesheet" type="text/css">
	<link href="{{CSS}}alumini/vanillabox.css" rel="stylesheet" type="text/css">
	<link href="{{CSS}}alumini/style.css" rel="stylesheet" type="text/css">
	
	
</head>

<body ng-app="academia" class="page-sub-page page-my-account">


	    <div class="wrapper">
        <!-- Header -->
        <div class="navigation-wrapper">
            <div class="secondary-navigation-wrapper">
                <div class="container">
                   <div class="navigation-contact pull-left">
			            Call Us:  
			      <span class="opacity-70">{{ getSetting('site_phone', 'site_settings') }}</span>
		          </div>
                    
                    <ul class="secondary-navigation list-unstyled pull-right">
                    <li>

                   @if(Auth::check()) 	

                    <a href="{{PREFIX}}">
                    <i class="fa fa-user"></i>My Profile</a>
                &nbsp;
                    <a href="{{URL_USERS_LOGOUT}}">Logout</a>

                    @else
                            
                       <a href="{{URL_ALUMINI_LOGIN}}">Login</a>

                    @endif
                    
                    </li>
                </ul>

    </div>
</div>

@include('layouts.alumini.header')
            <div class="background">
                <img src="{{URL_ALUMNI_BACKGROUND}}"  alt="background">
            </div>
        </div>
      

<!-- Ajax modal -->
<div class="modal fade" id="modal_ajax">
	<div class="modal-dialog">
		<div class="modal-content" style="margin-top: 22%;">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" align="center">Menorah Academy | Alumni</h4>
			</div>

			<div class="modal-body" style="height:420px; overflow:auto;">
				
			</div>

		</div>
	</div>
</div>



<!-- Delete modal -->
<div class="modal fade" id="modal_delete">
	<div class="modal-dialog">
		<div class="modal-content" style="margin-top: 222px;">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h4 class="modal-title" style="text-align:center;">Are you sure you want to remove this information?</h4>
			</div>

			<div class="modal-footer" style="margin:0px; bordet-top:0px; text-align:center;">
				<a href="#" class="btn btn-danger" id="delete_link">Delete</a>
				<button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
			</div>
			
		</div>
	</div>
</div>




<!-- Delete modal -->
<div class="modal fade" id="modal_finish">
	<div class="modal-dialog">
		<div class="modal-content" style="margin-top: 222px;">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h4 class="modal-title" style="text-align:center;">Are you sure that you have done uploading?</h4>
			</div>

			<div class="modal-footer" style="margin:0px; bordet-top:0px; text-align:center;">
				<a href="#" class="btn btn-primary" id="finish_link">Finished</a>
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			</div>
			
		</div>
	</div>
</div>


        <!-- Breadcrumb -->
@yield('breadcrumb')

<!-- end Breadcrumb -->
        <!-- Page Content -->


<!-- Page Content -->
<div id="page-content">
    <div class="container">
        <div class="row">
          

          @yield('content')

          @if(!isset($mystories))  

          @php

             $recent_stories  = App\AlumniStory::orderby('created_at','desc')->limit(3)->get();
             // dd($recent_stories);

          @endphp

            <!--SIDEBAR Content-->
            <div class="col-md-4">
                <div id="page-sidebar" class="sidebar">
                    <aside class="news-small" id="news-small">
                        <header><h2>Most Read Stories</h2></header>
                        <div class="section-content">
                           
                           @if(count($recent_stories) > 0)

                             @foreach($recent_stories as $mystory)

                            <article>
                                <figure class="date">
                                    <i class="fa fa-calendar"></i>
                                    {{$mystory->date}}                               
                                     </figure>
                                <header>

                                 @if(Auth::check())	
                                    <a href="{{URL_STORY_INFO.$mystory->id}}">
                                        {{ucwords($mystory->title)}}                                
                                   </a>
                                 @else

                                   <a href="{{URL_ALUMINI_LOGIN}}">
                                        {{ucwords($mystory->title)}}                                
                                   </a>
                                 
                                 @endif  
                                </header>
                            </article><!-- /article -->

                            @endforeach

                          @endif 
                                                 
                        </div>
                    </aside><!-- /.news-small -->
                </div><!-- /#sidebar -->
            </div><!-- /.col-md-4 -->


            @endif




        </div><!-- /.row -->
    </div><!-- /.container -->
</div>


		

@include('layouts.alumini.footer')
    </div>
 
 	
		
	<!-- /#wrapper -->
	<!-- jQuery -->
	<script src="{{JS}}jquery-1.12.1.min.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="{{JS}}bootstrap.min.js"></script>

 
	<!--JS Control-->
	<script src="{{JS}}main.js"></script>
	<script src="{{JS}}sweetalert-dev.js"></script>
	<script src="{{JS}}alumini/selectize.min.js"></script>
	<script src="{{JS}}alumini/owl.carousel.min.js"></script>
	<script src="{{JS}}alumini/parsley.js"></script>
	<script src="{{JS}}alumini/equalHeights.js"></script>
	<script src="{{JS}}alumini/icheck.min.js"></script>
	<script src="{{JS}}alumini/jquery.vanillabox-0.1.5.min.js"></script>
	<script src="{{JS}}alumini/countdown.js"></script>
	<script src="{{JS}}alumini/retina-1.1.0.min.js"></script>
	<script src="{{JS}}alumini/custom.js"></script>

	  <!-- end Header -->
		<script type="text/javascript">
	
	function showAjaxModal(url)
	{
		
		jQuery('#modal_ajax').modal('show', {backdrop: 'true'});

		//Show ajax response on request success
		$.ajax({
			url: url,
			success: function(response)
			{
				jQuery('#modal_ajax .modal-body').html(response);
			}
		});
	}

</script>


<script type="text/javascript">
	
	function confirm_modal(delete_url)
	{
		jQuery('#modal_delete').modal('show', {backdrop: 'static'});
		document.getElementById('delete_link').setAttribute('href' , delete_url);
	} 

</script>


<script type="text/javascript">
	
	function confirm_finish_modal(finish_url)
	{
		jQuery('#modal_finish').modal('show', {backdrop: 'static'});
		document.getElementById('finish_link').setAttribute('href' , finish_url);
	} 

</script>


	
	 @yield('footer_scripts')

	@include('errors.formMessages')
  
 	@yield('custom_div_end')
	{!!getSetting('google_analytics', 'seo_settings')!!}
 

 
</body>

</html>