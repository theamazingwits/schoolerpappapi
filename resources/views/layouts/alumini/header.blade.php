            <div class="primary-navigation-wrapper">
    <header class="navbar" id="top" role="banner">
        <div class="container">
            <div class="navbar-header">
                <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="navbar-brand nav" id="brand">
                    <a href="{{PREFIX}}">
                        <img src="{{IMAGE_PATH_SETTINGS.getSetting('site_logo', 'site_settings')}}" alt="Menorah">
                    </a>
                </div>
            </div>
            <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
                <ul class="nav navbar-nav">

                  @if(Auth::check())  
                  
                    <li {{ isActive($active_class, 'alumini-users') }} >
                        <a href="{{URL_SEARCH_ALUMNI_USERS}}">
                            Alumni                        </a>
                    </li>

                   

                @endif  
                           
                  <li {{ isActive($active_class, 'events') }}>
                        <a href="{{URL_ALUMNI_USER_EVENTS}}">
                            Events                        </a>
                    </li>

                     @if(Auth::check())  
                    <li {{ isActive($active_class, 'stories') }}>
                        <a href="{{URL_ALUMNI_USER_STORIES}}">
                            Stories                        </a>
                    </li>

                    @endif
                    <li {{ isActive($active_class, 'gallery') }}>
                        <a href="{{URL_ALUMNI_USER_GALLERY}}">
                            Gallery                        </a>
                    </li>
                    <li {{ isActive($active_class, 'volunteers') }}>
                        <a href="{{URL_ALUMNI_USER_VOLUNTEERS}}">
                            Volunteers                        </a>
                    </li>
                    <li {{ isActive($active_class, 'notices') }}>
                        <a href="{{URL_ALUMNI_USER_NOTICES}}">
                            Notice Board                        </a>
                    </li>
                 
                    

                     <li {{ isActive($active_class, 'donations') }}>
                        <a href="{{URL_ALUMNI_USER_DONATIONS}}">
                            Donations                        </a>
                    </li>

                     @if(Auth::check())

                       <li {{ isActive($active_class, 'user_donations') }} >
                        <a href="{{URL_USER_DONATIONS.Auth::user()->id}}">
                            Your Donations       </a>
                    </li>

                    @endif

                    <li {{ isActive($active_class, 'contact_us') }}>
                        <a href="{{URL_ALUMNI_USER_CONTACT}}">
                            Contact Us                        </a>
                    </li>

                
                    
                </ul>
            </nav><!-- /.navbar collapse-->
        </div><!-- /.container -->
    </header><!-- /.navbar -->
</div><!-- /.primary-navigation -->