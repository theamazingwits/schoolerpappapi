<!DOCTYPE html>

<html lang="en" dir="{{ (App\Language::isDefaultLanuageRtl()) ? 'rtl' : 'ltr' }}">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="{{getSetting('meta_description', 'seo_settings')}}">
	<meta name="keywords" content="{{getSetting('meta_keywords', 'seo_settings')}}">
	 
	<link rel="icon" href="{{IMAGE_PATH_SETTINGS.getSetting('site_favicon', 'site_settings')}}" type="image/x-icon" />
	<title>@yield('title') {{ isset($title) ? $title : getSetting('site_title','site_settings') }}</title>
	<!-- Bootstrap Core CSS -->
	 @yield('header_scripts')
	<link href="{{CSS}}bootstrap.css" rel="stylesheet">
	<link rel="stylesheet" href="{{CSS}}bootstrap-datepicker.min.css">
    <link href="{{CSS}}plugins/datetimepicker/css/bootstrap-datetimepicker.css" rel="stylesheet">
	<link href="{{CSS}}sweetalert.css" rel="stylesheet" type="text/css">
	@if(isset($module_helper))	
	<link href="{{CSS}}bootstrap-tour.css" rel="stylesheet">
	@endif
	<!-- Morris Charts CSS -->
	<link href="{{CSS}}plugins/morris.css" rel="stylesheet">
	<!-- Proxima Nova Fonts CSS -->
	<link href="{{CSS}}proximanova.css" rel="stylesheet">
	<!-- Custom CSS -->

	<link href="{{CSS}}{{getSetting('current_theme', 'site_settings')}}-theme.css" rel="stylesheet">
	<!-- Custom Fonts -->
	<!-- <link href="{{CSS}}custom-fonts.css" rel="stylesheet" type="text/css"> -->
	<link href="{{PREFIX1}}assets/css/style.css" rel="stylesheet" type="text/css">
	<script src="{{PREFIX1}}assets/js/system.js"></script>
     <!-- Multiple check Box CSS-->
    <link href="{{PREFIX1}}icons.css" rel="stylesheet" type="text/css" />
    <link href="{{PREFIX1}}multiple-checkbox/select2.min.css" rel="stylesheet" />
    <script src="{{JS}}jquery.min.js"></script>
    <script src="{{PREFIX1}}multiple-checkbox/select2.js"></script>
	@yield('header_scripts')
	 <style>
	 	#DataTables_Table_0_wrapper .row{
        width: 100%!important;
        margin:0%;
    }
    #DataTables_Table_0_wrapper{
        padding: 30px;
    }
    #users-table_wrapper .row{
        width: 100%!important;
        margin:0%;
	 
    }
     #users-table_wrapper{
        padding: 30px;
    }
    </style>
	
	
</head>

<body ng-app="academia">
 
 	
 @yield('custom_div')
 <?php 
 $class = '';
 if(!isset($right_bar))
 	$class = 'no-right-sidebar';

 ?>
	<div id="app" class="reactive-app header-bg-danger sidebar-type-push sidebar-state-open sidebar-tr-state-compact sidebar-bg-default sidebar-option-theme4 aside-close">
    <!-- inject-body-start:js -->
    <!-- endinject -->
    <!--Begin Header-->
    <header id="header" class="header-wrap clearfix">

        <div class="header-tools">
            <a href="" data-sidebar="#sidebar" class="m-icon pull-left" aria-label="toggle menu">
                <i class="icon m-icon-lines" aria-hidden="true"></i>
            </a>
            <!-- <div class="dropdown create-new pull-left hidden-md-down" id="create-new">
                <a class="clear-style" href="" id="dropdown-header-new" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-file-o icon-mr-ch"></i>New<i class="fa fa-angle-down icon-ml"></i>
                </a>
                <div class="dropdown-menu" aria-labelledby="dropdown-header-new">
                    <div class="dropdown-menu-content lh-2">
                        <a class="dropdown-item" href=""><i class="fa fa-envelope-o icon-mr"></i>Message</a>
                        <a class="dropdown-item" href=""><i class="icon-user-follow icon-mr"></i>User</a>
                        <a class="dropdown-item" href=""><i class="icon-event icon-mr"></i>Event</a>
                        <a class="dropdown-item" href=""><i class="ion-ios-box-outline icon-mr"></i>Product</a>
                        <a class="dropdown-item" href=""><i class="icon-wallet icon-mr"></i>Wallet</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href=""><i class="icon-settings icon-mr"></i>Settings</a>
                    </div>
                </div>
            </div> -->
            <!-- <a class="fs-3 hidden-md-down clear-style pull-left aside-trigger" data-aside="#aside" data-toggle="tab" href="#aside-uploads">
                <i class="ion-ios-bell-outline"></i>
                <span class="badge bgc-danger-lighter badge-pill waves fs-8 bc-danger-lighter">2</span>
            </a> -->
            <div class="mega-dropdown pull-left hidden-xs-down">
            </div>
            <a href="{{URL_USERS_LOGOUT}}" class="fs-5 clear-style pull-right">
                <i class="fa fa-sign-out"></i>
            </a>
            <a href="{{URL_LANGUAGES_LIST}}" class="fs-5 clear-style pull-right">
                <i class="fa fa-language"></i>
            </a>
            <!-- <a href="{{URL_LANGUAGES_LIST}}" class="clear-style language-box pull-right ui floating select-dropdown simple-select">
                <span class="text"><i class="gb flag"></i>Language Settings</span>
                <i class="select-dropdown icon"></i>
            </a> -->
            <a href="" class="fs-5 clear-style pull-right hidden-xs-down page-fullscreen">
                <i data-icon="icon-size-actual icon-size-fullscreen" class="fs-6 fw-bold icon-size-fullscreen"></i>
            </a>
        </div>
        <div id="page-title-wrap" class="page-title-wrap clearfix">
            <h1 class="page-title pull-left fs-4 fw-light">
                <i class="fa fa fa-dashboard icon-mr fs-4"></i><span class="hidden-xs-down">{{$title}}</span>
            </h1>
            <div class="smart-links">
                <ul class="nav" role="tablist">
                    <!-- <li class="nav-item">
                        <a class="nav-link clear-style aside-trigger" data-aside="#aside" data-toggle="tab" href="#aside-contacts" role="tab"><i class="fa fa-book"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link clear-style aside-trigger" data-aside="#aside" data-toggle="tab" href="#aside-uploads" role="tab"><i class="fa fa-cloud-upload"></i></a>
                    </li> -->
                    <li class="nav-item">
                        <a class="nav-link clear-style aside-trigger" data-aside="#aside" data-toggle="tab" href="#aside-settings" role="tab"><i class="fa fa-sliders"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </header>
    <!--End Header-->
    <!--Begin Loader-->
    <div class="page-loader loader-wrap" id="loader-wrap">
        <div class="loader loading-icon"></div>
    </div>
    <!--End Loader-->
    <!--Begin Content-->

    <!--End Content-->
    <!--Begin Sidebar-->

	<section id="sidebar" class="sidebar-wrap">
	    <div class="sidebar-content">
	        <a href="{{PREFIX}}" class="app-name">
	            <span class="full-name">{{ getPhrase('dashboard') }}</span>
	            <span class="compact-name"><i class="icon-layers"></i></span>
	        </a>
	        <div class="profile">
	            <div class="details">
	                <div class="clearfix">
	                    <div class="picture rounded-circle pull-left" style="background-image: url('{{ getProfilePath(Auth::user()->image, 'thumb') }}');"></div>
	                    <div class="about pull-left">
	                        <a href="" class="clear-style sbg-settings"><i class="fa fa-edit"></i></a>
	                        <a href="javascript:void(0)">
	                        	@if(Auth::check())
	                            <h3 class="name">{{Auth::user()->name}}</h3>
	                            @endif
	                        </a>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <div class="tab-links">
	            <ul class="clear-style li-left nav nav-tabs border-0" role="tablist">
	                <li class="nav-item">
	                    <a href="#side-home" class="bc-success active" data-toggle="tab" role="tab"><i class="fa fa-list"></i><span class="tab-title">Menu</span></a>
	                </li>
	                <li class="nav-item">
	                    <a href="#side-comments" class="bc-primary" data-toggle="tab" role="tab"><i class="fa fa-comments"></i><span class="tab-title">Comments</span>
	                        <span class="badge fs-8 badge-pill bgc-primary-lighter waves bc-primary">14</span>
	                    </a>
	                </li>
	                <li class="nav-item">
	                    <a href="#side-messages" class="bc-warning" data-toggle="tab" role="tab"><i class="fa fa-envelope"></i><span class="tab-title">Inbox</span>
	                        <span class="badge fs-8 badge-pill waves bc-warning bgc-warning-lighter">32</span>
	                    </a>
	                </li>
	                <li class="nav-item">
	                    <a href="#side-alerts" class="bc-danger" data-toggle="tab" role="tab"><i class="fa fa-warning"></i><span class="tab-title">Alert</span>
	                        <span class="badge fs-8 badge-pill waves bc-danger bgc-danger-lighter badge-counter transition hidden" data-counter="100" data-duration="400000"><span class="text"></span></span>
	                    </a>
	                </li>
	                <li class="nav-item tab-link-add">
	                    <a href="#side-settings" class="bc-success" data-toggle="tab" role="tab"><i class="fa fa-gear"></i><span class="tab-title">Settings</span></a>
	                </li>
	            </ul>
	        </div>
	        <div class="tab-content">
	            <!--**  MENU LISTS ** -->
	            <div class="tab-pane active" id="side-home" role="tabpanel">
	                <nav class="sidebar-menu pb-3" role="navigation">
                            <ul class="clear-style">

								<!-- DASHBOARD MENU -->
                                <li class="menu-item active active-icon">
                                    <a  href="{{PREFIX}}"><i class="menu-icon fa fa-dashboard"></i><span>{{ getPhrase('dashboard') }} </span></a>
                                    <!-- #Sub menu-->
								</li>
								
								<!-- USERS MENU -->
								<li class="menu-item js-custom-click">
                                    <a href="{{URL_USERS_DASHBOARD}}"><i class="menu-icon icon-user"></i><span>{{ getPhrase('users') }} </span></a>
								</li>
								
							
                                <!-- Menu item-->
                                <li class="menu-item">
                                    <a href="{{URL_ACADEMICOPERATIONS_DASHBOARD}}"><i class="menu-icon fa fa-university"></i><span>{{ getPhrase('academics_operations') }}</span><i class="fa fa-angle-left pull-right toggle"></i></a>
                                    <!-- Sub menu-->
                                    <ul class="sub-menu">
                                        <!-- Menu item-->
                                        <li class="menu-item">
                                            <a href="{{URL_CERTIFICATES_DASHBOARD}}"><i class="menu-icon fa fa-exchange"></i><span>{{ getPhrase('certificates')}}</span></a>
                                        </li>
                                        <!-- #Menu item-->
                                        <!-- Menu item-->
                                        <li class="menu-item">
                                            <a  href="{{URL_STUDENT_TRANSFERS}}"><i class="menu-icon fa fa-certificate"></i><span>{{ getPhrase('transfers')}}</span></a>
                                        </li>
										<!-- #Menu item-->
										<!-- Menu item-->
                                        <li class="menu-item">
                                            <a  href="{{URL_TIMETABLE_DASHBOARD}}"><i class="menu-icon fa fa-clock-o"></i><span>{{ getPhrase('timetable')}}</span></a>
                                        </li>
										<!-- #Menu item-->
										<!-- Menu item-->
                                        <li class="menu-item">
                                            <a  href="{{URL_OFFLINE_EXAMS}}"><i class="menu-icon fa fa-external-link"></i><span>{{ getPhrase('offline_exams_')}}</span></a>
                                        </li>
										<!-- #Menu item-->
										<!-- Menu item-->
                                        <li class="menu-item">
                                            <a  href="{{URL_STUDENT_CLASS_ATTENDANCE}}"><i class="menu-icon fa fa-check-square-o"></i><span>{{ getPhrase('class_attendance_report')}}</span></a>
                                        </li>
										<!-- #Menu item-->
										<!-- Menu item-->
                                        <li class="menu-item">
                                            <a href="{{URL_STUDENT_MARKS_REPORT}}"><i class="menu-icon fa fa-line-chart"></i><span>{{ getPhrase('class_marks_report')}}</span></a>
                                        </li>
										<!-- #Menu item-->
										<!-- Menu item-->
                                        <li class="menu-item">
                                            <a href="{{URL_STUDENT_LIST}}"><i class="menu-icon fa fa-users"></i><span>{{ getPhrase('student_list')}}</span></a>
                                        </li>
										<!-- #Menu item-->
										<!-- Menu item-->
                                        <li class="menu-item">
                                            <a href="{{URL_STUDENT_COMPLETED_LIST}}"><i class="menu-icon fa fa-graduation-cap"></i><span> {{ getPhrase('students_completed_list')}}</span></a>
                                        </li>
										<!-- #Menu item-->
										<!-- Menu item-->
                                        <li class="menu-item">
                                            <a href="{{URL_STUDENT_DETAINED_LIST}}"><i class="menu-icon fa fa-user-circle-o"></i><span> {{ getPhrase('students_detained_list')}}</span></a>
                                        </li>
                                        <!-- #Menu item-->
                                    </ul>
                                    <!-- #Sub menu-->
                                </li>
								<!-- #Menu item-->
								
                                <!-- Menu item-->
                                <li class="menu-item">
                                    <a href="{{URL_EXAMS_DASHBOARD}}"><i class="menu-icon fa fa-pencil-square-o"></i><span>{{ getPhrase('exams') }}</span><i class="fa fa-angle-left pull-right toggle"></i></a>
                                    <!-- Sub menu-->
                                    <ul class="sub-menu">
                                        <!-- Menu item-->
                                        <li class="menu-item">
                                            <a  href="{{URL_QUIZ_CATEGORIES}}"><i class="menu-icon fa fa-random"></i><span>{{ getPhrase('categories') }}</span></a>
                                        </li>
                                        <!-- #Menu item-->
                                        <!-- Menu item-->
                                        <li class="menu-item">
                                            <a href="{{URL_QUIZ_QUESTIONBANK}}"><i class="menu-icon fa fa-question"></i><span>{{ getPhrase('question_bank') }}</span></a>
                                        </li>
										<!-- #Menu item-->
										 <!-- Menu item-->
										 <li class="menu-item">
                                            <a href="{{URL_QUIZZES}}"><i class="menu-icon fa fa-clock-o"></i><span>{{ getPhrase('quiz') }}</span></a>
                                        </li>
										<!-- #Menu item-->
										 <!-- Menu item-->
										 <li class="menu-item">
                                            <a href="{{URL_EXAM_SERIES}}"><i class="menu-icon fa fa-list-ol"></i><span>{{ getPhrase('exam_series') }}</span></a>
                                        </li>
										<!-- #Menu item-->
											 <!-- Menu item-->
											 <li class="menu-item">
                                            <a href="{{URL_OFFLINEEXMAS_QUIZ_CATEGORIES}}"><i class="menu-icon fa fa-sort-amount-asc"></i><span>{{ getPhrase('offline_exams_categories') }}</span></a>
                                        </li>
										<!-- #Menu item-->
											 <!-- Menu item-->
											 <li class="menu-item">
                                            <a href="{{URL_INSTRUCTIONS}}"><i class="menu-icon fa fa-hand-o-right"></i><span>{{ getPhrase('instructions') }}</span></a>
                                        </li>
                                        <!-- #Menu item-->
                                      
                                    </ul>
                                    <!-- #Sub menu-->
                                </li>
                                <!-- #Menu item-->

                                <!-- Menu item-->
                                <li class="menu-item">
                                    <a href=""><i class="menu-icon icon-grid"></i><span>{{ getPhrase('coupons') }} </span><i class="fa fa-angle-left pull-right toggle"></i></a>
                                    <!-- Sub menu-->
                                    <ul class="sub-menu">
                                        <!-- Menu item-->
                                        <li class="menu-item">
                                            <a href="{{URL_COUPONS}}"><i class="menu-icon fa fa-list"></i><span>{{ getPhrase('list') }}</span></a>
                                        </li>
                                        <!-- #Menu item-->
                                        <!-- Menu item-->
                                        <li class="menu-item">
                                            <a href="{{URL_COUPONS_ADD}}"><i class="menu-icon fa fa-plus"></i><span>{{ getPhrase('add') }}</span></a>
                                        </li>
                                        <!-- #Menu item-->
                                    </ul>
                                    <!-- #Sub menu-->
                                </li>
								<!-- #Menu item-->

                                <!-- Menu item-->
                                <li class="menu-item">
                                    <a href="{{URL_LMS_DASHBOARD}}"><i class="menu-icon fa fa-leanpub"></i><span>{{ 'LMS' }}</span><i class="fa fa-angle-left pull-right toggle"></i></a>
                                    <!-- Sub menu-->
                                    <ul class="sub-menu">
                                        <!-- Menu item-->
                                        <li class="menu-item">
                                            <a href="{{ URL_LMS_CATEGORIES }}"><i class="menu-icon fa fa-random"></i><span>{{ getPhrase('categories') }}</span></a>
                                        </li>
                                        <!-- #Menu item-->
                                        <!-- Menu item-->
                                        <li class="menu-item">
                                            <a href="{{ URL_LMS_CONTENT }}"><i class="menu-icon icon-books"></i><span>{{ getPhrase('contents') }}</span></a>
                                        </li>
                                        <!-- #Menu item-->
                                        <!-- Menu item-->
                                        <li class="menu-item">
                                            <a href="{{ URL_LMS_SERIES }}"><i class="menu-icon fa fa-list-ol"></i><span>{{ getPhrase('series') }}</span></a>
                                        </li>
                                        <!-- #Menu item-->
                                    </ul>
                                    <!-- #Sub menu-->
                                </li>
								<!-- #Menu item-->
							
                                <!-- Menu item-->
                                <li class="menu-item">
                                    <a href="{{URL_LIBRARY_LIBRARYDASHBOARD}}"><i class="menu-icon fa fa-book"></i><span>{{ getPhrase('central_library') }}</span><i class="fa fa-angle-left pull-right toggle"></i></a>
                                    <!-- Sub menu-->
                                    <ul class="sub-menu">
                                        <!-- Menu item-->
                                        <li class="menu-item">
                                            <a href="{{URL_LIBRARY_ASSETS}}"><i class="menu-icon fa fa-book"></i><span>{{ getPhrase('asset_types') }}</span></a>
                                        </li>
										<!-- #Menu item-->
										<!-- Menu item-->
                                        <li class="menu-item">
                                            <a href="{{URL_LIBRARY_MASTERS}}"><i class="menu-icon fa fa-database"></i><span>{{ getPhrase('master_data') }}</span></a>
                                        </li>
										<!-- #Menu item-->
										<!-- Menu item-->
                                        <li class="menu-item">
                                            <a href="{{URL_PUBLISHERS}}"><i class="menu-icon fa fa-paint-brush"></i><span>{{ getPhrase('publishers') }}</span></a>
                                        </li>
										<!-- #Menu item-->
										<!-- Menu item-->
                                        <li class="menu-item">
                                            <a href="{{URL_AUTHORS}}"><i class="menu-icon fa fa-mortar-board"></i><span>{{ getPhrase('authors') }}</span></a>
                                        </li>
										<!-- #Menu item-->
										<!-- Menu item-->
                                        <li class="menu-item">
                                            <a href="{{URL_LIBRARY_USERS}}student"><i class="menu-icon fa fa-user"></i><span>{{ getPhrase('students') }}</span></a>
                                        </li>
										<!-- #Menu item-->
										<!-- Menu item-->
                                        <li class="menu-item">
                                            <a href="{{URL_LIBRARY_USERS}}staff"><i class="menu-icon fa fa-user-circle"></i><span>{{ getPhrase('staff') }}</span></a>
                                        </li>
										<!-- #Menu item-->
										<!-- Menu item-->
                                        <li class="menu-item">
                                            <a href="{{URL_LIBRARY_LIBRARYDASHBOARD_BOOKS}}"><i class="menu-icon fa fa-book"></i><span>{{ getPhrase('student_book_return') }}</span></a>
                                        </li>
										<!-- #Menu item-->
											<!-- Menu item-->
											<li class="menu-item">
                                            <a href="{{URL_LIBRARY_LIBRARYDASHBOARD_BOOKS_STAFF}}"><i class="menu-icon fa fa-address-book"></i><span>{{ getPhrase('staff_book_return') }}</span></a>
                                        </li>
                                        <!-- #Menu item-->
                                    </ul>
                                    <!-- #Sub menu-->
                                </li>
								<!-- #Menu item-->
								
 						<!-- Menu item-->
 						<li class="menu-item">
							<a href="{{URL_FEE_REPORTS}}"><i class="menu-icon fa fa-exchange"></i><span>{{ getPhrase('fee_management') }}</span><i class="fa fa-angle-left pull-right toggle"></i></a>
							<!-- Sub menu-->
							<ul class="sub-menu">
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_FEE_CATEGORIES}}"><i class="menu-icon fa fa-th"></i><span>{{ getPhrase('fee_categories') }}</span></a>
								</li>
								<!-- #Menu item-->
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_FEE_PARTICULARS}}"><i class="menu-icon fa fa-bars"></i><span>{{ getPhrase('fee_particulars') }}</span></a>
								</li>
								<!-- #Menu item-->
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_FEE_ACCEPT}}"><i class="menu-icon fa fa-money"></i><span>{{ getPhrase('pay_fee') }}</span></a>
								</li>
								<!-- #Menu item-->
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_FEE_REPORTS_DAILY}}"><i class="menu-icon fa fa-clock-o"></i><span>{{ getPhrase('fee_paid_reports') }}</span></a>
								</li>
								<!-- #Menu item-->
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_FEE_REPORTS_OFFLINE}}"><i class="menu-icon fa fa-bar-chart"></i><span>{{ getPhrase('offline_fee_payments') }}</span></a>
								</li>
								<!-- #Menu item-->
							</ul>
							<!-- #Sub menu-->
						</li>
						<!-- #Menu item-->

						<!-- Menu item-->
						<li class="menu-item">
							<a href="#"><i class="menu-icon fa fa-building"></i><span>{{ getPhrase('hostel_management') }}</span><i class="fa fa-angle-left pull-right toggle"></i></a>
							<!-- Sub menu-->
							<ul class="sub-menu">
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_HOSTEL}}"><i class="menu-icon fa fa-long-arrow-right"></i><span>{{ getPhrase('hostel') }}</span></a>
								</li>
								<!-- #Menu item-->
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_ROOM_TYPE}}"><i class="menu-icon fa fa-long-arrow-right"></i><span>{{ getPhrase('room_type') }}</span></a>
								</li>
								<!-- #Menu item-->
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_HOSTEL_ROOMS}}"><i class="menu-icon fa fa-long-arrow-right"></i><span>{{ getPhrase('hostel_rooms') }}</span></a>
								</li>
								<!-- #Menu item-->
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_ASSIGN_HOSTEL}}"><i class="menu-icon fa fa-long-arrow-right"></i><span>{{ getPhrase('assign_hostel_to_student') }}</span></a>
								</li>
								<!-- #Menu item-->
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_ASSIGN_HOSTEL_FEE}}"><i class="menu-icon fa fa-long-arrow-right"></i><span>{{ getPhrase('assign_hostel_fee') }}</span></a>
								</li>
								<!-- #Menu item-->
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_PAY_HOSTEL_FEE}}"><i class="menu-icon fa fa-long-arrow-right"></i><span>{{ getPhrase('pay_fee') }}</span></a>
								</li>
								<!-- #Menu item-->
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_HOSTEL_FEE_REPORTS}}"><i class="menu-icon fa fa-long-arrow-right"></i><span>{{ getPhrase('fee_reports') }}</span></a>
								</li>
								<!-- #Menu item-->
							</ul>
							<!-- #Sub menu-->
						</li>
						<!-- #Menu item-->


						<!-- Menu item-->
						<li class="menu-item">
							<a href="#"><i class="menu-icon fa fa-bus"></i><span>{{ getPhrase('transport_management') }}</span><i class="fa fa-angle-left pull-right toggle"></i></a>
							<!-- Sub menu-->
							<ul class="sub-menu">
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_VECHICLE_DRIVER}}"><i class="menu-icon fa fa-long-arrow-right"></i><span>{{ getPhrase('drivers') }}</span></a>
								</li>
								<!-- #Menu item-->
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_VECHICLE_ROUTE}}"><i class="menu-icon fa fa-long-arrow-right"></i><span>{{ getPhrase('routes') }}</span></a>
								</li>
								<!-- #Menu item-->
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_VECHICLES}}"><i class="menu-icon fa fa-long-arrow-right"></i><span>{{ getPhrase('vehicles') }}</span></a>
								</li>
								<!-- #Menu item-->
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_VIEW_VEHICLES}}"><i class="menu-icon fa fa-long-arrow-right"></i><span>{{ getPhrase('assign_routes') }}</span></a>
								</li>
								<!-- #Menu item-->
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_ASSIGN_STUDENT_VEHICLE}}"><i class="menu-icon fa fa-long-arrow-right"></i><span>{{ getPhrase('assign_students') }}</span></a>
								</li>
								<!-- #Menu item-->
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_ASSIGN_TRANSPORT_FEE}}student"><i class="menu-icon fa fa-long-arrow-right"></i><span>{{ getPhrase('assign_fee') }}</span></a>
								</li>
								<!-- #Menu item-->
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_PAY_TRANSPORT_FEE}}student"><i class="menu-icon fa fa-long-arrow-right"></i><span>{{ getPhrase('pay_fee') }}</span></a>
								</li>
								<!-- #Menu item-->
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_TRANSPORT_FEE_REPORTS}}student"><i class="menu-icon fa fa-long-arrow-right"></i><span>{{ getPhrase('fee_reports') }}</span></a>
								</li>
								<!-- #Menu item-->
							</ul>
							<!-- #Sub menu-->
						</li>
						<!-- #Menu item-->

						<!-- Menu item-->
						<li class="menu-item">
							<a href="#"><i class="menu-icon fa fa-money"></i><span>{{ getPhrase('payroll') }}</span><i class="fa fa-angle-left pull-right toggle"></i></a>
							<!-- Sub menu-->
							<ul class="sub-menu">
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_PAYROLL_SALARY_TEMPLATES}}"><i class="menu-icon fa fa-calculator"></i><span>{{ getPhrase('salary_template') }}</span></a>
								</li>
								<!-- #Menu item-->
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_PAYROLL_MANAGE_SALARY}}"><i class="menu-icon fa fa-beer"></i><span>{{ getPhrase('manage_salary') }}</span></a>
								</li>
								<!-- #Menu item-->
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_PAY_SALARY}}"><i class="menu-icon fa fa-money"></i><span>{{ getPhrase('make_payment') }}</span></a>
								</li>
								<!-- #Menu item-->
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_SALARY_REPORTS}}"><i class="menu-icon fa fa-list"></i><span>{{ getPhrase('reports') }}</span></a>
								</li>
								<!-- #Menu item-->
							</ul>
							<!-- #Sub menu-->
						</li>
						<!-- #Menu item-->

						<!-- Menu item-->
						<li class="menu-item">
							<a href="#"><i class="menu-icon fa fa-credit-card"></i><span>{{ getPhrase('expenses') }}</span><i class="fa fa-angle-left pull-right toggle"></i></a>
							<!-- Sub menu-->
							<ul class="sub-menu">
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_EXPENSE_CATEGORIES}}"><i class="menu-icon fa fa-list"></i><span>{{ getPhrase('expense_categories_list') }}</span></a>
								</li>
								<!-- #Menu item-->
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_EXPENSE_CATEGORIES_ADD}}"><i class="menu-icon fa fa-plus"></i><span>{{ getPhrase('add_expense_category') }}</span></a>
								</li>
								<!-- #Menu item-->
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_EXPENSES}}"><i class="menu-icon fa fa-list"></i><span>{{ getPhrase('expense_list') }}</span></a>
								</li>
								<!-- #Menu item-->
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_EXPENSES_ADD}}"><i class="menu-icon fa fa-plus"></i><span>{{ getPhrase('add_expense') }}</span></a>
								</li>
								<!-- #Menu item-->
									<!-- Menu item-->
									<li class="menu-item">
									<a href="{{URL_OVERALL_REPORTS}}"><i class="menu-icon fa fa-bar-chart"></i><span>{{ getPhrase('over_all_reports') }}</span></a>
								</li>
								<!-- #Menu item-->
							</ul>
							<!-- #Sub menu-->
						</li>
						<!-- #Menu item-->

						<!-- Menu item-->
						<li class="menu-item">
							<a href="#"><i class="menu-icon fa fa-leanpub"></i><span>{{ getPhrase('inventory') }}</span><i class="fa fa-angle-left pull-right toggle"></i></a>
							<!-- Sub menu-->
							<ul class="sub-menu">
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_INVENTORY}}"><i class="menu-icon fa fa-long-arrow-right"></i><span>{{ getPhrase('categories') }}</span></a>
								</li>
								<!-- #Menu item-->
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_INVENTORY_STORE}}"><i class="menu-icon fa fa-long-arrow-right"></i><span>{{ getPhrase('stores') }}</span></a>
								</li>
								<!-- #Menu item-->
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_INVENTORY_ITEM}}"><i class="menu-icon fa fa-long-arrow-right"></i><span>{{ getPhrase('items') }}</span></a>
								</li>
								<!-- #Menu item-->
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_INVENTORY_ITEM_STOCK}}"><i class="menu-icon fa fa-long-arrow-right"></i><span>{{ getPhrase('stock_items') }}</span></a>
								</li>
								<!-- #Menu item-->
									<!-- Menu item-->
									<li class="menu-item">
									<a href="{{URL_INVENTORY_ITEMS_SUPPLIER}}"><i class="menu-icon fa fa-long-arrow-right"></i><span>{{ getPhrase('suppliers') }}</span></a>
								</li>
								<!-- #Menu item-->
									<!-- Menu item-->
									<li class="menu-item">
									<a href="{{URL_INVENTORY_ISSUE_ITEM}}"><i class="menu-icon fa fa-long-arrow-right"></i><span>{{ getPhrase('issue_item') }}</span></a>
								</li>
								<!-- #Menu item-->
							</ul>
							<!-- #Sub menu-->
						</li>
						<!-- #Menu item-->

					<!-- Menu item-->
					<li class="menu-item">
							<a href="#"><i class="menu-icon fa fa-th-large"></i><span>{{ getPhrase('assets_management') }}</span><i class="fa fa-angle-left pull-right toggle"></i></a>
							<!-- Sub menu-->
							<ul class="sub-menu">
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_ASSET_LOCATION}}"><i class="menu-icon fa fa-long-arrow-right"></i><span>{{ getPhrase('asset_locations') }}</span></a>
								</li>
								<!-- #Menu item-->
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_ASSET_CATEGORY}}"><i class="menu-icon fa fa-long-arrow-right"></i><span>{{ getPhrase('asset_categories') }}</span></a>
								</li>
								<!-- #Menu item-->
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_ASSET_VENDOR}}"><i class="menu-icon fa fa-long-arrow-right"></i><span>{{ getPhrase('vendors') }}</span></a>
								</li>
								<!-- #Menu item-->
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_ASSET_ITEMS}}"><i class="menu-icon fa fa-long-arrow-right"></i><span>{{ getPhrase('assets') }}</span></a>
								</li>
								<!-- #Menu item-->
									<!-- Menu item-->
									<li class="menu-item">
									<a href="{{URL_ASSET_PURCHASE}}"><i class="menu-icon fa fa-long-arrow-right"></i><span>{{ getPhrase('purchases') }}</span></a>
								</li>
								<!-- #Menu item-->
									<!-- Menu item-->
									<li class="menu-item">
									<a href="{{URL_ASSET_ASSIGN}}"><i class="menu-icon fa fa-long-arrow-right"></i><span>{{ getPhrase('assign_asset') }}</span></a>
								</li>
								<!-- #Menu item-->
							</ul>
							<!-- #Sub menu-->
						</li>
						<!-- #Menu item-->

						<!-- Menu item-->
							<li class="menu-item">
								<a  href="{{URL_VISITORS}}"><i class="menu-icon fa fa-users"></i><span>{{ getPhrase('visitor_management') }}</span></a>
							</li>
							<!-- #Menu item-->

							<!-- Menu item-->
							<li class="menu-item">
								<a  href="{{URL_CERTIFICATES}}"><i class="menu-icon fa fa-certificate"></i><span>{{ getPhrase('certificates_management') }}</span></a>
							</li>
							<!-- #Menu item-->

					<!-- Menu item-->
					<li class="menu-item">
							<a href="{{REPOTS_DASHBOARD}}"><i class="menu-icon fa fa-file-text-o"></i><span>{{ getPhrase('reports') }}</span><i class="fa fa-angle-left pull-right toggle"></i></a>
							<!-- Sub menu-->
							<ul class="sub-menu">
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_STUDENT_CONSOLIDATE_VIEW}}"><i class="menu-icon fa fa-link"></i><span>{{ getPhrase('consolidate_reports') }}</span></a>
								</li>
								<!-- #Menu item-->
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_ONLINE_MARKS_REPORTS}}"><i class="menu-icon fa fa-file-text-o"></i><span>{{ getPhrase('online_marks_reports') }}</span></a>
								</li>
								<!-- #Menu item-->
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_STUDENT_MARKS_REPORT}}"><i class="menu-icon fa fa-chain-broken"></i><span>{{ getPhrase('offline_marks_reports') }}</span></a>
								</li>
								<!-- #Menu item-->
							</ul>
							<!-- #Sub menu-->
						</li>
						<!-- #Menu item-->

						<!-- Menu item-->
						<li class="menu-item">
							<a href="#"><i class="menu-icon fa fa-flag"></i><span>{{ getPhrase('payment_reports') }}</span><i class="fa fa-angle-left pull-right toggle"></i></a>
							<!-- Sub menu-->
							<ul class="sub-menu">
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_ONLINE_PAYMENT_REPORTS}}"><i class="menu-icon fa fa-link"></i><span>{{ getPhrase('online_payments') }}</span></a>
								</li>
								<!-- #Menu item-->
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_OFFLINE_PAYMENT_REPORTS}}"><i class="menu-icon fa fa-chain-broken"></i><span>{{ getPhrase('offline_payments') }}</span></a>
								</li>
								<!-- #Menu item-->
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_PAYMENT_REPORT_EXPORT}}"><i class="menu-icon fa fa-file-excel-o"></i><span>{{ getPhrase('export') }}</span></a>
								</li>
								<!-- #Menu item-->
							</ul>
							<!-- #Sub menu-->
						</li>
						<!-- #Menu item-->

						<!-- Menu item-->
						<li class="menu-item">
								<a  href="{{URL_CERTIFICATE_NOTIFICATION}}"><i class="menu-icon fa fa-bell"></i><span>{{ getPhrase('certificate_notifications') }}</span></a>
							</li>
							<!-- #Menu item-->

						<!-- Menu item-->
						<li class="menu-item">
							<a href="#"><i class="menu-icon fa fa-users"></i><span>{{ getPhrase('alumni') }}</span><i class="fa fa-angle-left pull-right toggle"></i></a>
							<!-- Sub menu-->
							<ul class="sub-menu">
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_ALUMNI_USERS}}"><i class="menu-icon fa fa-long-arrow-right"></i><span>{{ getPhrase('users') }}</span></a>
								</li>
								<!-- #Menu item-->
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_ALUMNI_EVENTS}}"><i class="menu-icon fa fa-long-arrow-right"></i><span>{{ getPhrase('events') }}</span></a>
								</li>
								<!-- #Menu item-->
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_ALUMNI_STORIES}}"><i class="menu-icon fa fa-long-arrow-right"></i><span>{{ getPhrase('stories') }}</span></a>
								</li>
								<!-- #Menu item-->
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_ALUMNI_NOTICES}}"><i class="menu-icon fa fa-long-arrow-right"></i><span>{{ getPhrase('notice_board') }}</span></a>
								</li>
								<!-- #Menu item-->
									<!-- Menu item-->
									<li class="menu-item">
									<a href="{{URL_ALUMNI_GALLERY}}"><i class="menu-icon fa fa-long-arrow-right"></i><span>{{ getPhrase('gallery') }}</span></a>
								</li>
								<!-- #Menu item-->
									<!-- Menu item-->
									<li class="menu-item">
									<a href="{{URL_ALUMNI_DONATIONS}}"><i class="menu-icon fa fa-long-arrow-right"></i><span>{{ getPhrase('donations') }}</span></a>
								</li>
								<!-- #Menu item-->
								<!-- Menu item-->
								<li class="menu-item">
								<a href="{{URL_ALUMNI_USERS_DONATIONS_LIST}}"><i class="menu-icon fa fa-long-arrow-right"></i><span>{{ getPhrase('user_donations') }}</span></a>
								</li>
								<!-- #Menu item-->
							</ul>
							<!-- #Sub menu-->
						</li>
						<!-- #Menu item-->

						<!-- Menu item-->
						<li class="menu-item">
							<a href="{{URL_SETTINGS_DASHBOARD}}"><i class="menu-icon fa fa-cog"></i><span>{{ getPhrase('master_settings') }}</span><i class="fa fa-angle-left pull-right toggle"></i></a>
							<!-- Sub menu-->
							<ul class="sub-menu">
								<!-- Menu item-->
								@if(checkRole(getUserGrade(2)))

								<li class="menu-item">
									<a href="{{URL_COURSES_DASHBOARD}}"><i class="menu-icon fa fa-cogs"></i><span>{{ getPhrase('master_setup') }}</span></a>
								</li>
								<li class="menu-item">
									<a href="{{URL_MASTERSETTINGS_SETTINGS}}"><i class="menu-icon icon-settings"></i><span>{{ getPhrase('settings') }}</span></a>
								</li>
								@endif

								<li class="menu-item">
									<a href="{{URL_MASTERSETTINGS_RELIGIONS}}"><i class="menu-icon fa fa-rebel"></i><span>{{ getPhrase('religions_master') }}</span></a>
								</li>
								<!-- #Menu item-->
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_MASTERSETTINGS_CATEGORIES}}"><i class="menu-icon fa fa-arrows"></i><span>{{ getPhrase('categories_master') }}</span></a>
								</li>
								<!-- #Menu item-->
								<!-- Menu item-->
								<li class="menu-item">
									<a href="{{URL_MASTERSETTINGS_EMAIL_TEMPLATES}}"><i class="menu-icon fa fa-telegram"></i><span>{{ getPhrase('email_templates') }}</span></a>
								</li>
								<!-- #Menu item-->

							</ul>
							<!-- #Sub menu-->
						</li>
						<!-- #Menu item-->
                            </ul>
                        </nav>
	            </div>
	            <div class="tab-pane px-3 pb-5" id="side-comments" role="tabpanel">
	                <div class="input-group input-right-icon mt-4">
	                    <input type="text" class="form-control search-control br-circle bbw-0" data-list=".sidebar-comment-list" placeholder="Search">
	                    <i class="input-icon fa fa-search"></i>
	                </div>
	                <span class="sidebar-header pt-2">New Comments</span>
	                <div class="sidebar-comments">
	                    <div class="list sidebar-comment-list">
	                        <a href="chat.html" class="item mb-5 p-0">
	                            <span class="message-count on-line">3</span>
	                            <img class="rounded-circle bgc-white-darkest image" alt="" src="assets/img/picture/pic6.jpg">
	                            <div class="content pl-1">
	                                <span class="header">Jamie Jones</span>
	                                <div class="description">" Sed ut perspiciatis un ...</div>
	                            </div>
	                        </a>
	                        <a href="chat.html" class="item mb-5 p-0">
	                            <span class="message-count on-line">2</span>
	                            <img class="rounded-circle bgc-white-darkest image" alt="" src="assets/img/picture/pic5.jpg">
	                            <div class="content pl-1">
	                                <span class="header">Jamie Jones</span>
	                                <div class="description">The master-builder of ca ...</div>
	                            </div>
	                        </a>
	                        <a href="chat.html" class="item mb-5 p-0">
	                            <span class="message-count on-line">2</span>
	                            <img class="rounded-circle bgc-white-darkest image" alt="" src="assets/img/picture/pic4.jpg">
	                            <div class="content pl-1">
	                                <span class="header">Taylor Fletcher</span>
	                                <div class="description">All this mistaken idea g ...</div>
	                            </div>
	                        </a>
	                        <a href="chat.html" class="item mb-5 p-0">
	                            <span class="message-count on-line"></span>
	                            <img class="rounded-circle bgc-white-darkest image" alt="" src="assets/img/picture/pic3.jpg">
	                            <div class="content pl-1">
	                                <span class="header">Rhys Brooks</span>
	                                <div class="description">Denouncing pleasure ...</div>
	                            </div>
	                        </a>
	                        <a href="chat.html" class="item mb-5 p-0">
	                            <span class="message-count on-line">1</span>
	                            <img class="rounded-circle bgc-white-darkest image" alt="" src="assets/img/picture/pic2.jpg">
	                            <div class="content pl-1">
	                                <span class="header">Jamie Jones</span>
	                                <div class="description">Expound the actual tea ...</div>
	                            </div>
	                        </a>
	                        <a href="chat.html" class="item mb-5 p-0">
	                            <span class="message-count on-line"></span>
	                            <img class="rounded-circle bgc-white-darkest image" alt="" src="assets/img/picture/pic1.jpg">
	                            <div class="content pl-1">
	                                <span class="header">Brock Giles</span>
	                                <div class="description">Praising pain was born ...</div>
	                            </div>
	                        </a>
	                        <a href="chat.html" class="item mb-5 p-0">
	                            <span class="message-count"></span>
	                            <img class="rounded-circle bgc-white-darkest image" alt="" src="assets/img/picture/pic6.jpg">
	                            <div class="content pl-1">
	                                <span class="header">Dariel Wallace</span>
	                                <div class="description">Will give you a complete ...</div>
	                            </div>
	                        </a>
	                        <a href="chat.html" class="item mb-5 p-0">
	                            <span class="message-count"></span>
	                            <img class="rounded-circle bgc-white-darkest image" alt="" src="assets/img/picture/pic5.jpg">
	                            <div class="content pl-1">
	                                <span class="header">Jared Allison</span>
	                                <div class="description">Expound the actual te ...</div>
	                            </div>
	                        </a>
	                        <a href="chat.html" class="item mb-5 p-0">
	                            <span class="message-count"></span>
	                            <img class="rounded-circle bgc-white-darkest image" alt="" src="assets/img/picture/pic4.jpg">
	                            <div class="content pl-1">
	                                <span class="header">Jamie Jones</span>
	                                <div class="description">Praising pain was born ...</div>
	                            </div>
	                        </a>
	                        <a href="chat.html" class="item p-0">
	                            <span class="message-count"></span>
	                            <img class="rounded-circle bgc-white-darkest image" alt="" src="assets/img/picture/pic3.jpg">
	                            <div class="content pl-1">
	                                <span class="header">Hunter Atkins</span>
	                                <div class="description">The great explorer of th ...</div>
	                            </div>
	                        </a>
	                    </div>
	                </div>
	            </div>
	            <div class="tab-pane px-3 pb-5" id="side-messages" role="tabpanel">
	                <span class="sidebar-header">Latest Emails</span>
	                <div class="sidebar-email">
	                    <div class="list">
	                        <a href="mail-inbox.html" class="item mb-5 p-0">
	                            <div class="content">
	                                <div class="header"><span>Jamie Jones</span><span class="fs-7 pull-right">Sep 15</span>
	                                </div>
	                                <div class="subject"><span>Lorem ipsum.</span>
	                                    <div class="icons">
	                                        <i class="fa fa-star c-warning"></i>
	                                    </div>
	                                </div>
	                                <div class="description">A alias aperiam corporis doloremque eligendi error, in laudantium minus molestiae ... </div>
	                            </div>
	                        </a>
	                        <a href="mail-inbox.html" class="item mb-5 p-0">
	                            <div class="content">
	                                <div class="header"><span>Jamie Jones</span><i class="fa icon-ml fa-paperclip"></i><span class="fs-7 pull-right">Sep 15</span></div>
	                                <div class="subject"><span>Lorem ipsum.</span>
	                                    <div class="icons">
	                                        <i class="fa fa-exclamation-circle c-danger"></i>
	                                        <i class="fa fa-star c-warning"></i>
	                                    </div>
	                                </div>
	                                <div class="description">Animi dolorum facere iure minus nam placeat quasi, sapiente temporibus ullam ut ... </div>
	                            </div>
	                        </a>
	                        <a href="mail-inbox.html" class="item mb-5 p-0">
	                            <div class="content">
	                                <div class="header"><span>Taylor Fletcher</span><span class="fs-7 pull-right">Sep 15</span></div>
	                                <div class="subject"><span>Lorem ipsum.</span>
	                                    <div class="icons">
	                                        <i class="fa fa-tags c-primary"></i>
	                                    </div>
	                                </div>
	                                <div class="description">A ad, assumenda at culpa dolorem doloribus enim et expedita ipsa iste minima ... </div>
	                            </div>
	                        </a>
	                        <a href="mail-inbox.html" class="item mb-5 p-0">
	                            <div class="content">
	                                <div class="header"><span>Rhys Brooks</span><span class="fs-7 pull-right">Sep 15</span>
	                                </div>
	                                <div class="subject"><span>Lorem ipsum.</span>
	                                    <div class="icons">
	                                        <i class="fa fa-tags c-primary"></i>
	                                        <i class="fa fa-exclamation-circle c-danger"></i>
	                                    </div>
	                                </div>
	                                <div class="description">Accusantium debitis distinctio ipsam iure natus, possimus, ratione reiciendis similique soluta sunt ... </div>
	                            </div>
	                        </a>
	                        <a href="mail-inbox.html" class="item mb-5 p-0">
	                            <div class="content">
	                                <div class="header"><span>Jamie Jones</span><span class="fs-7 pull-right">Sep 15</span>
	                                </div>
	                                <div class="subject"><span>Lorem ipsum.</span>
	                                    <div class="icons">
	                                        <i class="fa fa-star c-warning"></i>
	                                    </div>
	                                </div>
	                                <div class="description">Atque, aut beatae deserunt doloribus eius error est laborum libero natus omnis quibusdam ... </div>
	                            </div>
	                        </a>
	                        <a href="mail-inbox.html" class="item mb-5 p-0">
	                            <div class="content">
	                                <div class="header"><span>Brock Giles</span><span class="fs-7 pull-right">Sep 15</span>
	                                </div>
	                                <div class="subject"><span>Lorem ipsum.</span>
	                                    <div class="icons">
	                                    </div>
	                                </div>
	                                <div class="description">A ab consequuntur dignissimos doloribus et eum eveniet expedita, incidunt iste, iur ... </div>
	                            </div>
	                        </a>
	                        <a href="mail-inbox.html" class="item mb-5 p-0">
	                            <div class="content">
	                                <div class="header"><span>Dariel Wallace</span><span class="fs-7 pull-right">Sep 15</span></div>
	                                <div class="subject"><span>Lorem ipsum.</span>
	                                    <div class="icons">
	                                        <i class="fa fa-tags c-primary"></i>
	                                        <i class="fa fa-exclamation-circle c-danger"></i>
	                                        <i class="fa fa-star c-warning"></i>
	                                    </div>
	                                </div>
	                                <div class="description">Consequatur error id laborum nihil. Aliquam animi, doloremque fugit non quasi quis ... </div>
	                            </div>
	                        </a>
	                        <a href="mail-inbox.html" class="item mb-5 p-0">
	                            <div class="content">
	                                <div class="header"><span>Jared Allison</span><span class="fs-7 pull-right">Sep 15</span></div>
	                                <div class="subject"><span>Lorem ipsum.</span>
	                                    <div class="icons">
	                                        <i class="fa fa-star c-warning"></i>
	                                    </div>
	                                </div>
	                                <div class="description">Accusamus dolor, doloribus ducimus eum eveniet expedita facilis perspiciatis quaerat? </div>
	                            </div>
	                        </a>
	                        <a href="mail-inbox.html" class="item mb-5 p-0">
	                            <div class="content">
	                                <div class="header"><span>Jamie Jones</span><span class="fs-7 pull-right">Sep 15</span>
	                                </div>
	                                <div class="subject"><span>Lorem ipsum.</span>
	                                    <div class="icons">
	                                        <i class="fa fa-tags c-primary"></i>
	                                        <i class="fa fa-exclamation-circle c-danger"></i>
	                                    </div>
	                                </div>
	                                <div class="description">Accusantium ad blanditiis corporis deleniti ducimus, eaque earum eveniet hic illum, nisi ... </div>
	                            </div>
	                        </a>
	                        <a href="mail-inbox.html" class="item p-0">
	                            <div class="content">
	                                <div class="header"><span>Hunter Atkins</span><span class="fs-7 pull-right">Sep 15</span></div>
	                                <div class="subject"><span>Lorem ipsum.</span>
	                                    <div class="icons">
	                                        <i class="fa fa-star c-warning"></i>
	                                    </div>
	                                </div>
	                                <div class="description">Animi architecto at consequuntur deleniti doloribus earum et facilis minima modi officia speriores ... </div>
	                            </div>
	                        </a>
	                    </div>
	                </div>
	            </div>
	            <div class="tab-pane px-3 pb-5" id="side-alerts" role="tabpanel">
	                <span class="sidebar-header">Last Alerts</span>
	                <div class="sidebar-warning threaded">
	                    <a href="project-task.html" class="tm-line-item">
	                        <div class="tm-icon bgc-primary"><i class="fa fa-envelope"></i></div>
	                        <div class="content">
	                            <div class="metadata">
	                                <span class="date">Today</span>
	                                <span class="hour">10:30 PM</span>
	                            </div>
	                            <p class="text"> Lorem Ipsum is simply dummy text of industry. </p>
	                        </div>
	                    </a>
	                    <a href="project-task.html" class="tm-line-item">
	                        <div class="tm-icon-empty bgc-primary"></div>
	                        <div class="content">
	                            <div class="metadata">
	                                <span class="hour">11:33 AM</span>
	                            </div>
	                            <p class="text"> Consectetur adipiscing elit. </p>
	                        </div>
	                    </a>
	                    <a href="project-task.html" class="tm-line-item">
	                        <div class="tm-icon-empty bgc-primary"></div>
	                        <div class="content">
	                            <div class="metadata">
	                                <span class="hour">13:20 PM</span>
	                            </div>
	                            <p class="text"> Sed ut perspiciatis unde omnis. </p>
	                        </div>
	                    </a>
	                </div>
	                <span class="sidebar-header">Code Errors</span>
	                <div class="sidebar-warning threaded">
	                    <a href="project-task.html" class="tm-line-item">
	                        <div class="tm-icon bgc-danger"><i class="fa fa-bug"></i></div>
	                        <div class="content">
	                            <div class="metadata">
	                                <span class="date">Yesterday</span>
	                                <span class="hour">05:56 PM</span>
	                            </div>
	                            <p class="text"> Nemo enim ipsam voluptatem quia. </p>
	                        </div>
	                    </a>
	                    <a href="project-task.html" class="tm-line-item">
	                        <div class="tm-icon-empty bgc-danger"><i class="fa fa-bag"></i></div>
	                        <div class="content">
	                            <div class="metadata">
	                                <span class="date">Today</span>
	                                <span class="hour">11:10 AM</span>
	                            </div>
	                            <p class="text"> Sed quia non numquam eius! </p>
	                        </div>
	                    </a>
	                </div>
	                <div class="sidebar-warning threaded mt-6">
	                    <a href="project-task.html" class="tm-line-item">
	                        <div class="tm-icon bgc-warning"><i class="fa fa-exclamation-circle"></i></div>
	                        <div class="content">
	                            <div class="metadata">
	                                <span class="date">Sep 2, 2016</span>
	                                <span class="hour">13:20 PM</span>
	                            </div>
	                            <p class="text"> Sed ut perspiciatis unde accusantium doloremque </p>
	                        </div>
	                    </a>
	                    <a href="project-task.html" class="tm-line-item">
	                        <div class="tm-icon bgc-info"><i class="fa fa-life-bouy"></i></div>
	                        <div class="content">
	                            <div class="metadata">
	                                <span class="date">Aug 25, 2016</span>
	                                <span class="hour">05:56 PM</span>
	                            </div>
	                            <p class="text"> Nemo enim ipsam quia aut odit aut fugit! </p>
	                        </div>
	                    </a>
	                    <a href="project-task.html" class="tm-line-item">
	                        <div class="tm-icon-empty bgc-info"></div>
	                        <div class="content">
	                            <div class="metadata">
	                                <span class="date">Aug 25, 2016</span>
	                                <span class="hour">05:56 PM</span>
	                            </div>
	                            <p class="text"> Nemo enim ipsam quia aut odit aut fugit! </p>
	                        </div>
	                    </a>
	                </div>
	            </div>
	            <div class="tab-pane px-3 pb-5 c-body-lightest" id="side-settings" role="tabpanel">
	            	<span class="sidebar-header">Settings</span>
	                <nav class="sidebar-menu pb-3" role="navigation">
	                    <ul class="clear-style">
	                        <li class="menu-item">
	                            <a href="{{URL_USERS_EDIT.Auth::user()->slug}}">
	                                <i class="menu-icon fa fa-user"></i>
	                                <span>{{ getPhrase('my profile') }}</span>
	                            </a>
	                        </li>
	                        <li class="menu-item">
	                            <a href="{{URL_USERS_CHANGE_PASSWORD.Auth::user()->slug}}">
	                                <i class="menu-icon fa fa-key"></i>
	                                <span>{{  getPhrase('change password') }} </span>
	                            </a>
	                            
	                        </li>
	                        <!-- #Menu item-->
	                        <li class="menu-item">
	                            <a href="{{URL_FEEDBACK_SEND}}">
	                                <i class="menu-icon fa fa-commenting-o"></i>
	                                <span>{{ getPhrase('feedback') }}</span>
	                            </a>
	                        </li>
	                        <li class="menu-item">
	                            <a href="{{URL_USERS_LOGOUT}}">
	                                <i class="menu-icon fa fa-sign-out"></i>
	                                <span>{{ getPhrase('logout') }}</span>
	                            </a>
	                        </li>
	                    </ul>
	                </nav>
	            </div>
	            <div class="tab-pane px-3 pb-5 c-body-lightest" id="side-settings1" role="tabpanel">
	                <span class="sidebar-header">Grid Settings</span>
	                <div>
	                    <a href="" class="w-100 mb-2 btn btn-outline-sidebar restore-grid"><i class="icon-mr menu-icon fa fa-refresh"></i><span>Restore Grid</span></a>
	                    <a href="" class="w-100 mb-2 btn btn-outline-sidebar destroy-grid"><i class="icon-mr menu-icon fa fa-close"></i><span>Destroy Grid</span></a>
	                    <a href="" class="w-100 mb-2 btn btn-outline-sidebar remove-data"><i class="icon-mr menu-icon icon-trash"></i><span>Remove Data</span></a>
	                </div>
	                <span class="sidebar-header">Input Settings</span>
	                <div class="form-group mb-4">
	                    <label class="c-body-inverse-dark" for="sidebarInputSettings1">New Settings</label>
	                    <input type="email" class="form-control" id="sidebarInputSettings1" placeholder="New Value">
	                </div>
	                <div class="form-group mb-4">
	                    <label class="c-body-inverse-dark" for="sidebarInputSettings2">Another Settings</label>
	                    <input type="email" class="form-control" id="sidebarInputSettings2" placeholder="Your email">
	                </div>
	                <span class="sidebar-header">Dropdown Settings</span>
	                <div class="ui fluid selection simple-select select-dropdown mb-4">
	                    <input type="hidden" name="gender">
	                    <i class="select-dropdown icon"></i>
	                    <div class="default text">Profession</div>
	                    <div class="menu">
	                        <div class="item" data-value="2">Manager</div>
	                        <div class="item" data-value="1">Designer</div>
	                        <div class="item" data-value="0">Architecture</div>
	                    </div>
	                </div>
	                <select class="ui fluid simple-select select-dropdown mb-4">
	                    <option value="">Gender</option>
	                    <option value="1">Male</option>
	                    <option value="0">Female</option>
	                </select>
	                <span class="sidebar-header d-block">Checkboxes</span>
	                <div class="clerfix">
	                    <div class="ui dynamic checkbox checkbox-fill mb-1">
	                        <input type="checkbox" name="checkbox-color">
	                        <label><i class="ion-ios-arrow-right icon-mr-ch"></i>Hotels</label>
	                    </div>
	                    <span class="pull-right">45</span>
	                </div>
	                <div class="clerfix">
	                    <div class="ui dynamic checkbox checkbox-fill mb-1">
	                        <input type="checkbox" name="checkbox-color">
	                        <label><i class="ion-ios-arrow-right icon-mr-ch"></i>Hotels</label>
	                    </div>
	                    <span class="pull-right">45</span>
	                </div>
	                <div class="clerfix">
	                    <div class="ui dynamic checkbox checkbox-fill mb-1">
	                        <input type="checkbox" name="checkbox-color">
	                        <label><i class="ion-ios-arrow-right icon-mr-ch"></i>Shopping</label>
	                    </div>
	                    <span class="pull-right">39</span>
	                </div>
	                <div class="clerfix">
	                    <div class="ui dynamic checkbox checkbox-fill mb-1">
	                        <input type="checkbox" name="checkbox-color">
	                        <label><i class="ion-ios-arrow-right icon-mr-ch"></i>Restaurant</label>
	                    </div>
	                    <span class="pull-right">62</span>
	                </div>
	                <div class="clerfix">
	                    <div class="ui dynamic checkbox checkbox-fill mb-1">
	                        <input type="checkbox" name="checkbox-color">
	                        <label><i class="ion-ios-arrow-right icon-mr-ch"></i>Beauty & Spa</label>
	                    </div>
	                    <span class="pull-right">105</span>
	                </div>
	                <span class="sidebar-header">Input Settings</span>
	                <div class="form-group mb-3">
	                    <label class="c-body-inverse-dark" for="sidebarInputSettings3">New Settings</label>
	                    <input type="email" class="form-control" id="sidebarInputSettings3" placeholder="New Value">
	                </div>
	                <span class="sidebar-header">Date of Sell</span>
	                <section class="mb-4">
	                    <div id="year-slider"></div>
	                    <div class="handles fs-6-plus mt-3 d-flex justify-content-center">
	                        <div class="text-right pr-2 brw-1 bc-body">
	                            <span class="d-block fs-7">Min</span>
	                            <span class="d-block handle-0 fs-6 c-body-inverse"></span>
	                        </div>
	                        <div class="text-left pl-2">
	                            <span class="d-block fs-7">Max</span>
	                            <span class="d-block handle-1 fs-6 c-body-inverse"></span>
	                        </div>
	                    </div>
	                </section>
	                <span class="sidebar-header d-block">Radios</span>
	                <div class="clerfix">
	                    <div class="ui dynamic checkbox radio checkbox-fill mb-1">
	                        <input type="radio" name="checkbox-color">
	                        <label><i class="ion-ios-arrow-right icon-mr-ch"></i>With Swimming Pool</label>
	                    </div>
	                    <span class="pull-right">45</span>
	                </div>
	                <div class="clerfix">
	                    <div class="ui dynamic checkbox radio checkbox-fill mb-1">
	                        <input type="radio" name="checkbox-color">
	                        <label><i class="ion-ios-arrow-right icon-mr-ch"></i>With Garage</label>
	                    </div>
	                    <span class="pull-right">45</span>
	                </div>
	                <div class="clerfix">
	                    <div class="ui dynamic checkbox radio checkbox-fill mb-1">
	                        <input type="radio" name="checkbox-color">
	                        <label><i class="ion-ios-arrow-right icon-mr-ch"></i>Tree Room & Lodge</label>
	                    </div>
	                    <span class="pull-right">39</span>
	                </div>
	                <div class="clerfix">
	                    <div class="ui dynamic checkbox radio checkbox-fill mb-1">
	                        <input type="radio" name="checkbox-color">
	                        <label><i class="ion-ios-arrow-right icon-mr-ch"></i>With 2 Bathroom</label>
	                    </div>
	                    <span class="pull-right">62</span>
	                </div>
	                <div class="clerfix">
	                    <div class="ui dynamic checkbox radio checkbox-fill mb-1">
	                        <input type="radio" name="checkbox-color">
	                        <label><i class="ion-ios-arrow-right icon-mr-ch"></i>With 2 Bedroom</label>
	                    </div>
	                    <span class="pull-right">105</span>
	                </div>
	                <span class="sidebar-header d-block">Sliders</span>
	                <section class="mb-4">
	                    <h6 class="mb-2">Room Count</h6>
	                    <div id="room-slider"></div>
	                    <div class="handles fs-6-plus mt-3 d-flex justify-content-center">
	                        <div class="text-right pr-2 brw-1 bc-body">
	                            <span class="d-block fs-7">Min</span>
	                            <span class="d-block handle-0 fs-6 c-body-inverse"></span>
	                        </div>
	                        <div class="text-left pl-2">
	                            <span class="d-block fs-7">Max</span>
	                            <span class="d-block handle-1 fs-6 c-body-inverse"></span>
	                        </div>
	                    </div>
	                </section>
	                <section class="mb-4">
	                    <h6 class="mb-2">Construction Date</h6>
	                    <div id="construction-slider"></div>
	                    <div class="handles fs-6-plus mt-3 d-flex justify-content-center">
	                        <div class="text-right pr-2 brw-1 bc-body">
	                            <span class="d-block fs-7">Min</span>
	                            <span class="d-block handle-0 fs-6 c-body-inverse"></span>
	                        </div>
	                        <div class="text-left pl-2">
	                            <span class="d-block fs-7">Max</span>
	                            <span class="d-block handle-1 fs-6 c-body-inverse"></span>
	                        </div>
	                    </div>
	                </section>
	            </div>
	        </div>
	    </div>
	</section>
    <!--End Sidebar-->



    <!--Begin aside-->
    <aside id="aside" class="aside-wrap" role="complementary">
        <div class="tab-content">
            <section class="tab-pane active" id="aside-contacts" role="tabpanel">
                <div class="panel-wrap">
                    <div class="panel chat-profile-about bgc-white-dark">
                        <div class="panel-header bbw-1 btw-1 bc-gray-lighter clearfix bgc-white-dark panel-header-p panel-header-md">
                            <div class="panel-input pull-left">
                                <input class="form-control search-control" type="text" placeholder="Search" data-list=".chat-members-list">
                                <a href="" class="clear-style"><i class="fa fa-search"></i></a>
                            </div>
                            <div class="panel-icons">
                                <ul>
                                    <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row flex-column-reverse flex-sm-row">
                                <div class="col-sm-8 brw-1 pr-0 bc-gray-lighter">
                                    <div class="chat-members contact-list pt-3 px-4 pb-4">
                                        <div class="list chat-members-list">
                                            <div class="item mb-5 p-0">
                                                <a href="contacts.html">
                                                    <img class="rounded-circle bgc-white-darkest image" alt="" src="assets/img/picture/pic1.jpg">
                                                    <div class="content pl-3">
                                                        <span class="header">Jamie Jones</span>
                                                        <div class="description">Product Manager</div>
                                                    </div>
                                                </a>
                                                <div class="pull-right edit-btns">
                                                    <a href=""><i class="fa fa-edit"></i></a>
                                                    <a href=""><i class="fa fa-close"></i></a>
                                                </div>
                                            </div>
                                            <div class="item mb-5 p-0">
                                                <a href="contacts.html">
                                                    <img class="rounded-circle bgc-white-darkest image" alt="" src="assets/img/picture/pic2.jpg">
                                                    <div class="content pl-3">
                                                        <span class="header">Jamie Jones</span>
                                                        <div class="description">Developer</div>
                                                    </div>
                                                </a>
                                                <div class="pull-right edit-btns">
                                                    <a href=""><i class="fa fa-edit"></i></a>
                                                    <a href=""><i class="fa fa-close"></i></a>
                                                </div>
                                            </div>
                                            <div class="item mb-5 p-0">
                                                <a href="contacts.html">
                                                    <img class="rounded-circle bgc-white-darkest image" alt="" src="assets/img/picture/pic4.jpg">
                                                    <div class="content pl-3">
                                                        <span class="header">Taylor Fletcher</span>
                                                        <div class="description">Developer</div>
                                                    </div>
                                                </a>
                                                <div class="pull-right edit-btns">
                                                    <a href=""><i class="fa fa-edit"></i></a>
                                                    <a href=""><i class="fa fa-close"></i></a>
                                                </div>
                                            </div>
                                            <div class="item mb-5 p-0">
                                                <a href="contacts.html">
                                                    <img class="rounded-circle bgc-white-darkest image" alt="" src="assets/img/picture/pic3.jpg">
                                                    <div class="content pl-3">
                                                        <span class="header c-gray-dark font-weight-bold">Rhys Brooks</span>
                                                        <div class="description">Lead Developer</div>
                                                    </div>
                                                </a>
                                                <div class="pull-right edit-btns">
                                                    <a href=""><i class="fa fa-edit"></i></a>
                                                    <a href=""><i class="fa fa-close"></i></a>
                                                </div>
                                            </div>
                                            <div class="item mb-5 p-0">
                                                <a href="contacts.html">
                                                    <img class="rounded-circle bgc-white-darkest image" alt="" src="assets/img/picture/pic5.jpg">
                                                    <div class="content pl-3">
                                                        <span class="header">Jamie Jones</span>
                                                        <div class="description">QA Tester</div>
                                                    </div>
                                                </a>
                                                <div class="pull-right edit-btns">
                                                    <a href=""><i class="fa fa-edit"></i></a>
                                                    <a href=""><i class="fa fa-close"></i></a>
                                                </div>
                                            </div>
                                            <div class="item mb-5 p-0">
                                                <a href="contacts.html">
                                                    <img class="rounded-circle bgc-white-darkest image" alt="" src="assets/img/picture/pic6.jpg">
                                                    <div class="content pl-3">
                                                        <span class="header">Brock Giles</span>
                                                        <div class="description">Support Manager</div>
                                                    </div>
                                                </a>
                                                <div class="pull-right edit-btns">
                                                    <a href=""><i class="fa fa-edit"></i></a>
                                                    <a href=""><i class="fa fa-close"></i></a>
                                                </div>
                                            </div>
                                            <div class="item mb-5 p-0">
                                                <a href="contacts.html">
                                                    <img class="rounded-circle bgc-white-darkest image" alt="" src="assets/img/picture/pic1.jpg">
                                                    <div class="content pl-3">
                                                        <span class="header">Dariel Wallace</span>
                                                        <div class="description">Team Member</div>
                                                    </div>
                                                </a>
                                                <div class="pull-right edit-btns">
                                                    <a href=""><i class="fa fa-edit"></i></a>
                                                    <a href=""><i class="fa fa-close"></i></a>
                                                </div>
                                            </div>
                                            <div class="item mb-5 p-0">
                                                <a href="contacts.html">
                                                    <img class="rounded-circle bgc-white-darkest image" alt="" src="assets/img/picture/pic2.jpg">
                                                    <div class="content pl-3">
                                                        <span class="header">Jared Allison</span>
                                                        <div class="description">Team Member</div>
                                                    </div>
                                                </a>
                                                <div class="pull-right edit-btns">
                                                    <a href=""><i class="fa fa-edit"></i></a>
                                                    <a href=""><i class="fa fa-close"></i></a>
                                                </div>
                                            </div>
                                            <div class="item mb-5 p-0">
                                                <a href="contacts.html">
                                                    <img class="rounded-circle bgc-white-darkest image" alt="" src="assets/img/picture/pic3.jpg">
                                                    <div class="content pl-3">
                                                        <span class="header">Jamie Jones</span>
                                                        <div class="description">Team Member</div>
                                                    </div>
                                                </a>
                                                <div class="pull-right edit-btns">
                                                    <a href=""><i class="fa fa-edit"></i></a>
                                                    <a href=""><i class="fa fa-close"></i></a>
                                                </div>
                                            </div>
                                            <div class="item p-0">
                                                <a href="contacts.html">
                                                    <img class="rounded-circle bgc-white-darkest image" alt="" src="assets/img/picture/pic4.jpg">
                                                    <div class="content pl-3">
                                                        <span class="header">Hunter Atkins</span>
                                                        <div class="description">Team Member</div>
                                                    </div>
                                                </a>
                                                <div class="pull-right edit-btns">
                                                    <a href=""><i class="fa fa-edit"></i></a>
                                                    <a href=""><i class="fa fa-close"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 pr-4 pl-4 pl-sm-2 py-3">
                                    <ul class="list list-unstyled contact-categories">
                                        <li><a href="" class="btn">All Contacts</a></li>
                                        <li><a href="" class="btn">Team Members</a></li>
                                        <li><a href="" class="btn">Project Members</a></li>
                                        <li><a href="" class="btn">Friends</a></li>
                                        <li><a href="" class="btn">Clients</a></li>
                                        <li><a href="" class="btn">Support Team</a></li>
                                        <li><a href="" class="btn">Customers</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="tab-pane" id="aside-uploads" role="tabpanel">
                <div class="panel-wrap">
                    <div class="panel chat-profile-about bgc-white-dark">
                        <div class="panel-header bbw-1 btw-1 bc-gray-lighter clearfix bgc-white-dark panel-header-p panel-header-md">
                            <h2 class="pull-left"> Uploads </h2>
                            <div class="panel-icons">
                                <ul>
                                    <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="panel-body panel-body-p">
                            <div class="pb-5">
                                <div class="uploads mb-5">
                                    <div class="row mb-1">
                                        <div class="col-sm-12">
                                            <div class="drag-drop mb-3">
                                                <h3>Drag & Drop Files Here</h3>
                                                <h6>or cick to select files</h6>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="files">
                                                <ul class="list list-unstyled upload-list mb-0 upload-template dropzone-previews">
                                                    <li class="clearfix">
                                                        <div class="d-flex">
                                                            <div class="upload-name">
                                                                <img src="assets/img/empty.png" class="mr-1" data-dz-thumbnail width="50" height="50" alt="" />
                                                                <div class="d-inline-block upload-details">
                                                                    <span class="name mr-3" data-dz-name></span>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <span class="size mr-4" data-dz-size></span>
                                                            </div>
                                                            <div>
                                                                <a class="progress-circle start pull-right ml-1" href="">
                                                                    <i class="ion-ios-play-outline fs-4" data-success-icon="fa fa-check c-success" data-error-icon="fa fa-trash c-danger"></i>
                                                                </a>
                                                                <a href="" title="Click me to remove the file." data-dz-remove><span class="remove-btn btn-icon bw-1 br-circle bc-gray-lighter dz-error-mark btn-default c-danger">✘</span></a>
                                                            </div>
                                                        </div>
                                                        <span class="error text-danger" data-dz-errormessage></span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-center">
                                        <a href="" class="btn mr-1 btn-default mt-1 file-input-button">
                                            <span><i class="fa fa-file-o icon-mr c-primary"></i>New</span>
                                        </a>
                                        <a href="" class="btn mr-1 btn-default mt-1 file-start-button">
                                            <span><i class="icon-cloud-upload icon-mr c-success"></i>Upload</span>
                                        </a>
                                        <a href="" data-dz-remove class="btn btn-default mt-1 file-delete-button">
                                            <span><i class="icon-trash icon-mr c-danger"></i>Delete All</span>
                                        </a>
                                    </div>
                                </div>
                                <h6 class="p-1 bbw-1 bc-gray-lighter"> Activity Today </h6>
                                <div class="mt-3 aside-activity threaded">
                                    <a href="" class="tm-line-item">
                                        <div class="tm-icon bgc-primary"><i class="fa fa-envelope"></i></div>
                                        <div class="content">
                                            <div class="metadata">
                                                <span class="date">Today</span>
                                                <span class="hour">10:30 PM</span>
                                            </div>
                                            <div class="text">
                                                <p>Lorem Ipsum is simply dummy text of industry.</p>
                                                <div class="aside-files">
                                                    <div class="file file-sm mb-2">
                                                        <div class="image bgc-white-darkest">
                                                            <i class="fa fa-file-pdf-o"></i>
                                                        </div>
                                                        <div class="name">
                                                            <span class="d-block c-gray-dark">Ipsum.pdf</span>
                                                            <span>20/9KB Uploaded from a phone</span>
                                                        </div>
                                                    </div>
                                                    <div class="file file-sm">
                                                        <div class="image bgc-white-darkest">
                                                            <i class="fa fa-file-word-o"></i>
                                                        </div>
                                                        <div class="name">
                                                            <span class="d-block c-gray-dark">Ip Ipsum.word</span>
                                                            <span>Lorem ipsum dolor sit amet.</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="" class="tm-line-item">
                                        <div class="tm-icon-empty bgc-primary"></div>
                                        <div class="content">
                                            <div class="metadata">
                                                <span class="hour">11:33 AM</span>
                                            </div>
                                            <p class="text"> Consectetur adipiscing elit. </p>
                                        </div>
                                    </a>
                                    <a href="" class="tm-line-item">
                                        <div class="tm-icon-empty bgc-primary"></div>
                                        <div class="content">
                                            <div class="metadata">
                                                <span class="hour">13:20 PM</span>
                                            </div>
                                            <p class="text"> Consectetur adipisicing elit. Autem doloremque ex ipsum nobis non, nulla.. </p>
                                        </div>
                                    </a>
                                </div>
                                <div class="chat-room">
                                    <div class="chat-reply clearfix">
                                        <div class="pull-right textarea-group">
                                            <div class="form-control chat-reply-area"></div>
                                            <div class="chat-textarea-addon">
                                                <a href="" class="pull-right btn-icon rounded-circle btn-info"><i class="fa fa-paper-plane"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-5 aside-activity threaded">
                                    <a href="" class="tm-line-item">
                                        <div class="tm-icon bgc-warning"><img class="br-circle" src="assets/img/picture/pic3.jpg" alt=""></div>
                                        <div class="content">
                                            <div class="metadata">
                                                <span class="date">Today</span>
                                                <span class="hour">10:30 PM</span>
                                            </div>
                                            <div class="text">
                                                <p>Accusamus beatae dignissimos fuga iure numquam!</p>
                                                <div class="aside-files">
                                                    <div class="file file-sm mb-2">
                                                        <div class="image bgc-white-darkest">
                                                            <i class="fa fa-file-pdf-o"></i>
                                                        </div>
                                                        <div class="name">
                                                            <span class="d-block c-gray-dark">Ipsum.png</span>
                                                            <span>20/9KB Uploaded from my phone</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="chat-room">
                                    <div class="chat-reply clearfix">
                                        <div class="pull-right textarea-group">
                                            <div class="form-control chat-reply-area"></div>
                                            <div class="chat-textarea-addon">
                                                <a href="" class="pull-right btn-icon rounded-circle btn-info"><i class="fa fa-paper-plane"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="tab-pane" id="aside-settings" role="tabpanel">
                <div class="panel-wrap">
                    <div class="panel chat-profile-about bgc-white-dark">
                        <div class="panel-header bbw-1 btw-1 bc-gray-lighter clearfix bgc-white-dark panel-header-p panel-header-md">
                            <h2 class="pull-left"> Theme Colors & Layouts </h2>
                            <div class="panel-icons">
                                <ul>
                                    <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="panel-body pb-4 panel-body-p px-3">
                            <h5 class="mb-1">App Classes</h5>
                            <p class="fs-7 mb-1">Add these classes to the outer <code>#app</code> element if you don't need theme customizer</p>
                            <div class="alert alert-warning fs-7 mb-2" role="alert" id="aside-settings-alert">
                                <strong></strong> Better check yourself, you're not looking too good. </div>
                                <p class="fs-7 mb-3">Add <code>.no-saved-theme</code> class to `#app` element if you want the page is loaded with the specified classes (not with saved theme data - see <a href="page-starter.html">Starter Pages</a>)</p>
                                <div class="mb-3">
                                    <h5 class="mb-3"> Header </h5>
                                    <div class="site-themes">
                                        <div class="site-theme backgrounds" data-theme="header-bg-default"></div>
                                        <div class="site-theme backgrounds" data-theme="header-bg-primary"></div>
                                        <div class="site-theme backgrounds" data-theme="header-bg-info"></div>
                                        <div class="site-theme backgrounds" data-theme="header-bg-danger"></div>
                                        <div class="site-theme backgrounds" data-theme="header-bg-warning"></div>
                                    </div>
                                </div>
                                <div class="">
                                    <h5 class="mb-3"> Sidebar </h5>
                                    <div class="mb-4 hidden-sm-down">
                                        <h6 class="mb-3">Type</h6>
                                        <div class="site-themes">
                                            <div class="site-theme types" data-theme="sidebar-type-push"><span>Push</span></div>
                                            <div class="site-theme types" data-theme="sidebar-type-slide"><span>Slide</span></div>
                                            <div class="site-theme types d-none" data-theme="sidebar-type-top"><span>Top</span></div>
                                            <div class="site-theme types d-none" data-theme="sidebar-type-bottom"><span>Bottom</span></div>
                                        </div>
                                    </div>
                                    <div class="row mb-4 hidden-sm-down">
                                        <div class="col-sm-6 col-12">
                                            <h6 class="mb-3">Default States</h6>
                                            <div class="site-themes">
                                                <div class="site-theme types" data-theme="sidebar-state-open"><span>Open</span></div>
                                                <div class="site-theme types" data-theme="sidebar-state-compact"><span>Compact</span></div>
                                                <div class="site-theme types" data-theme="sidebar-state-close"><span>Close</span></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-12">
                                            <h6 class="mb-3">Triggered States</h6>
                                            <div class="site-themes">
                                                <div class="site-theme types" data-theme="sidebar-tr-state-open"><span>Open</span></div>
                                                <div class="site-theme types" data-theme="sidebar-tr-state-compact"><span>Compact</span></div>
                                                <div class="site-theme types" data-theme="sidebar-tr-state-close"><span>Close</span></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3 site-themes">
                                        <h6 class="mb-3">Background</h6>
                                        <div class="site-theme backgrounds" data-theme="sidebar-bg-default"></div>
                                        <div class="site-theme backgrounds image-backgrounds" data-theme="sidebar-bg-one">
                                            <span class="image-lazy" data-src="{{PREFIX1}}assets/img/profile-bg.jpg" data-alt=""></span>
                                            <div class="gradient"></div>
                                        </div>
                                    </div>
                                    <div class="mb-4">
                                        <h6 class="mb-3">Options</h6>
                                        <div class="row site-themes">
                                            <div class="col-sm-4 col-6">
                                                <div class="site-theme options" data-theme="sidebar-option-default">
                                                    <span data-src="{{PREFIX1}}assets/img/pages/page-starter-default.png" data-alt="" class="image-lazy"></span>
                                                </div>
                                            </div>
                                            <div class="col-sm-4 col-6">
                                                <div class="site-theme options" data-theme="sidebar-option-theme1">
                                                    <span data-src="{{PREFIX1}}assets/img/pages/page-starter-option-1.png" data-alt="" class="image-lazy"></span>
                                                </div>
                                            </div>
                                            <div class="col-sm-4 col-6">
                                                <div class="site-theme options" data-theme="sidebar-option-theme2">
                                                    <span data-src="{{PREFIX1}}assets/img/pages/page-starter-option-2.png" data-alt="" class="image-lazy"></span>
                                                </div>
                                            </div>
                                            <div class="col-sm-4 col-6">
                                                <div class="site-theme options" data-theme="sidebar-option-theme3">
                                                    <span data-src="{{PREFIX1}}assets/img/pages/page-starter-option-3.png" data-alt="" class="image-lazy"></span>
                                                </div>
                                            </div>
                                            <div class="col-sm-4 col-6">
                                                <div class="site-theme options" data-theme="sidebar-option-theme4">
                                                    <span data-src="{{PREFIX1}}assets/img/pages/page-starter-option-4.png" data-alt="" class="image-lazy"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </aside>
        
        @yield('content')
        <footer id="footer" class="footer-wrap" role="contentinfo" style="color: white;font-weight: bold;">
            <span class="fs-7">
                <i class="icon-layers fs-4 mr-2"></i> Powerd by<span class="hidden-xs-down">- Amazingwits</span>
                <span class="ml-1">&copy; 2019 - 2020</span>
            </span>
            
        </footer>
    </div>
	<!-- /#wrapper -->
	<!-- jQuery -->
	

	<!-- Bootstrap Core JavaScript -->
	<script src="{{JS}}bootstrap.min.js"></script>

 
	<!--JS Control-->
	<script src="{{JS}}main.js"></script>
	<script src="{{JS}}sweetalert-dev.js"></script>

	@if(isset($module_helper))	
	<script src="{{JS}}bootstrap-tour.min.js"></script>
	@include('common.module-helper', array('module_helper'=>$module_helper))
	@endif
    <script src="{{JS}}moment.min.js"></script>

    <script src="{{JS}}plugins/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
	<script src="{{PREFIX1}}assets/js/shim.js"></script>
    <script src="{{PREFIX1}}assets/js/settings.js"></script>
     @include('jspm.jspm')
    

      <script>
      SystemJS.import('');
  	</script>
     <script>
            SystemJS.import('scripts/chat');
        </script>

	 @yield('footer_scripts')

	@include('errors.formMessages')
     <script >
   
    function showFeeInstructions(feecat_id)
    {    
      $('#myModal').modal('show');
    }
</script>
 	@yield('custom_div_end')
	{!!getSetting('google_analytics', 'seo_settings')!!}
  <script>
        SystemJS.import('{{SCRIPTS}}dash1');
    </script>

<div class="ajax-loader" id="ajax_loader"><img src="{{AJAXLOADER}}"> {{getPhrase('please_wait')}}...</div>
 
</body>

</html>