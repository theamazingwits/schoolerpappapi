<!DOCTYPE html>

<html lang="en" dir="{{ (App\Language::isDefaultLanuageRtl()) ? 'rtl' : 'ltr' }}">



<head>

	<meta charset="utf-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<meta name="description" content="{{getSetting('meta_description', 'seo_settings')}}">

	<meta name="keywords" content="{{getSetting('meta_keywords', 'seo_settings')}}">

	<link rel="icon" href="{{PREFIX1}}assets/img/favicon.png" type="image/png">


	<link rel="icon" href="{{IMAGE_PATH_SETTINGS.getSetting('site_favicon', 'site_settings')}}" type="image/x-icon" />

	<title>@yield('title') {{ isset($title) ? $title : getSetting('site_title','site_settings') }}</title>
    <link href="{{CSS}}bootstrap.css" rel="stylesheet" type="text/css">
    <link href="{{CSS}}ajax-datatables.css" rel="stylesheet" type="text/css">
	<!-- Custom CSS -->

	<link href="{{PREFIX1}}assets/css/style.css" rel="stylesheet" type="text/css">

   <link href="{{CSS}}{{getSetting('current_theme', 'site_settings')}}-theme.css" rel="stylesheet">



    <!--===** HOSTAL LAYOUT LINKS  ===*** -->

	<!-- Bootstrap Core CSS -->
	 @yield('header_scripts')
	<!-- <link href="{{CSS}}bootstrap.min.css" rel="stylesheet"> -->
	<link rel="stylesheet" href="{{CSS}}bootstrap-datepicker.min.css">
	<link href="{{CSS}}sweetalert.css" rel="stylesheet" type="text/css">
	@if(isset($module_helper))	
	<link href="{{CSS}}bootstrap-tour.css" rel="stylesheet">
	@endif
	<!-- Morris Charts CSS -->
	<link href="{{CSS}}plugins/morris.css" rel="stylesheet">
	<!-- Proxima Nova Fonts CSS -->
	<link href="{{CSS}}proximanova.css" rel="stylesheet">


	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

	<!--[if lt IE 9]>

        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>

        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

    <![endif]-->
<!-- <style>
    .menu-icon1{
            margin-right: 14px;
    font-size: 1.28571rem;
    width: 1em;
    text-align: center;
}
</style> -->
<style>
.menu-icon1{
    font-size: 1.5em;
    padding-right:0.5em;
}
#DataTables_Table_0_wrapper .row{
    width: 100%!important;
    margin:0%;
}
#DataTables_Table_0_wrapper{
    padding: 30px;
}
.breadcrumb{

    padding: 10px 0px !important;
    margin: 0px !important;
    background: #none !important;   
}
.bc-home{
        font-size: 1.554em;
    }
.bc-row{
    margin: 0px !important;
    padding: 0px !important;
}

</style>


</head>


<body ng-app="academia">

 @yield('custom_div')
 <?php 
 $class = '';
 if(!isset($right_bar))
 	$class = 'no-right-sidebar';

 ?>


    <div id="app" class="reactive-app header-bg-danger sidebar-type-push sidebar-state-open sidebar-tr-state-compact sidebar-bg-default sidebar-option-theme4 aside-close">
		<!-- inject-body-start:js -->
		
    <script src="{{PREFIX1}}assets/js/system.js"></script>
    
        <!-- endinject -->
        <!--Begin Header-->
        <header id="header" class="header-wrap clearfix">

            <div class="header-tools">
                <a href="" data-sidebar="#sidebar" class="m-icon pull-left" aria-label="toggle menu">
                    <i class="icon m-icon-lines" aria-hidden="true"></i>
                </a>
                <div class="mega-dropdown pull-left hidden-xs-down">
                </div>
                <a href="{{ URL_USERS_LOGOUT }}" class="fs-5 clear-style pull-right">
                    <i class="fa fa-sign-out"></i>
                </a>
                <a href="" class="fs-5 clear-style pull-right hidden-xs-down page-fullscreen">
                    <i data-icon="icon-size-actual icon-size-fullscreen" class="fs-6 fw-bold icon-size-fullscreen"></i>
                </a>
            </div>
            <div id="page-title-wrap" class="page-title-wrap clearfix">
                <h1 class="page-title pull-left fs-4 fw-light">
                    <i class="fa fa fa-dashboard icon-mr fs-4"></i><span class="hidden-xs-down">{{$title}} </span>
                </h1>
                <div class="smart-links">
                    <ul class="nav" role="tablist">
                       <!--  <li class="nav-item">
                            <a class="nav-link clear-style aside-trigger" data-aside="#aside" data-toggle="tab" href="#aside-contacts" role="tab"><i class="fa fa-book"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link clear-style aside-trigger" data-aside="#aside" data-toggle="tab" href="#aside-uploads" role="tab"><i class="fa fa-cloud-upload"></i></a>
                        </li> -->
                        <li class="nav-item">
                            <a class="nav-link clear-style aside-trigger" data-aside="#aside" data-toggle="tab" href="#aside-settings" role="tab"><i class="fa fa-sliders"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
        <!--End Header-->
        <!--Begin Loader-->
        <div class="page-loader loader-wrap" id="loader-wrap">
            <div class="loader loading-icon"></div>
        </div>
        <!--End Loader-->
        <!--Begin Content-->
       
        <!--End Content-->
		<!--Begin Sidebar-->
		
        <section id="sidebar" class="sidebar-wrap">
            <div class="sidebar-content">
                <a href="{{PREFIX}}" class="app-name">
                    <span class="full-name">{{ getPhrase('dashboard') }}</span>
                    <span class="compact-name"><i class="icon-layers"></i></span>
                </a>
                <div class="profile">
                    <div class="details">
                        <div class="clearfix">
                        <div class="picture rounded-circle pull-left" style="background-image: url('{{ getProfilePath(Auth::user()->image, 'thumb') }}');"></div>
                        <div class="about pull-left">
                            <a href="" class="clear-style sbg-settings"><i class="fa fa-edit"></i></a>
                            <a href="javascript:void(0)">
                                @if(Auth::check())
                                <h3 class="name">{{Auth::user()->name}}</h3>
                                @endif
                            </a>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="tab-links">
                    <ul class="clear-style li-left nav nav-tabs border-0" role="tablist">
                        <li class="nav-item">
                            <a href="#side-home" class="bc-success active" data-toggle="tab" role="tab"><i class="fa fa-list"></i><span class="tab-title">Menu</span></a>
                        </li>
                       <!--  <li class="nav-item">
                            <a href="#side-comments" class="bc-primary" data-toggle="tab" role="tab"><i class="fa fa-comments"></i><span class="tab-title">Comments</span>
                                <span class="badge fs-8 badge-pill bgc-primary-lighter waves bc-primary">14</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#side-messages" class="bc-warning" data-toggle="tab" role="tab"><i class="fa fa-envelope"></i><span class="tab-title">Inbox</span>
                                <span class="badge fs-8 badge-pill waves bc-warning bgc-warning-lighter">32</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#side-alerts" class="bc-danger" data-toggle="tab" role="tab"><i class="fa fa-warning"></i><span class="tab-title">Alert</span>
                                <span class="badge fs-8 badge-pill waves bc-danger bgc-danger-lighter badge-counter transition hidden" data-counter="100" data-duration="400000"><span class="text"></span></span>
                            </a>
                        </li> -->
                        <li class="nav-item tab-link-add">
                            <a href="#side-settings" class="bc-success" data-toggle="tab" role="tab"><i class="fa fa-gear"></i><span class="tab-title">Settings</span></a>
                        </li>
                    </ul>
                </div>


                

                <div class="tab-content">

                <!--**  MENU LISTS ** -->
                    <div class="tab-pane active" id="side-home" role="tabpanel">
                        <nav class="sidebar-menu pb-3" role="navigation">
                            <ul class="clear-style">
                                
                                <!-- Menu header-->
                                <!-- <li class="header">User Interface</li> -->
                                <!-- #Menu header-->


                                 <!-- Menu item  **==  DASHBOARD == 0**-->
                                 <li class="menu-item">
                                    <a href="{{ PREFIX }}">
                                        <i class="menu-icon icon-layers"></i>
                                        <span>{{ getPhrase('dashboard') }}</span>
                                    </a>
                                </li>
                                <!-- #Menu item-->

                                <!-- Menu item  ** == hostel_management == 1**-->
                                <li class="menu-item">

                                    <a href="">
                                        <i class="menu-icon fa fa-building"></i>
                                        <span>{{ getPhrase('hostel_management') }} </span>
                                        <i class="fa fa-angle-left pull-right toggle"></i>
                                    </a>
                                    <!-- Sub menu-->
                                    <ul class="sub-menu">
                                        <!-- Menu item ** hostel **-->
                                        <li class="menu-item">
                                            <a href="{{URL_HOSTEL}}">
                                                <i class="menu-icon fa fa-long-arrow-right"></i>
                                                <span>{{ getPhrase('hostel') }}</span>
                                                <!-- <i class="fa fa-angle-left pull-right toggle"></i> -->
                                            </a>
                                        </li>
                                        <!-- #Menu item-->
                                        <!-- Menu item ** room_type **-->
                                        <li class="menu-item">
                                            <a href="{{URL_ROOM_TYPE}}">
                                                <i class="menu-icon fa fa-long-arrow-right"></i>
                                                <span>{{ getPhrase('room_type') }}</span>
                                                <!-- <i class="fa fa-angle-left pull-right toggle"></i> -->
                                                <!-- <small class="badge badge-pill pull-right badge-primary">+10</small> -->
                                            </a>
                                        </li>
                                        <!-- #Menu item-->
                                        <!-- Menu item ** hostel_rooms **-->
                                        <li class="menu-item">
                                            <a href="{{ URL_HOSTEL_ROOMS }}">
                                                <i class="menu-icon fa fa-long-arrow-right"></i>
                                                <span>{{ getPhrase('hostel_rooms') }}</span>
                                                <!-- <i class="fa fa-angle-left pull-right toggle"></i> -->
                                            </a>
                                        </li>
                                        <!-- #Menu item-->
                                        <!-- Menu item ** assign_hostel_to_student **-->
                                        <li class="menu-item">
                                            <a href="{{ URL_ASSIGN_HOSTEL }}">
                                                <i class="menu-icon fa fa-long-arrow-right"></i>
                                                <span>{{ getPhrase('assign_hostel_to_student') }}</span>
                                                <!-- <i class="fa fa-angle-left pull-right toggle"></i> -->
                                            </a>
                                        </li>
                                        <!-- #Menu item-->

                                        {{--    
                                        <li class="menu-item">
                                            <a href="{{ URL_ASSIGN_HOSTEL_FEE }}">
                                                <i class="menu-icon fa fa-long-arrow-right"></i>
                                                <span>{{ getPhrase('assign_hostel_fee') }}</span>
                                            
                                            </a>
                                        </li>
                                        <li class="menu-item">
                                            <a href="{{ URL_PAY_HOSTEL_FEE }}">
                                                <i class="menu-icon fa fa-long-arrow-right"></i>
                                                <span>{{ getPhrase('pay_fee') }}</span>
                                              
                                            </a>
                                        </li>
                                        <li class="menu-item">
                                            <a href="{{ URL_HOSTEL_FEE_REPORTS }}">
                                                <i class="menu-icon fa fa-long-arrow-right"></i>
                                                <span>{{ getPhrase('fee_reports') }}</span>
                                            </a>
                                        </li>  --}}

                
                                    </ul>
                                    <!-- #Sub menu-->
                                </li>
                                <!-- #Menu item-->

                                 <!-- Menu item  ** == hostel_fee_management == 1**-->
                                <li class="menu-item">

                                    <a href="">
                                        <i class="menu-icon fa fa-money"></i>
                                        <span>{{ getPhrase('hostel_fee_management') }} </span>
                                        <i class="fa fa-angle-left pull-right toggle"></i>
                                    </a>
                                    <!-- Sub menu-->
                                    <ul class="sub-menu">
                                        <!-- Menu item ** assign_hostel_fee **-->
                                        <li class="menu-item">
                                            <a href="{{URL_ASSIGN_HOSTEL_FEE}}">
                                                <i class="menu-icon fa fa-long-arrow-right"></i>
                                                <span>{{ getPhrase('assign_hostel_fee') }}</span>
                                                <!-- <i class="fa fa-angle-left pull-right toggle"></i> -->
                                            </a>
                                        </li>
                                        <!-- #Menu item-->
                                        <!-- Menu item ** pay_fee **-->
                                        <li class="menu-item">
                                            <a href="{{URL_PAY_HOSTEL_FEE}}">
                                                <i class="menu-icon fa fa-long-arrow-right"></i>
                                                <span>{{ getPhrase('pay_fee') }}</span>
                                                <!-- <i class="fa fa-angle-left pull-right toggle"></i> -->
                                                <!-- <small class="badge badge-pill pull-right badge-primary">+10</small> -->
                                            </a>
                                        </li>
                                        <!-- #Menu item-->
                                        <!-- Menu item ** fee_reports **-->
                                        <li class="menu-item">
                                            <a href="{{ URL_HOSTEL_FEE_REPORTS }}">
                                                <i class="menu-icon fa fa-long-arrow-right"></i>
                                                <span>{{ getPhrase('fee_reports') }}</span>
                                                <!-- <i class="fa fa-angle-left pull-right toggle"></i> -->
                                            </a>
                                        </li>
                                        <!-- #Menu item-->
                
                                    </ul>
                                    <!-- #Sub menu-->
                                </li>
                                <!-- #Menu item-->


                                <!-- Menu item  ** == notifications == 2**-->
                                <li class="menu-item">

                                    <a href="{{URL_NOTIFICATIONS}}">
                                        <i class="menu-icon fa fa-bell-o"></i>
                                        <span>{{ getPhrase('notifications') }} </span>
                                        <!-- <i class="fa fa-angle-left pull-right toggle"></i> -->
                                    </a>
                                </li>
                             
                            </ul>
                        </nav>
                    </div>



                    <div class="tab-pane px-3 pb-5" id="side-comments" role="tabpanel">
                        <div class="input-group input-right-icon mt-4">
                            <input type="text" class="form-control search-control br-circle bbw-0" data-list=".sidebar-comment-list" placeholder="Search">
                            <i class="input-icon fa fa-search"></i>
                        </div>
                        <span class="sidebar-header pt-2">New Comments</span>
                        <div class="sidebar-comments">
                            <div class="list sidebar-comment-list">
                                <a href="chat.html" class="item mb-5 p-0">
                                    <span class="message-count on-line">3</span>
                                    <img class="rounded-circle bgc-white-darkest image" alt="" src="assets/img/picture/pic6.jpg">
                                    <div class="content pl-1">
                                        <span class="header">Jamie Jones</span>
                                        <div class="description">" Sed ut perspiciatis un ...</div>
                                    </div>
                                </a>
                                <a href="chat.html" class="item mb-5 p-0">
                                    <span class="message-count on-line">2</span>
                                    <img class="rounded-circle bgc-white-darkest image" alt="" src="assets/img/picture/pic5.jpg">
                                    <div class="content pl-1">
                                        <span class="header">Jamie Jones</span>
                                        <div class="description">The master-builder of ca ...</div>
                                    </div>
                                </a>
                                <a href="chat.html" class="item mb-5 p-0">
                                    <span class="message-count on-line">2</span>
                                    <img class="rounded-circle bgc-white-darkest image" alt="" src="assets/img/picture/pic4.jpg">
                                    <div class="content pl-1">
                                        <span class="header">Taylor Fletcher</span>
                                        <div class="description">All this mistaken idea g ...</div>
                                    </div>
                                </a>
                                <a href="chat.html" class="item mb-5 p-0">
                                    <span class="message-count on-line"></span>
                                    <img class="rounded-circle bgc-white-darkest image" alt="" src="assets/img/picture/pic3.jpg">
                                    <div class="content pl-1">
                                        <span class="header">Rhys Brooks</span>
                                        <div class="description">Denouncing pleasure ...</div>
                                    </div>
                                </a>
                                <a href="chat.html" class="item mb-5 p-0">
                                    <span class="message-count on-line">1</span>
                                    <img class="rounded-circle bgc-white-darkest image" alt="" src="assets/img/picture/pic2.jpg">
                                    <div class="content pl-1">
                                        <span class="header">Jamie Jones</span>
                                        <div class="description">Expound the actual tea ...</div>
                                    </div>
                                </a>
                                <a href="chat.html" class="item mb-5 p-0">
                                    <span class="message-count on-line"></span>
                                    <img class="rounded-circle bgc-white-darkest image" alt="" src="assets/img/picture/pic1.jpg">
                                    <div class="content pl-1">
                                        <span class="header">Brock Giles</span>
                                        <div class="description">Praising pain was born ...</div>
                                    </div>
                                </a>
                                <a href="chat.html" class="item mb-5 p-0">
                                    <span class="message-count"></span>
                                    <img class="rounded-circle bgc-white-darkest image" alt="" src="assets/img/picture/pic6.jpg">
                                    <div class="content pl-1">
                                        <span class="header">Dariel Wallace</span>
                                        <div class="description">Will give you a complete ...</div>
                                    </div>
                                </a>
                                <a href="chat.html" class="item mb-5 p-0">
                                    <span class="message-count"></span>
                                    <img class="rounded-circle bgc-white-darkest image" alt="" src="assets/img/picture/pic5.jpg">
                                    <div class="content pl-1">
                                        <span class="header">Jared Allison</span>
                                        <div class="description">Expound the actual te ...</div>
                                    </div>
                                </a>
                                <a href="chat.html" class="item mb-5 p-0">
                                    <span class="message-count"></span>
                                    <img class="rounded-circle bgc-white-darkest image" alt="" src="assets/img/picture/pic4.jpg">
                                    <div class="content pl-1">
                                        <span class="header">Jamie Jones</span>
                                        <div class="description">Praising pain was born ...</div>
                                    </div>
                                </a>
                                <a href="chat.html" class="item p-0">
                                    <span class="message-count"></span>
                                    <img class="rounded-circle bgc-white-darkest image" alt="" src="assets/img/picture/pic3.jpg">
                                    <div class="content pl-1">
                                        <span class="header">Hunter Atkins</span>
                                        <div class="description">The great explorer of th ...</div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane px-3 pb-5" id="side-messages" role="tabpanel">
                        <span class="sidebar-header">Latest Emails</span>
                        <div class="sidebar-email">
                            <div class="list">
                                <a href="mail-inbox.html" class="item mb-5 p-0">
                                    <div class="content">
                                        <div class="header"><span>Jamie Jones</span><span class="fs-7 pull-right">Sep 15</span>
                                        </div>
                                        <div class="subject"><span>Lorem ipsum.</span>
                                            <div class="icons">
                                                <i class="fa fa-star c-warning"></i>
                                            </div>
                                        </div>
                                        <div class="description">A alias aperiam corporis doloremque eligendi error, in laudantium minus molestiae ... </div>
                                    </div>
                                </a>
                                <a href="mail-inbox.html" class="item mb-5 p-0">
                                    <div class="content">
                                        <div class="header"><span>Jamie Jones</span><i class="fa icon-ml fa-paperclip"></i><span class="fs-7 pull-right">Sep 15</span></div>
                                        <div class="subject"><span>Lorem ipsum.</span>
                                            <div class="icons">
                                                <i class="fa fa-exclamation-circle c-danger"></i>
                                                <i class="fa fa-star c-warning"></i>
                                            </div>
                                        </div>
                                        <div class="description">Animi dolorum facere iure minus nam placeat quasi, sapiente temporibus ullam ut ... </div>
                                    </div>
                                </a>
                                <a href="mail-inbox.html" class="item mb-5 p-0">
                                    <div class="content">
                                        <div class="header"><span>Taylor Fletcher</span><span class="fs-7 pull-right">Sep 15</span></div>
                                        <div class="subject"><span>Lorem ipsum.</span>
                                            <div class="icons">
                                                <i class="fa fa-tags c-primary"></i>
                                            </div>
                                        </div>
                                        <div class="description">A ad, assumenda at culpa dolorem doloribus enim et expedita ipsa iste minima ... </div>
                                    </div>
                                </a>
                                <a href="mail-inbox.html" class="item mb-5 p-0">
                                    <div class="content">
                                        <div class="header"><span>Rhys Brooks</span><span class="fs-7 pull-right">Sep 15</span>
                                        </div>
                                        <div class="subject"><span>Lorem ipsum.</span>
                                            <div class="icons">
                                                <i class="fa fa-tags c-primary"></i>
                                                <i class="fa fa-exclamation-circle c-danger"></i>
                                            </div>
                                        </div>
                                        <div class="description">Accusantium debitis distinctio ipsam iure natus, possimus, ratione reiciendis similique soluta sunt ... </div>
                                    </div>
                                </a>
                                <a href="mail-inbox.html" class="item mb-5 p-0">
                                    <div class="content">
                                        <div class="header"><span>Jamie Jones</span><span class="fs-7 pull-right">Sep 15</span>
                                        </div>
                                        <div class="subject"><span>Lorem ipsum.</span>
                                            <div class="icons">
                                                <i class="fa fa-star c-warning"></i>
                                            </div>
                                        </div>
                                        <div class="description">Atque, aut beatae deserunt doloribus eius error est laborum libero natus omnis quibusdam ... </div>
                                    </div>
                                </a>
                                <a href="mail-inbox.html" class="item mb-5 p-0">
                                    <div class="content">
                                        <div class="header"><span>Brock Giles</span><span class="fs-7 pull-right">Sep 15</span>
                                        </div>
                                        <div class="subject"><span>Lorem ipsum.</span>
                                            <div class="icons">
                                            </div>
                                        </div>
                                        <div class="description">A ab consequuntur dignissimos doloribus et eum eveniet expedita, incidunt iste, iur ... </div>
                                    </div>
                                </a>
                                <a href="mail-inbox.html" class="item mb-5 p-0">
                                    <div class="content">
                                        <div class="header"><span>Dariel Wallace</span><span class="fs-7 pull-right">Sep 15</span></div>
                                        <div class="subject"><span>Lorem ipsum.</span>
                                            <div class="icons">
                                                <i class="fa fa-tags c-primary"></i>
                                                <i class="fa fa-exclamation-circle c-danger"></i>
                                                <i class="fa fa-star c-warning"></i>
                                            </div>
                                        </div>
                                        <div class="description">Consequatur error id laborum nihil. Aliquam animi, doloremque fugit non quasi quis ... </div>
                                    </div>
                                </a>
                                <a href="mail-inbox.html" class="item mb-5 p-0">
                                    <div class="content">
                                        <div class="header"><span>Jared Allison</span><span class="fs-7 pull-right">Sep 15</span></div>
                                        <div class="subject"><span>Lorem ipsum.</span>
                                            <div class="icons">
                                                <i class="fa fa-star c-warning"></i>
                                            </div>
                                        </div>
                                        <div class="description">Accusamus dolor, doloribus ducimus eum eveniet expedita facilis perspiciatis quaerat? </div>
                                    </div>
                                </a>
                                <a href="mail-inbox.html" class="item mb-5 p-0">
                                    <div class="content">
                                        <div class="header"><span>Jamie Jones</span><span class="fs-7 pull-right">Sep 15</span>
                                        </div>
                                        <div class="subject"><span>Lorem ipsum.</span>
                                            <div class="icons">
                                                <i class="fa fa-tags c-primary"></i>
                                                <i class="fa fa-exclamation-circle c-danger"></i>
                                            </div>
                                        </div>
                                        <div class="description">Accusantium ad blanditiis corporis deleniti ducimus, eaque earum eveniet hic illum, nisi ... </div>
                                    </div>
                                </a>
                                <a href="mail-inbox.html" class="item p-0">
                                    <div class="content">
                                        <div class="header"><span>Hunter Atkins</span><span class="fs-7 pull-right">Sep 15</span></div>
                                        <div class="subject"><span>Lorem ipsum.</span>
                                            <div class="icons">
                                                <i class="fa fa-star c-warning"></i>
                                            </div>
                                        </div>
                                        <div class="description">Animi architecto at consequuntur deleniti doloribus earum et facilis minima modi officia speriores ... </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane px-3 pb-5" id="side-alerts" role="tabpanel">
                        <span class="sidebar-header">Last Alerts</span>
                        <div class="sidebar-warning threaded">
                            <a href="project-task.html" class="tm-line-item">
                                <div class="tm-icon bgc-primary"><i class="fa fa-envelope"></i></div>
                                <div class="content">
                                    <div class="metadata">
                                        <span class="date">Today</span>
                                        <span class="hour">10:30 PM</span>
                                    </div>
                                    <p class="text"> Lorem Ipsum is simply dummy text of industry. </p>
                                </div>
                            </a>
                            <a href="project-task.html" class="tm-line-item">
                                <div class="tm-icon-empty bgc-primary"></div>
                                <div class="content">
                                    <div class="metadata">
                                        <span class="hour">11:33 AM</span>
                                    </div>
                                    <p class="text"> Consectetur adipiscing elit. </p>
                                </div>
                            </a>
                            <a href="project-task.html" class="tm-line-item">
                                <div class="tm-icon-empty bgc-primary"></div>
                                <div class="content">
                                    <div class="metadata">
                                        <span class="hour">13:20 PM</span>
                                    </div>
                                    <p class="text"> Sed ut perspiciatis unde omnis. </p>
                                </div>
                            </a>
                        </div>
                        <span class="sidebar-header">Code Errors</span>
                        <div class="sidebar-warning threaded">
                            <a href="project-task.html" class="tm-line-item">
                                <div class="tm-icon bgc-danger"><i class="fa fa-bug"></i></div>
                                <div class="content">
                                    <div class="metadata">
                                        <span class="date">Yesterday</span>
                                        <span class="hour">05:56 PM</span>
                                    </div>
                                    <p class="text"> Nemo enim ipsam voluptatem quia. </p>
                                </div>
                            </a>
                            <a href="project-task.html" class="tm-line-item">
                                <div class="tm-icon-empty bgc-danger"><i class="fa fa-bag"></i></div>
                                <div class="content">
                                    <div class="metadata">
                                        <span class="date">Today</span>
                                        <span class="hour">11:10 AM</span>
                                    </div>
                                    <p class="text"> Sed quia non numquam eius! </p>
                                </div>
                            </a>
                        </div>
                        <div class="sidebar-warning threaded mt-6">
                            <a href="project-task.html" class="tm-line-item">
                                <div class="tm-icon bgc-warning"><i class="fa fa-exclamation-circle"></i></div>
                                <div class="content">
                                    <div class="metadata">
                                        <span class="date">Sep 2, 2016</span>
                                        <span class="hour">13:20 PM</span>
                                    </div>
                                    <p class="text"> Sed ut perspiciatis unde accusantium doloremque </p>
                                </div>
                            </a>
                            <a href="project-task.html" class="tm-line-item">
                                <div class="tm-icon bgc-info"><i class="fa fa-life-bouy"></i></div>
                                <div class="content">
                                    <div class="metadata">
                                        <span class="date">Aug 25, 2016</span>
                                        <span class="hour">05:56 PM</span>
                                    </div>
                                    <p class="text"> Nemo enim ipsam quia aut odit aut fugit! </p>
                                </div>
                            </a>
                            <a href="project-task.html" class="tm-line-item">
                                <div class="tm-icon-empty bgc-info"></div>
                                <div class="content">
                                    <div class="metadata">
                                        <span class="date">Aug 25, 2016</span>
                                        <span class="hour">05:56 PM</span>
                                    </div>
                                    <p class="text"> Nemo enim ipsam quia aut odit aut fugit! </p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="tab-pane px-3 pb-5 c-body-lightest" id="side-settings" role="tabpanel">
                        <span class="sidebar-header">Grid Settings</span>
                        <div>
                            <a href="" class="w-100 mb-2 btn btn-outline-sidebar restore-grid"><i class="icon-mr menu-icon fa fa-refresh"></i><span>Restore Grid</span></a>
                            <a href="" class="w-100 mb-2 btn btn-outline-sidebar destroy-grid"><i class="icon-mr menu-icon fa fa-close"></i><span>Destroy Grid</span></a>
                            <a href="" class="w-100 mb-2 btn btn-outline-sidebar remove-data"><i class="icon-mr menu-icon icon-trash"></i><span>Remove Data</span></a>
                        </div>
                        <span class="sidebar-header">Input Settings</span>
                        <div class="form-group mb-4">
                            <label class="c-body-inverse-dark" for="sidebarInputSettings1">New Settings</label>
                            <input type="email" class="form-control" id="sidebarInputSettings1" placeholder="New Value">
                        </div>
                        <div class="form-group mb-4">
                            <label class="c-body-inverse-dark" for="sidebarInputSettings2">Another Settings</label>
                            <input type="email" class="form-control" id="sidebarInputSettings2" placeholder="Your email">
                        </div>
                        <span class="sidebar-header">Dropdown Settings</span>
                        <div class="ui fluid selection simple-select select-dropdown mb-4">
                            <input type="hidden" name="gender">
                            <i class="select-dropdown icon"></i>
                            <div class="default text">Profession</div>
                            <div class="menu">
                                <div class="item" data-value="2">Manager</div>
                                <div class="item" data-value="1">Designer</div>
                                <div class="item" data-value="0">Architecture</div>
                            </div>
                        </div>
                        <select class="ui fluid simple-select select-dropdown mb-4">
                            <option value="">Gender</option>
                            <option value="1">Male</option>
                            <option value="0">Female</option>
                        </select>
                        <span class="sidebar-header d-block">Checkboxes</span>
                        <div class="clerfix">
                            <div class="ui dynamic checkbox checkbox-fill mb-1">
                                <input type="checkbox" name="checkbox-color">
                                <label><i class="ion-ios-arrow-right icon-mr-ch"></i>Hotels</label>
                            </div>
                            <span class="pull-right">45</span>
                        </div>
                        <div class="clerfix">
                            <div class="ui dynamic checkbox checkbox-fill mb-1">
                                <input type="checkbox" name="checkbox-color">
                                <label><i class="ion-ios-arrow-right icon-mr-ch"></i>Hotels</label>
                            </div>
                            <span class="pull-right">45</span>
                        </div>
                        <div class="clerfix">
                            <div class="ui dynamic checkbox checkbox-fill mb-1">
                                <input type="checkbox" name="checkbox-color">
                                <label><i class="ion-ios-arrow-right icon-mr-ch"></i>Shopping</label>
                            </div>
                            <span class="pull-right">39</span>
                        </div>
                        <div class="clerfix">
                            <div class="ui dynamic checkbox checkbox-fill mb-1">
                                <input type="checkbox" name="checkbox-color">
                                <label><i class="ion-ios-arrow-right icon-mr-ch"></i>Restaurant</label>
                            </div>
                            <span class="pull-right">62</span>
                        </div>
                        <div class="clerfix">
                            <div class="ui dynamic checkbox checkbox-fill mb-1">
                                <input type="checkbox" name="checkbox-color">
                                <label><i class="ion-ios-arrow-right icon-mr-ch"></i>Beauty & Spa</label>
                            </div>
                            <span class="pull-right">105</span>
                        </div>
                        <span class="sidebar-header">Input Settings</span>
                        <div class="form-group mb-3">
                            <label class="c-body-inverse-dark" for="sidebarInputSettings3">New Settings</label>
                            <input type="email" class="form-control" id="sidebarInputSettings3" placeholder="New Value">
                        </div>
                        <span class="sidebar-header">Date of Sell</span>
                        <section class="mb-4">
                            <div id="year-slider"></div>
                            <div class="handles fs-6-plus mt-3 d-flex justify-content-center">
                                <div class="text-right pr-2 brw-1 bc-body">
                                    <span class="d-block fs-7">Min</span>
                                    <span class="d-block handle-0 fs-6 c-body-inverse"></span>
                                </div>
                                <div class="text-left pl-2">
                                    <span class="d-block fs-7">Max</span>
                                    <span class="d-block handle-1 fs-6 c-body-inverse"></span>
                                </div>
                            </div>
                        </section>
                        <span class="sidebar-header d-block">Radios</span>
                        <div class="clerfix">
                            <div class="ui dynamic checkbox radio checkbox-fill mb-1">
                                <input type="radio" name="checkbox-color">
                                <label><i class="ion-ios-arrow-right icon-mr-ch"></i>With Swimming Pool</label>
                            </div>
                            <span class="pull-right">45</span>
                        </div>
                        <div class="clerfix">
                            <div class="ui dynamic checkbox radio checkbox-fill mb-1">
                                <input type="radio" name="checkbox-color">
                                <label><i class="ion-ios-arrow-right icon-mr-ch"></i>With Garage</label>
                            </div>
                            <span class="pull-right">45</span>
                        </div>
                        <div class="clerfix">
                            <div class="ui dynamic checkbox radio checkbox-fill mb-1">
                                <input type="radio" name="checkbox-color">
                                <label><i class="ion-ios-arrow-right icon-mr-ch"></i>Tree Room & Lodge</label>
                            </div>
                            <span class="pull-right">39</span>
                        </div>
                        <div class="clerfix">
                            <div class="ui dynamic checkbox radio checkbox-fill mb-1">
                                <input type="radio" name="checkbox-color">
                                <label><i class="ion-ios-arrow-right icon-mr-ch"></i>With 2 Bathroom</label>
                            </div>
                            <span class="pull-right">62</span>
                        </div>
                        <div class="clerfix">
                            <div class="ui dynamic checkbox radio checkbox-fill mb-1">
                                <input type="radio" name="checkbox-color">
                                <label><i class="ion-ios-arrow-right icon-mr-ch"></i>With 2 Bedroom</label>
                            </div>
                            <span class="pull-right">105</span>
                        </div>
                        <span class="sidebar-header d-block">Sliders</span>
                        <section class="mb-4">
                            <h6 class="mb-2">Room Count</h6>
                            <div id="room-slider"></div>
                            <div class="handles fs-6-plus mt-3 d-flex justify-content-center">
                                <div class="text-right pr-2 brw-1 bc-body">
                                    <span class="d-block fs-7">Min</span>
                                    <span class="d-block handle-0 fs-6 c-body-inverse"></span>
                                </div>
                                <div class="text-left pl-2">
                                    <span class="d-block fs-7">Max</span>
                                    <span class="d-block handle-1 fs-6 c-body-inverse"></span>
                                </div>
                            </div>
                        </section>
                        <section class="mb-4">
                            <h6 class="mb-2">Construction Date</h6>
                            <div id="construction-slider"></div>
                            <div class="handles fs-6-plus mt-3 d-flex justify-content-center">
                                <div class="text-right pr-2 brw-1 bc-body">
                                    <span class="d-block fs-7">Min</span>
                                    <span class="d-block handle-0 fs-6 c-body-inverse"></span>
                                </div>
                                <div class="text-left pl-2">
                                    <span class="d-block fs-7">Max</span>
                                    <span class="d-block handle-1 fs-6 c-body-inverse"></span>
                                </div>
                            </div>
                        </section>
                    </div>

                </div>
            </div>
        </section>
        <!--End Sidebar-->

   
 
        <!--Begin aside-->
        <aside id="aside" class="aside-wrap" role="complementary">
            <div class="tab-content">
                <section class="tab-pane active" id="aside-contacts" role="tabpanel">
                    <div class="panel-wrap">
                        <div class="panel chat-profile-about bgc-white-dark">
                            <div class="panel-header bbw-1 btw-1 bc-gray-lighter clearfix bgc-white-dark panel-header-p panel-header-md">
                                <div class="panel-input pull-left">
                                    <input class="form-control search-control" type="text" placeholder="Search" data-list=".chat-members-list">
                                    <a href="" class="clear-style"><i class="fa fa-search"></i></a>
                                </div>
                                <div class="panel-icons">
                                    <ul>
                                        <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="row flex-column-reverse flex-sm-row">
                                    <div class="col-sm-8 brw-1 pr-0 bc-gray-lighter">
                                        <div class="chat-members contact-list pt-3 px-4 pb-4">
                                            <div class="list chat-members-list">
                                                <div class="item mb-5 p-0">
                                                    <a href="contacts.html">
                                                        <img class="rounded-circle bgc-white-darkest image" alt="" src="assets/img/picture/pic1.jpg">
                                                        <div class="content pl-3">
                                                            <span class="header">Jamie Jones</span>
                                                            <div class="description">Product Manager</div>
                                                        </div>
                                                    </a>
                                                    <div class="pull-right edit-btns">
                                                        <a href=""><i class="fa fa-edit"></i></a>
                                                        <a href=""><i class="fa fa-close"></i></a>
                                                    </div>
                                                </div>
                                                <div class="item mb-5 p-0">
                                                    <a href="contacts.html">
                                                        <img class="rounded-circle bgc-white-darkest image" alt="" src="assets/img/picture/pic2.jpg">
                                                        <div class="content pl-3">
                                                            <span class="header">Jamie Jones</span>
                                                            <div class="description">Developer</div>
                                                        </div>
                                                    </a>
                                                    <div class="pull-right edit-btns">
                                                        <a href=""><i class="fa fa-edit"></i></a>
                                                        <a href=""><i class="fa fa-close"></i></a>
                                                    </div>
                                                </div>
                                                <div class="item mb-5 p-0">
                                                    <a href="contacts.html">
                                                        <img class="rounded-circle bgc-white-darkest image" alt="" src="assets/img/picture/pic4.jpg">
                                                        <div class="content pl-3">
                                                            <span class="header">Taylor Fletcher</span>
                                                            <div class="description">Developer</div>
                                                        </div>
                                                    </a>
                                                    <div class="pull-right edit-btns">
                                                        <a href=""><i class="fa fa-edit"></i></a>
                                                        <a href=""><i class="fa fa-close"></i></a>
                                                    </div>
                                                </div>
                                                <div class="item mb-5 p-0">
                                                    <a href="contacts.html">
                                                        <img class="rounded-circle bgc-white-darkest image" alt="" src="assets/img/picture/pic3.jpg">
                                                        <div class="content pl-3">
                                                            <span class="header c-gray-dark font-weight-bold">Rhys Brooks</span>
                                                            <div class="description">Lead Developer</div>
                                                        </div>
                                                    </a>
                                                    <div class="pull-right edit-btns">
                                                        <a href=""><i class="fa fa-edit"></i></a>
                                                        <a href=""><i class="fa fa-close"></i></a>
                                                    </div>
                                                </div>
                                                <div class="item mb-5 p-0">
                                                    <a href="contacts.html">
                                                        <img class="rounded-circle bgc-white-darkest image" alt="" src="assets/img/picture/pic5.jpg">
                                                        <div class="content pl-3">
                                                            <span class="header">Jamie Jones</span>
                                                            <div class="description">QA Tester</div>
                                                        </div>
                                                    </a>
                                                    <div class="pull-right edit-btns">
                                                        <a href=""><i class="fa fa-edit"></i></a>
                                                        <a href=""><i class="fa fa-close"></i></a>
                                                    </div>
                                                </div>
                                                <div class="item mb-5 p-0">
                                                    <a href="contacts.html">
                                                        <img class="rounded-circle bgc-white-darkest image" alt="" src="assets/img/picture/pic6.jpg">
                                                        <div class="content pl-3">
                                                            <span class="header">Brock Giles</span>
                                                            <div class="description">Support Manager</div>
                                                        </div>
                                                    </a>
                                                    <div class="pull-right edit-btns">
                                                        <a href=""><i class="fa fa-edit"></i></a>
                                                        <a href=""><i class="fa fa-close"></i></a>
                                                    </div>
                                                </div>
                                                <div class="item mb-5 p-0">
                                                    <a href="contacts.html">
                                                        <img class="rounded-circle bgc-white-darkest image" alt="" src="assets/img/picture/pic1.jpg">
                                                        <div class="content pl-3">
                                                            <span class="header">Dariel Wallace</span>
                                                            <div class="description">Team Member</div>
                                                        </div>
                                                    </a>
                                                    <div class="pull-right edit-btns">
                                                        <a href=""><i class="fa fa-edit"></i></a>
                                                        <a href=""><i class="fa fa-close"></i></a>
                                                    </div>
                                                </div>
                                                <div class="item mb-5 p-0">
                                                    <a href="contacts.html">
                                                        <img class="rounded-circle bgc-white-darkest image" alt="" src="assets/img/picture/pic2.jpg">
                                                        <div class="content pl-3">
                                                            <span class="header">Jared Allison</span>
                                                            <div class="description">Team Member</div>
                                                        </div>
                                                    </a>
                                                    <div class="pull-right edit-btns">
                                                        <a href=""><i class="fa fa-edit"></i></a>
                                                        <a href=""><i class="fa fa-close"></i></a>
                                                    </div>
                                                </div>
                                                <div class="item mb-5 p-0">
                                                    <a href="contacts.html">
                                                        <img class="rounded-circle bgc-white-darkest image" alt="" src="assets/img/picture/pic3.jpg">
                                                        <div class="content pl-3">
                                                            <span class="header">Jamie Jones</span>
                                                            <div class="description">Team Member</div>
                                                        </div>
                                                    </a>
                                                    <div class="pull-right edit-btns">
                                                        <a href=""><i class="fa fa-edit"></i></a>
                                                        <a href=""><i class="fa fa-close"></i></a>
                                                    </div>
                                                </div>
                                                <div class="item p-0">
                                                    <a href="contacts.html">
                                                        <img class="rounded-circle bgc-white-darkest image" alt="" src="assets/img/picture/pic4.jpg">
                                                        <div class="content pl-3">
                                                            <span class="header">Hunter Atkins</span>
                                                            <div class="description">Team Member</div>
                                                        </div>
                                                    </a>
                                                    <div class="pull-right edit-btns">
                                                        <a href=""><i class="fa fa-edit"></i></a>
                                                        <a href=""><i class="fa fa-close"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 pr-4 pl-4 pl-sm-2 py-3">
                                        <ul class="list list-unstyled contact-categories">
                                            <li><a href="" class="btn">All Contacts</a></li>
                                            <li><a href="" class="btn">Team Members</a></li>
                                            <li><a href="" class="btn">Project Members</a></li>
                                            <li><a href="" class="btn">Friends</a></li>
                                            <li><a href="" class="btn">Clients</a></li>
                                            <li><a href="" class="btn">Support Team</a></li>
                                            <li><a href="" class="btn">Customers</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="tab-pane" id="aside-uploads" role="tabpanel">
                    <div class="panel-wrap">
                        <div class="panel chat-profile-about bgc-white-dark">
                            <div class="panel-header bbw-1 btw-1 bc-gray-lighter clearfix bgc-white-dark panel-header-p panel-header-md">
                                <h2 class="pull-left"> Uploads </h2>
                                <div class="panel-icons">
                                    <ul>
                                        <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="panel-body panel-body-p">
                                <div class="pb-5">
                                    <div class="uploads mb-5">
                                        <div class="row mb-1">
                                            <div class="col-sm-12">
                                                <div class="drag-drop mb-3">
                                                    <h3>Drag & Drop Files Here</h3>
                                                    <h6>or cick to select files</h6>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="files">
                                                    <ul class="list list-unstyled upload-list mb-0 upload-template dropzone-previews">
                                                        <li class="clearfix">
                                                            <div class="d-flex">
                                                                <div class="upload-name">
                                                                    <img src="assets/img/empty.png" class="mr-1" data-dz-thumbnail width="50" height="50" alt="" />
                                                                    <div class="d-inline-block upload-details">
                                                                        <span class="name mr-3" data-dz-name></span>
                                                                    </div>
                                                                </div>
                                                                <div>
                                                                    <span class="size mr-4" data-dz-size></span>
                                                                </div>
                                                                <div>
                                                                    <a class="progress-circle start pull-right ml-1" href="">
                                                                        <i class="ion-ios-play-outline fs-4" data-success-icon="fa fa-check c-success" data-error-icon="fa fa-trash c-danger"></i>
                                                                    </a>
                                                                    <a href="" title="Click me to remove the file." data-dz-remove><span class="remove-btn btn-icon bw-1 br-circle bc-gray-lighter dz-error-mark btn-default c-danger">✘</span></a>
                                                                </div>
                                                            </div>
                                                            <span class="error text-danger" data-dz-errormessage></span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="text-center">
                                            <a href="" class="btn mr-1 btn-default mt-1 file-input-button">
                                                <span><i class="fa fa-file-o icon-mr c-primary"></i>New</span>
                                            </a>
                                            <a href="" class="btn mr-1 btn-default mt-1 file-start-button">
                                                <span><i class="icon-cloud-upload icon-mr c-success"></i>Upload</span>
                                            </a>
                                            <a href="" data-dz-remove class="btn btn-default mt-1 file-delete-button">
                                                <span><i class="icon-trash icon-mr c-danger"></i>Delete All</span>
                                            </a>
                                        </div>
                                    </div>
                                    <h6 class="p-1 bbw-1 bc-gray-lighter"> Activity Today </h6>
                                    <div class="mt-3 aside-activity threaded">
                                        <a href="" class="tm-line-item">
                                            <div class="tm-icon bgc-primary"><i class="fa fa-envelope"></i></div>
                                            <div class="content">
                                                <div class="metadata">
                                                    <span class="date">Today</span>
                                                    <span class="hour">10:30 PM</span>
                                                </div>
                                                <div class="text">
                                                    <p>Lorem Ipsum is simply dummy text of industry.</p>
                                                    <div class="aside-files">
                                                        <div class="file file-sm mb-2">
                                                            <div class="image bgc-white-darkest">
                                                                <i class="fa fa-file-pdf-o"></i>
                                                            </div>
                                                            <div class="name">
                                                                <span class="d-block c-gray-dark">Ipsum.pdf</span>
                                                                <span>20/9KB Uploaded from a phone</span>
                                                            </div>
                                                        </div>
                                                        <div class="file file-sm">
                                                            <div class="image bgc-white-darkest">
                                                                <i class="fa fa-file-word-o"></i>
                                                            </div>
                                                            <div class="name">
                                                                <span class="d-block c-gray-dark">Ip Ipsum.word</span>
                                                                <span>Lorem ipsum dolor sit amet.</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                        <a href="" class="tm-line-item">
                                            <div class="tm-icon-empty bgc-primary"></div>
                                            <div class="content">
                                                <div class="metadata">
                                                    <span class="hour">11:33 AM</span>
                                                </div>
                                                <p class="text"> Consectetur adipiscing elit. </p>
                                            </div>
                                        </a>
                                        <a href="" class="tm-line-item">
                                            <div class="tm-icon-empty bgc-primary"></div>
                                            <div class="content">
                                                <div class="metadata">
                                                    <span class="hour">13:20 PM</span>
                                                </div>
                                                <p class="text"> Consectetur adipisicing elit. Autem doloremque ex ipsum nobis non, nulla.. </p>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="chat-room">
                                        <div class="chat-reply clearfix">
                                            <div class="pull-right textarea-group">
                                                <div class="form-control chat-reply-area"></div>
                                                <div class="chat-textarea-addon">
                                                    <a href="" class="pull-right btn-icon rounded-circle btn-info"><i class="fa fa-paper-plane"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mt-5 aside-activity threaded">
                                        <a href="" class="tm-line-item">
                                            <div class="tm-icon bgc-warning"><img class="br-circle" src="assets/img/picture/pic3.jpg" alt=""></div>
                                            <div class="content">
                                                <div class="metadata">
                                                    <span class="date">Today</span>
                                                    <span class="hour">10:30 PM</span>
                                                </div>
                                                <div class="text">
                                                    <p>Accusamus beatae dignissimos fuga iure numquam!</p>
                                                    <div class="aside-files">
                                                        <div class="file file-sm mb-2">
                                                            <div class="image bgc-white-darkest">
                                                                <i class="fa fa-file-pdf-o"></i>
                                                            </div>
                                                            <div class="name">
                                                                <span class="d-block c-gray-dark">Ipsum.png</span>
                                                                <span>20/9KB Uploaded from my phone</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="chat-room">
                                        <div class="chat-reply clearfix">
                                            <div class="pull-right textarea-group">
                                                <div class="form-control chat-reply-area"></div>
                                                <div class="chat-textarea-addon">
                                                    <a href="" class="pull-right btn-icon rounded-circle btn-info"><i class="fa fa-paper-plane"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="tab-pane" id="aside-settings" role="tabpanel">
                    <div class="panel-wrap">
                        <div class="panel chat-profile-about bgc-white-dark">
                            <div class="panel-header bbw-1 btw-1 bc-gray-lighter clearfix bgc-white-dark panel-header-p panel-header-md">
                                <h2 class="pull-left"> Theme Colors & Layouts </h2>
                                <div class="panel-icons">
                                    <ul>
                                        <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="panel-body pb-4 panel-body-p px-3">
                                <h5 class="mb-1">App Classes</h5>
                                <p class="fs-7 mb-1">Add these classes to the outer <code>#app</code> element if you don't need theme customizer</p>
                                <div class="alert alert-warning fs-7 mb-2" role="alert" id="aside-settings-alert">
                                    <strong></strong> Better check yourself, you're not looking too good. </div>
                                <p class="fs-7 mb-3">Add <code>.no-saved-theme</code> class to `#app` element if you want the page is loaded with the specified classes (not with saved theme data - see <a href="page-starter.html">Starter Pages</a>)</p>
                                <div class="mb-3">
                                    <h5 class="mb-3"> Header </h5>
                                    <div class="site-themes">
                                        <div class="site-theme backgrounds" data-theme="header-bg-default"></div>
                                        <div class="site-theme backgrounds" data-theme="header-bg-primary"></div>
                                        <div class="site-theme backgrounds" data-theme="header-bg-info"></div>
                                        <div class="site-theme backgrounds" data-theme="header-bg-danger"></div>
                                        <div class="site-theme backgrounds" data-theme="header-bg-warning"></div>
                                    </div>
                                </div>
                                <div class="">
                                    <h5 class="mb-3"> Sidebar </h5>
                                    <div class="mb-4 hidden-sm-down">
                                        <h6 class="mb-3">Type</h6>
                                        <div class="site-themes">
                                            <div class="site-theme types" data-theme="sidebar-type-push"><span>Push</span></div>
                                            <div class="site-theme types" data-theme="sidebar-type-slide"><span>Slide</span></div>
                                            <div class="site-theme types d-none" data-theme="sidebar-type-top"><span>Top</span></div>
                                            <div class="site-theme types d-none" data-theme="sidebar-type-bottom"><span>Bottom</span></div>
                                        </div>
                                    </div>
                                    <div class="row mb-4 hidden-sm-down">
                                        <div class="col-sm-6 col-12">
                                            <h6 class="mb-3">Default States</h6>
                                            <div class="site-themes">
                                                <div class="site-theme types" data-theme="sidebar-state-open"><span>Open</span></div>
                                                <div class="site-theme types" data-theme="sidebar-state-compact"><span>Compact</span></div>
                                                <div class="site-theme types" data-theme="sidebar-state-close"><span>Close</span></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-12">
                                            <h6 class="mb-3">Triggered States</h6>
                                            <div class="site-themes">
                                                <div class="site-theme types" data-theme="sidebar-tr-state-open"><span>Open</span></div>
                                                <div class="site-theme types" data-theme="sidebar-tr-state-compact"><span>Compact</span></div>
                                                <div class="site-theme types" data-theme="sidebar-tr-state-close"><span>Close</span></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3 site-themes">
                                        <h6 class="mb-3">Background</h6>
                                        <div class="site-theme backgrounds" data-theme="sidebar-bg-default"></div>
                                        <div class="site-theme backgrounds image-backgrounds" data-theme="sidebar-bg-one">
                                            <span class="image-lazy" data-src="assets/img/profile-bg.jpg" data-alt=""></span>
                                            <div class="gradient"></div>
                                        </div>
                                    </div>
                                    <div class="mb-4">
                                        <h6 class="mb-3">Options</h6>
                                        <div class="row site-themes">
                                            <div class="col-sm-4 col-6">
                                                <div class="site-theme options" data-theme="sidebar-option-default">
                                                    <span data-src="assets/img/pages/page-starter-default.png" data-alt="" class="image-lazy"></span>
                                                </div>
                                            </div>
                                            <div class="col-sm-4 col-6">
                                                <div class="site-theme options" data-theme="sidebar-option-theme1">
                                                    <span data-src="assets/img/pages/page-starter-option-1.png" data-alt="" class="image-lazy"></span>
                                                </div>
                                            </div>
                                            <div class="col-sm-4 col-6">
                                                <div class="site-theme options" data-theme="sidebar-option-theme2">
                                                    <span data-src="assets/img/pages/page-starter-option-2.png" data-alt="" class="image-lazy"></span>
                                                </div>
                                            </div>
                                            <div class="col-sm-4 col-6">
                                                <div class="site-theme options" data-theme="sidebar-option-theme3">
                                                    <span data-src="assets/img/pages/page-starter-option-3.png" data-alt="" class="image-lazy"></span>
                                                </div>
                                            </div>
                                            <div class="col-sm-4 col-6">
                                                <div class="site-theme options" data-theme="sidebar-option-theme4">
                                                    <span data-src="assets/img/pages/page-starter-option-4.png" data-alt="" class="image-lazy"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </aside>

            @if(isset($right_bar))
			
		<aside class="right-sidebar" id="rightSidebar">
			<button class="sidebat-toggle" id="sidebarToggle" href='javascript:'><i class="mdi mdi-menu"></i></button>
			<div class="panel panel-right-sidebar">
				<?php $data = '';
			if(isset($right_bar_data))
				$data = $right_bar_data;
			?>
				@include($right_bar_path, array('data' => $data))
			</div>
		</aside>
	 
	@endif

        
        @yield('content')
        <!-- <footer id="footer" class="footer-wrap" role="contentinfo" style="color: white;font-weight: bold;">
            <span class="fs-7">
                <i class="icon-layers fs-4 mr-2"></i> Powerd by<span class="hidden-xs-down">- Amazingwits</span>
                <span class="ml-1">&copy; 2019 - 2020</span>
            </span>
            
        </footer> -->
	</div>

    
		

	<!-- </div> -->

	<!-- /#wrapper -->

		<!-- inject-body-end:js -->

  <script src="{{JS}}jquery-1.12.1.min.js"></script>
     <script src="{{JS}}main.js"></script>
     <script src="{{JS}}bootstrap.min.js"></script>
	<script src="{{PREFIX1}}assets/js/shim.js"></script>
    <script src="{{PREFIX1}}assets/js/settings.js"></script>
    @include('jspm.jspm')
    <script src="{{JS}}sweetalert-dev.js"></script>

	<!-- <script src="{{PREFIX1}}assets/js/jspm.config.js"></script> -->
	<!-- endinject -->

	<script>
		SystemJS.import('');
	</script>
   <!--   <script>
        SystemJS.import('scripts/table');
    </script> -->
     <script>
        SystemJS.import('scripts/chat');
    </script>

 <script>
        SystemJS.import('scripts/tabs');
    </script>
    <script>
        SystemJS.import('scripts/chart');
    </script>

    	@if(isset($module_helper))	
	<script src="{{JS}}bootstrap-tour.min.js"></script>
	@include('common.module-helper', array('module_helper'=>$module_helper))
	@endif



	 @yield('footer_scripts')

	@include('errors.formMessages')

 	@yield('custom_div_end')
	{!!getSetting('google_analytics', 'seo_settings')!!}

    
</body>


</html>