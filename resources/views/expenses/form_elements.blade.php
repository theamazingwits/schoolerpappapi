
					<div class="row">

 					 <fieldset class="form-group col-md-6">

						{{ Form::label('title', getphrase('title')) }}

						<span class="text-red">*</span>

						{{ Form::text('title', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('title'),

							'ng-model'=>'title', 

							'required'=> 'true', 

							'ng-class'=>'{"has-error": formExpenses.title.$touched && formExpenses.title.$invalid}',

							'ng-maxlength' => '512',

							)) }}

						<div class="validation-error" ng-messages="formExpenses.title.$error" >

	    					{!! getValidationMessage()!!}

	    					{!! getValidationMessage('maxlength')!!}

						</div>

					</fieldset>	

 					 <fieldset class="form-group  col-md-6">
						{{ Form::label('expense_category_id', getphrase('expense_category')) }}
						<span class="text-red">*</span>
						{{Form::select('expense_category_id', $expense_categories, null, ['class'=>'form-control', 'id'=>'expense_category_id', 'placeholder'=>getPhrase('select'),
							'ng-model'=>'expense_category_id',
							'required'=> 'true', 
							'ng-class'=>'{"has-error": formExpenses.expense_category_id.$touched && formExpenses.expense_category_id.$invalid}'
						])}}

						 <div class="validation-error" ng-messages="formExpenses.expense_category_id.$error" >
	    					{!! getValidationMessage()!!}
						</div>
					</fieldset>

				 </div>


							

 			 <div class="row">

			 	
 			 	<fieldset class="form-group col-md-6">
							

					{{ Form::label('expense_amount', getphrase('expense_amount')) }}

					<span class="text-red">*</span>

					{{ Form::number('expense_amount', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('enter_value'),

					'ng-model'=>'expense_amount', 
                    'min'=>'0',
	                 'string-to-number'=>'expense_amount',
					'required'=> 'true', 

					'ng-class'=>'{"has-error": formExpenses.expense_amount.$touched && formExpenses.expense_amount.$invalid}',
					 

					)) }}

				<div class="validation-error" ng-messages="formExpenses.expense_amount.$error" >

					{!! getValidationMessage()!!}

					{!! getValidationMessage('number')!!}

				</div>

				</fieldset>


			 	<?php 

				 	$expense_date = date('Y/m/d');

				 	if($record)

				 	{

				 		$expense_date = $record->expense_date;

				 	}

				 ?>

  				<fieldset class="form-group col-md-6">
                                    

                        {{ Form::label('expense_date', getphrase('expense_date')) }}
                     
                        {{ Form::text('expense_date', $value = $expense_date , $attributes = array('class'=>'input-sm form-control dp')) }}
                      

                </fieldset>


				</div>



				<div class="buttons text-center">

					<button class="btn btn-lg btn-primary button"

					ng-disabled='!formExpenses.$valid'>{{ $button_name }}</button>

				</div>

		 