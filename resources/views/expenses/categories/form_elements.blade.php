
					<div class="row">

 					 <fieldset class="form-group col-md-6">

						{{ Form::label('category_name', getphrase('category_name')) }}

						<span class="text-red">*</span>

						{{ Form::text('category_name', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('category_name'),

							'ng-model'=>'category_name', 

							'required'=> 'true', 

							'ng-class'=>'{"has-error": formExpenseCategories.category_name.$touched && formExpenseCategories.category_name.$invalid}',

							'ng-maxlength' => '512',

							)) }}

						<div class="validation-error" ng-messages="formExpenseCategories.category_name.$error" >

	    					{!! getValidationMessage()!!}

	    					{!! getValidationMessage('maxlength')!!}

						</div>

					</fieldset>	


 					 <fieldset class="form-group col-md-6">

						{{ Form::label('code', getphrase('code')) }}

						<span class="text-red">*</span>

						{{ Form::text('code', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('code'),

							'ng-model'=>'code', 

							'required'=> 'true', 

							'ng-class'=>'{"has-error": formExpenseCategories.code.$touched && formExpenseCategories.code.$invalid}',

							'ng-maxlength' => '20',

							)) }}

						<div class="validation-error" ng-messages="formExpenseCategories.code.$error" >

	    					{!! getValidationMessage()!!}

	    					{!! getValidationMessage('maxlength')!!}

						</div>

					</fieldset>				

					<fieldset class="form-group col-md-12">
						

						{{ Form::label('description', getphrase('description')) }}

						<span class="text-red">*</span>

						{{ Form::textarea('description', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('description'), 'rows' => '5',

							'ng-model'=>'description', 

							'required'=> 'true', 

							'ng-class'=>'{"has-error": formExpenseCategories.description.$touched && formExpenseCategories.description.$invalid}',

							'ng-maxlength' => '1000',

							)) }}

						<div class="validation-error" ng-messages="formExpenseCategories.description.$error" >

	    					{!! getValidationMessage()!!}

	    					{!! getValidationMessage('maxlength')!!}

						</div>

					</fieldset>

				 </div>



				<div class="buttons text-center">

					<button class="btn btn-lg btn-primary button"

					ng-disabled='!formExpenseCategories.$valid'>{{ $button_name }}</button>

				</div>

		 