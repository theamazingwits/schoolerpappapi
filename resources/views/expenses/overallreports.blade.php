@extends($layout)

@section('header_scripts')
<link href="{{CSS}}bootstrap-datepicker.css" rel="stylesheet">
@stop


@section('content')
<div id="page-wrapper" ng-controller="overAllReports">
  <section id="main" class="main-wrap bgc-white-darkest" role="main">
    <div class="container-fluid content-wrap">
      <div class="row">
        <div class="col-lg-12">
          <ol class="breadcrumb">
            <li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>
            <li>{{ $title }}</li>
          </ol>
        </div>
      </div>

      <section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item " >
        <!--Start Panel-->
        <div class="panel bgc-white-dark">
            <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                <h2 class="pull-left"> {{ $title }} </h2>             
                <!--End panel icons-->
            </div>
            <div class="panel-body panel-body-p packages">

              {!! Form::open(array('url' => URL_OVERALL_REPORTS, 'method' => 'POST', 'name'=>'htmlform ', 'id'=>'htmlform', 'novalidate'=>'')) !!}





              <div class="row input-daterange" id="dp">

                <?php 

                $date_from = (!empty($date_from)) ? $date_from : date('Y/m/d');

                $date_to = (!empty($date_to)) ? $date_to : date('Y/m/d');


                ?>

                <fieldset class="form-group col-md-4">


                  {{ Form::label('date_from', getphrase('date_from')) }}

                  {{ Form::text('date_from', $value = $date_from , 
                  [
                  'class'=>'input-sm form-control',
                  'placeholder' => '2015/7/17',
                  'ng-model'=>'date_from'

                  ])}}



                </fieldset>


                <fieldset class="form-group col-md-4">


                  {{ Form::label('date_to', getphrase('date_to')) }}


                  {{ Form::text('date_to', $value = $date_to , 
                  [
                  'class'=>'input-sm form-control', 
                  'placeholder' => '2015/7/17',
                  'ng-model'=>'date_to'
                  ]) }}

                </fieldset>

                <fieldset class="form-group col-md-4">
                  <button type="submit" class="btn btn-primary" style="margin-top:30px;">{{ getPhrase('submit') }}</button>
                </fieldset>
              </div>
              <br>

              {!! Form::close() !!}


              <div class="row vertical-scroll">


                <?php $currency  = getSetting('currency_symbol','site_settings');

                echo '<div class="col-md-4">
                <div class="card card-blue text-xs-center helper_step1">
                <div class="card-block">
                <h4 class="card-title">
                '.$currency.number_format($overall_payments,2).'
                </h4>
                <span class="card-text">'.getPhrase('overall_payments').'</span>
                </div>
                </div>
                </div>';

                echo '<div class="col-md-4">
                <div class="card card-red text-xs-center helper_step1">
                <div class="card-block">
                <h4 class="card-title">
                '.$currency.number_format($overall_expenses,2).'
                </h4>
                <span class="card-text">'.getPhrase('overall_expenses').'</span>
                </div>
                </div>
                </div>';

                echo '<div class="col-md-4">
                <div class="card card-green text-xs-center helper_step1">
                <div class="card-block">
                <h4 class="card-title">
                '.$currency.$profit.'
                </h4>
                <span class="card-text">'.getPhrase('profit').'</span>
                </div>
                </div>
                </div>';
                ?>

              </div>
              {!! Charts::assets() !!}
              <h4 class="cs-heading-small">Overall summary</h4>
              <div class="col-md-12">
                <?php
                $cc =Charts::create('bar', 'highcharts')
                ->title('Payments and Expenses')
                ->elementLabel('Total ('.$currency.')')
                ->labels(['Overall Payments', 'Overall Expenses'])
                ->values([$overall_payments, $overall_expenses])

                ->responsive(true);
                ?>
                {!! $cc->render() !!}  
              </div>

            </div>
          </div>
        </section>


    </div>
  </section>
</div>


 

@stop
 
 

@section('footer_scripts')

<script src="{{JS}}datepicker.min.js"></script>
 <script src="{{JS}}bootstrap-toggle.min.js"></script>   
 <script>
      $('.input-daterange').datepicker({
        autoclose: true,
         format: '{{getDateFormat()}}',
    });
 </script>
    
@stop