 
@extends('layouts.site')
@section('header_scripts')
<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
<script>
  var OneSignal = window.OneSignal || [];
  OneSignal.push(function() {
    OneSignal.init({
      appId: "db1e0bb3-0974-41d4-b542-424642a3776f",
      notifyButton: {
        enable: true,
      },
      allowLocalhostAsSecureOrigin: true,
    });
     
    OneSignal.getUserId(function(userId) {
    console.log("OneSignal User ID:", userId);
    $('#push_web_id').val(userId);
    // (Output) OneSignal User ID: 270a35cd-4dda-4b3f-b04e-41d7463a2316    
  });
	OneSignal.on('notificationDisplay', function(event) {
    console.warn('OneSignal notification displayed:', event);
  });
  });


</script>
@stop
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{LOGIN_DES}}login_vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{LOGIN_DES}}fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{LOGIN_DES}}fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{LOGIN_DES}}login_vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="{{LOGIN_DES}}login_vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{LOGIN_DES}}login_vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{LOGIN_DES}}login_vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="{{LOGIN_DES}}login_vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{LOGIN_DES}}login_css/util.css">
	<link rel="stylesheet" type="text/css" href="{{LOGIN_DES}}login_css/main.css">
<!--===============================================================================================-->
<style type="text/css">
	
	.login-user-details {
    display: none;
}
@media(min-width:768px) {
    .login-user-details {
        position: absolute;
        right: 200px;
        min-width: 180px;
        top: 40px;
        background-color: #d9edf7;
        padding: 15px 15px;
        display: block;
    }
    .login-user-details li {
        text-align: center;
        font-size: 18px;
    }
    .login-user-details li.title {
        color: #44a1ef;
        margin-bottom: 10px;
        font-weight: bold;
    }
    .login-user-details li + li {
        margin-top: 5px;
    }
    .login-user-details li a {
        padding: 5px 10px;
        display: block;
        text-decoration: none;
        border-radius: 3px;
        cursor: pointer;
    }
    .login-user-details li a.positive {
        border: 1px solid #44a1ef;
        background: #44a1ef;
        color: #fff;
    }
    .login-user-details li a:hover {
        color: #44a1ef;
        background: #FFF;
        border-color: #44a1ef;
    }

}


</style>
@section('content')

	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<!-- <form class="login100-form validate-form"> -->
					<span class="login100-form-title p-b-26">
						<img src="{{IMAGE_PATH_SETTINGS.getSetting('site_logo', 'site_settings')}}" alt="" style="height :auto !important;width:8em !important;">
						<p>{{getSetting('login_page_title','site_settings')}}</p>
					</span>
<!-- 
					@include('layouts.site-navigation')
 -->
				{!! Form::open(array('url' => URL_USERS_LOGIN, 'method' => 'POST', 'name'=>'formLanguage ', 'novalidate'=>'', 'class'=>"loginform", 'name'=>"loginForm")) !!}



				@include('errors.errors')	
				{{ Form::hidden('auth_id', $value = "12345" , $attributes = array('class'=>'form-control',

					'ng-model'=>'auth_id',
					'id'=>'auth_id',

					'placeholder' => getPhrase('username').'/'.getPhrase('email'),

					'ng-class'=>'{"has-error": loginForm.auth_id.$touched && loginForm.auth_id.$invalid}',

				)) }}
				{{ Form::hidden('push_web_id', $value = "12345" , $attributes = array('class'=>'form-control',

					'ng-model'=>'push_web_id',
					'id'=>'push_web_id',

					'placeholder' => getPhrase('username').'/'.getPhrase('email'),

					'ng-class'=>'{"has-error": loginForm.push_web_id.$touched && loginForm.push_web_id.$invalid}',

				)) }}

				<div class="wrap-input100 validate-input" data-validate = "Valid email is: a@b.c">

	    		{{ Form::text('email', $value = null , $attributes = array('class'=>'input100',

						'ng-model'=>'email',

						'required'=> 'true',

						'id'=>'email',

						'placeholder' => getPhrase('username').'/'.getPhrase('email'),

						'ng-class'=>'{"has-error": loginForm.email.$touched && loginForm.email.$invalid}',

					)) }}
					<span class="focus-input100" data-placeholder=""></span>

				<!-- <div class="validation-error" ng-messages="loginForm.email.$error" >

					{!! getValidationMessage()!!}

					{!! getValidationMessage('email')!!}

				</div>
 -->

					</div>

					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<!-- <span class="btn-show-pass">
							<i class="zmdi zmdi-eye"></i>
						</span>
 -->
						{{ Form::password('password', $attributes = array('class'=>'input100',

							'placeholder' => getPhrase("password"),

							'ng-model'=>'registration.password',

							'required'=> 'true', 

							'ng-class'=>'{"has-error": loginForm.password.$touched && loginForm.password.$invalid}',

							'ng-minlength' => 5,
							'id'=>'password'

						)) }}
						<span class="focus-input100" data-placeholder=""></span>
					<!-- <div class="validation-error" ng-messages="loginForm.password.$error" >

						{!! getValidationMessage()!!}

						{!! getValidationMessage('password')!!}

					</div> -->


					</div>

					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button  class="login100-form-btn" id="submit_button" ng-disabled='!loginForm.$valid'>{{getPhrase('login')}}
							</button>
						</div>
					</div>
				{!! Form::close() !!}
					<div class="text-center">
						<a href="javascript:void(0);" class="pull-left" data-toggle="modal" data-target="#myModal" ><i class="icon icon-question"></i> {{getPhrase('forgot_password')}}</a>
					</div>
				<!-- </form> -->
			</div>
		</div>
	</div>





	<!-- Modal -->

<div id="myModal" class="modal fade" role="dialog">

  <div class="modal-dialog">

	{!! Form::open(array('url' => URL_FORGOT_PASSWORD, 'method' => 'POST', 'name'=>'formLanguage ', 'novalidate'=>'', 'class'=>"loginform", 'name'=>"passwordForm")) !!}

    <!-- Modal content-->

    <div class="modal-content">

      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal">&times;</button>

        <h4 class="modal-title">{{getPhrase('forgot_password')}}</h4>

      </div>

      <div class="modal-body">

        <div class="input-group">

				<span class="input-group-addon" id="basic-addon1"><i class="icon icon-email-resend"></i></span>



	    		{{ Form::email('email', $value = null , $attributes = array('class'=>'form-control',

			'ng-model'=>'email',

			'required'=> 'true',

			'placeholder' => getPhrase('email'),

			'ng-class'=>'{"has-error": passwordForm.email.$touched && passwordForm.email.$invalid}',

		)) }}

	<div class="validation-error" ng-messages="passwordForm.email.$error" >

		{!! getValidationMessage()!!}

		{!! getValidationMessage('email')!!}

	</div>



			</div>

      </div>

      <div class="modal-footer">

      <div class="pull-right">

        <button type="button" class="btn btn-default" data-dismiss="modal">{{getPhrase('close')}}</button>

        <button type="submit" class="btn btn-primary" ng-disabled='!passwordForm.$valid'>{{getPhrase('submit')}}</button>

        </div>

      </div>

    </div>

	{!! Form::close() !!}

  </div>

</div>

@stop


<!--===============================================================================================-->
	<script src="{{LOGIN_DES}}login_vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="{{LOGIN_DES}}login_vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="{{LOGIN_DES}}login_vendor/bootstrap/js/popper.js"></script>
	<script src="{{LOGIN_DES}}login_vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="{{LOGIN_DES}}login_vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="{{LOGIN_DES}}login_vendor/daterangepicker/moment.min.js"></script>
	<script src="{{LOGIN_DES}}login_vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="{{LOGIN_DES}}login_vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="{{LOGIN_DES}}login_js/main.js"></script>

@section('footer_scripts')
<script>
	var appname;
  $(document).ready(function(){
        appname = '<?php echo $app_name; ?>';
        var data = {
        "app_name" : appname
      }
      $.ajax({
              url: "{{SASS_URL}}/getAuthentication",
              headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
              },
              type: "POST",
              data: data,
              success: function(success) {
                //$('#members').html(JSON.stringify(success));
                //console.clear();
                var data = success.auth;
                $('#auth_id').val(data);
              },
              dataType: "json",
              timeout: 2000
        })    
    });
function setCredentials(userType) {
	username = 'alivia@alivia.com';
	password = 'password';
	if(userType=='owner') {
		username = 'owner@owner.com';
		password = 'password';
	}
	else if(userType=='admin') {
		username = 'airship@att.net';
		password = 'password';
	}
	else if(userType=='parent') {
		username = 'dcoppit@comcast.net';
		password = 'password';
	}
	else if(userType=='staff') {
		username = 'purvis@verizon.net';
		password = 'password';
	}
	else if(userType=='student') {
		username = 'alivia@alivia.com';
		password = 'Password';
	}

	else if(userType=='librarian') {
		username = 'presoff@outlook.com';
		password = 'password';
	}
	else if(userType=='asstlibrarian') {
		username = 'cliffski@outlook.com';
		password = 'password';
	}

	else if(userType=='clerk') {
		username = 'haddawy@mac.com';
		password = 'password';
	}

	else if(userType=='hr') {
		username = 'jfriedl@att.net';
		password = 'password';
	}

	else if(userType=='receptionist') {
		username = 'agapow@icloud.com';
		password = 'password';
	}

	else if(userType=='transport_manager') {
		username = 'tskirvin@aol.com';
		password = 'password';
	}

	else if(userType=='hostel_manager') {
		username = 'alastair@att.net';
		password = 'password';
	}

	else if(userType=='coordinator') {
		username = 'coordinator@coordinator.com';
		password = 'password';
	}

	$('#email').val(username);
	$('#password').val(password);
	$('#submit_button').prop("disabled", false);
}
</script>
<!-- <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script> -->
<!-- <script>
  var OneSignal = window.OneSignal || [];
  OneSignal.push(function() {
    OneSignal.init({
      appId: "db1e0bb3-0974-41d4-b542-424642a3776f",
    });
  });
</script> -->

	@include('common.validations')

@stop


