<script src="{{CHAT}}/moment.js" type="text/javascript"></script>
<script src="{{CHAT}}/popper.js/dist/umd/popper.js" type="text/javascript"></script>
<!-- <script src="{{CHAT}}/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script> -->
<script src="{{CHAT}}/js-cookie/src/js.cookie.js" type="text/javascript"></script>
<script src="{{CHAT}}/perfect-scrollbar/dist/perfect-scrollbar.js" type="text/javascript"></script>
<!--end:: Vendor Plugins -->
<script src="{{CHAT}}/scripts.bundle.js" type="text/javascript"></script>
<script src="{{CHAT}}/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="{{CHAT}}/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" type="text/javascript"></script>