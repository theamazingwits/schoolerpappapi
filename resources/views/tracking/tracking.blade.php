@extends($layout)
@section('header_scripts')
<link href="{{CHAT}}/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
<link href="{{CHAT}}/flaticon/flaticon.css" rel="stylesheet" type="text/css" />
<link href="{{CHAT}}/flaticon2/flaticon.css" rel="stylesheet" type="text/css" />
<link href="{{CHAT}}/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<link href="{{CHAT}}/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css" rel="stylesheet" type="text/css" />
<!--end:: Vendor Plugins -->


<script src="{{CHAT}}/socket.io.js" type="text/javascript"></script>
@stop
<!-- $leaveapplystaff = []; -->
@section('content')
<link href="{{CHAT}}/style.bundle.css" rel="stylesheet" type="text/css" />
<style>
	.customscroll1{
		max-height: 36em;
		min-height: 36em;
		overflow-y: hidden;
	}
	.customscroll1:hover{
		overflow-y: scroll;
	}
	.customscroll1::-webkit-scrollbar {
		width: 0.5em;
	}

	.customscroll1::-webkit-scrollbar-track {
		-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
	}

	.customscroll1::-webkit-scrollbar-thumb {
		background-color: darkgrey;
		outline: 1px solid slategrey;
	}
	.customscroll{
		max-height: 38.4em;
		min-height: 38.4em;
		overflow-y: hidden;
	}
	.customscroll:hover{
		overflow-y: scroll;
	}
	.customscroll::-webkit-scrollbar {
		width: 0.5em;
	}

	.customscroll::-webkit-scrollbar-track {
		-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
	}

	.customscroll::-webkit-scrollbar-thumb {
		background-color: darkgrey;
		outline: 1px solid slategrey;
	}
	.container1 h3{
		font-size: 1.8rem!important;
	}
	.card-box{
		padding: 20px;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
    -webkit-border-radius: 3px;
    border-radius: 3px;
    -moz-border-radius: 3px;
    background-clip: padding-box;
    margin-bottom: 20px;
    background-color: #ffffff;
	}
	.text_head{
		font-size: 18px !important;
	}
	.text_des{
		font-size: 18px !important;
    font-weight: bold !important;
	}
	/*.container1{
		margin-top: 4em !important;
	}*/

</style>
<div id="page-wrapper">
  <section id="main" class="main-wrap bgc-white-darkest" role="main">
    <div class="container-fluid content-wrap">
      <br>
    	<div class="kt-container container1">
    		<!-- <div class="row card-box">
    			<div class="col-md-4 card-box">
    				<div class="row">
    					<div class="col-md-2">
    						<img src="{{CHAT}}/track.png" style="width: 8em;height: 9em;">
    					</div>
    					<div class="col-md-4">
    						<p class="kt-notification-v2__item-title">Vehilce No</p>
    					</div>
    					<div class="col-md-6">
    						<p class="kt-notification-v2__item-title">TN84B888</p>
    					</div>
    				</div>
    			</div>
    		</div> -->
    		<div class="row">
    			<div class="col-xl-4 col-lg-4 order-lg-2 order-xl-1">
    				<div class="kt-portlet">
    					<div class="kt-portlet__head">
    						<div class="kt-portlet__head-label">
    							<h3 class="kt-portlet__head-title">
    								Vehicle Details
    							</h3>
    						</div>
    					</div>
    					<div class="kt-portlet__body">
    						<div class="kt-notification-v2 customscroll">
    							@foreach($vehiclelist as $vehicles)
    							<a href="#" class="kt-notification-v2__item" onclick="liveTracking({{$vehicles->vehicle_id}})">
    								<div class="kt-notification-v2__item-icon">
    									<img src="{{CHAT}}/track1.png" style="width: 8em;height: 9em;margin-left: 2em;">
    								</div>
    								<div class="kt-notification-v2__itek-wrapper" style="margin-left: 2em;">
    									<div class="kt-notification-v2__item-title" style="font-size: 20px">
    										<span>Vehicle No :</span> {{$vehicles->number}}
    									</div>
    									<div class="kt-notification-v2__item-title" style="font-size: 20px">
    										<span>Driver Name :</span> {{$vehicles->drivername}}
    									</div>
    									<div class="kt-notification-v2__item-title" style="font-size: 20px">
    										<span>Student Name :</span> {{$vehicles->studentname}}
    									</div>
    									<div class="kt-notification-v2__item-title" style="font-size: 20px">
    										@if($vehicles->trip_status == '1')
    										<span>Trip Status :</span > <span style="color:green;">Running</span>
    										@else
    										<span>Trip Status :</span> <span style="color:red;">Completed</span>
    										@endif
    									</div>
    								</div>
    							</a>
    							<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
    							@endforeach
    						</div>
    					</div>
    				</div>
    			</div>
    			<div class="col-xl-8 col-lg-8 order-lg-2 order-xl-1">
    				<div class="kt-portlet">
    					<div class="kt-portlet__head">
    						<div class="kt-portlet__head-label">
    							<h3 class="kt-portlet__head-title">
    								Tracking
    							</h3>
    						</div>
    					</div>
    					<div class="kt-portlet__body">
    						<div id="map-canvas" style="height: 500px;width: 100%;"></div>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
  </section>
</div>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBa6mNrfx2rsr-S_7b5Asji6DW4E1aJVd8&callback=initialize"> </script>
<script>
	var i=0,k;    
	var map, marker, route ,directionsService1,directionsDisplay1,marker1,marker2;
	var numDeltas = 100;
	var delay = 10; //milliseconds
	var deltaLat;
	var deltaLng;
	var deltaLat1;
	var deltaLng1;
	var interval;
	var firstcount = 0;
	function initialize() {
    
    directionsService1 = new google.maps.DirectionsService;
  //   directionsDisplay1 = new google.maps.DirectionsRenderer(
  // {
  //     suppressMarkers: true
  // });
    directionsDisplay1 = new google.maps.DirectionsRenderer;
    map = new google.maps.Map(document.getElementById('map-canvas'), {
      center: { lat: 13.0827, lng: 80.2707 }, //Chennai latlng
      //minZoom: 12,
      maxZoom: 20,
      zoom: 12,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    directionsDisplay1.setMap(map);
    marker=new google.maps.Marker({map:map});
    marker1=new google.maps.Marker({map:map, icon:"../../images/placeholder.png"});
    marker2=new google.maps.Marker({map:map, icon:"../../images/placeholder.png"});
}

function liveTracking(vehicleid,studentid) {
    initialize();
    clearTimeout(interval);
    $.ajax({
              url: "{{TRACK_URL}}/getTrackData",
              headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
              },
              type: "POST",
              data: {"vehicle_Id" : vehicleid},
              success: function(success) {

                if(success.status == "success")
                {
                    var data=success.user;
                    if(data !== null){

                        if(firstcount == 0){
                    var srclat=parseFloat(data.lat);
                    var srclng=parseFloat(data.lan);
                    localStorage.setItem('testlat',srclat);
                    localStorage.setItem('testlng',srclng);
                    var latlng = new google.maps.LatLng(srclat, srclng);
                    marker.setPosition(latlng);
                    map.setCenter(latlng);
                    map.setZoom(15);
                    }
                    }
            }
                   

              },
              dataType: "json",
              timeout: 2000
            })
    console.log(firstcount);
    (function poll() {
        interval =setTimeout(function() {
            $.ajax({
              url: "{{TRACK_URL}}/getTrackData",
              headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
              },
              type: "POST",
              data: {"vehicle_Id":vehicleid},
              success: function(success) {

                if(success.status == "success")
                {
                    var data=success.user;
                    if(data !== null){

                        if(firstcount == 0){
                    var srclat=parseFloat(data.lat);
                    var srclng=parseFloat(data.lan);
                    localStorage.setItem('testlat',srclat);
                    localStorage.setItem('testlng',srclng);
                    var latlng = new google.maps.LatLng(srclat, srclng);
                    marker.setPosition(latlng);
                    }
                    firstcount++;
                    var srclat = parseFloat(localStorage.getItem('testlat'));
                    var srclng = parseFloat(localStorage.getItem('testlng'));
                    //console.log(srclat+'--->'+srclng);
                    var deslat=parseFloat(data.lat);
                    var deslng=parseFloat(data.lan);
                    //console.log(deslat+'--->'+deslng);
                    if(srclat == undefined || srclat == 0 || srclat == "" || srclat == "NULL" ){
                        return false;
                    }else if(srclng == undefined || srclng == 0 || srclng == "" || srclng == "NULL" ){
                        return false;
                    }else if(deslat == undefined || deslat == 0 || deslat == "" || deslat == "NULL" ){
                        return false;
                    }else if(deslng == undefined || deslng == 0 || deslng == "" || deslng == "NULL" ){
                        return false;
                    } else {
                        console.log(srclat+"--->"+srclng);
                        var dist =distance(srclat,srclng,deslat,deslng).toFixed(1);
                        console.log("distance---",dist);
                        if(dist < 20){
                            console.log("If");
                         }
                        else{
                            //marker.setMap(null);
                            console.log("Else");
                            deltaLat1 = srclat;
                            deltaLng1 = srclng;
                            deltaLat = (deslat - srclat)/numDeltas;
                            deltaLng = (deslng - srclng)/numDeltas;
                            console.log(deltaLat+"******"+deltaLng)
                             k=0;
                            moveMarker1();
                            //getDirections(map,srclat,srclng,deslat,deslng)
                            localStorage.setItem('testlat',deslat);
                            localStorage.setItem('testlng',deslng); 
                        } 
                    }
                    
                        
                }   
                else
                {
                    //swal(success.message);
                } 

            }
                   

              },
              dataType: "json",
              complete: poll,
              timeout: 2000
            })
        }, 10000);
    })();
    
}

function distance(lat1,lon1,lat2,lon2) {
	console.log(lat1+"--->"+lon1+"--->"+lat2+"--->"+lon2);
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2-lat1);  // deg2rad below
    var dLon = deg2rad(lon2-lon1); 
    var a = 
        Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
        Math.sin(dLon/2) * Math.sin(dLon/2)
        ; 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    var d = Math.round((R * c)*1000); // Distance in meter
    
    return d;
    }
      
function deg2rad(deg) {
return deg * (Math.PI/180)
}   
function moveMarker1(){
    console.log(deltaLat1+"******"+deltaLng1)
    deltaLat1 += deltaLat;
    deltaLng1 += deltaLng;
    console.log(deltaLat1+"******"+deltaLng1)
    var latlng = new google.maps.LatLng(deltaLat1, deltaLng1);
   // marker.setTitle("Latitude:"+position[0]+" | Longitude:"+position[1]);
    marker.setPosition(latlng);
    if(k!=numDeltas){
        k++;
        setTimeout(moveMarker1, delay);
    }
}
</script>
@stop
@section('footer_scripts')
@include('tracking.trackingscripts')
@stop