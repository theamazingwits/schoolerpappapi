 @extends($layout)
 @section('header_scripts')

@stop
@section('content')
<div id="page-wrapper">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
        <div class="container-fluid content-wrap">
			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="{{PREFIX}}"><i class="fa fa-home"></i> </a> </li>
							
						@if(checkRole(getUserGrade(2)))
						<li><a href="{{URL_USERS_DASHBOARD}}">{{getPhrase('users_dashboard')}}</a></li>
						<li><a href="{{URL_USERS."staff"}}">{{ getPhrase('staff_users') }}</a> </li>
						@endif
						
						@if(checkRole(getUserGrade(7)))
						<li><a href="{{URL_PARENT_CHILDREN}}">{{ getPhrase('users') }}</a> </li>
						@endif

						<li>{{ $title }} </li>
					</ol>
				</div>
			</div>
			<section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item">
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                        <h2 class="pull-left"> {{ getPhrase('details_of').' '.$record->name }}  
                        </h2>
                      	<!--Start panel icons-->
                  		<div class="panel-icons panel-icon-slide bgc-white-dark">

                            <ul>
                                <li><a href=""><i class="fa fa-angle-left"></i></a>
                                  <ul>
                                      <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                                      <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                                      <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
                                      <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
                                      <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li>
                                      <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li>
                                  </ul>
                              </li>
                          </ul>
                      	</div>
                      <!--End panel icons-->
                    </div>
                    <div class="panel-body panel-body-p">
						<div class="profile-details text-center">
							<div class="profile-img"><img src="{{ getProfilePath($record->image,'profile')}}" alt=""></div>
							<div class="aouther-school">
								<h2>{{ $record->name}}</h2>
								<p><span>{{$record->email}}</span></p>
							</div>

						</div>
						<hr>
						<div class="row panel-grid grid-stack" id="panel-grid">
							<section data-gs-min-width="4" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-4">
	                        <!--Start Panel-->
		                        <div class="panel pb-0 bgc-white-dark">
		                            <div class="panel-body pt-2 grid-stack-handle">
		                                <div class="h-lg pos-r panel-body-p py-0">
		                                    <h3 class=""><span class="fs-2 fw-light">{{ getPhrase('lesson_plans')}}</span> <i class="pull-right fa fa-paper-plane-o fs-1 m-2"></i></h3>
		                                    <h6 class="c-primary lh-5"></h6>
		                                    <br><br>
		                                    <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{URL_LESSION_PLANS_DASHBOARD.$record->slug}}">
		                                        <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
		                                            <i class="icon-directions fs-4"></i>
		                                        </span>
		                                        <span class="d-inline-block align-top lh-7">
		                                            <span class="fs-6 d-block">{{ getPhrase('view_all')}}</span>
		                                            <span class="c-gray fs-7">Click to View more details</span>
		                                        </span>
		                                        <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
		                                    </a>
		                                </div>
		                            </div>
		                        </div>
		                        <!--End Panel-->
		                    </section>
		                    <section data-gs-min-width="4" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-4">
	                        <!--Start Panel-->
		                        <div class="panel pb-0 bgc-white-dark">
		                            <div class="panel-body pt-2 grid-stack-handle">
		                                <div class="h-lg pos-r panel-body-p py-0">
		                                    <h3 class=""><span class="fs-2 fw-light">{{ getPhrase('library_history')}}</span> <i class="pull-right fa fa-book fs-1 m-2"></i></h3>
		                                    <h6 class="c-primary lh-5"></h6>
		                                    <br><br>
		                                    <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{URL_USER_LIBRARY_DETAILS.$record->slug}}">
		                                        <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
		                                            <i class="icon-directions fs-4"></i>
		                                        </span>
		                                        <span class="d-inline-block align-top lh-7">
		                                            <span class="fs-6 d-block">{{ getPhrase('view_all')}}</span>
		                                            <span class="c-gray fs-7">Click to View more details</span>
		                                        </span>
		                                        <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
		                                    </a>
		                                </div>
		                            </div>
		                        </div>
		                        <!--End Panel-->
		                    </section>
		                    <section data-gs-min-width="4" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-4">
	                        <!--Start Panel-->
		                        <div class="panel pb-0 bgc-white-dark">
		                            <div class="panel-body pt-2 grid-stack-handle">
		                                <div class="h-lg pos-r panel-body-p py-0">
		                                    <h3 class=""><span class="fs-2 fw-light">{{ getPhrase('timetable')}}</span> <i class="pull-right fa fa-calendar fs-1 m-2"></i></h3>
		                                    <h6 class="c-primary lh-5"></h6>
		                                    <br><br>
		                                    <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{URL_TIMETABLE_STAFF.$record->slug}}">
		                                        <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
		                                            <i class="icon-directions fs-4"></i>
		                                        </span>
		                                        <span class="d-inline-block align-top lh-7">
		                                            <span class="fs-6 d-block">{{ getPhrase('view_all')}}</span>
		                                            <span class="c-gray fs-7">Click to View more details</span>
		                                        </span>
		                                        <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
		                                    </a>
		                                </div>
		                            </div>
		                        </div>
		                        <!--End Panel-->
		                    </section>
		                    <section data-gs-min-width="4" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-4">
	                        <!--Start Panel-->
		                        <div class="panel pb-0 bgc-white-dark">
		                            <div class="panel-body pt-2 grid-stack-handle">
		                                <div class="h-lg pos-r panel-body-p py-0">
		                                    <h3 class=""><span class="fs-2 fw-light">{{ getPhrase('salary_details')}}</span> <i class="pull-right fa fa-money fs-1 m-2"></i></h3>
		                                    <h6 class="c-primary lh-5"></h6>
		                                    <br><br>
		                                    <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{ URL_STAFF_SALARY_DETAILS.$record->slug }}">
		                                        <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
		                                            <i class="icon-directions fs-4"></i>
		                                        </span>
		                                        <span class="d-inline-block align-top lh-7">
		                                            <span class="fs-6 d-block">{{ getPhrase('view_all')}}</span>
		                                            <span class="c-gray fs-7">Click to View more details</span>
		                                        </span>
		                                        <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
		                                    </a>
		                                </div>
		                            </div>
		                        </div>
		                        <!--End Panel-->
		                    </section>
		                    <section data-gs-min-width="4" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-4">
	                        <!--Start Panel-->
		                        <div class="panel pb-0 bgc-white-dark">
		                            <div class="panel-body pt-2 grid-stack-handle">
		                                <div class="h-lg pos-r panel-body-p py-0">
		                                    <h3 class=""><span class="fs-2 fw-light">{{ getPhrase('assignments')}}</span> <i class="pull-right fa fa-pencil fs-1 m-2"></i></h3>
		                                    <h6 class="c-primary lh-5"></h6>
		                                    <br><br>
		                                    <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{URL_ASSIGNMENTS.'/'.$record->slug}}">
		                                        <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
		                                            <i class="icon-directions fs-4"></i>
		                                        </span>
		                                        <span class="d-inline-block align-top lh-7">
		                                            <span class="fs-6 d-block">{{ getPhrase('view_all')}}</span>
		                                            <span class="c-gray fs-7">Click to View more details</span>
		                                        </span>
		                                        <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
		                                    </a>
		                                </div>
		                            </div>
		                        </div>
		                        <!--End Panel-->
		                    </section>
							
						</div>
					</div>
				</div>
			</section>
		</div>
	</section>
</div>
@endsection
 

@section('footer_scripts')
 


@stop
