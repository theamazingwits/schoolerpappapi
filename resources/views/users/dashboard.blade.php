@extends($layout)
@section('content')



		<section id="main" class="main-wrap bgc-white-darkest" role="main">
			<div class="container-fluid content-wrap">
				<div class="row panel-grid grid-stack">

				<section data-gs-min-width="3" data-gs-min-height="20" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-header grid-stack-handle bgc-white-dark panel-header-p panel-header-sm">
                                <h2 class="pull-left"> {{ getPhrase('super_admins')}} </h2>
                               
                            </div>
                            <div class="panel-body pt-2">
                                <div class="h-md pos-r panel-body-p py-0">
                                    <h3 class="lh-0 fs-1 fw-light"> 
										<?php $ownerObject =  App\User::where('role_id','=',1)->get()->count();?>
										 {{$ownerObject}}
									</h3>
							 <br><br>
                                    <a href="{{URL_USERS."owner"}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
                                  
                                </div>
                                  <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80" sparkLineColor="#fafafa"  sparkFillColor="#46aaf9" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>


				<section data-gs-min-width="3" data-gs-min-height="20" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-header grid-stack-handle bgc-white-dark panel-header-p panel-header-sm">
                                <h2 class="pull-left"> {{ getPhrase('admins')}} </h2>
                               
                            </div>
                            <div class="panel-body pt-2">
                                <div class="h-md pos-r panel-body-p py-0">
                                    <h3 class="lh-0 fs-1 fw-light"> 
										<?php $adminObject =  App\User::where('role_id','=',2)->get()->count();?>
							 			{{$adminObject}}
									</h3>
							 <br><br>
                                    <a href="{{URL_USERS."admin"}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
                                   
                                </div>
                                 <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80"  sparkLineColor="#fafafa" sparkFillColor="#97d881" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>


					<section data-gs-min-width="3" data-gs-min-height="20" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-header grid-stack-handle bgc-white-dark panel-header-p panel-header-sm">
                                <h2 class="pull-left"> {{ getPhrase('students')}} </h2>
                               
                            </div>
                            <div class="panel-body pt-2">
                                <div class="h-md pos-r panel-body-p py-0">
                                    <h3 class="lh-0 fs-1 fw-light"> 
									{{ App\Student::where('academic_id','!=','')
											->where('course_parent_id','!=','')
										->where('course_id','!=','')
										->get()->count()}}
									</h3>
							 <br><br>
                                    <a href="{{URL_USERS."student"}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
                                   
                                </div>
                                 <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80"  sparkLineColor="#fafafa" sparkFillColor="#f74948" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,7,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>


					<section data-gs-min-width="3" data-gs-min-height="20" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-header grid-stack-handle bgc-white-dark panel-header-p panel-header-sm">
                                <h2 class="pull-left"> {{ getPhrase('faculty_resources')}} </h2>
                               
                            </div>
                            <div class="panel-body pt-2">
                                <div class="h-md pos-r panel-body-p py-0">
                                    <h3 class="lh-0 fs-1 fw-light"> 
									<?php $staffObject =  App\User::join('staff','staff.user_id','=','users.id')->where('staff.course_id','!=','')
										->where('role_id','=',3)
										->where('status','!=',0)->get()->count();
									?>
						   				{{$staffObject}}
									</h3>
							 <br><br>
                                    <a href="{{URL_USERS."staff"}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
                                   
                                </div>
                                 <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80"  sparkLineColor="#fafafa" sparkFillColor="#aaaaaa" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>


					<section data-gs-min-width="3" data-gs-min-height="20" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-header grid-stack-handle bgc-white-dark panel-header-p panel-header-sm">
                                <h2 class="pull-left"> {{ getPhrase('librarians')}} </h2>
                               
                            </div>
                            <div class="panel-body pt-2">
                                <div class="h-md pos-r panel-body-p py-0">
                                    <h3 class="lh-0 fs-1 fw-light"> 
									<?php $librarianObject =  App\User::where('role_id','=',7)->get()->count();?>
						   				{{$librarianObject}}
									</h3>
							 <br><br>
                                    <a href="{{URL_USERS."librarian"}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
                                   
                                </div>
                                 <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80"  sparkLineColor="#fafafa" sparkFillColor="#f74948" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>



					<section data-gs-min-width="3" data-gs-min-height="20" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-header grid-stack-handle bgc-white-dark panel-header-p panel-header-sm">
                                <h2 class="pull-left"> {{ getPhrase('assistant_librarians')}} </h2>
                               
                            </div>
                            <div class="panel-body pt-2">
                                <div class="h-md pos-r panel-body-p py-0">
                                    <h3 class="lh-0 fs-1 fw-light"> 
									<?php $assistantlibrarianObject =  App\User::where('role_id','=',8)->get()->count();?>
							 			{{$assistantlibrarianObject}}
									</h3>
							 <br><br>
                                    <a href="{{URL_USERS."assistant_librarian"}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
                                 
                                </div>
                                   <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80"  sparkLineColor="#fafafa" sparkFillColor="#f3d74c" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,7,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>



					<section data-gs-min-width="3" data-gs-min-height="20" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-header grid-stack-handle bgc-white-dark panel-header-p panel-header-sm">
                                <h2 class="pull-left"> {{ getPhrase('parents')}} </h2>
                               
                            </div>
                            <div class="panel-body pt-2">
                                <div class="h-md pos-r panel-body-p py-0">
                                    <h3 class="lh-0 fs-1 fw-light"> 
									<?php $parentObject =  App\User::where('role_id','=',6)->get()->count();?>
							 			{{$parentObject}}
									</h3>
							 <br><br>
                                    <a href="{{URL_USERS."parent"}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
                                </div>
                                    <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80"  sparkLineColor="#fafafa" sparkFillColor="#46aaf9" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>

                            </div>
                        </div>
                        <!--End Panel-->
					</section>



					<section data-gs-min-width="3" data-gs-min-height="20" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-header grid-stack-handle bgc-white-dark panel-header-p panel-header-sm">
                                <h2 class="pull-left"> {{ getPhrase('clerks')}} </h2>
                               
                            </div>
                            <div class="panel-body pt-2">
                                <div class="h-md pos-r panel-body-p py-0">
                                    <h3 class="lh-0 fs-1 fw-light"> 
									<?php $clerksObject =  App\User::where('role_id','=',9)->get()->count();
                               
							   ?>
							 {{$clerksObject}}
									</h3>
							 <br><br>
                                    <a href="{{URL_USERS."clerk"}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
                                   
                                </div>
                                 <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80"  sparkLineColor="#fafafa" sparkFillColor="#97d881" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>



					<section data-gs-min-width="3" data-gs-min-height="20" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-header grid-stack-handle bgc-white-dark panel-header-p panel-header-sm">
                                <h2 class="pull-left"> HR </h2>
                               
                            </div>
                            <div class="panel-body pt-2">
                                <div class="h-md pos-r panel-body-p py-0">
                                    <h3 class="lh-0 fs-1 fw-light"> 
									<?php $hrObject =  App\User::where('role_id','=',10)->get()->count();?>
							 			{{$hrObject}}
									</h3>
							 <br><br>
                                    <a href="{{URL_USERS."hr"}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
                                 
                                </div>
                                   <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80"  sparkLineColor="#fafafa" sparkFillColor="#97d881" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>




					<section data-gs-min-width="3" data-gs-min-height="20" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-header grid-stack-handle bgc-white-dark panel-header-p panel-header-sm">
                                <h2 class="pull-left"> {{ getPhrase('receptionist')}} </h2>
                               
                            </div>
                            <div class="panel-body pt-2">
                                <div class="h-md pos-r panel-body-p py-0">
                                    <h3 class="lh-0 fs-1 fw-light"> 
									<?php $receptionistObject =  App\User::where('role_id','=',11)->get()->count();
									   ?>
									   {{$receptionistObject}}
									</h3>
							 <br><br>
                                    <a href="{{URL_USERS."receptionist"}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
                                 
                                </div>
                                   <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80"  sparkLineColor="#fafafa" sparkFillColor="#aaaaaa" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,7,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>



					<section data-gs-min-width="3" data-gs-min-height="20" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-header grid-stack-handle bgc-white-dark panel-header-p panel-header-sm">
                                <h2 class="pull-left"> {{ getPhrase('transport_manager')}} </h2>
                               
                            </div>
                            <div class="panel-body pt-2">
                                <div class="h-md pos-r panel-body-p py-0">
                                    <h3 class="lh-0 fs-1 fw-light"> 
									<?php $transport_managerObject =  App\User::where('role_id','=',12)->get()->count();?>
						   				{{$transport_managerObject}}
									</h3>
							 <br><br>
                                    <a href="{{URL_USERS."transport_manager"}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
                                   
                                </div>
                                 <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80"  sparkLineColor="#fafafa" sparkFillColor="#f74948" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>




					<section data-gs-min-width="3" data-gs-min-height="20" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-header grid-stack-handle bgc-white-dark panel-header-p panel-header-sm">
                                <h2 class="pull-left"> {{ getPhrase('hostel_manager')}} </h2>
                               
                            </div>
                            <div class="panel-body pt-2">
                                <div class="h-md pos-r panel-body-p py-0">
                                    <h3 class="lh-0 fs-1 fw-light"> 
									<?php $hostel_managerObject =  App\User::where('role_id','=',13)->get()->count();?>
							 			{{$hostel_managerObject}}
									</h3>
							 <br><br>
                                    <a href="{{URL_USERS."hostel_manager"}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
                                 
                                </div>
                                   <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80"  sparkLineColor="#fafafa" sparkFillColor="#f3d74c" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>

					
					<section data-gs-min-width="3" data-gs-min-height="20" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-header grid-stack-handle bgc-white-dark panel-header-p panel-header-sm">
                                <h2 class="pull-left"> {{ getPhrase('coordinator')}} </h2>
                               
                            </div>
                            <div class="panel-body pt-2">
                                <div class="h-md pos-r panel-body-p py-0">
                                    <h3 class="lh-0 fs-1 fw-light"> 
									<?php $coordinatorObject =  App\User::where('role_id','=',15)->get()->count();
                               
							   ?>
							 {{$coordinatorObject}}
									</h3>
							 <br><br>
                                    <a href="{{URL_USERS."coordinator"}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
                                  
                                </div>
                                  <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80"  sparkLineColor="#fafafa" sparkFillColor="#f74948" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>


					<section data-gs-min-width="3" data-gs-min-height="20" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-header grid-stack-handle bgc-white-dark panel-header-p panel-header-sm">
                                <h2 class="pull-left"> {{ getPhrase('all_users')}} </h2>
                               
                            </div>
                            <div class="panel-body pt-2">
                                <div class="h-md pos-r panel-body-p py-0">
                                    <h3 class="lh-0 fs-1 fw-light"> 
									{{ App\User::get()->where('users.role_id','!=',14)->count()}}
									</h3>
							 <br><br>
                                    <a href="{{URL_USERS."users"}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
                                  
                                </div>
                                  <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80"  sparkLineColor="#fafafa" sparkFillColor="#46aaf9" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,7,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>




					<section data-gs-min-width="3" data-gs-min-height="20" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-header grid-stack-handle bgc-white-dark panel-header-p panel-header-sm">
                                <h2 class="pull-left"> {{ getPhrase('faculty_resources_inactive_list')}} </h2>
                               
                            </div>
                            <div class="panel-body pt-2">
                                <div class="h-md pos-r panel-body-p py-0">
                                    <h3 class="lh-0 fs-1 fw-light"> 
									<?php $staff_inactive_listobject = App\User::where('status','=',0)
								->where('role_id','=',3)->get()->count();
								  ?>
										{{$staff_inactive_listobject}}
									</h3>
							 <br><br>
                                    <a href="{{URL_USERS_STAFF_INACTIVE."staff"}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
                                  
                                </div>
                                  <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80"  sparkLineColor="#fafafa" sparkFillColor="#97d881" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>


					 @if(count($academic_details)&&count($course_details))
					<section data-gs-min-width="3" data-gs-min-height="20" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-header grid-stack-handle bgc-white-dark panel-header-p panel-header-sm">
                                <h2 class="pull-left">   {{ getPhrase('create_user')}}    </h2>
                               
                            </div>
                            <div class="panel-body pt-2">
                                <div class="h-md pos-r panel-body-p py-0">
                                    <h3 class="lh-0 fs-1 fw-light"> 
									<i class="fa fa-user-plus" aria-hidden="true"></i>
									</h3>
							 <br><br>
							 <a href="{{URL_USERS_ADD}}" style="text-decoration: underline;"><h6 class="c-gray">{{ getPhrase('create')}}</h6></a>
                                   
                                </div>
                                 <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80"  sparkLineColor="#fafafa" sparkFillColor="#aaaaaa" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,7,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>
					@endif

					@if(!count($academic_details)||!count($course_details))
					<section data-gs-min-width="3" data-gs-min-height="20" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-header grid-stack-handle bgc-white-dark panel-header-p panel-header-sm">
                                <h2 class="pull-left">   {{ getPhrase('create_user')}}    </h2>
                               
                            </div>
                            <div class="panel-body pt-2">
                                <div class="h-md pos-r panel-body-p py-0">
                                    <h3 class="lh-0 fs-1 fw-light"> 
									<i class="fa fa-user-plus" aria-hidden="true"></i>
									</h3>
							 <br><br>
							 <a href="javascript:void(0);"  onclick="showMessage()" style="text-decoration: underline;"><h6 class="c-gray">{{ getPhrase('create')}}</h6></a>
                                    
                                   
                                </div>
                                 <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80"  sparkLineColor="#fafafa" sparkFillColor="#aaaaaa" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5,4,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>
					@endif

		</div>
		</div>
	</section>

	<div id="myUserModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><b>{{getPhrase('update_master_setup')}}</b></h4>
      </div>
      <div class="modal-body">
       
        <h4 style="color: #ffa616;" >{{getPhrase('please_update_master_setup_details')}}</h4 textalign="center">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">{{getPhrase('ok')}}</button>
      </div>
      
    </div>

  </div>
</div>


				
				
		<!-- /#page-wrapper -->

@stop

@section('footer_scripts')
<script >
 	 function showMessage(){

 			$('#myUserModal').modal('show');
 		}
  
 </script>

@stop
