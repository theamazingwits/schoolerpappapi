@extends('layouts.admin.adminlayout')

@section('header_scripts')
<link rel="stylesheet" type="text/css" href="{{CSS}}select2.css">
@stop

@section('content')
<div id="page-wrapper" ng-controller="academic_controller">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
	    <div class="container-fluid">
			<!-- Page Heading -->
			<div class="row">
					<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>
						 <li><a href="{{URL_USERS_DASHBOARD}}">{{getPhrase('users_dashboard')}}</a></li>
						<li><a href="{{URL_USERS."users"}}">{{ getPhrase('all_users')}}</a> </li>
						<li class="active">{{isset($title) ? $title : ''}}</li>
					</ol>
				</div>
			</div>

			
				@include('errors.errors')
			<!-- /.row -->
			
			<section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item">
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    
                    <div class="panel-body panel-body-p">
					<!--					<hr class="margin-mid">-->
						<h4 class="pull-left">{{getPhrase('update')}} {{ $user_name }} {{ getPhrase('profile')}}</h4>
                    	<div class="pull-right messages-buttons helper_step1">
							<a href="{{URL_USERS."student"}}" class="btn  btn-primary button panel-header-button" >{{ getPhrase('students_list')}}</a>
						</div>
						<br><br>

						<ul class="nav nav-tabs add-student-tabs">


							<li class="{{isActive($tab_active, 'general')}}"><a data-toggle="tab" href="#academic_details">{{ getPhrase('general_details')}}</a></li>
							
							<li class="{{isActive($tab_active, 'personal')}}" ><a data-toggle="tab" href="#personal_details">{{ getPhrase('personal_details')}}</a></li>
							
							<li  class="{{isActive($tab_active, 'contact')}}" ><a data-toggle="tab" href="#contact_details">{{ getPhrase('contact_details')}}</a></li>

							<li  class="{{isActive($tab_active, 'parent_details')}}" ><a data-toggle="tab" href="#parent_details">{{ getPhrase('parent_login')}}</a></li>
							
							 
						</ul>
						<div class="tab-content tab-content-style">
							
							@include('users.student.student-general-details-fields', 
									array('tab_active' => $tab_active, 'record' => $record,  'academic_years'=>$academic_years, 'courses_list'=>$courses_list, 'courses_parent_list'=>$courses_parent_list, 'years'=>$years, 'semisters'=>$semisters,
										'having_semiseter'=>$having_semiseter
									))
							@include('users.student.student-personel-details-fields', 
									array('tab_active' => $tab_active, 'record' => $record,'countries' => $countries, 'religions'=>$religions, 'categories'=>$categories))
							@include('users.student.student-contact-details-fields', 
									array('tab_active' => $tab_active, 'record' => $record,'ph_no'=>$ph_no))
							@include('users.student.student-parent-details-fields', 
									array('tab_active' => $tab_active, 'record' => $record,'countries' => $countries, 'religions'=>$religions, 'categories'=>$categories))
							

						</div>
					</div>
				</div>
			</section>
		</div>
		<!-- /.container-fluid -->
	</section>
</div>
<!-- /#page-wrapper -->
@stop

@section('footer_scripts')
	<script src="{{JS}}moment.min.js"></script>
	<script src="{{JS}}bootstrap-datepicker.min.js"></script>
	<script src="{{JS}}bootstrap-datetimepicker.js"></script>
	<script src="{{JS}}select2.js"></script>
	<script>
  $('#dp').datepicker({
    autoclose: true,
    format: '{{getDateFormat()}}',
  });
  $('#dp1').datepicker({
    autoclose: true,
    format: '{{getDateFormat()}}',
  });
   // $(function () {
   //      $('#dp').datetimepicker({
   //             format: 'YYYY-MM-DD',
            
   //          });
   //  });
</script>
	@include('users.student.scripts.js-scripts')
@stop

