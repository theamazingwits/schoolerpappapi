 
@extends($layout)
 
@section('content')
<div id="page-wrapper">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
    	<div class="container-fluid content-wrap">
			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>
						@if(checkRole(getUserGrade(2)))
						<li><a href="{{URL_USERS}}">{{ getPhrase('users')}}</a> </li>
						<li class="active">{{isset($title) ? $title : ''}}</li>
						@else
						<li class="active">{{$title}}</li>
						@endif
					</ol>
				</div>
			</div>
			@include('errors.errors')
			<!-- /.row -->
			<?php 
			$user_options = null;
			if($record->settings)
				$user_options = json_decode($record->settings)->user_preferences;
			?>
			<section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item">
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                        <h2 class="pull-left"> {{ $title }} </h2>
                        
                        <!--Start panel icons-->
                        <div class="panel-icons panel-icon-slide bgc-white-dark">
                            <ul>
                                <li><a href=""><i class="fa fa-angle-left"></i></a>
                                    <ul>
                                        <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                                        <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                                        <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
                                        <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
                                        <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li>
                                        <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <!--End panel icons-->
                    </div>
                    <div class="panel-body panel-body-p">
					 	<?php $button_name = getPhrase('update'); ?>
						{{ Form::model($record, 
						array('url' => URL_USERS_SETTINGS.$record->slug, 
						'method'=>'patch','novalidate'=>'','name'=>'formUsers ', 'files'=>'true' )) }}
						<h1>{{getPhrase('quiz_and_exam_series')}}</h1><br>
						<div class="row">
							@foreach($quiz_categories as $category)
		 					<?php 
			 					$checked = '';
			 					if($user_options) {
			 						if(count($user_options->quiz_categories))
			 						{
			 							if(in_array($category->id,$user_options->quiz_categories))
			 								$checked='checked';
			 						}
			 					}
		 					?>
							<div class="col-md-3">
								<div class="ui toggle checkbox dynamic mt-1">
                                    <input type="checkbox" name="quiz_categories[{{$category->id}}]" id="static-toggle" {{$checked}}>
                                    <label for="static-toggle">{{$category->category}}</label>
                                </div>
								<!-- <label class="checkbox-inline" >
									<input type="checkbox" data-toggle="toggle" name="quiz_categories[{{$category->id}}]" data-onstyle="primary" data-offstyle="default" {{$checked}}> {{$category->category}}
								</label> -->
							</div>
							@endforeach
				 		</div>
				 		<h1>LMS {{getPhrase('categories')}}</h1>
						<div class="row">
							@foreach($lms_category as $category)
		 					<?php 

			 					$checked = '';
			 					if($user_options) {
			 						if(count($user_options->lms_categories))
			 						{
			 							if(in_array($category->id,$user_options->lms_categories))
			 								$checked='checked';
			 						}
			 					}
		 					?>
							<div class="col-md-3">
								<div class="ui toggle checkbox dynamic mt-1">
                                    <input type="checkbox" name="lms_categories[{{$category->id}}]" id="static-toggle" {{$checked}}>
                                    <label for="static-toggle">{{$category->category}}</label>
                                </div>
								<!-- <label class="checkbox-inline">
									<input 	type="checkbox" 
											data-toggle="toggle" 
											data-onstyle="primary" 
											data-offstyle="default"
											name="lms_categories[{{$category->id}}]" 
											{{$checked}}
											> {{$category->category}}
								</label> -->
							</div>
							@endforeach
				 		</div>
						<div class="buttons text-center">
							<button class="btn btn-lg btn-primary button"
							>{{ getPhrase('update') }}</button>
						</div>
						{!! Form::close() !!}
					</div>
				</div>
			</section>
		</div>
	</section>
	<!-- /.container-fluid -->
</div>
		<!-- /#page-wrapper -->
@endsection

@section('footer_scripts')
 @include('common.validations');
 <script src="{{JS}}bootstrap-toggle.min.js"></script>
@stop