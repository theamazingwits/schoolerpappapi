@extends($layout)

@section('header_scripts')

<link rel="stylesheet" type="text/css" href="{{CSS}}select2.css">
@stop
@section('content')
<div id="page-wrapper" ng-controller="staffController">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
	    <div class="container-fluid content-wrap">
			<!-- Page Heading -->
			<div class="row">
					<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>
						 <li><a href="{{URL_USERS_DASHBOARD}}">{{getPhrase('users_dashboard')}}</a></li>
						<li><a href="{{URL_USERS."users"}}">{{ getPhrase('all_users')}}</a> </li>
						<li class="active">{{getPhrase('staff_profile')}}</li>
					</ol>
				</div>
			</div>


				@include('errors.errors')
			<!-- /.row -->
			<section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item">
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    
                    <div class="panel-body panel-body-p">
					<!--					<hr class="margin-mid">-->
						<h4 class="pull-left">{{getPhrase('update')}} {{ $user_name }} {{ getPhrase('profile')}}</h4>
                    	<div class="pull-right messages-buttons helper_step1">
							<a href="{{URL_USERS."staff"}}" class="btn  btn-primary button" >{{ getPhrase('staff_list')}}</a>
						</div>
						<br><br>


						<ul class="nav nav-tabs add-student-tabs">


							<li class="{{isActive($tab_active, 'general')}}"><a data-toggle="tab" href="#academic_details">{{ getPhrase('general_details')}}</a></li>
							
							<li class="{{isActive($tab_active, 'personal')}}" ><a data-toggle="tab" href="#personal_details">{{ getPhrase('personal_details')}}</a></li>
							
							<li  class="{{isActive($tab_active, 'contact')}}" ><a data-toggle="tab" href="#contact_details">{{ getPhrase('contact_details')}}</a></li>
							
						</ul>
						<div class="tab-content tab-content-style">
							
					@include('users.staff.staff-general-details-fields', 
					array('tab_active' => $tab_active, 'record' => $record ,'courses_list'=>$courses_list, 'courses_parent_list'=>$courses_parent_list))
							@include('users.staff.staff-personel-details-fields', 
									array('tab_active' => $tab_active, 'record' => $record,'countries' => $countries))
							@include('users.staff.staff-contact-details-fields', 
									array('tab_active' => $tab_active, 'record' => $record))
							
						</div>
					</div>
				</div>
			</section>
		</div>
		<!-- /.container-fluid -->
	</section>
</div>
<!-- /#page-wrapper -->
@stop

@section('footer_scripts')
	<script src="{{JS}}bootstrap-datepicker.min.js"></script>
 	<script>
  $('#dp').datepicker({
    autoclose: true,
    format: '{{getDateFormat()}}',
  });
  $('#dp1').datepicker({
    autoclose: true,
    format: '{{getDateFormat()}}',
  });
   // $(function () {
   //      $('#dp').datetimepicker({
   //             format: 'YYYY-MM-DD',
            
   //          });
   //  });
</script>
	@include('users.staff.scripts.js-scripts')

@stop