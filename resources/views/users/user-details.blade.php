 @extends($layout)
 @section('header_scripts')

@stop
@section('content')
<?php
	if($right_bar === TRUE){
		$column = "col-xl-8";
		$column1 = "col-xl-6";
	}else{
		$column = "col-xl-12";
		$column1 = "col-xl-4";
	}
?>
<div id="page-wrapper">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
    	<div class="container-fluid content-wrap">
			<!-- Page Heading -->
			<div class="">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="{{PREFIX}}"><i class="bc-home fa fa-home"></i> </a> </li>
	                     @if(checkRole(getUserGrade(2)))
						<li><a href="{{URL_USERS_DASHBOARD}}">{{getPhrase('users_dashboard')}}</a></li>
	                      @endif
						@if(checkRole(getUserGrade(2)))
						<li><a href="{{URL_USERS."student"}}">{{ getPhrase('student_users') }}</a> </li>
						@endif
						
						@if(checkRole(getUserGrade(7)))
						<li><a href="{{URL_PARENT_CHILDREN}}">{{ getPhrase('children') }}</a> </li>
						@endif

						<li>{{ $title }} </li>
					</ol>
				</div>
			</div>

			
	     	<section class="col-sm-12 col-md-12 col-lg-12 {{$column}} panel-wrap panel-grid-item">
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                        <h2 class="pull-left"> {{ $title }} </h2>
                        
                        <!--Start panel icons-->
                        <div class="panel-icons panel-icon-slide bgc-white-dark">
                            <ul>
                                <li><a href=""><i class="fa fa-angle-left"></i></a>
                                    <ul>
                                        <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                                        <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                                        <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
                                        <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
                                       <!--  <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li> -->
                                        <!-- <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li> -->
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <!--End panel icons-->
                    </div>
                    <div class="panel-body panel-body-p">
						<div class="profile-details text-center">
							<div class="profile-img"><img src="{{ getProfilePath($record->image,'profile')}}" alt=""></div>
							<div class="aouther-school">
								<h2>{{ $record->name}}</h2>
								<p><span>{{$record->email}}</span></p>
								
							</div>
						</div>
						<hr>
						<h3 class="profile-details-title">{{ getPhrase('reports')}}</h3>
						<!-- <div class="row"> -->
							<section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 {{$column1}}">
		                        <!--Start Panel-->
		                        <div class="panel pb-0 bgc-white-dark">
		                            <div class="panel-body pt-2 grid-stack-handle">
		                                <div class="h-lg pos-r panel-body-p py-0">
		                                    <h3 class=""><span class="fs-2 fw-light">{{ getPhrase('exam_history')}}</span> <i class="pull-right fa fa-history fs-1 m-2"></i></h3>
		                                    <h6 class="c-primary lh-5"></h6>
		                                    <br><br>
		                                    <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{URL_STUDENT_EXAM_ATTEMPTS.$record->slug}}">
		                                        <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
		                                            <i class="icon-directions fs-4"></i>
		                                        </span>
		                                        <span class="d-inline-block align-top lh-7">
		                                            <span class="fs-6 d-block">{{ getPhrase('view_details')}}</span>
		                                            <span class="c-gray fs-7">Click to View more details</span>
		                                        </span>
		                                        <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
		                                    </a>
		                                </div>
		                            </div>
		                        </div>
		                        <!--End Panel-->
		                    </section>
		                    <section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 {{$column1}}">
		                        <!--Start Panel-->
		                        <div class="panel pb-0 bgc-white-dark">
		                            <div class="panel-body pt-2 grid-stack-handle">
		                                <div class="h-lg pos-r panel-body-p py-0">
		                                    <h3 class=""><span class="fs-2 fw-light">{{ getPhrase('by_exam')}}</span> <i class="pull-right fa fa-flag fs-1 m-2"></i></h3>
		                                    <h6 class="c-primary lh-5"></h6>
		                                    <br><br>
		                                    <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{URL_STUDENT_ANALYSIS_BY_EXAM.$record->slug}}">
		                                        <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
		                                            <i class="icon-directions fs-4"></i>
		                                        </span>
		                                        <span class="d-inline-block align-top lh-7">
		                                            <span class="fs-6 d-block">{{ getPhrase('view_details')}}</span>
		                                            <span class="c-gray fs-7">Click to View more details</span>
		                                        </span>
		                                        <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
		                                    </a>
		                                </div>
		                            </div>
		                        </div>
		                        <!--End Panel-->
		                    </section>
		                    <section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 {{$column1}}">
		                        <!--Start Panel-->
		                        <div class="panel pb-0 bgc-white-dark">
		                            <div class="panel-body pt-2 grid-stack-handle">
		                                <div class="h-lg pos-r panel-body-p py-0">
		                                    <h3 class=""><span class="fs-2 fw-light">{{ getPhrase('by_subject')}}</span> <i class="pull-right fa fa-key fs-1 m-2"></i></h3>
		                                    <h6 class="c-primary lh-5"></h6>
		                                    <br><br>
		                                    <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{URL_STUDENT_ANALYSIS_SUBJECT.$record->slug}}">
		                                        <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
		                                            <i class="icon-directions fs-4"></i>
		                                        </span>
		                                        <span class="d-inline-block align-top lh-7">
		                                            <span class="fs-6 d-block">{{ getPhrase('view_details')}}</span>
		                                            <span class="c-gray fs-7">Click to View more details</span>
		                                        </span>
		                                        <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
		                                    </a>
		                                </div>
		                            </div>
		                        </div>
		                        <!--End Panel-->
		                    </section>
		                                 <!--**  subscriptions 4**-->
		                	<section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 {{$column1}}">
		                        <!--Start Panel-->
		                        <div class="panel pb-0 bgc-white-dark">
		                            <div class="panel-body pt-2 grid-stack-handle">
		                                <div class="h-lg pos-r panel-body-p py-0">
		                                    <h3 class=""><span class="fs-2 fw-light">{{ getPhrase('subscriptions')}}</span> <i class="pull-right fa fa-credit-card fs-1 m-2"></i></h3>
		                                    <h6 class="c-primary lh-5"></h6>
		                                    <br><br>
		                                    <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{URL_PAYMENTS_LIST.$record->slug}}">
		                                        <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
		                                            <i class="icon-directions fs-4"></i>
		                                        </span>
		                                        <span class="d-inline-block align-top lh-7">
		                                            <span class="fs-6 d-block">{{ getPhrase('view_details')}}</span>
		                                            <span class="c-gray fs-7">Click to View more details</span>
		                                        </span>
		                                        <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
		                                    </a>
		                                </div>
		                            </div>
		                        </div>
		                        <!--End Panel-->
		                    </section>
		                                 <!--**  marks 5**-->
		                	<section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 {{$column1}}">
		                        <!--Start Panel-->
		                        <div class="panel pb-0 bgc-white-dark">
		                            <div class="panel-body pt-2 grid-stack-handle">
		                                <div class="h-lg pos-r panel-body-p py-0">
		                                    <h3 class=""><span class="fs-2 fw-light">{{ getPhrase('marks')}}</span> <i class="pull-right fa fa-pencil fs-1 m-2"></i></h3>
		                                    <h6 class="c-primary lh-5"></h6>
		                                    <br><br>
		                                    <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{URL_STUDENT_RESULTS.$record->slug}}">
		                                        <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
		                                            <i class="icon-directions fs-4"></i>
		                                        </span>
		                                        <span class="d-inline-block align-top lh-7">
		                                            <span class="fs-6 d-block">{{ getPhrase('view_details')}}</span>
		                                            <span class="c-gray fs-7">Click to View more details</span>
		                                        </span>
		                                        <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
		                                    </a>
		                                </div>
		                            </div>
		                        </div>
		                        <!--End Panel-->
		                    </section>
		                                 <!--**  attendance 6**-->
		                	<section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 {{$column1}}">
		                        <!--Start Panel-->
		                        <div class="panel pb-0 bgc-white-dark">
		                            <div class="panel-body pt-2 grid-stack-handle">
		                                <div class="h-lg pos-r panel-body-p py-0">
		                                    <h3 class=""><span class="fs-2 fw-light">{{ getPhrase('attendance')}}</span> <i class="pull-right fa fa-calendar fs-1 m-2"></i></h3>
		                                    <h6 class="c-primary lh-5"></h6>
		                                    <br><br>
		                                    <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{URL_STUDENT_ATTENDENCE_REPORT.'/'.$record->slug}}">
		                                        <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
		                                            <i class="icon-directions fs-4"></i>
		                                        </span>
		                                        <span class="d-inline-block align-top lh-7">
		                                            <span class="fs-6 d-block">{{ getPhrase('view_details')}}</span>
		                                            <span class="c-gray fs-7">Click to View more details</span>
		                                        </span>
		                                        <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
		                                    </a>
		                                </div>
		                            </div>
		                        </div>
		                        <!--End Panel-->
		                    </section>
		                                 <!--**  timetable 7**-->
		                	<section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 {{$column1}}">
		                        <!--Start Panel-->
		                        <div class="panel pb-0 bgc-white-dark">
		                            <div class="panel-body pt-2 grid-stack-handle">
		                                <div class="h-lg pos-r panel-body-p py-0">
		                                    <h3 class=""><span class="fs-2 fw-light">{{ getPhrase('timetable')}}</span> <i class="pull-right fa fa-clock-o fs-1 m-2"></i></h3>
		                                    <h6 class="c-primary lh-5"></h6>
		                                    <br><br>
		                                    <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{URL_TIMETABLE_STAFF_STUDENT_PRINT.$record->slug}}">
		                                        <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
		                                            <i class="icon-directions fs-4"></i>
		                                        </span>
		                                        <span class="d-inline-block align-top lh-7">
		                                            <span class="fs-6 d-block">{{ getPhrase('view_details')}}</span>
		                                            <span class="c-gray fs-7">Click to View more details</span>
		                                        </span>
		                                        <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
		                                    </a>
		                                </div>
		                            </div>
		                        </div>
		                        <!--End Panel-->
		                    </section>
		                                 <!--**  library_history 8**-->
		                	<section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 {{$column1}}">
		                        <!--Start Panel-->
		                        <div class="panel pb-0 bgc-white-dark">
		                            <div class="panel-body pt-2 grid-stack-handle">
		                                <div class="h-lg pos-r panel-body-p py-0">
		                                    <h3 class=""><span class="fs-2 fw-light">{{ getPhrase('library_history')}}</span> <i class="pull-right fa fa-book fs-1 m-2"></i></h3>
		                                    <h6 class="c-primary lh-5"></h6>
		                                    <br><br>
		                                    <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{URL_USER_LIBRARY_DETAILS.$record->slug}}">
		                                        <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
		                                            <i class="icon-directions fs-4"></i>
		                                        </span>
		                                        <span class="d-inline-block align-top lh-7">
		                                            <span class="fs-6 d-block">{{ getPhrase('view_details')}}</span>
		                                            <span class="c-gray fs-7">Click to View more details</span>
		                                        </span>
		                                        <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
		                                    </a>
		                                </div>
		                            </div>
		                        </div>
		                        <!--End Panel-->
		                    </section>
		                                 <!--**  transfers_list 9**-->
		                	<section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 {{$column1}}">
		                        <!--Start Panel-->
		                        <div class="panel pb-0 bgc-white-dark">
		                            <div class="panel-body pt-2 grid-stack-handle">
		                                <div class="h-lg pos-r panel-body-p py-0">
		                                    <h3 class=""><span class="fs-2 fw-light">{{ getPhrase('transfers_list')}}</span> <i class="pull-right fa fa-exchange fs-1 m-2"></i></h3>
		                                    <h6 class="c-primary lh-5"></h6>
		                                    <br><br>
		                                    <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{URL_USER_TRANSFERS_DETAILS.$record->slug}}">
		                                        <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
		                                            <i class="icon-directions fs-4"></i>
		                                        </span>
		                                        <span class="d-inline-block align-top lh-7">
		                                            <span class="fs-6 d-block">{{ getPhrase('view_details')}}</span>
		                                            <span class="c-gray fs-7">Click to View more details</span>
		                                        </span>
		                                        <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
		                                    </a>
		                                </div>
		                            </div>
		                        </div>
		                        <!--End Panel-->
		                    </section>
		                                 <!--**  fee_schedules 10**-->
		                	<section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 {{$column1}}">
		                        <!--Start Panel-->
		                        <div class="panel pb-0 bgc-white-dark">
		                            <div class="panel-body pt-2 grid-stack-handle">
		                                <div class="h-lg pos-r panel-body-p py-0">
		                                    <h3 class=""><span class="fs-2 fw-light">{{ getPhrase('fee_schedules')}}</span> <i class="pull-right fa fa-bars fs-1 m-2"></i></h3>
		                                    <h6 class="c-primary lh-5"></h6>
		                                    <br><br>
		                                    <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{URL_USER_FEE_SCHEDULES.$student_details->id}}">
		                                        <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
		                                            <i class="icon-directions fs-4"></i>
		                                        </span>
		                                        <span class="d-inline-block align-top lh-7">
		                                            <span class="fs-6 d-block">{{ getPhrase('view_details')}}</span>
		                                            <span class="c-gray fs-7">Click to View more details</span>
		                                        </span>
		                                        <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
		                                    </a>
		                                </div>
		                            </div>
		                        </div>
		                        <!--End Panel-->
		                    </section>
		                                 <!--**  fee_history 11**-->
		                	<section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 {{$column1}}">
		                        <!--Start Panel-->
		                        <div class="panel pb-0 bgc-white-dark">
		                            <div class="panel-body pt-2 grid-stack-handle">
		                                <div class="h-lg pos-r panel-body-p py-0">
		                                    <h3 class=""><span class="fs-2 fw-light">{{ getPhrase('fee_history')}}</span> <i class="pull-right fa fa-money fs-1 m-2"></i></h3>
		                                    <h6 class="c-primary lh-5"></h6>
		                                    <br><br>
		                                    <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{URL_USER_FEE_PAID_HISTORY.$student_details->id}}">
		                                        <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
		                                            <i class="icon-directions fs-4"></i>
		                                        </span>
		                                        <span class="d-inline-block align-top lh-7">
		                                            <span class="fs-6 d-block">{{ getPhrase('view_details')}}</span>
		                                            <span class="c-gray fs-7">Click to View more details</span>
		                                        </span>
		                                        <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
		                                    </a>
		                                </div>
		                            </div>
		                        </div>
		                        <!--End Panel-->
		                    </section>
		                                 <!--**  assignments 12**-->
		                	<section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 {{$column1}}">
		                        <!--Start Panel-->
		                        <div class="panel pb-0 bgc-white-dark">
		                            <div class="panel-body pt-2 grid-stack-handle">
		                                <div class="h-lg pos-r panel-body-p py-0">
		                                    <h3 class=""><span class="fs-2 fw-light">{{ getPhrase('assignments')}}</span> <i class="pull-right fa fa-pencil fs-1 m-2"></i></h3>
		                                    <h6 class="c-primary lh-5"></h6>
		                                    <br><br>
		                                    <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{URL_STUDENT_ASSIGNMENT.'/'.$record->slug}}">
		                                        <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
		                                            <i class="icon-directions fs-4"></i>
		                                        </span>
		                                        <span class="d-inline-block align-top lh-7">
		                                            <span class="fs-6 d-block">{{ getPhrase('view_details')}}</span>
		                                            <span class="c-gray fs-7">Click to View more details</span>
		                                        </span>
		                                        <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
		                                    </a>
		                                </div>
		                            </div>
		                        </div>
		                        <!--End Panel-->
		                    </section>
		                                 <!--**  buildinghostel 13**-->
		                	<section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 {{$column1}}">
		                        <!--Start Panel-->
		                        <div class="panel pb-0 bgc-white-dark">
		                            <div class="panel-body pt-2 grid-stack-handle">
		                                <div class="h-lg pos-r panel-body-p py-0">
		                                    <h3 class=""><span class="fs-2 fw-light">{{ getPhrase('hostel')}}</span> <i class="pull-right fa fa-building fs-1 m-2"></i></h3>
		                                    <h6 class="c-primary lh-5"></h6>
		                                    <br><br>
		                                    <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{URL_STUDENT_HOSTELDETAILS.$record->id}}">
		                                        <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
		                                            <i class="icon-directions fs-4"></i>
		                                        </span>
		                                        <span class="d-inline-block align-top lh-7">
		                                            <span class="fs-6 d-block">{{ getPhrase('view_details')}}</span>
		                                            <span class="c-gray fs-7">Click to View more details</span>
		                                        </span>
		                                        <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
		                                    </a>
		                                </div>
		                            </div>
		                        </div>
		                        <!--End Panel-->
		                    </section>
		                                 <!--**  transport 14**-->
		                	<section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 {{$column1}}">
		                        <!--Start Panel-->
		                        <div class="panel pb-0 bgc-white-dark">
		                            <div class="panel-body pt-2 grid-stack-handle">
		                                <div class="h-lg pos-r panel-body-p py-0">
		                                    <h3 class=""><span class="fs-2 fw-light">{{ getPhrase('transport')}}</span> <i class="pull-right fa fa-bus fs-1 m-2"></i></h3>
		                                    <h6 class="c-primary lh-5"></h6>
		                                    <br><br>
		                                    <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{URL_STUDENT_TRANSPORTDETAILS.$record->id}}">
		                                        <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
		                                            <i class="icon-directions fs-4"></i>
		                                        </span>
		                                        <span class="d-inline-block align-top lh-7">
		                                            <span class="fs-6 d-block">{{ getPhrase('view_details')}}</span>
		                                            <span class="c-gray fs-7">Click to View more details</span>
		                                        </span>
		                                        <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
		                                    </a>
		                                </div>
		                            </div>
		                        </div>
		                        <!--End Panel-->
		                    </section>
							<section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 {{$column1}}">
		                        <!--Start Panel-->
		                        <div class="panel pb-0 bgc-white-dark">
		                            <div class="panel-body pt-2 grid-stack-handle">
		                                <div class="h-lg pos-r panel-body-p py-0">
		                                    <h3 class=""><span class="fs-2 fw-light">{{ getPhrase('Health Details')}}</span> <i class="pull-right 	fa fa-user-md fs-1 m-2"></i></h3>
		                                    <h6 class="c-primary lh-5"></h6>
		                                    <br><br>
		                                    <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{URL_USER_HEALTH_DETAILS.$record->slug}}">
		                                        <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
		                                            <i class="icon-directions fs-4"></i>
		                                        </span>
		                                        <span class="d-inline-block align-top lh-7">
		                                            <span class="fs-6 d-block">{{ getPhrase('view_details')}}</span>
		                                            <span class="c-gray fs-7">Click to View more details</span>
		                                        </span>
		                                        <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
		                                    </a>
		                                </div>
		                            </div>
		                        </div>
		                        <!--End Panel-->
		                    </section>
						<!-- </div> -->
					</div>
				</div>
			</section>
			@if(isset($right_bar))
			<section class="col-sm-12 col-md-12 col-lg-12 col-xl-4 panel-wrap panel-grid-item">
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                        <h2 class="pull-left"> {{ $title }} </h2>
                        
                        <!--Start panel icons-->
                        <div class="panel-icons panel-icon-slide bgc-white-dark">
                            <ul>
                                <li><a href=""><i class="fa fa-angle-left"></i></a>
                                    <ul>
                                        <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                                        <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                                        <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
                                        <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
                                        <!-- <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li> -->
                                        <!-- <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li> -->
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <!--End panel icons-->
                    </div>
                    <div class="panel-body panel-body-p">
                    	<?php $data = '';

						if(isset($right_bar_data))

							$data = $right_bar_data;

						?>

						@include($right_bar_path, array('data' => $data))
                    </div>
                </div>
            </section>
            @endif
		</div>
	</section>
</div>

@endsection
 

@section('footer_scripts')
 
 @include('common.chart', array($chart_data,'ids' =>$ids));

@stop
