@extends($layout)
@section('content')
<div id="page-wrapper">
  <section id="main" class="main-wrap bgc-white-darkest" role="main">
    <div class="container-fluid content-wrap">
      <!-- Page Heading -->
      <div class="row">
        <div class="col-lg-12">
          <ol class="breadcrumb">
            <li><a href="{{PREFIX}}"><i class="bc-home fa fa-home"></i></a> </li>
             @if(checkRole(getUserGrade(2)))
            <li><a href="{{URL_USERS_DASHBOARD}}">{{getPhrase('users_dashboard')}}</a></li>  
            <li><a href="{{URL_USERS."users"}}">{{ getPhrase('users')}}</a> </li>
            <li class="active">{{isset($title) ? $title : ''}}</li>
            @else
            <li class="active">{{$title}}</li>
            @endif
          </ol>
        </div>
      </div>
     @include('errors.errors')
     <!-- /.row -->
     <div class="row panel-grid" id="panel-grid">
        <section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item">
        <!--Start Panel-->
          <div class="panel bgc-white-dark">
            <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                <h2 class="pull-left"> {{ $title }} </h2>
                
                <!--Start panel icons-->
                <div class="panel-icons panel-icon-slide bgc-white-dark">
                    <ul>
                        <li><a href=""><i class="fa fa-angle-left"></i></a>
                            <ul>
                                <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                                <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                                <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
                                <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
                                <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li>
                                <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!--End panel icons-->
            </div>
            <div class="panel-body panel-body-p">
              <?php $button_name = getPhrase('create'); ?>
              @if ($record)
              <?php $button_name = getPhrase('update'); ?>
              {{ Form::model($record, 
                array('url' => URL_USERS_EDIT.$record->slug, 
                'method'=>'patch','novalidate'=>'','name'=>'formUsers ', 'files'=>'true' )) }}
                @else
                {!! Form::open(array('url' => URL_USERS_ADD, 'method' => 'POST', 'novalidate'=>'','name'=>'formUsers ', 'files'=>'true')) !!}
                @endif

                @include('users.form_elements', array('button_name'=> $button_name, 'record' => $record))

                {!! Form::close() !!}
            </div>
          </div>
        </section>
      </div>
    </div>
  </section>
  <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
@endsection

@section('footer_scripts')
@include('common.validations')
@include('common.alertify')
<script>
  var file = document.getElementById('image_input');

  file.onchange = function(e){
    var ext = this.value.match(/\.([^\.]+)$/)[1];
    switch(ext)
    {
      case 'jpg':
      case 'jpeg':
      case 'png':


      break;
      default:
      alertify.error("{{getPhrase('file_type_not_allowed')}}");
      this.value='';
    }
  };
</script>
@stop