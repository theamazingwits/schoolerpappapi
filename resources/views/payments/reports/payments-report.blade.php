@extends($layout)
@section('content')

<div id="page-wrapper">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
	    <div class="container-fluid content-wrap">
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						 <li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>
						<li>{{ $title}}</li>
					</ol>
				</div>
			</div>

		 	<div class="row panel-grid grid-stack">
			 	<section data-gs-min-width="3" data-gs-min-height="19"  class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
	                <!--Start Panel-->
	                <div class="panel pb-0 bgc-white-dark">
	                    <div class="panel-header grid-stack-handle bgc-white-dark panel-header-p panel-header-sm">
	                        <h3 class="pull-left fs-2 fw-light">  {{ getPhrase('Payments')}}</h3>
	                    </div>
	                    <div class="panel-body pt-2">
	                        <div class="h-md pos-r panel-body-p py-0">
	                            <div class="row">
	                        		<div class="col-md-8">
	                        			<h3 class="lh-0 fs-1 fw-light">{{ $payments->all}}</h3>
	                        		</div>
	                        		<div class="col-md-4">
										<i class="pull-right fw-light fa fa-file-text-o fs-1"></i>
	                        		</div>
	                        	</div>
	                            <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="@if($payment_mode=='online')
							{{URL_ONLINE_PAYMENT_REPORT_DETAILS}}
							@else {{URL_OFFLINE_PAYMENT_REPORT_DETAILS}}
							@endif
							all">
	                                <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
	                                    <i class="icon-directions fs-4"></i>
	                                </span>
	                                <span class="d-inline-block align-top lh-7">
	                                    <span class="fs-6 d-block">{{ getPhrase('view_all')}}</span>
	                                    <span class="c-gray fs-7">Click to View more details</span>
	                                </span>
	                                <span class="pull-right c-gray mt-1 lh-5"></span>
	                            </a>
	                        </div>
	                    </div>
	                </div>
	                <!--End Panel-->
	            </section>
	            <section data-gs-min-width="3" data-gs-min-height="19"  class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
	                <!--Start Panel-->
	                <div class="panel pb-0 bgc-white-dark">
	                    <div class="panel-header grid-stack-handle bgc-white-dark panel-header-p panel-header-sm">
	                        <h3 class="pull-left fs-2 fw-light">  {{ getPhrase('success')}}</h3>
	                    </div>
	                    <div class="panel-body pt-2">
	                        <div class="h-md pos-r panel-body-p py-0">
	                            <div class="row">
	                        		<div class="col-md-8">
	                        			<h3 class="lh-0 fs-1 fw-light">{{ $payments->success}}</h3>
	                        		</div>
	                        		<div class="col-md-4">
										<i class="pull-right fw-light fa fa-check fs-1"></i>
	                        		</div>
	                        	</div>
	                            <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="@if($payment_mode=='online')
							{{URL_ONLINE_PAYMENT_REPORT_DETAILS}}
							@else {{URL_OFFLINE_PAYMENT_REPORT_DETAILS}}
							@endif
							success">
	                                <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
	                                    <i class="icon-directions fs-4"></i>
	                                </span>
	                                <span class="d-inline-block align-top lh-7">
	                                    <span class="fs-6 d-block">{{ getPhrase('view_all')}}</span>
	                                    <span class="c-gray fs-7">Click to View more details</span>
	                                </span>
	                                <span class="pull-right c-gray mt-1 lh-5"></span>
	                            </a>
	                        </div>
	                    </div>
	                </div>
	                <!--End Panel-->
	            </section>
	            <section data-gs-min-width="3" data-gs-min-height="19"  class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
	                <!--Start Panel-->
	                <div class="panel pb-0 bgc-white-dark">
	                    <div class="panel-header grid-stack-handle bgc-white-dark panel-header-p panel-header-sm">
	                        <h3 class="pull-left fs-2 fw-light">  {{ getPhrase('pending')}}</h3>
	                    </div>
	                    <div class="panel-body pt-2">
	                        <div class="h-md pos-r panel-body-p py-0">
	                            <div class="row">
	                        		<div class="col-md-8">
	                        			<h3 class="lh-0 fs-1 fw-light">{{ $payments->pending}}</h3>
	                        		</div>
	                        		<div class="col-md-4">
										<i class="pull-right fw-light fa fa-warning fs-1"></i>
	                        		</div>
	                        	</div>
	                            <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="@if($payment_mode=='online')
							{{URL_ONLINE_PAYMENT_REPORT_DETAILS}}
							@else {{URL_OFFLINE_PAYMENT_REPORT_DETAILS}}
							@endif
							pending">
	                                <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
	                                    <i class="icon-directions fs-4"></i>
	                                </span>
	                                <span class="d-inline-block align-top lh-7">
	                                    <span class="fs-6 d-block">{{ getPhrase('view_all')}}</span>
	                                    <span class="c-gray fs-7">Click to View more details</span>
	                                </span>
	                                <span class="pull-right c-gray mt-1 lh-5"></span>
	                            </a>
	                        </div>
	                    </div>
	                </div>
	                <!--End Panel-->
	            </section>
	            <section data-gs-min-width="3" data-gs-min-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
	                <!--Start Panel-->
	                <div class="panel pb-0 bgc-white-dark">
	                    <div class="panel-header grid-stack-handle bgc-white-dark panel-header-p panel-header-sm">
	                        <h3 class="pull-left fs-2 fw-light">  {{ getPhrase('cancelled')}}</h3>
	                    </div>
	                    <div class="panel-body pt-2">
	                        <div class="h-md pos-r panel-body-p py-0">
	                            <div class="row">
	                        		<div class="col-md-8">
	                        			<h3 class="lh-0 fs-1 fw-light">{{ $payments->cancelled}}</h3>
	                        		</div>
	                        		<div class="col-md-4">
										<i class="pull-right fw-light fa fa-close fs-1"></i>
	                        		</div>
	                        	</div>
	                            <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="@if($payment_mode=='online')
							{{URL_ONLINE_PAYMENT_REPORT_DETAILS}}
							@else {{URL_OFFLINE_PAYMENT_REPORT_DETAILS}}
							@endif
							cancelled">
	                                <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
	                                    <i class="icon-directions fs-4"></i>
	                                </span>
	                                <span class="d-inline-block align-top lh-7">
	                                    <span class="fs-6 d-block">{{ getPhrase('view_all')}}</span>
	                                    <span class="c-gray fs-7">Click to View more details</span>
	                                </span>
	                                <span class="pull-right c-gray mt-1 lh-5"></span>
	                            </a>
	                        </div>
	                    </div>
	                </div>
	                <!--End Panel-->
	            </section>					
			</div>
			<!-- /.container-fluid -->
			<div class="row">
				<section class="col-sm-12 col-md-12 col-lg-12 col-xl-6 panel-wrap panel-grid-item " >
                <!--Start Panel-->
	                <div class="panel bgc-white-dark">
	                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
	                        <h2 class="pull-left"> {{getPhrase('payment_statistics')}}</h2>   
	                        <!--End panel icons-->
	                    </div>
	                    <div class="panel-body panel-body-p packages">
					    	<canvas id="payments_chart" width="100" height="60"></canvas>
					    </div>
					  </div>
				</section>

				<section class="col-sm-12 col-md-12 col-lg-12 col-xl-6 panel-wrap panel-grid-item " >
                <!--Start Panel-->
	                <div class="panel bgc-white-dark">
	                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
	                        <h2 class="pull-left"> {{getPhrase('payment_monthly_statistics')}}</h2>   
	                        <!--End panel icons-->
	                    </div>
	                    <div class="panel-body panel-body-p packages">
					    	<canvas id="payments_monthly_chart" width="100" height="60"></canvas>
					    </div>
				  	</div>
				</section>

				
			</div>
	 
		</div>
	</section>
</div>
		<!-- /#page-wrapper -->

@stop

@section('footer_scripts')
 
 @include('common.chart', array('chart_data'=>$payments_chart_data,'ids' =>array('payments_chart'), 'scale'=>TRUE))
 @include('common.chart', array('chart_data'=>$payments_monthly_data,'ids' =>array('payments_monthly_chart'), 'scale'=>true))
 
 

@stop
