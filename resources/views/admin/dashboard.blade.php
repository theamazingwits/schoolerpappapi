@extends($layout)
@section('header_scripts')
{!! Charts::assets() !!}
@stop
@section('content')

<div id="page-wrapper">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
	    <div class="container-fluid content-wrap">
	        <div class="row panel-grid grid-stack">
	            <section data-gs-min-width="3" data-gs-min-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
	                <!--Start Panel-->
	                <div class="panel pb-0 bgc-white-dark">
	                    <div class="panel-body pt-2 grid-stack-handle">
	                        <div class="h-lg pos-r panel-body-p py-0">
	                            <h3 class=""><span class="fs-2 fw-light">{{ getPhrase('users')}}</span> <i class="pull-right fa fa-users fs-1 m-2"></i></h3>
	                            <h6 class="c-primary lh-5"></h6>
	                            <br><br>
	                            <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{URL_USERS_DASHBOARD}}">
	                                <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
	                                    <i class="icon-directions fs-4"></i>
	                                </span>
	                                <span class="d-inline-block align-top lh-7">
	                                    <span class="fs-6 d-block">View All</span>
	                                    <span class="c-gray fs-7">Click to View more details</span>
	                                </span>
	                                <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
	                            </a>
	                        </div>
	                    </div>
	                </div>
	                <!--End Panel-->
	            </section>
	            <section data-gs-min-width="3" data-gs-min-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
	                <!--Start Panel-->
	                <div class="panel pb-0 bgc-white-dark">
	                    <div class="panel-body pt-2 grid-stack-handle">
	                        <div class="h-lg pos-r panel-body-p py-0">
	                            <h3 class=""><span class="fs-2 fw-light">{{ getPhrase('academics')}}</span> <i class="pull-right fa fa-university fs-1 m-2"></i></h3>
	                            <h6 class="c-primary lh-5"></h6>
	                            <br><br>
	                            <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{URL_ACADEMICOPERATIONS_DASHBOARD}}">
	                                <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
	                                    <i class="icon-directions fs-4"></i>
	                                </span>
	                                <span class="d-inline-block align-top lh-7">
	                                    <span class="fs-6 d-block">View All</span>
	                                    <span class="c-gray fs-7">Click to View more details</span>
	                                </span>
	                                <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
	                            </a>
	                        </div>
	                    </div>
	                </div>
	                <!--End Panel-->
	            </section>



	            <section data-gs-min-width="3" data-gs-min-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
	                <!--Start Panel-->
	                <div class="panel pb-0 bgc-white-dark">
	                    <div class="panel-body pt-2 grid-stack-handle">
	                        <div class="h-lg pos-r panel-body-p py-0">
	                            <h3 class=""><span class="fs-2 fw-light">{{ getPhrase('exams')}}</span> <i class="pull-right fa fa-pencil-square-o fs-1 m-2"></i></h3>
	                            <h6 class="c-primary lh-5"></h6>
	                            <br><br>
	                            <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{URL_EXAMS_DASHBOARD}}">
	                                <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
	                                    <i class="icon-directions fs-4"></i>
	                                </span>
	                                <span class="d-inline-block align-top lh-7">
	                                    <span class="fs-6 d-block">View All</span>
	                                    <span class="c-gray fs-7">Click to View more details</span>
	                                </span>
	                                <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
	                            </a>
	                        </div>
	                       
	                    </div>
	                </div>
	                <!--End Panel-->
	            </section>



	            <section data-gs-min-width="3"  data-gs-min-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
	                <!--Start Panel-->
	                <div class="panel pb-0 bgc-white-dark">
	                    <div class="panel-body pt-2 grid-stack-handle">
	                        <div class="h-lg pos-r panel-body-p py-0">
	                            <h3 class=""><span class="fs-2 fw-light">{{ getPhrase('LMS')}}</span> <i class="pull-right fa fa-leanpub fs-1 m-2"></i></h3>
	                            <h6 class="c-primary lh-5"></h6>
	                            <br><br>
	                            <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{URL_LMS_DASHBOARD}}">
	                                <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
	                                    <i class="icon-directions fs-4"></i>
	                                </span>
	                                <span class="d-inline-block align-top lh-7">
	                                    <span class="fs-6 d-block">View All</span>
	                                    <span class="c-gray fs-7">Click to View more details</span>
	                                </span>
	                                <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
	                            </a>
	                        </div>
	                       
	                    </div>
	                </div>
	                <!--End Panel-->
	            </section>



	            <section data-gs-min-width="3"  data-gs-min-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
	                <!--Start Panel-->
	                <div class="panel pb-0 bgc-white-dark">
	                    <div class="panel-body pt-2 grid-stack-handle">
	                        <div class="h-lg pos-r panel-body-p py-0">
	                            <h3 class=""><span class="fs-2 fw-light">{{ getPhrase('central_library')}}</span> <i class="pull-right fa fa-book fs-1 m-2"></i></h3>
	                            <h6 class="c-primary lh-5"></h6>
	                            <br><br>
	                            <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{URL_LIBRARY_LIBRARYDASHBOARD}}">
	                                <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
	                                    <i class="icon-directions fs-4"></i>
	                                </span>
	                                <span class="d-inline-block align-top lh-7">
	                                    <span class="fs-6 d-block">View All</span>
	                                    <span class="c-gray fs-7">Click to View more details</span>
	                                </span>
	                                <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
	                            </a>
	                        </div>
	                        
	                    </div>
	                </div>
	                <!--End Panel-->
	            </section>




	            <section data-gs-min-width="3"  data-gs-min-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
	                <!--Start Panel-->
	                <div class="panel pb-0 bgc-white-dark">
	                    <div class="panel-body pt-2 grid-stack-handle">
	                        <div class="h-lg pos-r panel-body-p py-0">
	                            <h3 class=""><span class="fs-2 fw-light">{{ getPhrase('languages')}}</span> <i class="pull-right fa fa-language fs-1 m-2"></i></h3>
	                            <h6 class="c-primary lh-5"></h6>
	                            <br><br>
	                            <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{URL_LANGUAGES_LIST}}">
	                                <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
	                                    <i class="icon-directions fs-4"></i>
	                                </span>
	                                <span class="d-inline-block align-top lh-7">
	                                    <span class="fs-6 d-block">View All</span>
	                                    <span class="c-gray fs-7">Click to View more details</span>
	                                </span>
	                                <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
	                            </a>
	                        </div>
	                        
	                    </div>
	                </div>
	                <!--End Panel-->
	            </section>



	            <section data-gs-min-width="3"  data-gs-min-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
	                <!--Start Panel-->
	                <div class="panel pb-0 bgc-white-dark">
	                    <div class="panel-body pt-2 grid-stack-handle">
	                        <div class="h-lg pos-r panel-body-p py-0">
	                            <h3 class=""><span class="fs-2 fw-light">{{ getPhrase('settings')}}</span> <i class="pull-right fa fa-cog fs-1 m-2"></i></h3>
	                            <h6 class="c-primary lh-5"></h6>
	                            <br><br>
	                            <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{URL_SETTINGS_DASHBOARD}}">
	                                <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
	                                    <i class="icon-directions fs-4"></i>
	                                </span>
	                                <span class="d-inline-block align-top lh-7">
	                                    <span class="fs-6 d-block">View All</span>
	                                    <span class="c-gray fs-7">Click to View more details</span>
	                                </span>
	                                <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
	                            </a>
	                        </div>
	                    </div>
	                </div>
	                <!--End Panel-->
	            </section>


	            <section data-gs-min-width="3" data-gs-min-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
	                <!--Start Panel-->
	                <div class="panel pb-0 bgc-white-dark">
	                    <div class="panel-body pt-2 grid-stack-handle">
	                        <div class="h-lg pos-r panel-body-p py-0">
	                            <h3 class=""><span class="fs-2 fw-light">{{ getPhrase('master_setup')}}</span> <i class="pull-right fa fa-cogs fs-1 m-2"></i></h3>
	                            <h6 class="c-primary lh-5"></h6>
	                            <br><br>
	                            <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{URL_COURSES_DASHBOARD}}">
	                                <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
	                                    <i class="icon-directions fs-4"></i>
	                                </span>
	                                <span class="d-inline-block align-top lh-7">
	                                    <span class="fs-6 d-block">View All</span>
	                                    <span class="c-gray fs-7">Click to View more details</span>
	                                </span>
	                                <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
	                            </a>
	                        </div>
	                    </div>
	                </div>
	                <!--End Panel-->
	            </section>
	            
	            <section class="col-sm-12 col-md-12 col-xl-6 panel-wrap panel-grid-item grid-stack-item">
	                <!--Start Panel-->
	                <div class="panel bgc-white-dark">
	                    <div class="panel-header clearfix grid-stack-handle panel-header-p bgc-white-dark panel-header-sm">
	                        <h2 class="pull-left">  {{getPhrase('latest_students')}} </h2>
	                        <!--Start panel icons-->
	                        <div class="panel-icons panel-icon-slide ">
	                            <ul>
	                                <li><a href=""><i class="fa fa-angle-left"></i></a>
	                                    <ul>
	                                        <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
	                                        <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
	                                        <li><a class="" href="{{URL_USERS}}students" ><i class="fa fa-eye"></i></a></li>
	                                    </ul>
	                                </li>
	                            </ul>
	                        </div>
	                        <!--End panel icons-->
	                    </div>
	                    <div class="panel-body panel-body-p">
	                        <div class="row">

	                        <?php $latest_students = App\User::getLatestUsersDashboard('student',8);?>   
	                            <!--Start Panel-->
	                <div class="panel bgc-white-dark">
	                    <div class="panel-body panel-body-p">
	                    <div class="list divided horizontal selection d-xl-flex flex-xl-row">
	                        <div class="row">
	                        @foreach($latest_students as $user)
	                        
	                            <div class="item px-2 px-lg-4 py-1 col-md-3">
	                                <div class="content">
	                                <img class="rounded-circle bgc-white-darkest image" alt="" src="{{ getProfilePath($user->image)}}"><br>
	                                    <a href="{{URL_USER_DETAILS.$user->slug}}" class="header">{{ucfirst($user->name)}}</a><br>
	                                    <span class="users-list-date" style="font-size: 10px;">{{humanizeDate($user->created_at)}}</span>
	                                </div>
	                            </div>
	                            
	                            @endforeach
	                        </div>
	                            </div>

	                    </div>
	                </div>
	                <!--End Panel-->
	                        </div>
	        
	                    </div>
	                </div>
	                <!--End Panel-->
	            </section>


	            <section class="col-sm-12 col-md-12 col-xl-6 panel-wrap panel-grid-item grid-stack-item">
	                <!--Start Panel-->
	                <div class="panel bgc-white-dark">
	                    <div class="panel-header clearfix grid-stack-handle panel-header-p bgc-white-dark panel-header-sm">
	                        <h2 class="pull-left">{{getPhrase('latest_faculty')}} </h2>
	                        <!--Start panel icons-->
	                        <div class="panel-icons panel-icon-slide ">
	                            <ul>
	                                <li><a href=""><i class="fa fa-angle-left"></i></a>
	                                    <ul>
	                                        <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
	                                        <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
	                                        <li><a class="" href="{{URL_USERS}}staff" ><i class="fa fa-eye"></i></a></li>
	                                    </ul>
	                                </li>
	                            </ul>
	                        </div>
	                        <!--End panel icons-->
	                    </div>
	                    <div class="panel-body panel-body-p">
	                        <div class="row">

	                        <?php $latest_staff = App\User::getLatestUsersDashboard('staff',8);?> 
	                            <!--Start Panel-->
	                <div class="panel bgc-white-dark">
	                    <div class="panel-body panel-body-p">
	                    <div class="list divided horizontal selection d-xl-flex flex-xl-row">
	                        <div class="row">
	                        @foreach($latest_staff as $user)
	                        
	                            <div class="item px-2 px-lg-4 py-1 col-md-3">
	                                <div class="content">
	                                <img class="rounded-circle bgc-white-darkest image" alt="" src="{{ getProfilePath($user->image)}}"><br>
	                                    <a href="{{URL_STAFF_DETAILS.$user->slug}}" class="header">{{ucfirst($user->name)}}</a><br>
	                                    <span class="users-list-date" style="font-size: 10px;">{{humanizeDate($user->created_at)}}</span>
	                                </div>
	                            </div>
	                            
	                            @endforeach
	                        </div>
	                            </div>

	                    </div>
	                </div>
	                <!--End Panel-->
	                        </div>
	        
	              
	                    </div>


	                    
	                </div>
	                <!--End Panel-->
	            </section>


	            <section class="col-sm-12 col-md-12 col-xl-6 panel-wrap panel-grid-item grid-stack-item">
	                <!--Start Panel-->
	                <div class="panel bgc-white-dark">
	                    <div class="panel-header clearfix grid-stack-handle panel-header-p bgc-white-dark panel-header-sm">
	                        <h2 class="pull-left">{{getPhrase('recent_online_payments')}} </h2>
	                        <!--Start panel icons-->
	                        <div class="panel-icons panel-icon-slide ">
	                            <ul>
	                                <li><a href=""><i class="fa fa-angle-left"></i></a>
	                                    <ul>
	                                        <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
	                                        <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
	                                        
	                                    </ul>
	                                </li>
	                            </ul>
	                        </div>
	                        <!--End panel icons-->
	                    </div>
	                    <div class="panel-body panel-body-p">
		                    <div class="row">
			                    <?php $online_payments = App\Payment::latestPayments('online',7); ?>
			                    @include('dashboard-elements.payments-information', 
								array(	'heading'	=> '',
									'records'   => $online_payments
								 ))
		                    </div>
	                	</div>
	                </div>
	                <!--End Panel-->
	            </section>


	            <section class="col-sm-12 col-md-12 col-xl-6 panel-wrap panel-grid-item grid-stack-item">
	                <!--Start Panel-->
	                <div class="panel bgc-white-dark">
	                    <div class="panel-header clearfix grid-stack-handle panel-header-p bgc-white-dark panel-header-sm">
	                        <h2 class="pull-left">{{getPhrase('recent_offline_payments')}} </h2>
	                        <!--Start panel icons-->
	                        <div class="panel-icons panel-icon-slide ">
	                            <ul>
	                                <li><a href=""><i class="fa fa-angle-left"></i></a>
	                                    <ul>
	                                        <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
	                                        <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
	                                    </ul>
	                                </li>
	                            </ul>
	                        </div>
	                        <!--End panel icons-->
	                    </div>
	                    <div class="panel-body panel-body-p">
		                    <div class="row">
			                    <?php $offline_payments = App\Payment::latestPayments('offline',7); ?>
			                    @include('dashboard-elements.payments-information', 
								array(	'heading'	=> '',
									'records'   => $offline_payments
								 ))
		                    </div>
	                	</div>
	                </div>
	                <!--End Panel-->
	            </section>                
	        </div>
	    </div>
	</section>			
</div>
		<!-- /#page-wrapper -->

@stop

@section('footer_scripts')
 @include('common.chart', array($chart_data,'ids' =>$ids))
@stop
