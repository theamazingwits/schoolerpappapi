@extends('layouts.admin.adminlayout')

@section('header_scripts')

<link href="//cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel="stylesheet">

@stop

@section('content')

<div id="page-wrapper">

	<section id="main" class="main-wrap bgc-white-darkest" role="main">
	    <div class="container-fluid content-wrap">

			<!-- Page Heading -->

			<div class="row">

				<div class="col-lg-12">

					<ol class="breadcrumb">

						<li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>
						<li><a  href="{{URL_SETTINGS_DASHBOARD}}">{{ getPhrase('master_settings')}}</a></li>

						<li>{{ $title }}</li>

					</ol>

				</div>

			</div>								

			<!-- /.row -->

			<section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item">
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                        <h2 class="pull-left"> {{$title}} </h2>
                        <div class="pull-right messages-buttons">
							<a href="{{URL_EMAIL_TEMPLATES_ADD}}" class="btn  btn-primary button panel-header-button" >{{ getPhrase('create')}}</a>
						</div>
                        <!--End panel icons-->
                    </div>
                    <div class="panel-body panel-body-p packages">

						<div > 

						<table class="table table-striped table-bordered datatable" cellspacing="0" width="100%">

							<thead>

								<tr>									 

									<th>{{ getPhrase('title')}}</th>

									<th>{{ getPhrase('subject')}}</th>

									<th>{{ getPhrase('type')}}</th>							 

									<th>{{ getPhrase('from_email')}}</th>

									<th>{{ getPhrase('from_name')}}</th>

									<th>{{ getPhrase('action')}}</th>								  

								</tr>

							</thead>							 

						</table>

						</div>

					</div>

				</div>
			</section>

		</div>

		<!-- /.container-fluid -->
	</section>

</div>

@endsection

@section('footer_scripts')

 @include('common.datatables', array('route'=>'emailtemplates.dataTable'))

 @include('common.deletescript', array('route'=>'/email/templates/delete/'))

@stop

