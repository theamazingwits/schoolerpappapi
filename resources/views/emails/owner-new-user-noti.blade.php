@include('emails.template_header')

 <div class="row">
    <div class="col-lg-12" style="margin:65px 0px;">
    <h5 class="text-center" style="font-size:20px;font-weight:600;">Alumni Registration was successfull</h5>
  </div>
  </div>
  
   
   <div class="row">
    <div class="col-lg-12">
      <p style="font-size:20px;margin:11px 0;">Dear {{$user_name}}, </p>
      <p style="font-size:20px;margin:11px 0;">Greetings,</p>
  <p style="font-size:20px;margin:11px 0;">New alumni registered with OES. Please approve the alumni</p>
  <p style="font-size:20px;margin:11px 0;"><a href="{{$link}}" class="btn btn-primary btn-sm">Click to login</a></p>
  
    <br>
  <p style="font-size:20px;margin:11px 0;">Alumni Login Details</p>
  <p style="font-size:20px;margin:11px 0;"><strong>Email:</strong> {{$email}}</p>
  <p style="font-size:20px;margin:11px 0;"><strong>Password:</strong> {{$password}}</p>
  <br><br>


  
<p style="font-size:20px;margin:11px 0;">Sincerely, </p>
<p style="font-size:20px;margin:11px 0;">Customer Support Services</p>

  </div>
   </div>





@include('emails.template_footer')