@extends($layout)
@section('content')
<div id="page-wrapper" ng-controller="TabController">
    <section id="main" class="main-wrap bgc-white-darkest" role="main">
        <div class="container-fluid content-wrap">
            <div class="row">
                <div class="col-lg-12">
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{PREFIX}}">
                                <i class="fa fa-home">
                                </i>
                            </a>
                        </li>


                        <li>

                            {{$title}}
                            
                        </li>
                    </ol>
                </div>
            </div>

            {!! Form::open(array('url' => URL_PRINT_STUDENT_ONLINE_MARKS, 'method' => 'POST', 'name'=>'htmlform ','target'=>'_blank', 'id'=>'htmlform', 'novalidate'=>'')) !!}

            <section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item " >
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                        <h2 class="pull-left">  {{getPhrase('select_details')}} </h2>   
                        <!--End panel icons-->
                    </div>
                    <div class="panel-body panel-body-p instruction">
                        @include('common.year-selection-view', array('class'=>'custom-row-6'))

                        <fieldset  class="form-group">
                           <label for = "online_exam_cateory_id">{{getPhrase('category')}}</label>
                           <select 
                           name      = "online_exam_cateory_id" 
                           id        = "online_exam_cateory_id" 
                           class     = "form-control" 
                           ng-model  = "online_exam_cateory_id" 
                           ng-change = "getStudentMarks112()"
                           ng-options= "option.id as option.title for option in quiz_categories track by option.id">
                           <option value="">{{getPhrase('select')}}</option>
                       </select>
                   </fieldset>


                   <hr>

                   <div ng-show="students.length>0" class="row">

                     <div class="col-sm-4 col-sm-offset-8">
                        <div class="input-group">
                            <input type="text" ng-model="search" class="form-control input-lg" placeholder="{{getPhrase('search')}}" name="search" />
                        </div>
                    </div>
                </div>
                <br>

                <div class="row vertical-scroll">
                    <h4 ng-if="result_data.students.length>0" >@{{course_title}}</h4>
                    <table ng-if="result_data.students.length>0" class="table table-bordered" style="border-collapse: collapse;">
                        <thead>
                            <th style="border:1px solid #000;text-align: center;">{{getPhrase('name')}}</th>

                            <th style="border:1px solid #000;text-align: center;">{{getPhrase('roll_no')}}</th>
                            <th style="border:1px solid #000;text-align: center;" ng-repeat="subject in subjects">@{{subject.subject_code}} (@{{subject.total_marks}})</th>

                            <th style="border:1px solid #000;text-align: center;" >Score</th>

                        </thead>
                        <tbody>
                            <tr ng-repeat="student in students | filter:search track by $index">
                                <td style="border:1px solid #000;" ng-click="printStudentData(student.user_id)">@{{student.name}}</td>

                                <td style="border:1px solid #000;">@{{student.roll_no}}</td>

                                <td style="border:1px solid #000; text-align: right;" ng-repeat="marks_record in student.marks">@{{marks_record.score.marks_obtained}}</td>
                                <td style="border:1px solid #000;text-align: right;" {{-- ng-if="student.course_type!='gpa'" --}}>

                                    <div class="progress" >
                                      <div  ng-class="{'progress-bar progress-bar-success':student.average>=75, 'progress-bar progress-bar-warning':student.average<75 && student.average>=50, 'progress-bar progress-bar-danger':student.average<50 && student.average>=0}" role="progressbar" aria-valuenow="@{{student.average}}"
                                          aria-valuemin="0" aria-valuemax="100" style="width:@{{student.average}}%">
                                          @{{student.average}}%
                                      </div>
                                  </div>




                              </td>

                              {{-- <td ng-if="student.course_type=='gpa'" style="border:1px solid #000; text-align: center;" ><strong>@{{student.grade_points}} GPA (@{{student.grade}} {{getPhrase('grade')}})</strong></td> --}}
                          </tr> 
                      </tbody>
                  </table>
              </div>
              <input type="hidden" name="studentid" id="studentid">

              <div ng-if="result_data.students.length<=0"  class="text-center" >{{getPhrase('no_data_available')}}</div> 
              <br>
              <a ng-if="result_data.students.length>0"  class="btn btn-primary pull-right panel-header-button" ng-click="printIt()">{{getPhrase('print_consolidate_report')}}</a>

              {!! Form::close() !!}

          </div>

        </div>
        </section>
    </div>
    </section>
</div>

@stop

@section('footer_scripts')

  
    @include('onlinemarks.scripts.js-script')
    
@stop