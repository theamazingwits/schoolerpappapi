@extends('layouts.admin.adminlayout')
@section('content')


<div id="page-wrapper" ng-controller = "TabController">
 <section id="main" class="main-wrap bgc-white-darkest" role="main">
    <div class="container-fluid content-wrap">
        <!-- Page Heading -->
        <div class="row">
           <div class="col-lg-12">
              <ol class="breadcrumb">
                 <li><a href="{{url('/')}}"><i class="fa fa-home"></i></a> </li>
                 <li><a  href="{{REPOTS_DASHBOARD}}">{{ getPhrase('reports_dashboard') }}</a></li>
                 <li>{{ $title }}</li>
             </ol>
         </div>
     </div>

     <!-- /.row -->
     <section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item " >
                <!--Start Panel-->
        <div class="panel bgc-white-dark">
            <div class="panel-body panel-body-p packages">

                <div class="row">
                   <fieldset class='col-sm-6'>
                       <label for="exampleInputEmail1">{{getPhrase('please_select')}}</label>
                       <div class="form-group row">
                          <div class="col-md-6">
                             <input type="radio" checked="checked" id="available" name="account" value="1" ng-model="account_available" ng-init="account_available=1; accountAvailable(1);" ng-click="accountAvailable(1)">
                             <label for="available"> <span class="fa-stack radio-button"> <i class="fa fa-check active"></i> </span>{{getPhrase('student_wise')}}</label>
                         </div>
                         <div class="col-md-6">
                             <input type="radio" id="not_available" name="account" value="0" ng-model="account_not_available" ng-click="accountAvailable(0)">
                             <label for="not_available"> <span class="fa-stack radio-button"> <i class="fa fa-check active"></i> </span>{{getPhrase('get_class_wise')}}</label>
                         </div>
                     </div>

                 </fieldset>
             </div>


             <!-- Class Wise					 -->



             <div ng-if = "student_select==0">


              {!! Form::open(array('url' => URL_VIEW_STUDENT_CONSOLIDATE_REPORT, 'method' => 'POST', 'name'=>'studentlist ','target'=>'_blank', 'id'=>'studentlist', 'novalidate'=>'')) !!}			



              @include('common.year-selection-view', array('class'=>'custom-row-6'))



              <hr>

              <div ng-show="result_data.length>0" class="row">

                 <div class="col-sm-4 col-sm-offset-8">
                    <div class="input-group">
                        <input type="text" ng-model="search" class="form-control input-lg" placeholder="{{getPhrase('search')}}" name="search" />
                    </div>
                </div>
            </div>
            <br>

            <div ng-if="result_data.length!=0">
             <div>

                <div class="row vertical-scroll">

                    <h4 ng-if="result_data[0].course_dueration<=1" style="text-align: center;"><u>@{{class_title}}</u></h4>
                    <h4 ng-if="result_data[0].course_dueration>1" style="text-align: center;"><u>@{{class_title_yer_sem}}</u></h4>

                    <table class="table table-bordered" style="border-collapse: collapse;">
                        <thead>
                            <th style="border:1px solid #000;text-align: center;"><b>{{getPhrase('sno')}}</b></th>
                            <th style="border:1px solid #000;text-align: center;"><b>{{getPhrase('name')}}</b></th>

                            <th style="border:1px solid #000;text-align: center;"><b>{{getPhrase('roll_no')}}</b></th>
                            <th style="border:1px solid #000;text-align: center;"><b>{{getPhrase('course')}}</b></th>
                            <th style="border:1px solid #000;text-align: center;"><b>{{getPhrase('consolidate_report')}}</b></th>


                        </thead>
                        <tbody>

                            <tr ng-repeat="user in result_data | filter:search track by $index">


                               <td style="border:1px solid #000;text-align: center;" >@{{$index+1}}</td>
                               <td style="border:1px solid #000;text-align: center;"><a target="_blank" href="{{URL_USER_DETAILS}}@{{user.slug}}">@{{user.name}}</a></td>

                               <td style="border:1px solid #000;text-align: center;">@{{user.roll_no}}</td>
                               <td style="border:1px solid #000;text-align: center;">@{{user.course_title}}</td>
                               <td style="border:1px solid #000;text-align: center;">
                                <a class="btn btn-primary panel-header-button" ng-click="viewStudentReport( user.user_id )">{{getPhrase('view')}}</a></td>




                            </tr> 

                        </tbody>
                    </table>
                </div>
            </div>

            <div ng-if="result_data.length==0" class="text-center" >{{getPhrase('no_data_available')}}</div> 
            <br>




            </div>

            <input type="hidden" name="user_id" id = "user_id">

            {!! Form::close() !!}

            </div>
            <!-- Student Wise -->

            <div ng-if = "student_select==1">

                {!! Form::open(array('url' => URL_VIEW_STUDENT_CONSOLIDATE_REPORT, 'method' => 'POST', 'name'=>'studentlist ','target'=>'_blank', 'id'=>'studentlist', 'novalidate'=>'')) !!}
                <div class="row">


                    <div class="col-md-6 col-md-offset-3">


                        <fieldset class="form-group">
                            {{ Form::label ('search', getphrase('search')) }}
                            <input type="text" class="form-control" name="search" ng-model="search" placeholder="{{'search'}}" ng-change="textChanged(search)">
                        </fieldset>


                    </div>

                </div>
                <hr>



                <div class="row vertical-scroll table-responsive" ng-if="users.length!=0" >
                    <table class="table">
                        <thead>
                            <th>{{getPhrase('image')}}</th>
                            <th>{{getPhrase('name')}}</th>
                            <th>{{getPhrase('roll_no')}}</th>
                            <th>{{getPhrase('admission_no')}}</th>
                            <th>{{getPhrase('class')}}</th>
                            <th>{{getPhrase('year')}}-{{getPhrase('semester')}}</th>
                            <th>{{getPhrase('consolidate_report')}}</th>
                        </thead>
                        <tbody>
                            <tr ng-repeat="myuser in users" id="@{{'selected_'+myuser.id}}" ng-click="getUserDetails(myuser)">
                                <td valign="middle">

                                    <img ng-if="myuser.image!=null && myuser.image!=''" class="thumb" src="{{IMAGE_PATH_PROFILE}}@{{myuser.image}}" height="60">

                                    <img ng-if="myuser.image==null || myuser.image==''" class="thumb" src="{{IMAGE_PATH_USERS_DEFAULT_THUMB}}">
                                </td>
                                <td valign="middle">@{{myuser.name}}</td>
                                <td valign="middle">@{{myuser.roll_no}}</td>
                                <td valign="middle">@{{myuser.admission_no}}</td>
                                <td valign="middle"> @{{myuser.academic_title+' '+myuser.course_title}} </td>
                                <td valign="middle"> @{{myuser.current_year}} 
                                    <span ng-if="myuser.current_semister!=0"> - @{{myuser.current_semister}}</span> 
                                </td>
                                <td>
                                    <a class="btn btn-primary panel-header-button"  ng-click="viewStudentReport( myuser.id )">{{getPhrase('print')}}</a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>


                    <div ng-if="users.length==0" class="text-center" >{{getPhrase('no_data_available')}}</div>               
                    <hr>

                    <input type="hidden" name="user_id" id = "user_id">

                    {!! Form::close() !!}

                </div>

            </div>
        </div>
    </section>
    <!-- /.container-fluid -->
    </div>
    </section>
</div>
@endsection
 

@section('footer_scripts')
  
 @include('onlinemarks.scripts.js-scripts1')

@stop
