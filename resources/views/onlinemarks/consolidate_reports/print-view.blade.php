@extends('layouts.admin.adminlayout')

@section('content')


<div id="page-wrapper">
			<div class="container-fluid">
			
				<div class="panel panel-custom">
					
					<div class="panel-body packages">
					
                       <div class="main">
                       	


                               
                               <div class="row">

									  <div style="border:4px solid #ddd;padding:10px 20px;border-radius:5px;">

									  	  <img src="{{IMAGE_PATH_SETTINGS.getSetting('watermark_image','certificate')}}" style="position: absolute;right: 0;top: 0;" width="100%" alt="">
									  	  
									  <div class="row" style="border:1px solid #ddd;margin: 0px -10px;">

									  <div class="col-md-12 col-sm-12 col-xs-12" style="margin:0px;padding:0px;">   
									  <table class="table">

									    <tbody>
										<tr>

										 <td class="text-left"><img src="{{IMAGE_PATH_SETTINGS.getSetting('site_logo', 'site_settings')}}" alt="{{getSetting('site_title','site_settings')}}" style="    width: 140px;"></td>

								<td class="text-center"><h4 class="text-center">{{getSetting('site_title','site_settings')}}</h4><span>{{getSetting('site_address','site_settings')}}</span>
									<p>{{getPhrase('phone')}} : {{getSetting('site_phone','site_settings')}}</p>
									<h4>CONSOLIDATE MARKS MEMO/CREDIT/GRADE SHEET</h4>
								</td>

						 <td class="text-right">{{getPhrase('date')}} : <strong><?php echo date('d/m/Y'); ?></strong></td>
                                          

										</tr>

										 <tr>
										   <td class="text-left">Reference NO:112233</td>

										   <td class="text-center">{{$parent_course_title}} : <strong>{{$course_title}}</strong></td>
										   <td class="text-right">Id:123456</td>
										 </tr>
										 
										<tr>
                                       
                                     @if($user_data->image)   
									<td  rowspan="3" class="text-left"><img src ={{IMAGE_PATH_PROFILE.$user_data->image}} style="width: 100px; margin: 8px 0 0 0px;" class="img-circle"></td>

									@else
									<td  rowspan="3" class="text-left"><img src ={{IMAGE_PATH_PROFILE_DEFAULT}} style="width: 100px; margin: 8px 0 0 0px;" class="img-circle"></td>

									@endif

							 <td class="text-center" >{{getPhrase('name')}} : <strong>{{$user_data->name}}</strong></td>


								<td rowspan="3" class="text-right"><?php  echo '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($student_data->roll_no, "C39") . '" alt="barcode"  style="width: 179px;
    height: 30px;display: inline-block;" />'; ?></td>
                                          
                       </tr>

                       <tr>
                       	<td class="text-center" style="border-top: 0px solid;" >{{getPhrase('roll_no')}} : <strong>{{$student_data->roll_no}}</strong></td>
                       </tr>

                        <tr>
                       	<td class="text-center" style="border-top: 0px solid;">{{getPhrase('year_of_admission')}} : <strong>{{$student_data->date_of_join}}</strong></td>
                       </tr>





										

										
										</tbody>

									  </table>
									  
									  
									  </div>

									 <div class="row">
                                  
                                  <!-- Previous Academic Marks  -->

                                        
                                    @if($marks_data)
                                             
                                     @foreach($marks_data as $key => $value)

                                     	 
                                       
									  <div class="col-md-6 col-sm-6 col-xs-12" style="height: 330px"> 

                                            <h4 class="text-center"><u>{{$course_titles[$key]}}</u></h4>

										    <table class="table table-striped">
										   <thead>
										      <tr>
										         <th><strong>{{getPhrase('subject_name')}}</strong></th>
										         <th><strong>{{getPhrase('offline_marks')}}</strong></th>
										         <th><strong>{{getPhrase('online_marks')}}</strong></th>
										         <th><strong>{{getPhrase('total_marks')}}</strong></th>
										      </tr>
										   </thead>
										   <tbody>
										   

										    @foreach($value as $key1 => $value1)      
										      <tr>
										         <th>{{ $value1['subject'] }}</th>

										         @if($value1['offline_marks']!= '-' || $value1['offline_marks']== 0)

										         <td class="text-right">{{ $value1['offline_marks'] }}</td>

										         @else

										         <td class="text-center" > - </td>

										         @endif

										         @if($value1['online_marks']!= '-' || $value1['online_marks']== 0)

										         <td class="text-right">{{ $value1['online_marks'] }}</td>

										         @else

										         <td class="text-center" > - </td>

										         @endif

										         @if($value1['offline_marks']!= '-' && $value1['online_marks'] != '-')

										         <td class="text-right">{{ $value1['offline_marks'] + $value1['online_marks'] }}</td>

										         @elseif($value1['offline_marks']== 0 || $value1['online_marks']== 0)

										         <td class="text-right">{{ $value1['offline_marks'] + $value1['online_marks'] }}</td>

										         @else

										          <td class="text-center" > - </td>

										         @endif

										      </tr>

										      @endforeach
										      
										   </tbody>
										
									

										</table>
						               </div>
                                           
                                  @endforeach
                                 
                                 @endif

                                 <!-- End of previous academic marks -->
                                 

                             <!-- Present Academic Marks -->
                            @if($current_data)
                                  <div class="col-md-6 col-sm-6 col-xs-12" style="height: 330px">  
                                  <h4 class="text-center"><u>{{$current_course_title[0]}}</u></h4> 
									    <table class="table table-striped">
									   <thead>
									      <tr>
							             <th><strong>{{getPhrase('subject_name')}}</strong></th>
								         <th><strong>{{getPhrase('offline_marks')}}</strong></th>
								         <th><strong>{{getPhrase('online_marks')}}</strong></th>
								         <th><strong>{{getPhrase('total_marks')}}</strong></th>
									      </tr>
									   </thead>
									   <tbody>
                                     @foreach($current_data[0] as $present_data)
									      <tr>
									         <th>{{$present_data['subject']}}</th>
									         <td>{{$present_data['offline_marks']}}</td>
									         <td>{{$present_data['online_marks']}}</td>
									      @if($present_data['offline_marks']!= '-' && $present_data['online_marks'] != '-')
									         <td>{{$present_data['offline_marks'] + $present_data['online_marks']}}</td>
									      @else
									       <td> - </td>
									      @endif   
									      </tr>

									 @endforeach 

									   </tbody>
									</table>
									  </div>
                              @endif
                                <!-- End of present academic marks -->

						                    </div>

											<br>
											<br>
						                    	  <div class="col-md-12 col-sm-12 col-xs-12" style="margin:0px;padding:0px;">   
									  <table class="table">

									    <tbody>
										<tr>

										 <td class="text-left">
										 	<img src="{{IMAGE_PATH_SETTINGS.getSetting('left_sign_image','certificate')}}" width="150" alt="">
										 	<br/><b style="font-size:16px;">{{getSetting('left_sign_name','certificate')}}</b>
										 	<br> <span style="font-size:14px; color:#aaa;">{{getSetting('left_sign_designation','certificate')}}</span>
										 </td>

										 <td class="text-center"></td>
										   <td class="text-right"><img src="{{IMAGE_PATH_SETTINGS.getSetting('right_sign_image','certificate')}}" width="150" alt="">

                        <br/><b style="font-size:16px;">{{getSetting('right_sign_name','certificate')}}</b>

                        <br> <span style="font-size:14px; color:#aaa;">{{getSetting('right_sign_designation','certificate')}}</span></td>
                                          

										</tr>

										 
										</tbody>

									  </table>
									  
									  
									  </div>
						              </div>
						          </div>
						  </div>
                          



					</div>
				</div>
			</div>
			<!-- /.container-fluid -->
		</div>
@endsection
 

@section('footer_scripts')
 

@stop
