<h1><b>{{$title}}</b></h1><br/>

<div class="row vertical-scroll">
  
    <table style="border-collapse: collapse;">
    <thead>
        <th style="border:1px solid #000;" >&nbsp;&nbsp;&nbsp;{{getPhrase('name')}}&nbsp;&nbsp;&nbsp;</th>
        <th style="border:1px solid #000;">&nbsp;&nbsp;&nbsp;{{getPhrase('roll_no')}}&nbsp;&nbsp;&nbsp;</th>
        
        @foreach($final_list['subjects'] as $subs)
        <th style="border:1px solid #000;">&nbsp;&nbsp;&nbsp;{{$subs['subject_code']}}({{$subs['total_marks']}})&nbsp;&nbsp;&nbsp;</th>
        @endforeach
     
        <th style="border:1px solid #000;">&nbsp;&nbsp;&nbsp;AVG.%&nbsp;&nbsp;&nbsp;</th>
        
        
    </thead>
    <tbody>
  @foreach($final_list['students'] as $student)
    <tr>

        <td style="border:1px solid #000;">&nbsp;&nbsp;&nbsp;{{$student['name']}}&nbsp;&nbsp;&nbsp;</td>
        <td style="border:1px solid #000;">&nbsp;&nbsp;&nbsp;{{$student['roll_no']}}&nbsp;&nbsp;&nbsp;</td>
         @foreach($student['marks'] as $mark)
        <td style="border:1px solid #000; text-align: right;">&nbsp;&nbsp;&nbsp;{{$mark['score']->marks_obtained }}&nbsp;&nbsp;&nbsp;</td>
         @endforeach

      
        <td style="border:1px solid #000;">&nbsp;&nbsp;&nbsp;{{$mark['score']->percentage }}&nbsp;&nbsp;&nbsp;</td>

       
       
       
    </tr> 
    @endforeach
    
    </tbody>
    </table>
</div>