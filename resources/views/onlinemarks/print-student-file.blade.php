<h1 style="text-align: center;"><b>{{$title}}</b></h1><br/>

<div class="row vertical-scroll" align="center">
  
    <table style="border-collapse: collapse;">
    <thead>
        <th style="border:1px solid #000;" >&nbsp;&nbsp;&nbsp;{{getPhrase('s_no')}}&nbsp;&nbsp;&nbsp;</th>
        <th style="border:1px solid #000;" >&nbsp;&nbsp;&nbsp;{{getPhrase('subject_title')}}&nbsp;&nbsp;&nbsp;</th>
        <th style="border:1px solid #000;">&nbsp;&nbsp;&nbsp;{{getPhrase('subject_code')}}&nbsp;&nbsp;&nbsp;</th>
        <th style="border:1px solid #000;">&nbsp;&nbsp;&nbsp;{{getPhrase('marks')}}&nbsp;&nbsp;&nbsp;</th>
       
        <th style="border:1px solid #000;">&nbsp;&nbsp;&nbsp;AVG.%&nbsp;&nbsp;&nbsp;</th>
        
        
    </thead>
    <tbody>
    <?php 
    $i = 1;
    $student  = array();
    foreach ($final_list['students'] as $mystudent) {
      if($mystudent['user_id']  == $user_id){
            $student   = $mystudent;
        }
     } 
   
    ?>
@foreach($student['marks'] as $mark)
    <tr>


        <td style="border:1px solid #000;">&nbsp;&nbsp;&nbsp;{{$i++}}&nbsp;&nbsp;&nbsp;</td>
        <td style="border:1px solid #000;">&nbsp;&nbsp;&nbsp;{{$mark['subject_title']}}&nbsp;&nbsp;&nbsp;</td>
        <td style="border:1px solid #000;">&nbsp;&nbsp;&nbsp;{{$mark['subject_code'] }}&nbsp;&nbsp;&nbsp;</td>
        <td style="border:1px solid #000;">&nbsp;&nbsp;&nbsp;{{$mark['score']->marks_obtained }}&nbsp;&nbsp;&nbsp;</td>
        
    
        <td style="border:1px solid #000;">&nbsp;&nbsp;&nbsp;{{$mark['score']->percentage }}&nbsp;&nbsp;&nbsp;</td>
     
       
    </tr> 

    @endforeach
    
    </tbody>
    </table>
</div>