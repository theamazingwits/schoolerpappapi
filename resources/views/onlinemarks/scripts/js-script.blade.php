@include('common.angular-factory')
<script>

 app.controller('TabController', function ($scope, $http, httpPreConfig)
  {
      @include('common.js-script-year-selection')
     $scope.tab             = 1;
      
      $scope.users = []; 
      $scope.exam_list      = []; 
      $scope.selected_user  = null;
      $scope.certificates_issued  = [];
      $scope.form_show = false;
      $scope.quiz_categories = [];
      $scope.subjects = [];
      $scope.students = [];
      $scope.course_title = '';
    


    

      $scope.doCall     = function () {

      academic_id          = $scope.selected_academic_id;
      parent_course_id     = $scope.selected_course_parent_id;
      course_id            = $scope.selected_course_id;
      
      year                 = $scope.selected_year;
      semister             = $scope.selected_semister;

       route   = '{{URL_STUDENT_CLASS_EXAM_CATEGORY_LIST}}';  
        data    = {   _method: 'post', 
                  '_token':httpPreConfig.getToken(), 
                  'academic_id': academic_id, 
                  'parent_course_id': parent_course_id,
                  'course_id': course_id,
                  'year': year,
                  'semister': semister,
               };
               
      httpPreConfig.webServiceCallPost(route, data).then(function(result){
        result = result.data;
        $scope.result_data = [];
        $scope.quiz_categories = [];
        $scope.online_exam_cateory_id =0;
        angular.forEach(result, function(value, key) {
          obj = {'title': value.category, 'id': value.id};
        
            $scope.quiz_categories.push(obj);
          });
        });
  
    }

     $scope.getStudentMarks112 = function(){
      academic_id          = $scope.selected_academic_id;
      parent_course_id     = $scope.selected_course_parent_id;
      course_id            = $scope.selected_course_id;
      
      year                 = $scope.selected_year;
      semister             = $scope.selected_semister;
      $scope.students=[];
      if(!$scope.online_exam_cateory_id)
        return;
      
      online_exam_cateory_id = $scope.online_exam_cateory_id;
       
      
       route   = '{{URL_STUDENT_CLASS_ONLINE_MARKS}}';  
        data    = {   _method: 'post', 
                  '_token':httpPreConfig.getToken(), 
                  'academic_id': academic_id, 
                  'parent_course_id': parent_course_id,
                  'course_id': course_id,
                  'year': year,
                  'semister': semister,
                  'online_exam_cateory_id': online_exam_cateory_id
               };
               
      httpPreConfig.webServiceCallPost(route, data).then(function(result){
        console.log(result.data);
        result = result.data;
        $scope.result_data = result;
 

        $scope.subjects = result.subjects;
        $scope.students = result.students;

        $scope.course_title = result.course_title;
      });
 }
 
 
 
 $scope.printIt = function(){
  
  $('#studentid').val($scope.studentid);
  $('#htmlform').submit();

 }

 $scope.printStudentData = function(user_id){
 $('#studentid').val(user_id);
  $('#htmlform').submit();
 }

 
});
 
  
</script>