@extends($layout)
@section('content')
<div id="page-wrapper" ng-controller="hostelAssign">
			<div class="container-fluid">
				<!-- Page Heading -->
				<div class="row">
					<div class="col-lg-12">
						<ol class="breadcrumb">
							<li><a href="/"><i class="mdi mdi-home"></i></a> </li>
							<li class="active">{{isset($title) ? $title : ''}}</li>
						</ol>
					</div>
				</div>
					@include('errors.errors')
				<!-- /.row -->
				
				<div class="panel panel-custom col-lg-12">
					<div class="panel-heading">
						
						
					<h1>{{ $title }}  </h1>
					</div>
					<div class="panel-body" >
					<?php 

					 $button_name = getPhrase('add_to_room');
					?>

					
				  {!! Form::open(array('url' => URL_STORE_STUDENT_HOSTEL_DETAILS, 'method' => 'POST', 'name'=>'formQuiz ', 'novalidate'=>'')) !!}
					

					 @include('hostel.hostelassign.form_elements', 
					 array('button_name'=> $button_name),
					 array(
					  
					  'hostels_list'=>$hostels_list,
					  'user'=>$user,
					  'details'=>$details,
					  'hoste_details'=>$hoste_details,
					  'years'=>$years,
					  'months'=>$months

					 ))
					 		
					{!! Form::close() !!}
					</div>

				</div>
			</div>
			<!-- /.container-fluid -->


			  <div id="hostelVacate" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" align="center"><b>{{getPhrase('vacate_from_hostel')}}</b></h4>
      </div>
      <div class="modal-body">

           {!!Form::open(array('url'=> URL_VACATE_FROM_HOSTEL,'method'=>'POST','name'=>'userstatus'))!!} 
       
        <h4 style="color: #ffa616;" align="center">{{getPhrase('are_you_sure_vacate_this_user_from_hostel')}}</h4>
        <input type="hidden" name="vacate_user_id" id="vacate_user_id">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>&nbsp;
        <button type="submit" class="btn btn-primary">Yes</button>
      </div>
      {!!Form::close()!!}
      
    </div>

  </div>
</div>

		</div>
		<!-- /#page-wrapper -->
@stop

@section('footer_scripts')
 {{-- @include('common.validations') --}}

 @include('hostel.scripts.student-scripts')

 <script>

 
      function VacateHoste(user_slug){
           // console.log(user_slug);
           $('#vacate_user_id').val(user_slug);
           $('#hostelVacate').modal('show');
        }
  </script>  
@stop
 
 