@extends($layout)

@section('header_scripts')

@stop

@section('content')

<div id="page-wrapper" ng-controller="TabController">
    <section id="main" class="main-wrap bgc-white-darkest" role="main">
    <div class="container-fluid content-wrap">
        <div class="row">
            <div class="col-lg-12">
                <ol class="breadcrumb">
                    <li>
                        <a href="{{PREFIX}}">
                            <i class="fa fa-home bc-home">
                            </i>
                        </a>
                    </li>
                     
                   
                    <li>{{$title}}</li>
                </ol>
            </div>
        </div>
    
     <div class="row panel-grid" id="panel-grid">
        <div class="col-sm-12 col-md-12 col-lg-12 panel-wrap panel-grid-item grid-stack-item">

            <div class="panel bgc-white-dark">
                <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
               
                     <h2 class="pull-left"> {{getPhrase('select_details')}}</h2>   
                             

                    <!--Start panel icons-->
                        <div class="panel-icons panel-icon-slide ">
                            <ul>
                                <li><a href=""><i class="fa fa-angle-left"></i></a>
                                    <ul>
                                        <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                                        <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                                        <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
                                        <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
                                        <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li>
                                       <!--  <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li> -->
                                    </ul>
                                </li>
                            </ul>
                        </div>
                                    <!--End panel icons-->

                </div>
                <div class="panel-body ml-1">

                    @if($is_multibranch)

                      @include('common.year-selection-view', array('class'=>'custom-row-6'))

                    @else  
                      @include('hostel.year-selection-view', array('class'=>'custom-row-6'))

                    @endif  


                       

                                           
                       <div ng-show="result_data.length>0" class="row">

                       <div class="col-sm-4 col-sm-offset-8">
                                <div class="input-group">
                                        <input type="text" ng-model="search" class="form-control input-lg" placeholder="{{getPhrase('search')}}" name="search" />
                                        <span class="input-group-btn">
                                            <button class="btn btn-primary btn-lg" type="button">
                                                <i class="glyphicon glyphicon-search"></i>
                                            </button>
                                        </span>
                                    </div>
                            </div>
                       </div>

                       <div ng-if="result_data.length!=0">
                       <div>
                       
                       <br>
                       


                        <div class="row vertical-scroll">

                        <h4 ng-if="result_data[0].course_dueration<=1" style="text-align: center;"><u>@{{class_title}}</u></h4>
                        <h4 ng-if="result_data[0].course_dueration>1" style="text-align: center;"><u>@{{class_title_yer_sem}}</u></h4>

                        <table class="table table-bordered" style="border-collapse: collapse;">
                        <thead>
                            <th style="border:1px solid #000;text-align: center;"><b>{{getPhrase('sno')}}</b></th>
                            <th style="border:1px solid #000;text-align: center;"><b>{{getPhrase('name')}}</b></th>
                            
                            <th style="border:1px solid #000;text-align: center;"><b>{{getPhrase('roll_no')}}</b></th>
                            <th style="border:1px solid #000;text-align: center;"><b>{{getPhrase('course')}}</b></th>
                            <th style="border:1px solid #000;text-align: center;"><b>{{getPhrase('assign_hostel')}}</b></th>
                            
                           
                        </thead>
                        <tbody>
                       
                        <tr ng-repeat="user in result_data | filter:search track by $index">

                        
                                 <td style="border:1px solid #000;text-align: center;" >@{{$index+1}}</td>
                                <td style="border:1px solid #000;text-align: center;">
                                    <a target="_blank" href="{{URL_USER_DETAILS}}@{{user.slug}}">@{{user.name}}</a>
                                </td>
                            
                            <td style="border:1px solid #000;text-align: center;">@{{user.roll_no}}</td>
                            <td style="border:1px solid #000;text-align: center;">@{{user.course_title}}</td>
                            <td style="border:1px solid #000;text-align: center;">

                                <a href="{{ URL_STUDENT_ASSIGN_HOSTEL }}@{{user.slug}}" class="btn btn-primary btn-sm" target="_blank">{{getPhrase('assign_hostel')}}</a>
                                
                            </td>
        
                        </tr> 
                     
                        </tbody>
                        </table>
                    </div>
                     </div>

                    <div ng-if="result_data.length==0" class="text-center" >{{getPhrase('no_data_available')}}</div> 
                    <br>
                    {{-- <a ng-if="result_data.length!=0" class="btn btn-primary" ng-click="printIt()">Print</a> --}}
                      </div>
                    </div>
                                
                           
                        
                    </hr>
                </div>
            </div>
            </div>
        </div>
    </div>


  </section>

</div>

 

@stop
 
 

@section('footer_scripts')

  @if($is_multibranch)
    @include('hostel.scripts.assign-scripts')
  @else
    @include('hostel.scripts.assign-scripts-2')
  
  @endif 

  
    
@stop