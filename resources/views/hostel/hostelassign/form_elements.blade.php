                    <div class="row">
                       
                       @if( $details > 0 )

                       

                    	<table class="table table-striped table-bordered datatable" cellspacing="0" width="100%">

                    		<thead>
                    			<th><b>{{getPhrase('hostel_name')}}</b></th>
                    			<th><b>{{getPhrase('room_number')}}</b></th>
                    			<th><b>{{getPhrase('room_type')}}</b></th>
                    			<th><b>{{getPhrase('cost')}}</b></th>
                    			<th><b>{{getPhrase('joined_on')}}</b></th>
                    			<th></th>
                    		</thead>
                    		<tbody>

                    			  @foreach($hoste_details as $hst_detail)
                    			<tr>
                    				<td>{{ $hst_detail->hostelname() }}</td>
                    				<?php
                    				  $hstldetails  = $hst_detail->roomname();
                    				?>
                    				<td>{{ $hstldetails['number'] }}</td>
                    				<td>{{ $hstldetails['type'] }}</td>
                    				<td>{{ getCurrencyCode() }} {{ $hstldetails['cost'] }}</td>
                    				<td>{{ $hst_detail->created_at }}</td>

                    				@if( $hst_detail->is_vacate == 0 && $hst_detail->is_active == 1 )
                    				
                                    <td><a href="javascript:void(0)" onclick="VacateHoste({{$user->id}})" class="btn btn-warning btn-sm">{{getPhrase('vacate_hostel')}}</a></td>

                                    @elseif($hst_detail->is_vacate == 1)

                                      <td><a href="javascript:void(0)" class="btn btn-info btn-sm"><b>{{getPhrase('vacated_on')}} {{$hst_detail->updated_at}}</b></a></td>

                    				@endif
                    			</tr>
                    	          @endforeach
                    		</tbody>
                    		
                    	</table>


                    	@endif

                    	<input type="hidden" name="user_id" value="{{$user->id}}">


            <div class="col-md-12">
                <fieldset class='col-sm-6'>
                <label for="exampleInputEmail1">{{ getPhrase('add_fee_record') }}</label>
                <div class="form-group row">
                    <div class="col-md-6">
                        <input type="radio" checked="checked" id="available" name="user_join_type" value="1" ng-model="account_available" ng-init="account_available=1; accountAvailable(1);" ng-click="accountAvailable(1)">
                        <label for="available"> <span class="fa-stack radio-button"> <i class="mdi mdi-check active"></i> </span> {{ getPhrase('no')}} </label>
                    </div>
                    <div class="col-md-6">
                        <input type="radio" id="not_available" name="user_join_type" value="0" ng-model="account_not_available" ng-click="accountAvailable(0)">
                        <label for="not_available"> <span class="fa-stack radio-button"> <i class="mdi mdi-check active"></i> </span> {{ getPhrase('yes')}} </label>
                    </div>
                </div>
            </fieldset>
          </div>
        

                    <fieldset class="form-group col-md-6">

							{{ Form::label('hostel_id', getphrase('hostel')) }}
							<span class="text-red">*</span>
							{{Form::select('hostel_id', $hostels_list, null, ['class'=>'form-control', 'id'=>'hostel_id',
								'placeholder'=>'Select',
								'ng-model'=>'hostel_id',
								"ng-change" => "getHostelRooms(hostel_id)",
								'required'=> 'true', 
								'ng-class'=>'{"has-error": formQuiz.hostel_id.$touched && formQuiz.hostel_id.$invalid}'
							])}}
							 <div class="validation-error" ng-messages="formQuiz.hostel_id.$error" >
		    					{!! getValidationMessage()!!}
							</div>

						
					</fieldset>

					   <fieldset ng-if="selected_hostel_id" class="form-group col-md-6">
                          <span class="text-red">*</span>
                           <label for = "room_id">{{getPhrase('rooms')}}</label>
                          <select 
                          name      = "room_id" 
                          id        = "room_id" 
                          class     = "form-control" 
                          ng-model  = "room_id" 
                          ng-options= "option.id as option.name for option in rooms track by option.id">
                          <option value="">{{getPhrase('select')}}</option>
                          </select>
                      </fieldset> 

                                <fieldset class="form-group col-md-4" ng-if="showMonth">

                        {{ Form::label('title', getphrase('fee_title')) }}

                        <span class="text-red">*</span>

                        {{ Form::text('title', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('title'),

                            'ng-model'=>'title', 
                            
                             'required'=> 'true', 

                            'ng-class'=>'{"has-error": formQuiz.title.$touched && formQuiz.title.$invalid}',

                            )) }}

                        <div class="validation-error" ng-messages="formQuiz.title.$error" >

                            {!! getValidationMessage()!!}
                            
                        
                        </div>

                    </fieldset> 

                         <fieldset class="form-group col-md-4" ng-if="showMonth">

                            {{ Form::label('year', getphrase('year')) }}
                            <span class="text-red">*</span>
                            {{Form::select('year', $years, null, ['class'=>'form-control', 'id'=>'year',
                                'placeholder'=>'Select',
                                'ng-model'=>'year',
                                'required'=> 'true', 
                                'ng-class'=>'{"has-error": formQuiz.year.$touched && formQuiz.year.$invalid}'
                            ])}}
                             <div class="validation-error" ng-messages="formQuiz.year.$error" >
                                {!! getValidationMessage()!!}
                            </div>

                        
          </fieldset>

            <fieldset class="form-group col-md-4" ng-if="showMonth">

                            {{ Form::label('month', getphrase('month')) }}
                            <span class="text-red">*</span>
                            {{Form::select('month', $months, null, ['class'=>'form-control', 'id'=>'month',
                                'placeholder'=>'Select',
                                'ng-model'=>'month',
                                'required'=> 'true', 
                                'ng-class'=>'{"has-error": formQuiz.month.$touched && formQuiz.month.$invalid}'
                            ])}}
                             <div class="validation-error" ng-messages="formQuiz.month.$error" >
                                {!! getValidationMessage()!!}
                            </div>

                        
          </fieldset>



				</div>

					   <div class="buttons text-center">

							<button class="btn btn-lg btn-primary button"

							ng-disabled='!formQuiz.$valid'>{{ $button_name }}</button>

						</div>