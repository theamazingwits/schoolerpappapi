@include('common.angular-factory')
<script>

 app.controller('hostelAssign', function ($scope, $http, httpPreConfig)
  {

   $scope.getHostelRooms = function(hostel_id){

            $scope.selected_hostel_id = hostel_id;

            route   = '{{ URL_GET_HOSTEL_ROOMS }}';  
            data    = {   
                   _method: 'post', 
                  '_token':httpPreConfig.getToken(), 
                  'hostel_id': hostel_id, 
               };

        httpPreConfig.webServiceCallPost(route, data).then(function(result){
          // console.log(result.data);
           $scope.rooms  = result.data;

       });
   }


      $scope.accountAvailable = function (availability)
      {

        
        if(!availability)
        {
          $scope.showYear = false;
          $scope.showMonth = true;
        }
        else {
          $scope.showMonth = false;
          $scope.showYear = true;
        }
      }


});

</script>