 					
 					@if(checkRole( ['owner']) && App\Hostel::isMultiBranch() )

					<div class="row">
 						<fieldset class="form-group col-md-12">
							{{ Form::label('branch_id', getphrase('branch')) }}
							<span class="text-red">*</span>
							{{Form::select('branch_id', $branches, null, ['class'=>'form-control', 'id'=>'branch_id',
								'ng-model'=>'branch_id',
								'required'=> 'true', 
								'ng-class'=>'{"has-error": formQuiz.branch_id.$touched && formQuiz.branch_id.$invalid}'
							])}}
							 <div class="validation-error" ng-messages="formQuiz.branch_id.$error" >
		    					{!! getValidationMessage()!!}
							</div>
						</fieldset>
 					</div>
 					@else 

						{{ Form::hidden('branch_id', getUserRecord()->branch_id, array('id'=>'branch_id')) }}
					@endif


                 <div class="row">

 					 <fieldset class="form-group col-md-12">

						{{ Form::label('name', getphrase('name')) }}

						<span class="text-red">*</span>

						{{ Form::text('name', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('name'),

							'ng-model'=>'name', 

							'ng-pattern'=>getRegexPattern('name'), 

							'required'=> 'true', 

							'ng-class'=>'{"has-error": formQuiz.name.$touched && formQuiz.name.$invalid}',

							'ng-minlength' => '4',

							'ng-maxlength' => '100',

							)) }}

						<div class="validation-error" ng-messages="formQuiz.name.$error" >

	    					{!! getValidationMessage()!!}

	    					{!! getValidationMessage('pattern')!!}

	    					{!! getValidationMessage('minlength')!!}

	    					{!! getValidationMessage('maxlength')!!}

						</div>

					</fieldset>	

					<fieldset class="form-group col-md-6">

						<?php $types = array('1' =>'Boys', '2' => 'Girls','3'=>'Combine' );?>

						{{ Form::label('type', getphrase('type')) }}

						<span class="text-red">*</span>

						{{Form::select('type', $types, null, ['class'=>'form-control'])}}
						

					</fieldset>

					<fieldset class="form-group col-md-6">

						{{ Form::label('intake', getphrase('intake')) }}

						<span class="text-red">*</span>

						{{ Form::number('intake', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => 
						getPhrase('200'),

							'ng-model'=>'intake',

							'required'=> 'true', 
							
							'ng-pattern' => getRegexPattern("phone"),

							'ng-class'=>'{"has-error": formQuiz.intake.$touched && formQuiz.intake.$invalid}',


						)) }}

						<div class="validation-error" ng-messages="formQuiz.intake.$error" >

	    					{!! getValidationMessage()!!}

	    					{!! getValidationMessage('phone')!!}

	    					{!! getValidationMessage('maxlength')!!}

						</div>

					</fieldset> 

					

					<fieldset class="form-group col-md-6">
						
						{{ Form::label('address', getphrase('address')) }}
						
						{{ Form::textarea('address', $value = null , $attributes = array('class'=>'form-control', 'rows'=>'5', 'placeholder' => 'Address','id'=>'address')) }}
					</fieldset>	

					<fieldset class="form-group col-md-6">
						
						{{ Form::label('description', getphrase('description')) }}
						
						{{ Form::textarea('description', $value = null , $attributes = array('class'=>'form-control', 'rows'=>'5', 'placeholder' => 'Description','id'=>'description')) }}
					</fieldset>	

					</div>


						<div class="buttons text-center">

							<button class="btn btn-lg btn-primary button"

							ng-disabled='!formQuiz.$valid'>{{ $button_name }}</button>

						</div>

		 