@extends($layout)

@section('header_scripts')
<link href="{{CSS}}bootstrap-datepicker.css" rel="stylesheet">  

@stop

@section('content')
<div id="page-wrapper" ng-controller="hostelAssign">
      <section id="main" class="main-wrap bgc-white-darkest" role="main">

    <div class="container-fluid content-wrap">
        <div class="row">
            <div class="">
                <ol class="breadcrumb">
                    <li>
                        <a href="{{PREFIX}}">
                            <i class="fa fa-home bc-home">
                            </i>
                        </a>
                    </li>
                    <li>{{$title}}
                    </li>
                </ol>
            </div>
        </div>


    
        
       {!! Form::open(array('url' => URL_PRINT_HOSTEL_FEE_DETAILS, 'method' => 'POST', 'name'=>'htmlform ','target'=>'_blank', 'id'=>'htmlform', 'novalidate'=>'')) !!}

        <div class="row panel-grid" id="panel-grid">
            <div class="col-sm-12 col-md-12 col-lg-12 panel-wrap panel-grid-item grid-stack-item">
            <div class="panel bgc-white-dark">
                <div class="panel-header  mll clearfix  panel-header-p bgc-white-dark panel-header-sm">
                    <h2 class="pull-left ">
                        {{getPhrase('select_details')}}
                    </h2>
                     <!--Start panel icons-->
                                <div class="panel-icons panel-icon-slide ">
                                    <ul>
                                        <li><a href=""><i class="fa fa-angle-left"></i></a>
                                            <ul>
                                                <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                                                <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                                                <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
                                                <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
                                                <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li>
                                               <!--  <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li> -->
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                
                </div>
                <div class="panel-body">


                     <div class="row" >

                         <fieldset class="form-group col-md-4">

                                        {{ Form::label('hostel_id', getphrase('hostel')) }}
                                        <span class="text-red">*</span>
                                        {{Form::select('hostel_id', $hostels_list, null, ['class'=>'form-control', 'id'=>'hostel_id',
                                            'placeholder'=>'Select',
                                            'ng-model'=>'hostel_id',
                                            "ng-change" => "getmonths(hostel_id)",
                                            'required'=> 'true', 
                                            'ng-class'=>'{"has-error": formQuiz.hostel_id.$touched && formQuiz.hostel_id.$invalid}'
                                        ])}}
                                         <div class="validation-error" ng-messages="formQuiz.hostel_id.$error" >
                                            {!! getValidationMessage()!!}
                                        </div>

                                    
                      </fieldset>
                      
                      <fieldset class="form-group col-md-4">

                                        {{ Form::label('year', getphrase('year')) }}
                                        <span class="text-red">*</span>
                                        {{Form::select('year', $years, null, ['class'=>'form-control', 'id'=>'year',
                                            'placeholder'=>'Select',
                                            'ng-model'=>'year',
                                            'required'=> 'true',
                                            "ng-change" => "getYearReports(hostel_id, year)", 
                                            'ng-class'=>'{"has-error": formQuiz.year.$touched && formQuiz.year.$invalid}'
                                        ])}}
                                         <div class="validation-error" ng-messages="formQuiz.year.$error" >
                                            {!! getValidationMessage()!!}
                                        </div>

                                    
                      </fieldset>

                        <fieldset class="form-group col-md-4" ng-if="showMonth == 1">

                                        {{ Form::label('month', getphrase('month')) }}
                                        <span class="text-red">*</span>
                                        {{Form::select('month', $months, null, ['class'=>'form-control', 'id'=>'month',
                                            'placeholder'=>'Select',
                                            'ng-model'=>'month',
                                            'required'=> 'true', 
                                            "ng-change" => "getMonthReports(hostel_id, year,month)",
                                            'ng-class'=>'{"has-error": formQuiz.month.$touched && formQuiz.month.$invalid}'
                                        ])}}
                                         <div class="validation-error" ng-messages="formQuiz.month.$error" >
                                            {!! getValidationMessage()!!}
                                        </div>

                                    
                      </fieldset>
                        
                    </div>

                     <h6>{{getPhrase('date_wise_reports')}}</h6>
                     <br>
                     <div class="row input-daterange" id="dp">
                          
                             <fieldset class="form-group col-md-6">

                                {{ Form::label('start_date', getphrase('date_from')) }}

                                {{ Form::text('start_date', $value = null , $attributes = array(

                                    'class'=>'input-sm form-control', 
                                    'ng-model'=>'start_date',
                                    'placeholder' => '2015/7/17'

                                    )) }}

                            </fieldset>

                            <fieldset class="form-group col-md-6">

                                {{ Form::label('end_date', getphrase('date_to')) }}

                                {{ Form::text('end_date', $value = null , $attributes = array(

                                    'class'=>'input-sm form-control',
                                     'ng-model'=>'end_date',
                                     "ng-change" => "getDateReports(start_date, end_date)",
                                     'placeholder' => '2015/7/17'

                                     )) }}

                            </fieldset>
                        </div>

                       

                    <br>
                           
                   <div ng-show="result_data.length>0" class="row">

                   <div class="col-sm-4 col-sm-offset-8">
                            <div class="input-group">
                                    <input type="text" ng-model="search" class="form-control input-lg" placeholder="{{getPhrase('search')}}" name="search" />
                                    <span class="input-group-btn">
                                        <button class="btn btn-primary btn-lg" type="button">
                                            <i class="glyphicon glyphicon-search"></i>
                                        </button>
                                    </span>
                                </div>
                        </div>
                   </div>
                   <br>

                   <div ng-if="result_data.length>0">
                   <div>
                   
                   <br>
                   
                <div class="row vertical-scroll">

                  

                    <table class="table table-bordered" style="border-collapse: collapse;">
                    <thead>
                        <th style="border:1px solid #000;text-align: center;"><b>{{getPhrase('name')}}</b></th>
                        <th style="border:1px solid #000;text-align: center;"><b>{{getPhrase('roll_no')}}</b></th>
                        <th style="border:1px solid #000;text-align: center;"><b>{{getPhrase('course')}}</b></th>
                        <th style="border:1px solid #000;text-align: center;"><b>{{getPhrase('room_number')}}</b></th>
                        <th style="border:1px solid #000;text-align: center;"><b>{{getPhrase('year')}}</b></th>
                        <th style="border:1px solid #000;text-align: center;"><b>{{getPhrase('month')}}</b></th>
                        <th style="border:1px solid #000;text-align: center;"><b>{{getPhrase('start_date')}}</b></th>
                        <th style="border:1px solid #000;text-align: center;"><b>{{getPhrase('end_date')}}</b></th>
                        <th style="border:1px solid #000;text-align: center;"><b>{{getPhrase('amount')}}</b></th>
                        <th style="border:1px solid #000;text-align: center;"><b>{{getPhrase('paid_amount')}}</b></th>
                        <th style="border:1px solid #000;text-align: center;"><b>{{getPhrase('balance')}}</b></th>
                        
                       
                    </thead>
                    <tbody>
                   
                    <tr ng-repeat="user in result_data | filter:search track by $index">

                    
                            <td style="border:1px solid #000;text-align: center;"><a target="_blank" href="{{URL_USER_DETAILS}}@{{user.slug}}">@{{user.name}}</a></td>
                        
                        <td style="border:1px solid #000;text-align: center;">@{{user.roll_no}}</td>
                        <td style="border:1px solid #000;text-align: center;">@{{user.course_title}}</td>
                        <td style="border:1px solid #000;text-align: center;">@{{user.room_number}}</td>
                        <td style="border:1px solid #000;text-align: center;">@{{user.year}}</td>
                        <td style="border:1px solid #000;text-align: center;">@{{user.month}}</td>
                        <td style="border:1px solid #000;text-align: center;">@{{user.start_date}}</td>
                        <td style="border:1px solid #000;text-align: center;">@{{user.end_date}}</td>
                        <td style="border:1px solid #000;text-align: center;">{{ getCurrencyCode() }} @{{user.amount}}</td>
                        <td style="border:1px solid #000;text-align: center;">{{ getCurrencyCode() }} @{{user.paid_amount}}</td>
                        <td style="border:1px solid #000;text-align: center;">{{ getCurrencyCode() }} @{{user.balance}}</td>
                       
                        
                      
                          
                    </tr> 
                 
                    </tbody>
                    </table>
                </div>
                 </div>

                <div ng-if="result_data.length==0" class="text-center" >{{getPhrase('no_data_available')}}</div> 
                <br>
                <a ng-if="result_data.length!=0" class="btn btn-primary" ng-click="printIt()">Print</a>
                  </div>
                </div>
                                
                           
                        
                        </hr>
                    </div>
                </div>
            </div>
         </div>
        </div>

      </section>

</div>

 
{!! Form::close() !!}

@stop
 
 

@section('footer_scripts')

  
    @include('hostel.reports.js-scripts')

    <script src="{{JS}}datepicker.min.js"></script>
    <script>
     
     $('.input-daterange').datepicker({
        autoclose: true,
        // startDate: "0d",
         format: '{{getDateFormat()}}',
    });

    </script>

    
@stop