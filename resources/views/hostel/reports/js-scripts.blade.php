@include('common.angular-factory')
<script>

 app.controller('hostelAssign', function ($scope, $http, httpPreConfig)
  {
   
   $scope.showMonth  = 1;
   $scope.selected_hostel_id  = 0;
   $scope.selected_year  = 0;

   $scope.getmonths = function(hostel_id){

            $scope.selected_hostel_id = hostel_id;

            route   = '{{ URL_GET_HOSTEL_MONTHS }}';  
            data    = {   
                   _method: 'post', 
                  '_token':httpPreConfig.getToken(), 
                  'hostel_id': hostel_id, 
               };

        httpPreConfig.webServiceCallPost(route, data).then(function(result){
          // console.log(result.data);
           $scope.showMonth  = result.data;

       });
   }

   $scope.getYearReports  = function(hostel_id, year_name){

           $scope.selected_year = year_name;

            route   = '{{ URL_GET_HOSTEL_YEAR_REPORTS }}';  
            data    = {   
                   _method: 'post', 
                  '_token':httpPreConfig.getToken(), 
                  'hostel_id': hostel_id, 
                  'year': year_name, 
               };

        httpPreConfig.webServiceCallPost(route, data).then(function(result){
          // console.log(result.data);
           $scope.result_data  = result.data.users;

       });
   }

    $scope.getMonthReports  = function(hostel_id, year_name,month_name){


            route   = '{{ URL_GET_HOSTEL_YEAR_REPORTS }}';  
            data    = {   
                   _method: 'post', 
                  '_token':httpPreConfig.getToken(), 
                  'hostel_id': hostel_id, 
                  'year': year_name, 
                  'month': month_name, 
               };

        httpPreConfig.webServiceCallPost(route, data).then(function(result){
          // console.log(result.data);
           $scope.result_data  = result.data.users;

       });
   }

    $scope.getDateReports  = function(start_date, end_date){


            route   = '{{ URL_GET_HOSTEL_YEAR_REPORTS }}';  
            data    = {   
                   _method: 'post', 
                  '_token':httpPreConfig.getToken(), 
                  'start_date': start_date, 
                  'end_date': end_date, 
                
               };

        httpPreConfig.webServiceCallPost(route, data).then(function(result){
          // console.log(result.data);
           $scope.result_data  = result.data.users;

       });
   }

   $scope.printIt = function(){
  dta = $('#printable_data').html();
  $('#html_data').val(dta);
  $('#htmlform').submit();
 }

});

</script>