 					@if(checkRole(['owner']) && App\Hostel::isMultiBranch())
					<div class="row">
 						<fieldset class="form-group col-md-12">
							{{ Form::label('branch_id', getphrase('branch')) }}
							<span class="text-red">*</span>
							{{Form::select('branch_id', $branches, null, ['class'=>'form-control', 'id'=>'branch_id',
								'ng-model'=>'branch_id',
								'required'=> 'true', 
								'ng-class'=>'{"has-error": formQuiz.branch_id.$touched && formQuiz.branch_id.$invalid}'
							])}}
							 <div class="validation-error" ng-messages="formQuiz.branch_id.$error" >
		    					{!! getValidationMessage()!!}
							</div>
						</fieldset>
 					</div>
 					@else 
						{{ Form::hidden('branch_id', getUserRecord()->branch_id, array('id'=>'branch_id')) }}
					@endif


					<div class="row">

 					 <fieldset class="form-group col-md-12">

						{{ Form::label('room_number', getphrase('room_number')) }}

						<span class="text-red">*</span>

						{{ Form::text('room_number', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('room_number'),

							'ng-model'=>'room_number', 


							'required'=> 'true', 

							'ng-class'=>'{"has-error": formQuiz.room_number.$touched && formQuiz.room_number.$invalid}',

							'ng-minlength' => '2',

							'ng-maxlength' => '100',

							)) }}

						<div class="validation-error" ng-messages="formQuiz.room_number.$error" >

	    					{!! getValidationMessage()!!}
                            
                            {!! getValidationMessage('minlength')!!}

	    					{!! getValidationMessage('maxlength')!!}

						</div>

					</fieldset>	

				    <fieldset class="form-group col-md-6">
							{{ Form::label('hostel_id', getphrase('hostel')) }}
							<span class="text-red">*</span>
							{{Form::select('hostel_id', $hostels_list, null, ['class'=>'form-control', 'id'=>'hostel_id',
								'ng-model'=>'hostel_id',
								'required'=> 'true', 
								'ng-class'=>'{"has-error": formQuiz.hostel_id.$touched && formQuiz.hostel_id.$invalid}'
							])}}
							 <div class="validation-error" ng-messages="formQuiz.hostel_id.$error" >
		    					{!! getValidationMessage()!!}
							</div>
						</fieldset>

						<fieldset class="form-group col-md-6">
							{{ Form::label('room_type_id', getphrase('room_type')) }}
							<span class="text-red">*</span>
							{{Form::select('room_type_id', $roomtype_list, null, ['class'=>'form-control', 'id'=>'room_type_id',
								'ng-model'=>'room_type_id',
								'required'=> 'true', 
								'ng-class'=>'{"has-error": formQuiz.room_type_id.$touched && formQuiz.room_type_id.$invalid}'
							])}}
							 <div class="validation-error" ng-messages="formQuiz.room_type_id.$error" >
		    					{!! getValidationMessage()!!}
							</div>
						</fieldset>


					<fieldset class="form-group col-md-6">

						{{ Form::label('beds', getphrase('number_of_beds')) }}

						<span class="text-red">*</span>

						{{ Form::number('beds', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => 
						getPhrase('5'),

							'ng-model'=>'beds',

							'required'=> 'true', 
							
                            'ng-class'=>'{"has-error": formQuiz.beds.$touched && formQuiz.beds.$invalid}',


						)) }}

						<div class="validation-error" ng-messages="formQuiz.beds.$error" >

	    					{!! getValidationMessage()!!}
                      </div>

					</fieldset> 
                      
                      	<fieldset class="form-group col-md-6">

						{{ Form::label('cost', getphrase('cost_per_bed')) }}

						<span class="text-red">*</span>

						{{ Form::number('cost', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => 
						getPhrase('5000'),

							'ng-model'=>'cost',

							'required'=> 'true', 
							
                            'ng-class'=>'{"has-error": formQuiz.cost.$touched && formQuiz.cost.$invalid}',


						)) }}

						<div class="validation-error" ng-messages="formQuiz.cost.$error" >

	    					{!! getValidationMessage()!!}
                      </div>

					</fieldset> 

					

				
					<fieldset class="form-group col-md-12">
						
						{{ Form::label('description', getphrase('description')) }}
						
						{{ Form::textarea('description', $value = null , $attributes = array('class'=>'form-control', 'rows'=>'5', 'placeholder' => 'Description','id'=>'description')) }}
					</fieldset>	

					</div>


						<div class="buttons text-center">

							<button class="btn btn-lg btn-primary button"

							ng-disabled='!formQuiz.$valid'>{{ $button_name }}</button>

						</div>

		 