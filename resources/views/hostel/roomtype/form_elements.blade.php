 					@if(checkRole(['owner']) && App\Hostel::isMultiBranch())
					<div class="row">
 						<fieldset class="form-group col-md-12">
							{{ Form::label('branch_id', getphrase('branch')) }}
							<span class="text-red">*</span>
							{{Form::select('branch_id', $branches, null, ['class'=>'form-control', 'id'=>'branch_id',
								'ng-model'=>'branch_id',
								'required'=> 'true', 
								'ng-class'=>'{"has-error": formQuiz.branch_id.$touched && formQuiz.branch_id.$invalid}'
							])}}
							 <div class="validation-error" ng-messages="formQuiz.branch_id.$error" >
		    					{!! getValidationMessage()!!}
							</div>
						</fieldset>
 					</div>
 					@else 
						{{ Form::hidden('branch_id', getUserRecord()->branch_id, array('id'=>'branch_id')) }}
					@endif


					<div class="row">

 					 <fieldset class="form-group col-md-12">

						{{ Form::label('room_type', getphrase('room_type')) }}

						<span class="text-red">*</span>

						{{ Form::text('room_type', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('room_type'),

							'ng-model'=>'room_type', 


							'required'=> 'true', 

							'ng-class'=>'{"has-error": formQuiz.room_type.$touched && formQuiz.room_type.$invalid}',

							'ng-minlength' => '4',

							'ng-maxlength' => '100',

							)) }}

						<div class="validation-error" ng-messages="formQuiz.room_type.$error" >

	    					{!! getValidationMessage()!!}


	    					{!! getValidationMessage('minlength')!!}

	    					{!! getValidationMessage('maxlength')!!}

						</div>

					</fieldset>	

					

					<fieldset class="form-group col-md-12">
						
						{{ Form::label('description', getphrase('description')) }}
						
						{{ Form::textarea('description', $value = null , $attributes = array('class'=>'form-control', 'rows'=>'5', 'placeholder' => 'Description','id'=>'description')) }}
					</fieldset>	

					</div>


						<div class="buttons text-center">

							<button class="btn btn-lg btn-primary button"

							ng-disabled='!formQuiz.$valid'>{{ $button_name }}</button>

						</div>

		 