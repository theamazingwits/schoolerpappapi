@extends($layout)
@section('content')

<div id="page-wrapper" ng-controller="feePayController">
  <section id="main" class="main-wrap bgc-white-darkest" role="main">

            <div class="container-fluid content-wrap">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <ol class="breadcrumb">
                            <li><a href="{{PREFIX}}"><i class="fa fa-home bc-home"></i></a> </li>
                            <li>{{ $title }}</li>
                        </ol>
                    </div>
                </div>

                @include('errors.errors')
                                
                <!-- /.row -->

                <div class="row panel-grid" id="panel-grid">
                    <div class="col-sm-12 col-md-12 col-lg-12 panel-wrap panel-grid-item grid-stack-item">

                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                        
                       
                        <h2 class="pull-left">{{ $title }}</h2>

                         <!--Start panel icons-->
                                <div class="panel-icons panel-icon-slide ">
                                    <ul>
                                        <li><a href=""><i class="fa fa-angle-left"></i></a>
                                            <ul>
                                                <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                                                <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                                                <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
                                                <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
                                                <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li>
                                               <!--  <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li> -->
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                <!--End panel icons-->

                    </div>
                    <div class="panel-body">
                         {!! Form::open(array('url' => URL_ADD_HOSTEL_FEE, 'method' => 'POST','name'=>'formSchedule', 'novalidate'=>'')) !!}
                        <div class="row">

                        <fieldset class="form-group col-md-4">

                          {{ Form::label('hostel_id', getphrase('hostels')) }}

                          <span class="text-red">*</span>

                          {{Form::select('hostel_id', $hostels_list, null, ['class'=>'form-control', 'id'=>'hostel_id',
                            'ng-model'=>'hostel_id',
                            'placeholder'=>'Select',
                            "ng-change" => "getHostelRooms(hostel_id)",
                            'required'=> 'true', 
                            'ng-class'=>'{"has-error": formQuiz.hostel_id.$touched && formQuiz.hostel_id.$invalid}'
                          ])}}

                           <div class="validation-error" ng-messages="formQuiz.hostel_id.$error" >
                              {!! getValidationMessage()!!}
                          </div>

                        </fieldset>

                       <fieldset ng-if="selected_hostel_id" class="form-group col-md-4">
                          <span class="text-red">*</span>
                           <label for = "room_id">{{getPhrase('rooms')}}</label>
                          <select 
                          name      = "room_id" 
                          id        = "room_id" 
                          class     = "form-control" 
                          ng-model  = "room_id" 
                          ng-change = "getRoomUsers(hostel_id,room_id)",
                          ng-options= "option.id as option.name for option in rooms track by option.id">
                          <option value="">{{getPhrase('select')}}</option>
                          </select>
                      </fieldset> 

                        <fieldset ng-if="selected_room_id" class="form-group col-md-4">
                          <span class="text-red">*</span>

                           <label for = "user_id">{{getPhrase('students')}}</label>
                          <select 
                          name      = "user_id" 
                          id        = "user_id" 
                          class     = "form-control" 
                          ng-model  = "user_id" 
                          ng-change = "getStuduntFeeDetails( hostel_id, user_id )"
                          ng-options= "option.id as option.name for option in students track by option.id">
                          <option value="">{{getPhrase('select')}}</option>

                          </select>

                      </fieldset> 

                      <input type="hidden" name="user_id" value="@{{student_data.id}}" id="user_id">
                  
                  </div>

                  <div class="row" ng-if="selected_studentid">
                    <div class="col-md-12">
                    <div class="btn btn-primary panel-btn collapsed" data-toggle="collapse" data-target="#student_details_box">{{getPhrase('student_details')}} <span class="dc-caret">
                   <i class="fa fa-angle-down" aria-hidden="true"></i></span></div>

                      <div class="collapse panel-expand-box" id="student_details_box">
                      <div class="row">
                          <div class="col-md-2">
                               <div class="profile-details text-center" ng-if="student_data.image==''">
                            <div class="profile-img"><img src="{{IMAGE_PATH_PROFILE_DEFAULT}}" alt=""  style="width: 100px;height: 100px;"></div>
                        </div>
                            <div class="profile-details text-center" ng-if="student_data.image!=''">
                            <div class="profile-img"><img src="{{IMAGE_PATH_PROFILE}}@{{student_data.image}}" alt=""  style="width: 100px;height: 100px;"></div>
                        </div>
                             <b class="text-center" style="display: block;"> 
                             @{{student_data.name | uppercase}}
                             </b>
                        </div>
                          
                          <div class="col-md-10">
                             <div class="row">
                                
                                 <div class="col-md-6">
                                      <table class="table panel-table">
                                         <tbody>
                                            
                                             <tr>
                                                 <th>{{getPhrase('email')}}</th>
                                                 <td>@{{student_data.email}}</td>
                                             </tr>
                                             <tr>
                                                 <th>{{getPhrase('phone')}}</th>
                                                 <td>@{{student_data.phone}} </td>
                                             </tr>
                                         </tbody>
                                     </table>
                                 </div>
                             </div>
                          </div>
                      </div>
                      </div>
                      </div>
                  </div>
          
                  <br>
                  {{-- Fee History and Add Discount To Student --}}
                  <div class="row" ng-if="selected_studentid">
                    <div class="col-md-12">
                       <div class="btn btn-primary panel-btn collapsed" data-toggle="collapse" data-target="#fee_history">{{getPhrase('fee_paid_history_and_add_discount')}} <span class="dc-caret">&nbsp;
                          <i class="fa fa-angle-down" aria-hidden="true"></i></span>
                        </div>

                        <div class="collapse panel-expand-box" id="fee_history">
                           <div class="row">

                              <div class="col-md-8">
                                  <table class="table panel-table">
                                    <thead>
                                      <th><b>Title</b></th>
                                      <th><b>Month - Year</b></th>
                                      <th><b>{{getPhrase('date')}}</b></th>
                                      <th><b>{{getPhrase('amount_to_pay')}}</b></th>
                                      <th><b>{{getPhrase('discount')}}</b></th>
                                      <th><b>{{getPhrase('paid_amount')}}</b></th>
                                      <th><b>{{getPhrase('balance')}}</b></th>
                                      
                                    </thead>
                                    <tbody>
                                      <tr ng-repeat="record in feerecords_data | filter:search track by $index" ng-if="feerecords_data.length > 0">

                                        <td>@{{record.title}}</td>
                                        <td>@{{record.month}} - @{{record.year}}</td>
                                        <td ng-if="record.paid_date!= null">@{{record.paid_date}}</td>
                                        <td ng-if="record.paid_date == null">-</td>
                                        <td>{{$currency}} @{{record.amount}}</td>
                                        <td>{{$currency}} @{{record.discount}}</td>
                                        <td>{{$currency}} @{{record.paid_amount}}</td>
                                        <td>{{$currency}} @{{record.balance}}</td>


                                      </tr>
                                      <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td><b>{{$currency}} @{{total_pay}}</b></td>
                                        <td><b>{{$currency}} @{{total_discount}}</b></td>
                                        <td><b>{{$currency}} @{{total_paid}}</b></td>
                                        <td><b>{{$currency}} @{{total_balance}}</b></td>
                                        
                                      </tr>

                                      <tr ng-if="feerecords_data.length == 0">
                                        <td></td>
                                        <td></td>
                                        <td>{{getPhrase('no_data_available')}}</td>
                                        <td></td>
                                        <td></td>
                                      </tr>

                                    </tbody>
                                   
                                 </table>
                                
                              </div>

                              <div class="col-md-4">

                                 

                                  
                                    <p><strong>Amount to Pay : </strong>{{$currency}}  @{{total_amount_pay | currency : '' : 2 }}</p>
                                  
                                   
                                  

                                     <fieldset class="form-group" ng-if="total_amount_pay > 0">
            
                                      {{ Form::label('discount', getphrase('add_discount_amount')) }}
                                      
                                       {{ Form::number('discount', null, 
                                      ['class'=>'form-control',
                                      "id"=>"discount", 
                                      'required'=> 'true',
                                      'min'=>'0',
                                    
                                        ])}} 

                                    </fieldset>


                                     <div ng-if="total_amount_pay > 0"  class="buttons text-center" >

                                          <a class="btn btn-sm btn-primary" href="javascript:void(0)" onclick="showConfirm()">{{ getPhrase('add_discount') }}</a>
                                       
                                  </div>

                                
                                
                              </div>
                         
                          
                         
                          </div>
                       </div>
                     </div>
                  </div>

                  <br>

                  <div class="row">
                   <div class="col-md-12">
                  <?php 

                    $minimum_percentage = 100; 

                  ?>
          
                  </div>
                  </div>
                 

                 <div class="row" ng-if="selected_studentid">
                  <div class="col-md-12">
                    <table class="table table-bordered">
                      <tbody>
                        <tr>
                          <td><strong>{{getphrase('payment_mode')}}</strong></td>
                          <td> 
                           
                            {{ Form::select('payment_mode', $payment_ways, null, 
                            ['class'=>'form-control',
                            "id"=>"payment_mode", 
                            "ng-model"=>"payment_mode", 
                            'required'=> 'true',
                            'ng-init' =>'payment_mode="cash"',
                             
                             'ng-class'=>'{"has-error": formSchedule.payment_mode.$touched && formSchedule.payment_mode.$invalid}',
                         ])}}
                          </td>
                          </tr>
                        
                      

                        <tr>
                          <td><strong>Amount to Pay</strong></td>
                          <td><strong>{{$currency}} @{{total_amount_pay | currency : '' : 2 }}</strong></td>
                        </tr>

                    
                         <tr>
                          <td><strong>Total Amount</strong></td>
                          <td><strong>{{$currency}} @{{total_amount_pay | currency : '' : 2 }}</strong></td>
                        </tr>

                 
                        <tr>
                          <td><strong>Enter amount</strong></td>
                          <td><strong>
                            <input autofocus="true" ng-model="paid_amount" ng-change="validateAmount(final_pay, paid_amount,{{$minimum_percentage}})" type="number" name="pay_amount" min="0">
                          </strong>
                        </td>
                        </tr>
                        <tr>
                          <td><strong>Notes</strong></td>
                          <td><textarea name="notes" class="form-control"></textarea></td>
                        </tr>
                      </tbody>
                      
                    </table>
                  </div>
                  </div>



             

                     <div ng-if="total_amount_pay > 0"  class="buttons text-center" >
                            <button class="btn btn-lg btn-success button">{{ getPhrase('pay_now') }}</button>
                         
                    </div>

                         {!! Form::close() !!}

                  </div>

                </div>
              </div>
            </div>
            </div>
            <!-- /.container-fluid -->

                        <!-- Modal -->
            <div id="myModal" class="modal fade" role="dialog">
              <div class="modal-dialog modal-sm" style="width: 600px;">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" align="center">{{getPhrase('add_discount')}}</h4>
                  </div>
                  <div class="modal-body">

                    {!! Form::open(array('url' => URL_ADD_DISCOUNT_TO_STUDENT, 'method' => 'POST','name'=>'formDiscount','id'=>'formDiscount','novalidate'=>'')) !!}


                    <h4 align="center">{{getPhrase('are_you_sure_to_add_discount_for_student')}}</h4>

                        <textarea name="comments" rows="5" cols="75" placeholder="Comments"></textarea>

                        <input type="hidden" name="user_discount" id="user_discount" value="0">

                        <input type="hidden" name="userid" id="userid" value="0">

                        <input type="hidden" name="user_hostel_id" id="user_hostel_id" value="0">

                        <input type="hidden" name="user_room_id" id="user_room_id" value="0">

                   </div>

                  

                  <div class="modal-footer">
                    <button type="submit" class="btn btn-primary pull-right" >{{getPhrase('yes')}}</button>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{getPhrase('no')}}</button>
                  </div>
                </div>
                 {!! Form::close() !!}

              </div>
            </div>
    </section>

 </div>
    @endsection
 


@section('footer_scripts')


<script>

  function showConfirm() {

       var mydiscount  = $('#discount').val();

       var userid      = $('#user_id').val();

       $('#user_discount').val(mydiscount);

       $('#userid').val(userid);

       $('#myModal').modal('show');

  }

 
 </script>

 @include('hostel.payfee.js-scripts')


 
@stop
