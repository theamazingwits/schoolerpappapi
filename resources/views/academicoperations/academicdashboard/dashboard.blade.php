@extends($layout)
@section('content')


				<section id="main" class="main-wrap bgc-white-darkest" role="main">
			<div class="container-fluid content-wrap">
				<div class="row panel-grid grid-stack">

					<section data-gs-min-width="3" data-gs-min-height="20" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-body pt-2 grid-stack-handle">
                                <div class="h-lg pos-r panel-body-p py-0">
									<h3 class=""><span class="fs-3 fw-light">{{ getPhrase('certificates')}}</span> <i class="pull-right fa fa-certificate fs-1 m-2"></i></h3>
									<br><br><br><br>
                                    <a href="{{URL_CERTIFICATES_DASHBOARD}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
									
                                </div>
                                <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80" sparkLineColor="#fafafa"  sparkFillColor="#97d881" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>
					


					<section data-gs-min-width="3" data-gs-min-height="20" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-body pt-2 grid-stack-handle">
                                <div class="h-lg pos-r panel-body-p py-0">
									<h3 class=""><span class="fs-3 fw-light">{{ getPhrase('student_transfers')}}</span> <i class="pull-right fa fa-exchange fs-1 m-2"></i></h3>
									<br><br><br><br>
                                    <a href="{{URL_STUDENT_TRANSFERS}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
								
                                </div>
                                    <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80" sparkLineColor="#fafafa"  sparkFillColor="#f74948" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>
					


					<section data-gs-min-width="3" data-gs-min-height="20" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-body pt-2 grid-stack-handle">
                                <div class="h-lg pos-r panel-body-p py-0">
									<h3 class=""><span class="fs-3 fw-light">{{ getPhrase('timetable')}}</span> <i class="pull-right fa fa-calendar fs-1 m-2"></i></h3>
									<br><br><br><br>
                                    <a href="{{URL_TIMETABLE_DASHBOARD}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
									
                                </div>
                                <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80" sparkLineColor="#fafafa"  sparkFillColor="#353f4d" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>
					


					<section data-gs-min-width="3" data-gs-min-height="20" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-body pt-2 grid-stack-handle">
                                <div class="h-lg pos-r panel-body-p py-0">
									<h3 class=""><span class="fs-3 fw-light">{{ getPhrase('offline_exams')}}</span> <i class="pull-right fa fa-external-link fs-1 m-2"></i></h3>
									<br><br><br><br>
                                    <a href="{{URL_OFFLINE_EXAMS}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
									
                                </div>
                                <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80" sparkLineColor="#fafafa"  sparkFillColor="#f3d74c" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>
					

					<section data-gs-min-width="3" data-gs-min-height="20" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-body pt-2 grid-stack-handle">
                                <div class="h-lg pos-r panel-body-p py-0">
									<h3 class=""><span class="fs-3 fw-light">{{ getPhrase('class_attendance_report')}}</span> <i class="pull-right fa fa-check-square-o fs-1 m-2"></i></h3>
									<br><br><br><br>
                                    <a href="{{URL_STUDENT_CLASS_ATTENDANCE}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
								
                                </div>
                                    <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80" sparkLineColor="#fafafa"  sparkFillColor="#aaaaaa" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>
					

					<section data-gs-min-width="3" data-gs-min-height="20" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-body pt-2 grid-stack-handle">
                                <div class="h-lg pos-r panel-body-p py-0">
									<h3 class=""><span class="fs-3 fw-light">{{ getPhrase('class_marks_report')}}</span> <i class="pull-right fa fa-line-chart fs-1 m-2"></i></h3>
									<br><br><br><br>
                                    <a href="{{URL_STUDENT_MARKS_REPORT}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
								
                                </div>
                                    <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80" sparkLineColor="#fafafa"  sparkFillColor="#97d881" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>
					

					<section data-gs-min-width="3" data-gs-min-height="20" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-body pt-2 grid-stack-handle">
                                <div class="h-lg pos-r panel-body-p py-0">
									<h3 class=""><span class="fs-3 fw-light">{{ getPhrase('students_list_class_wise')}}</span> <i class="pull-right fa fa-users fs-1 m-2"></i></h3>
									<br><br><br><br>
                                    <a href="{{URL_STUDENT_LIST}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
									
                                </div>
                                <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80" sparkLineColor="#fafafa"  sparkFillColor="#d2ae35" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>
					

					<section data-gs-min-width="3" data-gs-min-height="20" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-body pt-2 grid-stack-handle">
                                <div class="h-lg pos-r panel-body-p py-0">
									<h3 class=""><span class="fs-3 fw-light">{{ getPhrase('course_completed_students')}}</span> <i class="pull-right fa fa-graduation-cap fs-1 m-2"></i></h3>
									<br><br>
                                    <a href="{{URL_STUDENT_COMPLETED_LIST}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
									
                                </div>
                                <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80" sparkLineColor="#fafafa"  sparkFillColor="#ead5eb" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>
					

					<section data-gs-min-width="3" data-gs-min-height="20" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-body pt-2 grid-stack-handle">
                                <div class="h-lg pos-r panel-body-p py-0">
									<h3 class=""><span class="fs-3 fw-light">{{ getPhrase('detained_students_list_class_wise')}}</span> <i class="pull-right fa fa-user-circle-o fs-1 m-2"></i></h3>
									<br><br><br>
                                    <a href="{{URL_STUDENT_DETAINED_LIST}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
									
                                </div>
                                <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80" sparkLineColor="#fafafa"  sparkFillColor="#f74948" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>
					

					<section data-gs-min-width="3" data-gs-min-height="20" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-body pt-2 grid-stack-handle">
                                <div class="h-lg pos-r panel-body-p py-0">
									<h3 class=""><span class="fs-3 fw-light">{{ getPhrase('fee_paid_history_for_class_wise')}}</span> <i class="pull-right fa fa-money fs-1 m-2"></i></h3>
									<br><br><br>
                                    <a href="{{URL_CLASS_WISE_FEE_PAID_HISTORY}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
									
                                </div>
                                <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80" sparkLineColor="#fafafa"  sparkFillColor="#46aaf9" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
                            </div>

                        </div>
                        <!--End Panel-->
                    </section>
				
                 

		</div>
		</div>
	</section>


		<!-- /#page-wrapper -->

@stop

@section('footer_scripts')
 
 

@stop
