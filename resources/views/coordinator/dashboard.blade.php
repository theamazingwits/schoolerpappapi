@extends($layout)
@section('header_scripts')
 {!! Charts::assets() !!}
@stop

@section('content')

{{-- User Certificates --}}
<?php
    
   $crecords        = App\UserCertificates::DocumetsSubmitted();
   $user_submites   = App\UserCertificates::where('user_id',Auth::user()->id)
                                           ->where('is_submitted',1)
                                           ->orWhere('is_approved',1)
                                           ->groupby('notification_id')
                                           ->pluck('notification_id')
                                           ->toArray();

?>  

@if($crecords)                                         

   @if(count($user_submites) > 0 )

   	  @foreach ($crecords as $key => $value)
   	     
         @if(in_array($value['id'], $user_submites))

         @else

           <div class="alert alert-danger">
			  <strong>Note:</strong> {{$value['description']}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="{{URL_UPLOAD_USER_CERTIFICATES.$value['id']}}" class="btn btn-primary btn-sm">Click here to upload</a>
			</div>

         @endif

   	   
      @endforeach

   @else
     
       @foreach ($crecords as $crecord)
   	      
          <div class="alert alert-danger">
			  <strong>Note:</strong> {{$crecord['description']}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="{{URL_UPLOAD_USER_CERTIFICATES.$crecord['id']}}" class="btn btn-primary btn-sm">Click here to upload</a>
			</div>

      @endforeach

  @endif 

@endif 

<div id="page-wrapper">
			<div class="container-fluid">
			<div class="row">
					<div class="col-lg-12">
						<ol class="breadcrumb">
							 
							<li><i class="fa fa-home"></i> {{ $title}}</li>
						</ol>
					</div>
				</div>

				 <div class="row">
				 
				      <div class="col-md-3">
						<div class="card card-blue text-xs-center helper_step1">
							<div class="card-block">
					  <h4 class="card-title">
					  <i class="fa fa-users"></i>
					  </h4>
					  <p class="card-text">{{ getPhrase('users')}}</p>
							</div>
							<a class="card-footer text-muted" 
							href="{{URL_ALUMNI_USERS}}">
								{{ getPhrase('view_all')}}
							</a>
						</div>
					</div>

					 <div class="col-md-3">
						<div class="card card-green text-xs-center helper_step1">
							<div class="card-block">
					  <h4 class="card-title">
					  <i class="fa fa-etsy"></i>
					  </h4>
					  <p class="card-text">{{ getPhrase('events')}}</p>
							</div>
							<a class="card-footer text-muted" 
							href="{{URL_ALUMNI_EVENTS}}">
								{{ getPhrase('view_all')}}
							</a>
						</div>
					</div>



					 <div class="col-md-3">
						<div class="card card-red text-xs-center helper_step1">
							<div class="card-block">
					  <h4 class="card-title">
					  <i class="fa fa-language"></i>
					  </h4>
					  <p class="card-text">{{ getPhrase('stories')}}</p>
							</div>
							<a class="card-footer text-muted" 
							href="{{URL_ALUMNI_STORIES}}">
								{{ getPhrase('view_all')}}
							</a>
						</div>
					</div>

					 <div class="col-md-3">
						<div class="card card-default text-xs-center helper_step1">
							<div class="card-block">
					  <h4 class="card-title">
					  <i class="fa fa-picture-o"></i>
					  </h4>
					  <p class="card-text">{{ getPhrase('gallery')}}</p>
							</div>
							<a class="card-footer text-muted" 
							href="{{URL_ALUMNI_GALLERY}}">
								{{ getPhrase('view_all')}}
							</a>
						</div>
					</div>

					 

					 
						
				 </div>

</div>
		<!-- /#page-wrapper -->

@stop

@section('footer_scripts')
  
 
@stop
