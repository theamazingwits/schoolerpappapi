@extends($layout)
@section('content')

		<section id="main" class="main-wrap bgc-white-darkest" role="main">
			<div class="container-fluid content-wrap">
				<div class="row panel-grid grid-stack">

				<section data-gs-min-width="3" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-header grid-stack-handle bgc-white-dark panel-header-p panel-header-sm">
                                <h2 class="pull-left"> {{ getPhrase('students')}} </h2>
                               
                            </div>
                            <div class="panel-body pt-2">
                                <div class="h-md pos-r panel-body-p py-0">
                                    <h3 class="lh-0 fs-1 fw-light"> 
									<?php $studentObject =  App\Student::where('academic_id','!=',0)
					  	                              ->where('course_parent_id','!=',0)
					  	                              ->where('course_id','!=',0)
					  	                              ->where('current_year','!=',-1)
					  	                              ->where('current_semister','!=',-1)->get()->count();
                               
							 ?>
						   {{$studentObject}}	
									</h3>
							 <br><br>
                                    <a href="{{URL_LIBRARY_USERS}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
                                  
                                </div>
                                  <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80" sparkLineColor="#fafafa"  sparkFillColor="#46aaf9" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>


				<section data-gs-min-width="3" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-header grid-stack-handle bgc-white-dark panel-header-p panel-header-sm">
                                <h2 class="pull-left"> {{ getPhrase('faculty')}} </h2>
                               
                            </div>
                            <div class="panel-body pt-2">
                                <div class="h-md pos-r panel-body-p py-0">
                                    <h3 class="lh-0 fs-1 fw-light"> 
									{{ App\Staff::join('users','users.id','=','staff.user_id')
					                      ->where('course_parent_id','!=','')
					                      ->where('course_id','!=','')
					                      ->where('users.status','!=',0)
					                      ->get()->count()}}
									</h3>
							 <br><br>
                                    <a href="{{URL_LIBRARY_USERS}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
                               
                                </div>
                                     <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80"  sparkLineColor="#fafafa" sparkFillColor="#97d881" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>


					<section data-gs-min-width="3" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-header grid-stack-handle bgc-white-dark panel-header-p panel-header-sm">
                                <h2 class="pull-left"> {{ getPhrase('student_book_return')}} </h2>
                               
                            </div>
                            <div class="panel-body pt-2">
                                <div class="h-md pos-r panel-body-p py-0">
                                    <h3 class="lh-0 fs-1 fw-light"> 
									<?php $libraryIssuesObject = new App\LibraryIssue();
										$count = $libraryIssuesObject->getIssuesCount('student');
										?>
										{{$count}}
									</h3>
							 <br><br>
                                    <a href="{{URL_LIBRARY_LIBRARYDASHBOARD_BOOKS}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
                                  
                                </div>
                                  <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80"  sparkLineColor="#fafafa" sparkFillColor="#f74948" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,7,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>


					<section data-gs-min-width="3" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-header grid-stack-handle bgc-white-dark panel-header-p panel-header-sm">
                                <h2 class="pull-left"> {{ getPhrase('faculty_book_return')}} </h2>
                               
                            </div>
                            <div class="panel-body pt-2">
                                <div class="h-md pos-r panel-body-p py-0">
                                    <h3 class="lh-0 fs-1 fw-light"> 
									<?php $libraryIssuesObject = new App\LibraryIssue();
											$count = $libraryIssuesObject->getIssuesStaffCount('staff');
											?>
											{{$count}}
									</h3>
							 <br><br>
                                    <a href="{{URL_LIBRARY_LIBRARYDASHBOARD_BOOKS_STAFF}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
                                 
                                </div>
                                   <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80"  sparkLineColor="#fafafa" sparkFillColor="#aaaaaa" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>


					<section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-body pt-2 grid-stack-handle">
                                <div class="h-lg pos-r panel-body-p py-0">
									<h3 class=""><span class="fs-3 fw-light">{{ getPhrase('asset_types')}}</span> <i class="pull-right fa fa-database fs-1 m-2"></i></h3>
									<br><br><br><br>
                                    <a href="{{URL_LIBRARY_ASSETS}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
									
                                </div>
                                
                                <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80" sparkLineColor="#fafafa"  sparkFillColor="#aaaaaa" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>



					<section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-body pt-2 grid-stack-handle">
                                <div class="h-lg pos-r panel-body-p py-0">
									<h3 class=""><span class="fs-3 fw-light">{{ getPhrase('master_data')}}</span> <i class="pull-right fa fa-book fs-1 m-2"></i></h3>
									<br><br><br><br>
                                    <a href="{{URL_LIBRARY_MASTERS}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
									
                                </div>
                                <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80" sparkLineColor="#fafafa"  sparkFillColor="#aaaaaa" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>


					<section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-body pt-2 grid-stack-handle">
                                <div class="h-lg pos-r panel-body-p py-0">
									<h3 class=""><span class="fs-3 fw-light">{{ getPhrase('publishers')}}</span> <i class="pull-right fa fa-paint-brush fs-1 m-2"></i></h3>
									<br><br><br><br>
                                    <a href="{{URL_PUBLISHERS}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
									
                                </div>
                                <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80" sparkLineColor="#fafafa"  sparkFillColor="#aaaaaa" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>



					<section data-gs-min-width="3" data-gs-height="19" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel pb-0 bgc-white-dark">
                            <div class="panel-body pt-2 grid-stack-handle">
                                <div class="h-lg pos-r panel-body-p py-0">
									<h3 class=""><span class="fs-3 fw-light">{{ getPhrase('authors')}}</span> <i class="pull-right fa fa-mortar-board fs-1 m-2"></i></h3>
									<br><br><br><br>
                                    <a href="{{URL_AUTHORS}}" style="text-decoration: underline;"><h6 class="c-gray">View All</h6></a>
								
                                </div>
                                    <span class="col-7 p-0 spark-line spark-line-full panel-body-bottom-right" sparkHeight="80" sparkLineColor="#fafafa"  sparkFillColor="#aaaaaa" sparkSpotRadius="0" sparkType="line" values="2,3,5,4,6,5"></span>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>

				
					<section data-gs-min-width="6" data-gs-height="39" class="panel-wrap panel-grid-item grid-stack-item pos-a ui-draggable ui-resizable col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <!--Start Panel-->
                        <div class="panel bgc-white-dark">
                            <div class="panel-body grid-stack-handle">
                                <div class="panel-body-p">
								
								{!! Charts::assets() !!}
				  				{!! $asset_charts->render() !!}
				
                                </div>
                            </div>
                        </div>
                        <!--End Panel-->
					</section>

				
		</div>
		
		</div>
	</section>


				
				
		<!-- /#page-wrapper -->

@stop

@section('footer_scripts')


@stop
