@extends($layout)
@section('header_scripts')
<link href="{{CSS}}ajax-datatables.css" rel="stylesheet">
@stop
@section('content')

<div id="page-wrapper">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
	    <div class="container-fluid content-wrap">
			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>
						<li><a href="{{URL_LIBRARY_LIBRARYDASHBOARD}}">{{ getPhrase('library_dashboard')}}</a></li>
						<li>{{ $title }}</li>
					</ol>
				</div>
			</div>
							
			<!-- /.row -->
			<section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item">
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                        <h2 class="pull-left"> {{ $title }} </h2>
                        <div class="pull-right messages-buttons">
							<a href="{{URL_LIBRARY_MASTERS_ADD}}" class="btn  btn-primary button helper_step1 panel-header-button" >{{ getPhrase('create')}}</a>
						</div>
                        <!--End panel icons-->
                    </div>
                    <div class="panel-body panel-body-p packages">
						<div> 
						<table class="table table-striped table-bordered datatable" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th >{{ getPhrase('type')}}</th>
									<th>{{ getPhrase('image')}}</th>
									<th id="helper_step2">{{ getPhrase('title')}}</th>
									<th>{{ getPhrase('author')}}</th>
									<th>{{ getPhrase('publisher')}}</th>
									<th>{{ getPhrase('total')}}</th>
									<th>{{ getPhrase('available')}}</th>
									<th>{{ getPhrase('issued')}}</th>
									<th>{{ getPhrase('edition')}}</th>
									<th id="helper_step3">{{ getPhrase('action')}}</th>
								  
								</tr>
							</thead>
							 
						</table>
						</div>
					</div>
				</div>
			</section>
		</div>
		<!-- /.container-fluid -->
	</section>
</div>
@endsection
 

@section('footer_scripts')
  
 @include('common.datatables', array('route'=>'librarymasters.dataTable'))
 @include('common.deletescript', array('route'=>URL_LIBRARY_MASTERS_DELETE))

@stop
