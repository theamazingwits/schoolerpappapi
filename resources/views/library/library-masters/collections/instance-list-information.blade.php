 <?php 
         $image_path = IMAGE_PATH_UPLOAD_EXAMSERIES_DEFAULT;
         if($master_record->image)
         	$image_path = IMAGE_PATH_UPLOAD_LIBRARY.$master_record->image;
 ?>
<div class="row panel-grid grid-stack">
 	<section data-gs-min-width="3" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
        <!--Start Panel-->
        <div class="panel bgc-info spark-widget">
            <div class="panel-header bgc-info grid-stack-handle panel-header-p panel-header-sm">
                <h1 class="pull-left"> {{ $master_record->title }} </h1>
                <!--Start panel icons-->
                <!--End panel icons-->
            </div>
            <div class="panel-body panel-body-p">
                <div class="clearfix">
                    <div class="pull-left">
                        <h3 class="lh-7">{{ $master_record->author->author }}</h3>
                        <h4 class="fs-6-plus">{{getPhrase('total')}}:</h4>
                    </div>
                    <span class="display-4 pull-right lh-0">{{ $master_record->total_assets_count }}</span>
                </div>
            </div>
        </div>
        <!--End Panel-->
    </section>
    <section data-gs-min-width="3" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
        <!--Start Panel-->
        <div class="panel bgc-primary spark-widget">
            <div class="panel-header bgc-primary grid-stack-handle panel-header-p panel-header-sm">
                <h1 class="pull-left"> {{ getPhrase('available') }} </h1>
                <!--Start panel icons-->
                <!--End panel icons-->
            </div>
            <div class="panel-body panel-body-p">
                <div class="clearfix">
                    <div class="pull-left">
                        <h3 class="lh-7">{{getPhrase('total')}}:</h3>
                    </div>
                    <span class="display-4 pull-right lh-0">{{ $master_record->total_assets_available }}</span>
                </div>
            </div>
        </div>
        <!--End Panel-->
    </section>
    <section data-gs-min-width="3" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
        <!--Start Panel-->
        <div class="panel bgc-warning spark-widget">
            <div class="panel-header bgc-warning grid-stack-handle panel-header-p panel-header-sm">
                <h1 class="pull-left"> {{getPhrase('damaged')}} </h1>
                <!--Start panel icons-->
                <!--End panel icons-->
            </div>
            <div class="panel-body panel-body-p">
                <div class="clearfix">
                    <div class="pull-left">
                        <h3 class="lh-7">{{ getPhrase('total') }}:</h3>
                    </div>
                    <span class="display-4 pull-right lh-0"><?php $damaged= App\LibraryInstance::where('library_master_id','=',$master_record->id)
				->where('status','=','damaged')->get()->count();
				?>
				{{ $damaged }}</span>
                </div>
            </div>
        </div>
        <!--End Panel-->
    </section>
    <section data-gs-min-width="3" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
        <!--Start Panel-->
        <div class="panel bgc-danger spark-widget">
            <div class="panel-header bgc-danger grid-stack-handle panel-header-p panel-header-sm">
                <h1 class="pull-left"> {{getPhrase('lost')}} </h1>
                <!--Start panel icons-->
                <!--End panel icons-->
            </div>
            <div class="panel-body panel-body-p">
                <div class="clearfix">
                    <div class="pull-left">
                        <h3 class="lh-7">{{getPhrase('total')}}:</h3>
                    </div>
                    <span class="display-4 pull-right lh-0"><?php $lost= App\LibraryInstance::where('library_master_id','=',$master_record->id)
				->where('status','=','lost')->get()->count();
				?>   
				{{ $lost }}</span>
                </div>
            </div>
        </div>
        <!--End Panel-->
    </section>
</div>