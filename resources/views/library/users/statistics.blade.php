<?php $librarySettings = getLibrarySettings(); 
$max_issues = getSetting('maximum_issues_student','library_settings');
if($role=='staff')
	$max_issues = getSetting('maximum_issues_staff','library_settings');
?>
<div class="row panel-grid grid-stack">
	<section data-gs-min-width="3" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
        <!--Start Panel-->
        <div class="panel bgc-info spark-widget">
            <div class="panel-header bgc-info grid-stack-handle panel-header-p panel-header-sm">
                <h1 class="pull-left"> {{ getPhrase('maximum_allowed')}} </h1>
                <!--Start panel icons-->
                <!--End panel icons-->
            </div>
            <div class="panel-body panel-body-p">
                <div class="clearfix">
                    <div class="pull-left">
                        <h3 class="lh-7">{{getPhrase('total')}}:</h3>
                    </div>
                    <span class="display-4 pull-right lh-0">{{$max_issues}}</span>
                </div>
            </div>
        </div>
        <!--End Panel-->
    </section>
	<section data-gs-min-width="3" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
        <!--Start Panel-->
        <div class="panel bgc-warning spark-widget">
            <div class="panel-header bgc-warning grid-stack-handle panel-header-p panel-header-sm">
                <h1 class="pull-left"> {{ getPhrase('issued')}}</h1>
                <!--Start panel icons-->
                <!--End panel icons-->
            </div>
            <div class="panel-body panel-body-p">
                <div class="clearfix">
                    <div class="pull-left">
                        <h3 class="lh-7">{{getPhrase('total')}}:</h3>
                    </div>
                    <span class="display-4 pull-right lh-0">{{ count($books_issued)}}</span>
                </div>
            </div>
        </div>
        <!--End Panel-->
    </section>
    <section data-gs-min-width="3" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
        <!--Start Panel-->
        <div class="panel bgc-primary spark-widget">
            <div class="panel-header bgc-primary grid-stack-handle panel-header-p panel-header-sm">
                <h1 class="pull-left"> {{getPhrase('eligible')}} </h1>
                <!--Start panel icons-->
                <!--End panel icons-->
            </div>
            <div class="panel-body panel-body-p">
                <div class="clearfix">
                    <div class="pull-left">
                        <h3 class="lh-7">{{getPhrase('total')}}:</h3>
                    </div>
                    <span class="display-4 pull-right lh-0">{{ $max_issues - count($books_issued)}}</span>
                </div>
            </div>
        </div>
        <!--End Panel-->
    </section>
    <section data-gs-min-width="3" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
        <!--Start Panel-->
        <div class="panel bgc-danger spark-widget">
            <div class="panel-header bgc-danger grid-stack-handle panel-header-p panel-header-sm">
                <h1 class="pull-left"> {{ getPhrase('transactions')}} </h1>
                <!--Start panel icons-->
                <!--End panel icons-->
            </div>
            <div class="panel-body panel-body-p">
                <div class="clearfix">
                    <div class="pull-left">
                        <h3 class="lh-7">{{getPhrase('total')}}:</h3>
                    </div>
                    <span class="display-4 pull-right lh-0">{{ count($books_history)}}</span>
                </div>
            </div>
        </div>
        <!--End Panel-->
    </section>
	
</div>