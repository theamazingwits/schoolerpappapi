@extends($layout)
@section('header_scripts')
<link href="{{CSS}}ajax-datatables.css" rel="stylesheet">
@stop
@section('content')


<div id="page-wrapper">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
        <div class="container-fluid content-wrap">
			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>
						<li><a href="{{URL_LIBRARY_LIBRARYDASHBOARD}}">{{ getPhrase('library_dashboard')}}</a></li>

						<li>{{ $title }}</li>
					</ol>
				</div>
			</div>
							
			<!-- /.row -->
			<section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item">
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                        <h2 class="pull-left"> {{$title}} </h2>
                        <!--End panel icons-->
                    </div>
                    <div class="panel-body panel-body-p packages">
						<div > 
						<table class="table table-striped table-bordered datatable" cellspacing="0" width="100%" id="users-table">
							<thead>
								<tr>
									@if($user_type=='student') 
									<th id="helper_step1">{{ getPhrase('roll_no')}}</th>
									@else
									<th id="helper_step1">{{ getPhrase('faculty_id')}}</th>
									@endif
									<th>{{ getPhrase('image')}}</th>
									<th>{{ getPhrase('first_name')}}</th>
									<th>{{ getPhrase('last_name')}}</th>
									<th>{{ getPhrase('email')}}</th>

								</tr>
							</thead>
							 
						</table>
						</div>

					</div>
				</div>
			</section>
		</div>
		<!-- /.container-fluid -->
	</section>
</div>


@endsection
  

@section('footer_scripts')
  
 @include('common.datatables', array('route'=>URL_LIBRARY_USERS_GETLIST.$user_type, 'route_as_url'=>'TRUE','extra_var'=>1,'user_type'=>$user_type))
 
@stop
