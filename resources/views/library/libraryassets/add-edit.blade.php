@extends($layout)
@section('content')
<div id="page-wrapper">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
	    <div class="container-fluid content-wrap">
			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>
						<li><a href="{{URL_LIBRARY_LIBRARYDASHBOARD}}">{{ getPhrase('library_dashboard')}}</a></li>
						<li><a href="{{URL_LIBRARY_ASSETS}}">{{ getPhrase('library_assets')}}</a> </li>
						<li class="active">{{isset($title) ? $title : ''}}</li>
					</ol>
				</div>
			</div>
				@include('errors.errors')
			<!-- /.row -->
			<?php $settings = ($record) ? $settings : ''; ?>
			<section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item" ng-init="initAngData('{{ $settings }}');" ng-controller="angLibraryAssets">
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                        <h2 class="pull-left"> {{ $title }} </h2>
                        <div class="pull-right messages-buttons">
							<a href="{{URL_LIBRARY_ASSETS}}" class="btn  btn-primary button panel-header-button" >{{ getPhrase('list')}}</a>
						</div>
                        <!--End panel icons-->
                    </div>
                    <div class="panel-body panel-body-p">
						<?php $button_name = getPhrase('create'); ?>
						@if ($record)
						 <?php $button_name = getPhrase('update'); ?>
							{{ Form::model($record, 
							array('url' => URL_LIBRARY_ASSETS_EDIT.$record->slug, 
							'method'=>'patch','name'=>'formAssets ', 'novalidate'=>'')) }}
						@else
							{!! Form::open(array('url' =>URL_LIBRARY_ASSETS_ADD, 'method' => 'POST','name'=>'formAssets ', 'novalidate'=>'')) !!}
						@endif

						 @include('library.libraryassets.form_elements', 
						 array('button_name'=> $button_name),
						 array())
						{!! Form::close() !!}
					</div>

				</div>
			</section>
		</div>
		<!-- /.container-fluid -->
	</section>
</div>
<!-- /#page-wrapper -->
@stop
@section('footer_scripts')
 @include('common.validations')

 <script src="{{JS}}angular.js"></script>
 <script src="{{JS}}scripts/libraryAssets.js"></script>
@stop
 