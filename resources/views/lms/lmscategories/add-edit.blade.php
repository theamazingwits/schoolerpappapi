@extends('layouts.admin.adminlayout')
 
@section('content')
<div id="page-wrapper">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
	    <div class="container-fluid content-wrap">
			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="/"><i class="fa fa-home"></i></a> </li>
						<li><a  href="{{URL_LMS_DASHBOARD}}">LMS{{ getPhrase(' dashboard')}}</a></li>
						<li><a href="{{URL_LMS_CATEGORIES}}">LMS{{ getPhrase(' categories')}}</a> </li>
						<li class="active">{{isset($title) ? $title : ''}}</li>
					</ol>
				</div>
			</div>
			@include('errors.errors')	
			<section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item">
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                        <h2 class="pull-left"> {{ $title }} </h2>
                        <div class="pull-right messages-buttons">
							<a href="{{URL_LMS_CATEGORIES}}" class="btn  btn-primary button panel-header-button" >{{ getPhrase('list')}}</a>
						</div>
                        <!--End panel icons-->
                    </div>
                    <div class="panel-body panel-body-p form-auth-style">
						<?php $button_name = getPhrase('create'); ?>
						@if ($record)
						 <?php $button_name = getPhrase('update'); ?>
							{{ Form::model($record, 
							array('url' => URL_LMS_CATEGORIES_EDIT.$record->slug, 'novalidate'=>'','name'=>'formLms ',
							'method'=>'patch', 'files' => true)) }}
						@else
							{!! Form::open(array('url' => URL_LMS_CATEGORIES_ADD, 'method' => 'POST', 'novalidate'=>'','name'=>'formLms ', 'files' => true)) !!}
						@endif

						 @include('lms.lmscategories.form_elements', 
						 array('button_name'=> $button_name),
						 array('record' => $record))
						{!! Form::close() !!}
					</div>

				</div>
			</section>
		</div>
		<!-- /.container-fluid -->
	</section>
</div>
<!-- /#page-wrapper -->
@stop
@section('footer_scripts')
  @include('common.validations')
  @include('common.alertify')
 <script>
 	var file = document.getElementById('image_input');

file.onchange = function(e){
    var ext = this.value.match(/\.([^\.]+)$/)[1];
    switch(ext)
    {
        case 'jpg':
        case 'jpeg':
        case 'png':

     
            break;
        default:
         alertify.error("{{getPhrase('file_type_not_allowed')}}");
            this.value='';
    }
};
 </script>
@stop
 