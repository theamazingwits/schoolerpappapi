@extends('layouts.admin.adminlayout')

@section('custom_div')

<div ng-controller="prepareQuestions">

@stop
<?php
	if($right_bar === TRUE){
		$column = "col-xl-8";
		$column1 = "col-xl-6";
	}else{
		$column = "col-xl-12";
		$column1 = "col-xl-4";
	}
?>
@section('content')

<div id="page-wrapper">

	<section id="main" class="main-wrap bgc-white-darkest" role="main">
	    <div class="container-fluid content-wrap">

			<!-- Page Heading -->

			<div class="row">

				<div class="col-lg-12">

					<ol class="breadcrumb">

						<li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>
						<li><a  href="{{URL_LMS_DASHBOARD}}">LMS{{ getPhrase(' dashboard')}}</a></li>

						<li><a href="{{URL_LMS_SERIES}}">LMS{{ getPhrase(' series')}}</a></li>

						<li class="active">{{isset($title) ? $title : ''}}</li>

					</ol>

				</div>

			</div>

			@include('errors.errors')

			<?php $settings = ($record) ? $settings : ''; ?>

			<section class="col-sm-12 col-md-12 col-lg-12 {{$column}} panel-wrap panel-grid-item" ng-init="initAngData({{$settings}});">
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                        <h2 class="pull-left"> {{ $title }} </h2>
                       	<div class="pull-right messages-buttons">

							<a href="{{URL_LMS_SERIES}}" class="btn  btn-primary button panel-header-button" >{{ getPhrase('list')}}</a>

						</div>
                        <!--End panel icons-->
                    </div>
                    <div class="panel-body panel-body-p">

						<?php $button_name = getPhrase('create'); ?>
						<div class="row">

							<fieldset class="form-group col-md-6">

								{{ Form::label('lms_categories', getphrase('select_subject')) }}



								{{Form::select('lms_categories', $categories, null, ['class'=>'form-control', 'ng-model' => 'category_id', 

								'placeholder' => 'Select', 'ng-change'=>'categoryChanged(category_id)' ])}}

							</fieldset>

							<?php $lmssettings = getSettings('lms');?>

							<fieldset class="form-group col-md-6">

								{{ Form::label('file_type', getphrase('file_type')) }}



								{{Form::select('file_type', $lmssettings->content_types, null, ['class'=>'form-control', 'ng-model' => 'content_type', 

								'placeholder' => getPhrase('Select')  ])}}

							</fieldset>
							<div class="col-md-12">

								<div ng-if="examSeries!=''" class="vertical-scroll" >
									<h4 ng-if="categoryItems.length>0" class="text-success">{{getPhrase('total_items')}}: @{{ categoryItems.length}} </h4>
									<table  class="table table-hover">

										<th>{{getPhrase('title')}}</th>

										<th>{{getPhrase('code')}}</th>

										<th>{{getPhrase('type')}}</th>

										<th>{{getPhrase('action')}}</th>	

										<tr ng-repeat="item in categoryItems | filter : {content_type: content_type} | filter:search_term  track by $index">

											<td title="@{{item.title}}" >@{{item.title}}</td>

											<td>@{{item.code}}</td>

											<td>@{{item.content_type}}</td>

											<td>
												<a ng-click="addToBag(item);" class="btn btn-primary" >{{getPhrase('add')}}</a>
											</td>
										</tr>
									</table>
								</div>	
							</div>
						</div>
					</div>
				</div>
			</section>
			@if(isset($right_bar))
			<section class="col-sm-12 col-md-12 col-lg-12 col-xl-4 panel-wrap panel-grid-item">
                <!--Start Panel-->
                    	<?php $data = '';

						if(isset($right_bar_data))

							$data = $right_bar_data;

						?>

						@include($right_bar_path, array('data' => $data))
            </section>
            @endif
		</div>
		<!-- /.container-fluid -->
	</section>
</div>

<!-- /#page-wrapper -->

@stop

@section('footer_scripts')

@include('lms.lmsseries.scripts.js-scripts')

@stop

@section('custom_div_end')

</div>

@stop