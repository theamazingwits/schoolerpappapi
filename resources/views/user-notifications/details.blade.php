@extends($layout)
@section('header_scripts')
<link href="{{CSS}}ajax-datatables.css" rel="stylesheet">
@stop
@section('content')

<div id="page-wrapper">
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <ol class="breadcrumb">
                            <li><a href="{{PREFIX}}"><i class="mdi mdi-home"></i></a> </li>
                            <li><a href="{{URL_USER_NOTIFICATIONS}}">{{getPhrase('notifications')}}</a></li>
                        </ol>
                    </div>
                </div>
                <?php 
                            $title = $notification->data['title'];
                            $url = $notification->data['url'];
                            $description = $notification->data['description'];
                            
                     ?>
		<div class="panel panel-custom col-lg-9 col-lg-offset-2" >
                    <div class="panel-heading">
                        <h1><span class="text-left" >{{$title}}</span> 
                            <span class="pull-right">@ {{$notification->updated_at->toDateString()}}</span>
                        </h1> 
                    </div>
                    <div class="panel-body">
                        <div class="notification-details">
                            
                            <div class="notification-content text-center">
                                {!!$description!!}
                            </div>
                            
                            @if($url)
                            <div class="notification-footer text-center">
                                <a type="button" href="{{$url}}" class="btn btn-lg btn-dark button">{{getPhrase('read_more')}}</a>
                            </div>
                            @endif

                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        
@endsection
 
@section('footer_scripts')
 
@stop