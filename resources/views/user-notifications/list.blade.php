@extends($layout)
@section('header_scripts')
<link href="{{CSS}}ajax-datatables.css" rel="stylesheet">
@stop
@section('content')


<div id="page-wrapper">
			<div class="container-fluid">
				<!-- Page Heading -->
				<div class="row">
					<div class="col-lg-12">
						<ol class="breadcrumb">
							<li><a href="{{PREFIX}}"><i class="mdi mdi-home"></i></a> </li>
							<li>{{ $title }}</li>
						</ol>
					</div>
				</div>
								
				<!-- /.row -->
				<div class="panel panel-custom">
					<div class="panel-heading">
						
					 
					 
						<h1>{{ $title }}</h1>
					</div>
					<div class="panel-body packages">
						<div> 
						<table class="table table-striped table-bordered datatable" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>{{ getPhrase('title')}}</th>
									<th>{{ getPhrase('description')}}</th>
									<th>{{ getPhrase('created_at')}}</th>
									<th>{{ getPhrase('action')}}</th>
								</tr>
							</thead>


							<tbody>
							     
							     @if( count( $notifications ) > 0 )	

								@foreach($notifications as $notification)
								<?php 
									$title = $notification->data['title'];
		                            $url = $notification->data['url'];
		                            $description = $notification->data['description'];
		                            $notification->markAsRead();
								?>
								<tr>
									<td>{{$title}}</td>
									<td>{!!$description!!}</td>
									<td>{{$notification->updated_at->toDateString()}}</td>
									<td><a href="{{URL_USER_NOTIFICATIONS_VIEW.$notification->id}}">View more</a></td>
								</tr>
								@endforeach

								@else
								<tr>
									<td></td>
									<td></td>
									<td><h4>No Data Available</h4></td>
									<td></td>
								</tr>
								@endif
							</tbody>
							 
						</table>
						</div>
						<div class="pull-right">
						{{ $notifications->links() }}
						</div>
					</div>
				</div>
			</div>
			<!-- /.container-fluid -->
		</div>
@endsection
 

@section('footer_scripts')
  
 {{-- @include('common.datatables', array('route'=>URL_USER_NOTIFICATIONS_GETLIST, 'route_as_url' => TRUE)) --}}
 @include('common.deletescript', array('route'=>URL_USER_NOTIFICATIONS_DELETE))

@stop
