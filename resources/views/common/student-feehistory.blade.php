@extends($layout)
@section('header_scripts')
@stop

@section('custom_div')
<div ng-controller="studentFeeSchedules" ng-init="ingAngData({{$items}})">
@stop


@section('content')

<div id="page-wrapper">


	<section id="main" class="main-wrap bgc-white-darkest" role="main">
         <div class="container-fluid content-wrap">
         	<?php 
                  
                  $particulars = App\FeeParticularPayment::getStudentSchedules($student_id,$feecategory_id);
                  // dd($particulars);
                  $student_details = App\Student::where('id','=',$student_id)->first();
                  $feecategory_details =  App\FeeCategory::getCategory($student_details);
                  $record  = App\User::where('id','=',$student_details->user_id)->first();
                  $currency  = getSetting('currency_symbol','site_settings');

                  $feeshedules  = App\FeeScheduleParticular::getStudentSchedules($feecategory_id,$student_id);
                  
                 
			    ?>
				<div class="row">
					<div class="col-lg-12">
						<ol class="breadcrumb">
							<li><a href="{{PREFIX}}"><i class="menu-icon1 fa fa-home"></i></a> </li>
                         @if(checkRole(getUserGrade(2)))
                       <li><a href="{{URL_USERS_DASHBOARD}}">{{ getPhrase('users_dashboard') }}</a> </li>
                       

                    <li><a href="{{URL_USERS."student"}}">{{ getPhrase('student_users') }}</a> </li>
                    @endif

                        @if(checkRole(getUserGrade(7)))
                   <li><a href="{{URL_PARENT_CHILDREN}}">{{ getPhrase('children') }}</a> </li>
                   @endif
                   <li class="mr-1"><a href="{{URL_USER_DETAILS.$record->slug}}">{{ $student_details->first_name.$student_details->middle_name.$student_details->last_name }} {{getPhrase('details') }}</a> </li> 
                   
                        
                        
							<li>{{ $feecategory_details->title }}</li>
						</ol>
					</div>
				</div>

				<div class="row panel-grid grid-stack" id="panel-grid">

					<section data-gs-height="75" class="col-sm-12 col-lg-12 col-md-12 col-xl-12 panel-wrap panel-grid-item grid-stack-item">
								<!--Start Panel-->

                        <div class="panel bgc-white-dark">
                            <div class="panel-header clearfix grid-stack-handle panel-header-p bgc-white-dark panel-header-sm">
						<h2 class="pull-left">@{{ feecategory_details.title }} -  {{getPhrase('fee_paid_history')}}</h2>
                              
                                <!--Start panel icons-->
                                <div class="panel-icons panel-icon-slide ">
                                    <ul>
                                        <li><a href=""><i class="fa fa-angle-left"></i></a>
                                            <ul>
                                                <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                                                <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                                                <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
                                                <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
                                                <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                <!--End panel icons-->
                            </div>
                            <div class="panel-body panel-body-p packages" id="myForm">
                                <div id="accordion" class="accordion" role="tablist" aria-multiselectable="true">

                                    <div class="card mb-1">
                                        <div class="card-header" role="tab" id="headingOne">
                                            <h5 class="mb-0">
                                                <a class="collapse" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                    <i class="fa fa-heartbeat icon-mr"></i>{{getPhrase('fee_paid_history')}} </a>
                                            </h5>
                                        </div>
                                        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                                            <div class="card-body p-0 table-responsive">
                                                <table class="table m-0">
                                                    <thead>
                                                        <tr class="fs-7">
															<th>{{getPhrase('paid_date')}}</th>
															<th>{{getPhrase('fee')}}</th>
															<th>{{getPhrase('previous_balance')}}</th>
															<th>{{getPhrase('total_fee')}}</th>
															<th>{{getPhrase('paid_amount')}}</th>
															<th>{{getPhrase('discount')}}</th>
															<th>{{getPhrase('balance')}}</th>
															<th>{{getPhrase('receipt')}}</th>
														</tr>
                                                    </thead>

                                                    <tbody>

				                                        <tr ng-repeat = "item in paid_data">
													      <td><b>@{{item.created_at}}</b></td>
													      <td><b>{{$currency}} @{{item.amount}}</b></td>
													      <td><b>{{$currency}} @{{item.previous_balance}}</b>&nbsp;&nbsp;&nbsp;<span ng-if="item.previous_balance >0"><a class="btn btn-primary btn-sm" href="#" onclick="showPreviousFeeData()" >{{getPhrase('view')}}</a></span></td>
													      <td><b>{{$currency}} @{{item.total_amount}}</b></td>
													      <td><b>{{$currency}} @{{item.paid_amount}}</b></td>
													      <td><b>{{$currency}} @{{item.discount_amount}}</b></td>
													      <td><b>{{$currency}} @{{item.balance}}</b></td>
													      <td><a href="{{URL_PRINT_FEE_RECEIPT}}@{{item.transaction_id}}" class="btn btn-primary" target="_blank">{{getPhrase('yes')}}</a></td>
													</tr>
										
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card mb-1">
                                        <div class="card-header" role="tab" id="headingTwo">
                                            <h5 class="mb-0">
                                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                    <span class="fa fa-life-bouy icon-mr"></span>{{getPhrase('fee_category')}}
                                                    <!-- <span class="badge badge-pill badge-danger ml-1">+ 1</span> -->
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                                            <div class="card-body p-0 table-responsive">

                                                <table class="table m-0">
                                                    <thead>
														<tr class="fs-7">
															<th>{{getPhrase('total_installments')}}</th>
															<th>{{getPhrase('total_fee')}}</th>
															<th>{{getPhrase('installment_amount')}}</th>
															<th>{{getPhrase('other_amount')}}</th>
															<th>{{getPhrase('final_fee')}}</th>
														</tr>
                                                    </thead>

                                                    <tbody>
                                                        <tr>
													         <td><b>@{{ feecategory_details.total_installments }}</b></td>
													         <td><b>{{$currency}} @{{ feecategory_details.total_fee }}</b></td>
													         <td><b>{{$currency}} @{{ feecategory_details.installment_amount }}</b></td>
													         <td><b>{{$currency}} @{{ feecategory_details.other_amount }}</b></td>
													         <td><b>{{$currency}} @{{ final_fee}}</b></td>
														</tr>
                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--End Panel-->	

							</section>


				</div>




         </div>
    </section>

    <!--   <footer id="footer" class="footer-wrap" role="contentinfo">
            <span class="fs-7">
                <i class="icon-layers fs-4 mr-2 c-body-inverse-darker"></i> Reactive v.1.3.0 <span class="hidden-xs-down">- Admin Template</span>
                <span class="ml-1">&copy; 2017 - 2019</span>
            </span>
            <div class="footer-details hidden-lg-down">
                <a target="_blank" class="c-body-inverse-darker pull-right fs-6-plus mr-4" href="https://themeforest.net/item/reactive-responsive-admin-template/20407621">Purchase Now</a>
                <a class="c-body-inverse-darker pull-right fs-6-plus mr-2" href="documents.html">Documentation</a>
                <a class="c-body-inverse-darker pull-right fs-6-plus mr-2" href="documents.html#support">Support</a>
            </div>
        </footer> -->

        <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="text-align:center;color:#44a1ef;">{{getPhrase('previous_fee_details')}}</h4>
      </div>
    
     
      <div class="modal-body">
      <table class="table">
      <thead>
      <tr>
        <th><strong>{{getPhrase('fee_category')}}</strong></th>
        <th><strong>{{getPhrase('particular_name')}}</strong></th>
        <th><strong>{{getPhrase('term_number')}}</strong></th>
        <th><strong>{{getPhrase('is_schedule')}}</strong></th>
        <th><strong>{{getPhrase('amount')}}</strong></th>
      </tr> 
     
      </thead>
      <tbody>
        <tr ng-repeat="item in previous_data | filter:search track by $index">
          <td>@{{item.title}}</td>
          <td>@{{item.particular_title}}</td>
          <td ng-if = "item.term_number!=null">@{{item.term_number}}</td>
          <td ng-if = "item.term_number==null"> - </td>
          <td ng-if = "item.is_schedule=='1'" >{{getPhrase('yes')}}</td>
          <td ng-if = "item.is_schedule=='0'">{{getPhrase('no')}}</td>
          <td>{{$currency}} @{{item.amonut| currency : '' : 2 }}</td>
        </tr>
      </tbody>
      <tfoot>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td><strong>{{getPhrase('total')}}</strong></td>
        <td><strong> {{$currency}} @{{previous_amount | currency : '' : 2}}</strong></td>
        </tr>
      </tfoot>
        
      </table>
     
     </div>
    
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">{{getPhrase('ok')}}</button>
      </div>
    
    </div>

  </div>
</div>

        
</div>
		
@endsection

@section('footer_scripts')
@include('common.student-feepaid-history-script')
 <script >
   
    function showPreviousFeeData(feecat_id)
    {    
      $('#myModal').modal('show');
    }
</script>
  
@stop


<!-- /.row -->
			