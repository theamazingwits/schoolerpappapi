          <div class="panel-heading countdount-heading">
					<h2>{{getPhrase('fee_categories_list')}}</h2>
				</div>
								
				<div class="panel-body">
				     
		 <div class="row" ng-repeat = "user in student">
			<div class="profile-details text-center">
				<div class="profile-img">
				
				  <img ng-if="user.image!=null && user.image!=''" class="thumb" src="{{IMAGE_PATH_PROFILE}}@{{user.image}}" height="60">
            
            <img ng-if="user.image==null || user.image==''" class="thumb" src="{{IMAGE_PATH_USERS_DEFAULT_THUMB}}">

				</div>
				<div class="aouther-school">
					<h2>@{{user.name}}</h2>
					<p><span>@{{user.course_title+' ('+user.academic_year_title+')'}}</span><span ng-if="user.current_year==-1" >{{getPhrase('completed')}}</span>
					<span ng-if="user.current_year!=-1 && user.course_dueration>1 && user.is_having_semister==1">@{{user.current_year +' Year'}} - @{{user.current_semister +' Semester'}}</span><span ng-if="user.current_year!=-1 && user.course_dueration>1 && user.is_having_semister==0">@{{user.current_year +' Year'}}</span></p>
					<p><span>Roll: @{{user.roll_no}}</span></p>
				</div>

			</div>
		</div>

                  <div class="draggable-item-list" id="source">
					<div ng-repeat="item in source_items | filter:search track by $index" class="items-sub" 	
					ng-click="changeFeeCategory(item.feecategory_id,item.student_id)">@{{item.title}}
					
					</div>
				</div>

                </div>