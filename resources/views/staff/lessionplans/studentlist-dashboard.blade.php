@extends($layout)
@section('content')

<div id="page-wrapper">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
	    <div class="container-fluid content-wrap">
			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>
						
						<li>{{ $title }}</li>
					</ol>
				</div>
			</div>
							
			<section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item">
		      	<!--Start Panel-->
		        <div class="panel bgc-white-dark">
		          	<div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
		              <h2 class="pull-left"> {{ $title }} </h2>
		              
		              	<!--Start panel icons-->
		              	<div class="panel-icons panel-icon-slide bgc-white-dark">
		                  <ul>
		                      <li><a href=""><i class="fa fa-angle-left"></i></a>
		                          <ul>
		                              <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
		                              <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
		                              <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
		                              <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
		                              <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li>
		                              <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li>
		                          </ul>
		                      </li>
		                  </ul>
		              	</div>
		              <!--End panel icons-->
		          	</div>
		          	<div class="panel-body panel-body-p">
							<?php $lessionPlanObject = new App\LessionPlan();?>
						 @foreach($subjects as $subject)
				          
				          {!!Form::open(array('url'=>URL_LESSION_PLANS_VIEW_STUDENTS,'method'=>'POST','name'=>'studentList'))!!}

				         
						 <?php 

						 $summary = $lessionPlanObject->getSubjectCompletedStatus($subject->subject_id, $subject->staff_id, $subject->id);
						 $percent_completed = round($summary->percent_completed);
						 ?>
					 	<section data-gs-min-width="3" data-gs-height="39" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-3">
						    <!--Start Panel-->
						    <div class="panel {{getDashboardBoxColor()}}">
						        <div class="panel-body panel-body-p text-center grid-stack-handle">
						            <div class="text-center">
						                <h2 class="lh-0 fw-light">{{ $subject->subject_title}}</h2>
						                <h2 class="lh-0 fw-light">{{ $subject->course_title}}</h2>
						                <input type="hidden" name="academic_id" value="{{$subject->academic_id}}" >
						              	<input type="hidden" name="course_id"   value="{{$subject->course_id}}" >
						              	<input type="hidden" name="course_parent_id"   value="{{$subject->course_parent_id}}" >
						          		<input type="hidden" name="year"        value="{{$subject->year}}" >
						              	<input type="hidden" name="semister"    value="{{$subject->semister}}" >
						              	@if($subject->course_dueration>1)
									    	@if($subject->semister!=0)
												<p class="fs-6-plus mb-0 lh-6">{{ $subject->year.' '.getPhrase('year').' - '.$subject->semister.' '.getPhrase('semester')}}</p>
									      	@else
										      	<p class="fs-6-plus mb-0 lh-6">{{ $subject->year.' '.getPhrase('year')}}</p>
						                    @endif
										@endif
						                
						                <button class="btn btn-warning btn-lg my-4">{{ getPhrase('view_students') }}</button>
						            </div>
						        </div>
						    </div>
						    <!--End Panel-->
						</section>
					    <!-- <div class="col-md-3">
							<div class="card  text-xs-center card-progress" style="background: #a5311b;">
								<div class="card-lg-padding">
									<h4 class="card-title card-title-sm">{{ $subject->subject_title}}</h4>
									<p class="card-text">{{ $subject->course_title}}</p>

											  <input type="hidden" name="academic_id" value="{{$subject->academic_id}}" >
							                  <input type="hidden" name="course_id"   value="{{$subject->course_id}}" >
							                  <input type="hidden" name="course_parent_id"   value="{{$subject->course_parent_id}}" >
							                  <input type="hidden" name="year"        value="{{$subject->year}}" >
							                  <input type="hidden" name="semister"    value="{{$subject->semister}}" >
							                  
									@if($subject->course_dueration>1)
									    @if($subject->semister!=0)
									<p class="card-text">{{ $subject->year.' '.getPhrase('year').' - '.$subject->semister.' '.getPhrase('semester')}}</p>
									      @else
									      <p class="card-text">{{ $subject->year.' '.getPhrase('year')}}</p>
			                            @endif
									@endif

								</div>
								
								<div >

								<button class="btn card-footer text-muted"

								>{{ getPhrase('view_students') }}</button>

							</div>
							
							
							</div>
						</div> -->
						 {!!Form::close()!!}
						@endforeach
					</div>
				</div>
			</section>
		</div>
		<!-- /.container-fluid -->
	</section>
</div>
@stop
@section('footer_scripts')

@stop
