@extends($layout)
@section('header_scripts')
	
 {!! Charts::assets() !!}
@stop
@section('content')
<div id="page-wrapper">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
    	<div class="container-fluid content-wrap">
			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>
						 @if(checkRole(getUserGrade(2)))
	               <li><a href="{{URL_USERS_DASHBOARD}}">{{ getPhrase('users_dashboard') }}</a> </li>
	               

	            <li><a href="{{URL_USERS."staff"}}">{{ getPhrase('staff_users') }}</a> </li>
	            @endif
	            <li><a href="{{URL_STAFF_DETAILS.$record->slug}}">{{ $record->name }} {{getPhrase('details') }}</a> </li> 
						<li>{{ $title }}</li>
					</ol>
				</div>
			</div>
			<!-- /.row -->
			<section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item">
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                        <h2 class="pull-left"> {{ $title }} </h2>
                        
                        <!--Start panel icons-->
                        <div class="panel-icons panel-icon-slide bgc-white-dark">
                            <ul>
                                <li><a href=""><i class="fa fa-angle-left"></i></a>
                                    <ul>
                                        <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                                        <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                                        <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
                                        <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
                                        <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li>
                                        <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <!--End panel icons-->
                    </div>
                    <div class="panel-body panel-body-p">
						<?php $lessionPlanObject = new App\LessionPlan();?>
					 	@foreach($subjects as $subject)
					 	<?php 
						 $summary = $lessionPlanObject->getSubjectCompletedStatus($subject->subject_id, $subject->staff_id, $subject->id);
						 $percent_completed = round($summary->percent_completed);
					 	?>
				    	<div class="col-md-3 text-center">
						    <div class="lesson-plane-dashboard">
							    <div class="chart-title">
							    	<h4 title="{{$subject->subject_title}}">{{$subject->subject_title}}</h4>
							    	{{$subject->course_title}}
							    </div>
					    	 	<a class="lesson_plan_card text-muted" href="{{URL_LESSION_PLANS_VIEW_TOPICS.$user->slug.'/'.$subject->slug}}"> 
							    <?php   $chart = Charts::create('percentage', 'justgage')
												    ->title('')
												    ->elementLabel('')
												    ->values([$percent_completed,0,100])
												    ->responsive(false)
												    ->height(150)
												    ->width(0);
			    				?>
		    				 	{!! $chart->render() !!}
		 
							 	</a>
							</div>
						</div>
						@endforeach
					</div>
				</div>
			</section>
		</div>
		<!-- /.container-fluid -->
	</section>
</div>
@stop
@section('footer_scripts')

@stop
