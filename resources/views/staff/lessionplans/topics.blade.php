@extends($layout)
@section('content')

<div id="page-wrapper" ng-controller="lessionPlanController" ng-init="ingAngData({{$items}})">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
	 	<div class="container-fluid content-wrap">
			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>
						 @if($user=='admin'||$user=='owner')
	                         
						<li><a href="{{URL_STAFF_DETAILS.$user->slug}}">{{ getPhrase('staff_dashboard')}}</a> </li>
						@endif
						<li><a href="{{URL_LESSION_PLANS_DASHBOARD.$user->slug}}">{{ getPhrase('lesson_plans')}}</a> </li>
						<li>{{ $title }}</li>
					</ol>
				</div>
			</div>
			<!-- /.row -->
			<section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item">
		        <!--Start Panel-->
		        <div class="panel bgc-white-dark">
		          	<div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
		              <h2 class="pull-left"> {{ $title }} </h2>
		              
		              <!--Start panel icons-->
		              <div class="panel-icons panel-icon-slide bgc-white-dark">
		                  <ul>
		                      <li><a href=""><i class="fa fa-angle-left"></i></a>
		                          <ul>
		                              <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
		                              <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
		                              <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
		                              <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
		                              <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li>
		                              <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li>
		                          </ul>
		                      </li>
		                  </ul>
		              </div>
		              <!--End panel icons-->
		          	</div>
		          	<div class="panel-body panel-body-p" ng-repeat="topic in topics">
						<h4 class="title">@{{topic.topic_name}}</h4>
						<ul class="row topic-list">
							<li class="col-md-6" ng-if="topic.childs.length != 0 " ng-repeat="subtopic in topic.childs">
								<div class="topics clearfix">
									<div class="checkbox custom-checkbox">
									    <label>
										
									    <input 
									    ng-if="subtopic.is_completed==null || subtopic.is_completed==0"
									    @if($role_name=='staff')
									    ng-click="updateTopic(subtopic.id, topic.course_subject_id, 1)"
									    
									    @else 
									    disabled="" 
									    @endif
									    
									    type="checkbox">
									    <input 
									    ng-if="subtopic.is_completed!=null && subtopic.is_completed!=0"
									    @if($role_name=='staff')
									    ng-click="updateTopic(subtopic.id, topic.course_subject_id, 0)"
									    readonly="true" 
									    @else 
									    disabled="" 
									    @endif
									    checked type="checkbox">
									    <div class="item-checkbox">								    	
									    	<i class="fa fa-check checked" aria-hidden="true"></i>
									    </div>
									    </label>
								  	</div>
									<h4  ng-if="subtopic.is_completed==null || subtopic.is_completed==0" >@{{subtopic.topic_name | capitalize}}</h4>
									<h4 ng-if="subtopic.is_completed!=null && subtopic.is_completed!=0" class="text-primary">@{{subtopic.topic_name | capitalize}}</h4>
									<div class="form-group" ng-if="subtopic.is_completed!=null && subtopic.is_completed!=0" >
										<span>@{{subtopic.completed_on}}</span>
									</div>
								</div>
							</li>
						 	<li ng-if="topic.childs.length==0">
							 	<div class="topics clearfix">
								 	<h4>{{getPhrase('no_topics_available')}}</h4>
							 	</div>
						 	</li>
						</ul>
					</div>
				</div>
			</section>
		</div>
		<!-- /.container-fluid -->
	</section>
</div>
@stop
@section('footer_scripts')
@include('staff.lessionplans.scripts.js-scripts')
@include('common.alertify')
 @stop
