<!--Start Panel-->
<div class="panel bgc-white-dark">
    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
        <h2 class="pull-left"> {{ getPhrase('lesson_plan_statistics')}} </h2>
        
        <!--Start panel icons-->
        <div class="panel-icons panel-icon-slide bgc-white-dark">
            <ul>
                <li><a href=""><i class="fa fa-angle-left"></i></a>
                    <ul>
                        <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                        <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                        <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
                        <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
                        <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li>
                        <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <!--End panel icons-->
    </div>
    <div class="panel-body panel-body-p">
		<?php $lessionPlanObject = new App\LessionPlan();
		$user = Auth::user();
		$subjects = App\LessionPlan::getSubjects($user->id, 4, 'rand');
		?>
	 	@foreach($subjects as $subject)
		 <?php 

		 $summary = $lessionPlanObject->getSubjectCompletedStatus($subject->subject_id, $subject->staff_id, $subject->id);
		 $percent_completed = round($summary->percent_completed);
		 ?>
	    <div class="col-md-6 text-center">
		    <div class="lesson-plane-dashboard">
			    <strong title="{{$subject->subject_title}}">
			    	{{$subject->subject_title}}
			    </strong>
	    	 	<a class="text-muted" href="{{URL_LESSION_PLANS_VIEW_TOPICS.$user->slug.'/'.$subject->slug}}"> 
				    <?php   $chart = Charts::create('percentage', 'justgage')
								    ->title('')
								    ->elementLabel('')
								    ->values([$percent_completed,0,100])
								    ->responsive(false)
								    ->height(150)
								    ->width(0);
					?>
    				 {!! $chart->render() !!}
			 	</a>
			</div>
		</div>
		@endforeach
	</div>
</div>