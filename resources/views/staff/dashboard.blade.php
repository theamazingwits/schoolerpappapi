@extends($layout)
@section('header_scripts')
 {!! Charts::assets() !!}
@stop

@section('content')

{{-- User Certificates --}}
<?php
    
   $crecords        = App\UserCertificates::DocumetsSubmitted();
   $user_submites   = App\UserCertificates::where('user_id',Auth::user()->id)
                                           ->where('is_submitted',1)
                                           ->orWhere('is_approved',1)
                                           ->groupby('notification_id')
                                           ->pluck('notification_id')
                                           ->toArray();

?>  

@if($crecords)                                         

   @if(count($user_submites) > 0 )

   	  @foreach ($crecords as $key => $value)
   	     
         @if(in_array($value['id'], $user_submites))

         @else

           <div class="alert alert-danger">
			  <strong>Note:</strong> {{$value['description']}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="{{URL_UPLOAD_USER_CERTIFICATES.$value['id']}}" class="btn btn-primary btn-sm">Click here to upload</a>
			</div>

         @endif

   	   
      @endforeach

   @else
     
       @foreach ($crecords as $crecord)
   	      
          <div class="alert alert-danger">
			  <strong>Note:</strong> {{$crecord['description']}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="{{URL_UPLOAD_USER_CERTIFICATES.$crecord['id']}}" class="btn btn-primary btn-sm">Click here to upload</a>
			</div>

      @endforeach

  @endif 

@endif 


<div id="page-wrapper">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
	 	<div class="container-fluid content-wrap">
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						 
						<li><i class="fa fa-home"></i> {{ $title}}</li>
					</ol>
				</div>
			</div>
		 	<div class="row">
		 		<div class="col-md-9">
		 			<div class="row panel-grid grid-stack" id="panel-grid">
	                    <section data-gs-min-width="4" data-gs-min-height="20" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-4">
	                        <!--Start Panel-->
	                        <div class="panel pb-0 bgc-white-dark">
	                            <div class="panel-body pt-2 grid-stack-handle">
	                                <div class="h-lg pos-r panel-body-p py-0">
	                                    <h3 class=""><span class="fs-2 fw-light">{{ getPhrase('subject_preferences')}}</span> <i class="pull-right fa fa-hand-o-up fs-1 m-2"></i></h3>
	                                    <h6 class="c-primary lh-5"></h6>
	                                    <br><br>
	                                    <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{URL_STAFF_SUBJECT_PREFERENCES.Auth::user()->slug}}">
	                                        <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
	                                            <i class="icon-directions fs-4"></i>
	                                        </span>
	                                        <span class="d-inline-block align-top lh-7">
	                                            <span class="fs-6 d-block">{{ getPhrase('view_all')}}</span>
	                                            <span class="c-gray fs-7">Click to View more details</span>
	                                        </span>
	                                        <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
	                                    </a>
	                                </div>
	                            </div>
	                        </div>
	                        <!--End Panel-->
	                    </section>
	                    <section data-gs-min-width="4" data-gs-min-height="20" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-4">
	                        <!--Start Panel-->
	                        <div class="panel pb-0 bgc-white-dark">
	                            <div class="panel-body pt-2 grid-stack-handle">
	                                <div class="h-lg pos-r panel-body-p py-0">
	                                    <h3 class=""><span class="fs-2 fw-light">{{ getPhrase('lesson_plans')}}</span> <i class="pull-right fa fa-paper-plane-o fs-1 m-2"></i></h3>
	                                    <h6 class="c-primary lh-5"></h6>
	                                    <br><br>
	                                    <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{URL_LESSION_PLANS_DASHBOARD.Auth::user()->slug}}">
	                                        <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
	                                            <i class="icon-directions fs-4"></i>
	                                        </span>
	                                        <span class="d-inline-block align-top lh-7">
	                                            <span class="fs-6 d-block">{{ getPhrase('view_all')}}</span>
	                                            <span class="c-gray fs-7">Click to View more details</span>
	                                        </span>
	                                        <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
	                                    </a>
	                                </div>
	                            </div>
	                        </div>
	                        <!--End Panel-->
	                    </section>
	                    <section data-gs-min-width="4" data-gs-min-height="20" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-4">
	                        <!--Start Panel-->
	                        <div class="panel pb-0 bgc-white-dark">
	                            <div class="panel-body pt-2 grid-stack-handle">
	                                <div class="h-lg pos-r panel-body-p py-0">
	                                    <h3 class=""><span class="fs-2 fw-light">{{ getPhrase('student_attendance') }}</span> <i class="pull-right fa fa-calendar-check-o fs-1 m-2"></i></h3>
	                                    <h6 class="c-primary lh-5"></h6>
	                                    <br><br>
	                                    <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{URL_STUDENT_ATTENDENCE.Auth::user()->slug}}">
	                                        <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
	                                            <i class="icon-directions fs-4"></i>
	                                        </span>
	                                        <span class="d-inline-block align-top lh-7">
	                                            <span class="fs-6 d-block">{{ getPhrase('view_all')}}</span>
	                                            <span class="c-gray fs-7">Click to View more details</span>
	                                        </span>
	                                        <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
	                                    </a>
	                                </div>
	                            </div>
	                        </div>
	                        <!--End Panel-->
	                    </section>
                     	<section data-gs-min-width="4" data-gs-min-height="20" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-4">
	                        <!--Start Panel-->
	                        <div class="panel pb-0 bgc-white-dark">
	                            <div class="panel-body pt-2 grid-stack-handle">
	                                <div class="h-lg pos-r panel-body-p py-0">
	                                    <h3 class=""><span class="fs-2 fw-light">{{ getPhrase('timetable')}}</span> <i class="pull-right fa fa-calendar fs-1 m-2"></i></h3>
	                                    <h6 class="c-primary lh-5"></h6>
	                                    <br><br>
	                                    <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{URL_TIMETABLE_STAFF.Auth::user()->slug}}">
	                                        <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
	                                            <i class="icon-directions fs-4"></i>
	                                        </span>
	                                        <span class="d-inline-block align-top lh-7">
	                                            <span class="fs-6 d-block">{{ getPhrase('view_all')}}</span>
	                                            <span class="c-gray fs-7">Click to View more details</span>
	                                        </span>
	                                        <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
	                                    </a>
	                                </div>
	                            </div>
	                        </div>
	                        <!--End Panel-->
	                    </section>
	                     <section data-gs-min-width="4" data-gs-min-height="20" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-4">
	                        <!--Start Panel-->
	                        <div class="panel pb-0 bgc-white-dark">
	                            <div class="panel-body pt-2 grid-stack-handle">
	                                <div class="h-lg pos-r panel-body-p py-0">
	                                    <h3 class=""><span class="fs-2 fw-light">{{ getPhrase('students')}}</span> <i class="pull-right fa fa-users fs-1 m-2"></i></h3>
	                                    <h6 class="c-primary lh-5"></h6>
	                                    <br><br>
	                                    <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{URL_LESSION_PLANS_STUDENTLIST_DASHBOARD.Auth::user()->slug}}">
	                                        <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
	                                            <i class="icon-directions fs-4"></i>
	                                        </span>
	                                        <span class="d-inline-block align-top lh-7">
	                                            <span class="fs-6 d-block">{{ getPhrase('view_all')}}</span>
	                                            <span class="c-gray fs-7">Click to View more details</span>
	                                        </span>
	                                        <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
	                                    </a>
	                                </div>
	                            </div>
	                        </div>
	                        <!--End Panel-->
	                    </section>
					
                     	<section data-gs-min-width="4" data-gs-min-height="20" class="panel-wrap panel-grid-item grid-stack-item col-sm-12 col-md-6 col-lg-6 col-xl-4">
	                        <!--Start Panel-->
	                        <div class="panel pb-0 bgc-white-dark">
	                            <div class="panel-body pt-2 grid-stack-handle">
	                                <div class="h-lg pos-r panel-body-p py-0">
	                                    <h3 class=""><span class="fs-2 fw-light">{{ getPhrase('library_history')}}</span> <i class="pull-right fa fa-book fs-1 m-2"></i></h3>
	                                    <h6 class="c-primary lh-5"></h6>
	                                    <br><br>
	                                    <a class="slide-icon-x py-1 px-2 my-2 bw-1 bc-gray-lighter d-block text-left c-success lh-0" href="{{URL_USER_LIBRARY_DETAILS.Auth::user()->slug}}">
	                                        <span class="bgc-success btn-icon btn-icon-md rounded-circle mr-2">
	                                            <i class="icon-directions fs-4"></i>
	                                        </span>
	                                        <span class="d-inline-block align-top lh-7">
	                                            <span class="fs-6 d-block">{{ getPhrase('view_all')}}</span>
	                                            <span class="c-gray fs-7">Click to View more details</span>
	                                        </span>
	                                        <span class="pull-right c-gray mt-1 lh-5"><i class="fa fa-angle-right fs-5"></i></span>
	                                    </a>
	                                </div>
	                            </div>
	                        </div>
	                        <!--End Panel-->
	                    </section>
	                </div>
				</div>
				<div class="col-md-3">
					<div class="row panel-grid grid-stack" id="panel-grid">
						@if(isset($right_bar))
						<section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap  panel-grid-item grid-stack-item">
	                    	<?php $data = '';

							if(isset($right_bar_data))

								$data = $right_bar_data;

							?>

							@include($right_bar_path, array('data' => $data))
		            	</section>
		        	</div>
		            @endif
				</div>
			</div>
			<!-- /.container-fluid -->
	 		<div class = "row panel-grid grid-stack" id="panel-grid">
 			 	<section class="col-sm-12 col-md-12 col-xl-6 panel-wrap panel-grid-item grid-stack-item">
                    <!--Start Panel-->
                    <div class="panel bgc-white-dark">
                        <div class="panel-header clearfix grid-stack-handle panel-header-p bgc-white-dark panel-header-sm">
                            <h2 class="pull-left">
                            	<!-- <i class="menu-icon fa fa-book"></i> -->
                            {{getPhrase('library_history')}} </h2>
                             <div class="panel-input pull-right hidden-xs-down">
                                <input class="form-control search-control" data-list=".referral-list" type="text" placeholder="Search">
                                <a href="" class="clear-style"><i class="fa fa-search"></i></a>
                            </div>
                            <!--Start panel icons-->
                            <div class="panel-icons panel-icon-slide ">
                                <ul>
                                    <li><a href=""><i class="fa fa-angle-left"></i></a>
                                        <ul>
                                            <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                                            <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <!--End panel icons-->
                        </div>
                        <div class="panel-body panel-body-p">
                       		<?php 
					    	$records = App\LibraryIssue::issueHistory('',5);
					    	?>

					    	<table class="table ">
					    		<thead>
					    			<tr>
					    				<th>
					    				<strong>{{getPhrase('sno')}}</strong></th>
					    				<th><strong>{{getPhrase('title')}}
					    				</strong></th>
					    				<th><strong>{{getPhrase('number')}}</th>
					    				<th><strong>{{getPhrase('issued')}}
					    				</strong></th>
					    				<th><strong>{{getPhrase('status')}}
					    				</strong></th>
					    			</tr>
					    		</thead>
					    		<tbody>
					    		<?php $sno=1;?>
					    		@if(count($records))
					    		@foreach($records as $record)
					    		<?php $class = 'label label-success';
					    		if($record->issue_type=='issue' || $record->issue_type=='renewal')
					    			$class = 'label label-danger';
					    		if($record->issue_type=='lost')
					    			$class = 'label label-warning';
					    		?>
					    			<tr>
					    				<td>{{$sno++}}</td>
					    				<td>{{$record->title}}</td>
					    				<td>{{$record->library_asset_no}}</td>
					    				<td>{{date('Y-m-d',strtotime($record->issued_on))}}</td>
					    				<td><span class='{{$class}}' >{{ ucfirst($record->issue_type)}}</span></td>
					    			</tr>
					    		@endforeach
					    		@else
					    		<tr>
					    				<td>{{getPhrase('no_data_available')}}</td>
					    		</tr>
					    		@endif
					    		</tbody>
					    	</table>
			    		</div>
			    	</div>
                    <!--End Panel-->
                </section>
                <section class="col-sm-12 col-md-12 col-xl-6 panel-wrap panel-grid-item grid-stack-item">
                    <!--Start Panel-->
                    <div class="panel bgc-white-dark">
                        <div class="panel-header clearfix grid-stack-handle panel-header-p bgc-white-dark panel-header-sm">
                            <h2 class="pull-left">
                            	<!-- <i class="menu-icon fa fa-book"></i> -->
                            {{getPhrase("today's_classes")}} </h2>
                             <div class="panel-input pull-right hidden-xs-down">
                                <input class="form-control search-control" data-list=".referral-list" type="text" placeholder="Search">
                                <a href="" class="clear-style"><i class="fa fa-search"></i></a>
                            </div>
                            <!--Start panel icons-->
                            <div class="panel-icons panel-icon-slide ">
                                <ul>
                                    <li><a href=""><i class="fa fa-angle-left"></i></a>
                                        <ul>
                                            <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                                            <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <!--End panel icons-->
                        </div>
                        <div class="panel-body panel-body-p">
                       		<?php 
					    	$records = App\Timetable::getDayClasses();
					    	 ?>
					    	 <table class="table">
					    		<thead>
					    			<tr>
					    				<th>
					    				<strong>{{getPhrase('sno')}}</strong></th>
					    				<th><strong>{{getPhrase('subject')}}
					    				</strong></th>
					    				<th><strong>{{getPhrase('class')}}</th>
					    				<th><strong>{{getPhrase('from')}}
					    				</strong></th>
					    				<th><strong>{{getPhrase('to')}}
					    				</strong></th>
					    			</tr>
					    		</thead>
					    		<tbody>
					    		<?php $sno=1;?>
					    		@if(count($records))
					    		@foreach($records as $record)
			 
					    			<tr>
					    				<td>{{$sno++}}</td>
					    				<td>{{$record->subject_title}}</td>
					    				<td>{{$record->course_title}}</td>
					    				<td>{{$record->start_time}}</td>
					    				<td>{{ $record->end_time}}</td>
					    			</tr>
					    		@endforeach
					    		@else
					    		<tr>
					    				<td>{{getPhrase('no_data_available')}}</td>
					    		</tr>
					    		@endif
					    		</tbody>
					    	</table>
			    		</div>
			    	</div>
                    <!--End Panel-->
                </section>
			</div>
		</div>
	</section>
</div>
		<!-- /#page-wrapper -->

@stop

@section('footer_scripts')
  
 
@stop
