@extends($layout)

@section('header_scripts')
<link href="{{CSS}}animate.css" rel="stylesheet">
@stop
@section('custom_div')
<div ng-controller="academicCourses" ng-init="ingAngData({{$items}})">
@stop

@section('content')
<div id="page-wrapper">
  <section id="main" class="main-wrap bgc-white-darkest" role="main">
    <div class="container-fluid content-wrap">
      <!-- Page Heading -->
      <div class="row">
        <div class="col-lg-12">
          <ol class="breadcrumb">
            <li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>
            
            <li class="active">{{isset($title) ? $title : ''}}</li>
          </ol>
        </div>
      </div>
      @include('errors.errors')
      <!-- /.row -->
      <section class="col-sm-12 col-md-12 col-lg-12 col-xl-8 panel-wrap panel-grid-item">
        <!--Start Panel-->
        <div class="panel bgc-white-dark">
          <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
              <h2 class="pull-left"> {{ $title }} </h2>
              
              <!--Start panel icons-->
              <div class="panel-icons panel-icon-slide bgc-white-dark">
                  <ul>
                      <li><a href=""><i class="fa fa-angle-left"></i></a>
                          <ul>
                              <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                              <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                              <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
                              <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
                              <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li>
                              <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li>
                          </ul>
                      </li>
                  </ul>
              </div>
              <!--End panel icons-->
          </div>
          <div class="panel-body panel-body-p">
            <?php $button_name = getPhrase('create'); ?>
            <?php $button_name = getPhrase('update'); ?>
            {{ Form::model($record, 
            array('url' => URL_STAFF_SUBJECT_PREFERENCES.$record->slug, 
            'method'=>'post')) }}
            <div class="row">
              <div class="col-md-8">
                <h2 class="selected-item-title">{{getPhrase('preferred_subjects')}}</h2>
                <div class='containerVertical' id="target"  ng-drop="true" ng-drop-success="onDropComplete($data,$event)">
                  <div ng-if="!target_items.length" class="subject-placeholder"> No Item Selected</div>
                  <div ng-repeat="item in target_items" class="items-sub" id="target_items-@{{item.id}}">@{{item.subject_title}}
                    <input type="hidden" name="selected_list[]" data-myname="@{{item.subject_title}}" value="@{{item.id}}">
                    <div class="buttons-right">
                      <i class="fa fa-trash text-danger pull-right" ng-click="removeItem(item, target_items, 'target_items')"></i>
                      <i ng-if="item.is_lab==1" class="fa fa-flask pull-right text-primary" title="{{getPhrase('lab')}}" aria-hidden="true"></i> 
                      <i ng-if="item.is_elective_type==1" class="fa fa-hand-pointer-o pull-right text-info" title="{{getPhrase('elective')}}" aria-hidden="true"></i>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4 instruction">
                <h3>{{ getPhrase('summary') }}</h3>
                <ul class="guide">
                  <li>
                    <span class="answer">
                        <i class="fa fa-book">
                        </i>
                    </span>
                    {{getPhrase('subjects')}} &nbsp; @{{target_items_subjects}}
                  </li>
                  <li>
                    <span class="notanswer">
                        <i class="fa fa-flask">
                        </i>
                    </span>
                    {{getPhrase('labs')}} &nbsp; @{{target_items_labs}}
                  </li>
                  <li>
                    <span class="marked">
                        <i class="fa fa-hand-pointer-o">
                        </i>
                    </span>
                    {{getPhrase('electives')}} &nbsp; @{{target_items_electives}}
                  </li>
                </ul>
              </div>
            </div>
            <hr>
            <div class="text-center">
              <button class="btn btn-primary btn-lg">Update</button>
            </div>
            {!! Form::close() !!}
          </div>
        </div>
      </section>
      @if(isset($right_bar))
      <section class="col-sm-12 col-md-12 col-lg-12 col-xl-4 panel-wrap panel-grid-item">
        <!--Start Panel-->
        <div class="panel bgc-white-dark">
          <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
            <h2 class="pull-left"> {{ $title }} </h2>
            <!--Start panel icons-->
            <div class="panel-icons panel-icon-slide bgc-white-dark">
                <ul>
                    <li><a href=""><i class="fa fa-angle-left"></i></a>
                        <ul>
                            <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                            <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                            <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
                            <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
                            <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li>
                            <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!--End panel icons-->
          </div>
          <div class="panel-body panel-body-p">
            <?php $data = '';
            if(isset($right_bar_data))

              $data = $right_bar_data;

            ?>
            @include($right_bar_path, array('data' => $data))
          </div>
        </div>
      </section>
      @endif
    </div>
    <!-- /.container-fluid -->
  </section>
</div>
<!-- /#page-wrapper -->

  
@stop

@section('custom_div_end')
</div>
@stop

@section('footer_scripts')
@include('staff.subject-preferences.scripts.js-scripts')
@include('common.affix-window-size-script', array('newId'=>'app'))
@include('common.alertify')
 @stop