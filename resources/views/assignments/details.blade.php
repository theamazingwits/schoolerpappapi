@extends($layout)
@section('header_scripts')
@stop
@section('content')

  <div id="page-wrapper ">
        <!--Begin Content-->
        <section id="main" class="main-wrap bgc-white-darkest" role="main">

    
            <div class="container-fluid content-wrap">

                 <!-- Page Heading -->
				<div class="row">
					<div class="col-lg-12">
						<ol class="breadcrumb">
							<li><a href="{{PREFIX}}"><i class="fa fa-home bc-home"></i></a> </li>
							<li><a href="{{URL_STUDENT_ASSIGNMENT}}">{{getPhrase('assignments')}}</a> </li>
							<li>{{ $title }}</li>
						</ol>
					</div>
				</div>


                <div class="row panel-grid" id="panel-grid">
                    <div class="col-sm-12 col-md-12 col-lg-12 panel-wrap panel-grid-item grid-stack-item">
                        <!--Start Panel-->
                        <div class="panel bgc-white-dark">
                            <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                                <h2 class="pull-left">
                                {{ $title }}</h2>
                                <!--Start panel icons-->
                                <div class="panel-icons panel-icon-slide ">
                                    <ul>
                                        <li><a href=""><i class="fa fa-angle-left"></i></a>
                                            <ul>
                                                <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                                                <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                                                <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
                                                <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
                                                <!-- <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li> -->
                                                <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                <!--End panel icons-->
                            </div>

                            <div class="panel-body panel-body-p">
                                <div> 

					{!! Form::open(array('url' => URL_STUDENT_UPLOAD_ASSIGNMENT, 'method' => 'POST', 'name'=>'formQuiz ', 'novalidate'=>'','files'=>TRUE )) !!}

						<table class="table table-bordered table-hover">
							@if($reached)
							<div class="alert alert-info">
							  <b>Note : </b> {{getPhrase('your_last_date_to_submit_is_over_so_you_are_unable_to_upload_your_assignment')}}
							</div>
							@endif

							<tbody>
								<tr>
									<td>{{getPhrase('assignment_name')}}</td>
									<td><b>{{ucwords($details->title)}}</b></td>
									<td>{{getPhrase('subject')}}</td>
									<td><b>{{ucwords($details->subject_title)}}</b></td>
									
								</tr>

								<tr>
									<td>{{getPhrase('staff')}}</td>
									<td><b>{{ucwords($details->name)}}</b></td>
									<td>{{getPhrase('last_date_to_submit')}}</td>
									<td><b>{{$details->deadline}}</b></td>
									
								</tr>

								<tr>
									<td>{{getPhrase('assignment_file')}}</td>
									@if($details->file_name)
									<td><a href="{{URL_DOWNLOAD_ASSIGNMENT_FILE.$details->slug}}"><img src="{{IMAGE_PATH_UPLOAD_ASSIGNMENS_DEFAULT}}" height="40px" width="40px"><p><b>{{ucwords($details->file_name)}}</b></p></a></td>
									@else
									<td>-</td>
									@endif
									<td>{{getPhrase('description')}}</td>
									<td><b>{{ucwords($details->description)}}</b></td>
									
								</tr>

						</tbody>

					</table>

					<input type="hidden" name="allocate_id" value="{{$details->allocate_id}}">

							<div class="row">

								<fieldset class="form-group col-md-6" >

										{{ Form::label('category', getphrase('upload_your_assignment')) }}
										<i class="fa fa-cloud-upload"></i>
										<input type="file" class="form-control" name="catimage">
							
							</fieldset>

								<fieldset class="form-group col-md-6" >

									@if($details->user_file)

									<a href="{{URL_DOWNLOAD_STUDNET_ASSIGNMENT.$details->allocate_id}}">

										<img src="{{IMAGE_PATH_UPLOAD_ASSIGNMENS_DEFAULT}}" height="40px" width="40px">
										<p>
											<b>
												{{ucwords($details->user_file)}}
											</b>
										</p>
									</a>

									@endif
							
							</fieldset>

						</div>
                    
                     @if(!$reached)

						<div class="buttons text-center">

							<button class="btn btn-lg btn-primary button"

							>Upload</button>

						</div>

				    @endif		

							{!! Form::close() !!}

						</div>

					</div>

                            </div>
                        </div>
                        <!--End Panel-->
                    </div>
                </div>
            </div>
        </section>
        <!--End Content-->
      
 
    </div>
	@endsection
 

 @section('footer_scripts')
   
 @stop
