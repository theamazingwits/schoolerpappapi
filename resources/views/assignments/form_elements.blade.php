 					
 				

                 <div class="row">

 					 <fieldset class="form-group col-md-6">

						{{ Form::label('title', getphrase('name')) }}

						<span class="text-red">*</span>

						{{ Form::text('title', $value = null , $attributes = array('class'=>'form-control', 'placeholder' => getPhrase('title'),

							'ng-model'=>'title', 


							'required'=> 'true', 

							'ng-class'=>'{"has-error": formQuiz.title.$touched && formQuiz.title.$invalid}',


							)) }}

						<div class="validation-error" ng-messages="formQuiz.title.$error" >

	    					{!! getValidationMessage()!!}




						</div>

					</fieldset>	

						<fieldset class="form-group col-md-6">

						{{ Form::label('subject_id', getphrase('subject')) }}

						<span class="text-red">*</span>

						{{Form::select('subject_id', $subjects, null, [
							'class'=>'form-control',
							'placeholder'=>'Select',
							 'id'=>'subject',
							'ng-model'=>'subject_id',
							'required'=> 'true', 
							'ng-class'=>'{"has-error": formQuiz.subject_id.$touched && formQuiz.subject_id.$invalid}'
						])}}
						 <div class="validation-error" ng-messages="formQuiz.subject_id.$error" >
	    					{!! getValidationMessage()!!}
						</div>
					</fieldset>


						<fieldset class="form-group col-md-6">

						{{ Form::label('course_subject_id', getphrase('class')) }}

						<span class="text-red">*</span>

						{{Form::select('course_subject_id', $classes, null, [
							'class'=>'form-control',
							'placeholder'=>'Select',
							 'id'=>'subject',
							'ng-model'=>'course_subject_id',
							'required'=> 'true', 
							'ng-change'=>'changeClass(course_subject_id)',
							'ng-class'=>'{"has-error": formQuiz.course_subject_id.$touched && formQuiz.course_subject_id.$invalid}'
						])}}
						 <div class="validation-error" ng-messages="formQuiz.course_subject_id.$error" >
	    					{!! getValidationMessage()!!}
						</div>
					</fieldset>
					<fieldset class="form-group col-md-6">
                                     
                        {{ Form::label('deadline', getphrase('deadline')) }}
                        <span class="text-red">*</span>

                        <div class="input-group date" data-date="{{date('Y/m/d')}}" data-provide="datepicker" data-date-format="{{getDateFormat()}}">

                        {{ Form::text('deadline', $value = date('Y/m/d') , $attributes = array(

                        	'class'       =>'form-control',
                        	'placeholder' => '2015/7/17', 
                        	'id'          =>'dp',
                        	'ng-model'=>'deadline',
							'required'=> 'true',
							'ng-class'=>'{"has-error": formQuiz.deadline.$touched && formQuiz.deadline.$invalid}'

                        	 )) }}

                        <div class="validation-error" ng-messages="formQuiz.deadline.$error" >
	    					{!! getValidationMessage()!!}
						</div>

                            <div class="input-group-addon">

                                <span class="fa fa-calendar"></span>

                            </div>

                        </div>

                        </fieldset>
						<fieldset class="form-group col-md-12" ng-if="course_subject_id">
							<?php $types= array('0'=>'All Students','1'=>'Selected Students'); ?>

						{{ Form::label('type', getphrase('allocation_type')) }}

						<span class="text-red">*</span>

						{{Form::select('type', $types, null, [
							'class'=>'form-control',
							'placeholder'=>'Select',
							 'id'=>'subject',
							'ng-model'=>'type',
							'required'=> 'true', 
							'ng-change'=>'getStudents(type)',
							'ng-class'=>'{"has-error": formQuiz.type.$touched && formQuiz.type.$invalid}'
						])}}
						 <div class="validation-error" ng-messages="formQuiz.type.$error" >
	    					{!! getValidationMessage()!!}
						</div>
					</fieldset>

					<fieldset class="form-group col-md-12" ng-if="allocate_type == 1" style="max-height: 500px;overflow-y:scroll;">
						<table class="table responsive" >
							<thead>
								<th><b>{{getPhrase('S.No')}}</b></th>
								<th><b>{{getPhrase('roll_no')}}</b></th>
								<th><b>{{getPhrase('student_name')}}</b></th>
								<th><b>{{getPhrase('select')}}</b></th>
							</thead>

							<tbody>
								<tr ng-repeat="student in students track by $index">
									<td>@{{$index+1}}</td>
									<td>@{{student.user.roll_no}}</td>
									<td>@{{student.user.name}}</td>
									<td>
									<input type="checkbox" ng-checked="student.is_assined == 1" name="users_id[]" value="@{{student.user.id}}" style="display: block;">
									</td>
								</tr>
							</tbody>
							
						</table>
					</fieldset>

                       <fieldset class="form-group col-md-6" >
				        {{ Form::label('category', getphrase('file')) }}
				         <input type="file" class="form-control" name="catimage">


				      </fieldset>
                     
                      @if($record)

				      <fieldset class="form-group col-md-6">
				      	
				          <img src="{{IMAGE_PATH_UPLOAD_ASSIGNMENS_DEFAULT}}" width="40px" height="40px"><p><a href="{{URL_DOWNLOAD_ASSIGNMENT_FILE.$record->slug}}">{{$record->file_name}}</a></p>
				       

				      </fieldset>

				        @endif

				

					<fieldset class="form-group col-md-12">
						
						{{ Form::label('description', getphrase('description')) }}
						
						{{ Form::textarea('description', $value = null , $attributes = array('class'=>'form-control', 'rows'=>'5', 'placeholder' => 'Description','id'=>'description')) }}
					</fieldset>	

					</div>

					  @if(!$uploaded)


						<div class="buttons text-center">

							<button class="btn btn-lg btn-primary button"

							ng-disabled='!formQuiz.$valid'>{{ $button_name }}</button>

						</div>

				@endif		

		 