@include('common.angular-factory')
<script>

 app.controller('StudentAssignments', function ($scope, $http, httpPreConfig)
  {
 
   $scope.allocate_type  = 0;
   
   $scope.assignmentUsers = function(course_subject_id, allocate_type){


           $scope.allocate_type  = allocate_type;

          if(allocate_type == 0 )
            return;
           
            route   = '{{ URL_GET_CLASS_STUDENTS }}';  
            data    = {   
                   _method: 'post', 
                  '_token':httpPreConfig.getToken(), 
                  'type':  allocate_type, 
                  'course_subject_id':  course_subject_id, 
                  'record_type':'{{$add_record}}'
               };

        httpPreConfig.webServiceCallPost(route, data).then(function(result){

            // console.log(result.data);

            $scope.students  = result.data;
  
       });
   }

   $scope.changeClass  = function(course_subject_id){
       
       $scope.course_subject_id   = course_subject_id;
   } 

   $scope.getStudents = function(type){

           $scope.allocate_type  = type;
         
           if(type == 0 )
            return;


            course_subject_id   = $scope.course_subject_id;

            route   = '{{ URL_GET_CLASS_STUDENTS }}';  
            data    = {   
                   _method: 'post', 
                  '_token':httpPreConfig.getToken(), 
                  'type':  $scope.allocate_type, 
                  'course_subject_id':  course_subject_id, 
                  'record_type':'{{$add_record}}'
               };

        httpPreConfig.webServiceCallPost(route, data).then(function(result){

          // console.log(result.data);

           $scope.students  = result.data;

       });
   }

 

 
});

</script>