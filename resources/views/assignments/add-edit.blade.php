@extends($layout)
@section('header_layout')
<link href="{{CSS}}bootstrap-datepicker.css" rel="stylesheet">
<link href="{{CSS}}plugins/datetimepicker/css/bootstrap-datetimepicker.css" rel="stylesheet">   
@stop

@section('content')
@if($record)
<div id="page-wrapper" ng-controller="StudentAssignments" ng-init="assignmentUsers('{{$record->course_subject_id}}','{{$record->type}}')">
@else
<div id="page-wrapper" ng-controller="StudentAssignments">
@endif	
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
	    <div class="container-fluid content-wrap">
			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="/"><i class="fa fa-home"></i></a> </li>
						<li><a href="{{URL_ASSIGNMENTS}}">{{ getPhrase('assignments')}}</a></li>
						<li class="active">{{isset($title) ? $title : ''}}</li>
					</ol>
				</div>
			</div>
				@include('errors.errors')
			<!-- /.row -->
			
			<section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item">
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                    	<h2 class="pull-left">{{$title}}</h2>
						<div class="panel-icons panel-icon-slide bgc-white-dark">
                            <ul>
                                <li><a href=""><i class="fa fa-angle-left"></i></a>
                                  <ul>
                                      <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                                      <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                                      <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
                                      <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
                                      <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li>
                                      <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li>
                                  </ul>
                              </li>
                          	</ul>
                      	</div>
                      	<!--Start panel icons-->
                  		
                      <!--End panel icons-->
                    </div>
                    <div class="panel-body panel-body-p">
	            
		             	@if($record && $uploaded)

				      	<div class="alert alert-info">
						  <b>Note : </b> {{getPhrase('student_is_uploaded_the_work_so_your_are_unable_to_modify_the_assignment')}}
						</div>
				     	@endif		

						<?php $button_name = getPhrase('create'); ?>
						@if ($record)
						 <?php $button_name = getPhrase('update'); ?>
							{{ Form::model($record, 
							array('url' => URL_ASSIGNMENTS_EDIT.$record->slug, 
							'method'=>'patch', 'name'=>'formQuiz ', 'novalidate'=>'','files'=>TRUE)) }}
						@else
							{!! Form::open(array('url' => URL_ASSIGNMENTS_ADD, 'method' => 'POST', 'name'=>'formQuiz ', 'novalidate'=>'','files'=>TRUE )) !!}
						@endif
						

						 @include('assignments.form_elements', 
						 array(
						 	'button_name'=> $button_name,
						 	'record'=> $record,
						 	'subjects'=> $subjects,
						 	'classes'=> $classes,
						 	'assignment_id'=> $assignment_id,
						 	'uploaded'=> $uploaded,
						 	))
						 		
						{!! Form::close() !!}
					</div>
				</div>
			</section>
		</div>
		<!-- /.container-fluid -->
	</section>
</div>
<!-- /#page-wrapper -->
@stop

@section('footer_scripts')
 @include('assignments.scripts.js-scripts', array( 'add_record'=>$add_record ) )
 @include('common.validations',array('isLoaded'=>TRUE))


 <script src="{{JS}}moment.min.js"></script>

  <script src="{{JS}}plugins/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
 <script>
    
     $(function () {
        $('#dp').datetimepicker({
               format: 'YYYY-MM-DD',
            
            });
    });
 </script>

 

@stop
 
 