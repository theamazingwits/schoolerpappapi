@extends($layout)
@section('header_scripts')
@stop
@section('content')


<div id="page-wrapper">
	<section id="main" class="main-wrap bgc-white-darkest" role="main">
	    <div class="container-fluid content-wrap">
			<!-- Page Heading -->
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li><a href="{{PREFIX}}"><i class="fa fa-home"></i></a> </li>
						 @if(checkRole(getUserGrade(2)))
						 <li><a href="{{URL_USERS_DASHBOARD}}">{{getPhrase('users_dashboard')}}</a></li>
						<li><a href="{{URL_USERS."staff"}}">{{getPhrase('staff')}}</a></li>
						<li><a href="{{URL_ASSIGNMENTS.'/'.$user->slug}}">{{getPhrase('assignments')}}</a> </li>
						<li>{{ $title }}</li>
						 @else
						<li><a href="{{URL_ASSIGNMENTS}}">{{getPhrase('assignments')}}</a> </li>
						<li>{{ $title }}</li>
						 @endif
					</ol>
				</div>
			</div>
							
			<!-- /.row -->
			<section class="col-sm-12 col-md-12 col-lg-12 col-xl-12 panel-wrap panel-grid-item">
                <!--Start Panel-->
                <div class="panel bgc-white-dark">
                    <div class="panel-header clearfix  panel-header-p bgc-white-dark panel-header-sm">
                    	<h2 class="pull-left">{{$class_name}} - {{ucwords($record->title)}}</h2>
						<div class="panel-icons panel-icon-slide bgc-white-dark">
                            <ul>
                                <li><a href=""><i class="fa fa-angle-left"></i></a>
                                  <ul>
                                      <li><a class="panel-refresh-btn" href=""><i class="fa fa-refresh"></i></a></li>
                                      <li><a class="panel-pin-btn" href=""><i data-icon="icon-login icon-logout" class="icon-logout"></i></a></li>
                                      <li><a class="panel-full-btn" href=""><i data-icon="icon-size-actual icon-size-fullscreen" class="fs-7 icon-size-fullscreen fw-bold"></i></a></li>
                                      <li><a class="panel-maximize-btn" href=""><i data-icon="ion-android-contract ion-android-expand" class="fs-5 ion-android-expand fw-bold"></i></a></li>
                                      <li><a class="panel-collapse-btn" href=""><i data-icon="ion-android-add ion-android-remove" class="fs-4 ion-android-remove"></i></a></li>
                                      <li><a class="panel-close-btn" href=""><i class="fs-4 ion-android-close"></i></a></li>
                                  </ul>
                              </li>
                          	</ul>
                      	</div>
                      	<!--Start panel icons-->
                  		
                      <!--End panel icons-->
                    </div>
                    <div class="panel-body panel-body-p">
						<div style="max-height: 500px;overflow-y:auto; "> 

						{!! Form::open(array('url' => URL_APPROVE_ASSIGNMENT, 'method' => 'POST', 'name'=>'formQuiz ', 'novalidate'=>'','files'=>TRUE,'id'=>'formQuiz' )) !!}

							<table class="table table-bordered table-hover">
						     
						      <thead>
						      	<th><b>{{getPhrase('S.no')}}</b></th>
						      	<th><b>{{getPhrase('name')}}</b></th>
						      	<th><b>{{getPhrase('roll_no')}}</b></th>
						      	<th><b>{{getPhrase('dead_line')}}</b></th>
						      	{{-- <th><b>{{getPhrase('submitted_on')}}</b></th> --}}
						      	<th><b>{{getPhrase('file')}}</b></th>
						      	<th><b>{{getPhrase('credits')}}</b></th>
						      	<th><b>{{getPhrase('submitted')}}</b></th>
						      	<th><b>{{getPhrase('approved')}}</b></th>
						      	@if(!checkRole(getUserGrade(2)))
						      	<th><b>{{getPhrase('action')}}</b></th>
						      	@endif
						      </thead>	
						      
						      <tbody>
						         <?php $i = 1; ?>
						         @foreach($students as $student)
			                       
						         <tr>
						         	<td>{{$i++}}</td>
						         	<td>{{$student->name}}</td>
						         	<td>{{$student->roll_no}}</td>
						         	<td>{{$student->deadline}}</td>
						         	{{-- <td>{{$student->submitted_on}}</td> --}}
						         	<td>
										    @if($student->user_file)

										      <a href="{{URL_DOWNLOAD_STUDNET_ASSIGNMENT.$student->allocate_id}}">

										      	<img src="{{IMAGE_PATH_UPLOAD_ASSIGNMENS_DEFAULT}}" height="40px" width="40px">
										      	<p>
										      		<b>
										      			{{ucwords($student->user_file)}}
										      		</b>
										      	</p>
										      </a>

										      @else
										      -

										    @endif
								    </td>
								    <td><input type="number" class="form-control bt-modal-input-name" name="credits[{{$student->allocate_id}}]" min="0" max="50" value="{{$student->credits}}" id="{{$student->student_id}}"></td>

								    <td>
								    	@if( $student->is_submitted == 0 )
								    	<span class="label label-warning">No</span>
								    	@else
								    	<span class="label label-success">Yes</span>
								    	@endif 
								    	
								    </td>

								     <td>

								     @if( $student->is_approved == 0 )
								    	<span class="label label-warning">No</span>
								    	@else
								    	<span class="label label-success">Yes</span>
								    	@endif 
								    	
								    </td>

								    @if(!checkRole(getUserGrade(2)))

						         	<td>
						         		<a href="javascript:void(0)" onclick="SignleApprove('{{$student->allocate_id}}','{{$student->student_id}}')" class="btn btn-info btn-sm">Approve</a>
						         	</td>
						         	@endif
						         
						         </tr>

						         @endforeach	
						      
						      </tbody>
						 
							</table>
			            </div>

	            		<input type="hidden" name="assignment_id" value="{{$record->id}}"><br>

		                @if(!checkRole(getUserGrade(2)))

						<div class="buttons text-center">

							<a href="javascript:void(0)" class="btn btn-lg btn-primary button" onclick="approveAll()">{{getPhrase('approve_all')}}</a >

						</div>

						@endif


						{!! Form::close() !!}
					</div>
				</div>
			</section>
		</div>
	</section>
	<!-- /.container-fluid -->
	<div id="allApprove" class="modal fade" role="dialog">
	  	<div class="modal-dialog modal-md">
		    <!-- Modal content-->
		    <div class="modal-content">
		  		<div class="modal-header">
			        <h4 class="modal-title" align="center"><b>{{getPhrase('approve_student_assignment')}}</b></h4>
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
		      	</div>
		      	<div class="modal-body">
		        	<h4 style="color: #ffa616;" align="center">{{getPhrase('are_you_sure_to_approve_all_students_assignments')}}</h4>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>&nbsp;
			        <button type="button" class="btn btn-primary" onclick="ApproveStudents()">Yes</button>
		      	</div>
		    </div>
	  	</div>
	</div>
	<div id="approve" class="modal fade" role="dialog">
	  	<div class="modal-dialog modal-md">

		    <!-- Modal content-->
		    <div class="modal-content">
		     	<div class="modal-header">
		     		<h4 class="modal-title" align="center"><b>{{getPhrase('approve_student_assignment')}}</b></h4>
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
		      	</div>
		      	<div class="modal-body">
					{!! Form::open(array('url' => URL_APPROVE_SIGNLE_ASSIGNMENT, 'method' => 'POST', 'name'=>'formQuiz ', 'novalidate'=>'','files'=>TRUE,'id'=>'formQuiz' )) !!}
			       
			        <h4 style="color: #ffa616;" align="center">{{getPhrase('are_you_sure_to_approve_student_assignments')}}</h4>
			        <input type="hidden" name="allocation_id" id="allocation_id">
			        <input type="hidden" name="student_id" id="student_id">
			        <input type="hidden" name="credits" id="credits">
		      	</div>
		      	<div class="modal-footer">
			        <button type="submit" class="btn btn-primary">Yes</button>
			        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>&nbsp;
		      	</div>
		      	{!! Form::close() !!}
		    </div>
	  	</div>
	</div>
</div>
@endsection
 

@section('footer_scripts')

 <script>

      function approveAll()
      {
          $('#allApprove').modal('show');
      }

      function ApproveStudents(){

      	 $('#formQuiz').submit();
      }

      function SignleApprove(allocate_id, student_id)
      {    
      	// console.log(allocate_id);
      	// console.log(student_id);
      	
      	var credits   = $('#'+student_id).val();

      	// console.log(credits);

      	   $('#allocation_id').val(allocate_id); 
      	   $('#student_id').val(student_id); 
      	   $('#credits').val(credits); 
           $('#approve').modal('show');
      }

 </script>  
  
@stop
